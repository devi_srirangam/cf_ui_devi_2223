var hostName = window.location.hostname;
var isDevweb =/devweb/i.test(hostName);
var isSLWeb = /slweb/i.test(hostName);
var isWebappCheck = /webapp/i.test(hostName);
var isProd = false;
var _GA = 'UA-56350345-1';
var apicallurl = '';

if(isDevweb) {
    isProd = false;
}else if(isSLWeb) {
    isProd = false;
}else if(isWebappCheck == false){
    isProd = true;
    _GA = 'UA-37344062-1';
}else {
    isProd = true;
    _GA = 'UA-37344062-1';
}
var domainUrl = "https://"+hostName+"/";
apicallurl = "https://" + hostName + "/proxyservices/v1";

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', _GA, 'auto');
ga('send', 'pageview');


jQuery(function($) {'use strict';

	// Navigation Scroll
	/*$(window).scroll(function(event) {
		Scroll();
	});*/

	$('.navbar-collapse ul li a').on('click', function() {  
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
		return false;
	});

	// User define function
	/*function Scroll() {
		var contentTop      =   [];
		var contentBottom   =   [];
		var winTop      =   $(window).scrollTop();
		var rangeTop    =   200;
		var rangeBottom =   500;
		$('.navbar-collapse').find('.scroll a').each(function(){
			contentTop.push( $( $(this).attr('href') ).offset().top);
			contentBottom.push( $( $(this).attr('href') ).offset().top + $( $(this).attr('href') ).height() );
		})
		$.each( contentTop, function(i){
			if ( winTop > contentTop[i] - rangeTop ){
				$('.navbar-collapse li.scroll')
				.removeClass('active')
				.eq(i).addClass('active');			
			}
		})
	};*/

	$('#tohash').on('click', function(){
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
		return false;
	});

	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Slider
	$(document).ready(function() {
		var time = 7; // time in seconds

	 	var $progressBar,
	      $bar, 
	      $elem, 
	      isPause, 
	      tick,
	      percentTime;
	 
	    //Init the carousel
	    $("#main-slider").find('.owl-carousel').owlCarousel({
	      slideSpeed : 500,
	      paginationSpeed : 500,
	      singleItem : true,
	      navigation : true,
			navigationText: [
			"<i class='fa fa-angle-left'></i>",
			"<i class='fa fa-angle-right'></i>"
			],
	      afterInit : progressBar,
	      afterMove : moved,
	      startDragging : pauseOnDragging,
	      //autoHeight : true,
	      transitionStyle : "fadeUp"
	    });
	 
	    //Init progressBar where elem is $("#owl-demo")
	    function progressBar(elem){
	      $elem = elem;
	      //build progress bar elements
	      buildProgressBar();
	      //start counting
	      start();
	    }
	 
	    //create div#progressBar and div#bar then append to $(".owl-carousel")
	    function buildProgressBar(){
	      $progressBar = $("<div>",{
	        id:"progressBar"
	      });
	      $bar = $("<div>",{
	        id:"bar"
	      });
	      $progressBar.append($bar).appendTo($elem);
	    }
	 
	    function start() {
	      //reset timer
	      percentTime = 0;
	      isPause = false;
	      //run interval every 0.01 second
	      tick = setInterval(interval, 10);
	    };
	 
	    function interval() {
	      if(isPause === false){
	        percentTime += 1 / time;
	        $bar.css({
	           width: percentTime+"%"
	         });
	        //if percentTime is equal or greater than 100
	        if(percentTime >= 100){
	          //slide to next item 
	          $elem.trigger('owl.next')
	        }
	      }
	    }
	 
	    //pause while dragging 
	    function pauseOnDragging(){
	      isPause = true;
	    }
	 
	    //moved callback
	    function moved(){
	      //clear interval
	      clearTimeout(tick);
	      //start again
	      start();
	    }
	});

	//Initiat WOW JS
	new WOW().init();
	//smoothScroll
	smoothScroll.init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	$(document).ready(function() {
		//Animated Progress
		$('.progress-bar').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				$(this).css('width', $(this).data('width') + '%');
				$(this).unbind('inview');
			}
		});

		//Animated Number
		$.fn.animateNumbers = function(stop, commas, duration, ease) {
			return this.each(function() {
				var $this = $(this);
				var start = parseInt($this.text().replace(/,/g, ""));
				commas = (commas === undefined) ? true : commas;
				$({value: start}).animate({value: stop}, {
					duration: duration == undefined ? 1000 : duration,
					easing: ease == undefined ? "swing" : ease,
					step: function() {
						$this.text(Math.floor(this.value));
						if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
					},
					complete: function() {
						if (parseInt($this.text()) !== stop) {
							$this.text(stop);
							if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
						}
					}
				});
			});
		};

		$('.animated-number').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			var $this = $(this);
			if (visible) {
				$this.animateNumbers($this.data('digit'), false, $this.data('duration')); 
				$this.unbind('inview');
			}
		});
	});

	// Contact form
	/*var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),
			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">Thank you for contact us. As early as possible  we will contact you</p>').delay(3000).fadeOut();
		});
	});*/

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});

	/*//Google Map
	var latitude = $('#google-map').data('latitude');
	var longitude = $('#google-map').data('longitude');
	function initialize_map() {
		var myLatlng = new google.maps.LatLng(latitude,longitude);
		var mapOptions = {
			zoom: 14,
			scrollwheel: false,
			center: myLatlng
		};
		var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: mapvar _par
		});
	}
	google.maps.event.addDomListener(window, 'load', initialize_map);*/

	$('form').on('click','.signup',function(){
		var _parent = $(this).closest('form');
		var _name = _parent.find('[name="name"]').val().trim();
		var _email = _parent.find('[name="email"]').val().trim().toLowerCase();
		var _pwd = _parent.find('[name="pwd"]').val();
		var _cpwd = _parent.find('[name="cpwd"]').val();

        var data = {};
        data.name = _name;
        data.email = _email;
        data.pwd = _pwd;
        data.cpwd = _cpwd;
        data.instance = 'ORANGE';
        dovalidation(data, _parent);
    });

});


function dovalidation(object, _parent){
    var statusMsg = _parent.find('.statusMessage');
    var check = _parent.find('[type="checkbox"]').is(':checked');
    var _a = [];
    if (object.name.length < 1) {
        _a.push("nom");
    }
    if (object.email.length < 1) {
        _a.push("email");
    }
    if (object.pwd == "") {
        _a.push("mot de passe");
    }
    if (object.cpwd == "") {
        _a.push("Confirmez le mot de passe");
    }
    if (_a.length > 1) {
        return statusMsg.html("S'il vous pla&icirc;t remplir tous les champs n&eacute;cessaires.");
    }
    else if (_a.length == 1) {
        _parent.find('[name="'+_a[0]+'"]').focus();
        return statusMsg.html("Entrez s'il vous plait " + _a[0] + ".");
    }
    else if (check == false) {
        return statusMsg.html("S’il vous plait, acceptez les conditions d’utilisation.");
    }
    else {
        statusMsg.html('');
        var _name = cfValidations.name(object.name);
        if (_name == true) {
            var _email = cfValidations.email(object.email);
            if (_email == true) {
                var _s = cfValidations.searchUser(object.email);
                if (_s == true) {
                    var _pwd = cfValidations.password(object.pwd, object.cpwd);//CryptoJS.MD5(data.pwd).toString()
                    if (_pwd == true) {
                        object.enable = true;
                        var signup = cfValidations.signup(object);
                        if (signup != "failed") {
                            localStorage.setItem("UserAuthDetails", cfValidations.BasicAuth(object.email, CryptoJS.MD5(object.pwd).toString()));
                            localStorage.setItem('UserId', signup.id);
                            localStorage.setItem('UserName', signup.lastName);
                            localStorage.setItem('CFUser', JSON.stringify(signup));
                            window.location.href = '/pages/cloudmanager.html#orange'
                        }
                        else if (signup != "failed") {
                            alert('signup complete');
                            window.history.pushState("CloudFuze", "Signup Complete", window.location.pathname + "?signup=" + object.email);
                            //_gaq.push(['_trackPageview', window.location.href]);
                        }
                    }
                    else {
                        statusMsg.html(_pwd);
                        return _parent.find('[name="pwd"]').focus();
                    }
                }
                else {
                    statusMsg.html(_s);
                    return _parent.find('[name="email"]').focus();
                }
            }
            else {
                statusMsg.html(_email);
                return _parent.find('[name="email"]').focus();
            }
        }
        else {
            statusMsg.html(_name);
            return _parent.find('[name="name"]').focus();
        }
    }
}




var cfValidations = {
    name: function (a) {
        var user = /[^a-zA-Z ]/;
        var userlen = /[^a-zA-Z ]{2,20}/;
        if (user.test(a)) {
            return "Nom peut contenir que des lettres (az et AZ) .please re Entrer.";
        }
        else if (userlen.test(a)) {
            return "Nom doit &ecirc;tre 2-20 caract&egrave;res length.Please reenter.";
        }
        else {
            return true;
        }
    },
    email: function (b) {
        var email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!email.test(b)) {
            return "S'il vous pla&icirc;t, mettez une adresse email valide."
        }
        else {
            return true;
        }
    },
    password: function (c, d) {
        if (c.length < 6 || c.length > 20) {
            return "Mot de passe doit &ecirc;tre 6-20 caract&egrave;res.";
        }
        else if (c != d) {
            return "Mot de passe et la confirmation de passe ne correspondent pas. S'il vous pla&icirc;t entrer de nouveau.";
        }
        else {
            return true;
        }
    },
    searchUser: function (b) {
        var _b = '';
        $.ajax({
            type: "GET",
            url: apicallurl + "/users/validate?searchUser=" + b,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                if (data != "") {
                    return _b = "Cette adresse email est d&eacute;j&agrave; enregistr&eacute;e. S'il vous pla&icirc;t s&eacute;lectionner une adresse e-mail diff&eacute;rente pour enregistrer un nouveau compte.";
                }
                else {
                    return _b = true;
                }
            }
        });
        return _b;
    },
    signup: function (data) {
        var _a = '';
        var arr1 = {
            'type': 'USER',
            'role': 'SUBSCRIBER',
            'enabled': data.enable,
            'lastName': data.name,
            'mobileNumber': null,
            'userGroups': null,
            'createdDate': null,
            'lastUpdatedDate': null,
            'address1': null,
            'address2': null,
            'region': null,
            'city': null,
            'country': null,
            'zipcode': null,
            'userName': data.email,
            'primaryEmail': data.email,
            'password': CryptoJS.MD5(data.pwd).toString(),
            'instance': data.instance,
            'currency':'EUR'
        };
        var apiUrl = apicallurl + "/users/add?domainUrl=" + domainUrl + "/activeNew&trail=30&isEnable=true";
        $.ajax({
            type: 'PUT',
            url: apiUrl,
            async: false,
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(arr1),
            dataType: 'json',
            success: function (result) {
                return _a = result;
            },
            error: function () {
                return _a = "failed";
            }
        });
        return _a;
    },
    BasicAuth: function (a, b) {
        var c = a + ':' + b;
        c = Base64.encode(c);
        return "Basic " + c;
    }
};