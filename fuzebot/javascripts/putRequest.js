/**
 * Created by CloudFuze on 12/1/2014.
 */
//var jsonData = data;
localStorage.removeItem('cfObject');
var ajaxCall = {
    cloudput : function(url,data){
        $.ajax({
            type: 'PUT',
            url: decodeURIComponent(url),
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE"
            },
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (cloudObj) {
                closebox("Completed",data);
            },
            complete:function(xhr, statusText){
                if(xhr.status == 409){
                    localStorage.setItem("OauthProcess", "alreadyExist");
                    closebox("alreadyExist",data);
                }else if(xhr.status == 403){
                    if(xhr.getResponseHeader('exception') != undefined){
                        localStorage.setItem("OauthProcess", "restricted");
                        closebox("restricted",data);
                    }
                    else{
                        localStorage.setItem("OauthProcess", "failed");
                        closebox("failed",data);
                    }
                }
                else if(xhr.status > 300){
                    localStorage.setItem("OauthProcess", "failed");
                    closebox("failed",data);
                }
            }
        });
    },
    search : function(data){
        data = Base64.decode(data);
        data = JSON.parse(data);
        $.ajax({
            type: "GET",
            url: data.url+""+data.userName,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success:function(data, textStatus, xhr) {

            }
        });
    },
    login : function(data){
        data = Base64.decode(data);
        data.password = CryptoJS.MD5(data.password);
        $.ajax({
            type: 'PUT',
            url: decodeURIComponent(url),
            headers: {
                "Content-Type": "application/json",
                "Authorization": ajaxCall.BasicAuth(data.email,data.password),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE"
            },
            dataType: 'json',
            success: function (cloudObj) {
            }
        });
    },
    BasicAuth:function(username, password) {//TODO move this function to a common utility file - Mike
        var tok = username + ':' + password;
        var hash = Base64.encode(tok);
        return "Basic " + hash;
    }//TODO resolve error - Mike  :  Primitive value returned from constructor will be lost when called with 'new'
};

var urlParams;
(window.onpopstate = function () {
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) {
            return decodeURIComponent(s.replace(pl, " "));
        },
        query = window.location.search.substring(1);
    urlParams = {};
    while (match = search.exec(query)) {
        urlParams[decode(match[1])] = decode(match[2]);
    }
    if(urlParams.hasOwnProperty("data")){
        var data = JSON.parse(Base64.decode(urlParams["data"])),
            _url = data['putUrl'];
        delete data['putUrl'];
        ajaxCall.cloudput(_url,data);
    }else if(urlParams.hasOwnProperty("login")){
        var login = window.location.search.split('?')[1].split('=')[1];
    }else if(urlParams.hasOwnProperty('search')){
        ajaxCall.search(decodeURIComponent(urlParams["search"]));
    }
})();

function closebox(status,data) {
    if (typeof (Storage) !== "undefined") {
        $('#processStatemsg').text("operation " + status);
        if(data.cloudName == 'CISCO_SPARK'){
            localStorage.setItem("OauthCloud", 'CISCO_SPARK');
        }
        localStorage.setItem("OauthProcess", status);
        parent.window.close();
    }else {
        window.location.href = domainURL + "/oauth/closebox.html?status=" + status;
    }
}