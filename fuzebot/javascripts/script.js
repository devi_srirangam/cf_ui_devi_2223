//Load user data
var loadUserData = {
	Load : function(){
		if (localStorage.CFUser){
			var UserObj = JSON.parse(localStorage.getItem('CFUser')); 
			$('.nav.navbar-nav.navbar-right .dropdown > a').html(UserObj.lastName +" <b class='caret'></b>");
		}
	}
};
$.fn.setCursorPosition = function(pos) {
		  this.each(function(index, elem) {
			if (elem.setSelectionRange) {
			  elem.setSelectionRange(pos, pos);
			} else if (elem.createTextRange) {
			  var range = elem.createTextRange();
			  range.collapse(true);
			  range.moveEnd('character', pos);
			  range.moveStart('character', pos);
			  range.select();
			}
		  });
		  return this;
		};
// CloudFuze home page Left navigation events
var CFHPLeftnav = {
    drives: $('#sidebarContent .nav.nav-list.tree'),
    driveHeaders: $('#sidebarContent #driveHeading'),
    driveContainer: $('.driveContainer'),
    driveToggle: $('.driveToggle'),
    addCategory: $('#categoryList > #addCategory'),
	addTag:$('#addTag'),
    init: function (){
        // Loading Data
        loadUserData.Load();
        CFHPLeftnav.drives.slideUp(0);
        //Toggle Category..	
        $('#sidebarContent').find('.category .defaultFloat').on('click', function (){
			var driveContainer = $('#sidebarContent').find('.driveContainer');
            var driveContainer2 = $(this).parents('.drivecontent').find('.driveContainer');
            var slimScroll = $(this).parents('.drivecontent').find('.slimScrollDiv');
            var toggleActive = $(this).parents('.drivecontent').find('.driveToggle');
            function collapseDrive(){
                $('.drivecontent').removeClass('active');
                $('.driveToggle').removeClass('open');
                $('.drivecontent .slimScrollDiv, .driveContainer').slideUp();
            }
            if ($(this).parents('.drivecontent').hasClass('active')){
                CFHPLeftnav.driveContainer.slideUp();
                CFHPLeftnav.driveContainer.removeClass('open');
                collapseDrive();
            } else{
                collapseDrive();
                $(this).parents('.drivecontent').addClass('active');
                driveContainer2.addClass('open');
                driveContainer2.slideDown();
                slimScroll.slideDown();
                toggleActive.addClass('open');
            }
        });
        // Add category		
		$('#categoryList').on('click','#addCategory',function (){
			if($('#categoryList li:has(input)').length > 0){
				return false;
			}
		    var categoryUl = $(this).parent('#categoryList');
		    categoryUl.prepend('<li id="newCategory1"><input type="text" val="" class="categoryInput" style="margin-bottom:1px;height:20px"/><i class="ctRemove"></i><i class="OkCategoryEdit"></i></li>');
			$('.categoryInput').focus();
		    categoryInput = $('#categoryList #newCategory1 input[type="text"]');
			editbtn = $('#categoryList .OkCategoryEdit');
		    $(this).hide();
		    $('.OkCategoryEdit').on('click',function () {
				var thisVal = $('.categoryInput').val().trim();
		        var check = /^[-a-zA-Z 0-9._,]+$/.test(thisVal);
		        if (thisVal.length > 125) {
		            $('.categoryInput').css('border-color', 'red');
		            return false;
		        }else if (!check) {
		            $('.categoryInput').css('border-color', 'red');
		            return false;
		        }else{
					var catname = [];
					$(this).parent().siblings('li').children('a').each(function(){
						catname.push($(this).text());
					});
					for(var i=0;i<catname.length;i++){
						if(thisVal.toLowerCase() == catname[i].toLowerCase()){
							$('.categoryInput').css('border-color', 'red');
                            //$.smallBox({title:"Category already exists,please enter another name.",color:"#e35e00",timeout:notifyTime,sound:false});
                            showNotyNotification("error","Category already exists,please enter another name.");
                            return false;
						}
				    }
				}
				CFManageCategoryAjaxCall.addCategory(thisVal);
		        $('.categoryInput').parent().remove();
		        $('#addCategory').show();
		    });
		    $('body').on('mouseup',function(e){
				if(categoryInput.is(e.target) || editbtn.is(e.target)){}
				else{
					$('.ctRemove').trigger('click');
					$('body').off('mouseup');
				}
			});
			categoryInput.keydown(function (e) {
		        if (e.keyCode == 13) {
		            $('.OkCategoryEdit').trigger('click');
		        }else if(e.keyCode == 27){
					$('.ctRemove').trigger('click');
				}
		    });
			$('.ctRemove').on('click',function(){
				$(this).parent().remove();
				$('#addCategory').show();
			});
		});
		/* Edit Category */
		$('#categoryList').on('click', '.editCategory', function (){
			if($('#categoryList li:has(input)').length > 0){
				return false;
			}
            var currentVal = $(this).parent().children('a').text().trim();
            var categoryId = $(this).parent().attr('id');
            $(this).parent().html('<input type="text" val='+currentVal+' placeholder=' + currentVal + ' class="categoryInput" style="margin-bottom:1px;height:20px"/><i class="CancelCategoryEdit"></i><i class="OkCategoryEdit"></i>');
			$('.categoryInput').focus();
            var editCategoryInput = $('.categoryInput');
			var catedit = $('.categoryInput').siblings('.OkCategoryEdit');
            editCategoryInput.val(currentVal);
			$('.categoryInput').setCursorPosition(0);
			$('.OkCategoryEdit').on('click',function (){
                var editedVal = $('.categoryInput').val().trim();
				var catname = [];
				$(this).parent().siblings('li').children('a').each(function(){
					catname.push($(this).text());
				});
				if(currentVal == editedVal){
					$('.categoryInput').parent('li').html('<a href="#" class="getFiles" id="' + categoryId + '">' + currentVal + '</a><i class="removeCategory" data-toggle="modal" data-target="#myModal2"></i><i class="editCategory"></i>');
					return false;
				}
                var check = /^[-a-zA-Z 0-9._,]+$/.test(editedVal);
                if (editedVal.length > 125) {
                    $('.categoryInput').css('border-color', 'red');
                    return false;
                }else if (!check) {
                    $('.categoryInput').css('border-color', 'red');
                    return false;
                }else if($.inArray(editedVal,catname) > -1) {
					$('.categoryInput').css('border-color', 'red');
                    //$.smallBox({title:"Category already exists, please enter another name.",color:"#e35e00",timeout:notifyError,sound:false});
                    showNotyNotification("error","Category already exists,please enter another name.");
                    return false;
				}else{
					$('.categoryInput').css('border-color', '');
                    CFManageCategoryAjaxCall.editCategoryName(categoryId, editedVal);
				}
            });
			$('.CancelCategoryEdit').on('click',function(){
				$('.categoryInput').parent('li').html('<a href="#" class="getFiles" id="' + categoryId + '">' + currentVal + '</a><i class="removeCategory" data-toggle="modal" data-target="#myModal2"></i><i class="editCategory"></i>');
				return false;
			});
			$('body').on('mouseup',function(e){
				if(editCategoryInput.is(e.target) || catedit.is(e.target)){
				}else{
					$('.CancelCategoryEdit').trigger('click');
					$('body').off('mouseup');
				}
			});
            editCategoryInput.keydown(function (event){
                if(event.keyCode == 13){
                   $('.OkCategoryEdit').trigger('click');
                }
				if(event.keyCode == 27){
					$('.CancelCategoryEdit').trigger('click');
				}
            });
        });
		/* Remove Category Event */		
		$('#categoryList').on('click','.removeCategory',function () {
			var catId= $(this).parent('li').attr('id');
			$('#deletemodal').modal();
			var catname = $(this).parent().children('a').text();
			$('#deletefilename').css('font-size','20pt');
			$('#deletemodal .deletemodalid').attr('id','CFDeleteCategory');
			$('#deletesection').text("Are you sure you want to delete the selected Category?");
			$('#deletefilename').text(catname);
			$('#CFDeleteCategory').attr("data-type",catId);
			$('#CFDeleteCategory').on('click',function (){
			    var catId=$(this).attr("data-type");
				 if(catId != ""){
				   CFManageCategoryAjaxCall.deleteCategory(catId);
				   $('#CFDeleteCategory').attr("data-type","");
				 }
			});
		});
   // Add Tag		
		CFHPLeftnav.addTag.on('click',function (){
		if($('#TagList li:has(input)').length > 0){
				return false;
			} 
		    var tagUl = $(this).parents('#TagList');
		    tagUl.prepend('<li id="newTag1"><input type="text" val="" class="tagInput"/><i class="OkTagEdit"></i><i class="tagRemove"></i></li>');
			$('.tagInput').focus();
		    var tagInput = $('#TagList #newTag1 input[type="text"]');
		    $(this).hide();
		    $('.OkTagEdit').on('click',function () {
				var thisVal = $('.tagInput').val().trim();
		        var regexp = new RegExp('/^[-a-zA-Z 0-9._,]+$/');
		        if (thisVal.length > 125) {
		            $('.tagInput').css('border-color', 'red');
		            return false;
		        }else if (!thisVal.match(regexp)) {
		            $('.tagInput').css('border-color', 'red');
		            return false;
		        }
		        $('.tagInput').parent().remove();
		        $('#addTag').show();
		        //var tagHeight = $('.driveContainer .nav').height();
		        //$('.driveContainer').slimScroll({ scrollTo: tagHeight });
		    });
			tagInput.keydown(function (e) {
		        if (e.keyCode == 13) {
		            var thisVal = $(this).val().trim();
		            var regexp = new RegExp('/^[-a-zA-Z 0-9._,]+$/');
		            if (thisVal.length > 125) {
		                $('.tagInput').css('border-color', 'red');
		                return false;
		            }else if (!thisVal.match(regexp)) {
		                $('.tagInput').css('border-color', 'red');
		                return false;
		            }else { $('.tagInput').css('border-color', ''); }
		            $(this).parent().remove();
		            $('#addTag').show();
		            //var tagHeight = $('.driveContainer .nav').height();
		            //$('.driveContainer').slimScroll({ scrollTo: tagHeight });
		        }else if(e.keyCode == 27){
					$(this).parent().remove();
					$('#addTag').show();				
				}
		    });
		});

		$('#sidebar-wrapper').on('click','.tagRemove',function(){
			$(this).parent().remove();
			$('#addTag').show();
		});
        /*Edit Tag*/
        $('#sidebarContent').on('click', '.editTag', function () {	
		    if($('#TagList li:has(input)').length > 0){
				return false;
			}
            var currentCname = $(this).parent().children('a');
            var currentVal = currentCname.text();
            var tagId = currentCname.attr("id");
             $(this).parent().html('<input type="text" val="" placeholder=' + currentVal + ' class="categoryInput"/><i class="tageditok"></i><i class="tageditcancel"></i>');
			$('.categoryInput').focus();
            var editTagInput = $('.categoryInput');
            editTagInput.val(currentVal);
			$('.tageditcancel').on('click',function(){
				$(this).parent().html('<a href="#" class="getFilesforTag" id="'+tagId+'">' + currentVal + '</a><i class="editTag"></i><i class="removeTag" data-toggle="modal" data-target="#myModal2"></i>');
				return false;
			});
            $('.tageditok').on('click',function () {
                var editedVal = $('.categoryInput').val();
				if(editedVal == currentVal){
					$(this).parent().html('<a href="#" class="getFilesforTag" id="'+tagId+'">' + currentVal + '</a><i class="editTag"></i><i class="removeTag" data-toggle="modal" data-target="#myModal2"></i>');
					return false;
				}
                var regexp = new RegExp('/^[a-zA-Z0-9 ._-]+$/');
                if (editedVal.length > 125) {
                    $('.categoryInput').css('border-color', 'red');
                    return false;
                }else if (!editedVal.match(regexp)) {
                    $('.categoryInput').css('border-color', 'red');
                    return false;
                }else { $('.categoryInput').css('border-color', ''); }
                $(this).parent().html('<a href="#" class="getFilesforTag" id="'+tagId+'">' + editedVal + '</a><i class="editTag"></i><i class="removeTag" data-toggle="modal" data-target="#myModal2"></i>');
                //var categoryHeight = $('.driveContainer .nav').height();
                //$('.driveContainer').slimScroll({ scrollTo: categoryHeight });
            });
            editTagInput.keypress(function (event) {
                if (event.keyCode == 13) {
                    var editedVal = $(this).val();
                    var regexp = new RegExp('/^[a-zA-Z0-9 ._-]+$/');
                    if (editedVal.length > 125) {
                        $('.categoryInput').css('border-color', 'red');
                        return false;
                    }else if (!editedVal.match(regexp)) {
                        //$('.drivecontent .DisplayMsg').text('Invalid characters');
                        $('.categoryInput').css('border-color', 'red');
                        return false;
                    }else { $('.categoryInput').css('border-color', ''); }
                    $(this).parent().html('<a href="#" class="getFilesforTag" id="'+tagId+'">' + editedVal + '</a><i class="editTag"></i><i class="removeTag" data-toggle="modal" data-target="#myModal2"></i>');
                    //var categoryHeight = $('.driveContainer .nav').height();
                    //$('.driveContainer').slimScroll({ scrollTo: categoryHeight });
                }
            });
        });
    }
};
$('#logout').on('click', function(e) {
    e.preventDefault();
    sessionStorage.clear();
	localStorage.clear();
	window.location.href = "/pages/index.html";
});

$('#ListContent .panel-data[name="Workspace"]').find('input[type="checkbox"]').hide();

$('#CFHRename').on('click', function(e) {
    sendGAEvents("Rename");
    //_gaq.push(['_trackEvent', "Rename" , localStorage.getItem('UserId'),PageName]);
    var renameReg = /(\?|'|\\|;|:|"|}|{|\|)/;
    e.preventDefault();
    $('#CFHRename').css('pointer-events', 'none');
    $('#CFHRename > div').css('opacity', '0.2');
    var cname = $(this).parents('#page-header').find('#ListContent .LVcheckBox').children('input[type="checkbox"]:checked');
    //var wsname = $(this).parents('#page-header').find('#workspaceFiles .wsfcheckbox').children('input[type="checkbox"]:checked');
    var length = cname.length;
    var thname = $(this).parents('#page-header').find('#ThumbnailContent input[type="checkbox"]:checked');
    var thlength = thname.length;
    if (length == 1) {
        var cName = cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first');
        var reName1 = cName.attr('name');
        var classType ='',_shareStatus = 'share';
        if (PageName == "Share by Me" || PageName == "Share with Me") {
            classType = cname.parent('.LVcheckBox').siblings('.LVfileName').find('sup:eq(0)').attr('class');
            _shareStatus= cname.parent('.LVcheckBox').siblings('.LVfileName').find('.share').attr('class');
        }
        fileExention = reName1.split('.');
        fileExention = fileExention[fileExention.length - 1];
        var reName2 = reName1.split('.' + fileExention);
        var reName = reName2[0];
        var fileId = cName.parent().attr("id");
        var cloudId = cName.parent().siblings('.LVdrive').attr("id");
        if (cname.parent('.LVcheckBox').siblings('.LVfileName').children('i:first').attr('class') == 'LVFILE pull-left') {
            cname.parent('.LVcheckBox').siblings('.LVfileName').children('i:first').removeClass('LVFILE pull-left').addClass('LVFILE1 pull-left');
        }
        cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').css('width', '58%');
        cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').after('<i class="RenameOk" style="float:left"></i><i class="RenameCancel" style="float:left"></i>');
        $('body').on('mouseup', function (e) {
            var container = cname.parent('.LVcheckBox').siblings('.LVfileName');
            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                $('.RenameCancel').trigger('click');
                $('body').off('mouseup');
            }
        });
        cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').html('<input style="width:100%; height:20px;padding:0px 0px 0px 6px !important" type="text" class="renameInput" value=' + reName + '/>');
        if (cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name') == "WORKSPACE") {
            cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name', "WORKSPACE1");
        }
        if (cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name') == "FOLDER") {
            cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name', "FOLDER1");
        }
        var editTagInput = $('.renameInput');
        editTagInput.val(reName);
        $('.renameInput').focus().select();
        $('.RenameCancel').on('click', function () {
            $('body').off('click');
            var fname = reName1;
            disableActionPanel(actionPanel);
            cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').removeAttr('style');
            $('.RenameOk').remove();
            $('.RenameCancel').remove();
            cname.parent('.LVcheckBox').siblings('.LVfileName').children('p').text(CFManageCloudAccountsAjaxCall.getMaxChars(fname,50));
            cname.parent('.LVcheckBox').siblings('.LVfileName').children('p').attr('name', fname);
            if (cname.parent('.LVcheckBox').siblings('.LVfileName').children('i:first').attr('class') == 'LVFILE1 pull-left') {
                cname.parent('.LVcheckBox').siblings('.LVfileName').children('i:first').removeClass('LVFILE1 pull-left').addClass('LVFILE pull-left');
            }
            if (cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name') == "WORKSPACE1") {
                cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name', "WORKSPACE");
            }
            if (cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name') == "FOLDER1") {
                cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name', "FOLDER");
            }
            if (previousPage == "Share by Me" || previousPage == "Share with Me" || PageName == 'Share by Me' || PageName == 'Share with Me') {
                var _fileContainer = $('#ListContent div[id="' + fileId + '"]');
                var filename = _fileContainer.children('p:first').text();
                var _html = filename+'<sup class="'+classType+'"></sup><sup class="'+_shareStatus+'"></sup>';
                _fileContainer.children('p:first').html(_html);
            }
            $('#page-header input[type="checkbox"]').attr('checked', false);
            /*$('#CFHRename').css('pointer-events', 'auto');
            $('#CFHRename > div').css('opacity', '1');*/
            $('#LVContent').children('.panel-dataHoverClass').each(function () {
                $(this).removeClass('panel-dataHoverClass');
            });
        });
        $('.RenameOk').on('click', function () {
            var editedVal = $('.renameInput').val();
            if (editedVal == "") {
                if (FileType == "FILE") {
                    //$.smallBox({title: "Please enter file name.", color: "#e35e00", timeout: notifyTime, sound: false});
                    showNotyNotification("error","Please enter file name.");
                    return false;
                }
                if (FileType == "FOLDER") {
                   // $.smallBox({title: "Please enter folder name.", color: "#e35e00", timeout: notifyTime, sound: false});
                    showNotyNotification("error","Please enter folder name.");
                    return false;
                }
            }else if (editedVal == reName) {
                $('body').off('click');
                $('.RenameCancel').trigger('click');
                return false;
            }
            //else if (renameReg.test(editedVal)) {
            //    $.smallBox({title: "special characters ( ; : ' \" { } \ | ? ) are  not accepted.", color: "#e35e00", timeout: notifyTime, sound: false});
            //showNotyNotification("error","special characters ( ; : ' \" { } \ | ? ) are  not accepted.");
            //    return false;
            //}
            cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').removeAttr('style');
            editedVal = $('.renameInput').val();
            if (fileExention != "null") {
                if (reName1.indexOf('.' + fileExention) > 0) {
                    editedVal = editedVal + "." + fileExention;
                }
            }//else { editedVal = editedVal;}
            if (PageName == "WorkSpace") {
                sendGAEvents("Rename File");
                //_gaq.push(['_trackEvent', "Rename File" , localStorage.getItem('UserId'),PageName]);
                renameWorkSpaceFileName(fileId, editedVal);
                $('#page-header input[type="checkbox"]').attr('checked', false);
                $('#CFHRename').css('pointer-events', 'auto');
                $('#CFHRename > div').css('opacity', '1');
            }else {
                sendGAEvents("Rename");
                //_gaq.push(['_trackEvent', "Rename" , localStorage.getItem('UserId'),PageName]);
                var renameObject = CFHPlistview.renameFileFolder(fileId, editedVal, cloudId, renameObject);
                if (renameObject != null) {
                    $('#ThumbnailContent [id="' + fileId + '"]').attr('id', renameObject.id);
                    $('#ListContent [id="' + fileId + '"]').attr('id', renameObject.id);
                    fileId = renameObject.id;
                    editedVal = renameObject.objectName;
                    cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').text(CFManageCloudAccountsAjaxCall.getMaxChars(editedVal,50));
                    cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').attr('name', editedVal);
                    $('#ThumbnailContent [id="' + fileId + '"]').parents().children('i.filethumbnail').attr('title', editedVal);
                    $('#ThumbnailContent strong[id="' + fileId + '"]').html(CFManageCloudAccountsAjaxCall.getMaxChars(editedVal,14));
                    if (cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name') == "FOLDER1") {
                        cname.parent('.LVcheckBox').siblings('.LVfileName').attr('name', "FOLDER");
                    }
                    if (cname.parent('.LVcheckBox').siblings('.LVfileName').children('i:first').attr('class') == 'LVFILE1 pull-left') {
                        cname.parent('.LVcheckBox').siblings('.LVfileName').children('i:first').removeClass('LVFILE1 pull-left').addClass('LVFILE pull-left');
                    }
                    if (previousPage == "Share by Me" || previousPage == "Share with Me" || PageName == 'Share by Me' || PageName == 'Share with Me') {
                        var _fileContainer = $('#ListContent div[id="' + fileId + '"]');
                        var filename = _fileContainer.children('p:first').text();
                        var _html = filename+'<sup class="'+classType+'"></sup><sup class="'+_shareStatus+'"></sup>';
                        _fileContainer.children('p:first').html(_html);
                    }
                    $('#page-header input[type="checkbox"]').attr('checked', false);
                    $('.RenameOk').remove();
                    $('.RenameCancel').remove();
                }
            }
        });
        editTagInput.keydown(function (event) {
            if (event.keyCode == 13) {
                $('body').off('click');
                $('.RenameOk').trigger('click');
            }else if (event.keyCode == 27) {
                $('body').off('click');
                $('.RenameCancel').trigger('click');
            }
        });
    }
    else if (thlength == 1) {
        var fileName1 = thname.parent().children('i.filethumbnail').attr('title');
        var fileId = thname.parent().children('strong').attr('id');
        //var fileCid = thname.parent().attr('id');
        var classType ='';
        if (PageName == "Share by Me" || PageName == "Share with Me") {
            classType = thname.parent().children('sup').attr('class');
            classType = classType.split(" ");
            classType = classType[1];
        }
        fileExten = fileName1.split('.');
        fileExten = fileExten[fileExten.length - 1];
        var fileName2 = fileName1.split('.' + fileExten);
        var fileName = fileName2[0];
        thname.parent().children('.filesize').hide();
        thname.parent().children('strong').html('<input type="text" style="width:100%;height:22px;margin-bottom:1px;padding:0px 0px 0px 6px !important;" id="CFThumbRename" value="' + fileName + '" autofocus />');
        var offset = $('#CFThumbRename').offset().left;
        var check = $(document).width() - offset;
        if (check < 200) {
            $('#CFThumbRename').removeAttr('style').css('width', '100%').css('height', '22px');
            $('#CFThumbRename').css('padding', '0px 0px 0px 6px')
        }
        var pwdornot = thname.siblings('i:last').attr("class");
        if (thname.siblings('i:last').attr("id") == "ThFav") {
            thname.parents().children('#ThFav').after('<i class="RenameOk" style="position:absolute;left:0;top:139px;margin:0"></i><i class="RenameCancel" style="position:absolute;left:24px;top:139px;margin:0;"></i>');
        }
        if (thname.siblings('i:last').attr("id") == "wsLock") {
            thname.parents().children('#wsLock').after('<i class="RenameOk" style="position:absolute;left:0;top:139px;margin:0"></i><i class="RenameCancel" style="position:absolute;left:24px;top:139px;margin:0;"></i>');
        }
        $('#CFThumbRename').val(fileName);
        $('#CFThumbRename').focus().select();
        $('#CFThumbRename').keydown(function (e) {
            if (e.keyCode == 13) {
                var editedVal = $('#CFThumbRename').val();
                if (editedVal == "") {
                    if (PageName == "WorkSpace") {
                        //$.smallBox({title: "Please enter workspace name.", color: "#e35e00", timeout: notifyTime, sound: false});
                        showNotyNotification("error","Please enter workspace name.");
                        return false;
                    } else if (FileType == "FILE") {
                        //$.smallBox({title: "Please enter file name.", color: "#e35e00", timeout: notifyTime, sound: false});
                        showNotyNotification("error","Please enter file name.");
                        return false;
                    }
                    if (FileType == "FOLDER") {
                        //$.smallBox({title: "Please enter folder name.", color: "#e35e00", timeout: notifyTime, sound: false});
                        showNotyNotification("error","Please enter folder name.");
                        return false;
                    }
                }else if (editedVal == fileName) {
                    $('.RenameCancel').trigger('click');
                    return false;
                }
                //else if (renameReg.test(editedVal)) {
                //    $.smallBox({title: "special character ( ;:'{ } \ |? ) not accepted.", color: "#e35e00", timeout: notifyTime, sound: false});
                //    showNotyNotification("error","special character ( ;:'{ } \ |? ) not accepted.");
                // return false;
                //}
                if (PageName == "WorkSpace") {
                    var wslength = $('#ThumbnailContent strong.filename').length;
                    for (var i = 0; i < wslength; i++) {
                        var wsaname = $('#ThumbnailContent i.filethumbnail:eq(' + i + ')').siblings('strong').attr("name");
                        editedVal = editedVal.trim();
                        wsaname = wsaname.trim();
                        if (editedVal.toLowerCase() == wsaname.toLowerCase()) {
                            //$.smallBox({title: "This workspace name already exists.Please enter another name.", color: "#e35e00", timeout: notifyTime, sound: false});
                            showNotyNotification("error","This workspace name already exists.Please enter another name.");
                            $('#CFThumbRename').focus();
                            return false;
                        }
                    }
                }
                if (fileExten != "null") {
                    if (fileName1.indexOf(fileExten) > 0) {
                        editedVal = editedVal + "." + fileExten;
                    }
                }/*else {
                    editedVal = editedVal;
                }*/
                var renameObject;
                if (PageName == "WorkSpace") {
                    $('#page-header input[type="checkbox"]').attr('checked', false);
                    $('#CFHRename').css('pointer-events', 'auto');
                    $('#CFHRename > div').css('opacity', '1');
                    var updatwname = "";
                    var notes = "IGNORE_UPDATE";
                    fileId = encodeURIComponent(fileId);
                    updatwname += 'name=' + editedVal + '&';
                    updatwname += 'notes=' + notes + '&';
                    updatwname += 'password=' + notes;
                    sendGAEvents("Rename");
                    //_gaq.push(['_trackEvent', "Rename" , localStorage.getItem('UserId'),PageName]);
                    renameObject = CFWlistview.renameWorkspace(fileId, updatwname, editedVal, pwdornot);
                }else {
                    sendGAEvents("Rename");
                    //_gaq.push(['_trackEvent', "Rename" , localStorage.getItem('UserId'),PageName]);
                    renameObject = CFHPlistview.renameFileFolder(fileId, editedVal, cloudId, renameObject);
                }
                if (renameObject != null) {
                    $('.RenameOk').remove();
                    $('.RenameCancel').remove();
                    $('#ThumbnailContent [id="' + fileId + '"]').attr('id', renameObject.id);
                    $('#ListContent [id="' + fileId + '"]').attr('id', renameObject.id);
                    fileId = renameObject.id;
                    editedVal = renameObject.objectName;
                    cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').text(CFManageCloudAccountsAjaxCall.getMaxChars(editedVal,50));
                    $('#ThumbnailContent [id="' + fileId + '"]').parents().children('i.filethumbnail').attr('title', editedVal);
                    $('#ThumbnailContent strong[id="' + fileId + '"]').html(CFManageCloudAccountsAjaxCall.getMaxChars(editedVal,14));
                    $('#ListContent div[id="' + fileId + '"]').children('p:first').text(editedVal);
                    $('#ListContent div[id="' + fileId + '"]').children('p:first').attr('name', editedVal);
                    if (previousPage == "Share by Me" || previousPage == "Share with Me") {
                        var filename = $('#ListContent div[id="' + fileId + '"]').children('p:first').text();
                        $('#ListContent div[id="' + fileId + '"]').children('p:first').html(''+filename+'<sup class="'+classType+'"></sup>');
                    }
                    $('#page-header input[type="checkbox"]').attr('checked', false);
                    $('#CFHRename').css('pointer-events', 'auto');
                    $('#CFHRename > div').css('opacity', '1');
                    thname.parent().children('.filesize').show();
                    disableActionPanel(actionPanel);
                }
            }else if (e.keyCode == 27) {
                $('.RenameCancel').trigger('click');
            }
        });
        $('.RenameCancel').on('click', function () {
            sendGAEvents("Rename Cancel");
            //_gaq.push(['_trackEvent', "Rename Cancel" , localStorage.getItem('UserId'),PageName]);
            $('.RenameOk').remove();
            $('.RenameCancel').remove();
           
            if(PageName == "WorkSpace"){
            	  $('#CFEditWorkSpace').addClass('buttonDisable');
            	  var fileName1 = thname.parent().children('i.filethumbnail').attr('title');
            	  thname.parent().children('strong').html(CFManageCloudAccountsAjaxCall.getMaxChars(fileName1,14));

            }else if (fileExten != "null") {
                fileName = thname.parent().children('i').attr('title');
                thname.parent().children('strong').html(CFManageCloudAccountsAjaxCall.getMaxChars(fileName,14));
            }else {
                thname.parent().children('strong').html(CFManageCloudAccountsAjaxCall.getMaxChars(fileName,14));
            }
            if (previousPage == "Share by Me" || previousPage == "Share with Me") {
                var filename = $('#ListContent div[id="' + fileId + '"]').children('p:first').text();
                $('#ListContent div[id="' + fileId + '"]').children('p:first').html(''+filename+'<sup class="'+classType+'"></sup>');
            }
            thname.parent().children('.filesize').show();
            $('#page-header input[type="checkbox"]').attr('checked', false);
            $('#CFHRename').css('pointer-events', 'auto');
            $('#CFHRename > div').css('opacity', '1');
            disableActionPanel(actionPanel);
            // if (PageName == "WorkSpace") {
            //     //$('#CFEditWorkSpace').hide();
            //     $('#CFEditWorkSpace').addClass('buttonDisable');
            // }
        });
        $('.RenameOk').on('click', function () {
            var editedVal = $('#CFThumbRename').val();
            if (editedVal == "") {
                if (PageName == "WorkSpace") {
                    //$.smallBox({title: "Please enter workspace name.", color: "#e35e00", timeout: notifyTime, sound: false});
                    showNotyNotification("error","Please enter workspace name.");
                    return false;
                }else if (FileType == "FILE") {
                    //$.smallBox({title: "Please enter file name.", color: "#e35e00", timeout: notifyTime, sound: false});
                    showNotyNotification("error","Please enter file name.");
                    return false;
                }
                if (FileType == "FOLDER") {
                   //$.smallBox({title: "Please enter folder name.", color: "#e35e00", timeout: notifyTime, sound: false});
                    showNotyNotification("error","Please enter folder name.");
                    return false;
                }
            }else if (editedVal == fileName) {
                $('.RenameCancel').trigger('click');
                return false;
            }
            //else if (renameReg.test(editedVal)) {
            //    $.smallBox({title: "special character ( ;:'{ } \ |? ) not accepted.", color: "#e35e00", timeout: notifyTime, sound: false});
            //    showNotyNotification("error","special character ( ;:'{ } \ |? ) not accepted.");
            // return false;
            //}
            if (PageName == "WorkSpace") {
                var wslength = $('#ThumbnailContent strong.filename').length;
                for (var i = 0; i < wslength; i++) {
                    var wsaname = $('#ThumbnailContent i.filethumbnail:eq(' + i + ')').siblings('strong').attr("name");
                    editedVal = editedVal.trim();
                    wsaname = wsaname.trim();
                    if (editedVal.toLowerCase() == wsaname.toLowerCase()) {
                        //$.smallBox({title: "This workspace name already exists.Please enter another name.", color: "#e35e00", timeout: notifyTime, sound: false});
                        showNotyNotification("error","This workspace name already exists.Please enter another name.");
                        $('#CFThumbRename').focus();
                        return false;
                    }
                }
            }
            if (fileExten != "null") {
                if (fileName1.indexOf('.' + fileExten) > 0) {
                    editedVal = editedVal + "." + fileExten;
                }
            }/*else {
                editedVal = editedVal;
            }*/
            var renameObject;
            if (PageName == "WorkSpace") {
                $('#page-header input[type="checkbox"]').attr('checked', false);
                $('#CFHRename').css('pointer-events', 'auto');
                $('#CFHRename > div').css('opacity', '1');
                var updatwname = "";
                var notes = "IGNORE_UPDATE";
                fileId = encodeURIComponent(fileId);
                /* editedVal=encodeURIComponent(editedVal);	 */
                updatwname += 'name=' + editedVal + '&';
                updatwname += 'notes=' + notes + '&';
                updatwname += 'password=' + notes;
                sendGAEvents("Rename Ok");
                //_gaq.push(['_trackEvent', "Rename Ok" , localStorage.getItem('UserId'),PageName]);
                renameObject = CFWlistview.renameWorkspace(fileId, updatwname, editedVal, pwdornot);
            }else {
                sendGAEvents("Rename Ok");
                //_gaq.push(['_trackEvent', "Rename Ok" , localStorage.getItem('UserId'),PageName]);
                renameObject = CFHPlistview.renameFileFolder(fileId, editedVal, cloudId, renameObject);
            }
            if (renameObject != null) {
                $('.RenameOk').remove();
                $('.RenameCancel').remove();
                $('#ThumbnailContent [id="' + fileId + '"]').attr('id', renameObject.id);
                $('#ListContent [id="' + fileId + '"]').attr('id', renameObject.id);
                fileId = renameObject.id;
                editedVal = renameObject.objectName;
                cname.parent('.LVcheckBox').siblings('.LVfileName').children('p:first').text(CFManageCloudAccountsAjaxCall.getMaxChars(editedVal,50));
                $('#ThumbnailContent [id="' + fileId + '"]').parents().children('i.filethumbnail').attr('title', editedVal);
                $('#ThumbnailContent strong[id="' + fileId + '"]').html(CFManageCloudAccountsAjaxCall.getMaxChars(editedVal,14));
                $('#ListContent div[id="' + fileId + '"]').children('p:first').text(editedVal);
                $('#ListContent div[id="' + fileId + '"]').children('p:first').attr('name', editedVal);
                thname.parent().children('.filesize').show();
                if (previousPage == "Share by Me" || previousPage == "Share with Me") {
                    var filename = $('#ListContent div[id="' + fileId + '"]').children('p:first').text();
                    $('#ListContent div[id="' + fileId + '"]').children('p:first').html(''+filename+'<sup class="'+classType+'"></sup>');
                }
                $('#page-header input[type="checkbox"]').attr('checked', false);

                $('#CFHRename').css('pointer-events', 'auto');
                $('#CFHRename > div').css('opacity', '1');
                disableActionPanel(actionPanel);
            }
        });
        $('body').on('mouseup', function (e) {
            var container = thname.parent().children('.filename').children('input');
            var container1 = thname.parent().children('.RenameOk');
            if (container.is(e.target) || container1.is(e.target)) {}
            else {
                $('.RenameCancel').trigger('click');
                $('body').off('mouseup');
            }
        });
    }
    if (PageName == "InnerWorkSpace") {
		var checkBox = $(this).parents('#page-header').find('#workspaceFiles .wsfcheckbox').children('input[type="checkbox"]:checked');
		var inputPlace = checkBox.closest('tr').children('td:eq(2)');
		var prevHtml = inputPlace.html();
		var extention = inputPlace.closest('tr').attr('fexten');
		var fullname = decodeURIComponent(inputPlace.attr('name'));
		var fileType = inputPlace.prev().children('i').attr('class');
		var fileId = decodeURIComponent(inputPlace.attr('id'));
		var cloudId = inputPlace.attr('cloudid');
		var name = '';
		//Check For Folder / File
		if(fileType == "LVFIL"){
			var ext = "."+extention;
			name = fullname.split(ext)[0];
		}else{	name = fullname;}
		var appendHtml ="<span style='width: 58%;'>" +
			"<input style='width:71%; height:20px;padding:0px 0px 0px 6px !important' type='text' class='renameInput' value='"+name+"'>" +
			"</span>" +
			"<i class='RenameCancel'></i>" +
			"<i class='RenameOk'></i>";
		inputPlace.html(appendHtml);
		inputPlace.find('input').focus();
		inputPlace.addClass('Rename');
		inputPlace.find('.RenameCancel').on('click',function(){
			inputPlace.html(prevHtml);
			inputPlace.removeClass('Rename');
			checkBox.prop('checked',false);
		});
		inputPlace.find('.RenameOk').on('click',function(){
			var newName = $('#workspaceFiles').find('input[type="text"]').val();
			if(newName == name){
				return false;
			}else{
				if(newName.length == 0){
					//$.smallBox({title: "Please enter file name.", color: "#e35e00", timeout: notifyTime, sound: false});
                    showNotyNotification("error","Please enter file name.");
					return false;
				}
                //else if (renameReg.test(newName)) {
				//	$.smallBox({title: "special character( ;:'{ } \ |? ) not accepted.", color: "#e35e00", timeout: notifyTime, sound: false});
				//	showNotyNotification("error","special character ( ;:'{ } \ |? ) not accepted.");
                // return false;
				//}
                else{
					var finalName = "";
					if(fileType == "LVFIL"){finalName = newName+"."+extention;}
					else{finalName = newName;}
					var renameObject = CFHPlistview.renameFileFolder(fileId, finalName, cloudId, renameObject);
					inputPlace.attr('name',encodeURIComponent(renameObject.objectName));
					var html = "<span style='display:inline-block;width:100%'>"+finalName+"</span>";
					inputPlace.html(html);
					checkBox.prop('checked',false);
				}
			}
		});

        $('body').on('mouseup', function (e) {
            if (inputPlace.is(e.target) ||
                inputPlace.find('input').is(e.target) ||
                inputPlace.find('.RenameCancel').is(e.target) ||
                inputPlace.find('.RenameOk').is(e.target)
            ) {}
            else {
                $('.RenameCancel').trigger('click');
                $('body').off('mouseup');
            }
        });

		$('#workspaceFiles').find('input[type="text"]').keydown(function (event) {
			if (event.keyCode == 13) {
				$('body').off('click');
				$('.RenameOk').trigger('click');
			}else if (event.keyCode == 27) {
				$('body').off('click');
				$('.RenameCancel').trigger('click');
			}
		});
    }
});