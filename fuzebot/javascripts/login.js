window.CloudFuze = {};
window.CloudFuze.orangeLanding1 = '<div style="color: #fff;text-align: justify;font-size: 15px;">'+
    '<h4>&Egrave;largissez la port&egrave;e et le potentiel de votre cloud Orange avec CloudFuze !</h4>'+
    '<p>Pour tous vos fichiers. Sur tous vos appareils. O&ugrave; que vous soyez.</p>'+
    '<p>B&egrave;n&egrave;ficiez d\'un large &egrave;ventail de nouvelles fonctionnalit&egrave;s en acc&egrave;dant &agrave; votre espace de stockage Orange via CloudFuze :</p>'+
    '<ul style="list-style-type: disc;margin: 20px 0 20px 20px;">'+
    '<li>Un acc&egrave;s et une gestion multi-comptes : utilisez CloudFuze pour acc&egrave;der &agrave; votre cloud Orange et &agrave; d\'autres services de stockage &agrave; partir d\'un seul compte, et g&egrave;rer vos fichiers ainsi que leur transfert d\'un cloud &agrave; un autre plus facilement.</li>' +
    '<li>Un acc&egrave;s mobile : acc&egrave;dez &agrave; tous vos fichiers depuis vos appareils mobiles avec les applications CloudFuze pour iOS et Android.</li>'+
    '<li>Une capacit&egrave; de partage optimis&egrave;e : partagez facilement tous vos fichiers, quelles que soient leurs tailles, et b&egrave;n&egrave;ficiez de fonctionnalit&egrave;s de s&egrave;curit&egrave; avanc&egrave;es pour les mots de passe, les limitations de t&egrave;l&egrave;chargement et les dates d\'expiration.</li>'+
    '</ul>'+
    '<p>CloudFuze propose un panel d\'applications web, iOS, Android, et de bureau pour Windows. Nos clients peuvent pleinement b&egrave;n&egrave;ficier, o&ugrave; qu\'ils soient et depuis n\'importe quel appareil, de fonctionnalit&egrave;s de partage optimis&egrave;es, d\'espaces de travail partag&egrave;s et d\'autres fonctionnalit&egrave;s am&egrave;liorant la productivit&egrave;, sur leurs comptes de stockage Orange.</p>'+
    '<p>Cr&egrave;er votre compte CloudFuze et commencez &agrave; utiliser nos applications pour optimiser les possibilit&egrave;s d\'utilisation de votre cloud Orange au maximum !</p>'+
    '<p>Si vous souhaitez essayer le service, nous vous invitons de <a href="index.html?signup">cliquer ici</a></p>';
    '</div>';

/*window.CloudFuze.orangeLanding = '<div style="color: #000;text-align: justify;font-size: 15px;font-family: \'Raleway\', Arial, sans-serif;" class="span12">'+
    '<h4>  CloudFuze : c&#39;est quoi ?</h4>'+
    '<p>CloudFuze est une solution vous permettant d&#39;avoir un acc&egrave;s unique &agrave; tous vos services de stockage en ligne.</p>'+
    '<h4><p>B&eacute;n&eacute;ficiez d&#39;un large &eacute;ventail de nouvelles fonctionnalit&eacute;s en acc&eacute;dant &agrave; votre espace de stockage Orange via CloudFuze</p></h4>'+
    '<ul> '+
    '<li>Un acc&egrave;s et une gestion multi-comptes : utilisez CloudFuze pour acc&eacute;der &agrave; votre cloud Orange et &agrave; d&#39;autres services de stockage &agrave; partir d&#39;un seul compte, et g&eacute;rer vos fichiers ainsi que leur transfert d&#39;un cloud &agrave; un autre plus facilement.     </li>'+
    '<li>Un acc&egrave;s mobile : acc&eacute;dez &agrave; tous vos fichiers depuis vos appareils mobiles avec les applications CloudFuze pour iOS et Android.</li>'+
    '<li>Une capacit&eacute; de partage optimis&eacute;e : partagez facilement tous vos fichiers, quelles que soient leurs tailles, et b&eacute;n&eacute;ficiez de fonctionnalit&eacute;s de s&eacute;curit&eacute; avanc&eacute;es pour les mots de passe, les limitations de t&eacute;l&eacute;chargement et les dates d&#39;expiration.</li>'+
    '</ul>'+

    '<p>CloudFuze propose un panel d&#39;applications web, iOS, Android, et de bureau pour Windows.  </p>'+
    '<p>Le service est propos&eacute; en langue anglaise exclusivement.</p>'+
    '<h4>D&eacute;couvrez  le service sans engagement</h4>'+
    '<ul>'+
    '<li>Cr&eacute;er votre compte CloudFuze et d&eacute;couvrez les possibilit&eacute;s offertes</li>'+
    '<li>Les utilisateurs Orange disposent d&#39;une offre sp&eacute;cifique gratuite pendant 1 an</li>'+
    '</ul>'+
    '</div>'+
    '<div class="span4"><div class="orangeOffer">'+
    '<div class="offer"><p>offre sp&egrave;ciale</p> <p>gratuit 1 an pour toute inscription avant le 31/12/2015</p></div>'+
    '<span class="a"><strong>Souscription mensuelle</strong></span><span class="b"><i></i><h2>4.99&euro;/</h2>Mois</span>' +
    '<span class="c"><p>Application Mobile, Web, Bureau</p><p>Comptes illimit&eacute;s</p><p>Acc&egrave;s depuis un nombre illimit&eacute; de terminaux </p></span></div></div></div>';

*/
window.CloudFuze.orangeLanding = '<div style="color: #000;text-align: justify;font-size: 15px;font-family: \'Raleway\', Arial, sans-serif;" class="span12">'+
    '<h4>CloudFuze : c&#39;est quoi ?</h4>'+
    '<p>CloudFuze est une solution vous permettant d&#39;avoir un acc&egrave;s unique &agrave; tous vos services de stockage en ligne.</p>'+
    '<h4>B&eacute;n&eacute;ficiez d&#39;un large &eacute;ventail de nouvelles fonctionnalit&eacute;s en acc&eacute;dant &agrave; votre espace de stockage Orange via CloudFuze</h4>'+
    '<ul class="loginList"><li>Un acc&egrave;s et une gestion multi-comptes : utilisez CloudFuze pour acc&eacute;der &agrave; votre cloud Orange et &agrave; d&#39;autres services de stockage &agrave; partir d&#39;un seul compte, et g&eacute;rer vos fichiers ainsi que leur transfert d&#39;un cloud &agrave; un autre plus facilement.</li>' +
    '<li>Un acc&egrave;s mobile : acc&eacute;dez &agrave; tous vos fichiers depuis vos appareils mobiles avec les applications CloudFuze pour iOS et Android.</li>' +
    '<li>Une capacit&eacute; de partage optimis&eacute;e : partagez facilement tous vos fichiers, quelles que soient leurs tailles, et b&eacute;n&eacute;ficiez de fonctionnalit&eacute;s de s&eacute;curit&eacute; avanc&eacute;es pour les mots de passe, les limitations de t&eacute;l&eacute;chargement et les dates d&#39;expiration.</li></ul>'+
    '<p style="margin:0">&nbsp;</p><p>CloudFuze propose un panel d&#39;applications web, iOS, Android, et de bureau pour Windows.</p>' +
    '<i>Le service est propos&eacute; en langue anglaise exclusivement.</i>'+
    '<div class="span12" style="margin: 0">' +
    '<div class="span8"><h4>D&eacute;couvrez  le service sans engagement</h4>' +
    '<ul class="loginList"><li>Cr&eacute;er votre compte CloudFuze et d&eacute;couvrez les possibilit&eacute;s offertes</li>' +
    '<li>Les utilisateurs Orange disposent d&#39;une offre sp&eacute;cifique gratuite pendant 1 an</li></ul></div>' +
    '<div class="span4"><div class="orangeOffer"><div class="offer">offre sp&egrave;ciale gratuit 1 an pour toute inscription avant le 31/12/2015</div><span class="a"><strong>Souscription mensuelle</strong></span><span class="b"><i></i><h2>4.99&euro;/</h2>Mois</span>' +
    '<span class="c"><p>Application Mobile, Web, Bureau</p><p>Comptes illimit&eacute;s</p><p>Acc&egrave;s depuis un nombre illimit&eacute; de terminaux</p></span></div></div></div>';


window.CloudFuze.defaultLanding ='<img src="../img/cfslide1-1-3.png">';

(function() {
    var LoginManager,
        __bind = function (fn, me) {
            return function () {
                return fn.apply(me, arguments);
            };
        };

    LoginManager = (function () {
        function LoginManager(container) {
            window.CloudFuze.action = "";
            window.CloudFuze.emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            this.container = container;
            this.hideAll = __bind(this.hideAll, this);
            this.showForgotForm = __bind(this.showForgotForm, this);
            this.showRegisterForm = __bind(this.showRegisterForm, this);
            this.showLoginForm = __bind(this.showLoginForm, this);
            this.showResendForm = __bind(this.showResendForm, this);
            this.showOrangeForm = __bind(this.showOrangeForm, this);
            this.loginForm = this.container.find("#login");
            this.registerForm = this.container.find("#register");
            this.forgotForm = this.container.find("#forgot");
            this.resendForm = this.container.find("#resendlink");
            this.confForm = this.container.find("#confmsg");
            this.entLogin = this.container.find("#entLogin");
            this.orangeForm = this.container.find('#orangeRegister');
            this.loginLink = this.container.find("#login-link-d");
            this.forgotLink = this.container.find("#forgot-link-d");
            this.registerLink = this.container.find("#register-link-d");
            this.resendLink = this.container.find("#resend-link-d");
            this.loginLinks = this.container.find("#login-link");
            this.forgotLinks = this.container.find("#forgot-link");
            this.registerLinks = this.container.find("#register-link");
            this.resendLink = this.container.find("#resend-link");
            this.loginLinks.bind("click", this.showLoginForm);
            this.forgotLinks.bind("click", this.showForgotForm);
            this.registerLinks.bind("click", this.showRegisterForm);
            this.resendLink.bind("click", this.showResendForm);
            this.loginSubmit = this.container.find("#login-submit");
            this.resendSubmit = this.container.find("#resend-submit");
            this.forgetSubmit = this.container.find("#forgot-submit");
            this.registerSubmit = this.container.find("#register-submit");
            this.loginSubmit.click(function (e) {
                var _this = $(this).closest(".login-wrapper");
                var statusMsg = _this.find(".statusMesg");
                statusMsg.html('');
                _this.find(".recendConf").html('');
                var email = _this.find("#email").val().trim().toLowerCase();
                var pwd = _this.find("input[type='password']").val();
                if (email.length == 0 && pwd == "") {
                    statusMsg.text('Please enter your email address and password.');
                    _this.find("#email").focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (email.length == 0) {
                    statusMsg.text("Please enter your email.");
                    _this.find("#email").focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (pwd == "") {
                    statusMsg.text("Please enter your password.");
                    _this.find("input[type='password']").focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (!window.CloudFuze.emailReg.test(email)) {
                    statusMsg.text("Please enter a valid email address.");
                    _this.find("#email").focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (pwd.length < 6) {
                    statusMsg.text("Password must be of at least 6 characters.");
                }
                else if (pwd.length > 20) {
                    statusMsg.text("Password must be less than 20 characters.");
                }
                else {
                    var checksum = "loginPage";
                    ForgotpasswordAjaxCall.searchUser(email, checksum, pwd);
                }
            });
            this.resendSubmit.click(function (e) {
                e.preventDefault();
                var _this = $(this).closest(".login-wrapper");
                var email = $(this).closest("form").find("#email").val().trim().toLowerCase();
                var resend = $(this).closest("form").find("#email");
                var statusMsg = _this.find(".statusMesg");
                if (email.length == 0) {
                    statusMsg.text("Please enter email.");
                    resend.focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (email.length > 50) {
                    statusMsg.text("The email address that you entered does not have a CloudFuze account.");
                    resend.focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (!window.CloudFuze.emailReg.test(email)) {
                    statusMsg.text("Please enter a valid email address.");
                    resend.focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else {
                    var checksum = "resendConfirmLink";
                    ForgotpasswordAjaxCall.searchUser(email, checksum, email);
                }
            });
            this.forgetSubmit.click(function (e) {
                e.preventDefault();
                var _this = $(this).closest(".login-wrapper");
                var email = $(this).closest("form").find("#email").val().trim().toLowerCase();
                var statusMsg = _this.find(".statusMesg");
                var forgotEmail = $("#forgot").find("#email");
                if (email.length == 0) {
                    statusMsg.text("Please enter your email.");
                    forgotEmail.focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (email.length > 50) {
                    statusMsg.text("The email address that you entered does not have a CloudFuze account.");
                    forgotEmail.focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (!window.CloudFuze.emailReg.test(email)) {
                    statusMsg.text("Please enter a valid email address.");
                    forgotEmail.focus();
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else {
                    var checksum = "sendPwdLink";
                    ForgotpasswordAjaxCall.searchUser(email, checksum, email);
                }
            });
            this.registerSubmit.click(function (e) {
                var _this = $(this).closest(".login-wrapper");
                var statusMsg = _this.find('.statusMesg');
                var object = {};
                object.instance = null;
                object.email = $(this).closest("form").find("#email").val().toLowerCase();
                object.name = $(this).closest("form").find("#name").val();
                object.pwd = $(this).closest("form").find("#pwd").val();
                object.cpwd = $(this).closest("form").find("#cpwd").val();
                var check = _this.find('#terms_conditons').is(':checked');
                var _a = [];
                if (object.name.length < 1) {
                    _a.push('name');
                }
                if (object.email.length < 1) {
                    _a.push('email');
                }
                if (object.pwd == "") {
                    _a.push('password')
                }
                if (object.cpwd == "") {
                    _a.push('confirm password');
                }
                if (_a.length > 1) {
                    statusMsg.text('Please enter all required fields.');
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (_a.length == 1) {
                    _this.find('#' + _a[0] + '').focus();
                    statusMsg.text('Please enter ' + _a[0] + '.');
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else if (check == false) {
                    statusMsg.text('Please agree terms and conditions.');
                    if(cfValidations)
                    cfValidations.addAnimation(_this);
                }
                else {
                    statusMsg.text('');
                    var _name = cfValidations.name(object.name);
                    if (_name == true) {
                        var _email = cfValidations.email(object.email);
                        if (_email == true) {
                            var _s = cfValidations.searchUser(object.email);
                            if (_s == true) {
                                var _pwd = cfValidations.password(object.pwd, object.cpwd);//CryptoJS.MD5(data.pwd).toString()
                                if (_pwd == true) {
                                    object.enable = false;
                                    if (window.CloudFuze.action == 'orange') {
                                        object.enable = true;
                                    }
                                    var signup = loginAjaxCall.signup(object);
                                    if (window.CloudFuze.action == 'orange' && signup != "failed") {
                                        localStorage.setItem("UserAuthDetails", loginAjaxCall.BasicAuth(object.email, CryptoJS.MD5(object.pwd).toString()));
                                        localStorage.setItem('UserId', signup.id);
                                        localStorage.setItem('UserName', signup.lastName);
                                        localStorage.setItem('CFUser', JSON.stringify(signup));
                                        window.location.href = 'cloudmanager.html#orange'
                                    }
                                    else if (signup != "failed") {
                                        // var _height=$( '#login-manager' ).height() - 100;
                                        // $("#login-manager").html("");
                                        // $("#login-manager").height (_height);
                                        // $("#login-manager").append("<h3 style='text-align: center'>You have successfully registered.</h3>");
                                        // $("#login-manager").append("<h2 style='text-align: center'>Thank You.</h2>");
                                        // var opts = {
                                        //     lines: 11 // The number of lines to draw
                                        //     , length: 31 // The length of each line
                                        //     , width: 6 // The line thickness
                                        //     , radius: 33 // The radius of the inner circle
                                        //     , scale: 1 // Scales overall size of the spinner
                                        //     , corners: 1 // Corner roundness (0..1)
                                        //     , color: '#2bb5d8' // #rgb or #rrggbb or array of colors
                                        //     , opacity: 0.25 // Opacity of the lines
                                        //     , rotate: 0 // The rotation offset
                                        //     , direction: 1 // 1: clockwise, -1: counterclockwise
                                        //     , speed: 1 // Rounds per second
                                        //     , trail: 60 // Afterglow percentage
                                        //     , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                                        //     , zIndex: 2e9 // The z-index (defaults to 2000000000)
                                        //     , className: 'spinner' // The CSS class to assign to the spinner
                                        //     , top: '70%' // Top position relative to parent
                                        //     , left: '50%' // Left position relative to parent
                                        //     , shadow: false // Whether to render a shadow
                                        //     , hwaccel: false // Whether to use hardware acceleration
                                        //     , position: 'absolute' // Element positioning
                                        // }
                                        // var target = document.getElementById('login-manager')
                                        // var spinner = new Spinner(opts).spin(target);
                                        //loginAjaxCall.ajaxcall(object.email, CryptoJS.MD5(object.pwd), signup.createdDate, signup.expiresIn);
                                                                               //showConfForm("signup");
                                        localStorage.setItem("UserAuthDetails", loginAjaxCall.BasicAuth(object.email, CryptoJS.MD5(object.pwd).toString()));
                                        localStorage.setItem('UserId', signup.id);
                                        localStorage.setItem('UserName', signup.lastName);
                                        localStorage.setItem('CFUser', JSON.stringify(signup));
                                        window.location.href = "/pages/fileManager.html?ver=0502";
                                        sendPageView(window.location.pathname + "?signup=" + object.email);
                                        //window.history.pushState("CloudFuze", "Signup Complete", window.location.pathname + "?signup=" + object.email);
                                        ////_gaq.push(['_trackPageview', window.location.href]);
                                    }
                                }
                                else {
                                    $("#register-submit").text('Register')
                                    $("#register-submit").removeAttr('disabled');
                                    statusMsg.text(_pwd);
                                    _this.find('#pwd').focus();
                                    if(cfValidations)
                                    cfValidations.addAnimation(_this);
                                }
                            }
                            else {
                                statusMsg.text(_s);
                                _this.find('#email').focus();
                                if(cfValidations)
                                cfValidations.addAnimation(_this);
                            }
                        }
                        else {
                            statusMsg.text(_email);
                            _this.find('#email').focus();
                            if(cfValidations)
                            cfValidations.addAnimation(_this);
                        }
                    }
                    else {
                        statusMsg.text(_name);
                        _this.find('#name').focus();
                        if(cfValidations)
                        cfValidations.addAnimation(_this);
                    }
                }
            });
            window.CloudFuze.action = window.location.search.substring(1).split('=')[0];
            if (window.CloudFuze.action === 'signup') {
                this.showRegisterForm();
            }
            else if (window.CloudFuze.action === 'forgotpassword') {
                this.showForgotForm();
            }
            else if (window.CloudFuze.action == 'resendlink') {
                this.showResendForm();
            }
            else if (window.CloudFuze.action == 'campaign') {
                this.showOrangeForm();
            }
            else if (window.CloudFuze.action == 'orange') {
                this.showRegisterForm();
            }
            else {
                this.showLoginForm();
            }
        }
        LoginManager.prototype.showLoginForm = function () {
            this.hideAll();
            this.loginForm.show();
            this.registerLink.show();
            $("#login #email").focus();
            $('body').find('.span8').html(window.CloudFuze.defaultLanding);
            $('body').removeAttr('style').css('background','url("../img/cfbg.jpg") no-repeat');
            return this.forgotLink.show();
        };
        LoginManager.prototype.showResendForm = function () {
            this.hideAll();
            this.resendForm.show();
            this.registerLink.show();
            $("#resendlink #email").focus();
            $('body').find('.span8').html(window.CloudFuze.defaultLanding);
            $('body').removeAttr('style').css('background','url("../img/cfbg.jpg") no-repeat');
            return this.loginLink.show();
        };
        LoginManager.prototype.showRegisterForm = function () {
            this.hideAll();
            this.registerForm.show();
            this.loginLink.show();
            $('#name').focus();
            if(window.CloudFuze.action == 'orange'){
                $('body').find('.span8').html(window.CloudFuze.orangeLanding);
                $('body').removeAttr('style').css('background','#DEEFFE');
            }else{
                $('body').find('.span8').html(window.CloudFuze.defaultLanding);
                $('body').removeAttr('style').css('background','url("../img/cfbg.jpg") no-repeat');
            }
            return this.forgotLink.show();
        };
        LoginManager.prototype.showForgotForm = function () {
            this.hideAll();
            this.forgotForm.show();
            $('#forgot #email').focus();
            this.loginLink.show();
            $('body').find('.span8').html(window.CloudFuze.defaultLanding);
            $('body').removeAttr('style').css('background','url("../img/cfbg.jpg") no-repeat');
            return this.registerLink.show();
        };
        LoginManager.prototype.showOrangeForm = function () {
            this.hideAll();
            this.orangeForm.show();
            this.loginLink.show();
            $('[name="name"]').focus();
            $('body').find('.span8').html(window.CloudFuze.orangeLanding);
            $('body').removeAttr('style').css('background','#DEEFFE');
            return this.forgotLink.show();
        };
        LoginManager.prototype.hideAll = function () {
            this.loginForm.hide();
            this.confForm.hide();
            this.entLogin.hide();
            this.resendForm.hide();
            this.registerForm.hide();
            this.forgotForm.hide();
            this.orangeForm.hide();
            $('#login-manager .statusMesg').html('');
            $('#login-manager i#resend-link').html('');
            $('#login-manager input').val('');
            this.loginLink.hide();
            this.forgotLink.hide();
            $('#register-link-d,#forgot-link-d,#login-link-d').css('border', 'none');
            $('#login-link-d').css('border-right', '0px solid #000');
            return this.registerLink.hide();
        };
        LoginManager.prototype.getParameterByName = function (name) {
            var regex, regexS, results;
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            regexS = "[\\?&]" + name + "=([^&#]*)";
            regex = new RegExp(regexS);
            results = regex.exec(window.location.search);
            if (results === null) {
                return "";
            } else {
                return decodeURIComponent(results[1].replace(/\+/g, " "));
            }
        };
        return LoginManager;
    })();

    $(function () {
        return new LoginManager($("#login-manager"));
    });

}).call(this);

function showConfForm(checksum) {
    if(checksum == "resend"){
		$('#index-msg').html('<p>The confirmation link successfully sent to your registered email.</p>');
	}
    else if(checksum == "sendPwdLink"){
		$('#index-msg').html('<p>Please check your email for reset password link.</p>');
	}
    else if(checksum == "signup"){
		$('#index-msg').html('<p>Thank You for Choosing CloudFuze!</p><p>You are just one step away from logging into your CloudFuze Account.We have sent you an e-mail, please confirm your email address.</p>');
	}
	$('#login').hide();
	$('#register').hide();
	$('#forgot').hide();
	$('#resendlink').hide();
	$('#forgot-link-d').hide();
	$('#register-link-d').show();
	$('#login-link-d').show();
	$('#confmsg').show();
}
$('#login').keypress(function(e){
    if(e.which == 13){
        $('#login-submit').trigger('click');
    }
});
$('#register').keypress(function(e){
    if(e.which == 13){
        $('#register-submit').trigger('click');
    }
});
$('#forgot').keypress(function(e){
    if(e.which == 13){
        $('#forgot-submit').trigger('click');
    }
});
$('#resendlink').keypress(function(e){
    if(e.which == 13){
		$('#resend-submit').trigger('click');
    }
});
$('#entLogin').keypress(function(e){
    if(e.which == 13){
        $('#ent-submit').trigger('click');
        return false;
    }
});
$('#login-link').on('click',function(){
    window.history.pushState("CloudFuze", "Show login link", window.location.pathname+"?login=true");
    sendPageView(window.location.href);
	//_gaq.push(['_trackPageview', window.location.href]);
});
$('#forgot-link').on('click',function(){
    window.history.pushState("CloudFuze", "Show forgot password link", window.location.pathname+"?forgotpassword=true");
	sendPageView(window.location.href);
    //_gaq.push(['_trackPageview', window.location.href]);
});
$('#register-link').on('click',function(){
    window.history.pushState("CloudFuze", "Show signup link", window.location.pathname+"?signup=true");
	sendPageView(window.location.href);
    //_gaq.push(['_trackPageview', window.location.href]);
});
$('#loginWrapperFooter').on('click','a',function(){
    $('#login').removeClass('loginHide');
});
$('#ent-submit').on('click',function(e){
    e.preventDefault();
    var subdomain = $(this).parent().find('#domain').val().toLowerCase();
    if(subdomain.trim().length > 1){
        var api = null;
        var isDev = /devweb/i.test(domainUrl);
        var isSl = /slweb/i.test(domainUrl);
        if(isDev){
            api = subdomain+".devservices.api.cloudfuze.com";
        }else if(isSl){
            api = subdomain+".slservices.api.cloudfuze.com";
        }else{
            api = subdomain+".apis.cloudfuze.com";
        }
        $.ajax({
            type: "GET",
            url: apicallurl + "/subdomain/find?subdomain="+api,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success:function(data){

            },
            complete:function(xhr, statusText){
                if(xhr.status == 200){
                    //var url = "https://"+subdomain+"."+defaultDomain.split('//')[1];
                    window.location.href = "https://"+subdomain+"."+defaultDomain.split('//')[1];
                }else{
                    $('#entLogin').find('.statusMesg').text('Domain is not registered with us.');
                }
            }
        });
    }
    else{
        $('#entLogin').find('.statusMesg').text('Please enter domain name.');
        return false;
    }
});
$("#_orangeC").on("click",function() {
    var object = {};
    var _this = $(this).closest('.orange-signup');
    object.name = _this.find('[name="name"]').val().trim();
    object.email = _this.find('[name="email"]').val().trim().toLowerCase();
    object.pwd = _this.find('[name="password"]').val();
    object.cpwd = _this.find('[name="cpassword"]').val();
    var check = _this.find('[name="terms_conditons"]').is(':checked');
    var statusMsg = _this.find('#errormsg');
    var _a = [];
    if(object.name.length < 1){
        _a.push('name');
    }
    if(object.email.length < 1){
        _a.push('email');
    }
    if(object.pwd == ""){
        _a.push('password')
    }
    if(object.cpwd == ""){
        _a.push('confirm password');
    }
    if(_a.length > 1){
        statusMsg.text('Please enter all required fields.')
    }
    else if(check == false){
        statusMsg.text('Please agree terms and conditions.')
    }
    else if(_a.length == 1){
        statusMsg.text('Please enter '+_a[0]+'.');
    }
    else{
        statusMsg.text('');
        var _name = cfValidations.name(object.name);
        if(_name == true){
            var _email = cfValidations.email(object.email);
            if(_email == true){
                var _s = cfValidations.searchUser(object.email);
                if(_s == true){
                    var _pwd = cfValidations.password(object.pwd,object.cpwd);
                    if(_pwd == true){
                        window.CloudFuze.orangeDetails = object;
                        _this.hide().next().show();
                    }else{
                        statusMsg.text(_pwd);
                    }
                }else{
                    statusMsg.text(_s);
                }
            }else{
                statusMsg.text(_email);
            }
        }else{
            statusMsg.text(_name);
        }
    }
});

$(".orange-plans").find("a.button").on('click',function(){
    var _v = window.CloudFuze.orangeDetails;
    _v.plan = $(this).attr('data');
    _v.type = $(this).attr('mode');
    _v.instance = "ORANGE";
    var _a = $('.orange-user-details');
    _a.find('td.name').text(_v.name);
    _a.find('td.email').text(_v.email);
    _a.find('span.plan').text(_v.plan);
    _a.find('span.type').text(_v.type);
    $(this).closest('.orange-plans').hide().next().show();
});

$('.orange-user-details').find('.change-plan').on('click',function(){
    $(this).closest('.orange-user-details').hide().prev().show();
});
$('.orange-user-details').find('button').on('click',function(){
    var _a = {
        "code":"EUR",
        "amount":window.CloudFuze.orangeDetails.plan,
        "success":"orangesuccess",
        "cancel":"orangecancel"
    };
    window.CloudFuze.orangeDetails.enable = true;
    var _b = loginAjaxCall.signup(window.CloudFuze.orangeDetails);
    if(_b != "failed"){
        $(this).text('Please wait...');
        localStorage.setItem('UserId',_b.id);
        localStorage.setItem('UserName',_b.lastName);
        localStorage.setItem('CFUser',JSON.stringify(_b));
        localStorage.setItem("UserAuthDetails",loginAjaxCall.BasicAuth(_b.primaryEmail,CryptoJS.MD5(window.CloudFuze.orangeDetails.pwd)));
        if(window.CloudFuze.orangeDetails.type == "monthly"){
            localStorage.setItem('MonthlyAMNT', MonthlyAMT);
            CFPaymentsPaypal.createPmntMnthlyRecurring(_a);
        }
        else{
            localStorage.setItem('YearlyAMNT', YearlyAMT);
            CFPaymentsPaypal.createPmntAnnualRecurring(_a);
        }
    }

});

$('#_cfheader').on('click',function(){
    window.open('','_new').location.href='https://www.cloudfuze.com';
});

$('#_cfheader').hover(function() {
    $(this).css('cursor','pointer');
});


var BotConfig = {
    /*
    //Dev
    SPARK:{
        CLIENT_ID:'C0ade08cb85aa46c774a9ce9fbfc8d6147a10bc4731bc533927d719de1190deef',
        SECRET:'4d15d43dfa2ac41cc6b41f285a7aea75ca8136c82b44a71ad84d6cdb126f9d8b',
        REDIRECT: window.location.origin + '/fuzebot/index.html' //'https://devwebapp.cloudfuze.com/fuzebot/index.html'
    },
    SLACK:{
        CLIENT_ID:'166115099443.165506726673',//'42746786352.130175552323',
        SECRET:'b976adf6d2d7e5b214425bc7014e1809',//'4f5141289a20d1be85946a9d06fcec09',
        REDIRECT: window.location.origin + '/fuzebot/index.html'
    }*/

    /*
    //SL
    SPARK:{
        CLIENT_ID:'Ccbc3e9cd0a7adeabc1e70aa06cc8ce487903ca11140170787f2ace79b8b28862',
        SECRET:'6e47df4131a254b8f8fbdbb89348e09231412fd8137ebb63d95894c33f9da32a',
        REDIRECT: window.location.origin + '/fuzebot/index.html'
    },

     */

     //Prod
     SPARK:{
         CLIENT_ID:'Ce4cddfbf1438100064e92cf925ae2b48409f28c0b3e760c7a0db4d20c42d4983',
         SECRET:'40e40cf929b8c046df3ec157e948aa0c9bc721e5cd7280f340980ab607c4d61b',
         REDIRECT: window.location.origin + '/fuzebot/index.html'
     },
};


/*Login With Spark Functionality*/

var botLogin = function () {};

botLogin.prototype.redirect = function (bot) {
    switch (bot){
        case 'SPARK' : window.location.href = "https://api.ciscospark.com/v1/authorize?" +
            "client_id="+BotConfig[bot].CLIENT_ID+
            "&redirect_uri="+encodeURIComponent(BotConfig[bot].REDIRECT)+
            "&response_type=code" +
            "&scope=spark%3Apeople_read+spark%3Amessages_write+spark%3Arooms_read+spark%3Amemberships_read+spark%3Amessages_read+spark%3Arooms_write+spark%3Amemberships_write"+
            "&state=fuzebotsparklogin";
            break;

        case 'SLACK': window.location.href = "https://slack.com/oauth/authorize?" +
            "scope=bot,users.profile:read,channels:read,files:write:user" + //"scope=bot,users:read,users.profile:read,team:read"
            "&client_id=" + BotConfig[bot].CLIENT_ID +
            "&state=fuzebotslacklogin"+
            "&redirect_uri="+BotConfig[bot].REDIRECT;
            break;

        default:alert('Not yet implemented');
    }
};

botLogin.prototype.doBotLogin = function(code){
    var token = Base64.decode(code).split(':');
    var _auth = botLogin.prototype.basicAuth(token[0], token[2]);
    $.ajax({
        type: "POST",
        url: apicallurl + "/auth/user",
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": _auth,
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            localStorage.setItem("UserAuthDetails", _auth);
            localStorage.setItem("UserId", data.id);
            localStorage.setItem("UserName", data.lastName);
            localStorage.setItem('CFUser', JSON.stringify(data));
        },

        complete: function (xhr, statusText) {
            if (xhr.status > 300 && xhr.status != 401) {
                localStorage.clear();
                showNotyNotification("error","Operation Failed");
            }else if(xhr.status == 200){
                window.location.href = 'pages/cloudmanager.html'
            }
        },
        error: function (error) {
        }
    });
};

botLogin.prototype.basicAuth = function (a,b) {
    var c = a + ':' + b;
    c = Base64.encode(c);
    return "Basic " + c;
};

botLogin.prototype.doSparkLogin = function (authcode) {
    function getToken_Spark(code){
        var d = "grant_type=authorization_code&" +
            "code="+code+
            "&client_id="+BotConfig['SPARK'].CLIENT_ID+
            "&client_secret="+BotConfig['SPARK'].SECRET+"" +
            "&redirect_uri="+encodeURIComponent(BotConfig['SPARK'].REDIRECT);
        $.ajax({
            url: spark_token,
            type: "POST",
            data: d,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function (token) {
                getProfile_spark(code,token);
            },
            error: function (jqXHR, exception, errorstr) {
                window.location.href = 'pages';
            }
        });
    }

    function getProfile_spark(code,token){
        $.ajax({
            url: spark_profile,
            type: "GET",
            headers:{
                "Content-type":"application/json",
                "Authorization":"Bearer "+token.access_token
            },
            success: function (profile) {
                //apiPutData(userId, data.access_token, data.refresh_token, profile.emails[0], "CISCO_SPARK", profile.displayName, code, url);
                insertSparkUser(code,token,profile);
            },
            error: function (jqXHR, exception, errorstr) {
                window.location.href = 'pages';
            }
        });
    }

    function insertSparkUser(code,result,profile) {
        debugger;
        var sparkUser = {};
        sparkUser.firstName = '';
        sparkUser.lastName = profile.displayName;
        sparkUser.email = profile.emails[0];
        sparkUser.accessToken=result.access_token;
        sparkUser.refreshToken=result.refresh_token;
        sparkUser.botType="SPARK";
        $.ajax({
            url:apicallurl+'/users/addBotUser',
            type:'PUT',
            async: false,
            headers: {
                "Content-Type": "application/json",
                "accept":"text/plain"
            },
            data: JSON.stringify(sparkUser),
            success: function (result) {
                 localStorage.setItem('userBotLogin',"spark");
                    botLogin.prototype.doBotLogin(result);
            },
            error: function (error) {
                console.log(error);
            }
        });
    }

    getToken_Spark(authcode);
};

botLogin.prototype.doSlackLogin = function (authcode) {

    function insertSlackUser(code,token,profile) {
        debugger;
        var slackUser = {};

        slackUser.teamId = token.team_id;
        slackUser.teamName = token.team_name;
        slackUser.userId = token.user_id;
        slackUser.botUserId = token.bot.bot_user_id;
        slackUser.botAccessToken = token.bot.bot_access_token;
        slackUser.email = profile.profile.email;
        slackUser.botType = "SLACK";
        slackUser.accessToken=token.access_token;
        slackUser.refreshToken='';
        slackUser.firstName = profile.profile.first_name || '';
        slackUser.lastName = profile.profile.last_name || profile.profile.email.split('@')[0];
       
        localStorage.setItem('slackTeam',slackUser.teamName.toLowerCase().replace(" ",''));
        //alert('SLACK login Service is in pending');
        $.ajax({
            url:apicallurl+'/users/addBotUser',
            type:'PUT',
            async: false,
            headers: {
                "Content-Type": "application/json",
                "accept":"text/plain"
            },
            data: JSON.stringify(slackUser),
            success: function (result) {
                localStorage.setItem('userBotLogin',"slack");
                 botLogin.prototype.doBotLogin(result);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }

    function getUser_Slack(code, token) {
        var _url = 'https://slack.com/api/users.profile.get?token='+token.access_token+'&scope=email';
        $.ajax({
            url:_url,
            type:'get',
            success:function (user) {
                insertSlackUser(code,token,user);
            },
            error:function (err) {
                window.location.href = 'pages';
            }
        });
    }

    function getToken_Slack(code) {
        var _url = 'https://slack.com/api/oauth.access?client_id='+BotConfig['SLACK'].CLIENT_ID+'&client_secret='+BotConfig['SLACK'].SECRET+
            '&code='+code+'&redirect_uri='+BotConfig['SLACK'].REDIRECT;
        $.ajax({
            url:_url,
            type:'get',
            success:function (token) {
                getUser_Slack(code,token)
            },
            error:function (err) {
                window.location.href = 'pages';
            }
        });
    }

    getToken_Slack(authcode);
};

botLogin.prototype.bindEvents = function () {
    var loginContainer = $('#login-manager');
    loginContainer.on('click','#login-with-spark',function () {
        botLogin.prototype.redirect('SPARK');
        sendGAEvents("BOT_LOGIN","SPARK");
    });
    loginContainer.on('click','#login-with-slack',function () {
        botLogin.prototype.redirect('SLACK');
        sendGAEvents("BOT_LOGIN","SLACK");
    });

    loginContainer.on('click','#signUp',function () {
        window.location.href = '/pages/index.html?signup=true';
        sendGAEvents("BOT_LOGIN","SIGNUP");
    });

    loginContainer.on('click','#doLogin',function () {
        window.location.href = '/pages/index.html?login=true';
        sendGAEvents("BOT_LOGIN","LOGIN");
    });
};

var loginWithBot = new botLogin();
loginWithBot.bindEvents();
