var notifyTime = 4000;
var notifyError = 2000;

var _cccount = 1;
var ONE_KB = 1024;
var ONE_MB = ONE_KB * ONE_KB;
var ONE_GB = ONE_KB * ONE_MB;

var CFManageCloudAccountsAjaxCall = {

    //CloudFuze Users
    getAuthDetails: function () {
        var dt = new Date();
        var time = dt.getTime();
        if (localStorage.getItem("time") != null) {
            // session expires on 2 hours mentioned in milliseconds.
            if (time - localStorage.getItem("time") >= 7200000) {
                // $.smallBox({
                //     title: "Your session has expired.  Please log in again.",
                //     color: "#e35e00",
                //     timeout: notifyTime
                // });
                showNotyNotification("error","Your session has expired.  Please log in again.");
                setTimeout(function () {
                    $('#cf-logout').trigger("click");
                    window.location.href = "index.html";
                }, 2000);
            }
        }
        localStorage.setItem("time", time);
        $.ajaxSetup({
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    var a = window.location.pathname;
                    var b = /cloudmanager/i.test(a);
                    if (b) {
                        return false;
                    }
                    else if (PageName == "InnerFolders" && previousPage == "Share with Me") {
                        return false
                    } else {
                        var notification = {};
                        if (localStorage.getItem('UserAuthDetails') == undefined
                                || localStorage.getItem('UserAuthDetails') == null
                                || localStorage.getItem('UserAuthDetails') == 'null') {
                            notification.title = "You have been logged out";
                        } else {
                            notification.title = "Your password was changed.Please relogin";
                        }
                        notification.color = "#a4c400";
                        notification.timeout = notifyTime;
                        //$.smallBox(notification);
                        showNotyNotification("notify",notification.title);
                        setTimeout(function () {
                            $('#cf-logout').trigger("click");
                            window.location.href = "index.html";
                        }, 3000);
                    }
                } else if (jqXHR.status === 503) {
                    //$.smallBox({title: "Server is under maintanance.", color: "#a4c400", timeout: notifyTime});
                    showNotyNotification("notify","Server is under maintanance.");
                }
            }
        });
        return localStorage.getItem("UserAuthDetails");
    },
    getUserId: function () {
        return localStorage != null ? localStorage.getItem("UserId") : '';
    },
    getUserEmail: function () {
        return JSON.parse(localStorage.getItem('CFUser')).primaryEmail;
    },
    isPaidUser: function () {
        var apiUrl = apicallurl + "/subscription/status/" + CFManageCloudAccountsAjaxCall.getUserId();
        var isPaidUser = false;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (isPaid) {
                isPaidUser = isPaid;
            }
        });
        return isPaidUser;
    },

    //CloudFuze Clouds
    getAllClouds: function () {
       // var apiUrl = apicallurl + "/users/" + CFManageCloudAccountsAjaxCall.getUserId() + "/cloud/get/all";
        var apiUrl = apicallurl + "/users/" + CFManageCloudAccountsAjaxCall.getUserId() + "/get/all/cloud";
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (AllClouds) {
                if (AllClouds.length == 0 && count == 1) {
                    sessionStorage.setItem('clouds', false);
                } else {
                    sessionStorage.setItem('clouds', true);
                }
                AllCloudsInfo = AllClouds;
                CloudName = [];
                CloudId = [];
                CLParent = [];
                PrimaryEmail = [];
                CLDisplayName = [];
                $.each(AllClouds, function (i, cloudObj) {
                    CloudName.push(cloudObj.cloudName);
                    CloudId.push(cloudObj.id);
                    CLParent.push(cloudObj.rootFolderId);
                    var dispname = cloudObj.userDisplayName;
                    var clemail = cloudObj.cloudUserId;
                    if (clemail != null) {
                        clemail = clemail.split('|');
                        PrimaryEmail.push(clemail[1]);
                    }

                    if (dispname == null || dispname.length == 0) {
                        CLDisplayName.push(clemail[1]);
                    } else {
                        CLDisplayName.push(dispname);
                    }

                    if (cloudObj.loadLock && /fileManager.html/.test(window.location.href)) {
                        CFManageCloudAccountsAjaxCall.CloudSyncStatus(cloudObj.id);
                    }
                    // else{
                    //    CFManageCloudAccountsAjaxCall.refreshcloud(cloudObj.id); 
                    // }
                });
                return true;
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getAllCloudTitles: function () {
        var uniqueCloudNames = CloudName.filter(function (itm, i, CloudName) {
            return i == CloudName.indexOf(itm);
        });
        $.each(uniqueCloudNames, function (i, CName) {
            var CFCName;
            $.each(CLName, function (key, value) {
                if (key == CName) {
                    CFCName = value;
                }
            });
            $('#CloudDriveList').append('<li style="padding-left:0;border-bottom: 1px solid #024A5B;"><i class="Driveicon" id="' + CName + '"></i><a href="#" class="cltrigger">' + CFCName + '</a><ul class="clsubmenu" style="display: none;" id="' + CName + '"></ul></li>');
            CFManageCloudAccountsAjaxCall.getAllCloudChilds(CloudName, CName, CFCName, CloudId, CLParent);
        });
    },
    getAllCloudChilds: function (CloudName, CName, CFCName, CloudId, CLParent) {
        for (var i = 0; i < CloudName.length; i++) {
            if (CName == CloudName[i]) {
                var _a = AllCloudsInfo[i].cloudStatus == "INACTIVE" ? "cf-warning ErrorRefresh" : "cf-refresh4";
                $("#CloudDriveList").find(".clsubmenu[id='" + CName + "']").append('<li style="padding-left:10px;height:23px;"' +
                        ' id="' + CloudId[i] + '" class="getCloudFiles" pid="' + CLParent[i] + '">' +
                        '<p class="cloudDispName">' + CLDisplayName[i] + '</p>' +
                        '<i class="' + _a + '" style="font-size:15px" id="' + CloudId[i] + '"></i></li>');
            }
        }
    },
    getAllCloudTitlesSettings: function (CloudName, CloudId) {
        $('#manageclouddiv').find('tbody').html('');
        var uniqueCloudNames = CloudName.filter(function (itm, i, CloudName) {
            return i == CloudName.indexOf(itm);
        });
        $.each(uniqueCloudNames, function (i, CName) {
            CFManageCloudAccountsAjaxCall.getAllCloudChildsSettings(CloudName, CName, CLName[CName], CloudId, CLParent);
        });  

    
          CFManageCloudAccountsAjaxCall.loadDataToProgressBar();
    },
    getAllCloudChildsSettings: function (CloudName, CName, CFCName, CloudId, CLParent) {
        var count = 1;
        var errorCount = 0;
        for (var i = 0; i < CloudName.length; i++) {
            if (CName == CloudName[i]) {
                var dispname;
                if (CloudName[i] == "" || CloudName[i] == " ") {
                    dispname = PrimaryEmail[i];
                } else {
                    dispname = CLDisplayName[i];
                }
                var errorClass = AllCloudsInfo[i].cloudStatus == "ACTIVE" ? "" : "cf-warning3";
                if (errorClass == "cf-warning3") {
                    errorCount++;
                }
                var titleText = "This cloud exceeds the upper limit of CloudFuze Public Cloud. Please contact the support@cloudfuze.com.";
                if (errorClass == "cf-warning3") {
                    if (AllCloudsInfo[i].exceptionMessage == undefined || AllCloudsInfo[i].exceptionMessage == null || AllCloudsInfo[i].exceptionMessage == "") {
                    }
                    else if (/Exception/i.test(AllCloudsInfo[i].exceptionMessage)) {
                    }
                    else {
                        titleText = AllCloudsInfo[i].exceptionMessage;
                    }
                }

                //rename
                //<i class="cf-pencil9 pull-right" data-action="edit"></i>
                var _a = '<tr data-cloud="' + CloudId[i] + '">' +
                        '<td style="padding-left: 15px;"><i class="Driveicon" id="' + CloudName[i] + '" ></i></td>' +
                        '<td style="padding-left: 10px;"><span class="CloudDriveList">' + CFCName + '</span></td>' +
                        '<td style="padding-left: 15px;"><span style="padding-right: 30px;">' + CFManageCloudAccountsAjaxCall.getMaxChars(dispname, 30) + '</span><i class="cf-pencil9 pull-right " style="padding-right: 10px;" data-action="edit"></i></td>';
                var _c = CloudName[i] == "ORANGE" ? CFManageCloudAccountsAjaxCall.getMaxChars(dispname, 30)  : PrimaryEmail[i];
                var  _totsize= CFManageCloudAccountsAjaxCall.getObjectSize(AllCloudsInfo[i].totalSpace);
                var  _availableSize=CFManageCloudAccountsAjaxCall.getObjectSize(AllCloudsInfo[i].availableSpace);
                if(CloudName[i] == "AMAZON_STORAGE" && parseInt(CFManageCloudAccountsAjaxCall.getObjectSize(AllCloudsInfo[i].totalSpace)) >5)
                {
                    _totsize="Unlimited";
                    _availableSize="Unlimited";
                }
                _a += '<td>' + _c + '</td>' +
                        '<td style="padding-left: 25px;">'+_totsize+'</td>' +
                        '<td style="padding-left: 20px;">'+CFManageCloudAccountsAjaxCall.getObjectSize(AllCloudsInfo[i].usedSpace)+'</td>' +
                        '<td style="padding-left: 30px;">'+_availableSize+'</td>' +
                        '<td style="padding-left: 15px;"><i class="cf-times" data-action="delete" data-id="' + CloudId[i] + '"></i>' +
                        '<i style="margin-left: 75px;color:rgb(236, 40, 40);font-size:16px;display:block;margin-top:6px;" class="' + errorClass + '" title="' + titleText + '"></i></td></tr>';

                $('#manageclouddiv').find('tbody').append(_a);
            }
        }
        if (errorCount > 0) {
            $('#cm-managecloud > a').find('.cf-warning3').remove();
            $('#cm-managecloud > a').append('<i class="cf-warning3" style="color:rgb(236, 40, 40);font-size:11pt"></i>')
        }
    },
    loadDataToProgressBar : function(){

    var tspaceby=0,uspaceby=0,aspaceby=0,allCTs=0,
                allCUs=0,allCAs=0;


        if(AllCloudsInfo.length < 1){
               var _empty='<tr id="noclouds"> '+' <td colspan="100%"><b><div style="margin-left:30px;text-align: center;font-size: 20px;">No Clouds To Display</div></b></td>'+'</tr>';
               $('#manageclouddiv').find('tbody').append(_empty);
        }

        if(AllCloudsInfo != undefined) {

        //if clouds length is 0 need to make progress bar progress 0 while deleting it shows 25 %
        if(AllCloudsInfo.length > 0)
        {
             for (var i = 0; i < AllCloudsInfo.length; i++) 
             {
                var cname = AllCloudsInfo[i].userDisplayName;
                 if(!(CloudName[i] == "AMAZON_STORAGE" && parseInt(CFManageCloudAccountsAjaxCall.getObjectSize(AllCloudsInfo[i].totalSpace)) < 5368709120)) {
                     allCTs += AllCloudsInfo[i].totalSpace;
                     allCUs += AllCloudsInfo[i].usedSpace;
                 }
                 tspaceby += AllCloudsInfo[i].totalSpace;
                uspaceby += AllCloudsInfo[i].usedSpace;
                
                aspaceby += AllCloudsInfo[i].availableSpace;

                allCAs += AllCloudsInfo[i].availableSpace;
               
            }
        var allts=CFManageCloudAccountsAjaxCall.getReportObjectSize(allCTs);
        var allus=CFManageCloudAccountsAjaxCall.getReportObjectSize(allCUs);

        var allas=allCTs-allCUs;
        allas=CFManageCloudAccountsAjaxCall.getReportObjectSize(allas);
        $('#total').html('');
        $('#used').html('');
        $('#free').html('');
        $('#ptotal').html('');
        $('#pused').html('');
         $('#totalclouds').html('');
        $('#total').append(allts);
        $('#used').append(allus);
        $('#free').append(allas);
        $('#ptotal').append(allts);;
        $('#pused').append(allus);
        if(AllCloudsInfo.length > 0)
        $('#totalclouds').append(AllCloudsInfo.length);
            else
        $('#totalclouds').append(0);
        var progrssPercent ;
        if ((uspaceby/tspaceby)*100 < 1){

            progrssPercent = "1%";

        }else {
            progrssPercent = (uspaceby/tspaceby)*100+'%';

        }
        $('#usedProgress').css("width",progrssPercent);
        }else{
            progrssPercent = 0+'%';
             $('#usedProgress').css("width",progrssPercent);
        $('#total').html('');
        $('#used').html('');
        $('#free').html('');
        $('#totalclouds').html('');
        $('#total').append(0);
        $('#used').append(0);
        $('#free').append(0);
        $('#totalclouds').append(0);
        }
           
        }else{
             $('#totalclouds').append(0);
        }
       
   },
    renameCloud: function (a) {
        var _u = apicallurl + "/users/update/cloud/displayname/" + a.id + '?displayName=' + a.name;
        var _b = false;
        $.ajax({
            type: 'POST',
            url: _u,
            async: false,
            headers: {
                "Content-Type": "multipart/form-data",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (s) {
                return _b = true;
            },
            complete: function (xhr) {
                if (xhr.status > 300) {
                    return _b = false;
                }
            }
        });
        return _b;
    },
    CloudSyncStatus: function (cloudid) {
        var apiUrl = apicallurl + "/users/" + CFManageCloudAccountsAjaxCall.getUserId() + "/loadstatus?cloudId=" + cloudid;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (Syncstatus) {
                if (Syncstatus == false) {
                    $('#CloudDriveList').find('[id="' + cloudid + '"].cloudSpinn').addClass('cf-refresh4').removeClass('cloudSpinn');
                    if (PageName == "CloudDrive") {
                        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(SingleCloudId, 1);
                    }
                    if (PageName == "InnerFolders" && !$('#CFSharedWithMe').parent().hasClass('active')) {
                        CFManageCloudAccountsAjaxCall.gotoInnerFolderandFiles(SingleCloudId, SinglePId, 1);
                    }
                    if (PageName == "move") {
                        $('.cloudSpinn').addClass('cf-refresh4').removeClass('cloudSpinn');
                        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(SingleCloudId, 1);
                    }
                }
                if (Syncstatus == true) {
                    $('#CloudDriveList').find('[id="' + cloudid + '"].cf-refresh4').addClass('cloudSpinn').removeClass('cf-refresh4');
                    if (PageName == "move") {
                        $('.cloudSpinn').addClass('cloudSpinn').removeClass('cf-refresh4');
                        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(SingleCloudId, 1);
                    }
                    setTimeout(function () {
                        CFManageCloudAccountsAjaxCall.CloudSyncStatus(cloudid);
                    }, 15000);
                }
            }
        });
    },
    refreshcloud: function (cloudId1) {
        if (cloudId1 != undefined && cloudId1 != null && cloudId1 != '') {
            cloudId = cloudId1;
        } else {
            cloudId = "all";
        }
        var data = "";
        var apiUrl = apicallurl + "/filefolder/refresh/cache/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/cloud/" + cloudId;
        $.ajax({
            type: "post",
            url: apiUrl,
            data: data,
            headers: {
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (data) {
                CFManageCloudAccountsAjaxCall.CloudSyncStatus(cloudId);
            },
            complete: function (xhr) {
                if (xhr.status > 300) {
                }
            }
        });
    },

    //CloudFuze Events
    getAllEvents: function (number, len) {
        var apiUrl = apicallurl + "/events/all?page_size=" + len + "&page_nbr=" + number;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (events) {
                var _notiFooterBtn = $(".notification_footer button");
                var _notiBody = $(".notification_body");
                $('#notify_count').show();
                if (events.length < 10) {
                    _notiFooterBtn.addClass('buttonDisable');
                }
                _notiFooterBtn.attr('page', number);
                _notiFooterBtn.attr('size', len);
                if (number == 1) {
                    _notiBody.html('');
                    if (events == null || events.length == 0) {
                        $('#notify_count').hide();
                        $('.notification_footer button,.notification_head #ignore,.notification_head #clear').addClass('buttonDisable');
                        _notiBody.append("<li style='font-weight: bold;text-align: center'>You don't have any notifications.</li>");
                    }
                }
                var event = {
                    "FILESHARE": "icon-sharedbyicon",
                    "WORKSPACE": "cf-workspace",
                    "WORKSPACE_FILE_ADDED": "cf-file-text",
                    "WORKSPACE_FILE_VIEWED": "cf-eye4",
                    "GROUPS": 'cf-group2'
                };
                var linktype = {
                    "FILESHARE": "pages/fileManager.html?shared=true&fileId=",
                    "WORKSPACE": "pages/workspace.html?wspaceshared=true&workspaceId=",
                    "WORKSPACE_FILE_ADDED": "pages/workspace.html?wspaceshared=true&workspaceId=",
                    "WORKSPACE_FILE_VIEWED": "pages/workspace.html?wspaceshared=true&workspaceId=",
                    "SHAREBYME": "pages/fileManager.html?sharebyme=true&fileId="
                };
                if (events != null || events.length != 0) {
                    $.each(events, function (i, e) {
                        var icon, description, link, color, status = '';
                        icon = event[e.eventType];
                        var _owner = JSON.parse(localStorage.getItem('CFUser')).primaryEmail;
                        var _link = linktype[e.eventType];
                        if (e.ownerEmailId != null && e.ownerEmailId == _owner) {
                            _link = linktype['SHAREBYME'];
                        }
                        link = domainUrl + _link + e.eventRef;
                        if (e.isRead == false) {
                            color = "green";
                            status = "notify_unred";
                        } else {
                            color = "gray";
                        }
                        var _html = '<li type="' + e.eventType + '" class="' + status + '">' +
                                '<i id="' + e.id + '" class="cf-cross32"></i>' +
                                '<span class="icon"><i class="' + icon + '"></i>' +
                                '<i class="cf-circle2 ' + color + '" style="font-size:7px"></i></span>' +
                                '<span class="content"><a data-target="' + link + '">' +
                                '<p style="margin:0">' + e.description + '</p></a></span>' +
                                '</li>';

                        _notiBody.append(_html);
                    });
                }
                return true;
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                }
            }
        });
    },
    getEventCount: function () {
        if (localStorage.getItem('UserAuthDetails') == undefined
                || localStorage.getItem('UserAuthDetails') == null
                || localStorage.getItem('UserAuthDetails') == 'null') {
            return;
        }
        var apiUrl = apicallurl + "/events/count";
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (eventCount) {
                var notiCnt = $("#notify_count");
                var _shareWithmeCount = $("#CFSharedWithMe").find("#count");
                if (eventCount == null) {
                    notiCnt.hide();
                    $('#CFSharedWithMe #count').hide();
                    $('.cf-workspace').next('.badge').hide();
                }
                if (eventCount != null) {
                    if (eventCount.all > 0) {
                        notiCnt.show();
                        notiCnt.text(eventCount.all);
                    } else {
                        $('#notify_count').text('').hide();
                    }
                    if (eventCount.fileShare > 0) {
                        _shareWithmeCount.show();
                        _shareWithmeCount.text(eventCount.fileShare);
                    } else {
                        _shareWithmeCount.hide();
                    }

                    var _monBadge = $(".cf-workspace").next(".badge");
                    if (eventCount.workspace > 0 || eventCount.workspaceFileAdded > 0) {
                        var count = parseInt(eventCount.workspace) + parseInt(eventCount.workspaceFileAdded);
                        _monBadge.show();
                        _monBadge.text(count);
                    } else {
                        _monBadge.hide();
                    }
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                }
            }
        });
    },
    doIgnoreEvents: function (eventId) {
        var apiUrl = "";
        if (eventId == "FILESHARE" || eventId == "WORKSPACE" || eventId == "WORKSPACE_FILE_ADDED" || eventId == "WORKSPACE_FILE_VIEWED") {
            apiUrl = apicallurl + "/events/markAsReadByType/" + eventId;
        } else {
            apiUrl = apicallurl + "/events/markAsRead/" + eventId;
        }
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (isMarked) {
                var _notBody = $(".notification_body");
                if (eventId == "all") {
                    _notBody.find(".cf-circle2.green").each(function () {
                        $(this).removeClass('green').addClass('gray');
                    });
                } else {
                    var type = _notBody.find('#' + eventId + '').closest('li').attr('type');
                    var count = parseInt($('#notify_count').text());
                    count = count - 1;
                    _notBody.find('#' + eventId + '').closest('li').find('.cf-circle2').removeClass('green').addClass('gray');
                }
            },
            complete: function (xhr, statusText) {
                if (eventId == "all") {
                    $('#cf_notification').fadeToggle('slow');
                }
                if (xhr.status > 300) {
                }
            }
        });
    },
    doClearAllEvents: function (eventId) {
        var apiUrl = apicallurl + "/events/clear/" + eventId;
        $.ajax({
            type: "DELETE",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function () {
                var _shWithmeCount = $("#CFSharedWithMe").find("#count");
                if (eventId == "all") {
                    $('.notification_body>li').each(function () {
                        $(this).remove();
                    });
                    $('#notify_count').text('').hide();
                    $('.cf-workspace').next('.badge').text('').hide();
                    _shWithmeCount.text('').hide();
                    $('.notification_footer button,.notification_head #ignore,.notification_head #clear').addClass('buttonDisable');
                } else {
                    var _notBody = $(".notification_body");
                    var type = _notBody.find('#' + eventId + '').closest('li').attr('type');
                    var count = parseInt($('#notify_count').text());
                    count = count - 1;
                    if (count > 0) {
                        $('#notify_count').text(count);
                    } else {
                        $('#notify_count').text('').hide();
                    }
                    _notBody.find('#' + eventId + '').closest('li').remove();
                    var text;
                    if (type == "FILESHARE") {
                        text = parseInt(_shWithmeCount.text());
                        text = text - 1;
                        if (text > 0) {
                            _shWithmeCount.text(text);
                        } else {
                            _shWithmeCount.text('').hide();
                        }
                    } else {
                        var _monBadge = $(".cf-workspace").next(".badge");
                        text = parseInt(_monBadge.text());
                        text = text - 1;
                        if (text > 0) {
                            _monBadge.text(text);
                        } else {
                            _monBadge.text('').hide();
                        }
                    }
                    if (_notBody.children('li').length < 0) {
                        $('.notification_footer button,.notification_head #ignore,.notification_head #clear').addClass('buttonDisable');
                    }
                }
            },
            complete: function (xhr, statusText) {
                $('#cf_notification').fadeToggle('slow');
                if (xhr.status > 300) {
                }
            }
        });
    },
    getActivities: function (PageNumber, PageSize) {
        var apiUrl = apicallurl + "/analytics/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "?page_nbr=" + PageNumber + "&page_size=" + PageSize + "";
        var ActivitesInfo;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (Activities) {
                ActivitesInfo = Activities;
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
        return ActivitesInfo;
    },

    //******** sorting file names ***********//
    getSortFiles: function (sortCond, PageNumber) {
        var apiUrl;
        if (PageName == "Folders") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=&isAllFiles=false";
        }
        else if (PageName == "CloudDrive") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + SingleCloudId + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=&isAllFiles=true";
        }
        else if (PageName == "Home") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=&isAllFiles=true";
        }
        else if (PageName == "InnerFolders") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + InnerCloudId[0] + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=" + InnerFolderId[0] + "&isAllFiles=true";
        }
        else if (PageName == "Share by Me") {
            apiUrl = apicallurl + "/fileshare/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "?page_nbr=1&page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&sharedBy=me";
        }
        else if (PageName == "Share with Me") {
            apiUrl = apicallurl + "/fileshare/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "?page_nbr=1&page_size=50&isAscen=" + urlParameterObject.isAscen + "&fetchCollabInfo=true&orderField=" + sortCond + "&sharedBy=others";
        }
        else if (PageName == "All Items") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/cloud/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&fetchCollabInfo=true&page_nbr=" + PageNumber + "";
        }
        else if (PageName == "Category") {
            apiUrl = apicallurl + "/category/files/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/category?categoryId=" + globalCategoryId + "&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond;
        }
        else if (PageName == "Recent files") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/cloud/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber;
        }
        else if (PageName == "Favorites") {
            return false;
        }
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (SortFiles) {
                if (PageName == "Share with Me" || PageName == "Share by Me") {
                    CFManageCategoryAjaxCall.getSharedFileNames(SortFiles);
                }
                else {
                    CFManageCloudAccountsAjaxCall.getAllFileNames(SortFiles, PageNumber);
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                   // $.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getSortFilesD: function (sortCond, PageNumber) {
        var apiUrl;
        if (PageName == "Folders") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=&isAllFiles=false";
        } else if (PageName == "CloudDrive") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + SingleCloudId + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=&isAllFiles=true";
        } else if (PageName == "Home") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=&isAllFiles=true";
        } else if (PageName == "InnerFolders") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + InnerCloudId[0] + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=" + InnerFolderId[0] + "&isAllFiles=true";
        } else if (PageName == "Share by Me") {
            apiUrl = apicallurl + "/fileshare/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "?page_nbr=1&page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&sharedBy=me";
        } else if (PageName == "Share with Me") {
            apiUrl = apicallurl + "/fileshare/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "?page_nbr=1&page_size=50&isAscen=" + urlParameterObject.isAscen + "&fetchCollabInfo=true&orderField=" + sortCond + "&sharedBy=others";
        } else if (PageName == "All Items") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/cloud/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&fetchCollabInfo=true&page_nbr=" + PageNumber + "&isAllFiles=true";
        } else if (PageName == "Recent files") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/cloud/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond + "&page_nbr=" + PageNumber;
        } else if (PageName == "Category") {
            apiUrl = apicallurl + "/category/files/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/category?categoryId=" + globalCategoryId + "&isAscen=" + urlParameterObject.isAscen + "&orderField=" + sortCond;
        } else if (PageName == "Favorites") {
            return false;
        }
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (SortDFiles) {
                if (PageName == "Share with Me" || PageName == "Share by Me") {
                    CFManageCategoryAjaxCall.getSharedFileNames(SortDFiles);
                }
                else {
                    CFManageCloudAccountsAjaxCall.getAllFileNames(SortDFiles, PageNumber);
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                   // $.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },

    //CloudFuze Files
    getAllFiles: function (PageNumber) {
        var apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=" + urlParameterObject.isAllFiles;
        var _a = '';
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (s) {
                return _a = s;
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
        return _a;
    },
    getAllFileNames: function (ALLFilesForAllClouds, PageNumber, fileshareUrl, sharedFolderId) {
        enblePanel();
        $('#LVHeader #CFHSortFileName,#LVHeader #CFHSortfileSize').removeAttr('style');
        $("#LVHeader").find(".LVHFavorites").removeAttr('style');
        if (ALLFilesForAllClouds.length == 1 || ALLFilesForAllClouds.length == 0) {
            $('#ScrollBarScrolling').addClass('divdisplay');
            $('#spinner2').hide();
        }
        if (PageNumber == 1) {
            $('#ThumbnailContent').html('');
            $('#LVContent').html('');
            totalFiles = 0;
        }
        if (PageName == 'move' || PageName == 'innermove') {
            var _moveSrc = $("#moveSource");
            var _moveDest = $("#moveDestination");
            if (moveCheckSum == 'source' && PageNumber == 1) {
                _moveSrc.append('<div style="width:80%;height:30px;margin: 0 auto;background:rgba(20,124,185,0.6);padding:6px;padding-left: 4px;"><input type="checkbox" style="margin:0;margin-left:12px" id="moveSourceCheckbox"><strong style="margin-left: 34px;display: inline-block;width: :90%">File / Folder Name</strong></div>');
                _moveSrc.append('<div class="list-group" style="max-height: 350px;width: 100%;height:350px">');
            }
            if (moveCheckSum == 'dest' && PageNumber == 1) {
                _moveDest.append('<div class="list-group" style="height:350px">');
            }
            if (ALLFilesForAllClouds.length > 49) {
                if (moveCheckSum == 'source') {
                    _moveSrc.siblings('#movePageShowMore').show();
                    _moveSrc.siblings('#movePageShowMore').attr('movepagenumber', PageNumber)
                }
                if (moveCheckSum == 'dest') {
                    _moveDest.siblings('#movePageShowMore').show();
                    _moveDest.siblings('#movePageShowMore').attr('movepagenumber', PageNumber)
                }
            } else {
                if (moveCheckSum == 'source') {
                    _moveSrc.siblings('#movePageShowMore').hide();
                    _moveSrc.siblings('#movePageShowMore').removeAttr('movepagenumber');
                }
                if (moveCheckSum == 'dest') {
                    _moveDest.siblings('#movePageShowMore').hide();
                    _moveDest.siblings('#movePageShowMore').removeAttr('movepagenumber');
                }
            }
        }

        $.each(ALLFilesForAllClouds, function (i, AllCloudFileNames) {
            if (AllCloudFileNames != null && AllCloudFileNames.objectName != null) {
                var objectName = AllCloudFileNames.objectName;
                var objectSize = AllCloudFileNames.objectSize;
                var cloudName = AllCloudFileNames.cloudName;
                var cloudId = AllCloudFileNames.cloudId;
                var fileId = AllCloudFileNames.id;
                var parent = AllCloudFileNames.parent;
                var fileExten = AllCloudFileNames.objectExtn;
                var filetype1 = AllCloudFileNames.directory,
                    type = AllCloudFileNames.type;
                var filetype;

                if (fileshareUrl != undefined) {
                    fileshareUrl = fileshareUrl.replace(/([?|&]fileId=)[^\&]+/, '$1' + fileId);
                    fileshareUrl = fileshareUrl + "&isShared=true";
                }
                if (filetype1 == false && type == "SECTION") {
                    filetype = "NOTE"
                }
                else if (filetype1 == false) {
                    filetype = "FILE"
                }
                else if(filetype1 == true && type == "NOTEBOOK"){
                    filetype = "NOTEBOOK"
                }
                else if (filetype1 == true) {
                    filetype = "FOLDER"
                } else if (filetype1 == null && AllCloudFileNames.fileType == null) {
                    filetype = "SITE"
                }
                else if (filetype1 == undefined || filetype1 == null) {
                    filetype = "SITE"
                }
                var favourite = AllCloudFileNames.favourite;
                var favouriteicon = favourite == true ? heartFill : heart;
                var fileIcon = CFManageCloudAccountsAjaxCall.getFileIcon(fileExten, filetype);
                var dateCreated = AllCloudFileNames.createdTime == null ? "___" : CFManageCloudAccountsAjaxCall.getDateConversion(AllCloudFileNames.createdTime);
                var dateModified = AllCloudFileNames.modifiedTime == null ? "___" : CFManageCloudAccountsAjaxCall.getDateConversion(AllCloudFileNames.modifiedTime);

                if (PageName == "InnerWorkspaceFolders") {
                    var _workFile = '<tr class="gradeA">' +
                            '<td class="wsfcheckbox" style="width:32px"><input type="checkbox" /></td>' +
                            '<td class="sorting_1" style="cursor:pointer;" name=' + filetype1 +
                            ' cloudid=' + AllCloudFileNames.cloudId + ' fileper=' + AllCloudFileNames.id +
                            ' data-type=' + AllCloudFileNames.fileType + ' fexten=' + AllCloudFileNames.objectExtn +
                            ' id=' + AllCloudFileNames.id + ' title="' + objectName + '">' + objectName + '</td>' +
                            '<td class=" ">' + CFManageCloudAccountsAjaxCall.getObjectSize(AllCloudFileNames.objectSize, filetype) +
                            '</td><td class=" ">' + CFManageCloudAccountsAjaxCall.getDateConversion(AllCloudFileNames.modifiedTime) + '</td></tr>'

                    $("#workspaceFiles").append(_workFile);
                }
                else if (PageName == 'move' || PageName == 'innermove') {
                    var typeicon = '';
                    if (filetype == "FILE" || filetype == "NOTE") {
                        typeicon = "cf-file-text";
                    } else if (filetype == "FOLDER" || filetype == "NOTEBOOK") {
                        typeicon = "cf-folder6";
                    }
                    else if (filetype == "SITE") {
                        typeicon = "cf-network";
                    }

                    var _srcFile = '<span class="list-group-item dropabbleParent" id="' + fileId + '" cid="' +
                            cloudId + '" style="height:30px;padding:4px;border-radius:0px">' +
                            '<input type="checkbox" style="float:left;" name="srcfile"/>' +
                            '<label data-type="' + filetype + '" id="' + fileId + '" cid="' + cloudId +'" size="' + objectSize +
                            '" style="width:92%;font-weight:100"><i style="float:left;margin:3px 10px 0px 10px;cursor:move;font-size:16px" class="' + typeicon + '"></i>' +
                            '<p style="float:left;cursor:move;width:87%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;font-size:14px;margin:0">' +
                            objectName + '</p></label><i id="verifyStatus"></i></span>';

                    var _destInput = filetype1 == true ? '<input type="radio" style="float:left;" name="srcfile" />' : '<input type="radio" style="float:left;opacity:0.2;" name="srcfile" disabled />';

                    var _destFile = '<span class="list-group-item dropabbleParent" id="' + fileId + '" cid="' +
                            cloudId + '" style="height:30px;padding:4px;border-radius:0px">' + _destInput +
                            '<label data-type="' + filetype + '" id="' + fileId + '" cid="' + cloudId + '" style="width:92%;font-weight:100">' +
                            '<i class="' + typeicon + '" style="float:left;margin:3px 10px 0px 10px;cursor:pointer;font-size:16px"></i>' +
                            '<p style="float:left;cursor:pointer;width:87%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;font-size:14px;margin:0">' +
                            objectName + '</p></label><i id="verifyStatus"></i></span>';

                    moveCheckSum == 'source' ? $("#moveSource").find(".list-group").append(_srcFile) : $("#moveDestination").find(".list-group").append(_destFile);

                } else {
                    var _shareHtml = sharedFolderId != undefined ? 'fileShare="' + fileshareUrl + '" sharedFolderId="' + sharedFolderId + '">' : '>';

                    /*var _shareStatus = AllCloudFileNames.fileShareStatus == 'INACTIVE' ? 'INACTIVE' : '';*/

                    var _Lfile = '<div class="panel-data" id="' + parent + '" data-type="' + filetype + '">' +
                            '<div class="LVcheckBox" name="' + filetype + '"><input type="checkbox"/></div>' +
                            '<div class="LVfileName" id="' + fileId + '" style="height:20px" name="' + filetype + '" ' + _shareHtml +
                            '<i class="pull-left LV' + filetype + '"></i>' +
                            /*'<sup class="share ' + _shareStatus + '"></sup>' +*/
                            '<p class="pull-left displvfilename" name="' + objectName + '" fexten="' + fileExten + '">'
                            + CFManageCloudAccountsAjaxCall.getMaxChars(objectName,50) + '</p>' +
                            '<i class="LVShare' + AllCloudFileNames.fileShared + '"></i></div>' +
                            '<div class="LVFavorites"><a href="#" id="LVFavorite" class="' + favouriteicon + '"></a></div>' +
                            '<div class="LVfileSize" data-type=' + objectSize + ' style="cursor:pointer;">'
                            + CFManageCloudAccountsAjaxCall.getObjectSize(objectSize, filetype) + '</div>' +
                            '<div class="LVdrive" id="' + cloudId + '">' + CLName[cloudName] + '</div>' +
                            '<div class="LVaddedDate">' + dateCreated + '</div><div class="LVmodifiedDate">' + dateModified + '</div></div>';

                    $("#LVContent").append(_Lfile);

                    var _TFile = '<div class="file ' + fileIcon + '" id="' + cloudId + '" style="cursor:pointer;" data-type="' + filetype + '">' +
                            '<i title="' + objectName + '" class="filethumbnail" name="' + filetype + '"></i>' +
                            '<strong class="filename" pid="' + parent + '" id="' + fileId + '" fexten="' + fileExten + '">'
                            + CFManageCloudAccountsAjaxCall.getMaxChars(objectName,14) + '</strong><div' +
                            ' class="filesize"' +
                            ' data-type='
                            + objectSize + '>' + CFManageCloudAccountsAjaxCall.getObjectSize(objectSize, filetype) + '</div>' +
                            '<input type="checkbox" class="fileCheck" name="' + filetype + '"/>' +
                            '<i id="ThFav" class="' + favouriteicon + '" style="cursor:pointer;"></i></div>';

                    $("#ThumbnailContent").append(_TFile);
                }
                if (previousPage == "Share with Me" && PageName == "InnerFolders") {
                    $(".LVFavorites").addClass('buttonDisable');
                    $("#LVContent").children(".panel-data").attr('fileper', FilePer[0]);
                    $("#ThumbnailContent").children(".file").attr('fileper', FilePer[0]);
                }
            }
            if (i == ALLFilesForAllClouds.length - 1) {
                if (PageName == 'move' || PageName == 'innermove' || PageName == 'moveLanding') {
                    $('#spinner2').hide();
                }
                else if (previousPage == "Share with Me") {
                    if ($.inArray('owner', FilePer) > -1) {

                    }
                }
            }
        });
        if (PageName == 'move' || PageName == 'innermove') {
            $("#moveSource").find(".list-group-item").draggable({
                cursor: 'move',
                containment: 'document',
                helper: helperEvent,
                revert: function (is_valid_drop) {
                    if (!is_valid_drop) {
                        $("#moveSource").find(".fileActive").each(function () {
                            $(this).removeClass('fileActive');
                        });
                        $("#moveSource").find("input").each(function () {
                            $(this).attr('checked', false);
                        });
                        return 'invalid';
                    }
                },
                cursorAt: {top: 0, left: 0}
            });
            $("#moveSource").find(".list-group-item").selectable();
            $("#moveDestination").find(".list-group-item").droppable({
                accept: '#moveSource .list-group-item',
                drop: dropEventHandler,
                greedy: true,
                over: function () {
                    $(this).addClass('draghover').removeClass('dropabbleParent');
                },
                out: function () {
                    $(this).addClass('dropabbleParent').removeClass('draghover')
                }
            });
            $("#moveDestination").find(".list-group").droppable({
                greedy: true,
                accept: '#moveSource .list-group-item',
                drop: rootDropEventHandler,
                hoverClass: 'rootDragHover'
            });
            if (moveCheckSum == 'source' && PageNumber == 1) {
                $("#moveSource").append('</div>');
            }
            if (moveCheckSum == 'dest' && PageNumber == 1) {
                $("#moveDestination").append('</div>');
            }
            $("#moveSource").on('click', '.list-group-item', function (e) {
                if (e.ctrlKey) {
                    $(this).children('input').attr('checked', true);
                    $(this).addClass('fileActive');
                }
            });
        }
        if (ALLFilesForAllClouds.length < 50) {
            if (PageName == "Home" || PageName == "Folders" || PageName == "InnerFolders" || PageName == "AllFiles" ||PageName == "All Files"|| PageName == "All Items" || PageName == "Recent files" || PageName == "Category" || PageName == "CloudDrive" || PageName == "search") {
                setTimeout(function () {
                    $('#listShowMore').addClass('divdisplay').text('Show more files...')
                }, 100);
            }
        } else {
            setTimeout(function () {
                $('#listShowMore').removeClass('divdisplay').text('Show more files...');
            }, 100);
        }
        if (ALLFilesForAllClouds.length < 40) {
            if (PageName == "Favorites") {
                setTimeout(function () {
                    $('#listShowMore').addClass('divdisplay').text('Show more files...');
                }, 100);
            }
        } else if (PageName == "Favorites") {
            setTimeout(function () {
                $('#listShowMore').removeClass('divdisplay').text('Show more files...');
            }, 100);
        }

        $('#CFRecentSpinner').removeClass('ajaxLoadImage');
        $('#CFRecentSpinner').addClass(spinnerClass);
        if (PageName == 'Recent files') {
            setTimeout(function () {
                $('#spinner1').addClass('divdisplay');
            }, 100);
        } else {
            setTimeout(function () {
                $('#spinner1').addClass('divdisplay');
            }, 100);
        }
        if (ALLFilesForAllClouds.length == 0) {
            $('.LVHcheckBox input').prop("disabled", true);
        } else {
            $('.LVHcheckBox input').prop("disabled", false);
        }
        if (PageName == "move" || PageName == "innermove" || PageName == "moveLanding") {

        } else {
            if ($('#mainContentWrapper').hasScrollBar() == true) {
                $('#mainContentWrapper').css('width', '100%');
            } else if ($('#mainContentWrapper').hasScrollBar() == false) {
                $('#mainContentWrapper').css('width', '99%');
            }
        }
        if (selectEvent != undefined && selectEvent != null) {
            selectEvent.init();
        }
    },
    getAllFolders: function (toId, PageNumber) {
        var apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + toId + "?page_size=40&isAscen=false&orderField=createdTime&page_nbr=" + PageNumber + "&fetchCollabInfo=true&folderId=&isAllFiles=false";
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (FoldersOfGivenCloud) {
                if (FoldersOfGivenCloud.length > 0) {
                    $.each(FoldersOfGivenCloud, function (i, Folders) {

                    });
                }
                return false;
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                     showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getChildFolders: function (CId, FId) {
        var apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + CId + "?page_size=40&isAscen=false&orderField=createdTime&page_nbr=1&fetchCollabInfo=true&folderId=" + FId + "&isAllFiles=false";
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (ChildFolders) {
                if (ChildFolders.length > 0) {
                    $.each(ChildFolders, function (i, Folders) {
                        $('#MoveToFunctionality ul#' + FId + '').append('<li id="' + Folders.id + '" ><div style="display: inline-block;width: 100%;padding-left:10px; "><a href="javascript:void(0)" id="' + Folders.id + '" name="Folder" style="float:left;" class="arrw"></a><i style="float:left;padding-left:5px; color:#fff;" class="loc" id="' + Folders.id + '" name="Folder">' + Folders.objectName + '</i></div><ul id="' + Folders.id + '"></ul></li>');
                    });
                }
                return false;
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getIpInfo: function (ip) {
        var apiUrl = domainUrl+"/proxy/ip/" + ip;
        var ipInfo;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            success: function (IpInfo) {
                ipInfo = IpInfo;
            },
            complete: function (xhr) {
                if (xhr.status > 300) {
                    ipInfo = ip;
                }
            }
        });
        return ipInfo;
    },

    //Universal Functions
    getFileIcon: function (fileExten, filetype) {
        var fileIcon;
        var icons = ["jpg", "pdf", "xlsx", "ppt", "ai", "avi", "csv", "dll", "dmg", "exe", "flv", "gif", "jpg", "mov", "mpg", "png", "psd", "wma", "doc", "docx", "xls"];
        if (fileExten != undefined && fileExten != null && filetype == "FILE") {
            if ($.inArray(fileExten, icons) > -1) {
                return fileIcon = fileExten;
            }
            else {
                return fileIcon = "DefaultIcon";
            }
        }
        else if (fileExten == null && filetype == "FOLDER") {
            return fileIcon = "folder";
        } else if (filetype == "SITE") {
            return fileIcon = "SITE";
        }else if(filetype == "NOTEBOOK"){
            return fileIcon = "folder";
        }
        else if(filetype == "NOTE"){
            return fileIcon = "DefaultIcon";
        }
        else {
            return fileIcon = "DefaultIcon";
        }
    },
    getMaxChars: function (a, b) {
        var f = "...";
        var c = parseInt(b / 2);
        var d = a != undefined ? a.length : 0;
        if (a == "" || d < b) {
            return a;
        } else {
            var e = a;
            a = e.substring(0, b - c - 3) + f + e.substring(e.length - c);
        }
        return a;
    },
    getListObjectName: function (objectName) {
        var maxCharacters = 50;
        if (PageName == "InnerWorkSpace") {
            maxCharacters = 50;
        } else {
            maxCharacters = 40;
        }
        return CFManageCloudAccountsAjaxCall.getMaxChars(objectName, maxCharacters);
    },
    getThumbObjectName: function (objectName) {
        var maxCharacters = 14;
        if (PageName == "InnerWorkSpace") {
            maxCharacters = 26;
        } else {
            maxCharacters = 14;
        }
        return CFManageCloudAccountsAjaxCall.getMaxChars(objectName, maxCharacters);
    },

    getObjectSizeInGB: function (sizeBytes, type) {
        if (sizeBytes == undefined || sizeBytes == null || sizeBytes == 0) {
            return "___";
        } else if (type == "FOLDER") {
            return "___";
        } else {
            var displaySize = "___";
            displaySize = parseFloat(sizeBytes / ONE_GB).toFixed(2) + " GB";
            return displaySize;
        }
    },
    getObjectSize: function (sizeBytes, type) {
        if (sizeBytes == undefined || sizeBytes == null || sizeBytes == 0) {
            return "___";
        } else if (type == "FOLDER") {
            return "___";
        } else {
            var displaySize = "___";
            var fsize = 0;
            if (Math.round(sizeBytes / ONE_GB) > 0) {
                fsize = sizeBytes / ONE_GB;
                displaySize = parseFloat(fsize).toFixed(2) + " GB";
            } else if (Math.round(sizeBytes / ONE_MB) > 0) {
                fsize = sizeBytes / ONE_MB;
                displaySize = parseFloat(fsize).toFixed(2) + " MB";
            } else if (Math.round(sizeBytes / ONE_KB) > 0) {
                fsize = sizeBytes / ONE_KB;
                displaySize = parseFloat(fsize).toFixed(2) + " KB";
            } else {
                displaySize = sizeBytes + " Bytes";
            }
            return displaySize;
        }
    },
    getCloudName: function (cloudName) {
        $.each(CLName, function (key, value) {
            if (key == cloudName) {
                cloudName = value;
            }
        });
        return cloudName;
    },
    getDateConversion: function (dateToConvert) {
        if (dateToConvert == undefined || dateToConvert == null || dateToConvert == "") {
            dateToConvert = "___";
            return dateToConvert;
        }
        var date = new Date(dateToConvert);
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        if (month < 10) {
            month = "0" + month;
        }
        var marr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        month = month - 1;
        dateToConvert = marr[month] + " " + day + ", " + year;
        if (date == "Invalid Date") {
            dateToConvert = "___";
        }
        return dateToConvert;
    },
    moveToAjaxCall: function (moveFileObject, files) {
        var jsonArray = {
            "fromRootId": moveFileObject.fromId,
            "toRootId": moveFileObject.toId,
            "type": "MOVE_WORKSPACE",
            "fromCloudId": {
                "id": moveFileObject.fromCId
            },
            "toCloudId": {
                "id": moveFileObject.toCId
            },
            "fromCloudSpace": null,
            "toCloudSpace": null,
            "validSpace": true,
            "errorDescription": null,
            "totalFolders": null,
            "totalFiles": null,
            "moveFoldersStatus": false,
            "totalFilesAndFolders": 0,
            "userEmails": moveFileObject.userEmails,
            "threadBy": null,
            "retry": 0,
            "useEncryptKey": false,
            "notify": moveFileObject.notify,
            "fileMove": false,
            "success": false
        };
        var apiurl = apicallurl + "/move/clouds/files?isCopy=" + moveFileObject.isCopy;
        jsonArray.fileIds =files;
        var MoveToJson = JSON.stringify(jsonArray);

        $.ajax({
            type: 'POST',
            url: apiurl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            data: MoveToJson,
            dataType: 'json',
            success: function (move) {
                var moveId = move.id;
                var moveProgress = move.processStatus;
                switch (moveProgress) {
                    case "PROCESSED"        :
                        $("#moveProgressBar").find(".progress-bar-success").css('width', '100%');
                        setTimeout(function () {
                            $("#moveProgressBar").hide();
                            $("#moveProgressBar").find(".progress-bar-success").css('width', '0%');
                        }, 2000);
                        $('#moveSource [id="' + move.fromRootId + '"]').children('input').prop('checked', false);
                        $('#moveSource [id="' + move.fromRootId + '"]').fadeOut(500);

                        break;
                    case "IN_PROGRESS"        :
                        CFManageCloudAccountsAjaxCall.getStatus(moveId);
                        break;
                    case "NOT_PROCESSED":
                        CFManageCloudAccountsAjaxCall.getStatus(moveId);
                        break;
                    case "SUSPENDED":
                        CFManageCloudAccountsAjaxCall.getStatus(moveId);
                        break;
                    case "PROCESSED_WITH_SOME_ERRORS":
                        CFManageCloudAccountsAjaxCall.getStatus(moveId);
                        break;
                    case "PROCESSED_WITH_SOME_WARNINGS":
                         CFManageCloudAccountsAjaxCall.getStatus(moveId);
                         break;
                    case "ERROR"                :
                        break;
                }
            },
            complete: function (xhr) {
                if (xhr.status > 300) {
                    unCheckFile();
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getStatus: function (moveId, drag) {
        var apiUrl = apicallurl + "/move/clouds/status/" + moveId + "";
        setTimeout(function () {
            $.ajax({
                type: "GET",
                url: apiUrl,
                async: false,
                dataType: "json",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
                },
                success: function (status) {
                    var moveStatus = status.processStatus;
                    if (drag == false) {
                        var tolal_files = status.totalFilesAndFolders;
                        var moved_folders = status.totalFolders;
                        var moved_files = 0;
                        if (status.totalFiles != null) {
                            moved_files = status.totalFiles;
                        }
                        var totalFiles = moved_folders + moved_files;
                        var moveProgress = (totalFiles / tolal_files) * 100 + "%";
                        $("#moveProgressBar").find(".progress-bar-success").css('width', moveProgress);
                    }
                    switch (moveStatus) {
                        case "PROCESSED"        :
                            $("#moveProgressBar").find(".progress-bar-success").css('width', '100%');
                            setTimeout(function () {
                                $('#moveProgressBar').hide();
                                $("#moveProgressBar").find(".progress-bar-success").css('width', '0%');
                            }, 2000);
                            $('#moveSource [id="' + status.fromRootId + '"]').children('input').prop('checked', false);
                            $('#moveSource [id="' + status.fromRootId + '"]').fadeOut(500);
                            break;
                        case "IN_PROGRESS"        :
                            CFManageCloudAccountsAjaxCall.getStatus(status.id);
                            break;
                        case "NOT_PROCESSED":
                            CFManageCloudAccountsAjaxCall.getStatus(status.id);
                            break;
                        case "SUSPENDED"                :
                            $("#moveProgressBar").hide();
                            $("#moveProgressBar").find(".progress-bar-success").css('width', '0%');
                            break;
                        case "ERROR"                :
                            $("#moveProgressBar").hide();
                            $("#moveProgressBar").find(".progress-bar-success").css('width', '0%');
                            break;
                        case "PROCESSED_WITH_SOME_ERRORS"                :
                            $("#moveProgressBar").hide();
                            $("#moveProgressBar").find(".progress-bar-success").css('width', '0%');
                            break;
                        case "PROCESSED_WITH_SOME_WARNINGS"                :
                            $("#moveProgressBar").hide();
                            $("#moveProgressBar").find(".progress-bar-success").css('width', '0%');
                            break;
                    }
                },
                complete: function (xhr) {
                    if (xhr.status > 300) {
                        unCheckFile();
                        //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                        showNotyNotification("error","Operation Failed");
                    }
                }
            });
        }, 10000);
    },
    moveToVerify: function (fromfid, fromcid, tofid, tocid) {
        $('#VerifyMsg').removeAttr('class').html('');
        var moveVerify = {
            "fromCloudId": {
                "id": fromcid
            },
            "toCloudId": {
                "id": tocid
            },
            "fromRootId": fromfid,
            "toRootId": tofid,
            "deleteOriginalFiles": false,
            "createdTime": "",
            "modifiedTime": "",
            "userEmails": [],
            "useEncryptKey": false,
            "notify": false,
            "fileMove": false,
            "success": false
        };
        var jsondata = JSON.stringify(moveVerify);
        var apiUrl = apicallurl + "/move/verify";
        $.ajax({
            type: "POST",
            url: apiUrl,
            async: true,
            dataType: "json",
            data: jsondata,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
            },
            statusCode: {
                202: function () {
                    $("#VerifyMsg").addClass('verifySuccess');
                    $("#VerifyMsg").text('Verified Successfully.');
                },
                406: function () {
                    $("#VerifyMsg").addClass('verifyFailure');
                    $("#VerifyMsg").text('Already exists in destination folder. Please verify and correct.');
                },
                400: function () {
                    $("#VerifyMsg").addClass('verifyFailure');
                    $("#VerifyMsg").text('Request failed..try again.');
                }
            }
        });
    },
    movereportForGiveId: function (moveId, page) {
        var apiUrl = apicallurl + "/move/movereport?workspaceId=" + moveId + "&page_nbr=" + page;
        var totalmoved;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                var moveStatus = {
                    "PROCESSED": "Processed",
                    "IN_PROGRESS": "In Progress",
                    "NOT_PROCESSED": "In queue",
                    "ERROR": "Error",
                    "IN_QUEUE": "In queue",
                    "WARNING": "Warning",
                    "SUSPENDED": "Suspended",
                    "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
                    "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
                    "CANCEL": "Cancel"
                };
                if (data == undefined || data == null || data.length < 1) {
                    $('#moveReportsForUser #'+moveId+' #getMoveDetails').children().removeClass("cf-minus5").addClass("cf-plus5disabled");
                    $('#spinner2').hide();
                    showNotyNotification('notify','No migration details found. ');
                    return false;
                }
                var count = CFManageCloudAccountsAjaxCall.fileFolderCount(moveId);
                if(count.totalFiles !=null)
                {
                    //$("#"+moveId).next().find("#_totfiles").text("Total No: of files & folders : " + (count.totalFiles + count.totalFolders));
                    // $("#"+moveId).next().find("#_movefolders").text("No: of folders : " + count.totalFolders);
                    // $("#"+moveId).next().find("#_movefiles").text("No: of files : " + count.totalFiles);
                    $("#"+moveId).next().find("#_totfiles").text("No. of Files/Folders : " + (count.totalFiles + count.totalFolders)/*+" ( "+count.totalFiles+" + "+count.totalFolders+" )"*/);
                    $("#"+moveId).next().find("#_movefolders").text("Error Files/Folders : "+count.errorCount);
                    $("#"+moveId).next().find("#_movefiles").text("Processed Files/Folders : "+count.processedCount );
                }
                $("#"+moveId).next().next().find("#_prcsdCnt").text("No: of Processed files : "+count.processedCount);
                $("#"+moveId).next().next().find("#_wrngCnt").text("No: of Warned files : "+count.warningCount);
                $("#"+moveId).next().next().find("#_errCnt").text("No: of Error files : "+count.errorCount);
                var folderCount = count == null ? 'N/A' : count.totalFolders;
                var fileCount = count == null ? 'N/A' : count.totalFiles;
                if (page) {
                    totalmoved = folderCount + fileCount;
                    var formCloud = data[0].fromCloudId;
                    var toCloud = data[0].toCloudId;
                    var moveObj = data[0].moveWorkSpace;
                    var processStatus = moveStatus[moveObj.processStatus];
                    $('#moveDetailedReport').children('div:eq(2)').remove();
                    var _fcn = formCloud != null ? formCloud.cloudName : 'Not Available',
                            _fcd = formCloud != null ? formCloud.userDisplayName : 'Not Available',
                            _tcn = toCloud != null ? toCloud.cloudName : 'Not Available',
                            _tcd = toCloud != null ? toCloud.userDisplayName : 'Not Available';
                    /*$('#moveReportsBack').after('<div style="width: 100%;margin: 20px;">' +
                            '<div style="width: 60%;padding: 15px 0 0 15px;" class="blockCode">' +
                            '<div style="width: 100%;height: 40px;" class="blockCode">' +
                            '<b style="width: 13%;margin-top:6px;" class="blockCode">From:</b>' +
                            '<i class="blockCode Driveicon" id="' + _fcn + '" style="width: 7%;"></i>' +
                            '<span class="blockCode" style="margin-top:6px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width:26%">' + _fcd + '</span>' +
                            '<b style="width: 10%;margin-top:6px;" class="blockCode">To:</b>' +
                            '<i style=";width: 7%" class="blockCode Driveicon" id="' + _tcn + '"></i>' +
                            '<span class="blockCode" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis;width:26%;margin-top:6px;">' + _tcd + '</span>' +
                            '</div><div style="width: 100%; height: 40px;" class="blockCode"><b class="blockCode" style="width: 26%">No.of Folders:</b>' +
                            '<span class="blockCode" style="width: 21%">' + folderCount + '</span><b class="blockCode" style=";width: 23%">No.of Files:</b>' +
                            '<span class="blockCode" style="width: 17%">' + fileCount + '</span></div></div>' +
                            '<div style="width: 30%;padding: 15px 0 0 15px;" class="blockCode"><div style="width: 100%;height: 40px;" class="blockCode">' +
                            '<b class="blockCode" style="width: 57%">Process Status:</b>' +
                            '<span style="" class="blockCode ' + moveObj.processStatus + '">' + processStatus + '</span>' +
                            '</div><div style="width: 100%;height: 40px" class="blockCode">' +
                            '<b class="blockCode" style="width: 57%">Files & Folders:</b>' +
                            '<span class="blockCode">' + totalmoved + '</span></div></div></div>');*/
                    var moveObject = data[0].moveWorkSpace;
                    var userEmails = moveObject.userEmails;
                    var emails = '';
                    if (userEmails != null || userEmails != null && userEmails.length != 0) {
                        $.each(userEmails, function (i, email) {
                            emails = email + "," + emails;
                        });
                    }
                    // $('#moveNotifiedUsers').text(emails);
                    // $('#moveDetailedReport').modal();
                    // $("#_FmoveCount").text("No: of Files & Folders : "+count);
                    // $("#_FmoveCount").text("Notified Users : "+emails);
                }
                $('#moveReportFilesList').find('li:last-child').remove();
                $("#"+moveId).next().next().next().find("#moveReportFilesList").html('');
                CFManageCloudAccountsAjaxCall.appendMoveFiles(data, moveStatus, page);
                // if (data.length > 19) {
                //     $('#moveReportFilesList').append('<li type="button" data-move="' + moveId + '" data-page="' + page + '" class="button mini blue movereport" style="float:left;width:100%;cursor:pointer">Show More</li>')
                // }
                var _id;
                var _d=Math.ceil(totalmoved/20);
                //if(page==1)
                   // $("#"+moveId).next().next().find("#moveFiles_pagination").twbsPagination('destroy');
                $("#"+moveId).next().next().next().find("#moveFiles_pagination").twbsPagination({
                    totalPages: _d,
                    visiblePages: 10,
                    onPageClick: function (event, page) {
                        _id=moveId;
                        CFManageCloudAccountsAjaxCall.movereportForGiveId(moveId,page);
                    }
                });
                $('#spinner2').hide();
                $("#"+moveId).next().show();
                //$("#"+moveId).next().next().show();
                $("#"+moveId).next().next().next().show();
            }
        });
    },
    fileFolderCount: function (a) {
        var _b;
        var apiCall = apicallurl + "/move/moveworkspacefilefoldercount?workspaceId=" + a;
        $.ajax({
            url: apiCall,
            type: 'get',
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (s) {
                return _b = s;
            }
        });
        return _b;
    },
    appendMoveFiles: function (data, b) {
        var _status = ["CANCEL", "WARNING", "ERROR"];
        var moveId=data[0].moveWorkSpaceId;
        $("#"+moveId).next().find("#moveReportFilesList").html('');
        $.each(data, function (i, file) {
            var fi = file.cfFileMini;
            var filetype = '';
            if (fi.directory == true) {
                filetype = "FOLDER";
            }
            else if (fi.directory == false) {
                filetype = 'FILE';
            }
            var fileName = ['All Files', 'My Drive', 'SkyDrive', "", 'My Files & Folders', 'cloudian'];
            var parent1 = fi.parentFileRef;
            var parent = '';
            var fstatus = file.processStatus;
            if (parent1 == null || $.inArray(parent1.objectName, fileName) > -1) {
                parent = '&lt;root&gt;';
            }
            else {
                parent = parent1.objectName;
            }
            var _a = $.inArray(file.processStatus, _status) > -1;
            file.processStatus = b[file.processStatus];
            var _errorDesc = "";
            if (_a) {
                _errorDesc = JSON.stringify(file.errorDescription);
                _a = '<i id="_error' + file.id + '" class="cf-warning3" style="margin-left:5px;color:red"></i>';
            }
            else {
                _errorDesc = "";
                _a = "<i></i>"
            }
            // $('#moveReportFilesList').append('<li>' +
            //         '<span class="" title="' + fi.objectName + '" style="width:53%;float:left;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;">' +
            //         '<i class="blockCode LV' + filetype + '"></i>' + fi.objectName + '</span>' +
            //         '<span style="width:15%;float:left;" class="' + fstatus + '">' + file.processStatus + _a + '</span>' +
            //         '<span style="width:15%;float:left;">' + CFManageCloudAccountsAjaxCall.getObjectSize(fi.objectSize, filetype) + '</span>' +
            //         '<span style="width:17%;float:left;">' + CFManageCloudAccountsAjaxCall.getDateConversion(file.createdTime) + '</span></li>');
            $("#"+moveId).next().next().next().find("#moveReportFilesList").append('<tr>' +
                '<td class="" title="' + fi.objectName + '" ><i class="blockCode LV' + filetype + '"></i>' + CFManageCloudAccountsAjaxCall.getMaxChars(fi.objectName,30) + '</td>' +
                '<td class="' + fstatus + '">' + file.processStatus + _a + '</td>' +
                '<td>' + CFManageCloudAccountsAjaxCall.getObjectSize(fi.objectSize, filetype) + '</td>' +
                '<td>' + CFManageCloudAccountsAjaxCall.getDateConversion(file.createdTime) + '</td></tr>');if ($('#_error' + file.id).length) {
                $('#_error' + file.id).attr("title", _errorDesc);
            }
        });
    },
    getAllMoveStatusForUser: function () {
        var apiUrl = apicallurl + "/move/list?page_size=30&page_nbr=" + PageNumber;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                if (data.length == 30) {
                    $('#showMoreReports').show();
                } else {
                    $('#showMoreReports').hide();
                }
                var moveStatus = [
                    {"key": "PROCESSED", "value": "Processed"},
                    {"key": "IN_PROGRESS", "value": "In Progress"},
                    {"key": "NOT_PROCESSED", "value": "In queue"},
                    {"key": "ERROR", "value": "Error"},
                    {"key": "IN_QUEUE", "value": "In queue"},
                    {"key": "WARNING", "value": "Warning"},
                    {"key": "SUSPENDED", "value": "Suspended"},
                    {"key": "PROCESSED_WITH_SOME_ERRORS", "value": "Processed with some errors"},
                    {"key": "PROCESSED_WITH_SOME_WARNINGS", "value": "Processed with some warnings"},
                    {"key": "CANCEL", "value": "Cancel"}
                ];
                if (PageNumber == 1) {
                    $("#moveReportsForUser").html('');
                    //$("#moveReportsForUser").append('<div class="tab-header" style="background: #2bb5d8; color:white;"><span style="display: block;float: left;width: 25%;padding-left:18px">File Name</span><span style="display: block;float: left;width: 23%">From Cloud</span><span style="display: block;float: left;width: 20%">To Cloud</span><span style="display: block;float: left;width: 12%">Processed Status</span><span style="display: block;float: left;width: 10%">Processed On</span></div>');
                }
                var _ertitle = "";
                var _erClass = "";
                var _status = ["CANCEL", "WARNING", "ERROR"];
                $.each(data, function (i, move) {
                    if (move.fromCloudId != null && move.toCloudId != null) {
                        var fromCloud = move.fromCloudId;
                        var toCloud = move.toCloudId;
                        var totalFiles = move.totalFilesAndFolders;
                        var colorClass = move.processStatus;
                        _ertitle = move.errorDescription != null ? move.errorDescription : "Error occurred while moving the files, please contact the support team";
                        var _ar = $.inArray(move.processStatus, _status) > -1;
                        _erClass = _ar == true ? "cf-warning3" : "";
                        $.each(moveStatus, function (i, code) {
                            if (code.key == move.processStatus) {
                                move.processStatus = code.value;
                            }
                        });
                        toCloud.userDisplayName = toCloud.userDisplayName == null ? toCloud.cloudUserId.split('|').pop() : toCloud.userDisplayName;
                        fromCloud.userDisplayName = fromCloud.userDisplayName == null ? fromCloud.cloudUserId.split('|').pop() : fromCloud.userDisplayName;

                        if (totalFiles == 0) {
                            /*$('#moveReportsForUser').append('<div style="width: 100%;padding:5px;height: 35px;border-bottom:1px solid #ccc;" id="' + move.id + '">' +
                                '<div class="pull-left" style="height: 30px;width: 25%;padding-left:25px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis"><span>' + move.fileName + '</span></div>' +
                                '<div style="height: 30px;width:46%;" class="pull-left">' +
                                '<b style="height: 30px; width:50%;float: left">' +
                                '<i class="Driveicon pull-left" id="' + fromCloud.cloudName + '"></i>' +
                                '<span style="display: block;margin-top:5px">' + fromCloud.userDisplayName + '</span></b>' +
                                '<b style="height: 30px;width:50%;float: left">' +
                                '<i class="Driveicon pull-left" id="' + toCloud.cloudName + '"></i>' +
                                '<span style="display:block;margin-top:5px">' + toCloud.userDisplayName + '</span></b></div>' +
                                '<div class="pull-left" style="height: 30px;width: 10%"><span class="' + colorClass + '">' + move.processStatus + '</span></div>' +
                                '<div style="float: left;width:auto;">' + jQuery.timeago(move.createdTime) + '</div>' +
                                '<div style="float: right;width:auto;">Single File move</div>' +
                                '<i style="float:right;margin-right:10px;color:red" title="' + _ertitle + '" class="' + _erClass + '"></i></div>');*/
                            $('#moveReportsForUser').append('<tr id="' + move.id + '" style="font-size: 14px;height: 40px;border-bottom:1px solid #ccc">'+
                                '<td mid="' + move.id + '" id="getMoveDetails"><i class="cf-plus5" style="font-size: 10px;margin-left: 7px;cursor: pointer;"></i></td>'+
                                '<td>'+ CFManageCloudAccountsAjaxCall.getMaxChars(move.fileName,35) +'</td>'+
                                '<td><i class="Driveicon pull-left" id="' + fromCloud.cloudName + '"></i><span style="display: block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(fromCloud.userDisplayName,25) + '</span></td>'+
                                '<td><i class="Driveicon pull-left" id="' + toCloud.cloudName + '"></i><span style="display:block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(toCloud.userDisplayName,25) + '</span></td>'+
                                '<td><span class="' + colorClass + '">' + move.processStatus + '</span></td>'+
                                '<td style="padding-left: 15px;">' + jQuery.timeago(move.createdTime) + '</td>'+
                                '<td></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_totfiles" style="text-align: center"></td><td id="_movefiles" style="text-align: center"></td><td id="_movefolders" style="text-align: right"></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_prcsdCnt" style="text-align: center"></td><td id="_wrngCnt" style="text-align: right"></td><td></td><td id="_errCnt"></td></tr>'+
                                '<tr class="hide-report2" style="display:none"><td colspan="5"><table class="table" style="margin-left:10%"><thead><tr><th>File Name</th><th style="padding-left: 13px">Status</th><th>Size</th><th style="padding-left: 0px">Created Date</th></tr></thead>'+
                                '<tbody id="moveReportFilesList"></tbody><tfoot><tr style="border:none !important;"><th colspan="3"><div class="paginations" style="display: flex;justify-content: center">'+
                                '<ul id="moveFiles_pagination" class="pagination-sm users_footer" style="margin:0;display: flex;justify-content: center"></ul></div></th></tr></tfoot></table></td></tr>');

                        }
                        else {
                            /*$('#moveReportsForUser').append('<div style="width: 100%;padding:5px;height: 35px;border-bottom:1px solid #ccc;" id="' + move.id + '">' +
                                '<div class="pull-left" style="height: 30px;width: 25%;padding-left:25px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis">' +
                                '<span>' + move.fileName + '</span></div><div style="height: 30px;width:46%;" class="pull-left">' +
                                '<b style="height: 30px; width:50%;float: left"><i class="Driveicon pull-left" id="' + fromCloud.cloudName + '"></i>' +
                                '<span style="display: block;margin-top:5px">' + fromCloud.userDisplayName + '</span></b>' +
                                '<b style="height: 30px;width:50%;float: left"><i class="Driveicon pull-left" id="' + toCloud.cloudName + '"></i>' +
                                '<span style="display:block;margin-top:5px">' + toCloud.userDisplayName + '</span></b></div>' +
                                '<div class="pull-left" style="height: 30px;width: 10%">' +
                                '<span class="' + colorClass + '">' + move.processStatus + '</span></div>' +
                                '<div style="float: left;width:auto;">' + jQuery.timeago(move.createdTime) + '</div>' +
                                '<div style="float: right;width:6%;cursor: pointer" class="button blue mini" mid="' + move.id + '" id="getMoveDetails">Details</div>' +
                                '<i style="float:right;margin-right:10px;color:red" title="' + _ertitle + '" class="' + _erClass + '"></i></div>');*/
                            $('#moveReportsForUser').append('<tr id="' + move.id + '" style="font-size: 14px;height: 40px;border-bottom:1px solid #ccc">'+
                                '<td mid="' + move.id + '" id="getMoveDetails"><i class="cf-plus5" style="font-size: 10px;margin-left: 7px;cursor: pointer;"></i></td>'+
                                '<td>'+ CFManageCloudAccountsAjaxCall.getMaxChars(move.fileName,35)  +'</td>'+
                                '<td><i class="Driveicon pull-left" id="' + fromCloud.cloudName + '"></i><span style="display: block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(fromCloud.userDisplayName,25) + '</span></td>'+
                                '<td><i class="Driveicon pull-left" id="' + toCloud.cloudName + '"></i><span style="display:block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(toCloud.userDisplayName,25) + '</span></td>'+
                                '<td><span class="' + colorClass + '">' + move.processStatus + '</span></td>'+
                                '<td style="padding-left: 15px;">' + jQuery.timeago(move.createdTime) + '</td><td></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_totfiles" style="text-align: center"></td><td id="_movefiles" style="text-align: center"></td><td id="_movefolders" style="text-align: right"></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_prcsdCnt" style="text-align: center"></td><td id="_wrngCnt" style="text-align: right"></td><td></td><td id="_errCnt"></td></tr>'+
                                '<tr class="hide-report2" style="display:none"><td colspan="5"><table class="table" style="margin-left:10%"><thead><tr><th>File Name</th><th style="padding-left: 13px">Status</th><th>Size</th><th style="padding-left: 0px">Created Date</th></tr></thead>'+
                                '<tbody id="moveReportFilesList"></tbody><tfoot><tr style="border:none !important;"><th colspan="3"><div class="paginations" style="display: flex;justify-content: center">'+
                                '<ul id="moveFiles_pagination" class="pagination-sm users_footer" style="margin:0;display: flex;justify-content: center"></ul></div></th></tr></tfoot></table></td></tr>');
                        }
                    }
                    if (move.fromCloudId == null || move.toCloudId == null) {
                        totalFiles = move.totalFilesAndFolders;
                        colorClass = move.processStatus;
                        _ertitle = move.errorDescription != null ? move.errorDescription : "Error occurred while moving the files, please contact the support team";
                        var _ar = $.inArray(move.processStatus, _status) > -1;
                        _erClass = _ar == true ? "cf-warning3" : "";
                        var ToCloud, FromCloud;
                        //toCloud.userDisplayName = toCloud.userDisplayName == null ? toCloud.cloudUserId.split('|').pop() : toCloud.userDisplayName;
                        //fromCloud.userDisplayName = fromCloud.userDisplayName == null ? fromCloud.cloudUserId.split('|').pop() : fromCloud.userDisplayName;

                        if (move.fromCloudId == null && move.toCloudId != null) {
                            ToCloud = move.toCloudId;
                            FromCloud = {};
                            FromCloud.id = "Not Available";
                            FromCloud.cloudName = "Not_Available";
                            FromCloud.userDisplayName = "Not Available";
                            ToCloud.userDisplayName = ToCloud.userDisplayName == null ? ToCloud.cloudUserId.split('|').pop() : ToCloud.userDisplayName;
                        }
                        else if (move.toCloudId == null && move.fromCloudId != null) {
                            FromCloud = move.fromCloudId;
                            ToCloud = {};
                            ToCloud.id = "Not Available";
                            ToCloud.cloudName = "Not_Available";
                            ToCloud.userDisplayName = "Not Available";
                            FromCloud.userDisplayName = FromCloud.userDisplayName == null ? FromCloud.cloudUserId.split('|').pop() : FromCloud.userDisplayName;
                        }
                        else if (move.toCloudId == null && move.fromCloudId == null) {
                            ToCloud = {};
                            ToCloud.id = "Not Available";
                            ToCloud.cloudName = "Not_Available";
                            ToCloud.userDisplayName = "Not Available";
                            FromCloud = {};
                            FromCloud.id = "Not Available";
                            FromCloud.cloudName = "Not_Available";
                            FromCloud.userDisplayName = "Not Available";
                        }
                        $.each(moveStatus, function (i, code) {
                            if (code.key == move.processStatus) {
                                move.processStatus = code.value;
                            }
                        });
                        if (totalFiles == 0) {
                            /*$('#moveReportsForUser').append('<div style="width: 100%;padding:5px;height: 35px;border-bottom:1px solid #ccc;" ' +
                                'id="' + move.id + '"><div class="pull-left" style="height: 30px;width: 25%;padding-left:25px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis"><span>'
                                + move.fileName + '</span></div><div style="height: 30px;width:46%;" class="pull-left"><b style="height: 30px; width:50%;float: left">' +
                                '<i class="Driveicon pull-left" id="' + FromCloud.cloudName + '"></i><span style="display: block;margin-top:5px">' + FromCloud.userDisplayName + '</span></b>' +
                                '<b style="height: 30px;width:50%;float: left"><i class="Driveicon pull-left" id="' + ToCloud.cloudName + '"></i><span style="display:block;margin-top:5px">'
                                + ToCloud.userDisplayName + '</span></b></div><div class="pull-left" style="height: 30px;width: 10%"><span class="' + colorClass + '">'
                                + move.processStatus + '</span></div><div style="float: left;width:auto;">' + jQuery.timeago(move.createdTime) + '</div>' +
                                '<div style="float: right;width:auto;">Not Available</div><i style="float:right;margin-right:10px;color:red" title="' + _ertitle + '" ' +
                                'class="' + _erClass + '"></i></div>');*/
                            $('#moveReportsForUser').append('<tr id="' + move.id + '" style="font-size: 14px;height: 40px;border-bottom:1px solid #ccc">'+
                                '<td mid="' + move.id + '" id="getMoveDetails"><i class="cf-plus5disabled" style="font-size: 10px;margin-left: 7px;cursor: pointer;color: rgba(51,51,51,0.65);"></i></td>'+
                                '<td>'+ CFManageCloudAccountsAjaxCall.getMaxChars(move.fileName,35)  +'</td>'+
                                '<td><i class="Driveicon pull-left" id="' + FromCloud.cloudName + '"></i><span style="display: block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(FromCloud.userDisplayName,25) + '</span></td>'+
                                '<td><i class="Driveicon pull-left" id="' + ToCloud.cloudName + '"></i><span style="display:block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(ToCloud.userDisplayName,25) + '</span></td>'+
                                '<td><span class="' + colorClass + '">' + move.processStatus + '</span></td>'+
                                '<td style="padding-left: 15px;">' + jQuery.timeago(move.createdTime) + '</td>'+
                                '<td></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_totfiles" style="text-align: center"></td><td id="_movefiles" style="text-align: center"></td><td id="_movefolders" style="text-align: right"></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_prcsdCnt" style="text-align: center"></td><td id="_wrngCnt" style="text-align: right"></td><td></td><td id="_errCnt"></td></tr>'+
                                '<tr class="hide-report2" style="display:none"><td colspan="5"><table class="table" style="margin-left:10%"><thead><tr><th>File Name</th><th style="padding-left: 13px">Status</th><th>Size</th><th style="padding-left: 0px">Created Date</th></tr></thead>'+
                                '<tbody id="moveReportFilesList"></tbody><tfoot><tr style="border:none !important;"><th colspan="3"><div class="paginations" style="display: flex;justify-content: center">'+
                                '<ul id="moveFiles_pagination" class="pagination-sm users_footer" style="margin:0;display: flex;justify-content: center"></ul></div></th></tr></tfoot></table></td></tr>');

                        }
                        else if( move.processStatus == "Suspended")
                        {
                            /*$('#moveReportsForUser').append('<div style="width: 100%;padding:5px;height: 35px;border-bottom:1px solid #ccc;" ' +
                                'id="' + move.id + '"><div class="pull-left" style="height: 30px;width: 25%;padding-left:25px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis"><span>'
                                + move.fileName + '</span></div><div style="height: 30px;width:46%;" class="pull-left"><b style="height: 30px; width:50%;float: left">' +
                                '<i class="Driveicon pull-left" id="' + FromCloud.cloudName + '"></i><span style="display: block;margin-top:5px">' + FromCloud.userDisplayName + '</span></b>' +
                                '<b style="height: 30px;width:50%;float: left"><i class="Driveicon pull-left" id="' + ToCloud.cloudName + '"></i><span style="display:block;margin-top:5px">'
                                + ToCloud.userDisplayName + '</span></b></div><div class="pull-left" style="height: 30px;width: 10%"><span class="' + colorClass + '">'
                                + move.processStatus + '</span></div><div style="float: left;width:auto;">' + jQuery.timeago(move.createdTime) + '</div>' +
                                '<div style="float: right;width:6%;cursor: pointer" class="button blue mini" mid="' + move.id + '" id="getMoveDetails">Details</div>' +
                                '<i style="float:right;margin-right:10px;color:red" title="' + _ertitle + '" class="' + _erClass + '"></i></div>');*/
                            $('#moveReportsForUser').append('<tr id="' + move.id + '" style="font-size: 14px;height: 40px;border-bottom:1px solid #ccc">'+
                                '<td mid="' + move.id + '" id="getMoveDetails"><i class="cf-plus5" style="font-size: 10px;margin-left: 7px;cursor: pointer;"></i></td>'+
                                '<td>'+ CFManageCloudAccountsAjaxCall.getMaxChars(move.fileName,35)  +'</td>'+
                                '<td><i class="Driveicon pull-left" id="' + FromCloud.cloudName + '"></i><span style="display: block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(FromCloud.userDisplayName,25) + '</span></td>'+
                                '<td><i class="Driveicon pull-left" id="' + ToCloud.cloudName + '"></i><span style="display:block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(ToCloud.userDisplayName,25) + '</span></td>'+
                                '<td><span class="' + colorClass + '">' + move.processStatus + '</span></td>'+
                                '<td style="padding-left: 15px;">' + jQuery.timeago(move.createdTime) + '</td><td></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_totfiles" style="text-align: center"></td><td id="_movefiles" style="text-align: center"></td><td id="_movefolders" style="text-align: right"></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_prcsdCnt" style="text-align: center"></td><td id="_wrngCnt" style="text-align: right"></td><td></td><td id="_errCnt"></td></tr>'+
                                '<tr class="hide-report2" style="display:none"><td colspan="5"><table class="table" style="margin-left:10%"><thead><tr><th>File Name</th><th style="padding-left: 13px">Status</th><th>Size</th><th style="padding-left: 0px">Created Date</th></tr></thead>'+
                                '<tbody id="moveReportFilesList"></tbody><tfoot><tr style="border:none !important;"><th colspan="3"><div class="paginations" style="display: flex;justify-content: center">'+
                                '<ul id="moveFiles_pagination" class="pagination-sm users_footer" style="margin:0;display: flex;justify-content: center"></ul></div></th></tr></tfoot></table></td></tr>');
                        }
                        else {
                            /*$('#moveReportsForUser').append('<div style="width: 100%;padding:5px;height: 35px;border-bottom:1px solid #ccc;" ' +
                                'id="' + move.id + '"><div class="pull-left" style="height: 30px;width: 25%;padding-left:25px;overflow: hidden;white-space: nowrap;text-overflow: ellipsis">' +
                                '<span>' + CFManageCloudAccountsAjaxCall.getMaxChars(move.fileName, 45) + '</span>' +
                                '</div><div style="height: 30px;width:46%;" class="pull-left"><b style="height: 30px; width:50%;float: left">' +
                                '<i class="Driveicon pull-left" id="' + FromCloud.cloudName + '"></i><span style="display: block;margin-top:5px">'
                                + FromCloud.userDisplayName + '</span></b> <b style="height: 30px;width:50%;float: left"><i class="Driveicon pull-left" id="'
                                + ToCloud.cloudName + '"></i><span style="display:block;margin-top:5px">' + ToCloud.userDisplayName + '</span></b></div>' +
                                '<div class="pull-left" style="height: 30px;width: 10%"><span class="' + colorClass + '">' + move.processStatus + '</span>' +
                                '</div><div style="float: left;width:auto;">' + jQuery.timeago(move.createdTime) + '</div><div style="float: right;width:auto;">' +
                                'Not Available</div><i style="float:right;margin-right:10px;color:red" title="' + _ertitle + '" class="' + _erClass + '"></i></div>');*/
                            $('#moveReportsForUser').append('<tr id="' + move.id + '" style="font-size: 14px;height: 40px;border-bottom:1px solid #ccc">'+
                                '<td mid="' + move.id + '" id="getMoveDetails"><i class="cf-plus5" style="font-size: 10px;margin-left: 7px;cursor: pointer;"></i></td>'+
                                '<td>'+ CFManageCloudAccountsAjaxCall.getMaxChars(move.fileName,35)  +'</td>'+
                                '<td><i class="Driveicon pull-left" id="' + FromCloud.cloudName + '"></i><span style="display: block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(FromCloud.userDisplayName,25) + '</span></td>'+
                                '<td><i class="Driveicon pull-left" id="' + ToCloud.cloudName + '"></i><span style="display:block;">' + CFManageCloudAccountsAjaxCall.getMaxChars(ToCloud.userDisplayName,25) + '</span></td>'+
                                '<td><span class="' + colorClass + '">' + move.processStatus + '</span></td>'+
                                '<td style="padding-left: 15px;">' + jQuery.timeago(move.createdTime) + '</td>'+
                                '<td></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_totfiles" style="text-align: center"></td><td id="_movefiles" style="text-align: center"></td><td id="_movefolders" style="text-align: right"></td></tr>'+
                                '<tr class="hide-report1" style="display: none;font-weight: bold;font-size: 14px;height:40px"><td></td><td id="_prcsdCnt" style="text-align: center"></td><td id="_wrngCnt" style="text-align: right"></td><td></td><td id="_errCnt"></td></tr>'+
                                '<tr class="hide-report2" style="display:none"><td colspan="5"><table class="table" style="margin-left:10%"><thead><tr><th>File Name</th><th style="padding-left: 13px">Status</th><th>Size</th><th style="padding-left: 0px">Created Date</th></tr></thead>'+
                                '<tbody id="moveReportFilesList"></tbody><tfoot><tr style="border:none !important;"><th colspan="3"><div class="paginations" style="display: flex;justify-content: center">'+
                                '<ul id="moveFiles_pagination" class="pagination-sm users_footer" style="margin:0;display: flex;justify-content: center"></ul></div></th></tr></tfoot></table></td></tr>');

                        }
                    }
                });
                setTimeout(function () {
                    $('#showMoreReports').text('Show more reports.');
                    $('#spinner2').hide();
                }, 1000);
            }
        });
    },
    copyToAjaxCall: function (fromfid, fromcid, tofid, tocid, len) {
        if (tofid == 0) {
            tofid = tocid;
        }
        var jsonArray = {
            "fromRootId": fromfid,
            "toRootId": tofid,
            "type": "MOVE_WORKSPACE",
            "fromCloudId": {
                "id": fromcid
            },
            "toCloudId": {
                "id": tocid
            },
            "fromCloudSpace": null,
            "toCloudSpace": null,
            "validSpace": true,
            "errorDescription": null,
            "totalFolders": null,
            "totalFiles": null,
            "moveFoldersStatus": false,
            "totalFilesAndFolders": 0,
            "userEmails": null,
            "threadBy": null,
            "retry": 0,
            "useEncryptKey": false,
            "notify": true,
            "fileMove": false,
            "success": false
        };
        var MoveToJson = JSON.stringify(jsonArray);
        $.ajax({
            type: 'POST',
            url: apicallurl + "/move/clouds/files?isCopy=true",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            data: MoveToJson,
            dataType: 'json',
            success: function (move) {
                var moveId = move.id;
                var moveProgress = move.processStatus;
                switch (moveProgress) {
                    case "PROCESSED"        :
                        break;

                    case "IN_PROGRESS"        :
                        CFManageCloudAccountsAjaxCall.getStatus(moveId, len);
                        break;

                    case "NOT_PROCESSED":
                        CFManageCloudAccountsAjaxCall.getStatus(moveId, len);
                        break;

                    case "SUSPENDED":
                        CFManageCloudAccountsAjaxCall.getStatus(moveId, len);
                        break;
                    case "PROCESSED_WITH_SOME_ERRORS":
                        CFManageCloudAccountsAjaxCall.getStatus(moveId, len);
                        break;
                    case "PROCESSED_WITH_SOME_WARNINGS":
                        CFManageCloudAccountsAjaxCall.getStatus(moveId, len);
                        break;

                    case "ERROR"                :
                        break;
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    unCheckFile();
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getAllRootFoldersAndFiles: function (idofCloud, PageNumber) {
        $("#refreshcloudinmove").hide();
        var apiUrl;
        if (HomePageOnload == "Home") {
            $('#breadCrumbdync').empty();
            appendBreadCrumb("Home", "Home", "Home");
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=" + urlParameterObject.isAllFiles;
        }
        if (PageName == "Folders") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=" + urlParameterObject.isAllFiles;
        }
        else if (PageName == "CloudDrive") {
            if (previousPage == 'innermove' && PageName == 'CloudDrive') {
                if (moveCheckSum == 'source') {
                    apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + idofCloud + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=true";
                }
                else {
                    apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + idofCloud + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=false";
                }
                PageName = previousPage;
                previousPage = "";
            } else {
                apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + idofCloud + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=" + urlParameterObject.isAllFiles;
            }
        }
        else if (PageName == "Home") {
            $("#breadCrumbdync").empty();
            $("#breadCrumbdync").append('<li id="Home" class="BCRFList" cloudId="Home" fileId="All Files"><a href="#" style="color:blue;text-decoration: underline;cursor:default;">Files</a><span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a href="#"> All Files</a></li>');
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=" + urlParameterObject.isAllFiles;
        }
        else if (PageName == "InnerFolders") {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=" + urlParameterObject.isAllFiles;
            $('#LVContent .panel-data[name="back"]').remove();
        }
        else if (PageName == 'move') {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + idofCloud + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=true";
        }
        else if (PageName == 'innermove') {
            apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + SingleCloudId + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=" + SinglePId + "&isAllFiles=true";
        }
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (RootFoldersandFiles) {
                var fileName = ['All Files', 'My Drive', 'SkyDrive', '', 'My Files & Folders', 'cloudian', ""];
                /*if(isProd){
                 fileName.push('CloudFuze');
                 }else{
                 fileName.push('Cloudfuze Dev');
                 }*/
                var type = '';
                if (PageName == 'move' || PageName == 'innermove') {
                    $('#spinner2').hide();
                    if (RootFoldersandFiles.length != 0 && RootFoldersandFiles[0].parentFileRef == null) {
                        var _a = ["AMAZON", "SALES_FORCE", "DOCUMENTUM", "CLOUDIAN", "CENTURYLINK"];

                        if ($.inArray(RootFoldersandFiles[0].cloudName, _a) > -1) {
                            if (moveCheckSum == 'dest') {
                                $("#move-header").find("#dynamicDestCloudName").attr('fid', RootFoldersandFiles[0].parent);
                            }
                        }
                        type = 'getClouds';
                    }
                    else if (RootFoldersandFiles.length == 0) {
                        var fileDetails = CFHPlistview.getFileDetails(SinglePId);
                        if (fileDetails == undefined || fileDetails == null ||  fileDetails == "") {
                            if (moveCheckSum == 'source') {
                                var _srcShowMore = $("#moveSource").siblings("#movePageShowMore");
                                _srcShowMore.hide();
                                _srcShowMore.attr('movepagenumber', '-1');
								if(SinglePId == 0)
								{
									$("#moveSource").html('');
                                    $("#moveSource").append('<div data-type="sourceback" style="width: 50%;cursor:pointer;height:30px" name="' + name + '" id="getClouds" cid="' + SingleCloudId + '"><span style="width:20%;float: left;display: block;padding-left:10px;font-size: 30px;margin-left: 60px;" class="cf-back"></span></div>')
                                    CFManageCloudAccountsAjaxCall.getAllFileNames(RootFoldersandFiles, PageNumber);
								} 
                            }
                            if (moveCheckSum == 'dest') {
                                var _destShowMore = $("#moveDestination").siblings("#movePageShowMore");
                                _destShowMore.hide();
                                _destShowMore.attr('movepagenumber', '-1')
								if(SinglePId == 0)
								{
								   $("#moveDestination").html('');
                                   $("#moveDestination").append('<div data-type="destback" style="width: 100%;cursor:pointer;height:30px" name="' + name + '" id="getClouds" cid="' + SingleCloudId + '"><span style="width:20%;float: left;display: block;padding-left:10px;font-size: 30px;margin-left: 60px;" class="cf-back"></span></div>')
                                   CFManageCloudAccountsAjaxCall.getAllFileNames(RootFoldersandFiles, PageNumber);
								}
                            }
                            return false;
                        }
                        if (moveCheckSum == 'dest') {
                            $("#move-header").find("#dynamicDestCloudName").attr('fid', fileDetails.id);
                        }
                        if ($.inArray(fileDetails.objectName, fileName) > -1) {
                            type = 'getClouds';
                        }
                        else {
                            type = fileDetails.parent;
                        }
                    }
                    else {
                        var type1 = RootFoldersandFiles[0].parentFileRef;
                        if ($.inArray(type1.objectName, fileName) > -1 && type1.parentFileRef == null) {
                            type = 'getClouds';
                            if (moveCheckSum == 'dest') {
                                $('#dynamicDestCloudName').attr('check', 'droot');
                                $("#move-header").find("#dynamicDestCloudName").attr('fid', type1.id);
                            }
                        }
                        else if (moveCheckSum == 'dest') {
                            type = type1.parent;
                            $("#move-header").find("#dynamicDestCloudName").attr('fid', type1.id);
                        }
                        else if (moveCheckSum == 'source') {
                            type = type1.parent;
                        }
                    }
                    if (moveCheckSum == 'source' && PageNumber == 1) {
                        $("#moveSource").html('');
                        $("#moveSource").append('<div data-type="sourceback" style="width: 50%;cursor:pointer;height:30px" name="' + name + '" id="' + type + '" cid="' + SingleCloudId + '"><span style="width:20%;float: left;display: block;padding-left:10px;font-size: 30px;margin-left: 60px;" class="cf-back"></span></div>')
                    }
                    if (moveCheckSum == 'dest' && PageNumber == 1) {
                        $("#moveDestination").html('');
                        $("#moveDestination").append('<div data-type="destback" style="width: 100%;cursor:pointer;height:30px" name="' + name + '" id="' + type + '" cid="' + SingleCloudId + '"><span style="width:20%;float: left;display: block;padding-left:10px;font-size: 30px;margin-left: 60px;" class="cf-back"></span></div>')
                    }
                }
                CFManageCloudAccountsAjaxCall.getAllFileNames(RootFoldersandFiles, PageNumber);

                if (PageName != 'move') {
                    if (RootFoldersandFiles.length == 0) {
                        InnerCloudId = [];
                        InnerFolderId = [];
                        setTimeout(function () {
                            var _brcrdyncFchild;

                            if (moveCheckSum == 'source') {
                                _brcrdyncFchild = $('#breadCrumbdyncmove > :nth-child(2)');
                            } else {
                                _brcrdyncFchild = $('#breadCrumbdyncmovedest > :nth-child(2)');
                            }

                            _brcrdyncFchild.nextAll().remove();
                            //var fileObj = CFHPlistview.getFileDetails(fid);
                            var fileObject = CFHPlistview.getFileDetails(SinglePId);
                            var fileCount = 0;
                            do {
                                //var fileName = ['All Files', 'My Drive', 'SkyDrive', "", 'My Files & Folders', 'cloudian'];
                                if (fileObject != undefined && fileObject != null) {
                                    if (fileObject.directory == true) {
                                        InnerCloudId.push(fileObject.cloudId);
                                        InnerFolderId.push(fileObject.id);
                                    }
                                    if (fileObject.parentFileRef != undefined || fileObject.parentFileRef != null) {
                                        if ($.inArray(fileObject.objectName, fileName) < 0) {
                                            _brcrdyncFchild.after('<li style="cursor:pointer;" id="' + fileObject.id + '"' +
                                                'class="BCRFList" cloudid="' + fileObject.cloudId + '">' +
                                                '<span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a style="cursor:pointer;">' + fileObject.objectName + '</a></li>');
                                            fileObject = fileObject.parentFileRef;
                                            fileCount = fileCount + 1;
                                        }
                                    }
                                    else if ($.inArray(fileObject.objectName, fileName) < 0) {
                                        _brcrdyncFchild.after('<li style="cursor:pointer;" id="' + fileObject.id + '"' +
                                            'class="BCRFList" cloudid="' + fileObject.cloudId + '">' +
                                            '<span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a style="cursor:pointer">' + fileObject.objectName + '</a></li>');
                                        break;
                                    }
                                    if ($.inArray(fileObject.objectName, fileName) > -1) {
                                        fileCount = 5;
                                    }
                                }else{
                                    fileCount = 5;
                                }
                            } while (fileCount < 5);
                        }, 500);
                    }
                    else {
                        var _brcrdyncFchild;//$("#breadCrumbdyncmove").find("li:first-child");

                        if (moveCheckSum == 'source') {
                            _brcrdyncFchild = $('#breadCrumbdyncmove > :nth-child(2)');
                        } else {
                            _brcrdyncFchild = $('#breadCrumbdyncmovedest > :nth-child(2)');
                        }
                        _brcrdyncFchild.nextAll().remove();
                        var fileObject = RootFoldersandFiles;
                        var fileCount = 0;
                        var test = [];
                        InnerCloudId = [];
                        InnerFolderId = [];
                        do {
                            test = [];
                            if (fileObject[0].parentFileRef != undefined || fileObject[0].parentFileRef != null) {
                                if (fileCount > 0) {
                                    if (fileObject[0].directory == true) {
                                        InnerCloudId.push(fileObject[0].cloudId);
                                        InnerFolderId.push(fileObject[0].id);
                                    }
                                    if ($.inArray(fileObject.objectName, fileName) < 0) {
                                        _brcrdyncFchild.after('<li style="cursor:pointer;" id="' + fileObject[0].id + '" class="BCRFList" cloudid="' + fileObject[0].cloudId + '"><span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a style="cursor:pointer;">' + fileObject[0].objectName + '</a></li>');
                                    }
                                    if ($.inArray(fileObject.objectName, fileName) > 0) {
                                        fileCount = 5;
                                    }
                                }
                                fileObject = fileObject[0].parentFileRef;
                                test.push(fileObject);
                                fileObject = test;
                                fileCount = fileCount + 1;
                            } else {
                                fileCount = fileCount + 1;
                                if (fileObject[0].directory == true) {
                                    InnerCloudId.push(fileObject[0].cloudId);
                                    InnerFolderId.push(fileObject[0].id);
                                    if ($.inArray(fileObject[0].objectName, fileName) > 0) {
                                        fileCount = 5;
                                    }
                                }
                                if ($.inArray(fileObject[0].objectName, fileName) < 0) {
                                    _brcrdyncFchild.after('<li style="cursor:pointer;" id="' + fileObject[0].id + '" class="BCRFList" cloudid="' + fileObject[0].cloudId + '"><span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a style="cursor:pointer;">' + fileObject[0].objectName + '</a></li>');
                                    break;
                                }
                                if ($.inArray(fileObject[0].objectName, fileName) > -1) {
                                    fileCount = 5;
                                }
                            }
                        } while (fileCount < 5);
                    }
                    setTimeout(function () {
                        var _brCrdync;
                        if (moveCheckSum == 'source') {
                            _brCrdync = $("#breadCrumbdyncmove");
                        } else {
                            _brCrdync = $("#breadCrumbdyncmovedest");
                        }
                        var liLength = _brCrdync.children('li').length;
                        var _brCrdyncChild;

                        for (var i = 0; i < liLength - 1; i++) {
                            _brCrdyncChild = _brCrdync.children("li:eq(" + i + ")").children("a");
                            _brCrdyncChild.css('color', 'blue');
                            _brCrdyncChild.css('text-decoration', 'underline');
                            _brCrdyncChild.css('cursor', 'pointer');
                        }
                        var lastFol = liLength - 1;
                        _brCrdync.children('li:eq(' + lastFol + ')').children('a').css('cursor', 'default');
                    }, 520);
                }
                if(RootFoldersandFiles[0].cloudName == "SHAREPOINT_ONLINE")
                {
                    $('#CFUploadFiles').parents(".fm").addClass('buttonDisable');
                }

            },
            complete: function (xhr) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getAllHomeFolders: function (PageNumber) {
        $.ajax({
            type: "GET",
            url: apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=" + urlParameterObject.isAllFiles,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (Folders) {
                CFManageCloudAccountsAjaxCall.getAllFileNames(Folders, PageNumber);
            },
            complete: function (xhr) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: 2000});
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                    $('#spinner1').addClass('divdisplay');
                }
            }
        });
    },
    gotoInnerFolderandFiles: function (cid, fid, pgn, fileshareUrl, sharedFolderId, sharedPassword) {
        var apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/" + cid + "?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + pgn + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=" + encodeURIComponent(fid) + "&isAllFiles=" + urlParameterObject.isAllFiles;
        if (sharedFolderId != undefined) {
            apiUrl = apiUrl + "&sharedFolderId=" + sharedFolderId;
        }
        if (sharedPassword != undefined) {
            apiUrl = apiUrl + "&sharePassword=" + sharedPassword;
        }
        var _share = $('#sharePasswordModel'),
            breadCrumb = $("#breadCrumbdync");
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (gotoInnerFolderandFiles) {
                _share.modal('hide');
                _share.find('input').val('');
                CFManageCloudAccountsAjaxCall.getAllFileNames(gotoInnerFolderandFiles, pgn, fileshareUrl, sharedFolderId);
                var fileName = ['All Files', 'My Drive', 'SkyDrive', '', 'My Files & Folders', 'cloudian', ""];
                /*if(isProd){
                 fileName.push('Cloudfuze');
                 }else{
                 fileName.push('Cloudfuze Dev');
                 }*/
                var fileObj;
                if (previousPage == "Share with Me") {
                    fileObj = CFHPlistview.getFileDetails(SinglePId, cid);
                    if (fileObj.id != undefined && fileObj.id != sharedFolderId && fileshareUrl != "back") {
                        breadCrumb.append('<li style="cursor:pointer;" id="' + fileObj.id + '" class="BCRFList" cloudid="' + fileObj.cloudId + '">' +
                            '<span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span>' +
                            '<a style="cursor:pointer;">' + fileObj.objectName + '</a></li>');
                    }
                    else if (fileObj.parentFileRef == null && fileshareUrl == "forword") {
                        breadCrumb.append('<li style="cursor:pointer;" id="' + fileObj.id + '" class="BCRFList" cloudid="' + fileObj.cloudId + '">' +
                            '<span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span>' +
                            '<a style="cursor:pointer;">' + fileObj.objectName + '</a></li>');
                    }
                    else if (fileObj.parentFileRef != null && fileshareUrl == "forword") {
                        breadCrumb.append('<li style="cursor:pointer;" id="' + fileObj.id + '" class="BCRFList" cloudid="' + fileObj.cloudId + '">' +
                            '<span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span>' +
                            '<a style="cursor:pointer;">' + fileObj.objectName + '</a></li>');
                    }
                }
                else if (previousPage == "Share by Me") {
                    fileObj = CFHPlistview.getFileDetails(SinglePId, cid);
                    if (fileshareUrl != "back") {
                        breadCrumb.append('<li style="cursor:pointer;" id="' + fileObj.id + '" class="BCRFList" cloudid="' + fileObj.cloudId + '">' +
                            '<span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span>' +
                            '<a style="cursor:pointer;">' + fileObj.objectName + '</a></li>');
                    }
                }
                else if (gotoInnerFolderandFiles.length == 0) {
                    InnerCloudId = [];
                    InnerFolderId = [];
                    setTimeout(function () {
                        var _brcrdyncFchild = breadCrumb.find("li:first-child");
                        _brcrdyncFchild.nextAll().remove();
                        //var fileObj = CFHPlistview.getFileDetails(fid);
                        var fileObject = CFHPlistview.getFileDetails(fid);
                        var fileCount = 0;
                        do {
                            //var fileName = ['All Files', 'My Drive', 'SkyDrive', "", 'My Files & Folders', 'cloudian'];
                            if (fileObject != undefined || fileObject != null) {
                                if (fileObject.directory == true) {
                                    InnerCloudId.push(fileObject.cloudId);
                                    InnerFolderId.push(fileObject.id);
                                }
                                if (fileObject.parentFileRef != undefined || fileObject.parentFileRef != null) {
                                    if ($.inArray(fileObject.objectName, fileName) < 0) {
                                        _brcrdyncFchild.after('<li style="cursor:pointer;" id="' + fileObject.id + '"' +
                                            'class="BCRFList" cloudid="' + fileObject.cloudId + '">' +
                                            '<span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a style="cursor:pointer;">' + fileObject.objectName + '</a></li>');
                                        fileObject = fileObject.parentFileRef;
                                        fileCount = fileCount + 1;
                                    }
                                }
                                else if ($.inArray(fileObject.objectName, fileName) < 0) {
                                    _brcrdyncFchild.after('<li style="cursor:pointer;" id="' + fileObject.id + '"' +
                                        'class="BCRFList" cloudid="' + fileObject.cloudId + '">' +
                                        '<span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a style="cursor:pointer">' + fileObject.objectName + '</a></li>');
                                    break;
                                }
                                if ($.inArray(fileObject.objectName, fileName) > -1) {
                                    fileCount = 5;
                                }
                            }
                            else{
                                fileCount = 5;
                            }
                        } while (fileCount < 5);
                    }, 500);
                }
                else {
                    var _brcrdyncFchild = $("#breadCrumbdync").find("li:first-child");
                    _brcrdyncFchild.nextAll().remove();
                    var fileObject = gotoInnerFolderandFiles;
                    var fileCount = 0;
                    var test = [];
                    InnerCloudId = [];
                    InnerFolderId = [];
                    do {
                        test = [];
                        if (fileObject[0].parentFileRef != undefined || fileObject[0].parentFileRef != null) {
                            if (fileCount > 0) {
                                if (fileObject[0].directory == true) {
                                    InnerCloudId.push(fileObject[0].cloudId);
                                    InnerFolderId.push(fileObject[0].id);
                                }
                                if ($.inArray(fileObject.objectName, fileName) < 0) {
                                    _brcrdyncFchild.after('<li style="cursor:pointer;" id="' + fileObject[0].id + '" class="BCRFList" cloudid="' + fileObject[0].cloudId + '"><span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a style="cursor:pointer;">' + fileObject[0].objectName + '</a></li>');
                                }
                                if ($.inArray(fileObject.objectName, fileName) > 0) {
                                    fileCount = 5;
                                }
                            }
                            fileObject = fileObject[0].parentFileRef;
                            test.push(fileObject);
                            fileObject = test;
                            fileCount = fileCount + 1;
                        } else {
                            fileCount = fileCount + 1;
                            if (fileObject[0].directory == true) {
                                InnerCloudId.push(fileObject[0].cloudId);
                                InnerFolderId.push(fileObject[0].id);
                                if ($.inArray(fileObject[0].objectName, fileName) > 0) {
                                    fileCount = 5;
                                }
                            }
                            if ($.inArray(fileObject[0].objectName, fileName) < 0) {
                                _brcrdyncFchild.after('<li style="cursor:pointer;" id="' + fileObject[0].id + '" class="BCRFList" cloudid="' + fileObject[0].cloudId + '"><span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a style="cursor:pointer;">' + fileObject[0].objectName + '</a></li>');
                                break;
                            }
                            if ($.inArray(fileObject[0].objectName, fileName) > -1) {
                                fileCount = 5;
                            }
                        }
                    } while (fileCount < 5);
                }
                setTimeout(function () {
                    var _brCrdync = $("#breadCrumbdync");
                    var liLength = _brCrdync.children('li').length;
                    var _brCrdyncChild;
                    for (var i = 0; i < liLength - 1; i++) {
                        _brCrdyncChild = _brCrdync.children("li:eq(" + i + ")").children("a");
                        _brCrdyncChild.css('color', 'blue');
                        _brCrdyncChild.css('text-decoration', 'underline');
                        _brCrdyncChild.css('cursor', 'pointer');
                    }
                    var lastFol = liLength - 1;
                    _brCrdync.children('li:eq(' + lastFol + ')').children('a').css('cursor', 'default');
                }, 520);
                var test = $("#CloudDriveList").find(".catactive").closest(".clsubmenu").attr('id');
                var array = ["EGNYTE_STORAGE"];
                if ($.inArray(test, array) > -1) {
                    var testId = fid.split('/');
                    var len = testId.length;
                    if (testId[2] == "Private") {
                        $('#CFUploadFiles').parent().addClass('buttonDisable');
                    }
                    else if (testId.length == 3 && testId[2] == "Shared") {
                        $('#CFUploadFiles').parent().addClass('buttonDisable');
                    }
                }
                if(test == "SHAREPOINT_ONLINE")
                {
                    if(PageName == "SharepointSite")
                        $('#CFUploadFiles').parents(".fm").addClass('buttonDisable');
                    else
                        $('#CFUploadFiles').parents(".fm").removeClass('buttonDisable');

                }
            },
            complete: function (xhr, statusText) {
                $('#' + viewTrack + '').trigger('click');
                $('#CFSharePwdButton').removeClass('disable');
                if (xhr.status == 401) {
                    _share.find("input").val('');
                    _share.modal("show");
                    _share.find('.button.blue').removeClass('buttonDisable');
                    setTimeout(function () {
                        _share.find("input").focus();
                    }, 2);
                } else if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getRecentFilesandFolders: function (PageNumber) {
        var apiUrl = apicallurl + '/filefolder/user/' + CFManageCloudAccountsAjaxCall.getUserId() + '/cloud/all?page_size=50&fetchCollabInfo=' + urlParameterObject.fetchCollabInfo + '&isAscen=' + urlParameterObject.isAscen + '&orderField=' + urlParameterObject.orderField + '&page_nbr=' + PageNumber + '';
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (RecentFiles) {
                CFManageCloudAccountsAjaxCall.getAllFileNames(RecentFiles, PageNumber);
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    createFolder: function (SingleCloudId, SinglePId, FolderName) {
        var _ab = false;
        //var renameReg = /(%)/;
        //if (renameReg.test(FolderName)) {
        //    $('#creating').addClass('divdisplay');
        //    $.smallBox({
        //        title: "% is not allowed in folder name.",
        //        color: "#e35e00",
        //        timeout: notifyTime,
        //        sound: false
        //    });
        //showNotyNotification("error","% is not allowed in folder name.");
        //    return false;
        //}
        // var renameReg = /[+]/;
        // if (renameReg.test(FolderName)) {
        //    $('#creating').addClass('divdisplay');
        //    $.smallBox({
        //        title: "+ is not allowed in folder name.",
        //        color: "#e35e00",
        //        timeout: notifyTime,
        //        sound: false
        //    });
        //showNotyNotification("error","% is not allowed in folder name.");
        //    return false;
        // }
        var sid;
        if (previousPage == "Share with Me") {
            sid = encodeURIComponent($('#breadCrumbdync').children('li:eq(1)').attr('id'));
        }
        else if (previousPage == "Share by Me") {
            sid = encodeURIComponent($('#breadCrumbdync').children('li:eq(1)').attr('id'));
        }
        if (sid == undefined || sid == "undefined") {
            sid = "";
        }
        var apiUrl = apicallurl + "/filefolder/create/folder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "?sharedFolderId=" + sid;
        var cloudId = SingleCloudId;
        var parentId = SinglePId;
        var checkboxName = "isSecure";
        var checkboxEncrypt = false;
        var boundary = '-----WebKitFormBoundary' + Math.floor(Math.random() * 32768) + Math.floor(Math.random() * 32768) + Math.floor(Math.random() * 32768);
        var parentname = "parentId";
        var NewName = FolderName;
        var foldername = "newName";
        var cloudName = "cloudId";
        var data = contactus_formfield(boundary, parentname, parentId);
        data += contactus_formfield(boundary, foldername, encodeURIComponent(NewName));
        data += contactus_formfield(boundary, cloudName, cloudId);
        data += contactus_formfield(boundary, checkboxName, checkboxEncrypt);
        data += boundary;
        function contactus_formfield(boundary, name, value) {
            var text = "";
            text += '--' + boundary + '\r\n' + 'Content-Disposition: form-data; name="';
            text += name;
            text += '"\r\n\r\n';
            text += value;
            text += '\r\n';
            return text;
        }

        $.ajax({
            type: "PUT",
            url: apiUrl,
            data: data,
            async: false,
            headers: {
                "Content-Type": "multipart/form-data",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (CreateFolder) {
                var objectName = CreateFolder.objectName;
                var objectSize = CreateFolder.objectSize;
                var cloudName = CreateFolder.cloudName;
                var cloudId = CreateFolder.cloudId;
                var fileId = CreateFolder.id;
                var parent = CreateFolder.parent;
                var fileExten = CreateFolder.objectExtn;
                var filetype1 = CreateFolder.directory;
                var filetype;
                if (filetype1 == false) {
                    filetype = "FILE"
                }
                if (filetype1 == true) {
                    filetype = "FOLDER"
                }
                var favourite = CreateFolder.favourite;
                var favouriteicon;
                if (favourite == true) {
                    favouriteicon = heartFill;
                } else if (favourite == false) {
                    favouriteicon = heart;
                }
                var userType = FilePer[0];
                var fileIcon = CFManageCloudAccountsAjaxCall.getFileIcon(fileExten, filetype);
                var dateCreated = CFManageCloudAccountsAjaxCall.getDateConversion(CreateFolder.createdTime);
                var dateModified = CFManageCloudAccountsAjaxCall.getDateConversion(CreateFolder.modifiedTime);
                if (PageName == "InnerFolders" || PageName == "CloudDrive") {
                    $("#ThumbnailContent").prepend('<div class="file ' + fileIcon + '" id="' + cloudId + '" style="cursor:pointer;" data-type=' + filetype + ' ><i title="' + objectName + '" class="filethumbnail" name="FOLDER"></i><strong class="filename" id="' + fileId + '" fexten="' + fileExten + '">' + CFManageCloudAccountsAjaxCall.getMaxChars(objectName,14) + '</strong><div class="filesize">' + CFManageCloudAccountsAjaxCall.getObjectSize(objectSize, filetype) + '</div><input type="checkbox" class="fileCheck" /><a href="#" id="' + fileId + '"><!--i class="MetaDataIcon"></i--></a><i id="ThFav" class="' + favouriteicon + '" style="cursor:pointer;"></i></div>');
                    $("#LVContent").find(".panel-data:first").remove();
                    $("#LVContent").prepend('<div class="panel-data" id="' + parent + '" data-type=' + filetype + '><div  class="LVcheckBox" name="' + filetype + '"><input type="checkbox"/></div><div class="LVfileName" id="' + fileId + '" style="height:20px" name="' + filetype + '"><i class="LV' + filetype + ' pull-left"></i><p class="pull-left" name="' + objectName + '" fexten="' + fileExten + '">' + CFManageCloudAccountsAjaxCall.getMaxChars(objectName,50) + '</p><a href="#" title=""><!--i class="MetaDataIcon"></i--></a></div><div class="LVFavorites"><a href="#" id="LVFavorite" class="' + favouriteicon + '"></a></div><div class="LVfileSize" style="cursor:pointer;">' + CFManageCloudAccountsAjaxCall.getObjectSize(objectSize, filetype) + '</div><div class="LVdrive" id="' + cloudId + '">' + CLName[cloudName] + '</div><div class="LVaddedDate">' + dateCreated + '</div><div class="LVmodifiedDate">' + dateModified + '</div></div>');

                    if (sharedFolderId != null && sharedFolderId != undefined)
                        var fileinfo1 = CFHPlistview.getFileShare(sharedFolderId);
                    if (previousPage == "Share with Me" && fileinfo1) {
                        $("#LVContent").children(".panel-data").attr('fileper', FilePer[0]);
                        $("#ThumbnailContent").children(".file").attr('fileper', FilePer[0]);
                    }


                } else if (PageName == "Favorites") {
                    $("#ThumbnailContent").find(".file:first").remove();
                    $("#ThumbnailContent").prepend('<div class="file ' + fileIcon + '" id="' + cloudId + '" style="cursor:pointer;" data-type=' + filetype + '><i title="' + objectName + '" class="filethumbnail" ></i><strong class="filename" id="' + fileId + '" fexten="' + fileExten + '">' + CFManageCloudAccountsAjaxCall.getMaxChars(objectName,14) + '</strong><div class="filesize">' + CFManageCloudAccountsAjaxCall.getObjectSize(objectSize, filetype) + '</div><input type="checkbox" class="fileCheck" /><a href="#" id="' + fileId + '"><!--i class="MetaDataIcon"></i--></a><i id="ThFav" class="' + heartFill + '" style="cursor:pointer;"></i></div>');
                    $("#LVContent").find(".panel-data:first").remove();
                    $("#LVContent").prepend('<div class="panel-data" id="' + parent + '" data-type=' + filetype + '><div class="LVcheckBox" name="' + filetype + '"><input type="checkbox"/></div><div class="LVfileName" id="' + fileId + '" style="height:20px" name="' + filetype + '"><i class="LV' + filetype + ' pull-left"></i><p class="pull-left" name="' + objectName + '" fexten="' + fileExten + '">' + CFManageCloudAccountsAjaxCall.getMaxChars(objectName,50) + '</p><a href="#" title=""><!--i class="MetaDataIcon"></i--></a></div><div class="LVFavorites"><a href="#" id="LVFavorite" class="' + heartFill + '"></a></div><div class="LVfileSize" style="cursor:pointer;">' + CFManageCloudAccountsAjaxCall.getObjectSize(objectSize, filetype) + '</div><div class="LVdrive" id="' + cloudId + '">' + CLName[cloudName] + '</div><div class="LVaddedDate">' + dateCreated + '</div><div class="LVmodifiedDate">' + dateModified + '</div></div>');
                    if (PageName == "Favorites") {
                        CFManageCategoryAjaxCall.addFavouriteFile(fileId);
                    }
                }
                $("#creating").addClass("divdisplay");
                $("#CFCreateFolder").prop('disabled', false);
                $("#mainContent").find(".FolderOk").remove();
                $("#mainContent").find(".FolderCancel").remove();
//				$('.LVHcheckBox input').prop("disabled",false);
                if (selectEvent != undefined && selectEvent != null) {
                    selectEvent.init();
                }
            },
            complete: function (xhr, statusText) {
                $('#creating').addClass('divdisplay');
                var text = xhr.getResponseHeader('exception');
                if (xhr.status == 406) {
                    renameObject = null;
                    if (text == "Can't be null or empty. Length must be 3 to 63 symbols..") {
                        // $.smallBox({
                        //     title: "Folder name must be between 3 to 63 characters",
                        //     color: "#1ba1e2",
                        //     timeout: notifyTime,
                        //     sound: false
                        // });
                        showNotyNotification("notify","Folder name must be between 3 to 63 characters");
                    } else {
                        //$.smallBox({title: text + ".", color: "#1ba1e2", timeout: notifyTime, sound: false});
                        showNotyNotification("notify",text);
                    }
                    $("#mainContent").find(".CFTHCreateFolder").css('border-color', 'red');
                }
                else if (xhr.status == 403) {
                    if (text != undefined || text != null || text != "") {
                        //$.smallBox({title: text, color: "#e35e00", timeout: notifyError, sound: false})
                        showNotyNotification("error",text);
                    }
                }
                else if (xhr.status == 500) {
                    if (/Folder Unique Name: The Folder API Name/i.test(text)) {
                        var _a = "Folder name must begin with a letter, not include spaces, not end with an underscore," +
                                " and not contain two consecutive underscores.";
                        //$.smallBox({title: _a, color: "#1ba1e2", timeout: notifyTime, sound: false});
                        showNotyNotification("notify",_a);
                        $("#mainContent").find(".CFTHCreateFolder").css('border-color', 'red');
                    } else if (/Create folder operation failed/i.test(text)) {
                        //$.smallBox({title: text, color: "#1ba1e2", timeout: notifyTime, sound: false});
                        showNotyNotification("notify",text);
                        $("#mainContent").find(".CFTHCreateFolder").css('border-color', 'red');
                    }
                }
                else if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                    $("#mainContent").find(".CFTHCreateFolder").css('border-color', 'red');
                } else if (xhr.status == 200) {
                    _ab = true;
                    var _x = $('#secondary').find('.active').children('a').attr('id');
                    if (_x == 'CFSharedWithMe') {
                        $("#LVContent").find('.LVFavorites').addClass('buttonDisable');
                    }
                }
            }
        });
        return _ab;
    },
    searchFiles: function (searchTerm, PageNumber) {
        var apiUrl;
        if (previousPage == "Folders" || previousPage == "WorkSpace" || previousPage == "InnerWorkSpace") {
            return false;
        } else {
            apiUrl = apicallurl + "/filefolder/search/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "?page_size=50&isAscen=false&fetchCollabInfo=true&orderField=modifiedTime&page_nbr=" + PageNumber + "&query=" + searchTerm;
        }
        $.ajax({
            url: apiUrl,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                // if(data.length < 1){
                //     return showNotyNotification('notify','No Search Results found.');
                // }else{
                //     CFManageCloudAccountsAjaxCall.getAllFileNames(data, PageNumber);
                // }

                if(data.length < 1){
                    $('#LVContent').html('');
                    $('#ThumbnailContent').html('');
                    $("#listShowMore").hide();
                   // hideCreateControls(true);
                   enableCrateControls1(true,true);


                    return showNotyNotification('notify','No Search Results found.');
                }else{
                    CFManageCloudAccountsAjaxCall.getAllFileNames(data, PageNumber);
                }
            }
        });
    },
    
    getWorkSpaceForaUser: function () {
        var apiUrl = apicallurl + "/workspace/user?isAscen=true";
        var ListViewChildCount = $('#LVContent').children('div').length;
        $("#LVContent > div").remove();
        $("#ThumbnailContent > div").remove();
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (workspace) {
                if (workspace.length == 0) {
                    $('#CFEditWorkSpace').hide();
                    $('#NoWorkspaces').show();
                } else {
                    $('#CFEditWorkSpace').show();
                    $('#NoWorkspaces').hide();
                }
                var _leftNav = $('#leftNav');
                _leftNav.html('');
                $.each(workspace, function (i, e) {
                    if (e.user != null) {
                        var workspaceName = e.workspaceName;
                        var workspaceId = e.id;
                        var type = e.type;
                        var filesCount = 0;
                        var foldersCount = 0;
                        var noofFilesLabel = "0 Folders, 0 Files";
                        var shareObjects = [];
                        var listLock;

                        if (e.files != null) {
                            $.each(e.files, function (i, sf) {
                                if (sf != null) {
                                    shareObjects.push(sf);
                                }
                            });
                            $.each(shareObjects, function (i, sobj) {
                                if (sobj != null) {
                                    if (sobj.directory == true) {
                                        foldersCount = foldersCount + 1;
                                    } else {
                                        filesCount = filesCount + 1;
                                    }
                                }
                            });

                            noofFilesLabel = foldersCount > 1 || foldersCount == 0 ? foldersCount + ' Folders, ' : foldersCount + ' Folder, ';

                            noofFilesLabel += filesCount > 1 || filesCount == 0 ? filesCount + ' Files ' : filesCount + ' File ';
                        }

                        if (e.password == "" || e.password == "NOT_REQUIRED" || e.password == "IGNORE_UPDATE") {
                            workspaceLock = "workspaceLockFalse";
                            listLock = "";
                        } else {
                            workspaceLock = "workspaceLockTrue";
                            listLock = "cf-locked";
                        }

                        var _left = '<li class="getFilesForWorkspace" title="' + workspaceName + '">' +
                                '<a href="#" id="' + workspaceId + '">' +
                                '<i class="cf-workspace" style="display: inline-block;margin:0"></i>' +
                                '<i style="font-size:13px" class="' + listLock + '"></i><span>' + workspaceName + '</span></a></li>';

                        var _thumb = '<div class="file ' + type + '" name="Workspace" style="cursor:pointer;position:relative">' +
                                '<span class="badge" style="display: none;position: absolute;right: 3px;top: 3px;background: #dc0d17;color: #fff;" ' +
                                'id="notify_file_count"></span>' +
                                '<i title="' + workspaceName + '" class="filethumbnail cf-workspace wsIcon" name="Workspace"></i>' +
                                '<strong class="filename" id="' + workspaceId + '" name="' + workspaceName + '" fexten="null">'
                                + CFManageCloudAccountsAjaxCall.getMaxChars(workspaceName,14) + '</strong>' +
                                '<div class="filesize"> ' + noofFilesLabel + '</div><input type="checkbox" class="fileCheck" />' +
                                '<p href="#" id="' + workspaceId + '"></p><i style="cursor:pointer;" id="wsLock" class="' + workspaceLock + '"></i>' +
                                '</div>';

                        _leftNav.append(_left);

                        $('#ThumbnailContent').append(_thumb);
                    }
                });
                $('#spinner1').addClass('divdisplay');
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
        if (selectEvent != undefined && selectEvent != null) {
            selectEvent.init();
        }
    },
    getSharefileUsername: function (email) {
        var apiUrl = apicallurl + "/users/all";
        $.ajax({
            type: 'GET',
            url: apiUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            dataType: 'json',
            success: function (userData) {
                if (userData != "") {
                    bl1:{
                        for (var i = 0; i < userData.length; i++) {
                            if (userData[i].primaryEmail == email) {
                                $('#shareENames').append("<i><span class='label label-default' style='float:left;'>" + userData[i].userName + "<i class='removeTag1'></i></span></i>");
                                sharemailsUnames[email] = userData[i].userName;
                                break bl1;
                            }
                        }
                        sharemailsUnames[email] = email;
                        $('#shareENames').append("<i><span class='label label-default' style='float:left;'>" + email + "<i class='removeTag1'></i></span></i>");
                    }
                    $('#shareFiles .statusMesg').text('');
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getShareUseremail: function (username) {
        var apiUrl = apicallurl + "/users/all";
        $.ajax({
            type: 'GET',
            url: apiUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            dataType: 'json',
            success: function (userData) {
                if (userData != "") {
                    bl1:{
                        for (var i = 0; i < userData.length; i++) {

                            var email = userData[i].primaryEmail;
                            var pcount = $('.addShareContent').find('#addUsersToShare').children('p').length;
                            for (var j = 0; j < pcount; j++) {
                                var currentRow = $('.addShareContent').find('#addUsersToShare').children('p:eq(' + j + ')');
                                if (email == currentRow.children('span').text()) {
                                    currentRow.html('');
                                    break bl1;
                                }
                                if (username == currentRow.children('span').text()) {
                                    currentRow.html('');
                                    break bl1;
                                }
                            }
                        }
                    }
                    $("#shareFiles").find(".statusMesg").text('');
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getFilesForaWorkSpace: function (WorkSpaceId, PageNumber) {
        var apiUrl = apicallurl + "/workspace/info/" + WorkSpaceId;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (WSInfo) {
                $('#workspaceFiles').html('');
                var ShareObjects = [];
                var Files = [];
                var filetype = '12';
                PageNumber = 1;
                if (WSInfo.shares != null) {
                    $.each(WSInfo.shares, function (i, WSShare) {
                        ShareObjects.push(WSShare);
                    });
                    $.each(ShareObjects, function (i, sobj) {
                        if (sobj.file != null) {
                            Files.push(sobj.file);
                        }
                    });
                }
                $.each(Files, function (i, file) {
                    var filetype1 = file.directory;
                    var filetype;
                    var fileClass = "";
                    if (filetype1 == false) {
                        filetype = "FIL";
                        fileClass = "sorting_1";
                    }
                    if (filetype1 == true) {
                        filetype = "FOLDER";
                        fileClass = "folder";
                    }
                    $('#workspaceFiles').append('<tr class="gradeA"><td class="wsfcheckbox" style="width:32px"><input type="checkbox" /></td><td class="' + fileClass + '" style="cursor:pointer;" name=' + filetype1 + ' cloudid=' + file.cloudId + '  id=' + file.id + ' title="' + file.objectName + '">' + file.objectName + '</td><td class=" ">' + CFManageCloudAccountsAjaxCall.getObjectSize(file.objectSize, filetype) + '</td><td class=" ">' + CFManageCloudAccountsAjaxCall.getDateConversion(file.modifiedTime) + '</td></tr>');
                });
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
        var pagename = PageName;
        if (pagename == "InnerWorkSpace" || pagename == "Share with Me") {
            $('#LVHeader').children('.LVHFavorites').hide();
            $("#mainContent").find(".LVcheckBox").find('input:checkbox').each(function () {
                $("#mainContent").find(".LVFavorites").hide();
            });
            $("#mainContent").find(".file").find('input:checkbox').each(function () {
                $("#mainContent").find(".file").find('input:checkbox').prop('checked', false);
                $(".container").find(".cf-heart32").hide();
            });
        }
    },
    deleteFile: function (fileId) {
        apiUrl = apicallurl + "/filefolder/delete?fileId=" + fileId;
        $.ajax({
            type: "DELETE",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (DeleteFile) {
            },
            complete: function (xhr, statusText) {
                if (xhr.status == 204) {
                    unCheckFile();
                    $('#LVContent .LVfileName[id="' + fileId + '"]').parent('.panel-data').remove();
                    $('#LVContent .panel-data[id="' + fileId + '"]').each(function () {
                        $(this).remove();
                    });
                    $('#ThumbnailContent strong[id="' + fileId + '"]').parent('.file').remove();
                    $('#ThumbnailContent .file[id="' + fileId + '"]').each(function () {
                        $(this).remove();
                    });
                    if ($('#LVContent').children('div').length == 0) {
                        $('.LVHcheckBox input').prop("disabled", true);
                    }
                }
                else if (xhr.status == 406 || xhr.status == 404) {
                    var ex = xhr.getResponseHeader('exception');
                    var text = "The requested file might have deleted or moved from the Cloud . Please verify.";
                    if (ex == "Delete not accepting") {
                        text = "Folder delete is not allowed in WebDav.";
                    }
                    else if (ex == "Delete is not allowed for SalesForce") {
                        text = ex;
                    }
                    //$.smallBox({title: text, color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error",text);
                }
                else if (xhr.status > 300) {
                    unCheckFile();
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
                $('#deleting').addClass("divdisplay");
                $('#spinner2').addClass("divdisplay");
            }
        });
    },
    getUsername: function (email) {
        var apiUrl = apicallurl + "/users/all";
        $.ajax({
            type: 'GET',
            url: apiUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            dataType: 'json',
            success: function (userData) {
                if (userData != "") {
                    bl1:{
                        for (var i = 0; i < userData.length; i++) {
                            if (userData[i].primaryEmail == email) {
                                $('#userDataWS').removeClass('divdisplay');
                                $('#userDataWS').append("<i><span class='label label-default' style='float:left;'>" + userData[i].userName + "<i class='removeTag1'></i></span></i>");
                                mailsUnames[email] = userData[i].userName;
                                break bl1;
                            }
                        }
                        mailsUnames[email] = email;
                        $('#userDataWS').append("<i><span class='label label-default' style='float:left;'>" + email + "<i class='removeTag1'></i></span></i>");
                    }
                    $('#Msg .statusMesg').text('');
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getUseremail: function (username) {
        var apiUrl = apicallurl + "/users/all";
        $.ajax({
            type: 'GET',
            url: apiUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            dataType: 'json',
            success: function (userData) {
                if (userData != "") {
                    bl1:{
                        for (var i = 0; i < userData.length; i++) {
                            var email = userData[i].primaryEmail;
                            var pcount = $('.addWorkspaceContent').find('#addUsers').children('p').length;
                            for (var j = 0; j < pcount; j++) {
                                var currentRow = $('.addWorkspaceContent').find('#addUsers').children('p:eq(' + j + ')');
                                if (email == currentRow.children('span').text()) {
                                    currentRow.remove();
                                    break bl1;
                                }
                                if (username == currentRow.children('span').text()) {
                                    currentRow.remove();
                                    break bl1;
                                }
                            }
                        }
                    }
                    $('#Msg .statusMesg').text('');
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    addFilesToWorkspace: function (wsId, fileId) {
        var fileIds = '';
        if (typeof fileId == "object" || typeof fileId == Array) {
            for (var i = 0; i < fileId.length; i++) {
                fileIds += 'fileId=' + encodeURIComponent(fileId[i]) + '&';
            }
        } else {
            fileIds = "fileId=" + fileId;
        }
        var apiUrl = apicallurl + "/workspace/addFiles/" + wsId + "?" + fileIds + "&domainUrl=" + domainUrl + "publicNew";
        $.ajax({
            type: "POST",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (AllClouds) {
                if (PageName == 'InnerWorkSpace') {
                } else {
                    unCheckFile();
                    // $.smallBox({
                    //     title: "File(s) added to workspace successfully.",
                    //     color: "#1ba1e2",
                    //     timeout: notifyTime
                    // });
                    showNotyNotification("notify","File(s) added to workspace successfully.");
                    $('#workspaceAct').html('');
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status == 403) {
                    unCheckFile();
                    // $.smallBox({
                    //     title: "You have only view permission.You can't add to files this workspace.",
                    //     color: "#1ba1e2",
                    //     timeout: notifyTime
                    // });
                    showNotyNotification("notify","You have only view permission.You can't add to files this workspace.");
                }
            }
        });
    },
    deleteWorkspaceFile: function (wsId, fileId) {
        var wdid = encodeURIComponent(fileId);
        var fileIds = '';
        if (typeof fileId == "object" || typeof fileId == Array) {
            for (var i = 0; i < fileId.length; i++) {
                fileIds += 'fileId=' + encodeURIComponent(fileId[i]) + '&';
            }
        } else {
            fileIds = "fileId=" + fileId;
        }
        var apiUrl = apicallurl + "/workspace/removeFiles/" + wsId + "?" + fileIds;
        var sid = $('tbody[id="workspaceFiles"]').find('.cf-back').attr('sid');
        if (sid != undefined) {
            apiUrl += "sharedFolderId=" + encodeURIComponent(sid);
        } else {
            apiUrl += "sharedFolderId=";
        }
        $.ajax({
            type: "POST",
            url: apiUrl,
            async: true,
            dataType: "json",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (DeleteFile) {
                $('#deleting').addClass("divdisplay");
                $('#fileComments').html('');
                $('#addingComments').addClass("divdisplay");
                $.each(fileId, function (i, fid) {
                    $('[id="' + encodeURIComponent(fid) + '"]').parents('.gradeA').remove();
                });
                if ($('#workspaceFiles').children('tr').length == 0) {
                    $('#NoFilesWorkspace').show();
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Opertion Failed.", color: "#1ba1e2", timeout: notifyError});
                    showNotyNotification("notify","Opertion Failed.");
                }
                $('#wsdeleting').hide();
            }
        });
    },
    getReportObjectSize: function (a) {
        if (a == undefined || a == null || a == 0) {
            return 0;
        } else {
            return CFManageCloudAccountsAjaxCall.getObjectSize(a, "");
        }
    },
    deleteWorspace: function (wid) {
        var apiUrl = apicallurl + "/workspace/delete/" + wid;
        $.ajax({
            type: "DELETE",
            url: apiUrl,
            async: false,
            dataType: "json",
            data: "",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (DeleteFile) {
            },
            statusCode: {
                204: function () {
                    $('[id="' + wid + '"]').parent('.file').remove();
                    $('[id="' + wid + '"]').parent('li').remove();
                    var wslength = $('#ThumbnailContent strong.filename').length;
                    if (wslength == 0) {
                        $('#CFEditWorkSpace').hide();
                        $('#NoWorkspaces').show();
                    }
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status == 403) {
                    var userDetails = JSON.parse(localStorage.getItem("CFUser"));
                    var email = userDetails.primaryEmail;
                    // $.smallBox({
                    //     title: "This emailid " + email + " not authorized.",
                    //     color: "#1ba1e2",
                    //     timeout: notifyTime
                    // });
                    showNotyNotification("notify","This emailid " + email + " not authorized.");
                    return false;
                }
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    removeWorkspace: function (wid, updateemail) {
        apiUrl = apicallurl + "/workspace/remove/collab";
        $.ajax({
            type: "POST",
            url: apiUrl,
            async: true,
            dataType: "json",
            data: updateemail,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (DeleteFile) {
            },
            statusCode: {
                200: function () {
                    $('[id="' + wid + '"]').parent('.file').remove();
                    $('[id="' + wid + '"]').parent('li').remove();
                    var wslength = $('#ThumbnailContent strong.filename').length;
                    if (wslength.length == 0) {
                        $('#CFEditWorkSpace').hide();
                        $('#NoWorkspaces').show();
                    }
                },
                204: function () {
                    $('[id="' + wid + '"]').parent('.file').remove();
                    $('[id="' + wid + '"]').parent('li').remove();
                    var wslength = $('#ThumbnailContent strong.filename').length;
                    if (wslength == 0) {
                        $('#CFEditWorkSpace').hide();
                        $('#NoWorkspaces').show();
                    }
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status == 403) {
                    var userDetails = JSON.parse(localStorage.getItem("CFUser"));
                    var email = userDetails.primaryEmail;
                    // $.smallBox({
                    //     title: "This emailid " + email + " not authorized.",
                    //     color: "#1ba1e2",
                    //     timeout: notifyTime
                    // });
                    showNotyNotification("notify","This emailid " + email + " not authorized.");
                    return false;
                }
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    sendEntLeadRequest: function (requestData) {
        var apiUrl = apicallurl + "/users/entLeadRequest?domianUrl=" + encodeURIComponent(domainUrl);
        $.ajax({
            type: "PUT",
            url: apiUrl,
            async: false,
            data: requestData,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                //$.smallBox({title: "Request sent successfully.", color: "#1ba1e2", timeout: notifyTime});
                showNotyNotification("notify","Request sent successfully.");
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    createCategoryGlobal: function (CatName) {
        var bool;
        var apiUrl = apicallurl + "/category/create?categoryName=" + CatName + "";
        $.ajax({
            type: "PUT",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                newCategory = true;
                var testlength = $('#categoryList #' + data.id + '').length;
                CFManageCloudAccountsAjaxCall.addCategoryToFiles(data.id, FromfileId);
                if (testlength >= 1) {
                    return false
                }
                else {
                    $('#categoryList').append('<li id=' + data.id + ' class="getFiles"><a href="#">' + data.categoryName + '</a><i class="removeCategory" data-toggle="modal" data-target="#myModal2"></i><i class="editCategory"></i></li>');
                }

            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    addCategoryToFiles: function (CatgoryId, fileId) {
        var catID = CatgoryId;
        var fileIds = '';
        var fid;
        if (typeof fileId == "object" || typeof fileId == Array) {
            for (var i = 0; i < fileId.length; i++) {
                fid = encodeURIComponent(fileId[i]);
                fileIds += 'fileIds=' + fid + '&';
            }
            fileIds = fileIds.substring(0, fileIds.length - 1);
        }
        else {
            fid = encodeURIComponent(fileId);
            fileIds = "fileIds=" + fid;
        }
        if (catID == "undefined") {
            return false;
        }
        else {
            sendGAEvents("Add to category Ok", PageName + "/" + catID + "/" + fileIds);
            //_gaq.push(['_trackEvent',"Add to category Ok", localStorage.getItem('UserId'),PageName+"/"+catID+"/"+fileIds]);
            var apiUrl = apicallurl + "/category/add/" + catID;
            $.ajax({
                type: "POST",
                url: apiUrl,
                async: false,
                dataType: "json",
                data: fileIds,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
                },
                success: function (data) {
                    unCheckFile();
                    catID = "undefined";
                },
                complete: function (xhr, statusText) {
                    if (xhr.status == 200) {
                        unCheckFile();
                        // $.smallBox({
                        //     title: "File(s) added to category successfully.",
                        //     color: "#1ba1e2",
                        //     timeout: notifyTime
                        // });
                        showNotyNotification("notify","File(s) added to category successfully.");
                        catID = "undefined";
                    }
                    if (xhr.status > 300) {
                        unCheckFile();
                        //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                        showNotyNotification("error","Operation Failed");
                    }
                }
            });
        }
    },
    deleteCloud: function (a) {
        var _a = false;
        var apiUrl = apicallurl + "/users/" + CFManageCloudAccountsAjaxCall.getUserId() + "/cloud/delete/" + a + "";
        $.ajax({
            type: 'DELETE',
            url: apiUrl,
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (json) {
            },
            statusCode: {
                204: function () {
                    return _a = true;
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
        return _a;
    },
    updateUserName: function (Updatedusername) {
        var userData = JSON.parse(localStorage.getItem('CFUser'));
            userData.lastName = Updatedusername;
        var UpdateUser = JSON.stringify(userData);
        var apiUrl = apicallurl + "/users/update/" + CFManageCloudAccountsAjaxCall.getUserId();
        $.ajax({
            type: 'POST',
            url: apiUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            data: UpdateUser,
            dataType: 'json',
            success: function (json) {
                var userObject = JSON.parse(localStorage.getItem('CFUser'));
                userObject.lastName = Updatedusername;
                localStorage.setItem('CFUser', JSON.stringify(userObject));
                localStorage.setItem('UserName', Updatedusername);
                $('#homenavbarcollapse').parent().children('.navbar-right').children().children('.dropdown-toggle').html('<span>' + json.lastName + '</span><b class="caret"></b>');
                $('.ProfileContent').html('');
                $('.ProfileContent').text(json.lastName);
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },

    updateMobileNumber:function (Updatedusername) {
        var userData = JSON.parse(localStorage.getItem('CFUser'));
        userData.mobileNumber = Updatedusername;
        var UpdateUser = JSON.stringify(userData);
        var apiUrl = apicallurl + "/users/update/" + CFManageCloudAccountsAjaxCall.getUserId();
        $.ajax({
            type: 'POST',
            url: apiUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            data: UpdateUser,
            dataType: 'json',
            success: function (json) {
                var userObject = JSON.parse(localStorage.getItem('CFUser'));
                userObject.mobileNumber = Updatedusername;
                localStorage.setItem('CFUser', JSON.stringify(userObject));
                localStorage.setItem('mobileNumber', Updatedusername);
                $('#homenavbarcollapse').parent().children('.navbar-right').children().children('.dropdown-toggle').html('<span>' + json.mobileNumber + '</span><b class="caret"></b>');
                $('.ProfileContentmobile').html('');
                $('.ProfileContentmobile').text(json.mobileNumber);
                if(json.mobileNumber.length ==0 ){
                    $('.ProfileContentmobile').text('Not Available');
                    localStorage.setItem('mobileNumber', 'Not Available');
                }
                

            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });
    },
    getCloudsFromConfig: function () {
        var apiUrl = apicallurl + "/subdomain/user/clouds/info";
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (cloudConfig) {//cloud_webdav_header cloud_ecm_header cloud_ecm_message
                if (cloudConfig != undefined && cloudConfig != null) {
                    if (cloudConfig.cloudsList != undefined && cloudConfig.cloudsList.length > 0) {
                        var ecmClouds = ["CMIS", "SHAREFILE", "ALFRESCO", "WALRUS", "SHAREPOINT_2013", "SHAREPOINT_2010", "DOCUMENTUM", "CLOUDIAN",
                            "AZURE_OBJECT_STORAGE", "GENERIC_OBJECT_STORAGE", "NTLM_STORAGE"];
                        var webdavClouds = ["WEBDAV"];
                        var ecmCloudCount = 0;
                        var webdavCloudCount = 0;
                        $.each(CLName, function (i, e) {
                            if ($.inArray(i, cloudConfig.cloudsList) == -1) {
                                var div;
                                if (i == "DOCUMENTUM") {
                                    div = $('.cloudImgDocu').closest('a').parent();
                                    div.remove();
                                } else if (i == "WALRUS") {
                                    div = $('.cloudImgEUCA').closest('a').parent();
                                    div.remove();
                                } else {
                                    div = $('.cloudImg' + i + '').closest('a').parent();
                                    div.remove();
                                }
                            } else {
                                if ($.inArray(i, ecmClouds) != -1) {
                                    ecmCloudCount++;
                                } else if ($.inArray(i, webdavClouds) != -1) {
                                    webdavCloudCount++;
                                }
                            }
                        });
                        if (ecmCloudCount == 0) {
                            $('#cloud_ecm_header').remove();
                        }
                        if (webdavCloudCount == 0) {
                            $('#cloud_webdav_header').remove();
                        }
                    }
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                    showNotyNotification("error","Operation Failed");
                }
            }
        });

    },
    showNotification: function (a, b) {
        // var c = '#1ba1e2';
        // var t = 4000;
        // if (a == 'error') {
        //     c = '#e35e00';
        //     t = 1500;
        // } else if (a == 'notify') {
        //     c = '#1ba1e2';
        //     t = 2000;
        // } else if (a == 'warn') {
        //     c = '#f86227';
        //     t = 1500;
        // }
        //$.smallBox({title: b, color: c, timeout: t});
        showNotyNotification(a,b);
    },

    //Spark Configuration
    sparkSync: function (a, b) {
        var apiUrl = apicallurl + "/spark/sync?cloudId=" + a;
        if (b != undefined) {
            apiUrl += '&folderId=' + encodeURIComponent(b);
        }
        $.ajax({
            type: 'POST',
            url: apiUrl,
            async: true,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (json) {

            },
            complete: function (xhr) {
                if (xhr.status == 204) {
                    CFManageCloudAccountsAjaxCall.showNotification('notify', 'Spark content sync completed.');
                } else {
                    CFManageCloudAccountsAjaxCall.showNotification('error', 'Spark content sync failed.');
                }
            }
        });
    },
    //spark/syncFolder
    sparkSyncFolder: function (a) {
        //var apiUrl = apicallurl + "/spark/syncFolder" + a,
        var apiUrl = apiboturl + "/bot/spark/syncFolder" + a,
            _a = null;
        $.ajax({
            type: 'POST',
            url: apiUrl,
            async: true,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (json) {
                setTimeout(function(){
                    CFManageCloudAccountsAjaxCall.getRoomSyncInfo(a.split('roomId=').pop());
                },5000);
            },
            complete: function (xhr) {
                var text = xhr.getResponseHeader('exception');
                if(xhr.status == 406){
                    CFManageCloudAccountsAjaxCall.showNotification('error', text);
                    $('#config_spark').trigger('click');
                    $("#spark_pair span").removeClass("fa-spin");
                }else if (xhr.status > 300) {
                    CFManageCloudAccountsAjaxCall.showNotification('error', 'Spark content sync failed.');
                    $('#config_spark').trigger('click');
                    $("#spark_pair span").removeClass("fa-spin");
                }
            }
        });
    },
    getSpark: function () {
        //spark/cloud/get
        var _a = null;
        var apiUrl = apicallurl + "/spark/cloud/get";
        $.ajax({
            type: 'GET',
            url: apiUrl,
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (json) {
                return _a = json;
            }
        });
        return _a;
    },
    deleteSpark: function (a) {
        var _a = null;
        var apiUrl = apicallurl + "/spark/cloud/delete/" + a;
        $.ajax({
            type: 'DELETE',
            url: apiUrl,
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (json) {
            },
            complete: function (xhr) {
                if (xhr.status == 204) {
                    return _a = true;
                }
            }
        });
        return _a;
    },
    getSparkSync: function (a) {
        var apiUrl = apicallurl + "/spark/" + CFManageCloudAccountsAjaxCall.getUserId() + "/loadstatus",
            _b = null;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (r) {
                return _b = r;
            }
        });
        return _b;
    },
    getSparkRooms: function () {
        var _a = null;
        //var apiUrl = apicallurl + "/spark/getRooms";
        var apiUrl = apiboturl + "/bot/spark/rooms";
        $.ajax({
            type: 'GET',
            url: apiUrl,
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (json) {
                return _a = json;
            }
        });
        return _a;
    },
    appendSparkPairClouds:function(parent){
        $.each(AllCloudsInfo,function(i,e) {
            var _dispname = e.userDisplayName == null ? e.cloudUserId.split('|').pop() : e.userDisplayName,
                src = '../img/drive/'+ e.cloudName+'.png';
            var _html = '<li class="list-group-item cloud waves-effect" data-cid="'+ e.id+'"><input type="radio" name="cloud"><img src="'+src+'">'+_dispname+'</li>';
            if(e.cloudName != "DROPBOX_BUSINESS"){
                parent.append(_html);
            }
        });
    },
    appendSparkPairRooms:function(rooms,container){
        $.each(rooms,function(i,e) {
            var _html = '<li class="list-group-item" data-id="' + e.id + '"><input type="checkbox">';
            if (e.roomSyncInfo != null) {
                _html = '<li class="list-group-item" data-id="' + e.id + '" data-cid="'+ e.roomSyncInfo.cloudId+'"><input type="checkbox">';
            }
            _html += '<div class="room_icon"><img src="../img/spark_room.png">' +
                '<span>' + e.members.length + '</span>' +
                '</div>' + CFManageCloudAccountsAjaxCall.getMaxChars(e.title, 25) + '</li>';
            container.append(_html);
        })
    },
    getRoomSyncInfo:function(a){
        var _roomId = a;
        //var apiUrl = apicallurl + "/spark/roomSyncInfo?roomId="+a,
        var apiUrl = apiboturl + "/bot/spark/roomSyncInfo?roomId="+a,
            _a = null;
        $.ajax({
            type: 'GET',
            url: apiUrl,
            async: true,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (sync) {
                var _p = $('li[data-id="'+_roomId+'"]'),
                    _rName = _p.attr('data-room-name');
                _p.find('[data-folder-name]').text(sync.folderName);
                _p.attr('data-fid',sync.folderId);
                if(sync.loadLock){
                    CFManageCloudAccountsAjaxCall.checkSyncStatus(_roomId);
                }
                else{
                    if(_rName == undefined)
                    {
                        $(".pair_body .rooms .list-group-item").each(function(){
                           if($(this).attr('data-id')== _roomId)
                               _rName = $(this).clone().children().remove().end().text().trim();
                        });
                    }
                    CFManageCloudAccountsAjaxCall.showNotification('notify',_rName+' Sync Completed');
                    _p.find('.fa-spin').removeClass('fa-spin');
                     $(".sync_container").find('.fa-spin').removeClass('fa-spin');
                    sparkConfig();
                }
            }
        });
    },
    checkSyncStatus:function(a){
        setTimeout(function(){
            CFManageCloudAccountsAjaxCall.getRoomSyncInfo(a);
        },1000);
    },
    unlinkSparkRoom:function(a){
        //var apiUrl = apicallurl + "/spark/unlink/room?roomId="+a,
        var apiUrl = apiboturl + "/bot/spark/unlink/room?roomId="+a,
            _a = null;
        $.ajax({
            type: 'DELETE',
            url: apiUrl,
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
            },
            success: function (sync) {
                return _a = true;
            }
        });
        return _a;
    },
    //UI Re-Render
    applyTooltip: function () {
        return $('[data-toggle="tooltip"]').tooltip();
    },
    makeScrollTo:function(a,b) {
        a.animate({
                scrollTop: b.offset().top
            },
            'slow');
    },
    getFiles:function(u){
        var _a = null;
        $.ajax({
            type: "GET",
            url: u,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (s) {
                return _a = s;
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    CFManageCloudAccountsAjaxCall.showNotification('error','File fetching failed.')
                }
            }
        });
        return _a;
    },
    appendFiles: function (c, f) {
        $.each(f, function (i, e) {
            var _icon = e.directory == true ? 'cf-folder6' : 'cf-file3';
            _icon = e.type == 'SITE' ? 'cf-network':_icon;
            var _html = '';

            _html += '<li class="files" data-fid="' + e.id + '" data-cid="' + e.cloudId + '"><input type="radio" name="radio">';
            _html += '<i class="' + _icon + '"></i><span>' + e.objectName + '</span></li>';

            c.append(_html);

        });
    },
    getAllFiles: function (PageNumber) {
        var apiUrl = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() + "/childfolders/all?page_size=50&isAscen=" + urlParameterObject.isAscen + "&orderField=" + urlParameterObject.orderField + "&page_nbr=" + PageNumber + "&fetchCollabInfo=" + urlParameterObject.fetchCollabInfo + "&folderId=&isAllFiles=" + urlParameterObject.isAllFiles;
        var _a = '';
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (s) {
                return _a = s;
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    $.smallBox({title: "Operation Failed", color: "#e35e00", timeout: notifyError});
                }
            }
        });
        return _a;
    },

    //BreadCrumb
    constructBreadCrumb:function(p,data,id) {
        var _html = '';
        if (data == 'clouds') {
            var _cname = '';
            CFManageCloudAccountsAjaxCall.getAllClouds();
            $.each(AllCloudsInfo, function (i, e) {
                if (e.id == id) {
                    _cname = e.userDisplayName;
                }
            });
            _html = '<li><a href="javascript:void(0)" data-action="getAllClouds">My Clouds</a></li>' +
                '<li><a href="javascript:void(0)" data-action="getFolders" data-cid="' + id + '">' + _cname + '</a></li>';
            p.append(_html);
        } else if (data == 'folders') {
            var _fileDetails = CFManageCloudAccountsAjaxCall.getFileInfo(id);
            if (_fileDetails != null) {
                _html = '<li><a href="javascript:void(0)" data-action="navigate" ' +
                    'data-cid="' + _fileDetails.cloudId + '" data-fid="' + _fileDetails.id + '">'
                    + _fileDetails.objectName + '</a></li>';
                p.append(_html);
            }
        }
    },
    getFileInfo: function (fileid) {
        var apiUrl = apicallurl+"/filefolder/info?fileId="+encodeURIComponent(fileid)+"&fetchCollabInfo=true";
        var _a = null;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (r) {
                return _a = r;
            },
            complete:function(xhr, statusText){
                if(xhr.status == 404 || xhr.status == 500){
                    return _a;
                }
                else if(xhr.status > 300){
                    CFManageCloudAccountsAjaxCall.showNotification('error','Failed to get file info');
                }
            }
        });
        return _a;
    },

};

function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

$('#priviewPswd').keypress(function(e) {
    if (e.which == 13) {
        $('.CFPriviewPwdok').trigger('click');
    }
});
function CFHideContents(){
		$('#CFHSortFileName').children('span').html('');
		$('#CFHSortfileSize').children('span').html('');
		$('#CFHSortAddedOn').children('span').html('');
		$('#CFHSortModifiedOn').children('span').html('');
		$('#CFHSortCloudDrive').children('span').html('');
		//var fileControls = $('.filecontrols');
        disableActionPanel(actionPanel);
        //fileControls.addClass('buttonDisable');
		$('#LVWContentHeader').addClass('divdisplay');
		$('#ThumbnailContent').hide();
		$('#ThumbnailContent').parent('.slimScrollDiv').hide(); 		
		$('#LVContentHeader').addClass('divdisplay');
		$('#LVContent').hide();
		$('#AccountSettings').hide();
		$('#ReportsContent').hide();
		$('#HomeContent').hide();
		$('#CFCreateWorkSpace').hide();
		$('.fileview').hide();
		$('#LVContentHeader input[type="checkbox"]').prop('checked', false);
		$('#page-header .searchbox').show();
		urlParameterObject = {
			"isAscen":"false",
			"fetchCollabInfo":"true",
			"folderId":"",
			"isAllFiles":"true",
			"orderField":"modifiedTime"
		};
		$('#CFHSortModifiedOn').children('span').html("&#9650");
}
function CFShowContents(){
	var pagename = PageName;
	  $('#LVHeader').children('.LVHFavorites').show();
	  $('#LVHeader').children('.LVHFavorites').text("Favorites");
	if(pagename == "Reports"){
		$('#HomeContent').show();
		$('#ReportsContent').show();
		$('#ListContent').parent('.slimScrollDiv').hide();
	}
    else if(pagename == "WorkSpace"){
		$('.workspaceUpload#CFUploadFiles').addClass('buttonDisable');
		$('#HomeContent , #LVContent, .fileview').show();
		$('#ListContent').parent('.slimScrollDiv').show();
		$('#LVWContentHeader').removeClass('divdisplay');
	}
    else if(pagename == "Folders" ){
		$('#HomeContent').show();
		$('#LVContent').show();
		$('#ListContent').parent('.slimScrollDiv').show();
		$('#LVContentHeader').removeClass('divdisplay');
		$('.fileview').show();
		$('.filecontrols #shareData > div').css('opacity', '0.2');
		urlParameterObject.isAllFiles = "false";
	}
    else if(pagename == "Category"){
		$('#HomeContent').show();
		$('#LVContent').show();
		$('#ListContent').parent('.slimScrollDiv').show();
		$('#LVContentHeader').removeClass('divdisplay');
		$('.fileview').show();
		$('.filecontrols #shareData > div').css('opacity','');
		$('#CFUploadFiles').removeClass('buttonDisable');
	}
    else if(pagename == "Home" || pagename == "Recent files" || pagename =="Favorites" || pagename =="All Items" || pagename =="Tag"){
		$('#HomeContent').show();
		$('#LVContent').show();
		$('#ListContent').parent('.slimScrollDiv').show();
		$('#LVContentHeader').removeClass('divdisplay');
		$('.fileview').show();
		$('.filecontrols #shareData > div').css('opacity','');
	}
    else if(pagename == "InnerFolders" && previousPage == "Share with Me"){
        $('#HomeContent').show();
        $('#LVContent').show();
        $('#ListContent').parent('.slimScrollDiv').show();
        $('#LVContentHeader').removeClass('divdisplay');
        $('.fileview').show();
        $('.filecontrols #shareData > div').css('opacity','');
    }
    else if(pagename == "CloudDrive" || pagename == "InnerFolders"){
		$('#HomeContent').show();
		$('#LVContent').show();
		$('#ListContent').parent('.slimScrollDiv').show();
		$('#LVContentHeader').removeClass('divdisplay');
		$('.fileview').show();
		$('.filecontrols #shareData > div').css('opacity','');
	}
    else if(pagename == "SharepointSite"){
        $('#HomeContent').show();
        $('#LVContent').show();
        $('#ListContent').parent('.slimScrollDiv').show();
        $('#LVContentHeader').removeClass('divdisplay');
        $('.fileview').show();
        $('.filecontrols #shareData > div').css('opacity','');
    }
    else if(pagename =="Share by Me" || pagename =="Share with Me"){
		$('#HomeContent').show();
		$('#LVContent').show();
		$('#ListContent').parent('.slimScrollDiv').show();
		$('#LVContentHeader').removeClass('divdisplay');
		$('.fileview').show();
		$('.filecontrols #shareData > div').css('opacity','');
	}
    else if(pagename == "InnerWorkSpace"){
	    $('#HomeContent').show();
		$('#LVContent').show();
		$('#ListContent').parent('.slimScrollDiv').show();
		$('#LVContentHeader').removeClass('divdisplay');
		 $('#CFEditWorkSpace').show(); 
        $('#CFCreateWorkSpace').show();		  
		$('.fileCreatecontrols #CFCreateWorkSpace').hide();
		if(AllCloudsInfo.length >= 1){
		  $('.workspaceUpload#CFUploadFiles').removeClass('buttonDisable');
		}
	}
}
var downCount = 0;
function downloadURL(url) {
    if(sharedFolderId == undefined || sharedFolderId == ""){
        sharedFolderId = "";
    }
    var sid = '';
    if (PageName != "Share with Me" && previousPage != "Share with Me") {
        var url1 = url;
        if(PageName == "InnerWorkSpace"){
            sid = $('tbody[id="workspaceFiles"]').find('.cf-back').attr('sid');
            if(sid != undefined){
                url1 +="&sharedFolderId="+encodeURIComponent(sid);
            }else{
                url1 +="&sharedFolderId=";
            }
        }
        $.get(url1+'&isDownloading=false', function () {
        }).fail(function (data) {
            if (data.status == 404 || data.status == 406) {
                var test = '';
                if (PageName == 'InnerWorkSpace') {
                    test = "The requested file might have deleted or moved from the Cloud . Please verify and add to the workspace.";
                }else {
                    test = "The requested file might have deleted or moved from the Cloud . Please verify.";
                }
                showNotyNotification('error',test);
            }
        });
    }
    if(PageName == "InnerWorkSpace"){
        sid = $('tbody[id="workspaceFiles"]').find('.cf-back').attr('sid');
        if(sid != undefined){
            url = url+"&sharedFolderId="+encodeURIComponent(sid);
        }
    }
    var hiddenIFrameID = 'hiddenDownloader' + downCount;
    downCount = downCount + 1;
    iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
    setTimeout(function () {
        if (PageName != "InnerWorkSpace") {
            unCheckFile();
        }
    }, 1000);
}
function appendBreadCrumb(fileId,cloudId,displayname,isBase){
    if(isBase==undefined ||isBase==true){
		$('#breadCrumbdync').append('<li style="cursor:pointer;" id='+fileId+' class="BCRFList" cloudId='+cloudId+' fileId='+fileId+'><a style="cursor:pointer;">'+displayname.substr(0,30)+'</a><span class="divider"></span></li>');
	}else{
		$('#breadCrumbdync').append('<li style="cursor:pointer;" id='+fileId+' class="BCRFList" cloudId='+cloudId+'><a style="cursor:pointer;">&nbsp;/&nbsp;'+displayname.substr(0,30)+'</a><span class="divider"></span></li>');
	}
}

// $('#moveSource').on('click','.cf-refresh4',function(){
//     $(this).removeClass('cf-refresh4').addClass('cloudSpinn');
//     var id = $(this).parent().attr('id');
//    // sendGAEvents("Clicked on Cloud Sync",id);
//    // refreshCloud.push(id);
//     $.smallBox({title:'Preparing files to move.',color:"#1ba1e2",timeout:2000});
//showNotyNotification("notify",'Preparing files to move.');
//     //CFManageCloudAccountsAjaxCall.refreshcloud(id);
// });

$('#refreshcloudmovesrc').on('click',function(e){
    
    //$.smallBox({title:'Preparing files to move.',color:"#1ba1e2",timeout:2000});
    showNotyNotification("notify",'Preparing files to move.');
});

$('#breadCrumbdyncmove').on('click', 'li.BCRFList', function(e){
 var fileId = $(this).attr('id');
     moveCheckSum = 'source';
    // if(fileId == SinglePId){
    //     return false;
    // }
    PageName = 'innermove';
    $(this).children('a:last').css("color","");
    $(this).children('a:last').css("text-decoration","none");
    var cloudId = $(this).attr('cloudid');
    var PageNumber =1;
    var nextfiles = $(this).next();
    $('#breadCrumbdyncmove li').show();
    if(fileId == "cloud"){
       $('#breadCrumbdyncmove li').hide();
       CFManageCloudAccountsAjaxCall.getAllClouds();
       $('#dynamicCloudName').siblings('i').removeAttr('id');

        var dispanme = '';
        // if(moveCheckSum == null || moveCheckSum == ''){
        //     $('#moveSource , #moveDestination').html('');
        // }
       // else if(moveCheckSum == 'source'){
            $('#moveSource').html('');
            $('#moveSource').siblings('#movePageShowMore').hide();
        // }
        // else if(moveCheckSum == 'dest'){
        //     $('#moveDestination').html('');
        //     $('#moveDestination').siblings('#movePageShowMore').hide();
        // }
        $.each(AllCloudsInfo,function(i,cloud){
            var email = cloud.cloudUserId;
            email = email.split('|');
            email = email[1];
            if(cloud.userDisplayName != '' || cloud.userDisplayName != null){dispanme = cloud.userDisplayName;}
            else{dispanme = email;}
            var _warning = cloud.cloudStatus == "INACTIVE" ? "cf-warning" : "";
            $('.span6 #moveSource').append('<div class="moveCloudBlock button '+_warning+'" style="width:150px;padding:0" type="button">' +
            '<div class="move '+cloud.cloudName+'" cid="'+cloud.id+'" pid="'+cloud.rootFolderId+'"></div>' +
            '<p id="userEmail">'+dispanme+'</p></div>');
        });
        PageName = 'moveLanding';
        $('#moveSource').siblings('.tab-header').children('#dynamicCloudName').html('');
        $('#moveSource').siblings('.tab-header').children('#totalfiles').html('');
    }else if(fileId == 'cloudFolders'){
        PageNumber = 1;
        SingleCloudId = cloudId;
        previousPage = PageName;
        PageName = 'CloudDrive';
        SinglePId = SingleCloudId;
        $('#spinner2').show();
        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(SingleCloudId, PageNumber);
    }
    else{
        SinglePId = fileId;
        SingleCloudId = cloudId;
        PageNumber = 1;
        if(fileId == 'allcloud')//if we click on the cloud name to get all files from a cloud
        {
            PageName='move';
        }
        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(cloudId, PageNumber);
    }
    $(this).nextAll().remove();
});

$('#breadCrumbdyncmovedest').on('click', 'li.BCRFList', function(e){
 var fileId = $(this).attr('id');

    var navid = $(this).attr('id');
    var cloudid= $(this).attr('cid');
    PageName = 'innermove';
    moveCheckSum = 'dest';

    if(navid == SinglePId){
        return false;
    }
    $(this).children('a:last').css("color","");
    $(this).children('a:last').css("text-decoration","none");
    var cloudId = $(this).attr('cloudid');
    var PageNumber =1;
    var nextfiles = $(this).next();
    $('#breadCrumbdyncmovedest li').show();
    if(navid == "cloud"){
       $('#breadCrumbdyncmovedest li').hide();
       CFManageCloudAccountsAjaxCall.getAllClouds();
       $('#breadCrumbdyncmovedest').siblings('i').removeAttr('id');
       $('#dynamicDestCloudName').siblings('i').removeAttr('id');

        var dispanme = '';
        // if(moveCheckSum == null || moveCheckSum == ''){
        //     $('#moveSource , #moveDestination').html('');
        // }
       // else if(moveCheckSum == 'source'){
            $('#moveDestination').html('');
            $('#moveDestination').siblings('#movePageShowMore').hide();
        // }
        // else if(moveCheckSum == 'dest'){
        //     $('#moveDestination').html('');
        //     $('#moveDestination').siblings('#movePageShowMore').hide();
        // }
         $.each(AllCloudsInfo,function(i,cloud){
            var email = cloud.cloudUserId;
            email = email.split('|');
            email = email[1];
            var _warning = cloud.cloudStatus == "INACTIVE" ? "cf-warning" : "";
            if(cloud.userDisplayName != '' || cloud.userDisplayName != null){dispanme = cloud.userDisplayName;}
            else{dispanme = email}
            $('.span6 #moveDestination').append('<div class="moveCloudBlock button '+_warning +'" type="button" style="padding:0">' +
            '<input type="radio"  name="destCloud" class="destCloudInput" style="visibility: hidden">' +
            '<div class="move '+cloud.cloudName+'"  cid="'+cloud.id+'" pid="'+cloud.rootFolderId+'" style="margin-left:30px"></div>' +
            '<p id="userEmail">'+dispanme+'</p></div>');
            $('#moveDestination .moveCloudBlock').droppable({accept:'#moveSource .list-group-item',drop:dropEventHandler,over:function(){$(this).addClass('moveCloudBlockDragHover').removeClass('moveCloudBlock');},out:function(){$(this).removeClass('moveCloudBlockDragHover').addClass('moveCloudBlock');}});
        });
        PageName = 'moveLanding';
       $('#moveDestination').siblings('.tab-header').children('#dynamicDestCloudName').html('');
        $('#moveDestination').siblings('.tab-header').children('#totalfilesdestination').html('');
    }else if(navid == 'cloudFolders'){
        PageNumber = 1;
        SingleCloudId = cloudId;
        previousPage = PageName;
        PageName = 'CloudDrive';
        SinglePId = SingleCloudId;
        $('#spinner2').show();
        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(SingleCloudId, PageNumber);
    }
    else{
        SinglePId = navid;
        SingleCloudId = cloudId;
        PageNumber = 1;
        if(navid == 'allcloud')//if we click on the cloud name to get all files from a cloud
        {
            PageName='move';
        }
        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(cloudId, PageNumber);
    }
    $(this).nextAll().remove();
});


$('#breadCrumbdync').on('click', 'li.BCRFList', function(e) {
    var fileId = $(this).attr('id');

    if (fileId == SinglePId) {
        return false;
    }
    $(this).children('a:last').css("color", "");
    $(this).children('a:last').css("text-decoration", "none");
    var cloudId = $(this).attr('cloudid');
    var PageNumber = 1;
    var nextfiles = $(this).next();
    $('#breadCrumbdync li').show();
    if (fileId == "cloud") {
        $('#CloudDriveList li [id="' + SingleCloudId + '"] p').trigger('click');
    }
    else if (fileId.trim() == "Home") {
        $('#homeHome').trigger('click');
        InnerCloudId = [];
        InnerFolderId = [];
    }
    else if (fileId == "RecentFiles") {
        $('#CFRecentFilesAndFolders').trigger('click');
        InnerCloudId = [];
        InnerFolderId = [];
    }
    else if (fileId == "FavouriteFiles") {
        $('#favourite').trigger('click');
        InnerCloudId = [];
        InnerFolderId = [];
    }
    else if (fileId == "ShareByMe") {
        $('#CFSharedByMe').trigger('click');
        InnerCloudId = [];
        InnerFolderId = [];
    }
    else if (fileId == "ShareWithMe") {
        $('#CFSharedWithMe').trigger('click');
        InnerCloudId = [];
        InnerFolderId = [];
    }
    else if (fileId == "Workspace") {
        $('#homeWorkspace').trigger('click');
        InnerCloudId = [];
        InnerFolderId = [];
    }
    else if (fileId == "Folders") {
        $('#homeFolder').trigger('click');
        InnerCloudId = [];
        InnerFolderId = [];
    }
    else if (fileId == "AllFiles") {
        var _AllFiles = CFManageCloudAccountsAjaxCall.getAllFiles(PageNumber);
        CFManageCloudAccountsAjaxCall.getAllFileNames(_AllFiles, PageNumber);
        InnerCloudId = [];
        InnerFolderId = [];
    }
    else if (PageName == "Category") {
        return false;
    }
    else {
        SinglePId = fileId;
        var _fileDetails = CFHPlistview.getFileDetails(fileId);
        
        var _type = _fileDetails != undefined ? _fileDetails.type : null;
        var _isDir = _fileDetails != undefined ? _fileDetails.directory : null;
        var _a;
        if (_isDir == false) {
            _a = "File";
        }
        else if (_isDir == true) {
            _a = "Folder";
        }
        else if (_type == "SITE" && _isDir == null) {
            _a = "Site";
        }
        PageName = _a == "Site" ? "SharepointSite" : "InnerFolders";
        CFHideContents();
        CFShowContents();
        InnerCloudId = [];
        InnerFolderId = [];
        if (previousPage == "Share with Me" || previousPage == "Share by Me") {
            sharedFolderId = $('#breadCrumbdync').children('li:eq(1)').attr('id');
            CFManageCloudAccountsAjaxCall.gotoInnerFolderandFiles(cloudId, fileId, PageNumber, 'back', sharedFolderId);
        }
        else {
            CFManageCloudAccountsAjaxCall.gotoInnerFolderandFiles(cloudId, fileId, PageNumber);
        }
    }
    $(this).nextAll().remove();
});
var currentDate = new Date(); // Todays date 
 $('#sharecontent').find("#datepicker3").datepicker({
    minDate: currentDate,
    inline: true,
    showOtherMonths: true,
    dateFormat: 'mm/dd/yy',
    onSelect: function (date) {
        var selectedDate = $('#datepicker3').datepicker('getDate');
        Date.prototype.DaysBetween = function () {
            var intMilDay = 24 * 60 * 60 * 1000;
            var intMilDif = arguments[0] - this;
            return Math.floor(intMilDif / intMilDay);
        };
        var days = currentDate.DaysBetween(selectedDate);
        $(this).attr('data-days', days);
    }
});
//Delete Cloud
$('#manageclouddiv').on('click', '[data-action="delete"]' , function(){
    sendGAEvents("Delete Cloud");
    var _id = $(this).attr('data-id');
    $('#CFCLDelete').attr("data-type",_id);
    var apiUrl = apicallurl + "/users/" + CFManageCloudAccountsAjaxCall.getUserId() + "/get/cloud/movestatus?cloudId="+_id;
    var status=0;
    $.ajax({
        type: "GET",
        url: apiUrl,
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            status=data;
            if(data>0)
            {
                $('#CFCLdeleteModal h4').text('Cloud you have selected to delete is associated with file migration. Do you want to delete it anyway?');
            }
            else
            {
                $('#CFCLdeleteModal h4').text('Are you sure you want to delete the selected Cloud?');
            }
        },
        complete: function (xhr, statusText) {
            if (xhr.status > 300) {
                //$.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError});
                showNotyNotification("error","Operation Failed");
            }
        }
    });
    $('#CFCLdeleteModal').modal('show');
});

$('#CFCLDelete').on('click',function() {
    sendGAEvents("Delete Cloud Yes");
    var _id = $(this).attr('data-type');
    var _r = CFManageCloudAccountsAjaxCall.deleteCloud(_id);

    var _parent = $('#manageclouddiv');

    if(_r){
        showNotyNotification('notify','Cloud deleted successfully.');
        _parent.find('[data-cloud="'+_id+'"]').remove();

    }else {

    }

    var index = $.inArray(_id, CloudId);
    PrimaryEmail.splice(index,1);
    CloudId.splice(index,1);
    CloudName.splice(index,1);
    CLParent.splice(index,1);
    AllCloudsInfo.splice(index, 1);
    CFManageCloudAccountsAjaxCall.loadDataToProgressBar();
    $('#CFCLdeleteModal').modal('hide');
});

//Rename Cloud
$('#manageclouddiv').on('click','[data-action="edit"]',function(){
    var _parent = $(this).closest('td');
    var _old = _parent.html();
    var _name = $(this).prev().text().trim();
    var _new = '<input type="text" style="width:73%" class="form-control slim-form-control" value="'+_name+'"><span' +
        ' data-action="rename"><i' +
        ' class="cf-check"></i><i class="cf-times" data-action="cancel"></i></span>';
    _parent.html(_new);

    var _input = _parent.find('input');

    $('[data-action="cancel"]').on('click',function(){
        _parent.html(_old);
    });

    $('[data-action="rename"]').on('click',function(){
        var user = /[^a-zA-Z ]/;
        var userlen = /[^a-zA-Z ]{2,20}/;
        var _val = _input.val().trim();

        if(_val.length < 1){
            _input.addClass('danger');
            return showNotyNotification('error','Please Enter Displayname');
        }
        else if(_val.length < 6 || _val.length > 20){
            _input.addClass('danger');
            return showNotyNotification('error',"Name must be 6-20 characters in length.");
        }/*
        else if(user.test(_val)){
            _input.addClass('danger');
            return showNotyNotification('error',"Name can contain only letters (A-Z and" +
                " a-z).");
        }
        else if(userlen.test(_val)){
            _input.addClass('danger');
            return showNotyNotification('error',"Name must be 2-20 characters in length.");
        }*/
        else{
            _input.removeClass('danger');
            var _CloudID = $(this).closest('tr').attr('data-cloud');
            var _a = {
                id:_CloudID,
                name:_val
            };
            var _r  = CFManageCloudAccountsAjaxCall.renameCloud(_a);
            if(_r){
                var _html = '<span style="padding-right: 30px;">'+_val+'</span><i style="padding-right:10px;" class="cf-pencil9 pull-right"' +
                    ' data-action="edit"></i>';
                $('body').off('mouseup');
                _parent.html(_html);
            }else{
                return showNotyNotification('error','Cloud rename failed.');
            }
        }
    });

    $('body').on('mouseup', function (e) {
        if (_input.is(e.target) ||
            _input.next().find('i').is(e.target)
        ) {}
        else {
            $('[data-action="cancel"]').trigger('click');
            $('body').off('mouseup');
        }
    });

});

function getParameterByName( name,href ){
    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS );
    var results = regex.exec( href );
    if( results == null ) {
        return "";
    }else {
        return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
}

function enableCrateControls1(c,u){
    var _c = $("#CFCreateFolder").parent(),
    _u =$('#CFUploadFiles').parent();

    if(c){
        _c.addClass('buttonDisable');
    }else{
        _c.removeClass('buttonDisable');
    }

    if(u){
        _u.addClass('buttonDisable');
    }else{
        _u.removeClass('buttonDisable');
    }
}

function clearSorting(){
	$('#CFHSortfileSize').children('span').html('');
	$('#CFHSortAddedOn').children('span').html('');
	$('#CFHSortModifiedOn').children('span').html('');
	$('#CFHSortCloudDrive').children('span').html('');
	$('#CFHSortFileName').children('span').html('');
}
function helperEvent(e,ui){
    if($('#moveSource input:checked').length == 0){
        this.classList.add('fileActive');
        var noneid = e.currentTarget.id;
        $('[id="'+noneid+'"] input').attr('checked',true);
        $('[id="'+noneid+'"]').addClass('fileActive');
    }
    var helperList = $('<div class="draggable-helper" ></div>');
    if ($(this).is('.fileActive')){
        helperList.append($(this).siblings('.fileActive').addBack().clone());
    }
    var fid=[];
    var cid = [];
    $(helperList[0]).find('label').each(function(){
        fid.push($(this).attr('id'));
        cid.push($(this).attr('cid'));
    });
    var helperText = "";
    if(fid.length == 1){
        sendGAEvents("Drag Single File");
        //_gaq.push(['_trackEvent',"Drag Single File", localStorage.getItem('UserId')]);
        var fileName = helperList.find('label').children('p').text();
        helperText = "<span fid='"+fid+"' cid='"+cid+"' style='z-index:99999;width:100px;padding:5px;background:#428bca;color:#fff;font-size:10px;word-wrap:break-word'>"+fileName+"</"+"span>";
      }else if(fid.length < 1){
        sendGAEvents("Drag Multiple File");
        //_gaq.push(['_trackEvent',"Drag Multiple File", localStorage.getItem('UserId')]);
        helperText = "<span fid='"+fid+"' cid='"+cid+"' style='z-index:99999;width:100px;padding:5px;background:#428bca;color:#fff;font-size:10px;word-wrap:break-word'>Please select File </"+"span>";
     }else{
        helperText = "<span fid='"+fid+"' cid='"+cid+"' style='z-index:99999;width:100px;padding:5px;background:#428bca;color:#fff;font-size:10px;word-wrap:break-word'>Move "+fid.length+" item</"+"span>";
    }
    return helperText;
}
function dropEventHandler(e,ui) {
    sendGAEvents("Drop in Folder");
    //_gaq.push(['_trackEvent',"Drop in Folder", localStorage.getItem('UserId')]);
    $("#moveSource").find(".list-group-item").each(function () {
        $(this).removeClass('fileActive');
    });
    $("#moveDestination").find("input").removeAttr('checked');
    var tofid = '';
    var tocid = '';
    var fromfid = [];
    var fromcid = [];
    var htmlCont = '';
    /*Extract Html content from Helper object*/
    htmlCont = ui.helper[0].outerHTML;
    fromfid = $(htmlCont).attr('fid');
    fromfid = fromfid.split(",");
    if ($("#moveSource").find("input:checked").length < 1) {
        ui.draggable.draggable('option', 'revert', true);
        $('#moveSource [id="' + fromfid + '"]').children('input').attr('checked', 'false');
        $('#moveDestination').find('.draghover').addClass('dropabbleParent').removeClass('draghover');
        $("#moveDestination").find(".list-group-item").each(function () {
            $(this).removeClass('fileActive');
        });
        $("#moveDestination").children('.button.ui-droppable').each(function () {
            $(this).removeClass('fileActive').removeClass('moveCloudBlockDragHover').addClass('moveCloudBlock');
        });
        return false;
    }else {
        // $('.draghover').each(function(){
        //     $(this).removeClass('draghover').addClass('dropabbleParent');
        // })
        // $('#moveSource').find('input').prop('checked',false);
        // $('#moveSource').find('.ui-selectee').each(function(){
        //     $(this).removeClass('fileActive');
        // })
        // $('#moveDestination div').removeClass('rootDragHover');
        //return ui.draggable.draggable('option', 'revert', false);
        ui.draggable.draggable('option', 'revert', false);
    }
    fromcid = $(htmlCont).attr('cid');
    fromcid = fromcid.split(",");
    for (var i = 0; i < fromfid.length; i++) {
        $('#moveSource [id="' + fromfid[i] + '"]').children('input').attr('checked', 'true');
        $('#moveSource span[id="' + fromfid[i] + '"]').addClass('fileActive');
    }
    /*Check Page State and set Page Name*/
    var test = $(this).find('input').attr('name');
    if (test == 'destCloud') {
        PageName = 'moveLanding';
        tofid = $(this).find('.move').attr('pid');
        tocid = $(this).find('.move').attr('cid');
        $(this).find('input').attr('checked', true);
    }else if (test == 'srcfile') {
        if ($('#dynamicDestCloudName').attr('check') == 'droot') {
            PageName = 'move';
        }else {
            PageName = 'innermove';
        }
        test = $('#dynamicDestCloudName').attr('check');
        $('#dynamicDestCloudName').removeAttr('check');
        $('#dynamicDestCloudName').attr('recheck',test);
        tofid = $(this).find('label').attr('id');
        tocid = $(this).find('label').attr('cid');
        $(this).find('input').attr('checked', true);
    }
    if (PageName == 'moveLanding') {
        tofid = $(this).find('.move').attr('pid');
        tocid = $(this).find('.move').attr('cid');
        //$(this).find('.move').parent().addClass('cloudActive')
        $('#moveDestination .dropabbleParent[id="' + tocid + '"]').children('input').attr('checked', 'true');
        $('#moveDestination .move[cid="' + tocid + '"]').prev('input').attr('checked', 'true');
        $('#moveDestination .ui-droppable').each(function () {
            $(this).removeClass('moveCloudBlockDragHover').addClass('moveCloudBlock')
        });
    }
    if ($('input:checked').length > 0) {
        if ($('[type="radio"]:checked').length > 0) {
            $('#movecheckModal').removeAttr('disabled').addClass('blue');
        }
        if ($('[type="checkbox"]:checked').length == 0) {
            $('#movecheckModal').prop('disabled', true).removeClass('blue');
        }
    }
    if (PageName == 'moveLanding') {
        $('#moveDestination .ui-droppable.button').each(function () {
            $(this).removeClass('fileActive');
        });
        $('[cid="' + tocid + '"]').parent('.ui-droppable.button').addClass('fileActive');
    }
    if (PageName == 'move' || PageName == 'innermove') {
        $('#moveDestination .list-group-item').each(function () {
            $(this).removeClass('fileActive').removeClass('draghover');
        });
        $('#moveDestination .ui-droppable[id="' + tofid + '"]').addClass('fileActive').addClass('dropabbleParent');
    }
    $('#movecheckModal').trigger('click');
    $('#movecheckModal').prop('disabled', true).removeClass('blue');
}
function rootDropEventHandler(e,ui){
    sendGAEvents("Drop in Root");
    //_gaq.push(['_trackEvent',"Drop in Root", localStorage.getItem('UserId')]);
    var test = $('#dynamicDestCloudName').attr('recheck');
    if(test != undefined || test != null){
        $('#dynamicDestCloudName').attr('check',test);
        $('#dynamicDestCloudName').removeAttr('recheck');
    }
    var tofid = '';
    var tocid = '';
    var fromfid = [];
    var fromcid = [];
    var htmlCont = '';
    htmlCont = ui.helper[0].outerHTML;
    fromfid = $(htmlCont).attr('fid');
    fromfid = fromfid.split(",");
    if(fromfid[0]== ""){
        ui.draggable.draggable('option', 'revert', true);
        $('#moveSource input,#moveDestination input').attr('checked', false);
        $('#moveDestination').find('.draghover').addClass('dropabbleParent').removeClass('draghover');
        $("#moveDestination").find(".list-group-item").each(function () {
            $(this).removeClass('fileActive');
        });
        $('#moveDestination').children('.button.ui-droppable').each(function () {
            $(this).removeClass('fileActive').removeClass('moveCloudBlockDragHover').addClass('moveCloudBlock');
        });
        return false;
    }else{
        ui.draggable.draggable('option', 'revert', false);
    }
    $('#moveDestination').find('span.fileActive').removeClass('fileActive');
    $('input[type="radio"]:checked').prop('checked',false);
    $('#movecheckModal').trigger('click');
}

