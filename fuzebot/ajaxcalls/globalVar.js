var notifyTime = 4000;
var notifyError = 2000;
var MonthlyAMT = 4.99;
var YearlyAMT = 49.99;
var pmntack1;
var pmntack;
var pmntpage;
var lastpmntresponse;
var lastsubsresponse;
var subsresponse;
var cmpletepmntresponse;
var completeresponse;
var cfinternalpmntid;
var Onetime;
var paypal1;
var paypaltoken;
var paypalpayerid;
var paypaldate;
var Monthly;
var Yearly;
var DiscAMT = 0 ;
var syncing;
var WorkSpaceId;
var spinner;
var cid = 0;
var count=1;
var fid = 0;
var CloudName = [];
var PrimaryEmail = [];
var CloudId = [];
var CLParent = [];
var CLDisplayName = [];
var AllFilesForAllClouds;
var PageNumber = 1;
var PageSize=5;
var commentsCount;
var FromfileId = [];
var FilePer = [];
var FromWorkspaceName = [];
var downloadfile = [];
var FromcloudId = [];
var FromObjectName = [];
var Extention = [];
var FileType = [];
var InnerCloudId = [];
var InnerFolderId = [];
var InnerObjectName = [];
var FromShareUrl =[];
var SingleCloudId;
var SinglePId;
var SingleObjectName;
var file;
var CFCreateCloudName;
var moveCheckSum = '';
var searchTerm;
var homeCid=0;
var previousPage;
var mailsUnames={};
var sharemailsUnames=[];
var AllCloudsInfo;
var Files= [];
var spinnerClass;
var viewTrack = "LView";
var uploadPageTrack;
var mainProgress = 0;
var previousFileProgressSts ;
var HomePageOnload = "Home";
var globalCategoryId;
var slimScrollHeight;
var CfShareInfo = '';
var urlParameterObject = {
	"isAscen":"false",
	"fetchCollabInfo":"true",
	"folderId":"",
	"isAllFiles":"true",
	"orderField":"modifiedTime"
};
var PageName = '';
var commentInfo;
var wsdetails, wsDetails;
var categoryName;
var OOBCategory=['Photos','Music','Videos','Documents','Others'];
var newCategory=false;
var allCategoryData=[];
var CLName = {
    "ALFRESCO": "Alfresco",//ECM
    "AMAZON": "Amazon S3",
    "AMAZON_STORAGE": "Amazon Cloud Drive",
    "AXWAY": "Axway",
    "AZURE_OBJECT_STORAGE": "Azure",//ECM
    "BOX": "Box",
    "CIFS": "CIFS",
    "CLOUDIAN": "Cloudian",//ECM
    "CMIS": "CMIS",//ECM
    /*'COPY':'Copy Cloud',*/
    "DOCUMENTUM": "Documentum",//ECM
    "DROP_BOX": "Dropbox",
    "DROPBOX_BUSINESS": "Dropbox Business",
    "EGNYTE_STORAGE": "Egnyte",
    "FTP": "FTP",
    "G_DRIVE": "Google Drive",
    "GENERIC_OBJECT_STORAGE": "Object Storage",//ECM
    "NTLM_STORAGE": "NFS",//ECM
    "ONEDRIVE": "OneDrive",
    "ONEDRIVE_BUSINESS": "OneDrive for Business",
    "ORANGE": "Orange",
    "SALES_FORCE": "SalesForce",
    "SHAREFILE": "Citrix ShareFile",
    "SHAREPOINT_2010": "SharePoint 2010",//ECM
    "SHAREPOINT_2013": "SharePoint 2013",//ECM
    "SUGAR_SYNC": "SugarSync",
    "WALRUS": "Eucalyptus",//ECM
    "WEBDAV": "WebDav",//WEBDAV
    "YANDEX_DISK": "Yandex",
    "CENTURYLINK": "CenturyLink",
    "SHAREPOINT_ONLINE":"SharePoint Online"
};
var angle=0;
var moveDestParent;
var PreviewP;
var clipboardURL = new ZeroClipboard($("#copy"));
var VersionFileId=[];
var versionFileType=[];
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
var multiEmailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4},?)+$/;
var sharedFolderId;
var actionPanel = {
    "open":true,
    "download":true,
    "share":true,
    "rename":true,
    "delete":true,
    "category":true,
    "workspace":true,
    "folder":true,
    "upload":true
};
var heartFill = "cf-heart22";
var heart = "cf-heart32";