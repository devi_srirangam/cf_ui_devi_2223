var hostName = window.location.hostname;
var defaultDomain = null;
var isDevweb =/devweb/i.test(hostName);
var isSLWeb = /slweb/i.test(hostName);
var isWebappCheck = /webapp/i.test(hostName);
var isQA = /qaweb/i.test(hostName);
var isOnpremise = false;
var isProd = false;
var apicallurl = "",redirectUrl = "",GA = "",apiboturl="";
// TODO make same variables useful to all environments - Mike (Done)
if(isDevweb) {
    defaultDomain = "https://devwebapp.cloudfuze.com/";
    redirectUrl = "https://devwebapp.cloudfuze.com";
    GA = "UA-56350345-1";
    isProd = false;
}else if(isQA) {
    defaultDomain = "https://qawebapp.cloudfuze.com/";
    redirectUrl = "https://qawebapp.cloudfuze.com";
    GA = "UA-56350345-1";
    isProd = false;
}
else if(isSLWeb) {
    defaultDomain = "https://slwebapp.cloudfuze.com/";
    redirectUrl = "https://devwebapp.cloudfuze.com";
    GA = "UA-56350345-1";
    isProd = false;
}else if(isWebappCheck == false){
    isOnpremise = true;
    defaultDomain = "https://"+hostName+"/";
    redirectUrl = defaultDomain;
    GA = "UA-56350345-1";
    isProd = true;
}else {
    defaultDomain = "https://webapp.cloudfuze.com/";
    redirectUrl = "https://webapp.cloudfuze.com";
    GA = "UA-37344062-1";
    isProd = true;
}
var domainUrl = "https://"+hostName+"/";
apicallurl = "https://" + hostName + "/botproxy/v1";
apiboturl="https://" + hostName + "/botproxy/v1";

var CFInterConvertTimeUtility = {
    Ptstamptomillsec: function (timestmp) {
        var myDate = new Date(timestmp);
        var result = myDate.getTime();
        return result;
    }
};

// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
//         (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
//     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
// ga('create', GA, 'auto');
// ga('send', 'pageview');

//google analytics for fuzebot
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-96987091-1', 'auto');
ga('send', 'pageview');


/*var _gaq = _gaq || [];
_gaq.push(['_setAccount', GA]);// TODO make GA vars to all environments - Mike (Done)
_gaq.push(['_trackPageview']);
_gaq.push(['_addIgnoredRef', 'www.cloudfuze.com']);
(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();*/


if (window.addEventListener) {
    window.addEventListener('error', trackJavaScriptError, false);
}else if (window.attachEvent) {
    window.attachEvent('onerror', trackJavaScriptError);
}else {
    window.onerror = trackJavaScriptError;
}
var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
            || this.searchVersion(navigator.appVersion)
            || "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i=0;i<data.length;i++)	{
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        }
    ],
    dataOS : [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows"
        },
        {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        },
        {
            string: navigator.userAgent,
            subString: "iPhone",
            identity: "iPhone/iPod"
        },
        {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }
    ]

};
BrowserDetect.init();
function trackJavaScriptError(e) {
    var _msg = e.filename+' : '+e.lineno+' - '+BrowserDetect.browser+' Ver: '+BrowserDetect.version+' OS: '+BrowserDetect.OS;
    //_gaq.push(['_trackEvent', "JavaScript Error" , e.message, _msg]);//Old Code
    ga('send', 'event', "JavaScript Error" , e.message, _msg); //New Code
}

/*GA Events Tracking*/
function sendGAEvents(a,b){
    if(b == null || b == undefined){
        b = PageName;
    }
    //_gaq.push(['_trackEvent', a , localStorage.getItem('UserId'),b]); //OLD Code
    var _usrId=localStorage.getItem('UserId');
    if(_usrId==null)
        _usrId="Login Page";
    ga('send', 'event', a,_usrId,b); // New Code
}

function sendPageView(a){
    ga('send', 'pageview', a); // New Code
}