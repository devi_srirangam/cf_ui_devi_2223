/**
 * Created by VINAY on 08-11-2016.
 */



$('#paypalPmntBtn').on('click',function(){
    $('#CFmoveStatus').modal('hide');
    var _a = {
        "amount":null,
        "code":"USD",
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    };
    var _b;
    var _chkd=$('#CFmoveStatus input[name=subscription]:checked').val();
    if(_chkd =='AdvncdAnnually')
    {
        localStorage.setItem('YearlyAMNT', 199.99);
        _a.amount = 199.99;
        _b="CloudFuze+Advanced+Yearly+Subscription";
        sendGAEvents("Advanced Yearly Subscription Move Limit popup");
        //paypalPayments.createPmntAnnualRecurring(_a,_b);
    }
    else if(_chkd =='StndAnnually')
    {
        localStorage.setItem('YearlyAMNT', 99.99);
        _a.amount = 99.99;
        _b="CloudFuze+Standard+Yearly+Subscription";
        sendGAEvents("Standard Yearly Subscription Move Limit popup");
        //paypalPayments.createPmntAnnualRecurring(_a,_b);
    }
    else if(_chkd =='BasicAnnually')
    {
        localStorage.setItem('YearlyAMNT', 49.99);
        _a.amount = 49.99;
        _b="CloudFuze+Basic+Yearly+Subscription";
        sendGAEvents("Basic Yearly Subscription Move Limit popup");
        //paypalPayments.createPmntAnnualRecurring(_a,_b);
    }
    else if(_chkd =='AdvncdMonthly')
    {
        localStorage.setItem('MonthlyAMNT', 19.99);
        _a.amount = 19.99;
        _b="CloudFuze+Advanced+Monthly+Subscription";
        sendGAEvents("Advanced Monthly Subscription Move Limit popup");
        //paypalPayments.createPmntMonthlyRecurring(_a,_b);
    }
    else if(_chkd =='StndMonthly')
    {
        localStorage.setItem('MonthlyAMNT',9.99);
        _a.amount = 9.99;
        _b="CloudFuze+Standard+Monthly+Subscription";
        sendGAEvents("Standard Monthly Subscription Move Limit popup");
        //paypalPayments.createPmntMonthlyRecurring(_a,_b);
    }
    else
    {
        localStorage.setItem('MonthlyAMNT', 4.99);
        _a.amount = 4.99;
        _b="CloudFuze+Basic+Monthly+Subscription";
        sendGAEvents("Basic Monthly Subscription Move Limit popup");
        //paypalPayments.createPmntMonthlyRecurring(_a,_b);
    }
    localStorage.setItem('CFSubscribtion',_b);
    localStorage.setItem('CFPrice',_a.amount);
   couponCheck();
});
var paypalPayments={
    createPmntMonthlyRecurring: function (a,b)
    {
        var d = new Date();
        var _time = d.toISOString();
        sendGAEvents("Requesting token from paypal",_time);
        $('#paypalsuccessmodalinit').modal('show');
        createdata = "method=SetExpressCheckout&" +
            "PAYMENTREQUEST_0_AMT=" + a.amount + "&" +
            "PAYMENTREQUEST_0_CURRENCYCODE="+ a.code+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "L_BILLINGTYPE0=RecurringPayments&" +
            "L_BILLINGAGREEMENTDESCRIPTION0="+b+"&" +
            "returnUrl=" + pmntsreturnurl + "%3F"+ a.success+"%3Dtrue%26M%3Dtrue&" +
            "cancelUrl=" + pmntsreturnurl + "%3F"+ a.cancel+"%3Dtrue&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "L_PAYMENTREQUEST_0_NAME0="+b+"&" +
            "L_PAYMENTREQUEST_0_AMT0=" + a.amount + "&" +
            "L_PAYMENTREQUEST_0_QTY0=1&SOLUTIONTYPE=Sole&LANDINGPAGE=Billing";
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (createpmnttoken) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var pmntresponse = xhr.responseText;
                    //console.log(pmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl," "));
                        },
                        query = pmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query)){
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    }
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    localStorage.setItem('paypaltstamp', pmnttimestamp);
                    var paypalcreateurl = paymentcreateurl + pmnttoken;
                    if(pmnttoken == undefined || pmnttoken == null){
                        $('#paypalsuccessmodalinit').modal('hide');
                        return showNotyNotification('error','Failed to initiate payment with PayPal.');
                    }
                    window.location.href = paypalcreateurl;
                }
            }
        });
    },
    createPmntAnnualRecurring: function (a,b) {
        var d = new Date();
        var _time = d.toISOString();
        sendGAEvents("Requesting token from paypal",_time);
        $('#paypalsuccessmodalinit').modal('show');
        createdata = "method=SetExpressCheckout&" +
            "PAYMENTREQUEST_0_AMT=" + a.amount+ "&" +
            "PAYMENTREQUEST_0_CURRENCYCODE="+ a.code+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "L_BILLINGTYPE0=RecurringPayments&" +
            "L_BILLINGAGREEMENTDESCRIPTION0="+b+"&" +
            "returnUrl=" + pmntsreturnurl + "%3F"+ a.success+"%3Dtrue%26Y%3Dtrue&" +
            "cancelUrl=" + pmntsreturnurl + "%3F"+ a.cancel+"%3Dtrue&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "L_PAYMENTREQUEST_0_NAME0="+b+"&" +
            "L_PAYMENTREQUEST_0_AMT0=" + a.amount + "&" +
            "L_PAYMENTREQUEST_0_QTY0=1";
        //createpmntUrl = "https://devwebapp.cloudfuze.com/pmntrecurring/nvp" ;
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (createpmnttoken) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var pmntresponse = xhr.responseText;
                    //console.log(pmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = pmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query))
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    opmnttoken = pmntcreateParams["TOKEN"];
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    localStorage.setItem('paypaltstamp', pmnttimestamp);
                    var paypalcreateurl = paymentcreateurl + pmnttoken;
                    if(opmnttoken == undefined || opmnttoken == null){
                        $('#paypalsuccessmodalinit').modal('hide');
                        return showNotyNotification('error','Failed to initiate payment with PayPal.');
                    }
                    window.location.href = paypalcreateurl;
                }
                if (xhr.status > 300) {
                    //console.log("Operation Failed");
                }
            },
            error: function (error) {
            }
        });
    },
    cmpleteMnthlyPmntRecurring: function (_c) {
        var d = new Date();
        var _time = d.toISOString();
        sendGAEvents("Connecting paypal with payment token",_time);
        var _b=localStorage.getItem('CFSubscribtion');
        createdata = "method=CreateRecurringPaymentsProfile&" +
            "AMT=" + _c.amount + "&" +
            "CURRENCYCODE="+_c.country+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "L_BILLINGTYPE0=RecurringPayments&" +
            "DESC="+_b+"&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "token=" + _c.token + "&" +
            "PAYERID=" + _c.id + "&" +
            "PROFILESTARTDATE=" + _c.time + "&" +
            "BILLINGPERIOD=Month&" +
            "BILLINGFREQUENCY=1&" +
            "MAXFAILEDPAYMENTS=1&" +
            "INITAMT=" + _c.amount + "&" +
            "TRIALTOTALBILLINGCYCLES=1&" +
           "TRIALBILLINGPERIOD=Month&"+
           "TRIALBILLINGFREQUENCY=1";
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (cmpltepmntotime) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var cmpletepmntresponse = xhr.responseText;
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = cmpletepmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query))
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    pmntprofid = pmntcreateParams["PROFILEID"];
                    pmntack = pmntcreateParams["ACK"];
                    $('#transstatus').find('#contclick').attr('instance',_c.instance);
                    if (pmntack == "Success") {
                        $('#transstatus').find('#contclick').attr('status','success');
                        pmntack = "PAID";
                        var intpmntid = CFPaymentsInternal.pmntmonthlyrecurring(pmntack, pmnttimestamp, _c.id, pmntprofid, cmpletepmntresponse);
                        //var subname = "Monthly Recurring";

                       /* if(_b == "CloudFuze+Basic+Monthly+Subscripton")
                            _b= "Basic Monthly Subscription";
                        else if(_b == "CloudFuze+Standard+Monthly+Subscripton")
                            _b= "Standard Monthly Subscription";
                        else if(_b == "CloudFuze+Advanced+Monthly+Subscripton")
                            _b= "Advanced Monthly Subscription";*/
                        _b=(_b.replace(new RegExp('[+]','g')," ").split('CloudFuze').pop().trim());

                        var subname =_b.replace(/\+/g," ").replace("CloudFuze ","");
                        var status1 = "ACTIVE";
                        var stype = "PAY_MONTHLY";
                        var expdate = getuserexpiry.searchuserexpiry(localStorage.getItem('UserId'));
                        var expdate_expiresIn = expdate[0].expiresIn;
                        var expdate_createdDate = expdate[0].createdDate;
                        var etstamp;
                        if (expdate_expiresIn == null) {
                            etstamp = expdate_createdDate + (30 * 24 * 60 * 60 * 1000);
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }
                        else {
                            //etstamp = expdate_expiresIn;
                            var myDate = new Date();
                            var result = myDate.getTime();
                            etstamp = result + (30 * 24 * 60 * 60 * 1000);
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }

                        if(_c.instance == "orange"){
                            $('#transstatustext').text('Thank you for choosing CloudFuze. Your payment was successful.');
                        }
                        else{
                            $('#transstatustext').text('Thank you for choosing CloudFuze. Your payment was successful.');
                        }
                    }
                    else if (pmntack == "Failure") {
                        $('#transstatus').find('#contclick').attr('status','failed');
                        pmntack = "PENDING";
                        $('#transstatustext').html('Your payment was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
                        $('#transbutton').text('Please try again');
                    }
                }
            }
        });
        return pmntack;

    },
    cmpleteAnnualPmntRecurring: function (_a) {
        var d = new Date();
        var _time = d.toISOString();
        sendGAEvents("Connecting paypal with payment token",_time);
        var _b=localStorage.getItem('CFSubscribtion');
        createdata = "method=CreateRecurringPaymentsProfile&" +
            "AMT=" + _a.amount + "&" +
            "CURRENCYCODE="+_a.country+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "L_BILLINGTYPE0=RecurringPayments&" +
            "DESC="+_b+"&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "token=" + _a.token + "&" +
            "PAYERID=" + _a.id + "&" +
            "PROFILESTARTDATE=" + _a.time + "&" +
            "BILLINGPERIOD=Year&" +
            "BILLINGFREQUENCY=1&" +
            "MAXFAILEDPAYMENTS=1&" +
            "INITAMT=" + _a.amount + "&" +
            "TRIALTOTALBILLINGCYCLES=1&" +
            "TRIALBILLINGPERIOD=Year&"+
            "TRIALBILLINGFREQUENCY=1";
        $('#transstatus').find('#contclick').attr('instance',_a.instance);
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (cmpltepmntotime) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    $('#transstatus').find('#contclick').attr('status','success');
                    var cmpletepmntresponse = xhr.responseText;
                    //console.log(cmpletepmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = cmpletepmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query))
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    pmntprofid = pmntcreateParams["PROFILEID"];
                    pmntack = pmntcreateParams["ACK"];
                    if (pmntack == "Success") {
                        pmntack = "PAID";
                        if ($('#subsname').text() == "Monthly Recurring") {
                            if (lastpmntresponse.recurringId && lastpmntresponse.referenceId == "") {
                                var status4 = CFPaymentsPaypal.cnclRecurringSubscription(lastpmntresponse.recurringId);
                                //console.log("createannualpmnt");
                                //console.log(status4);id
                                if (status4 == "Success") {
                                    CFSubscriptionsInternal.CancelSubscription(lastsubsresponse.id);
                                }
                            }
                        }
                        var intpmntid = CFPaymentsInternal.pmntannualrecurring(pmntack, pmnttimestamp, _a.id, pmntprofid, cmpletepmntresponse);
                        //var subname = "Yearly Recurring";

                        var subname = _b.replace(/\+/g," ").replace("CloudFuze ","");

                        var status1 = "ACTIVE";
                        var stype = "PAY_YEARLY";
                        var expdate = getuserexpiry.searchuserexpiry(localStorage.getItem('UserId'));
                        var expdate_expiresIn = expdate[0].expiresIn;
                        var expdate_createdDate = expdate[0].createdDate;
                        var etstamp;
                        if (expdate_expiresIn == null) {
                            etstamp = expdate_createdDate + (365 * 24 * 60 * 60 * 1000);
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        } else {
                            var myDate = new Date();
                            var result = myDate.getTime();
                            etstamp = result + (365 * 24 * 60 * 60 * 1000);
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }
                        $('#transstatustext').text('Thank you for choosing CloudFuze. Your payment was successful.');
                    }
                    else if (pmntack == "Failure") {
                        pmntack = "PENDING";
                        $('#transstatus').find('#contclick').attr('status', 'failed');
                        $('#transstatustext').html('Your payment was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
                        $('#transbutton').text('Please try again');
                    }
                }
            }
        });
        return pmntack;
    },
}
$('#basicannualsubscribe').on('click',function(){
    var _a = {
        "amount":49.99,
        "code":"USD",
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    }, _b;
        localStorage.setItem('YearlyAMNT', 49.99);
         _b="CloudFuze+Basic+Yearly+Subscription";
         sendGAEvents("Basic Yearly Subscription upgrade button");
        localStorage.setItem('CFSubscribtion',_b);
    localStorage.setItem('CFPrice',_a.amount);
        //paypalPayments.createPmntAnnualRecurring(_a,_b);
   couponCheck();
});
$('#stndannualsubscribe').on('click',function(){
    var _a = {
        "amount":99.99,
        "code":"USD",
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    }, _b;
    localStorage.setItem('YearlyAMNT', 99.99);
    _b="CloudFuze+Standard+Yearly+Subscription";
    sendGAEvents("Standard Yearly Subscription upgrade button");
    localStorage.setItem('CFSubscribtion',_b);
    localStorage.setItem('CFPrice',_a.amount);
    //paypalPayments.createPmntAnnualRecurring(_a,_b);
   couponCheck();
});
$('#advncdannualsubscribe').on('click',function(){
    var _a = {
        "amount":199.99,
        "code":"USD",
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    }, _b;
    localStorage.setItem('YearlyAMNT', 199.99);
    _b="CloudFuze+Advanced+Yearly+Subscription";
    sendGAEvents("Advanced Yearly Subscription upgrade button");
    localStorage.setItem('CFSubscribtion',_b);
    localStorage.setItem('CFPrice',_a.amount);
    //paypalPayments.createPmntAnnualRecurring(_a,_b);
   couponCheck();
});
$('#basicmonthlysubscribe').on('click',function(){
    var _a = {
        "amount":4.99,
        "code":"USD",
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    }, _b;
    localStorage.setItem('MonthlyAMNT', 4.99);
    _b="CloudFuze+Basic+Monthly+Subscription";
    sendGAEvents("Basic Monthly Subscription upgrade button");
    localStorage.setItem('CFSubscribtion',_b);
    localStorage.setItem('CFPrice',_a.amount);
    //paypalPayments.createPmntMonthlyRecurring(_a,_b);
   couponCheck();
});
$('#stndmonthlysubscribe').on('click',function(){
    var _a = {
        "amount":9.99,
        "code":"USD",
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    }, _b;
    localStorage.setItem('MonthlyAMNT', 9.99);
    _b="CloudFuze+Standard+Monthly+Subscription";
    sendGAEvents("Standard Monthly Subscription upgrade button");
    localStorage.setItem('CFSubscribtion',_b);
    localStorage.setItem('CFPrice',_a.amount);
    //paypalPayments.createPmntMonthlyRecurring(_a,_b);
   couponCheck();
});
$('#advncdmonthlysubscribe').on('click',function(){
    var _a = {
        "amount":19.99,
        "code":"USD",
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    }, _b;
    localStorage.setItem('MonthlyAMNT', 19.99);
    _b="CloudFuze+Advanced+Monthly+Subscription";
    sendGAEvents("Advanced Monthly Subscription upgrade button");
    localStorage.setItem('CFSubscribtion',_b);
    localStorage.setItem('CFPrice',_a.amount);
    //paypalPayments.createPmntMonthlyRecurring(_a,_b);
   couponCheck();
});

$('#cnclbasicmonthlysubscribe').on('click', function() {
    $('#cnclmonthlysubscribemodal').modal('show');
});
$('#cnclstndmonthlysubscribe').on('click', function() {
    $('#cnclmonthlysubscribemodal').modal('show');
});
$('#cncladvncdmonthlysubscribe').on('click', function() {
    $('#cnclmonthlysubscribemodal').modal('show');
});

$('#cnclbasicannualsubscribe').on('click', function(){
    $('#cnclannualsubscribemodal').modal('show');
});
$('#cnclstndannualsubscribe').on('click', function(){
    $('#cnclannualsubscribemodal').modal('show');
});
$('#cncladvncdannualsubscribe').on('click', function(){
    $('#cnclannualsubscribemodal').modal('show');
});
$('#checkoutbuttonNew').on('click', function() {
    sendGAEvents("proceed to checkout");
    $("#calcpricemodalNew").modal('hide');
    var _a = {
        "amount":localStorage.getItem('CFPrice'),
        "code":"USD",
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    };
    var _b=localStorage.getItem('CFSubscribtion');
    if (_b.indexOf("Yearly") >= 0)
        paypalPayments.createPmntAnnualRecurring(_a,_b);
    else
        paypalPayments.createPmntMonthlyRecurring(_a,_b);
});
function couponCheck() {
    var _s=localStorage.getItem('CFSubscribtion'),_p=localStorage.getItem('CFPrice'),_parent=$('#calcpricemodalNew');
    _s=_s.replace(/\+/g," ").replace("CloudFuze ","");
    $(_parent).find('#subscribetypeNew').text(_s);
    $(_parent).find('#amntNew').text(_p);
    $(_parent).find('#discountNew').text(0);
    $(_parent).find('#totalamntNew').text(_p);
    $('#applycouponNew').addClass('blue');
    $('#applycouponNew').on('click', enableCoupon);
    $("#calcpricemodalNew").modal('show');
}
var enableCoupon = function(event)
{
    var _parent=$('#calcpricemodalNew'),_price;
    if($("#couponcode").val() == "CF20")
    {
        var price=localStorage.getItem('CFPrice'),_price=((price*80)/100).toFixed(2);
        $(_parent).find('#discountNew').text(price-_price);
        $(_parent).find('#totalamntNew').text(_price);
        localStorage.setItem('CFPrice',_price)
        if (localStorage.getItem('YearlyAMNT') != null)
            localStorage.setItem('YearlyAMNT',_price);
        else if (localStorage.getItem('MonthlyAMNT') != null)
            localStorage.setItem('MonthlyAMNT',_price);
        $('#couponsmessage').text('Your Coupon has been Applied Successfully');
        $("#applycouponNew").off( "click" ).removeClass("blue");
    }
    else if($("#couponcode").val() == "") {
        $('#couponemessage').text('Please enter coupon code');
        $("#couponemessage").css({"display":"block"});
        $('#couponsmessage').text('');
    }
    else if($("#couponcode").val() == "PLEXQ1" && localStorage.getItem('CFSubscribtion').includes("Advanced")) {//PLEX2016
        if (localStorage.getItem('CFSubscribtion').includes("Yearly"))
        {
            $(_parent).find('#discountNew').text(80.0);
            $(_parent).find('#totalamntNew').text(119.9);
            localStorage.setItem('YearlyAMNT',119.9);
            localStorage.setItem('CFPrice',119.9);
        }
        else  if (localStorage.getItem('CFSubscribtion').includes("Monthly"))
        {
            $(_parent).find('#discountNew').text(0);
            $(_parent).find('#amntNew').text(24.99);
            $(_parent).find('#totalamntNew').text(24.99);
            localStorage.setItem('MonthlyAMNT',24.99);
            localStorage.setItem('CFPrice',24.99);;
        }
        $("#couponemessage").css({"display":"none"});
        $('#couponsmessage').text('Your coupon has been applied successfully. You now have unlimited migration bandwidth !');
        //$('#couponsmessage1').text('Your Coupon has been Applied Successfully');
        //$('#couponsmessage').text('Your Coupon has been Applied Successfully');
        $("#applycouponNew").off( "click" ).removeClass("blue");
    }
    else
    {
        $('#couponemessage').text('Invalid coupon code');
        $("#couponemessage").css({"display":"block"});
        $('#couponsmessage').text('');
    }
}



//Link Spark Account
$('#accsettings').on('change','#data_spark_status',function() {
    if ($('#data_spark_status').prop('checked')) {
        $('#data_spark_status').prop('checked', false);
        getHost(domainUrl);
        sendGAEvents(ADD_CLOUD, "Cisco Spark");
        window.open(redirectURL + 'CISCO_SPARK', '_blank', 'width=750px,height=750px');
    } else {
        var _id = $('#data_spark_status').attr('data-cid');
        var _result = CFManageCloudAccountsAjaxCall.deleteSpark(_id);
        if (_result) {
            CFManageCloudAccountsAjaxCall.showNotification('notify', 'Spark unlink successful.');
            $('#accsettings').find('[data-spark-user]').text('');
        } else {
            CFManageCloudAccountsAjaxCall.showNotification('error', 'Spark unlink failed.');
        }
    }
});


//Spark Configurations

function sparkConfig(){
    var _parent = $('#spark_integration'),
        _pp = $('#spark_sync').find('ul'),
        _child = _parent.find('#all_clouds_container'),
        _spark_pair = $('#spark_pair'),
        _pair_rooms = _spark_pair.find('[data-container="rooms"]'),
        _pair_clouds = _spark_pair.find('[data-container="clouds"]'),
        _g_parent = $('#spark_cloud_container'),
        _sync_count = 0;
    _child.removeAttr('data-syncfolder');
    _parent.show();

    _spark_pair.find('.spark_sync_container').removeClass('fa-spin');

    _child.html('');
    _pp.html('');
    _pair_rooms.html('');
    _pair_clouds.html('');
    _pair_clouds.prev().remove();

    $('#show_spark_pair').find('i').removeClass('cf-sort-up');

    _spark_pair.addClass('hidden');

    $('#syncSpark').addClass('disabled');
    $('#infotitle').text("Cisco Spark Sync");

    // var _sparkCloud = CFManageCloudAccountsAjaxCall.getSpark();
    //
    // if(_sparkCloud != null){
    //     _parent.find('#data_spark_status').prop('checked',true);
    //     _parent.find('[data-spark-user]').text(_sparkCloud.userDisplayName);
    //     _parent.find('#data_spark_status').attr('data-cid',_sparkCloud.id);
    //     _g_parent.find('.col-sm-12').not(':last-child').show();
    // }else{
    //     _parent.find('#data_spark_status').prop('checked',false);
    //     _parent.find('[data-spark-user]').text('Inactive');
    //     _g_parent.find('.col-sm-12').not(':first-child').hide();
    //     //return _pp.html('<h5 style="font-weight:bold">Please go to My Account and link your Spark account to your CloudFuze account.</h5>');
    // }

    var _sparkRooms = CFManageCloudAccountsAjaxCall.getSparkRooms();
    // _sparkRooms = [
    //     {
    //         created:"2017-01-05T08:15:48.777Z",
    //         id:"Y2lzY29zcGFyazovL3VzL1JPT00vMmE0MWExOTAtZDMxZi0xMWU2LThiZjktYjM2YzNjZTRkZDk5",
    //         isLocked:false,
    //         lastActivity:"2017-01-05T08:15:48.797Z",
    //         length:3,
    //         members:["vinay.relangi@cloudfuze.com","DevFuzeBot@sparkbot.io","FuzeBot@sparkbot.io"],
    //         roomSyncInfo:null,
    //         sipAddress:null,
    //         title:"DevFuzeBot",
    //         type:"group"
    //     },
    //     {
    //         created:"2017-01-05T08:15:48.777Z",
    //         id:"Y2lzY29zcGFyazovL3VzL1JPT00vMmE0MWExOTAtZDMxZi0xMWU2LThiZjktYjM2YzNjZTRkZDk5",
    //         isLocked:false,
    //         lastActivity:"2017-01-05T08:15:48.797Z",
    //         length:7,
    //         members:["vinay.relangi@cloudfuze.com","DevFuzeBot@sparkbot.io","FuzeBot@sparkbot.io","vinay.relangi@cloudfuze.com","DevFuzeBot@sparkbot.io","FuzeBot@sparkbot.io","vinay.relangi@cloudfuze.com"],
    //         roomSyncInfo:null,
    //         sipAddress:null,
    //         title:"DevTest",
    //         type:"group"
    //     }
    // ]
    var value=true;
    $.each(_sparkRooms,function(i,e){
        var _members = '',
            _syncStatus = '',
            _myClouds = '<button style="width:100px;margin-top:8px;display: block" class="btn btn-primary" data-action="show-clouds-drop">My Clouds</button>',
            _cname = '',
            _d_name='';

        $.each(e.members,function(i,f){
            _members += f+'\n';
        });

        if(e.roomSyncInfo != null){
            _sync_count ++;
            var _spin = e.roomSyncInfo.loadLock == true ? 'fa-spin': '';
            var _cloudDrive = {};

            $.each(AllCloudsInfo,function(i,g) {
                if (g.id == e.roomSyncInfo.cloudId) {
                    var src = '../img/drive/'+g.cloudName+'.png';
                    _cloudDrive = g;
                    _d_name = g.userDisplayName == null ? g.cloudUserId.split('|').pop() : g.userDisplayName;
                    return _cname = '<img src="'+src+'" style="width:36px;margin-right:5px;">';
                }
            });

            var _folderName = e.roomSyncInfo.folderName != null ? e.roomSyncInfo.folderName : '';
            //var _mail_ID = CFManageCloudAccountsAjaxCall.getMaxChars(_d_name.split("@")[0],9)  + "@" + CFManageCloudAccountsAjaxCall.getMaxChars(_d_name.split("@")[1],7);
            var _mail_ID = CFManageCloudAccountsAjaxCall.getMaxChars(_d_name,10);
            if(CLName[_cloudDrive.cloudName] == undefined){
                _myClouds = '<div style="height:80px;margin-right:10px;float:left;width:100px;display: block;cursor:pointer;text-align: center" data-id="'+e.roomSyncInfo.cloudId+'">'+_cname +'<p>'+ _mail_ID +'</p></div>';

                _myClouds += '<div class="spark_dest_display"><i class="cf-toggle-right"></i>Not-Available</div>';

                _syncStatus = '<span class="spark_sync_container btn btn-success disabled'+_spin+'" data-toggle="tooltip" data-original-title="Resync '+e.title+'"><i class="cf-refresh4"></i></span>';
            }else{
                value=false;
                _myClouds = '<div style="height:80px;margin-right:10px;float:left;width:100px;display: block;cursor:pointer;text-align: center" data-action="show-clouds-drop" data-id="'+e.roomSyncInfo.cloudId+'">'+_cname +'<p>'+ _mail_ID +'</p></div>';

                _myClouds += '<div class="spark_dest_display"><i class="cf-toggle-right"></i>'+CLName[_cloudDrive.cloudName]+'('+_d_name+') / <span data-folder-name>'+_folderName +'</span></div>';

                _syncStatus = '<span class="spark_sync_container btn btn-success '+_spin+'" data-toggle="tooltip" data-original-title="Resync '+e.title+'"><i class="cf-refresh4"></i></span>';

                var _html = '<li class="list-group-item" data-room-name="'+e.title+'" data-id="'+ e.id+'" data-cid="'+e.roomSyncInfo.cloudId+'" data-fid="'+e.roomSyncInfo.folderId+'">' +
                    '<div class="row">' +
                    '<div class="col-sm-3">' +
                    '<div class="spark_room"><span style="width: 55px;height: 100%;float: left;" data-toggle="tooltip" data-original-title="'+_members+'"><svg xmlns="http://www.w3.org/2000/svg" width="50" height="50">'+
                    '<defs><clipPath id="top">'+
                    '<path d="M25,25 L0,25 A25,25 0 0,1 25,0 Z"></path></clipPath>'+
                    '<clipPath id="bottom"><path d="M25,25 L25,50 A25,25 0 0,1 0,25 Z"></path>'+
                    '</clipPath><clipPath id="count">'+
                    '<path d="M25,0 L25,50 A25,25 0 0,1 25,50 M25,50 A25,25 0 1,0 25,0 Z"></path></clipPath> </defs>'+
                    '<path d="M25,0 L25,50 A25,25 0 0,1 25,50 M25,50 A25,25 0 1,0 25,0 Z" fill="#BBBBBB"></path>'+
                    '<image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://00792fd90955bc2b54b7-dde9bcd8a491bb35da928cc2123a400b.ssl.cf1.rackcdn.com/default_avatar~80" x="0" y="0" width="25" height="25" clip-path="url(#top)"></image>'+
                    '<image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://00792fd90955bc2b54b7-dde9bcd8a491bb35da928cc2123a400b.ssl.cf1.rackcdn.com/default_avatar~80" x="0" y="25" width="25" height="25" clip-path="url(#bottom)"></image>'+
                    '<text x="37" y="25" style="text-anchor: middle; dominant-baseline: middle;"  fill="white" font-family="arial" font-size="12px">'+ e.members.length+'</text>'+
                    '</svg></span><span style="width: 56%;float: left;line-height: 50px;">'+CFManageCloudAccountsAjaxCall.getMaxChars(e.title,12)+'</span></div>'+
                    '</div>' +
                    '<div class="col-sm-3 spark_link">' +
                    '<div class="progress">' +
                    '<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;background-image: linear-gradient(to bottom, #ccc 0%, #ccc 100%);">' +
                    '<span class="sr-only"></span></div></div>' +
                    '<i class="cf-right-dark"></i>'+_syncStatus+
                    '</div><div class="col-sm-6">' +_myClouds+
                    '<div style="float:left;margin: 7px;text-align: right;font-size:18px"  data-toggle="tooltip" data-original-title="Unsync Room to Cloud" data-placement="top" data-action="unlink-room"><i class="cf-times"></i></div><ul class="hidden animated" data-container="files-mini"><li style="min-height:18px;display:block"><i class="cf-times pull-right" data-action="hide-file-menu"></i></li>' +
                    '<li><ul class="clouds_drop_down"></ul></li>' +
                    '</ul></div></div></li>';

                _pp.append(_html);
            }
            if(e.roomSyncInfo.loadLock){
                CFManageCloudAccountsAjaxCall.getRoomSyncInfo(e.id);
            }
        }
    });

    if(_sync_count == 0 || value == true){
        var _Nohtml = '<div class="col-sm-7 row">' +
            '<h5>No rooms have been synced. Please click on the button below to start the configuration.</h5><br>' +
            '<h5>You may sync as many rooms as you wish. You can create one to one mapping between rooms and clouds. It is also possible to sync many rooms to one cloud.</h5></div>';
        _pp.html(_Nohtml);
    }
    $("#show_spark_pair").css({"background":"white"});
    $("#show_spark_pair").parent().css({"display":""})
    CFManageCloudAccountsAjaxCall.appendSparkPairRooms(_sparkRooms,_pair_rooms);

    CFManageCloudAccountsAjaxCall.appendSparkPairClouds(_pair_clouds);

    CFManageCloudAccountsAjaxCall.applyTooltip();

    sparkScroll();
}

function sparkScroll(){
    $('#spark_sync').find('.clouds_drop_down').on('scroll',function(){
        if(($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)) {
            var _g_parent = $('#spark_sync'),
                _bread = _g_parent.find('.bread'),
                _container = $(this),
                _page = _container.attr('data-page'),
                _li = _bread.find('li').last().find('a'),
                _cid = _li.attr('data-cid'),
                _fid = _li.attr('data-fid'),
                _check = _li.attr('data-action'),
                _files = null,
                _url = '';

            if(_page === '-1'){
                return false;
            }

            _page = parseInt(_page)+1;

            if(_check == 'getFolders'){
                _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
                    "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
                    "&orderField=modifiedTime" +
                    "&page_nbr=" +_page+
                    "&fetchCollabInfo=true" +
                    "&folderId=&isAllFiles=false";
                _files = CFManageCloudAccountsAjaxCall.getFiles(_url);
            }else if(_check == 'navigate'){
                _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
                    "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
                    "&orderField=modifiedTime" +
                    "&page_nbr=" +_page+
                    "&fetchCollabInfo=true" +
                    "&folderId="+_fid+"&isAllFiles=false";
                _files = CFManageCloudAccountsAjaxCall.getFiles(_url);
            }

            if(_files != null){
                CFManageCloudAccountsAjaxCall.appendFiles(_container,_files);
                _container.attr('data-page',_page);
                if(_files.length < 20){
                    _container.attr('data-page','-1');
                }
            }
        }
    });
}

//Step 1
$('#spark_cloud_container').on('change','#data_spark_status',function(){
    if($('#data_spark_status').prop('checked')){
        $('#data_spark_status').prop('checked',false);
        getHost(domainUrl);
        sendGAEvents(ADD_CLOUD,"Cisco Spark");
        window.open(redirectURL + 'CISCO_SPARK', '_blank', 'width=750px,height=750px');
    }else{
        var _id = $('#data_spark_status').attr('data-cid');
        var _result = CFManageCloudAccountsAjaxCall.deleteSpark(_id);
        if(_result){
            CFManageCloudAccountsAjaxCall.showNotification('notify','Spark unlink successful.');
            $('#config_spark').trigger('click');
        }else{
            CFManageCloudAccountsAjaxCall.showNotification('error','Spark unlink failed.');
        }
    }
});

$('#spark_sync').on('click','.spark_sync_container',function(){
    sendGAEvents("Clicked on spark sync");
    var _parent = $(this).closest('li'),
        _rid= _parent.attr('data-id'),
        _cid = _parent.attr('data-cid'),
        _fid = _parent.attr('data-fid'),
        _f_container = _parent.find('[data-container="files-mini"]'),
        _isVisible = _f_container.is(':visible'),
        _roomName = _parent.attr('data-room-name'),
        data = '',
        msg = 'Spark content re-sync Initiated.',
        syncContainer = $(this).closest('div');

    //$(this).find('i').addClass('fa-spin');
    $(this).addClass('fa-spin');

    if(_isVisible){
        var _fileSelected = _f_container.find('input:checked');

        if(_fileSelected.length > 0){
            var _li = _fileSelected.closest('li');
            _cid = _li.attr('data-cid');
            _fid = _li.attr('data-fid');
        }else{
            var _bread = _f_container.find('.bread'),
                _last_child = _bread.children('li:last-child');
            _cid = _last_child.find('a').attr('data-cid');
            _fid = _last_child.find('a').attr('data-fid');
            if(_fid == undefined){
                _fid = '';
            }
        }
        msg = 'Spark content sync Initiated.';
        _f_container.addClass('hidden');
    }

    data += '?folderId='+encodeURIComponent(_fid);
    data += '&cloudId='+_cid;
    data += '&roomId='+encodeURIComponent(_rid);
    CFManageCloudAccountsAjaxCall.showNotification('notify', msg);
    CFManageCloudAccountsAjaxCall.sparkSyncFolder(data);
});

//Step 2 Functions
$('#spark_cloud_container').on('click','[data-action="show-clouds-drop"]',function(){
    $('#spark_cloud_container').find('[data-action="hide-file-menu"]').trigger('click');
    var _parent = $(this).siblings('ul'),
        _container = _parent.find('.clouds_drop_down'),
        _c_id = $(this).attr('data-id'),
        _breadParent = _parent.children('li').first(),
        _bread = _parent.find('.bread'),
        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _c_id + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1" +
            "&fetchCollabInfo=true" +
            "&folderId=&isAllFiles=false";

    _container.attr('data-page','1');

    _bread.remove();

    _breadParent.append('<ul class="bread"></ul>');

    _bread = _parent.find('.bread');

    var _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

    if(_files != null){
        _container.html('');
        CFManageCloudAccountsAjaxCall.appendFiles(_container,_files)
    }
    CFManageCloudAccountsAjaxCall.constructBreadCrumb(_bread, 'clouds', _c_id);

    _parent.removeClass('hidden');

    _parent = $('#spark_sync');
    var _s_c = $(this),
        _s_s = _s_c.siblings('[data-action="sync"]'),
        _s_d = _s_c.siblings('.spark_dest_display');

    /*_s_c.hide();
     _s_d.hide();*/
    _s_s.show();

    CFManageCloudAccountsAjaxCall.makeScrollTo($('#main'),$(this).closest('.list-group-item'));

});

$('#spark_sync').on('click','.spark_dest_display',function(){
    $(this).siblings('[data-action="show-clouds-drop"]').trigger('click');
});

$('#spark_sync').on('click','[data-action="sync"]',function(){
    var _parent = $(this).closest('div'),
        _g_parent = $(this).closest('.list-group-item'),
        _check = _parent.find('input:checked'),
        data = '',
        _li = _check.closest('li');

    data += '?folderId='+encodeURIComponent(_li.attr('data-fid'));
    data += '&cloudId='+_li.attr('data-cid');
    data += '&roomId='+encodeURIComponent(_g_parent.attr('data-id'));
    CFManageCloudAccountsAjaxCall.sparkSyncFolder(data);
});

$('#spark_cloud_container').on('click','[data-action="hide-file-menu"]',function() {
    var _parent = $(this).closest('ul'),
        _s_c = _parent.siblings('[data-action="show-clouds-drop"]'),
        _s_s = _parent.siblings('[data-action="sync"]'),
        _s_d = _parent.siblings('.spark_dest_display');

    _s_c.show();
    _s_s.hide();
    _s_d.show();

    _parent.addClass('hidden');
    $(this).next().remove();
});

$('#spark_sync').on('click','li.files',function(e) {
    var _fid = $(this).attr('data-fid'),
        _cid = $(this).attr('data-cid'),
        _g_parent = $('#spark_sync'),
        _bread = _g_parent.find('.bread'),
        _check = $(this).find('input'),
        _container = _g_parent.find('.clouds_drop_down'),
        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1" +
            "&fetchCollabInfo=true" +
            "&folderId=" + _fid +
            "&isAllFiles=false&",
        _spinner = $('#spinner2');

    if ($(e.target).is(_check)) {
        var _prop = _check.prop('checked');
        if (_prop) {
            _check.prop('checked', true);
            $('[data-action="sync"]').removeClass('disabled');
        } else {
            _check.prop('checked', false);
        }
        return true;
    }
    _spinner.show();

    var _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

    if(_files != null){
        _container.html('');
        CFManageCloudAccountsAjaxCall.appendFiles(_container,_files);
        CFManageCloudAccountsAjaxCall.constructBreadCrumb(_bread,'folders',_fid);
    }
    _spinner.hide();
    $('[data-action="sync"]').addClass('disabled');
    _container.attr('data-page','1');
});

$('#spark_sync').on('click','.bread a',function(){
    var _navigation = $(this).attr('data-action'),
        _cid = '',
        _fid = '',
        _parent = $('#spark_sync'),
        _filesContainer = _parent.find('.clouds_drop_down'),
        _url = '',
        _files = null;
    if (_navigation == "navigate") {
        _cid = $(this).attr('data-cid');
        _fid = $(this).attr('data-fid');
        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1"+
            "&fetchCollabInfo=true" +
            "&folderId=" + _fid +
            "&isAllFiles=false";

        _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

        if (_files != null) {
            _filesContainer.html('');
            CFManageCloudAccountsAjaxCall.appendFiles(_filesContainer,_files);
            $(this).closest('li').nextAll().remove();
        }

    } else if (_navigation == 'getFolders') {
        _cid = $(this).attr('data-cid');

        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1" +
            "&fetchCollabInfo=true" +
            "&folderId=&isAllFiles=false";

        _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

        if (_files != null) {
            _filesContainer.html('');
            CFManageCloudAccountsAjaxCall.appendFiles(_filesContainer,_files);
            $(this).closest('li').nextAll().remove();
        }
    }else if(_navigation == "getAllClouds"){
        $(this).closest('ul.animated').siblings('[data-action="show-clouds-drop"]').trigger('click');
    }
    $('[data-action="sync"]').addClass('disabled');
});

$('#spark_sync').on('click','.cloudcontainer',function() {
    var _cid = $(this).attr('data-cloudid'),
        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1" +
            "&fetchCollabInfo=true" +
            "&folderId=&isAllFiles=false",
        _spinner = $('#spinner2'),
        _parent = $(this).closest('ul'),
        _prev = _parent.parent().prev();

    _prev.append('<ul class="bread"></ul>');

    var _bread = _prev.find('.bread');

    _spinner.show();

    var _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

    if(_files != null){
        _parent.html('');
        CFManageCloudAccountsAjaxCall.appendFiles(_parent,_files)
    }
    CFManageCloudAccountsAjaxCall.constructBreadCrumb(_bread, 'clouds', _cid);
    _parent.attr('data-page','1');
    _spinner.hide();
});

$('#spark_sync').on('click','[data-action="unlink-room"]',function () {
    var _parent = $(this).closest('li'),
        _id = _parent.attr('data-id'),
        _modal = $('#unlinkSpark');

    _modal.find('.button.blue').attr('data-rid',_id);

    _modal.modal('show');
});

$('#unlinkSpark').on('click','.button.blue',function(){
    var _id = $(this).attr('data-rid');
    var _result = CFManageCloudAccountsAjaxCall.unlinkSparkRoom(_id);
    if(_result){
        CFManageCloudAccountsAjaxCall.showNotification('notify','Room Unlink Success.');
        $('#spark_sync').find('li.list-group-item[data-id="'+_id+'"]').remove();
    }else{
        CFManageCloudAccountsAjaxCall.showNotification('error','Room Unlink Failed.');
    }
});

//Step 3

$('#spark_pair').on('click','li.cloud',function(e){
    console.log($(this).attr('data-cid'));
    var _cid = $(this).attr('data-cid'),
        _parent = $(this).closest('ul'),
        _check = $(this).find('input'),
        _g_parent = _parent.closest('div'),
        _pair= $('#spark_pair'),
        _sync_button = _pair.find('.spark_sync_container'),
        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1" +
            "&fetchCollabInfo=true" +
            "&folderId=&isAllFiles=false";

    _parent.before('<ul class="bread"></ul>');

    var _bread = _g_parent.find('.bread');

    var _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

    if(_files != null){
        _parent.html('');
        var _r_length = _pair.find('input[type="checkbox"]:checked').length;
        if(_r_length > 0){
            _sync_button.removeClass('disabled');
        }else{
            _sync_button.addClass('disabled');
        }
        _parent.find('input[type="radio"]').prop('checked',false);
        CFManageCloudAccountsAjaxCall.appendFiles(_parent,_files);
        CFManageCloudAccountsAjaxCall.constructBreadCrumb(_bread, 'clouds', _cid);
        _g_parent.attr('data-page','1');
    }
});

$('#spark_pair').on('click','li.files',function(e){
    var _fid = $(this).attr('data-fid'),
        _cid = $(this).attr('data-cid'),
        _parent = $(this).closest('ul'),
        _bread = _parent.prev(),
        _check = $(this).find('input'),
        _g_parent = _parent.closest('div'),
        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1" +
            "&fetchCollabInfo=false" +
            "&folderId=" + _fid +
            "&isAllFiles=false",
        _spinner = $('#spinner2'),
        _spark_pair = $('#spark_pair'),
        _check_length = _spark_pair.find('input[type="checkbox"]:checked').length,
        _sync_button = _spark_pair.find('.spark_sync_container');

    if ($(e.target).is(_check)) {
        var _prop = _check.prop('checked');
        if (_prop) {
            _check.prop('checked', true);
        } else {
            _check.prop('checked', false);
        }
        return true;
    }
    _spinner.show();

    var _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

    if(_files != null){
        _parent.html('');
        $('.clouds .list-group').nextAll().remove();
        $('.clouds .list-group').html('');
        if(_check_length > 0){
            _sync_button.removeClass('disabled');
        }
        else{
            _sync_button.addClass('disabled');
        }
        _parent.find('input[type="radio"]').prop('checked',false);
        _bread = $(".clouds .bread");
        CFManageCloudAccountsAjaxCall.appendFiles(_parent,_files);
        CFManageCloudAccountsAjaxCall.constructBreadCrumb(_bread,'folders',_fid);
        _spinner.hide();
        _g_parent.attr('data-page','1');
    }
});

$('#spark_pair').on('click','.bread a',function(){
    var _navigation = $(this).attr('data-action'),
        _cid = '',
        _fid = '',
        _parent = $(this).closest('ul'),
        _filesContainer = _parent.next(),
        _url = '',
        _files = null,
        _spark_pair = $('#spark_pair'),
        _Sync = _spark_pair.find('.spark_sync_container'),
        _check_length = _spark_pair.find('input[type="checkbox"]:checked').length;

    _filesContainer.find('input[type="radio"]').prop('checked',false);

    if(_check_length > 0){
        _Sync.removeClass('disabled');
    }else{
        _Sync.addClass('disabled');
    }
    $('.clouds .list-group').nextAll().remove();
    $('.clouds .list-group').html('');
    if (_navigation == "navigate") {
        _cid = $(this).attr('data-cid');
        _fid = $(this).attr('data-fid');
        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1"+
            "&fetchCollabInfo=true" +
            "&folderId=" + _fid +
            "&isAllFiles=false";

        _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

        if (_files != null) {
            _filesContainer.html('');
            CFManageCloudAccountsAjaxCall.appendFiles(_filesContainer,_files);
            $(this).closest('li').nextAll().remove();
        }

    } else if (_navigation == 'getFolders') {
        _cid = $(this).attr('data-cid');

        _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
            "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
            "&orderField=modifiedTime" +
            "&page_nbr=1" +
            "&fetchCollabInfo=true" +
            "&folderId=&isAllFiles=false";

        _files = CFManageCloudAccountsAjaxCall.getFiles(_url);

        if (_files != null) {
            _filesContainer.html('');
            CFManageCloudAccountsAjaxCall.appendFiles(_filesContainer,_files);
            $(this).closest('li').nextAll().remove();
        }
    }else if(_navigation == "getAllClouds"){
        _parent.remove();
        _Sync.addClass('disabled');
        _filesContainer.html('');
        CFManageCloudAccountsAjaxCall.appendSparkPairClouds(_filesContainer);
    }
});

$('#show_spark_pair').on('click',function(){
    sendGAEvents("Clicked on configure spark pair");
    var _parent =$('#spark_pair');
    _parent.show();
    _parent.toggleClass('hidden');
    $(this).find('i').toggleClass('cf-sort-up');
    CFManageCloudAccountsAjaxCall.makeScrollTo($('#main'),_parent);
});

$('#spark_pair').on('click','.spark_sync_container',function(){
    CFManageCloudAccountsAjaxCall.showNotification('notify','Spark Sync Initiated for selected room(s).');
    var _parent =$('#spark_pair'),
        _sync = _parent.find('.spark_sync_container'),
        _rooms = _parent.find('input[type="checkbox"]:checked'),
        _clouds = _parent.find('input[type="radio"]:checked'),
        _r_id = [],
        _c_id = '',
        _f_id = '',
        _bread = $('.bread');

    $(this).addClass('fa-spin');

    _rooms.each(function(i,e){
        _r_id.push($(e).closest('li').attr('data-id'));
    });

    if(_clouds.length > 0){
        _c_id = _clouds.closest('li').attr('data-cid');
        _f_id = _clouds.closest('li').attr('data-fid');
    }else {
        var last = _bread.find('li:last-child').find('a'),
            _action = last.attr('data-action');
        if (_action == "getFolders") {
            _c_id = last.attr('data-cid');
        }else if(_action == "navigate"){
            _c_id = last.attr('data-cid');
            _f_id = last.attr('data-fid');
        }
    }
    $.each(_r_id,function(i,e){
        var data = '?cloudId='+_c_id;
        data +='&folderId='+encodeURIComponent(_f_id);
        data += '&roomId='+encodeURIComponent(e);
        CFManageCloudAccountsAjaxCall.sparkSyncFolder(data);
    });

    setTimeout(function () {
        $('#config_spark').trigger('click');
    },7000);

});

$('#spark_pair').on('change','input[type="checkbox"]',function(){
    var _parent =$('#spark_pair'),
        _sync = _parent.find('.spark_sync_container'),
        _rooms = _parent.find('input[type="checkbox"]:checked'),
        _bread = _parent.find('.bread');

    if(_rooms.length > 0 && _bread.is(':visible')){
        _sync.removeClass('disabled');
    }else{
        _sync.addClass('disabled');
        $('[data-container="clouds"]').find('input').prop('checked',false);
    }

    if(_rooms.length == 1){
        var _r_id = $(this).closest('li').attr('data-cid');
        $('[data-container="clouds"]').find('[data-cid="'+_r_id+'"]').find('input').prop('checked',true);
    }
});


/*$('#spark_pair').on('change','input',function(e){
 var _parent =$('#spark_pair'),
 _sync = _parent.find('.spark_sync_container'),
 _clouds= _parent.find('input[type="radio"]:checked'),
 _rooms = _parent.find('input[type="checkbox"]:checked');

 if(_rooms.length == 1){
 var _cid = $(this).closest('li').attr('data-cid');
 _parent.find('li.cloud[data-cid="'+_cid+'"]').find('input').trigger('click');
 }else if(_rooms.length == 0){
 _parent.find('input[type="radio"]').prop('checked',false);
 }

 _clouds = _parent.find('input[type="radio"]:checked');

 if(_rooms.length > 0 && _clouds.length > 0){
 _sync.removeClass('disabled');
 }else{
 _sync.addClass('disabled');
 }
 });*/

$('div.clouds').on('scroll',function(){
    if(($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight)) {
        var _container = $(this),
            _bread = _container.find('.bread'),
            _page = _container.attr('data-page'),
            _li = _bread.find('li').last().find('a'),
            _cid = _li.attr('data-cid'),
            _fid = _li.attr('data-fid'),
            _check = _li.attr('data-action'),
            _files = null,
            _url = '';

        if(_page === '-1'){
            return false;
        }

        _page = parseInt(_page)+1;

        if(_check == 'getFolders'){
            _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
                "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
                "&orderField=modifiedTime" +
                "&page_nbr=" +_page+
                "&fetchCollabInfo=true" +
                "&folderId=&isAllFiles=false";
            _files = CFManageCloudAccountsAjaxCall.getFiles(_url);
        }
        else if(_check == 'navigate'){
            _url = apicallurl + "/filefolder/user/" + CFManageCloudAccountsAjaxCall.getUserId() +
                "/childfolders/" + _cid + "?page_size=20&isAscen=false" +
                "&orderField=modifiedTime" +
                "&page_nbr=" +_page+
                "&fetchCollabInfo=true" +
                "&folderId="+_fid+"&isAllFiles=false";
            _files = CFManageCloudAccountsAjaxCall.getFiles(_url);
        }

        if(_files != null){
            CFManageCloudAccountsAjaxCall.appendFiles(_container,_files);
            _container.attr('data-page',_page);
            if(_files.length < 20){
                _container.attr('data-page','-1');
            }
        }
    }
});