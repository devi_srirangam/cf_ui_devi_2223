$(document).ready(function () {
    var urlParams;
    (window.onpopstate = function () {
        var match,pl     = /\+/g,
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);
        urlParams = {};
        while (match = search.exec(query))
        urlParams[decode(match[1])] = decode(match[2]);
    })();
    pmntpage = urlParams["pinfo"];
	if (pmntpage){
		var url=domainUrl+'/pages/settings.html?pinfo=true';
		sessionStorage.setItem("pmnt",url);
	}
    if (CFManageCloudAccountsAjaxCall != undefined && CFManageCloudAccountsAjaxCall.getUserId()) {
        $("body").removeClass("divdisplay");
    }
    else {
        window.location.href = "index.html";
    }
    $("#LView").css("opacity","0.2");
	$(".secondary-nav-menu > li > ul > li").mouseenter(function(){
		$("#categoryList").find(".editCategory").css("color","#fff");
	});

    $(".fileCreatecontrols").show();
    $(".fileview").show();

    /*(function(){
        CFManageCloudAccountsAjaxCall.getAllClouds(CloudName, CloudId);
        var count=0;
        $.each(AllCloudsInfo,function(i,c){
            if(c.cloudStatus != "ACTIVE"){
                count++;
            }
        });
        if(count >0){
            var cloudPage =$('#primary').find('.icon-cloud');
            cloudPage.nextAll().remove();
            cloudPage.after('<i class="cf-warning" style="color: rgb(236, 40, 40);font-size: 15pt;position: absolute;top: -12px;right: -4px;width: 32px;height: 32px;"></i>');
        }
    })();*/

    CFManageCloudAccountsAjaxCall.getAllCloudTitles(CloudName, CloudId);
    CFManageCloudAccountsAjaxCall.getAllClouds();

	var cloudlength=$("#CloudDriveList").children("div").length;

    if(cloudlength > 0){
		$("#spinner1").removeClass("divdisplay");
		setTimeout(function(){CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(homeCid , PageNumber);},1000);
		$("#CFHSortModifiedOn").children("span").html("&#9650");
	}

    if(CFHPlistview != undefined){
        CFHPlistview.init();
    }
	CFHPthumbnail.init();
    $("#LVContentHeader").removeClass("divdisplay");

	$("#settingspage").click(function(){
		var UserObj = JSON.parse(localStorage.getItem('CFUser'));
		$(".ProfileContent").html(UserObj.lastName +" </span>");
		$(".ProfileContentemail").html(UserObj.primaryEmail +" </span>");
        var _mobile = UserObj.mobileNumber == null ? 'Not Available' : UserObj.mobileNumber;
        if(_mobile == "")
            _mobile ="Not Available";
        $(".ProfileContentmobile").html( _mobile+" </span>");
		timeOffset();
		function get_time_zone_offset( ) {
			var current_date = new Date();
			return -current_date.getTimezoneOffset() / 60;
		}
		function timeOffset(){
			var timeZoneData = get_time_zone_offset();
			var timezones = {
				'-12': 'Pacific/Kwajalein',
				'-11': 'Pacific/Samoa',
				'-10': 'Pacific/Honolulu',
				'-9': 'America/Juneau',
				'-8': 'America/Los_Angeles',
				'-7': 'America/Denver',
				'-6': 'America/Mexico_City',
				'-5': 'America/New_York',
				'-4': 'America/Caracas',
				'-3.5': 'America/St_Johns',
				'-3': 'America/Argentina/Buenos_Aires',
				'-2': 'Atlantic/Azores',
				'-1': 'Atlantic/Azores',
				'0': 'Europe/London',
				'1': 'Europe/Paris',
				'2': 'Europe/Helsinki',
				'3': 'Europe/Moscow',
				'3.5': 'Asia/Tehran',
				'4': 'Asia/Baku',
				'4.5': 'Asia/Kabul',
				'5': 'Asia/Karachi',
				'5.5': 'Asia/Calcutta',
				'6': 'Asia/Colombo',
				'7': 'Asia/Bangkok',
				'8': 'Asia/Singapore',
				'9': 'Asia/Tokyo',
				'9.5': 'Australia/Darwin',
				'10': 'Pacific/Guam',
				'11': 'Asia/Magadan',
				'12': 'Asia/Kamchatka'
			};
			$.each(timezones, function (key, value) {
				if (key == timeZoneData) {
					timeZoneData = value;
                    var tspan = $("#timeZone").find("span");
                    tspan.html("");
                    tspan.append("<select class='dropdown'><option value='' class='label'>GMT+"+key+" "+timeZoneData+"</option></select></span>");
				}
			});
		}
		/*Progress Bar*/			
		var tspaceby = 0;
		var uspaceby = 0;
		var aspaceby = 0;
        if(AllCloudsInfo != undefined) {
            for (var i = 0; i < AllCloudsInfo.length; i++) {
                tspaceby += AllCloudsInfo[i].totalSpace;
                uspaceby += AllCloudsInfo[i].usedSpace;
                aspaceby += AllCloudsInfo[i].availableSpace;
                var totalsp = CFManageCloudAccountsAjaxCall.getObjectSize(tspaceby);
                var usedsp = CFManageCloudAccountsAjaxCall.getObjectSize(uspaceby);
            }
        }
		var progrssPercent = (uspaceby/tspaceby)*100+'%';
		$("#usedProgress").css("width",progrssPercent);
        var pgrbarSpace = $("#Progressbar-Space");
        pgrbarSpace.html("");
		if(tspaceby == 0){
            pgrbarSpace.append("<span>used </span><b id='pused'>0</b><span> out of </span><b d='ptotal'>0</b>");
		}else{
            pgrbarSpace.append("<span>used </span><b id='pused'>"+usedsp+"</b><span> out of </span><b d='ptotal'>"+totalsp+"</b>");
        }
		
	});

	$("#progressbar").progressbar({value: 0});

	$("#workspace").on("click", function(){
		$("#workspacecontent").show();
		$("#wsaccesscontent").hide();
	});

    $("#wsaccess").on("click", function(){
		$("#workspacecontent").hide();
		$("#wsaccesscontent").show();
	});
	HomePageOnload = "";
    /*Window size*/
    setTimeout(function(){
        if(viewTrack == "LView"){
            if($(window).width() < 1025){
                $("#FooterLinks").css("width",$(window).width());
                var height =$(window).height() - $("#FooterLinks").height() - 2 - $("#expireMsg").height() -  $(".contentWrapper:eq(0)").height() -  $(".contentWrapper:eq(1)").height() - 15 - $("#actionPanel").height() - $("#LVContentHeader").height() - $("#primary").height() - $("#secondary").height();
                $("#mainContentWrapper").css("max-height",height).css("height",height);
                if(PageName == "WorkSpace"){
                    $("#FooterLinks").css("width","100%");
                }
                /*if($(window).width() == 768){
                 $('#mainContentWrapper').css('max-height', '710px').css('height', '710px');
                 }*/
            }else if($(window).width() > 1024){
                $("#FooterLinks").css("width",$(window).width() - $("#primary").width() - $("#secondary").width());
                var height =$(window).height() - $("#FooterLinks").height() - 2 - $("#expireMsg").height() -  $(".contentWrapper:eq(0)").height() -  $(".contentWrapper:eq(1)").height() - 15 - $("#actionPanel").height() - $("#LVContentHeader").height();
                $("#mainContentWrapper").css("max-height",height).css("height",height);
                if(PageName == "WorkSpace"){
                    $("#FooterLinks").css("width","95%");
                }
            }
        }else if(viewTrack == "GView") {
            if ($(window).width() < 1025) {
                $("#FooterLinks").css("width", $(window).width());
                var height = $(window).height() - $("#FooterLinks").height() - 2 - $("#expireMsg").height() - $(".contentWrapper:eq(0)").height() - $(".contentWrapper:eq(1)").height() - 15 - $("#actionPanel").height() - $("#primary").height() - $("#secondary").height();
                $("#mainContentWrapper").css("max-height", height).css("height", height);
                if (PageName == "WorkSpace") {
                    $("#FooterLinks").css("width", "100%");
                }
                /*if($(window).width() == 768){
                 $('#mainContentWrapper').css('max-height', '700px').css('height', '700px');
                 }*/
            }else if ($(window).width() > 1024) {
                $("#FooterLinks").css("width", $(window).width() - $("#primary").width() - $("#secondary").width());
                var height = $(window).height() - $("#FooterLinks").height() - 2 - $("#expireMsg").height() - $(".contentWrapper:eq(0)").height() - $(".contentWrapper:eq(1)").height() - 15 - $("#actionPanel").height();
                $("#mainContentWrapper").css("max-height", height).css("height", height);
                if (PageName == "WorkSpace") {
                    $("#FooterLinks").css("width", "95%");
                }
            }
        }
        var a=$('#expireMsg').text().toLowerCase().indexOf("subscription");
        var _path=window.location.pathname;
        if((_path.indexOf("fileManager.html")==-1) && (_path.indexOf("workspace.html")==-1))
        { if(a > 0)
            $('#main').css('zoom','92%');
        }

    },2000);

    $(window).resize(function(){
        if(viewTrack == "LView"){
            if($(window).width() < 1025){
                $("#FooterLinks").css("width",$(window).width());
                var height =$(window).height() - $("#FooterLinks").height() - 2 - $("#expireMsg").height() -  $(".contentWrapper:eq(0)").height() -  $(".contentWrapper:eq(1)").height() - 15 - $("#actionPanel").height() - $("#LVContentHeader").height() - $("#primary").height() - $("#secondary").height();
                $("#mainContentWrapper").css("max-height",height).css("height",height);
                if(PageName == "WorkSpace"){
                    $("#FooterLinks").css("width","100%");
                }
            }else if($(window).width() > 1024){
                $("#FooterLinks").css("width",$(window).width() - $("#primary").width() - $("#secondary").width());
                var height =$(window).height() - $("#FooterLinks").height() - 2 - $("#expireMsg").height() -  $(".contentWrapper:eq(0)").height() -  $(".contentWrapper:eq(1)").height() - 15 - $("#actionPanel").height() - $("#LVContentHeader").height();
                $("#mainContentWrapper").css("max-height",height).css("height",height);
                if(PageName == "WorkSpace"){
                    $("#FooterLinks").css("width","95%");
                }
            }
        }else if(viewTrack == "GView") {
            if ($(window).width() < 1025) {
                $('#FooterLinks').css('width', $(window).width());
                var height = $(window).height() - $('#FooterLinks').height() - 2 - $('#expireMsg').height() - $('.contentWrapper:eq(0)').height() - $('.contentWrapper:eq(1)').height() - 15 - $('#actionPanel').height() - $('#primary').height() - $('#secondary').height();
                $("#mainContentWrapper").css("max-height", height).css("height", height);
                if (PageName == "WorkSpace") {
                    $("#FooterLinks").css("width", "100%");
                }
            }else if ($(window).width() > 1024) {
                $("#FooterLinks").css("width", $(window).width() - $("#primary").width() - $("#secondary").width());
                var height = $(window).height() - $("#FooterLinks").height() - 2 - $("#expireMsg").height() - $(".contentWrapper:eq(0)").height() - $(".contentWrapper:eq(1)").height() - 15 - $("#actionPanel").height();
                $("#mainContentWrapper").css("max-height", height).css("height", height);
                if (PageName == "WorkSpace") {
                    $("#FooterLinks").css("width", "95%");
                }
            }
        }
    });
    if(PageName == "WorkSpace"){
        (function(){
            $("#FooterLinks").css("width","95%");
            var docHeight = $("#eyebrowHeader").children(".container-fluid").height() +
                $("#actionPanel").height() +
                $("#FooterLinks").height();
            var wheight = $(window).height() - docHeight - 10;
            setTimeout(function(){
                $("#mainContentWrapper").css("max-height",wheight);
                $("#mainContentWrapper").css("height",wheight);
            },500);
        })();
    }
   /* if($(window).width() == 768){
        $('#mainContentWrapper').css('max-height', '710px').css('height', '710px');
    }*/
    /*Window size*/
            var userBotLogin = localStorage.getItem("userBotLogin");
            var _html='';
            if(userBotLogin=='spark'){
                    _html = '<a href="https://web.ciscospark.com/rooms/new" target="_blank">'+
                    '<i style="padding-top:2px;"><img src="../img/drive/CISCO_SPARK.png" style="width: 18px;margin-right: 5px;"></i>'+ 'Open in Spark</a>';
            }
            else if(userBotLogin=='slack'){
                var slackTeam = "https://"+localStorage.getItem("slackTeam")+".slack.com/messages/";
                _html = '<a href=' + slackTeam + ' target="_blank">'+
                    '<i style="padding-top:2px;"><img src="../img/drive/SLACK.png" style="width: 18px;margin-right: 5px;"></i>'+ 'Open in Slack</a>';
            } 
        $("#sparkButton").html(_html);
});

function showNotyNotification(type,msg) {
    if (type == 'notify')
        type='success';
    else if (type == 'warn')
        type='warning';
    noty({
        text: msg,
        layout: 'topRight',
        closeWith: ['button','click'],
        timeout: 13000,
        type: type,//'information-b', 'error-r', 'warning-y', 'success-g';
        maxVisible: 3,
    });
    //$('#noty_topRight_layout_container').css('right','20px');
    $('#noty_topRight_layout_container li').css({'height':'40px'});
    $('#noty_topRight_layout_container .noty_close').css({"background":"url('../img/PNG/cancel.png')","display":"block"});
}

    $(".sparkButton").click(function(){
    sendGAEvents("BOT_REDIRECTION",localStorage.getItem("userBotLogin"));
});