/**
 * Payment calls for paypal api
 *
 */
var MonthlyAMT = 4.99;
var YearlyAMT = 49.99;
var pmntscope ;
var pmntaccess_token;
var pmnttokentype ;
var pmntapp_id;
var pmnttoken;
var opmnttoken;
var pmnttimestamp;
var opmnttimestamp;
var pmntcreateParams;
var createdata;
var createpmntUrl;
var pmntprofid;
function paypal_make_basic (user, pass) {
    var tok = user + ':' + pass;
    var hash = Base64.encode(tok);
    return "Basic " + hash;
}
var grantpmnttoken = 'grant_type=client_credentials';
var CFPaymentsPaypal = {

    getpmntToken: function () {
        var pmntUrl = paymenturl + "/oauth2/token";
        $.ajax({
            type: "POST",
            url: pmntUrl,
            data: grantpmnttoken,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/x-www-form-urlencoded",
                "Authorization": paypal_make_basic(Paypal_clientid, Paypal_clientsecret),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (pmntTokenInfo) {
                pmntscope = pmntTokenInfo.scope;
                pmntaccess_token = pmntTokenInfo.access_token;
                pmnttokentype = pmntTokenInfo.token_type;
                pmntapp_id = pmntTokenInfo.app_id;
            }
        });
    },

    createPmntMnthlyOneTime: function (a) {
        createdata = "method=SetExpressCheckout&" +
            "PAYMENTREQUEST_0_AMT=" + a.amount + "&" +
            "PAYMENTREQUEST_0_CURRENCYCODE="+ a.code+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "returnUrl=" + pmntsreturnurl + "%3F"+ a.success+"%3Dtrue%26M%3Dtrue%26O%3Dtrue&" +
            "cancelUrl=" + pmntsreturnurl + "%3F"+ a.cancel+"%3Dtrue&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "L_PAYMENTREQUEST_0_NAME0=CloudFuze+Monthly+Subscription&" +
            "L_PAYMENTREQUEST_0_AMT0=" + a.amount + "&" +
            "L_PAYMENTREQUEST_0_QTY0=1&SOLUTIONTYPE=Sole";
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (createpmnttoken) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var pmntresponse = xhr.responseText;
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = pmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query)){
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    }
                    opmnttoken = pmntcreateParams["TOKEN"];
                    opmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    localStorage.setItem('paypaltstamp', pmnttimestamp);
                    var paypalcreateurl = paymentcreateurl + opmnttoken;
                    window.location.href = paypalcreateurl;
                }
                if (xhr.status > 300) {
                }
            }
        });
    },

    createPmntMnthlyRecurring: function (a) {
        createdata = "method=SetExpressCheckout&" +
            "PAYMENTREQUEST_0_AMT=" + a.amount + "&" +
            "PAYMENTREQUEST_0_CURRENCYCODE="+ a.code+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "L_BILLINGTYPE0=RecurringPayments&" +
            "L_BILLINGAGREEMENTDESCRIPTION0=CloudFuze+Monthly+Subscripton&" +
            "returnUrl=" + pmntsreturnurl + "%3F"+ a.success+"%3Dtrue%26M%3Dtrue&" +
            "cancelUrl=" + pmntsreturnurl + "%3F"+ a.cancel+"%3Dtrue&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "L_PAYMENTREQUEST_0_NAME0=CloudFuze+Monthly+Subscription&" +
            "L_PAYMENTREQUEST_0_AMT0=" + a.amount + "&" +
            "L_PAYMENTREQUEST_0_QTY0=1&SOLUTIONTYPE=Sole&LANDINGPAGE=Billing";
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (createpmnttoken) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var pmntresponse = xhr.responseText;
                    //console.log(pmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = pmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query)){
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    }
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    localStorage.setItem('paypaltstamp', pmnttimestamp);
                    var paypalcreateurl = paymentcreateurl + pmnttoken;
                    window.location.href = paypalcreateurl;
                }
            }
        });
    },

    createPmntAnnualOneTime: function (a) {
        createdata = "method=SetExpressCheckout&" +
            "PAYMENTREQUEST_0_AMT=" + a.amount + "&" +
            "PAYMENTREQUEST_0_CURRENCYCODE="+ a.code+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "returnUrl=" + pmntsreturnurl + "%3F"+ a.success+"%3Dtrue%26Y%3Dtrue%26O%3Dtrue&" +
            "cancelUrl=" + pmntsreturnurl + "%3F"+ a.cancel+"%3Dtrue&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "L_PAYMENTREQUEST_0_NAME0=CloudFuze+Annual+Subscription&" +
            "L_PAYMENTREQUEST_0_AMT0=" + a.amount + "&" +
            "L_PAYMENTREQUEST_0_QTY0=1";
        //createpmntUrl = "https://devwebapp.cloudfuze.com/pmntrecurring/nvp" ;
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (createpmnttoken) {
            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var pmntresponse = xhr.responseText;
                    //console.log(pmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = pmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query)){
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    }
                    opmnttoken = pmntcreateParams["TOKEN"];
                    opmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    localStorage.setItem('paypaltstamp', opmnttimestamp);
                    var paypalcreateurl = paymentcreateurl + opmnttoken;
                    window.location.href = paypalcreateurl;
                }
            }
        });
    },

    createPmntAnnualRecurring: function (a) {
        createdata = "method=SetExpressCheckout&" +
            "PAYMENTREQUEST_0_AMT=" + a.amount+ "&" +
            "PAYMENTREQUEST_0_CURRENCYCODE="+ a.code+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "L_BILLINGTYPE0=RecurringPayments&" +
            "L_BILLINGAGREEMENTDESCRIPTION0=CloudFuze+Annual+Subscripton&" +
            "returnUrl=" + pmntsreturnurl + "%3F"+ a.success+"%3Dtrue%26Y%3Dtrue&" +
            "cancelUrl=" + pmntsreturnurl + "%3F"+ a.cancel+"%3Dtrue&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "L_PAYMENTREQUEST_0_NAME0=CloudFuze+Annual+Subscription&" +
            "L_PAYMENTREQUEST_0_AMT0=" + a.amount + "&" +
            "L_PAYMENTREQUEST_0_QTY0=1";
        //createpmntUrl = "https://devwebapp.cloudfuze.com/pmntrecurring/nvp" ;
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (createpmnttoken) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var pmntresponse = xhr.responseText;
                    //console.log(pmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = pmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query))
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    localStorage.setItem('paypaltstamp', pmnttimestamp);
                    var paypalcreateurl = paymentcreateurl + pmnttoken;
                    window.location.href = paypalcreateurl;
                }
                if (xhr.status > 300) {
                    //console.log("Operation Failed");
                }
            },
            error: function (error) {
            }
        });
    },

    cmpleteMnthlyPmntOneTime: function (a, tokid, pyrid) {
        createdata = "method=DoExpressCheckoutPayment&" +
            "PAYMENTREQUEST_0_AMT=" + a.amount + "&" +
            "PAYMENTREQUEST_0_CURRENCYCODE="+ a.code+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "TOKEN=" + tokid + "&" +
            "PAYERID=" + pyrid;
        //createpmntUrl = "https://devwebapp.cloudfuze.com/pmntrecurring/nvp" ;
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (cmpltepmntotime) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    cmpletepmntresponse = xhr.responseText;
                    //console.log(cmpletepmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = cmpletepmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query)) {
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    }
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    pmntack = pmntcreateParams["ACK"];
                    pmnttid = pmntcreateParams["PAYMENTINFO_0_TRANSACTIONID"];
                    pmntstatus = pmntcreateParams["PAYMENTINFO_0_PAYMENTSTATUS"];
                    if (pmntack == "Success") {
                        pmntack = "PAID";
                        var intpmntid = CFPaymentsInternal.pmntmnthlyonetime(pmnttid, pmntack, pmnttimestamp, pyrid, cmpletepmntresponse);
                        var subname = "1 Month";
                        var status1 = "ACTIVE";
                        var stype = "PAY_ONETIME";
                        var expdate = getuserexpiry.searchuserexpiry(localStorage.getItem('UserId'));
                        var expdate_expiresIn = expdate[0].expiresIn;
                        var expdate_createdDate = expdate[0].createdDate;
                        var etstamp;
                        if (expdate_expiresIn == null) {
                            etstamp = expdate_createdDate;
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }else {
                            etstamp = expdate_expiresIn;
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }
                        $('#transstatustext').text('Thank you for Choosing CloudFuze. Your payment was successful.');
                    }
                    else if (pmntack == "Failure") {
                        pmntack = "PENDING";
                        $('#transstatustext').text('Your payment was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
                    }
                }
            }
        });
        return pmntack;
    },

    cmpleteAnnualPmntOneTime: function (Oamt, tokid, pyrid) {
        createdata = "method=DoExpressCheckoutPayment&" +
            "PAYMENTREQUEST_0_AMT=" + Oamt + "&" +
            "PAYMENTREQUEST_0_CURRENCYCODE=USD&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "TOKEN=" + tokid + "&" +
            "PAYERID=" + pyrid;
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (cmpltepmntotime) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    cmpletepmntresponse = xhr.responseText;
                    //console.log(cmpletepmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = cmpletepmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query))
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    pmntack = pmntcreateParams["ACK"];
                    pmnttid = pmntcreateParams["PAYMENTINFO_0_TRANSACTIONID"];
                    pmntstatus = pmntcreateParams["PAYMENTINFO_0_PAYMENTSTATUS"];
                    if (pmntack == "Success") {
                        pmntack = "PAID";
                        var intpmntid = CFPaymentsInternal.pmntannualonetime(pmnttid, pmntack, pmnttimestamp, pyrid, cmpletepmntresponse);
                        var subname = "1 Year ";
                        var status1 = "ACTIVE";
                        var stype = "PAY_ONETIME";
                        var expdate = getuserexpiry.searchuserexpiry(localStorage.getItem('UserId'));
                        var expdate_expiresIn = expdate[0].expiresIn;
                        var expdate_createdDate = expdate[0].createdDate;
                        var etstamp;
                        if (expdate_expiresIn == null) {
                            etstamp = expdate_createdDate;
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        } else {
                            etstamp = expdate_expiresIn;
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }
                        $('#transstatustext').text('Thank you for Choosing CloudFuze. Your payment was successful.');
                    }
                    else if (pmntack == "Failure") {
                        pmntack = "PENDING";
                        $('#transstatustext').text('Your payment was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
                    }
                }
            }
        });
        return pmntack;
    },

    cmpleteMnthlyPmntRecurring: function (_c) {
        createdata = "method=CreateRecurringPaymentsProfile&" +
            "AMT=" + _c.amount + "&" +
            "CURRENCYCODE="+_c.country+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "L_BILLINGTYPE0=RecurringPayments&" +
            "DESC=CloudFuze+Monthly+Subscripton&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "TOKEN=" + _c.token + "&" +
            "PAYERID=" + _c.id + "&" +
            "PROFILESTARTDATE=" + _c.time + "&" +
            "BILLINGPERIOD=Month&" +
            "BILLINGFREQUENCY=1&" +
            "MAXFAILEDPAYMENTS=1&" +
            "TRIALAMT=" + _c.amount + "&" +
            "TRIALBILLINGFREQUENCY=1";
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (cmpltepmntotime) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var cmpletepmntresponse = xhr.responseText;
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = cmpletepmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query))
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    pmntprofid = pmntcreateParams["PROFILEID"];
                    pmntack = pmntcreateParams["ACK"];
                    $('#transstatus').find('#contclick').attr('instance',_c.instance);
                    if (pmntack == "Success") {
                        $('#transstatus').find('#contclick').attr('status','success');
                        pmntack = "PAID";
                        var intpmntid = CFPaymentsInternal.pmntmonthlyrecurring(pmntack, pmnttimestamp, _c.id, pmntprofid, cmpletepmntresponse);
                        var subname = "Monthly Recurring";
                        var status1 = "ACTIVE";
                        var stype = "PAY_MONTHLY";
                        var expdate = getuserexpiry.searchuserexpiry(localStorage.getItem('UserId'));
                        var expdate_expiresIn = expdate[0].expiresIn;
                        var expdate_createdDate = expdate[0].createdDate;
                        var etstamp;
                        if (expdate_expiresIn == null) {
                            etstamp = expdate_createdDate;
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }
                        else {
                            etstamp = expdate_expiresIn;
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }

                        if(_c.instance == "orange"){
                            $('#transstatustext').text('Thank you for Choosing CloudFuze. Your payment was successful.');
                        }
                        else{
                            $('#transstatustext').text('Thank you for Choosing CloudFuze. Your payment was successful.');
                        }
                    }
                    else if (pmntack == "Failure") {
                        $('#transstatus').find('#contclick').attr('status','failed');
                        pmntack = "PENDING";
                        $('#transstatustext').html('Your payment was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
                        $('#transbutton').text('Please try again');
                    }
                }
            }
        });
        return pmntack;

    },

    cmpleteAnnualPmntRecurring: function (_a) {
        createdata = "method=CreateRecurringPaymentsProfile&" +
            "AMT=" + _a.amount + "&" +
            "CURRENCYCODE="+_a.country+"&" +
            "PAYMENTREQUEST_0_PAYMENTACTION=sale&" +
            "L_BILLINGTYPE0=RecurringPayments&" +
            "DESC=CloudFuze+Annual+Subscripton&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "TOKEN=" + _a.token + "&" +
            "PAYERID=" + _a.id + "&" +
            "PROFILESTARTDATE=" + _a.time + "&" +
            "BILLINGPERIOD=Year&" +
            "BILLINGFREQUENCY=1&" +
            "MAXFAILEDPAYMENTS=1&" +
            "TRIALAMT=" + _a.amount + "&" +
            "TRIALBILLINGFREQUENCY=1";
        $('#transstatus').find('#contclick').attr('instance',_a.instance);
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (cmpltepmntotime) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    $('#transstatus').find('#contclick').attr('status','success');
                    var cmpletepmntresponse = xhr.responseText;
                    //console.log(cmpletepmntresponse);
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        },
                        query = cmpletepmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query))
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    pmntprofid = pmntcreateParams["PROFILEID"];
                    pmntack = pmntcreateParams["ACK"];
                    if (pmntack == "Success") {
                        pmntack = "PAID";
                        if ($('#subsname').text() == "Monthly Recurring") {
                            if (lastpmntresponse.recurringId && lastpmntresponse.referenceId == "") {
                                var status4 = CFPaymentsPaypal.cnclRecurringSubscription(lastpmntresponse.recurringId);
                                //console.log("createannualpmnt");
                                //console.log(status4);id
                                if (status4 == "Success") {
                                    CFSubscriptionsInternal.CancelSubscription(lastsubsresponse.id);
                                }
                            }
                        }
                        var intpmntid = CFPaymentsInternal.pmntannualrecurring(pmntack, pmnttimestamp, _a.id, pmntprofid, cmpletepmntresponse);
                        var subname = "Yearly Recurring";
                        var status1 = "ACTIVE";
                        var stype = "PAY_YEARLY";
                        var expdate = getuserexpiry.searchuserexpiry(localStorage.getItem('UserId'));
                        var expdate_expiresIn = expdate[0].expiresIn;
                        var expdate_createdDate = expdate[0].createdDate;
                        var etstamp;
                        if (expdate_expiresIn == null) {
                            etstamp = expdate_createdDate;
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        } else {
                            etstamp = expdate_expiresIn;
                            CFSubscriptionsInternal.CreateSubscription(subname, status1, stype, pmnttimestamp, etstamp, intpmntid);
                        }
                        $('#transstatustext').text('Thank you for Choosing CloudFuze. Your payment was successful.');
                    }
                    else if (pmntack == "Failure") {
                        pmntack = "PENDING";
                        $('#transstatus').find('#contclick').attr('status', 'failed');
                        $('#transstatustext').html('Your payment was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
                        $('#transbutton').text('Please try again');
                    }
                }
            }
        });
        return pmntack;
    },

    cnclRecurringSubscription: function (profid) {
        createdata = "method=ManageRecurringPaymentsProfileStatus&" +
            "version=104.0&" +
            "user=" + API_username + "&" +
            "pwd=" + API_password + "&" +
            "signature=" + Signature + "&" +
            "PROFILEID=" + profid + "&" +
            "ACTION=Cancel";
        //createpmntUrl = "https://devwebapp.cloudfuze.com/pmntrecurring/nvp" ;
        $.ajax({
            type: "POST",
            url: createpmntUrl,
            data: createdata,
            async: false,
            dataType: "application/x-www-form-urlencoded",
            success: function (cmpltepmntotime) {

            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    var cnclpmntresponse = xhr.responseText;
                    var match, pl = /\+/g,
                        search = /([^&=]+)=?([^&]*)/g,
                        decode = function (s) {
                            return decodeURIComponent(s.replace(pl, " "));
                        };
                    query = cnclpmntresponse;
                    pmntcreateParams = {};
                    while (match = search.exec(query))
                        pmntcreateParams[decode(match[1])] = decode(match[2]);
                    pmnttoken = pmntcreateParams["TOKEN"];
                    pmnttimestamp = pmntcreateParams["TIMESTAMP"];
                    pmntack1 = pmntcreateParams["ACK"];
                }
            }
        });
        return pmntack1;
    },

    createPayment:function(urlParams){
        pmntpage = urlParams["pinfo"];
        paypal1 = urlParams["paypalsuccess"];
        Monthly = urlParams["M"];
        Yearly = urlParams["Y"];
        Onetime = urlParams["O"];
        var orangeSuccess = urlParams["orangesuccess"];
        var orangeCancel = urlParams["orangecancel"];
        paypaltoken = urlParams["token"];
        paypalpayerid = urlParams["PayerID"];
        if (pmntpage) {
            $('#paymentinfo').trigger('click');
        }
        else if (paypal1 === "true") {

            var PayPalObject = {
                "amount":null,
                "time":null,
                "country":'',
                "token":paypaltoken,
                "id":paypalpayerid,
                "instace":"cloudfuze"
            };
            PayPalObject.country = JSON.parse(localStorage.getItem('CFUser')).currency == null ? 'USD' : JSON.parse(localStorage.getItem('CFUser')).currency;
            $('#paymentinfo').trigger('click');
            $('#paypalsuccessmodal').modal('show');
            if (Monthly === "true") {
                PayPalObject.amount = localStorage.getItem('MonthlyAMNT');
                PayPalObject.time = localStorage.getItem('paypaltstamp');
                CFPaymentsPaypal.cmpleteMnthlyPmntRecurring(PayPalObject);
                localStorage.removeItem('MonthlyAMNT');
                $('#monthlysubscribe').addClass('divdisplay');
                $('#cnclmonthlysubscribe').removeClass('divdisplay');
                $('#paypalsuccessmodal').modal('hide');
                $('#transstatus').modal('show');
            }
            else if (Yearly === "true") {
                PayPalObject.amount = localStorage.getItem('YearlyAMNT');
                PayPalObject.time = localStorage.getItem('paypaltstamp');
                CFPaymentsPaypal.cmpleteAnnualPmntRecurring(PayPalObject);
                localStorage.setItem('paypalpayerid', paypalpayerid);
                localStorage.removeItem('YearlyAMNT');
                $('#annualsubscribe').addClass('divdisplay');
                $('#cnclannualsubscribe').removeClass('divdisplay');
                $('#paypalsuccessmodal').modal('hide');
                $('#transstatus').modal('show');
            }
        }
        else if(orangeSuccess === "true"){
            var object = {
                "amount":null,
                "time":null,
                "country":"EUR",
                "token":paypaltoken,
                "id":paypalpayerid,
                "instance":"orange"
            };
            if(Monthly === "true"){
                object.amount = localStorage.getItem('MonthlyAMNT');
                object.time = localStorage.getItem('paypaltstamp');
                CFPaymentsPaypal.cmpleteMnthlyPmntRecurring(object);
                localStorage.removeItem('MonthlyAMNT');
                localStorage.setItem('paypalpayerid', paypalpayerid);
                $('#monthlysubscribe').addClass('divdisplay');
                $('#cnclmonthlysubscribe').removeClass('divdisplay');
                $('#paypalsuccessmodal').modal('hide');
                $('#transstatus').modal('show');
            }
            else if(Yearly === "true"){
                object.amount = localStorage.getItem('YearlyAMNT');
                object.time = localStorage.getItem('paypaltstamp');
                CFPaymentsPaypal.cmpleteAnnualPmntRecurring(object);
                localStorage.setItem('paypalpayerid', paypalpayerid);
                localStorage.removeItem('YearlyAMNT');
                $('#annualsubscribe').addClass('divdisplay');
                $('#cnclannualsubscribe').removeClass('divdisplay');
                $('#paypalsuccessmodal').modal('hide');
                $('#transstatus').modal('show');
            }
        }
        else if(orangeCancel === "true"){
            $('#transstatus').find('#contclick').attr('status', 'failed');
            $('#transstatustext').html('Your payment was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
            $('#transbutton').text('Please try again');
            $('#transstatus').modal('show');
        }
    }
};

var CFSubscriptionsInternal= {

    CreateSubscription: function (subname, state, stype, ctstamp, etstamp, ipmntid ) {
        ctstamp = CFInterConvertTimeUtility.Ptstamptomillsec(ctstamp);
        var apiUrl = apicallurl + "/subscription/add";
        //console.log(apiUrl);
        if (etstamp < ctstamp) { //already expired users
            if (subname == "Monthly Recurring") {
                etstamp = CFInterConvertTimeUtility.Pincreasethirty(ctstamp);
            }
            if (subname == "Yearly Recurring") {
                etstamp = CFInterConvertTimeUtility.Pincreaseyear(ctstamp);
            }
        }
        else {
            if (subname == "Monthly Recurring") {
                etstamp = CFInterConvertTimeUtility.Pincreasethirty(etstamp);
            }
            if (subname == "Yearly Recurring") {
                etstamp = CFInterConvertTimeUtility.Pincreaseyear(etstamp);
            }
        }
        var subsarray = {"subscriptionName": subname, "userId": {"id": localStorage.getItem('UserId')}, "subscriptionStatus": state, "subscriptionType": stype, "subscriptionStartDate": ctstamp, "subscriptionEndDate": etstamp, "paymentId": ipmntid};
        //console.log(subsarray);
        var subsarrayjson = JSON.stringify(subsarray);
        //console.log(subsarrayjson);
        var subsresponse1 = "";
        $.ajax({
            type: "PUT",
            url: apiUrl,
            data: subsarrayjson,
            async: false,
            dataType: "application/json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                subsresponse1 = data;
            },

            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    subsresponse = xhr.responseText;
                    //console.log(subsresponse.id);
                    subsresponse = jQuery.parseJSON(subsresponse);
                    //console.log(subsresponse);
                }
                if (xhr.status > 300) {
                    //console.log("Payment Operation Failed");
                }
            }
        });
    },
    
    GetlastSubscriptioninfo : function (){
        var apiUrl= apicallurl + "/subscription/all/"+localStorage.getItem('UserId');
            var subsresponse1;
            $.ajax({
                type: "GET",
                url: apiUrl,
                async: false,
                dataType: "application/json",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
                },
                success: function (data) {
                    subsresponse1 = data;
                },

                complete: function (xhr, statusText) {
                    if (xhr.status == 200) {
                        subsresponse = xhr.responseText;
                        subsresponse = jQuery.parseJSON(subsresponse);
                    }
                    if (xhr.status > 300) {
                    }
                }
            });
        return subsresponse[0] ;
    },
    
    CancelSubscription : function(lstsubid) {
        var apiUrl = apicallurl + "/subscription/cancel?subscriptionId=" + lstsubid;
        //console.log(apiUrl);
        var subsresponse1;
        $.ajax({
            type: "POST",
            url: apiUrl,
            async: false,
            dataType: "application/json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                subsresponse1 = data;
            },

            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    cnclsubsresponse = xhr.responseText;
                    cnclsubsresponse = jQuery.parseJSON(subsresponse);
                }
                if (xhr.status > 300) {
                }
            }
        });
    }
};

var CFPaymentsInternal = {

    getlastpmntinfo: function (intpmntid) {
        var apiUrl = apicallurl + "/pay/info/" + intpmntid;
        //console.log(apiUrl);
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "application/json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    completeresponse = xhr.responseText;
                    completeresponse = jQuery.parseJSON(completeresponse);
                }
                if (xhr.status > 300) {
                }
            }
        });
        return completeresponse;
    },

    pmntmnthlyonetime: function (pmttid, pmntack, pmnttimestamp, pyrid, cmpletepmntresponse) {
        var apiUrl = apicallurl + "/pay/user/add";
        var pmntarray = {"referenceId": pmnttid, "userId": {"id": localStorage.getItem('UserId')}, "paymentStatus": pmntack, "paymentDate": CFInterConvertTimeUtility.Ptstamptomillsec(pmnttimestamp), "paymentType": "PAYPAL", "billingId": pyrid, "gateWayResponse": cmpletepmntresponse };
        var pmntarrayjson = JSON.stringify(pmntarray);
        $.ajax({
                type: "PUT",
                url: apiUrl,
                data: pmntarrayjson,
                async: false,
                dataType: "application/json",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
                },
                complete: function (xhr, statusText) {
                    if (xhr.status == 200) {
                        completeresponse = xhr.responseText;
                        completeresponse = jQuery.parseJSON(completeresponse);
                        cfinternalpmntid = completeresponse.id;
                    }
                    if (xhr.status > 300) {
                    }
                }
            });
        return cfinternalpmntid;
    },

    pmntmonthlyrecurring: function (pmntack, pmnttimestamp, pyrid, pmntprofid, cmpletepmntresponse) {
        var apiUrl = apicallurl + "/pay/user/add";
        var pmntarray = {"referenceId": "", "userId": {"id": localStorage.getItem('UserId')}, "paymentStatus": pmntack, "paymentDate": CFInterConvertTimeUtility.Ptstamptomillsec(pmnttimestamp), "paymentType": "PAYPAL", "billingId": pyrid, "recurringId": pmntprofid, "gateWayResponse": cmpletepmntresponse};
        var pmntarrayjson = JSON.stringify(pmntarray);
        $.ajax({
                type: "PUT",
                url: apiUrl,
                data: pmntarrayjson,
                async: false,
                dataType: "application/json",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
                },
                complete: function (xhr, statusText) {
                    if (xhr.status == 200) {
                        completeresponse = xhr.responseText;
                        completeresponse = jQuery.parseJSON(completeresponse);
                        cfinternalpmntid = completeresponse.id;
                    }
                    if (xhr.status > 300) {
                    }
                }
            }
        );
        return cfinternalpmntid;
    },

    pmntannualonetime: function (pmttid, pmntack, pmnttimestamp, pyrid, cmpletepmntresponse) {
        var apiUrl = apicallurl + "/pay/user/add";
        var pmntarray = {"referenceId": pmnttid, "userId": {"id": localStorage.getItem('UserId')}, "paymentStatus": pmntack, "paymentDate": CFInterConvertTimeUtility.Ptstamptomillsec(pmnttimestamp), "paymentType": "PAYPAL", "billingId": pyrid, "gateWayResponse": cmpletepmntresponse};
        var pmntarrayjson = JSON.stringify(pmntarray);
        $.ajax({
                type: "PUT",
                url: apiUrl,
                data: pmntarrayjson,
                async: false,
                dataType: "application/json",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
                },
                complete: function (xhr, statusText) {
                    if (xhr.status == 200) {
                        completeresponse = xhr.responseText;
                        completeresponse = jQuery.parseJSON(completeresponse);
                        cfinternalpmntid = completeresponse.id;
                    }
                    if (xhr.status > 300) {
                    }
                }
            }
        );
        return cfinternalpmntid;
    },

    pmntannualrecurring: function (pmntack, pmnttimestamp, pyrid, pmntprofid, cmpletepmntresponse) {
        var apiUrl = apicallurl + "/pay/user/add";
        var pmntarray = {"referenceId": "", "userId": {"id": localStorage.getItem('UserId')}, "paymentStatus": pmntack, "paymentDate": CFInterConvertTimeUtility.Ptstamptomillsec(pmnttimestamp), "paymentType": "PAYPAL", "billingId": pyrid, "recurringId": pmntprofid, "gateWayResponse": cmpletepmntresponse};
        var pmntarrayjson = JSON.stringify(pmntarray);
        $.ajax({
            type: "PUT",
            url: apiUrl,
            data: pmntarrayjson,
            async: false,
            dataType: "application/json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    completeresponse = xhr.responseText;
                    completeresponse = jQuery.parseJSON(completeresponse);
                    cfinternalpmntid = completeresponse.id;
                }
                if (xhr.status > 300) {
                }
            }
        });
        return cfinternalpmntid;
    }
};

var getuserexpiry = {

    searchuserexpiry: function (user) {
        var jsondata = "";
        $.ajax({
            type: "GET",
            url: apicallurl + "/users/validate?searchUser=" + user,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data, textStatus, xhr) {
                var serverDate = CFInterConvertTimeUtility.Ptstamptomillsec(xhr.getResponseHeader("Date"));
                data.servetime = serverDate;
                jsondata = data;
            }
        });
        return jsondata;
    },

    getSubscriptionExpiry:function(){
        var data = this.searchuserexpiry(localStorage.getItem('UserId'));
        serverDate = data.servetime;
        if (data[0].expiresIn != undefined && data[0].expiresIn < serverDate) {
            if (serverDate - data[0].expiresIn > (3 * 24 * 60 * 60 * 1000)) {
                $('#expireMsg').html('');
                $('#expireMsg').html('<div  style="font-size: 13pt;text-align: center;color: #f90;background: #F5F5F5;" class=""><span>Your subscription has expired ' + jQuery.timeago(data[0].expiresIn) + '.Please subscribe to continue...</span></div>');
                $('#monthlysubscribe').removeClass('divdisplay');
                $('#cnclmonthlysubscribe').addClass('divdisplay');
                $('#expirelnk').removeClass('divdisplay');
            }
            else if (0 < serverDate - data[0].expiresIn < (3 * 24 * 60 * 60 * 1000)) {
                $('#expireMsg').html('');
                $('#main').prepend('<div id="expireMsg" style="font-size: 13pt;text-align: center;color: #f90;background: #F5F5F5;" class=""><span>Your subscription has expired ' + jQuery.timeago(data[0].expiresIn) + '.Please subscribe to continue...</span></div>');
                $('#expirelnk').removeClass('divdisplay');
            }
        }
        else if (0 > serverDate - data[0].expiresIn && serverDate - data[0].expiresIn > -(3 * 24 * 60 * 60 * 1000)) {
            $('#expireMsg').html('');
            $('#main').prepend('<div id="expireMsg" style="font-size: 13pt;text-align: center;color: #f90;background: #F5F5F5;" class=""><span>Your subscription will expire on ' + CFManageCloudAccountsAjaxCall.getDateConversion(data[0].expiresIn) + '.Please extend your <a href="settings.html?pinfo=true">subscription</a></span></div>');
            $('#expirelnk').removeClass('divdisplay');
        }
        else if (0 > serverDate - data[0].expiresIn && serverDate - data[0].expiresIn > -(11 * 24 * 60 * 60 * 1000)) {
            $('#expireMsg').html('');
            var diffDate = data[0].expiresIn - serverDate;
            var finaldate = diffDate / 86400000;
            finaldate = Math.round(finaldate);
            if (finaldate == 0) {
                $('#main').prepend('<div id="expireMsg" style="font-size: 13pt;text-align: center;color: #f90;background: #f5f5f5" class=""><span>Your subscription will expire today.Please extend your <a href="settings.html?pinfo=true">subscription</a></span></div>');
            }
            if (finaldate > 0) {
                $('#main').prepend('<div id="expireMsg" style="font-size: 13pt;text-align: center;color: #f90;background: #f5f5f5" class=""><span>Your subscription will expire in ' + finaldate + ' day(s).Please extend your <a href="settings.html?pinfo=true">subscription</a></span></div>');
            }
            $('#expirelnk').removeClass('divdisplay');
        }
    },

    getLastSubscription:function(){
        lastsubsresponse = CFSubscriptionsInternal.GetlastSubscriptioninfo();
        if (lastsubsresponse == undefined) {
            $('#subsstatus').text("Active");
            $('#subsname').text("Free Trail");
            $('#subslevel').text('Free Trail');
            var expdate = getuserexpiry.searchuserexpiry(localStorage.getItem('UserId'));
            var expdate_expiredDate = expdate[0].expiresIn;
            var expdate_createdDate = expdate[0].createdDate;
            if (expdate_expiredDate == null || expdate_expiredDate == "") {
                var testconv1 = CFManageCloudAccountsAjaxCall.getDateConversion(CFInterConvertTimeUtility.Pincreasethirty(expdate_createdDate));
                $('#subsexpdate').text(testconv1);
                $('#setingexpdate').text(testconv1);
                $('#pmntstatus').text("N/A");
                $('#pmntdate').text("N/A");
                $('#pmntlevel').text('N/A');
            }
            else {
                var testconv1 = CFManageCloudAccountsAjaxCall.getDateConversion(expdate_expiredDate);
                $('#subsexpdate').text(testconv1);
                $('#setingexpdate').text(testconv1);
                $('#pmntstatus').text("N/A");
                $('#pmntdate').text("N/A");
                $('#pmntlevel').text('N/A');
            }
        }
        else {
            $('#subsstatus').text(lastsubsresponse.subscriptionStatus);
            $('#subsname').text(lastsubsresponse.subscriptionName);
            var testconv1 = CFManageCloudAccountsAjaxCall.getDateConversion(lastsubsresponse.subscriptionEndDate);
            $('#subsexpdate').text(testconv1);
            $('#setingexpdate').text(testconv1);
            if (lastsubsresponse.subscriptionType == "TRAIL" || lastsubsresponse.subscriptionType == "EXTENDED_TRAIL") {
                $('#subslevel').text('Free Trail');
            } else {
                $('#subslevel').text('Paid');
            }

            if(CFPaymentsInternal != undefined){
                lastpmntresponse = CFPaymentsInternal.getlastpmntinfo(lastsubsresponse.paymentId);
            }
            if(lastpmntresponse != undefined && lastpmntresponse != null){
                $('#pmntstatus').text(lastpmntresponse.paymentStatus);//TODO - check response is null - Rat, Mike(Done)
                $('#pmnttype').text(lastpmntresponse.paymentType);
                $('#pmntdate').text(CFManageCloudAccountsAjaxCall.getDateConversion(lastpmntresponse.paymentDate));
                if (lastpmntresponse.referenceId == "" || lastpmntresponse.referenceId == null) {
                    $('#pmntlevel').text('Recurring');
                } else if (lastpmntresponse.recurringId == "" || lastpmntresponse.recurringId == null) {
                    $('#pmntlevel').text('One Time');
                }
            }

            if (lastsubsresponse.subscriptionName == "") {
            } else if ((lastsubsresponse.subscriptionName).indexOf("Month") != -1 && lastsubsresponse.subscriptionStatus == "ACTIVE") {
                $('#monthlysubscribe').addClass('divdisplay');
                $('#cnclmonthlysubscribe').removeClass('divdisplay');
            }
            if ((lastsubsresponse.subscriptionName).indexOf("Year") != -1 && lastsubsresponse.subscriptionStatus == "ACTIVE") {
                $('#annualsubscribe').addClass('divdisplay');
                $('#month').addClass('divdisplay');
                $('#cnclannualsubscribe').removeClass('divdisplay');
            }

            if ((lastsubsresponse.subscriptionName).indexOf("Month") == -1 || lastsubsresponse.subscriptionStatus == "INACTIVE") {
                $('#monthlysubscribe').removeClass('divdisplay');
                $('#cnclmonthlysubscribe').addClass('divdisplay');
            }
            if ((lastsubsresponse.subscriptionName).indexOf("Year") == -1 || lastsubsresponse.subscriptionStatus == "INACTIVE") {
                $('#annualsubscribe').removeClass('divdisplay');
                $('#month').removeClass('divdisplay');
                $('#cnclannualsubscribe').addClass('divdisplay');
            }
        }
    }
};

var CFInterConvertTimeUtility = {
    Ptstamptomillsec: function (timestmp) {
        var myDate = new Date(timestmp);
        return myDate.getTime();
    },
    Pincreasethirty: function (timestmp) {
        var myDate = new Date(timestmp);
        var result = myDate.getTime();
        result = result + (30 * 24 * 60 * 60 * 1000);
        return result;
    },
    Pincreaseyear: function (timestmp) {
        var myDate = new Date(timestmp);
        var result = myDate.getTime();
        result = result + (365 * 24 * 60 * 60 * 1000);
        return result;
    },
    PmillisectoDate: function (milsec) {
        milsec1 = new Date(milsec).toString();
        return milsec1;
    }
};
$('#closepricing').on('click', function() {
    $('#couponsmessage').text('');
    $('#couponemessage').text('');
    $('#couponcode').val('');
    var tmpamnt = $('#amnt').text();
    DiscAMT = 0;
    $('#discount').text(DiscAMT);
    $('#totalamnt').text((tmpamnt - DiscAMT).toFixed(2));

});

$('#monthlysubscribe').on('click', function() {
    //_gaq.push(['_trackEvent',"Upgrade", localStorage.getItem('UserId'),"Monthly"]);
    sendGAEvents("Upgrade","Monthly");
    var _e = JSON.parse(window.localStorage.CFUser).primaryEmail.split('.');
    _e = _e[_e.length -1];
    var finalAmount;
    if(_e == "org"){
        DiscAMT = (12/100)*MonthlyAMT;
        DiscAMT = DiscAMT.toFixed(2);
        finalAmount = MonthlyAMT - DiscAMT;
        finalAmount =  finalAmount.toFixed(2)
    }
    else if(_e == "edu"){
        DiscAMT = (20/100)*MonthlyAMT;
        DiscAMT = DiscAMT.toFixed(2);
        finalAmount = MonthlyAMT - DiscAMT;
        finalAmount = finalAmount.toFixed(2)
    }
    else{
        finalAmount = MonthlyAMT - DiscAMT
    }
    $('#subscribetype').text('Monthly Subscription');
    $('#amnt').text(MonthlyAMT);
    $('#discount').text(DiscAMT);
    var _cur = JSON.parse(localStorage.getItem('CFUser')).currency;
    if(_cur == null || _cur != 'EUR'){
        _cur = 'USD';
    }
    var cur = {
        'USD':'$',
        'EUR':'\u20AC'
    };
    $('#calcpricemodal').find('[data-currency]').attr('data-currency',_cur);
    $('#calcpricemodal').find('[data-currency]').text(cur[_cur]);
    $('#totalamnt').text(finalAmount);
    $('#calcpricemodal').modal('show');
});

$('#annualsubscribe').on('click', function() {
    sendGAEvents("Upgrade","Anually");
    //_gaq.push(['_trackEvent',"Upgrade", localStorage.getItem('UserId'),"Anually"]);
    var  _e = JSON.parse(window.localStorage.CFUser).primaryEmail.split('.');
    _e = _e[_e.length -1];
    var finalAmount;
    if(_e == "org"){
        DiscAMT = (12/100)*YearlyAMT;
        DiscAMT = DiscAMT.toFixed(2);
        finalAmount = YearlyAMT - DiscAMT;
        finalAmount = finalAmount = finalAmount.toFixed(2)
    }else if(_e == "edu"){
        DiscAMT = (20/100)*YearlyAMT;
        DiscAMT = DiscAMT.toFixed(2);
        finalAmount = YearlyAMT - DiscAMT;
        finalAmount = finalAmount.toFixed(2)
    }else{
        finalAmount = YearlyAMT - DiscAMT
    }
    $('#subscribetype').text('Annual Subscription');
    $('#amnt').text(YearlyAMT);
    $('#discount').text(DiscAMT);
    $('#totalamnt').text(finalAmount );
    $('#calcpricemodal').modal('show');
});
 
$('#cnclmonthlysubscribe').on('click', function() {
    $('#cnclmonthlysubscribemodal').modal('show');
});

$('#cnclannualsubscribe').on('click', function(){
    $('#cnclannualsubscribemodal').modal('show');   
});

$('#cnclmonthlysubscribeyes').on('click', function() {
    $('#processimg').removeClass('divdisplay');
    $('#cnclmonthlysubscribemodal').modal('hide');
    $('#cnclsubscribemodalfinal').modal('show');
    if (lastpmntresponse.recurringId && lastpmntresponse.referenceId == "") {
        var status2 = CFPaymentsPaypal.cnclRecurringSubscription(lastpmntresponse.recurringId);
        if (status2 == "Success") {
            CFSubscriptionsInternal.CancelSubscription(lastsubsresponse.id);
            $('#transstatustext').text('Thank you for Choosing CloudFuze. Your cancellation was successful.');
        } else {
            $('#transstatustext').html('Your cancellation was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
            $('#transbutton').text('Please try again');
        }
    } else {
        CFSubscriptionsInternal.CancelSubscription(lastsubsresponse.id);
    }
    $('#cnclsubscribemodalfinal').modal('hide');
    $('#transstatus').modal('show');
});

$('#cnclannualsubscribeyes').on('click', function() {
    $('#processimg').removeClass('divdisplay');
    $('#cnclmonthlysubscribemodal').modal('hide');
    $('#cnclsubscribemodalfinal').modal('show');
    if (lastpmntresponse.recurringId && lastpmntresponse.referenceId == "") {
        var status3 = CFPaymentsPaypal.cnclRecurringSubscription(lastpmntresponse.recurringId);
        if (status3 == "Success") {
            CFSubscriptionsInternal.CancelSubscription(lastsubsresponse.id);
            $('#transstatustext').text('Thank you for Choosing CloudFuze. Your cancellation was successful.');
        } else {
            $('#transstatustext').html('Your cancellation was not successful. Please try again. If still facing problems, contact <a href="mailto:support@cloudfuze.com"> support@cloudfuze.com </a>');
            $('#transbutton').text('Please try again');
        }
    } else {
        CFSubscriptionsInternal.CancelSubscription(lastsubsresponse.id);
    }

    $('#cnclsubscribemodalfinal').modal('hide');
    $('#transstatus').modal('show');
});

$('#contclick').on('click', function() {
    var instance = $(this).attr('instance');
    var status = $(this).attr('status');
    if(instance == null || instance == undefined){
        window.location.href = "settings.html?pinfo=true";
    }
    else{
        if(status == "failed"){
            localStorage.clear();
            sessionStorage.clear();
            window.location.href = "index.html?orange=true";
        }
        else{
            window.location.href = "cloudmanager.html#orange";
        }
    }
});

$('#applycoupon').on('click', function() {
    var substypechk = $('#subscribetype').text();
    var bla = $('#couponcode').val();

    if (bla === "") {
        $('#couponemessage').text('Please enter coupon code');
        $('#couponsmessage').text('');
        var tmpamnt = $('#amnt').text();
        DiscAMT = 0;
        $('#discount').text(DiscAMT);
        $('#totalamnt').text((tmpamnt - DiscAMT).toFixed(2));
    }else if ($.inArray(CryptoJS.MD5(bla).toString(), cou5) > -1 && substypechk === "Annual Subscription") {
        $('#couponsmessage').text('Your Coupon has been Applied Successfully');
        $('#couponcode').val("");
        $('#couponemessage').text('');
        var tmpamnt = $('#amnt').text();
        DiscAMT = 0.05 * tmpamnt;
        DiscAMT = DiscAMT.toFixed(2);
        $('#discount').text(DiscAMT);
        $('#totalamnt').text((tmpamnt - DiscAMT).toFixed(2));
    }else if ($.inArray(CryptoJS.MD5(bla).toString(), cou10) > -1 && substypechk === "Annual Subscription") {
        $('#couponsmessage').text('Your Coupon has been Applied Successfully');
        $('#couponcode').val("");
        $('#couponemessage').text('');
        var tmpamnt = $('#amnt').text();
        DiscAMT = 0.10 * tmpamnt;
        DiscAMT = DiscAMT.toFixed(2);
        $('#discount').text(DiscAMT);
        $('#totalamnt').text((tmpamnt - DiscAMT).toFixed(2));
    }else if ($.inArray(CryptoJS.MD5(bla).toString(), cou20) != -1 && substypechk === "Annual Subscription") {
        $('#couponsmessage').text('Your Coupon has been Applied Successfully');
        $('#couponcode').val("");
        $('#couponemessage').text('');
        var tmpamnt = $('#amnt').text();
        DiscAMT = 0.20 * tmpamnt;
        DiscAMT = DiscAMT.toFixed(2);
        $('#discount').text(DiscAMT);
        $('#totalamnt').text((tmpamnt - DiscAMT).toFixed(2));
    }else if ($.inArray(CryptoJS.MD5(bla).toString(), cou50) != -1 && substypechk === "Annual Subscription") {
        $('#couponsmessage').text('Your Coupon has been Applied Successfully');
        $('#couponcode').val("");
        $('#couponemessage').text('');
        var tmpamnt = $('#amnt').text();
        DiscAMT = 0.50 * tmpamnt;
        DiscAMT = DiscAMT.toFixed(2);
        $('#discount').text(DiscAMT);
        $('#totalamnt').text((tmpamnt - DiscAMT).toFixed(2));
    }else {
        $('#couponemessage').text('Invalid coupon code');
        $('#couponsmessage').text('');
        var tmpamnt = $('#amnt').text();
        DiscAMT = 0;
        $('#discount').text(DiscAMT);
        $('#totalamnt').text(tmpamnt);
    }
});

var substype;
var MOtime;
var YOtime;
$('#checkoutbutton').on('click', function() {
    var isChecked = $('#terms').is(':checked');
    var _a = {
        "amount":null,
        "code":$('#calcpricemodal').find('[data-currency]').attr('data-currency'),
        "success":"paypalsuccess",
        "cancel":"paypalcancel"
    };
    if (isChecked) {
        substype = $('#subscribetype').text();
        MOtime = $('#autorenewal').is(':checked');
        YOtime = $('#autorenewal2').is(':checked');
        if (substype === "Monthly Subscription" && MOtime == true) {
            Monthly = true;
            MonthlyAMT = $('#totalamnt').text();
            localStorage.setItem('MonthlyAMNT', MonthlyAMT);
            _a.amount = MonthlyAMT;
            CFPaymentsPaypal.createPmntMnthlyRecurring(_a);
            $('#calcpricemodal').modal('hide');
            $('#paypalsuccessmodalinit').modal('show');
        }
        else if (substype === "Annual Subscription" && YOtime == true) {
            Yearly = true;
            YearlyAMT = $('#totalamnt').text();
            localStorage.setItem('YearlyAMNT', YearlyAMT);
            _a.amount = YearlyAMT;
            CFPaymentsPaypal.createPmntAnnualRecurring(_a);
            $('#calcpricemodal').modal('hide');
            $('#paypalsuccessmodalinit').modal('show');
        }
        else if (substype === "Monthly Subscription" && MOtime == false) {
            Monthly = true;
            MonthlyAMT = $('#totalamnt').text();
            localStorage.setItem('MonthlyAMNT', MonthlyAMT);
            _a.amount = MonthlyAMT;
            CFPaymentsPaypal.createPmntMnthlyOneTime(_a);
            $('#calcpricemodal').modal('hide');
            $('#paypalsuccessmodalinit').modal('show');
        }
        else if (substype === "Annual Subscription" && YOtime == false) {
            Yearly = true;
            YearlyAMT = $('#totalamnt').text();
            localStorage.setItem('YearlyAMNT', YearlyAMT);
            _a.amount = YearlyAMT;
            CFPaymentsPaypal.createPmntAnnualOneTime(_a);
            $('#calcpricemodal').modal('hide');
            $('#paypalsuccessmodalinit').modal('show');
        }
    }
    else {
        $('#couponemessage').html('Please agree for terms and condtions');
    }
});
$("#editPswd").click(function() {
    $('#Cpswd').val('');
    $('#Rpswd').val('');
    $('#Cpswd').css("border-color", "");
    $('#Rpswd').css("border-color", "");
    $('#Cpswd').focus();
    $('#stars').hide();
    $('#changePwd').show();
    $('body').on('mouseup', function (e) {
        var container = $('#changePwd');
        if (!container.is(e.target)
            && container.has(e.target).length === 0) {    //console.log("jsf");
            $('.changePwdCancel').trigger('click');
            $('body').off('mouseup');
        }
    });
});
$('#changePwd').keydown(function (event) {
    if (event.keyCode == 13) {
        $('.changePwdOk').trigger("click");
    }
});
$('.changePwdCancel').click(function() {
    $('#stars').show();
    $(this).parent().hide();
});
$('.changePwdOk').click(function() {
    $('#Cpswd').css("border-color", "");
    $('#Rpswd').css("border-color", "");
    var pswd = $('#Cpswd').val();
    var Rpswd = $('#Rpswd').val();
    if (pswd == "" && Rpswd == "") {
        $.smallBox({title: "Please enter the required fields.", color: "#e35e00", timeout: notifyTime, sound: false});
        $('#Cpswd').css("border-color", "red");
        $('#Rpswd').css("border-color", "red");
        $('#Cpswd').focus();
        return false;
    }else if (pswd == "") {
        $.smallBox({title: "Please enter password.", color: "#e35e00", timeout: notifyTime, sound: false});
        $('#Cpswd').css("border-color", "red");
        $('#Cpswd').focus();
        return false;
    }else if (pswd.length > 18) {
        $.smallBox({title: "Please enter a password less than 18 characters.", color: "#e35e00", timeout: notifyTime, sound: false});
        $('#Cpswd').css("border-color", "red");
        $('#Cpswd').focus();
        return false;
    }else if (pswd.length < 6) {
        $.smallBox({title: "Please enter a password with atleast 6 characters.", color: "#e35e00", timeout: notifyTime, sound: false});
        $('#Cpswd').css("border-color", "red");
        $('#Cpswd').focus();
        return false;
    }else if (Rpswd == "") {
        $.smallBox({title: "Please Reenter password.", color: "#e35e00", timeout: notifyTime, sound: false});
        $('#Rpswd').css("border-color", "red");
        $('#Rpswd').focus();
        return false;
    } else if (Rpswd.length > 18) {
        $.smallBox({title: "Please enter a confirm password less than 18 characters.", color: "#e35e00", timeout: notifyTime, sound: false});
        $('#Cpswd').css("border-color", "red");
        $('#Cpswd').focus();
        return false;
    }
    else if (Rpswd.length < 6) {
        $.smallBox({title: "Please enter a confirm password with atleast 6 characters.", color: "#e35e00", timeout: notifyTime, sound: false});
        $('#Cpswd').css("border-color", "red");
        $('#Cpswd').focus();
        return false;
    }
    else if (pswd != Rpswd) {
        $.smallBox({title: "Password and Confirm password doesnot match.", color: "#e35e00", timeout: notifyTime, sound: false});
        $('#Rpswd').css("border-color", "red");
        $('#Rpswd').focus();
        return false;
    }
    if (pswd == Rpswd) {
        pswd = CryptoJS.MD5(pswd);
        Rpswd = CryptoJS.MD5(Rpswd);
        var apiurl = apicallurl + '/auth/user/changepassword?newPassword=' + Rpswd + '';
        $.ajax({
            type: "POST",
            url: apiurl,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": localStorage.getItem("UserAuthDetails"),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                if (data == null) {
                    $('#AccountSettings .DisplayMsg').text('Password updated successfully');
                    var uid = localStorage.getItem("UserId");
                    var pwd = pswd;
                    localStorage.setItem('UserAuthDetails', CFHPActions.BasicAuth(uid, pwd));
                    $.smallBox({title: "Your password updated successfully.", color: "#1ba1e2", timeout: notifyTime, sound: false});
                    $('.changePwdCancel').trigger('click');
                }
            },
            complete: function (xhr, statusText) {
                if (xhr.status > 300) {
                    $.smallBox({title: "Operation Failed.", color: "#e35e00", timeout: notifyError, sound: false});
                }
            },
            error: function (error) {
            }
        });
    }
});

$('#cf-logout').on('click',function(e) {
    e.preventDefault();
    localStorage.clear();
    sessionStorage.clear();
    window.location.href = "index.html?ver=0217";
});

$('#CFEditName').on('click', function() {
    $(this).hide();
    var UserName = $('.ProfileContent').text().trim();
    $('.ProfileContent').html("<span class='editUser'><input type='text' name='username' id='uName' value='" + UserName + "' autofocus /></span>");
    $('.ProfileContent').append("<i class='userNameOk' style='cursor:pointer;margin-left:10px;'></i><i class='userNameCancel' style='margin-left:10px;cursor:pointer;'></i>");
    $('#uName').focus();
    $('.userNameCancel').click(function () {
        $('.ProfileContent').text(UserName);
        $('#CFEditName').show();
        $('.userNameCancel').hide();
        $('.userNameOk').hide();
    });
    $('.userNameOk').click(function () {
        var Updatedusername = $('#uName').val();
        var userReg = /[^a-zA-Z ]/;
        if (Updatedusername == "") {
            $('#uName').css("border-color", "red");
            return false;
        }
        if (Updatedusername.length > 20 || Updatedusername.length > 0 && Updatedusername.length < 2) {
            $('#uName').css("border-color", "red");
            //toastr.error("Please enter your Name.Accepts 2-20 characters in this field.");
            $.smallBox({title: "Please enter your Name.Accepts 2-20 characters in this field.", color: "#e35e00", timeout: 2000, sound: false});
            return false;
        }
        else if (userReg.test(Updatedusername)) {
            $('#uName').css("border-color", "red");
            $.smallBox({title: "Please enter a valid Name. Name can contain characters (a-z, A-Z).", color: "#e35e00", timeout: 2000, sound: false});
            //toastr.error("Please enter a valid Name. Name can contain characters (a-z, A-Z).");
            return false;
        } else {
            $('#uName').css("border-color", "");
        }

        CFManageCloudAccountsAjaxCall.updateUserName(Updatedusername);
        $('#CFEditName').show();
        $('.userNameCancel').hide();
        $('.userNameOk').hide();

    });
    $('.ProfileContent input').keydown(function (event) {
        var Updatedusername = $('#uName').val();
        if (event.keyCode == 13) {
            $('.userNameOk').trigger("click");
        }
    });
    $('body').on('mouseup', function (e) {
        var container = $('.ProfileContent');
        if (!container.is(e.target)
            && container.has(e.target).length === 0) {    //console.log("jsf");
            $('.userNameCancel').trigger('click');
            $('body').off('mouseup');
        }
    });
});
$('#CFEditMobile').on('click', function() {
    $(this).hide();
    var UserName = $('.ProfileContentmobile').text().trim();
    $('.ProfileContentmobile').html("<span class='editMobile'><input type='text' name='username' id='uMobile' value='" + UserName + "' autofocus /></span>");
    $('.ProfileContentmobile').append("<i class='userNameOk' style='cursor:pointer;margin-left:10px;'></i><i class='userNameCancel' style='margin-left:10px;cursor:pointer;'></i>");
    $('#uName').focus();
    $('.userNameCancel').click(function () {
        $('.ProfileContentmobile').text(UserName);
        $('#CFEditMobile').show();
        $('.userNameCancel').hide();
        $('.userNameOk').hide();
    });
    $('.userNameOk').click(function () {
        var Updatedusername = $('#uMobile').val();
        var userReg = /[^0-9 ]/;
         if (Updatedusername == "") {
            $('#uMobile').css("border-color", "red");
             return false;
         }
          if (Updatedusername.length > 10 || Updatedusername.length < 10) {
            $('#uMobile').css("border-color", "red");
             return false;
          }
         else if (userReg.test(Updatedusername)) {
            $('#uMobile').css("border-color", "red");
        //     $.smallBox({title: "Please enter a valid Name. Name can contain characters (a-z, A-Z).", color: "#e35e00", timeout: 2000, sound: false});
        //     //toastr.error("Please enter a valid Name. Name can contain characters (a-z, A-Z).");
             return false;
        } else {
             $('#uMobile').css("border-color", "");
         }

        CFManageCloudAccountsAjaxCall.updateMobileNumber(Updatedusername);
        $('#CFEditMobile').show();
        $('.ProfileContentmobile').show();
        $('.userNameCancel').hide();
        $('.userNameOk').hide();

    });
    $('body').on('mouseup', function (e) {
        var container = $('.ProfileContentmobile');
        if (!container.is(e.target)
            && container.has(e.target).length === 0) {    //console.log("jsf");
            $('.userNameCancel').trigger('click');
            $('body').off('mouseup');
        }
    });
});