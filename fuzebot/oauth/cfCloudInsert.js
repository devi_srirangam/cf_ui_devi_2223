function apiPutData(userId, cloudaccesstoken, cloudrefreshtoken, cloudUserId, cloudName, userDisplayName, authorizationCode, domainURL_other) {
	var domainURL_put = null;
	if (domainURL_other == undefined || domainURL_other == "undefined" || domainURL_other==window.location.origin){
		domainURL_put =domainURL+"/proxyservices";
	}
	else if(domainURL_put == "https://slwebapp.cloudfuze.com"){
		domainURL_put =  domainURL_other+"proxyservices";
	}
	else{
		domainURL_put =  domainURL_other+"proxyservices";
		putFrameURL = domainURL_other;
	}
	//_gaq.push(['_trackEvent',  , localStorage.getItem('UserId'),cloudName]);
	sendGAEvents("Add Cloud",cloudName);
	var CloudObject = {
		'cloudName': cloudName,
		'userId': userId,
		'cloudStatus': 'ACTIVE',
		'type': 'CLOUD',
        'cloudNotes': null,
		'pictureUrl': null,
		'userDisplayName': userDisplayName,
		'totalSpace': 0,
		'usedSpace': 0,
		'cloudUserId': cloudUserId, //'test.cloudfuze6@gmail.com',
		'authorizationCode': authorizationCode,
		'cfoAuthCredential': null
	};
	if(cloudName == "SHAREPOINT_ONLINE"){
		CloudObject.rootFolderId = '/';
	}

	CloudObject.accesstoken = cloudaccesstoken;
	CloudObject.refreshtoken = cloudrefreshtoken;


	if(localStorage.getItem('putUrl') == null){
		var apiUrl = domainURL_put + "/v1/users/" + userId + "/cloud/create";
		if(cloudName == "CISCO_SPARK"){
			apiUrl += "/spark";
		}
		//apiUrl += "?cloudaccesstoken=" + encodeURIComponent(cloudaccesstoken) + "&cloudrefreshtoken=" + encodeURIComponent(cloudrefreshtoken);

		$.ajax({
			type: 'PUT',
			url: apiUrl,
			headers: {
				"Content-Type": "application/json",
				"Access-Control-Allow-Methods": "GET, POST, PUT, DELETE"
			},
			data: JSON.stringify(CloudObject),
			dataType: 'json',
			success: function () {
				location.href="/oauth/oauth.html?cloudSuccess=true";
				closebox("Completed",cloudName);
			},
			complete:function(xhr, statusText){
				if(xhr.status == 409){
					if(localStorage != null) {
						localStorage.setItem("OauthProcess", "alreadyExist");
						closebox("alreadyExist");
						location.href="/oauth/oauth.html?cloudConflict=true"
					}
				}
				else if(xhr.status > 300 && xhr.status != 406){
					if(localStorage != null) {
						localStorage.setItem("OauthProcess", "failed");
						closebox("failed");
						location.href="/oauth/oauth.html?cloudFail=true"
					}
				}
			}
		});
	}
	else{
		var _putUrl = localStorage.getItem('putUrl') + "/users/" + userId + "/cloud/create";
		if(cloudName == "CISCO_SPARK"){
			_putUrl += "/spark";
		}
		var _frame =$('#putFrame');
		//_putUrl +="?cloudaccesstoken=" + encodeURIComponent(cloudaccesstoken) + "&cloudrefreshtoken=" + encodeURIComponent(cloudrefreshtoken);

		var html = "<iframe id='putFrame' style='display:none'></iframe>";

		_frame.remove();

		CloudObject.putUrl = _putUrl;

        putFrameURL = localStorage.getItem('putUrl').split('.com/')[0]+".com/pages/cloudput.html?ver=0502&data="+Base64.encode(JSON.stringify(CloudObject));

		$('body').append(html);

		$('#putFrame').attr('src',putFrameURL);
	}
}
function make_base_auth(user, password) {
	var tok = user + ':' + password;
	var hash = btoa(tok);
	return "Basic " + hash;
}
function closebox(status,cloudName) {
	if (typeof (Storage) !== "undefined") {
		$('#processStatemsg').text("Operation " + status);
		localStorage.setItem("OauthProcess", status);
		window.close();
	}
	else {
		window.location.href = domainURL + "/oauth/closebox.html?status=" + status;
		window.close();
	}
}

function egnyteInsert(domain,token){
	var domainURL_put = null;
	var domainURL_other;
	if (domainURL_other == undefined || domainURL_other == "undefined" || domainURL_other==window.location.origin){
		domainURL_put =domainURL+"/proxyservices";
	}
	else{
		domainURL_put =  domainURL+"/proxyservices";
	}
	var arr = { "cloudName": "EGNYTE_STORAGE",
		"userId" : userId,
		"cloudStatus": "ACTIVE",
		"type" : "CLOUD",
		"cloudNotes": null,
		"pictureUrl": null,
		"totalSpace": 0,
		"usedSpace": 0,
		"cloudUserId": domain,
		"userDisplayName" : domain,
		"cfoAuthCredential": null
	};

	$('#egnyteDomainToken').hide();
	$('#authProcess').show();
	arr.accesstoken = token+":"+domain;

	var userId = localStorage.getItem('UserId');

	if(localStorage.getItem('putUrl') == null){
		var apiUrl = domainURL_put+"/v1/users/"+userId+"/cloud/create";
		$.ajax({
			type: 'PUT',
			url: apiUrl,
			headers: {
				"Content-Type": "application/json"
			},
			data: JSON.stringify(arr),
			dataType: 'json',
			success: function () {
				closebox("Completed");
			},
			complete:function(jqXHR, textStatus, errorThrown){
				if(jqXHR.status == 409){
					closebox("alreadyExist");
				}
				else if(jqXHR.status > 300){
					closebox("failed");
				}
				else if(textStatus == "error"){
					closebox("failed");
				}
			}
		});
	}
	else{
		apiUrl = localStorage.getItem('putUrl') + "/users/" + userId + "/cloud/create";

		$('#putFrame').remove();

		var html = "<iframe id='putFrame' style='display:none'></iframe>";

		arr.putUrl = apiUrl;

		var putFrameURL = localStorage.getItem('putUrl').split('.com/')[0]+".com/pages/cloudput.html?data="+Base64.encode(JSON.stringify(arr));

		$('body').append(html);

		$('#putFrame').attr('src',putFrameURL);
	}
	
}