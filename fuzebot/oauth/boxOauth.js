var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
/////=======================box selection events===================================
//Google Drive
function redirectGDrive(state) {
    window.location = 'https://accounts.google.com/o/oauth2/auth?client_id=' + CloudFuze.clientId +
        '&redirect_uri=' + CloudFuze.redirecturl +
        '&scope=https://www.googleapis.com/auth/drive profile email&response_type=code&access_type=offline&' +
        'include_granted_scopes=true&approval_prompt=force&state=' + encodeURIComponent(state);
}
function getToken_GDrive(code, userId, url) {
    var keys = getOauthKeys('G_DRIVE');
    if(keys == undefined || keys == null){
        return alert('Client keys not registered with the current domain!!!');
    }
    localStorage.setItem("OauthProcess", "Inprogress");
    $('#ProcessMessage').append("<p id='CurrentProccessingState' class='CurrentProccessingState'>Processing</p>");
    var data = "code=" + code + "&client_id=" + CloudFuze.clientId + "&client_secret=" + CloudFuze.clientSecret + "&redirect_uri=" + CloudFuze.redirecturl + "&grant_type=authorization_code";
    $.ajax({
        url: grdrive_postURL,
        type: "POST",
        data: data,
        success: function (data) {
            getGoogleUserEmails(data, code, userId, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}
function getGoogleUserEmails(data, code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    $.ajax({
        url: google_userinfo + data.access_token,
        type: "GET",
        success: function (result) {
            apiPutData(userId, data.access_token, data.refresh_token, result.email, "G_DRIVE", result.given_name, code, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}

//DropBox
function redirectDropBox(state) {
    window.location = 'https://www.dropbox.com/1/oauth2/authorize?client_id=' + CloudFuze.clientId +
        '&response_type=code&force_reapprove=true&redirect_uri=' + CloudFuze.redirecturl +
        '&state=' + encodeURIComponent(state);
}
function doOauth_dropbox(code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    $('#ProcessMessage').append("<p id='CurrentProccessingState' class='CurrentProccessingState'>Processing</p>");
    data = "code=" + code + "&grant_type=authorization_code&client_id=" + CloudFuze.clientId + "&client_secret=" + CloudFuze.clientSecret + "&redirect_uri=" + CloudFuze.redirecturl;
    $.ajax({
        url: dropbox_redirect_URL,
        type: "POST",
        data: data,
        dataType: 'text',
        success: function (result) {
            result = JSON.parse(result);
            apiPutData(userId, result.access_token, " ", result.uid, "DROP_BOX", "", "", url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}

//DropBox Business
function redirectDropBoxBusiness(state) {
    window.location = 'https://www.dropbox.com/1/oauth2/authorize?client_id=' + CloudFuze.clientId +
        '&response_type=code&redirect_uri=' + CloudFuze.redirecturl +
        '&state=' + encodeURIComponent(state);
}
function doOauth_dropboxBusiness(code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    $('#ProcessMessage').append("<p id='CurrentProccessingState' class='CurrentProccessingState'>Processing</p>");
    data = "code=" + code + "&grant_type=authorization_code&client_id=" + CloudFuze.clientId + "&client_secret=" + CloudFuze.clientSecret + "&redirect_uri=" + CloudFuze.redirecturl;
    $.ajax({
        url: dropbox_business_token,
        type: "POST",
        data: data,
        dataType: 'text',
        success: function (result) {
            result = JSON.parse(result);
            apiPutData(userId, result.access_token, " ", result.uid, "DROPBOX_BUSINESS", "", "", url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}

//Egnyte
function getEgnyteDetails(){
	var Egnytedomain = $('#egnyteDomain').val().trim();
    var state = localStorage.getItem('UserId')+"_"+Egnytedomain;
	if (Egnytedomain.length < 1) {
		$('#egnyteStatusMsg').text('All Fields are required.');
		return false;
	}
	if (Egnytedomain.length > 0) {
		window.location = 'https://'+Egnytedomain+'.egnyte.com/puboauth/token?client_id='
            +CloudFuze.clientId+'&redirect_uri=' + CloudFuze.redirecturl + '&mobile=1&scope=Egnyte.filesystem&state='+state;
    }
}

//SkyDrive
/*function redirectSkyDrive(state) {
    window.location = 'https://login.live.com/oauth20_authorize.srf?client_id='
        + CloudFuze.clientId + '&scope=wl.photos,wl.skydrive,wl.signin,wl.skydrive_update,wl.emails,wl.offline_access&' +
        'response_type=code&redirect_uri=' + CloudFuze.redirecturl + '&state=' + encodeURIComponent(state);
}
function getToken_SkyDrive(code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    var data = "code=" + code + "&grant_type=authorization_code&client_id=" + CloudFuze.clientId + "&" +
        "client_secret=" + CloudFuze.clientSecret + "&redirect_uri=" + CloudFuze.redirecturl;
    $.ajax({
        url: skyDrive_postURL,
        type: "POST",
        data: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (result) {
            getSkyDriveUserInfo(result, code, userId, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");

        }
    });
}
function getSkyDriveUserInfo(data, code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    $.ajax({
        url: skyDrive_userinfo + data.access_token,
        type: "GET",
        success: function (result) {
            apiPutData(userId, encodeURIComponent(data.access_token), encodeURIComponent(data.refresh_token), result.emails.preferred, "ONEDRIVE", result.name, code, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}*/

//SkyDrive new Oauth
function redirectSkyDrive(state) {
    window.location = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id='
        + CloudFuze.clientId +'&scope=files.readwrite offline_access&' +
        'response_type=code&redirect_uri=' + CloudFuze.redirecturl + '&state=' + encodeURIComponent(state);
}
function getToken_SkyDrive(code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    var data = "code=" + code + "&grant_type=authorization_code&client_id="+ CloudFuze.clientId +
        "&client_secret=" + CloudFuze.clientSecret + "&redirect_uri=" + CloudFuze.redirecturl;
    $.ajax({
        url: oneDrive_token_v2,
        type: "POST",
        data: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (result) {
            getSkyDriveUserInfo(result, code, userId, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");

        }
    });
}
function getSkyDriveUserInfo(data, code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    $.ajax({
        url: 'https://graph.microsoft.com/v1.0/me/drive/root',
        crossDomain:true,
        type: "GET",
        headers:{
            "Authorization":"Bearer "+data.access_token
        },
        success: function (result) {
            var email,_displayName;
            if(result.hasOwnProperty("createdBy")) {
                email = result["@odata.context"].split("('")[1].split("')")[0].replace("%40","@");
                _displayName = result.createdBy.user.displayName;
                if(_displayName == "")
                    _displayName = email;
            }
            else{
                email = result.id;
                _displayName = email;
            }
            apiPutData(userId, encodeURIComponent(data.access_token), encodeURIComponent(data.refresh_token),email, "ONEDRIVE", _displayName, code, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}

//ShareFile
function redirectShareFile(state) {
    window.location = 'https://secure.sharefile.com/oauth/authorize?response_type=code&' +
        'client_id=' + CloudFuze.clientId + '&redirect_uri=' + CloudFuze.redirecturl + '&state=' + encodeURIComponent(state);
}
function getToken_ShareFile(code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    $.ajax({
        url: sharefile_gettokenURL + CloudFuze.redirecturl + "&code=" + code + "&client_id=" + CloudFuze.clientId + "&client_secret=" + CloudFuze.clientSecret,
        type: "GET",
        success: function (result) {
            $('#authProcess').hide();
            $('#ShareFileDialog').show();
            $('#processStatemsg').hide();
            sharefiledata = result;
            sharefiledata.loginuserid = userId;
            sharefiledata.sharefilecode = code;
            sharefiledata.url_sl = url;
            //alert("clouduserId id=" + userId + " and code is " + code);
            //apiPutData(userId, result.access_token, result.refresh_token,result.subdomain, "SHAREFILE", result.subdomain, code);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}
function submitShareflieauthdtails() {
    $('#ShareFileDialog').hide();
    $('#processStatemsg').show();
    $('#authProcess').show();
    var userDisplayName = ($('#shareFileEmailId').val());
    if(userDisplayName == ""){
        $('#shareFileStatusMsg').text('Please Enter your ShareFile Email Address');
        return false;
    }
    if (userDisplayName.trim() != null && userDisplayName.trim().length > 0) {
        apiPutData(sharefiledata.loginuserid, sharefiledata.access_token, sharefiledata.refresh_token, sharefiledata.subdomain, "SHAREFILE", userDisplayName, sharefiledata.sharefilecode, sharefiledata.url_sl);
    }
    else {
        $('#shareFileStatusMsg').text('Please Enter your ShareFile Email Address');
    }
}

//Box
function redirectBox(state) {
    window.location = 'https://www.box.com/api/oauth2/authorize?response_type=code&' +
        'client_id=' + CloudFuze.clientId + '&redirect_uri=' + CloudFuze.redirecturl + '&state=' + encodeURIComponent(state);
}
function getToken_Box(code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    $('#ProcessMessage').append("<p id='CurrentProccessingState' class='CurrentProccessingState'>Processing</p>");
    var data = "code=" + code + "&grant_type=authorization_code&client_id=" + CloudFuze.clientId + "&" +
        "client_secret=" + CloudFuze.clientSecret;
    $.ajax({
        url: box_Post_URL,
        type: "POST",
        data: data,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (result) {
            getBoxUserInfo(result, code, userId, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}
function getBoxUserInfo(data, code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    $.ajax({
        url: box_userinfo,
        type: "GET",
        headers: {
            "Authorization": "Bearer " + data.access_token
        },
        success: function (result) {
            apiPutData(userId, data.access_token, data.refresh_token, result.login, "BOX", result.name, code, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}

//Orange Cloud
function redirectOrange(state) {
    window.location  = "https://api.orange.com/oauth/v2/authorize?response_type=code&scope="+encodeURIComponent('cloud cloudfullread offline_access openid profile')+"&" +
        "client_id="+CloudFuze.clientId+"&redirect_uri="+CloudFuze.redirecturl+"&state="+encodeURIComponent(state);
}
function getToken_Orange(code, userId, url) {
    localStorage.setItem("OauthProcess", "Inprogress");
    var data  = "redirect_uri="+CloudFuze.redirecturl + "&code=" + code + "&grant_type=authorization_code";
    $.ajax({
        url: orange_TokenURL,
        type: "POST",
        data:data,
        headers:{
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": make_base_auth(CloudFuze.clientId, CloudFuze.clientSecret),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (token) {
            var accessToken = token.access_token;
            var refreshToken = token.refresh_token;
            var user = getOrangeUserInfo(accessToken);
            var cloudUserId = "ORANGE|"+user.sub;
            var finalAccess = CloudFuze.clientId+":"+CloudFuze.clientSecret+":"+accessToken+":"+refreshToken;
            var urlFolder="https://api.orange.com/cloud/v1/folders?restrictedmode";
            $.ajax({
                url: urlFolder,
                type: "GET",
                headers: {
                    "Authorization": "Bearer " + accessToken,
                },
                success: function (token) {
                    apiPutData(userId, finalAccess, url, cloudUserId, "ORANGE", user.name, "", "");
                },
                error: function (jqXHR, exception, errorstr) {
                    closebox("failed");
                }
            });
        },
        error: function (jqXHR, exception, errorstr) {
            //closebox("failed");
            closebox("failed");
        }
    });
}
function getOrangeUserInfo(_b) {
    var a = '';
    $.ajax({
        url: orangeUserInfo,
        type: 'get',
        async: false,
        headers: {
            "Authorization": "Bearer " + _b
        },
        success: function (s) {
            return a = s;
        }
    });
    return a;
}

//SalesForce
function redirectSalesForce(state){
    window.location  = "https://login.salesforce.com/services/oauth2/authorize" +
        "?response_type=code&" +
        "client_id="+CloudFuze.clientId+"&" +
        "redirect_uri="+CloudFuze.redirecturl+"&" +
        "state="+encodeURIComponent(state);
}
function getToken_SalesForce(code,userId,url){
    localStorage.setItem("OauthProcess", "Inprogress");
    var data = "code="+code+"&grant_type=authorization_code&client_id="+CloudFuze.clientId+"&client_secret="+CloudFuze.clientSecret+"&redirect_uri="+CloudFuze.redirecturl;
    $.ajax({
        url: salesForce_TokenUrl,
        type: "POST",
        data:data,
        success: function (result) {
            var accessToken = CloudFuze.clientId+":"+CloudFuze.clientSecret+":"+result.access_token+":"+result.refresh_token;
            var _a = getUserInfo_SalesForce(result.id.split('/id/')[1],result.access_token);
            apiPutData(userId,accessToken , result.instance_url,_a, "SALES_FORCE", _a, code);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}
function getUserInfo_SalesForce(_a,_b){
    _d = "salesforce";
    $.ajax({
        url:salesForce_userInfo+_a,
        type:"get",
        async:false,
        headers: {
            "Authorization": "Bearer " + _b
        },
        success:function(_c){
            return _d = _c.email;
        }
    });
    return _d;
}

//One Drive Business
function redirectOneDriveBusiness(state){
    window.location = "https://login.microsoftonline.com/common/oauth2/authorize?" +
        "response_type=code&" +
        "client_id="+CloudFuze.clientId+"&" +
        "redirect_uri="+CloudFuze.redirecturl+"&" +
        "state="+encodeURIComponent(state)+"&prompt=login";
}
function getToken_OneDriveBusiness(code,userId,url){
    localStorage.setItem("OauthProcess", "Inprogress");
    $('#ProcessMessage').append("<p id='CurrentProccessingState' class='CurrentProccessingState'>Processing</p>");
    var data = "code=" + code + "&" +
        "client_id=" + CloudFuze.clientId + "&" +
        "client_secret=" + CloudFuze.clientSecret + "&" +
        "redirect_uri=" + CloudFuze.redirecturl + "&" +
        "grant_type=authorization_code" +
        "&resource=https://api.office.com/discovery/";
    $.ajax({
        url: OneDriveB_Token,
        type: "POST",
        data: data,
        async:false,
        success: function (data) {
            var resource = getResource_OneDriveBusiness(data.access_token);
            var renew = getRenewToken_OneDriveBusiness(data.refresh_token, resource.value[0].serviceResourceId);
            var cloudaccesstoken = CloudFuze.clientId + ":" + CloudFuze.clientSecret + ":" + renew.access_token + ":" + renew.refresh_token;
            var cloudrefreshtoken = resource.value[0].serviceResourceId;
            var cloudUserId = "";
               /* _modal = $('#onedriveEmail');
            _modal.modal('show');
            _modal.on('click', '#onedriveEmailOk', function () {
                email = _modal.find('input').val().trim();
                if (email.length < 1) {
                    _modal.find('.statusMesg').text('Please enter valid email id.');
                    return false;
                }
                else if (!emailReg.test(email)) {
                    _modal.find('.statusMesg').text('Please enter valid email id.');
                    return false;
                }
                else {
                    _modal.modal('hide');
                    cloudUserId = "ONEDRIVE_BUSINESS|" + email;
                    apiPutData(userId, cloudaccesstoken, cloudrefreshtoken, cloudUserId, "ONEDRIVE_BUSINESS", email, redirectURL, url)
                }
            });*/
            var email =data.id_token;
            var arr = email.split('.');
            var emailId=Base64.decode(arr[1]);
            emailId = emailId.split(',');
            $.each(emailId,function(i,e){
                if(/unique/.test(e)) {
                    email = e.split(':').pop();
                    email = email.split('"')[1];
                }
                })
                cloudUserId = "ONEDRIVE_BUSINESS|" + email;
            apiPutData(userId, cloudaccesstoken, cloudrefreshtoken, cloudUserId, "ONEDRIVE_BUSINESS", email, redirectURL, url)

        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}
function getResource_OneDriveBusiness(data,code,userId,url){
    var r= "";
    $.ajax({
        url: OneDriveB_Service,
        type: "GET",
        async:false,
        headers:{
            "Authorization":"Bearer "+data
        },
        success: function (result) {
            r = result;
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
    return r;
}
function getRenewToken_OneDriveBusiness(a,b){
    var _a = "";
    var data = "client_id=" + CloudFuze.clientId + "&" +
        "client_secret=" + CloudFuze.clientSecret + "&" +
        "redirect_uri=" + redirectURL + "&" +
        "grant_type=refresh_token&resource="+b+"&refresh_token="+a;
    $.ajax({
        url: OneDriveB_Token,
        type: "POST",
        data: data,
        async:false,
        success: function (data) {
            _a = data;
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
    return _a;
}

//Amazon Storage
function redirectAmazonStorage(state){
    window.location = "https://www.amazon.com/ap/oa" +
        "?client_id="+CloudFuze.clientId+
        "&response_type=code" +
        "&redirect_uri="+CloudFuze.redirecturl+
        "&scope=clouddrive%3Aread_all%20clouddrive%3Awrite%20profile"+
        "&state="+encodeURIComponent(state);
}
function getToken_amazon_token(a,b,c){
    var d = "grant_type=authorization_code&" +
        "code="+a+
        "&client_id="+CloudFuze.clientId+
        "&client_secret="+CloudFuze.clientSecret+"" +
        "&redirect_uri="+CloudFuze.redirecturl;
    $.ajax({
        url: amazon_storage_token,
        type: "POST",
        data: d,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (result) {
            getAmazonStorageUser(result, a, b, c);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed");
        }
    });
}
function getAmazonStorageUser(r,a,b,c){
    $.ajax({
        url:amazon_storage_user,
        type:"GET",
        headers:{
            "Authorization": "Bearer " + r.access_token
        },
        success:function(data){
            apiPutData(b, r.access_token, r.refresh_token,data.email, "AMAZON_STORAGE", data.name, a, c);
        },
        error:function(){
            closebox("failed");
        }
    });
}

//CISCO Spark
function redirectSparkIntgration(state) {
    window.location = "https://api.ciscospark.com/v1/authorize?" +
        "client_id=" + CloudFuze.clientId + "" +
        "&redirect_uri=" + encodeURIComponent(CloudFuze.redirecturl) + "" +
        "&response_type=code" +
        "&scope=spark%3Apeople_read+spark%3Amessages_write+spark%3Arooms_read+spark%3Amemberships_read+spark%3Amessages_read+spark%3Arooms_write+spark%3Amemberships_write" +
        "&state=" + encodeURIComponent(state);
}
function getToken_Spark(a,b,c,cname){
    var d = "grant_type=authorization_code&" +
        "code="+a+
        "&client_id="+CloudFuze.clientId+
        "&client_secret="+CloudFuze.clientSecret+"" +
        "&redirect_uri="+encodeURIComponent(CloudFuze.redirecturl);
    $.ajax({
        url: spark_token,
        type: "POST",
        data: d,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (result) {
            getProfile_spark(result,a,b,c,cname);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed",cname);
        }
    });
}
function getProfile_spark(data,code, userId, url,cname){
    //spark_profile
    $.ajax({
        url: spark_profile,
        type: "GET",
        headers:{
            "Content-type":"application/json",
            "Authorization":"Bearer "+data.access_token
        },
        success: function (profile) {
            apiPutData(userId, data.access_token, data.refresh_token, profile.emails[0], "CISCO_SPARK", profile.displayName, code, url);
        },
        error: function (jqXHR, exception, errorstr) {
            closebox("failed",cname);
        }
    });
}

//SharePoint Online
// function getReleamSharPointOnline(state,domain){
//     var _modal = $('#onedriveDomain');
//     localStorage.setItem('state',state);
//     _modal.find('.tab-header').find('span').text('SharePoint Online Registration');
//     _modal.find('.modal-body>p').text('Enter SharePoint Online Domain');
//     var _input = _modal.find('.modal-body').find('input');
//     _input.attr('placeholder','Enter Domain');
//     _modal.find('.modal-footer').find('.button').attr('id','sharepoint-domain');
//     _modal.on('click','#sharepoint-domain',function(){
//         var _val = _input.val().trim();
//         if(_val.length ==0){
//             _modal.find('.statusMessage').text('All Fields are required.');
//             return false;
//         }
//         else{
//             _modal.find('.statusMessage').text('');
//             var state = localStorage.getItem('state');
//             /*var keys = getOauthKeys("SHAREPOINT_ONLINE",_val.split('-')[0]);
//             if(keys == undefined || keys == null){
//                 return alert('Client keys not registered with the current domain!!!');
//             }
//             else{
//                 window.CloudFuze = {};
//                 CloudFuze.clientId = keys.clientId;
//                 CloudFuze.clientSecret = keys.clientSecret;
//                 CloudFuze.redirecturl = keys.redirecturl;
//                 CloudFuze.domain = keys.domainName;
//             }*/
//
//             $.ajax({
//                 url:apicallurl+'/auth/sharepointonline/realm?domain='+_val,
//                 type:'GET',
//                 async:false,
//                 headers: {
//                     "Content-Type": "application/json",
//                     "Authorization": localStorage.getItem('UserAuthDetails'),
//                     "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
//                 },
//                 success:function(s){
//                     s.domain = _val;
//                     localStorage.setItem('releam',JSON.stringify(s));
//                     redirectSharePointOnline(state,_val.split('-')[0]);
//                 },
//                 complete:function(xhr){
//                     if(xhr.status > 299){
//                         alert('Invalid SharePoint Domain');
//                     }
//                 }
//             });
//         }
//     });
//
//     _modal.modal('show');
// }
// function redirectSharePointOnline(state,domain){
//     window.location = 'https://'+domain+'.sharepoint.com/_layouts/15/OAuthAuthorize.aspx?' +
//         'client_id='+CloudFuze.clientId+
//         '&scope=AllSites.Write' +
//         '&response_type=code' +
//         '&redirect_uri='+encodeURIComponent(CloudFuze.redirecturl)+
//         '&state='+encodeURIComponent(state);
// }
// function getToken_SharePointOnline(code,userId,url) {
//     var _releam = JSON.parse(localStorage.getItem('releam')),
//         _b = '----WebKitFormBoundary' + Math.floor(Math.random() * 32768) + Math.floor(Math.random() * 32768) + Math.floor(Math.random() * 32768),
//         _domain = _releam.domain.split('-')[0];
//
//     function createFormParam(b,n,v){
//         var _a = "";
//         _a += '--' + b + '\r\n' + 'Content-Disposition: form-data; name="';
//         _a += n;
//         _a += '"\r\n\r\n';
//         _a += v;
//         _a += '\r\n';
//         return _a;
//     }
//
//     /*var keys = getOauthKeys("SHAREPOINT_ONLINE",_domain );
//     if(keys == undefined || keys == null){
//         return alert('Client keys not registered with the current domain!!!');
//     }
//     else{
//         window.CloudFuze = {};
//         CloudFuze.clientId = keys.clientId;
//         CloudFuze.clientSecret = keys.clientSecret;
//         CloudFuze.redirecturl = keys.redirecturl;
//         CloudFuze.domain = keys.domainName;
//     }*/
//
//     var _data = createFormParam(_b,'grant_type','authorization_code');
//     _data += createFormParam(_b,'redirect_uri',CloudFuze.redirecturl);
//     _data += createFormParam(_b,'client_secret',encodeURIComponent(CloudFuze.clientSecret));
//     _data += createFormParam(_b,'client_id',CloudFuze.clientId+'@'+_releam.realm);
//     _data += createFormParam(_b,'code',code);
//     _data += createFormParam(_b,'resource',_releam.client_id + '/' + _domain  + '.sharepoint.com@' + _releam.realm );
//     _data += createFormParam(_b,'releam',_releam.realm);
//     _data += '--' + _b;
//
//     $.ajax({
//         url:apicallurl+'/auth/sharepointonline/accesstoken',
//         type:'POST',
//         data: _data,
//         headers: {
//             "Content-Type": "multipart/form-data;boundary="+_b
//         },
//         success: function (data) {
//             if(data.hasOwnProperty('error')){
//                 closebox('failed','SHAREPOINT_ONLINE');
//             }
//
//             var cloudaccesstoken = _releam.realm+':'+data.refresh_token+':'+data.access_token,
//                 cloudrefreshtoken ='https://'+_releam.domain.split('-')[0]+'.sharepoint.com/',
//                 cloudUserId = '',
//                 authorizationCode = _releam.realm,
//                 _modal = $('#onedriveEmail');
//
//             _modal.find('.tab-header>span').text('SharePoint Online Registration');
//             _modal.find('.modal-body>p').text('Enter SharePoint Online E-Mail - Address');
//             _modal.find('.button').attr('id','shponemail');
//             _modal.find('input').attr('placeholder','Enter User Id');
//             _modal.show();
//
//             _modal.on('click','#shponemail', function () {
//                 var email = _modal.find('input').val().trim();
//                 if (email.length < 1) {
//                     _modal.find('.statusMesg').text('Please enter valid email id.');
//                     return false;
//                 }
//                 else if (!emailReg.test(email)) {
//                     _modal.find('.statusMesg').text('Please enter valid email id.');
//                     return false;
//                 }
//                 else {
//                     _modal.modal('hide');
//                     cloudUserId = "SHAREPOINT_ONLINE|" + email;
//                     apiPutData(userId, cloudaccesstoken, cloudrefreshtoken, cloudUserId, 'SHAREPOINT_ONLINE', email, authorizationCode, url);
//                 }
//             });
//         }
//     });
// }
//
function getOauthKeys(a,d){
    var b = '',
        _u = apicallurl+'/auth/oauthkeys?cloudName='+a;
    if(d!= undefined){
        _u +='&domainName='+d;
    }
    $.ajax({
        type:'GET',
        url:_u,
        async:false,
        headers:{
            "Content-Type": "application/json",
            "Authorization":'',
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success:function(data) {
            return b = data[0];
        }
    });
    return b;
}



//Sharepoint-Online

function redirectSharePointOnline(state){
    window.location = 'https://login.microsoftonline.com/common/oauth2/authorize?response_type=code&response_mode=query&' +
        'client_id='+CloudFuze.clientId+
        //'client_id=b0d1e3f7ab39471aae8d12d6735ac07d'+
        '&scope=Sites.ReadWrite.All' +
        '&redirect_uri='+encodeURIComponent(CloudFuze.redirecturl)+
        '&prompt=login&state='+encodeURIComponent(state);
}
function getToken_SharePointOnline(code,userId,url) {
    var data='grant_type=authorization_code'+
        '&redirect_uri='+encodeURIComponent(CloudFuze.redirecturl)+
        //'&client_id=b0d1e3f7ab39471aae8d12d6735ac07d'+
        '&client_id='+CloudFuze.clientId+
        '&client_secret='+encodeURIComponent(CloudFuze.clientSecret)+
         //'&client_secret='+encodeURIComponent('LQX6xAjBmAeOHWZz7fwIZMK/BTlI+7Dv89dVnDhEvtM=')+
        '&resource=https://api.office.com/discovery/'+
            '&code='+code,_code=code;
    $.ajax({
        url:SharePoint_Online_token,
        type:'POST',
        data: data,
        headers: {
            "Content-Type": 'application/x-www-form-urlencoded'
        },
        success: function (data) {
            var _resource=getSharepointDomain(data.access_token);
            var _tokens=getSharepointTokens(data.refresh_token,_resource.value[2].serviceResourceId,_code);
            var _domain = _resource.value[2].serviceResourceId;
            _domain=_domain.split(".")[0]+"-spo.";
            //var url=_domain+SharePoint_Online_email.split("https://")[1];
            var mail;
            $.ajax({
                //url:url+'sp.userprofiles.peoplemanager/GetMyProperties',
                url:_resource.value[2].serviceResourceId+"_api/sp.userprofiles.peoplemanager/GetMyProperties",
                type:'POST',
                crossDomain: true,
                headers: {
                    "Authorization": "Bearer " + _tokens.access_token,
                    "Accept": "application/json"
                },
                success: function (data) {
                    mail=data;
                    //_tokens.access_token,_tokens.access_token.refresh_token,mail.Email,_resource.value[2].serviceResourceId
                    var cloudUserId = "SHAREPOINT_ONLINE|" + mail.Email;
                    apiPutData(userId, _tokens.access_token, _tokens.refresh_token,cloudUserId , 'SHAREPOINT_ONLINE',mail.Email, _resource.value[2].serviceResourceId, url);
                   // apiPutData(userId, cloudaccesstoken, cloudrefreshtoken, cloudUserId, 'SHAREPOINT_ONLINE', email, authorizationCode, url);
                },
                error: function (jqXHR, exception, errorstr) {
                    closebox("failed");
                }
            });
        }
    });
}
function getSharepointDomain(accessToken) {
    var resource;
    $.ajax({
        url:SharePoint_Online_services,
        type:'GET',
        async:false,
        headers: {
            "Authorization": "Bearer " + accessToken
        },
        success: function (data) {
            resource=data;
        }
    });
    return resource;
}
function getSharepointTokens(refreshToken,resource,code) {
    var tokens, data='grant_type=refresh_token'+
        '&redirect_uri='+encodeURIComponent(CloudFuze.redirecturl)+
        //'&client_id=b0d1e3f7ab39471aae8d12d6735ac07d'+
        '&client_id='+CloudFuze.clientId+
        '&client_secret='+encodeURIComponent(CloudFuze.clientSecret)+
         //'&client_secret='+encodeURIComponent('LQX6xAjBmAeOHWZz7fwIZMK/BTlI+7Dv89dVnDhEvtM=')+
        '&resource='+resource+
        '&refresh_token='+refreshToken+
        '&code='+code;
    $.ajax({
        url:SharePoint_Online_token,
        type:'POST',
        async:false,
        data: data,
        headers: {
            "Content-Type": 'application/x-www-form-urlencoded'
        },
        success: function (data) {
            tokens=data;
        }
    });
    return tokens;
}