/**
 * Created by CloudFuze WebApp on 10/22/2014.
 */
var CLName = {
    "G_DRIVE": "Google Drive",
    "BOX": "Box",
    "DROP_BOX": "Dropbox",
    "CMIS": "CMIS",//ECM
    "WEBDAV": "WebDav",//WEBDAV
    "SHAREFILE": "Citrix ShareFile",
    "ALFRESCO":"Alfresco",//ECM
    "AMAZON":"Amazon S3",
    "FTP":"FTP",
    "WALRUS":"Eucalyptus",//ECM
    "ONEDRIVE":"OneDrive",
    "EGNYTE_STORAGE":"Egnyte",
    "SUGAR_SYNC":"SugarSync",
    "SHAREPOINT_2013":"SharePoint 2013",//ECM
    "SHAREPOINT_2010":"SharePoint 2010",//ECM
    "DOCUMENTUM":"Documentum",//ECM
    "CLOUDIAN":"Cloudian",//ECM
    "AZURE_OBJECT_STORAGE":"Azure",//ECM
    "GENERIC_OBJECT_STORAGE":"Object Storage",//ECM
    "NTLM_STORAGE":"NFS Storage",//ECM,
    "CENTURYLINK" : "CenturyLink",
    "DROPBOX_BUSINESS": "Dropbox Business",
};
var fileIds = [];
var sharedFolderId = '';
var PageName = "Public Share";
var parentId ="" , fileId = "",cloudId = "";
var saveingurlforpasswordprotectedfiles="";
$(document).ready(function() {
    
    sessionStorage.removeItem('sharePassword');
    var userid = localStorage.getItem("UserId");
    if (userid != null) {
        $("#login-greet").show();
        $("#login-uname").show();
        $("#login-uname").text(localStorage.getItem('UserName'));
        $("#Logout").text("Logout");
    }
    else {
        $("#login-greet").hide();
        $("#login-uname").hide();
        $('#Logout').attr('href', domainUrl + "/fuzebot/");
    }
    var urlParams;
    (window.onpopstate = function () {
        var match,
            pl = /\+/g,
            search = /([^&=]+)=?([^&]*)/g,
            decode = function (s) {
                return decodeURIComponent(s.replace(pl, " "));
            },
            query = window.location.search.substring(1);
        urlParams = {};
        while (match = search.exec(query))
            urlParams[decode(match[1])] = decode(match[2]);
    })();
    var fileId = urlParams["fileId"];
    var fileObj= publicNew.getPublicShare(fileId);
    if (fileObj.objectName != undefined) {
        sharedFolderId = fileId;
        $('#fileInfo').attr('shareID',fileId);
        $('#fileInfo').attr('token',urlParams["accessToken"]);
        publicNew.appendFiles(fileObj);
        $('#fileInfo').find('.fileCheck').css('visibility','hidden');
    }
    else {
        $('#fileInfo').append('<li><span>File might have been removed from the cloud or your download count may have exceeded. Please contact the owner.</span></li>');
        $('#fileDownload').parent().remove();
        return false;
    }
    var auth = makeBasicAuth();
    if(fileObj.status != "PUBLIC"){
        sendGAEvents("Private Share");
        //_gaq.push(['_trackEvent', "Private Share", localStorage.getItem('UserId'),PageName]);
        $('#fileInfo').find('.directorytrue').css('pointer-events','none');
        $('#fileInfo').find('.directorytrue').css('cursor','default');
        $('#fileDownload').hide();
        var url = domainUrl + "pages/fileManager.html?shared=true&fileId=" +fileId;
        if(auth == null){
            $('#private-login,#private-signup').show();
        }
        else{
            $('#private-login').text('Show me in CloudFuze');
            $('#private-login').attr('href',url);
            $('#private-login').show();
        }
        return false;
    }
    else{
        sendGAEvents("Public Share");
        //_gaq.push(['_trackEvent', "Public Share", localStorage.getItem('UserId'),PageName]);
        if(fileObj.directory == false){
            $('#fileDownload').removeClass('buttonDisable').removeAttr('style');
        }
    }
    var apicallurl1 = '';
    if (auth == null || auth == undefined || auth == "") {
        apicallurl1 = apicallurl + '/fileshare/content/revision/latest?' +
        'fileId=' + encodeURIComponent(fileId) +
        '&accessToken=' + urlParams["accessToken"] +
        '&sharePassword=';
    }
    else {
        apicallurl1 = apicallurl + '/fileshare/content/revision/latest?' +
        'fileId=' + encodeURIComponent(fileId) +
        '&accessToken=' + urlParams["accessToken"] +
        '&token=' + auth +
        '&sharePassword=';
    }
    $('#fileDownload').attr("value", apicallurl1);
    $.ajax({
        type: "GET",
        url: apicallurl1 + '&verifyAuth=true',
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
        },
        complete: function (xhr, statusText) {
            if (xhr.status == 401 || xhr.status == 403) {
                var exception = xhr.getResponseHeader("exception");
                if (exception.indexOf("Password Protected") == -1) {
                    var fileId = urlParams["fileId"];
                    var url = domainUrl + "pages/fileManager.html?shared=true&fileId=" + encodeURIComponent(fileId);
                    sessionStorage.setItem("url", url);
                    $('.tab-header').find('#shareTitle').text('Private Share');
                }
            }
        }
    });
});
function fileDownload(url) {
    $.ajax({
        type: "GET",
        url: url + '&verifyAuth=true',
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
        },
        complete: function (xhr, ajaxOptions, thrownError) {
            saveingurlforpasswordprotectedfiles=url;
            if (xhr.status == 200) {
                downloadURL(url);
            }
            if (xhr.status == 401 || xhr.status == 403) {
                var exception = xhr.getResponseHeader("exception");
                if (exception.indexOf("Password Protected") != -1) {
                    var url1 = url;
                    setTimeout(function () {
                        var url2 = updateURLParameter(url1, "sharePassword", "");
                        $('#CFPwdButton').attr("value", url2);
                    }, 500);
                    setTimeout(function () {
                        $('#myPasswordModel').modal("show");
                        $('#sharePswd').val('');
                    }, 700);
                }
            }
            if (xhr.status == 404) {
                $('#fileInfo').html('');
                $('#fileInfo').append('<tr><td colspan="2">File might have been removed from the cloud or your download count may have exceeded. Please contact the owner.</td></tr>');
                $('#fileDownload').parent().remove();
            }
        }
    });
}
function checkAuth(url){
    $.ajax({
        type: "GET",
        url: url + '&verifyAuth=true',
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
        },
        complete: function (xhr, ajaxOptions, thrownError) {
            if (xhr.status == 200) {
                $('#Msg span').text('');
                $('#myPasswordModel').modal('hide');
                downloadURL(url);
            }
            if (xhr.status == 401 || xhr.status == 403) {
                $('#myPasswordModel').find('.statusMesg').text('Password does not match.');
            }
            if (xhr.status == 404) {
                $('#fileInfo').html('');
                $('#fileInfo').append('<tr><td colspan="2">File might have been removed from the cloud or your download count may have exceeded. Please contact the owner.</td></tr>');
                $('#fileDownload').parent().remove();
            }
        }
    });
}

$('#fileDownload').click(function(){
    $('.statusMesg').text('');
    sendGAEvents("Download");
    //_gaq.push(['_trackEvent', "Download", localStorage.getItem('UserId'),PageName]);
    var child = $('#fileInfo').children('li').length;
    if(child == 1){
        var url = $(this).attr('value');
        fileDownload(url);
    }
    var cid = $('#fileInfo > li').attr('cid');
    var shareId = $(this).closest('.span7').find('#fileInfo').attr('shareID');
    var fileId = [];
    var token = $(this).closest('.span7').find('#fileInfo').attr('token');
    $('input:checked').each(function (){
        var id = $(this).closest('li').attr('id');
         fileId.push(id);
    });
    $.each(fileId,function(i,fid){
        var durl = '';
        /*if(localStorage.getItem('UserAuthDetails')) {
            durl = apicallurl + "/filefolder/content?fileId=" + encodeURIComponent(fid) + "&sharedFolderId=" + encodeURIComponent(shareId)+ "&token=" + localStorage.getItem('UserAuthDetails');
        }
        else {*/
            durl = apicallurl + "/fileshare/content/revision/latest?fileId=" + encodeURIComponent(fid) + "&sharedFolderId=" + encodeURIComponent(shareId) + "&accessToken=" + token;
        /*}*/
        fileDownload(durl);
    });
});

$('#CFPwdButton').click(function() {
    sendGAEvents("File Password");
    //_gaq.push(['_trackEvent', "File Password", localStorage.getItem('UserId'),PageName]);
    var pwd=$('#sharePswd').val();
    if(pwd == ""){
        $('#sharePswd').focus();
        $('#Msg').find('.statusMesg').text('Please Enter Password');
        return false;
    }
    var url=saveingurlforpasswordprotectedfiles;
    checkAuth(saveingurlforpasswordprotectedfiles+pwd);
});

$('#protectedFolder').on('keydown','input',function(e){
    if(e.which == 13){
        $('#CFFPwdButton').trigger('click');
    }
});

$('#myPasswordModel').on('keydown','input',function(e){
    if(e.which == 13){
        $('#CFPwdButton').trigger('click');
    }
});

$('#Logout').on('click',function(e){
    sendGAEvents("Logout");
    //_gaq.push(['_trackEvent', "Logout", localStorage.getItem('UserId'),PageName]);
    e.preventDefault();
    localStorage.removeItem('CFUser');
    localStorage.removeItem('UserAuthDetails');
    localStorage.removeItem('UserId');
    localStorage.removeItem('UserName');
    window.location.href="/fuzebot/";
});
downCount = 0;
function downloadURL(url) {
    var hiddenIFrameID = 'hiddenDownloader' + downCount;
    $(document).find('input').prop('checked',false);
    downCount = downCount + 1;
    iframe = document.getElementById(hiddenIFrameID);
    if (iframe === null) {
        iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
    }
    iframe.src = url;
}

function updateURLParameter(url, param, paramVal){
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }
    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}

function getObjectSize (objectSize ,filetype) {
    var size = objectSize;
    if(filetype == "FOLDER"){
        objectSize = "___";
        return objectSize;
    }
    else if(objectSize == 0){
        objectSize = "___";
        return objectSize;
    }
    else{
        if (size > 1024) {
            var ksize = size / 1024;
            if (ksize > 1024) {
                var mbsize = ksize / 1024;
                if (mbsize > 1024) {
                    var gbsize = mbsize / 1024;
                    objectSize = parseInt(gbsize) + " GB";
                } else {
                    objectSize = parseInt(mbsize) + " MB";
                }
            } else {
                objectSize = parseInt(ksize) + " KB";
            }
        } else {
            objectSize = parseInt(size) + " Bytes";
        }
    }
    return objectSize;
}

function makeBasicAuth(){
    return localStorage.getItem("UserAuthDetails");
}

$('#fileInfo').on('click','.directorytrue .fileName',function(){
    sendGAEvents("Navigate in to Folders");
    //_gaq.push(['_trackEvent', "Navigate in to Folders", localStorage.getItem('UserId'),PageName]);
    var object = {};
    object.folderId = $(this).closest('.directorytrue').attr('id');
    fileId = $(this).closest('.directorytrue').attr('id');
    parentId = $(this).closest('.directorytrue').attr('pid');
    object.sharedFolderId = $(this).closest('#fileInfo').attr('shareID');
    object.cloudId = $(this).closest('.directorytrue').attr('cid');
    cloudId = $(this).closest('.directorytrue').attr('cid');
    var pwd = sessionStorage.getItem("sharePassword");
    if(pwd == null){
        object.password = "NO";
    }
    else{
        object.password = pwd;
    }
    var fileObject = publicNew.navigateFolders(object);
    if(fileObject == "unAuthorized"){
        object.page = 1;
    }
    publicNew.handleNavigation(fileObject,object);
});

$('#protectedFolder').on('click','#CFFPwdButton',function(){
    var object = JSON.parse(atob($(this).attr('data')));
    var pass = $('#protectedFolder').find('input').val();
    if(pass == ""){
        $('#protectedFolder').find('.statusMesg').text('Please Enter Password.');
        return false;
    }
    else{
        object.password = pass;
        var fileObject = publicNew.navigateFolders(object);
        publicNew.handleNavigation(fileObject,object);
    }
});

$('#fileInfo').on('change','input',function(){
    if($('input:checked').length > 0){
        sendGAEvents("File Selection");
        //_gaq.push(['_trackEvent', "File Selection", localStorage.getItem('UserId'),PageName]);
        fileIds.push($(this).closest('li').attr('id'));
        $('#fileDownload').removeClass('buttonDisable');
    }
    else{
        fileIds = [];
        $('#fileDownload').addClass('buttonDisable');
    }
});

$('#fileInfo').on('click','.cf-back',function(){
    sendGAEvents("Folder Back Navigation");
    //_gaq.push(['_trackEvent', "Folder Back Navigation", localStorage.getItem('UserId'),PageName]);
    parentId = $(this).closest('li').attr('pid');
    fileId = $(this).closest('li').attr('id');
    if(fileId == "root"){
        window.location.reload();
        return false;
    }
    sharedFolderId = $('#fileInfo').attr('shareid');
    var object = {};
    var pwd = sessionStorage.getItem("sharePassword");
    if(pwd == null){
        object.password = "NO";
    }
    else{
        object.password = pwd;
    }
    if(parentId == fileId){
        window.location.reload();
    }
    else{
        fileId = parentId;
        object.folderId = parentId;
        object.sharedFolderId = sharedFolderId;
        object.cloudId = cloudId;
        var object = publicNew.navigateFolders(object);
        publicNew.handleNavigation(object,false);
    }
});

var publicNew = {
    navigateFolders : function(object){
        var apiUrl = apicallurl+"/filefolder/user/"+localStorage.getItem('UserId')+"/childfolders/"+object.cloudId+"?folderId="+encodeURIComponent(object.folderId)+"&sharedFolderId="+encodeURIComponent(object.sharedFolderId);
        if(object.password == "NO"){
            apiUrl += "&sharePassword=";
        }
        else{
            apiUrl += "&sharePassword="+object.password;
        }
        var fileObject = '';
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS",
                "Authorization": localStorage.getItem('UserAuthDetails')
            },
            success: function (data) {
                sessionStorage.setItem("sharePassword",object.password);
                $('#protectedFolder').modal('hide');
                fileObject = data;
            },
            complete: function (xhr, statusText) {
                if(xhr.status == 401){
                    fileObject = "unAuthorized";
                }
                else if(xhr.status > 300){
                    fileObject = "error";
                }
            }
        });
        return fileObject;
    },

    handleNavigation:function(fileObject,object){
        if(fileObject == "unAuthorized"){
            if(object.page == 1){
                $('#protectedFolder').find('.statusMesg').text('');
                delete object.page;
            }
            else{
                $('#protectedFolder').find('.statusMesg').text('Password does not match.');
            }
            $('#protectedFolder').find('#CFFPwdButton').attr('data',btoa(JSON.stringify(object)));
            $('#protectedFolder').modal('show');
            setTimeout(function () {
                $('#protectedFolder').find('input').focus();
            },400);
        }
        else if(fileObject == "error"){

        }
        else{
            if(fileObject != undefined || fileObject != null || fileObject != ''){
                $('#fileInfo').html('');
                var htmlc = '<li class="" id="'+fileId+'" pid="'+parentId+'">' +
                    '<span class="fileIcon"></span>' +
                    '<span class="fileCheck buttonDisable"></span>' +
                    '<span class=""><i class="cf-back" style="font-size: 24px;cursor: pointer"></i></span>' +
                    '<span class="fileSize"></span>' +
                    '<span class="fileCloud"></span>' +
                    '<span class="fileDate"></span></li>';
                $('#fileInfo').append(htmlc);
                publicNew.appendFiles(fileObject,true);
                $('#fileDownload').show();
            }
        }
    },

    appendFiles:function(fileObject,direction){
        $('#fileInfo').find('.cf-back').closest('li').nextAll().remove();
        if(fileObject != null || fileObject != undefined) {
            if (typeof fileObject == "object" && fileObject.length == undefined) {
                if (direction == false) {
                    $('#fileInfo').find('.cf-back').attr('pid', fileObject.parent);
                    if (fileId == fileObject.id) {
                        $('#fileInfo').find('.cf-back').attr('id', fileObject.parent);
                    }
                }
                $('#fileInfo').append('<li class="directory' + fileObject.directory + '" cid="' + fileObject.cloudId + '" id="' + fileObject.id + '" pid="' + fileObject.parent + '">'
                + '<span class="fileCheck ' + publicNew.getCheckStatus(fileObject.directory) + '"><input type="checkbox"></span>'
                + '<span class="fileIcon"><i class="' + publicNew.getFileIcon(fileObject.directory) + '"></i></span>'
                + '<span class="fileName">' + fileObject.objectName + '</span>'
                + '<span class="fileSize">' + getObjectSize(fileObject.objectSize) + '</span>'
                + '<span class="fileCloud">' + publicNew.getCloudName(fileObject.cloudName) + '</span>'
                + '<span class="fileDate">' + publicNew.DateConvertion(fileObject.modifiedTime) + '</span>'
                + '</li>');
            }
            else {
                if(fileObject.length == 0){
                    if(fileId == sharedFolderId){
                        $('#fileInfo').find('.cf-back').attr('id','root');
                    }
                }
                $.each(fileObject, function (i, file) {
                    if (direction == false && i == 0) {
                        $('#fileInfo').find('.cf-back').attr('pid', file.parent);
                    }
                    if (direction == false && file.parentFileRef == null) {
                        $('#fileInfo').find('.cf-back').closest('li').attr('id', "root");
                    }
                    else if (direction == true && file.parentFileRef == null && fileId == sharedFolderId) {
                        $('#fileInfo').find('.cf-back').closest('li').attr('id', "root");
                    }
                    $('#fileInfo').append('<li class="directory' + file.directory + '" cid="' + file.cloudId + '" id="' + file.id + '" pid="' + file.parent + '">'
                    + '<span class="fileCheck ' + publicNew.getCheckStatus(file.directory) + '"><input type="checkbox"></span>'
                    + '<span class="fileIcon"><i class="' + publicNew.getFileIcon(file.directory) + '"></i></span>'
                    + '<span class="fileName">' + file.objectName + '</span>'
                    + '<span class="fileSize">' + getObjectSize(file.objectSize) + '</span>'
                    + '<span class="fileCloud">' + publicNew.getCloudName(file.cloudName) + '</span>'
                    + '<span class="fileDate">' + publicNew.DateConvertion(file.modifiedTime) + '</span>'
                    + '</li>');
                });
            }
        }

    },

    DateConvertion : function (dateToConvert) {
        if(dateToConvert == undefined || dateToConvert == null || dateToConvert == ""){
            dateToConvert = "----";
            return dateToConvert;
        }
        var date = new Date(dateToConvert);
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        if (month < 10) {
            month = "0" + month;
        }
        var marr = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        month = month - 1;
        dateToConvert = marr[month] + " " + day + ", " + year;
        if (date == "Invalid Date") {
            dateToConvert = "___";
        }
        return dateToConvert;
    },

    getCloudName:function(cloudName){
        if(cloudName == undefined || cloudName == null || cloudName == ""){
            cloudName = "----";
            return cloudName;
        }
        $.each(CLName,function(key,value){
            if(key==cloudName){
                cloudName = value;
                return cloudName;
            }
        });
        return cloudName;
    },

    getFileIcon : function(fileIcon){
        if(fileIcon == true){
            fileIcon = "cf-folder-open";
            return fileIcon;
        }
        else{
            fileIcon = "cf-file6";
            return fileIcon;
        }
    },

    getCheckStatus : function(iconClass){
        if(iconClass == true){
            iconClass = "checkHide"
        }
        else{
            iconClass = "none";
        }
        return iconClass;
    },

    getFileDetails: function (fileid) {
        var accesstoken = $('#fileInfo').attr('token');
        var apiUrl = apicallurl+"/filefolder/info?fileId=" + encodeURIComponent(fileid) + "&sharedFolderId="+sharedFolderId+"&fetchCollabInfo=true&accessToken="+accesstoken;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (fileDetailsData) {
                fileDetails = fileDetailsData;
            },
            complete:function(xhr, statusText){
                if(xhr.status > 300 && xhr.status != 404){
                   //$.smallBox({title:"Operation Failed",color:"#e35e00",timeout:notifyError,sound:false});
                    showNotyNotification("error","Operation Failed");
                    //toastr.error("Operation Failed");
                }
            }
        });
        return fileDetails;
    },

    getPublicShare : function(fid){
        var fileShareData;
        var apiUrl = apicallurl + "/fileshare/openfileshare?" +
            "fileId=" + encodeURIComponent(fid) +
            "&sharedFolderId=" + sharedFolderId;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": makeBasicAuth(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (fileShareDetails) {
                fileShareData = fileShareDetails;
            },
            statusCode: {
                404: function () {
                    fileShareData = 0;
                }
            },
            error:function(){
                fileShareData = 0;
            }
        });
        return fileShareData;
    },

    checkAuthForPrivate : function(){
        var auth = makeBasicAuth();
        $('#fileDownload').hide();
        if(auth == null){
            $('#private-login,#private-signup').show();
        }
        else{
            $('#private-login,#private-signup').hide();
            $('#private-login').text('Show me in CloudFuze');
            $('#private-login').href('/pages/fileManager.html?shared=true&fileId='+fileId);
            $('#private-login').show();
        }
    },

    redirectAndroid:function(){
        //https://play.google.com/store/apps/details?id=com.cloudfuze.main
        $('body').append('<iframe id="deepLink" style="display:none"></iframe>');
        //Test Deep Link
        $('#deepLink').attr('src','birdland://');
        //If DeepLink Fails
        setTimeout(function() {
            window.location = "https://play.google.com/store/apps/details?id=com.cloudfuze.main";
        }, 500);
    },

    redirectios:function(){
        /*$('body').append('<iframe id="deepLink" style="display:none"></iframe>');*/
        //Test Deep Link

        //$('#deepLink').attr('src','birdland://');
        //If DeepLink Fails
        window.location = 'birdland://';
        setTimeout(function() {
            window.location = "https://itunes.apple.com/us/app/my.app/id694718661?ls=1&mt=8";
        }, 500);
    }
};