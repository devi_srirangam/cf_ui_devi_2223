/**
 * Created by WebApp on 2/25/2015.
 */
"use strict";

var CloudFuze = angular.module('testApp', ['ui.router']);

CloudFuze.config(['$urlRouterProvider','$stateProvider',function(a,b) {
    a.otherwise('/keys');
    b.state("app", {
            url: '/keys'
        })
        .state('app.keys', {
            url: '/update',
            views: {
                '@': {
                    templateUrl: 'views/app.html'
                },
                'choosekeys@app.keys': {
                    templateUrl: 'views/choosekeys.html',
                    controller: 'setOptions'
                }
            }
        })
        .state('app.keys.G_DRIVE', {
            url: '/google',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/google.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('Google Drive');
                }
            }
        })
        .state('app.keys.GMAIL', {
            url: '/google',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/google.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('Google Mail');
                }
            }
        })
        .state('app.keys.ONEDRIVE', {
            url: '/onedrive',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/onedrive.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('OneDrive');
                }
            }
        })
        .state('app.keys.ONEDRIVE_BUSINESS', {
            url: '/onedrivebusiness',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/onedrivebusiness.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('OneDrive for Business');
                }
            }
        })
        .state('app.keys.BOX', {
            url: '/box',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/box.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('Box');
                }
            }
        })
        .state('app.keys.DROP_BOX', {
            url: '/dropbox',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/dropbox.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('Drop Box');
                }
            }
        })
        .state('app.keys.SALES_FORCE', {
            url: '/salesforce',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/salesforce.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('SalesForce');
                }
            }
        })
        .state('app.keys.SHAREFILE', {
            url: '/sharefile',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/salesforce.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('Citrix ShareFile');
                }
            }
        })
        .state('app.keys.EGNYTE_STORAGE', {
            url: '/egnyte',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/salesforce.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('Egnyte');
                }
            }
        })
        .state('app.keys.ORANGE', {
            url: '/orange',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/salesforce.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('Orange');
                }
            }
        })
        .state('app.keys.AMAZON_STORAGE', {
            url: '/amazonstorage',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/salesforce.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('Amazon Storage');
                }
            }
        })
        .state('app.keys.SHAREPOINT_ONLINE', {
            url: '/sharepointonline',
            views: {
                'changedrive@app.keys': {templateUrl: 'views/sharepointonline.html'}
            },
            resolve: {
                data: function (CFFactory) {
                    CFFactory.setoption('SharePoint Online');
                }
            }
        });
}]);

CloudFuze.controller('IndexCtrl',['$scope','$state','CFVariable','CFFactory','$http','$rootScope',function(a,s,cf,fact,aj,rs){
    var login = {};
    alert('Do not refresh the page to keep your session alive !!!');
    a.authenticate = function(){
        var email = a.login.email;
        var pwd = a.login.pwd;
        var create = cf.config;
        create.headers.Authorization = fact.basicAuth(email, CryptoJS.MD5(pwd));
        create.url = cf.api + "/auth/user";
        create.method = "POST";
        create.async = false;
        aj(create).success(function (data, status, headers) {
            rs.login = true;
            rs.auth = create.headers.Authorization;
            s.go('app.keys');
        }).error(function (data, status, headers) {
            alert('Unauthorized');
            s.go('app');
        });
    };
    a.revokeAccess = function(){
        rs.auth = undefined;
        a.login = {};
        s.go('app');
    }
}]);

CloudFuze.controller('setOptions',['$scope','$state','$rootScope',function(a,s,r){
    if(r.auth != undefined){
        a.types = [
            {id:"G_DRIVE",name:"Google Drive"},
            {id:"GMAIL",name:"Google Mail"},
            {id:"ONEDRIVE",name:"OneDrive"},
            {id:"ONEDRIVE_BUSINESS",name:"OneDrive for Business"},
            {id:"BOX",name:"Box"},
            {id:"DROP_BOX",name:"Drop Box"},
            {id:"SALES_FORCE",name:"SalesForce"},
            {id:"SHAREFILE",name:"Citrix ShareFile"},
            {id:"EGNYTE_STORAGE",name:"Egnyte"},
            {id:"ORANGE",name:"Orange"},
            {id:"AMAZON_STORAGE",name:"Amazon Cloud Drive"},
            {id:"SHAREPOINT_ONLINE",name:"SharePoint Online"}
        ];
    }
    else{
        s.go('^');
    }
}]);

CloudFuze.controller('changeview',['$scope','$state','CFVariable','$http','CFFactory','$rootScope',function(a,state,cf,aj,fact,rs){
    var value = '';
    a.data = {};
    a.getvalue = function(b){
        angular.element($('#wrap').find('#driveopt').children('[value=""]')).remove();
        value = b;
        var _a = "app.keys."+ b.id;
        state.go(_a);
        var getDetails = cf.config;
        getDetails.headers.Authorization = rs.auth;
        getDetails.url = cf.api + "/auth/oauthkeys?cloudName="+b.id;
        getDetails.method = "GET";
        getDetails.async = false;
        aj(getDetails).success(function (data, status, headers) {
            if(data[0] == null){
                a.data = {};
                return false;
            }
            a.data.clientId = data[0].clientId;
            a.data.clientSecret = data[0].clientSecret;
            a.data.redirecturl = data[0].redirecturl;
            a.data.secretJson = data[0].secretJson;
        }).error(function (data, status, headers) {
            if(status == 401) {
                alert('Unauthorized');
                window.location.href = "#/";
            }
        });
    };
    a.formreset = function(){
        a.data = {};
    };
    a.updateKeys = function() {
        a.data.cloudName = value.id;
        var email = $('#cfUserId').val().trim().toLowerCase();
        var pwd = $('#cfPassword').val();
        var create = cf.config;
        create.headers.Authorization = rs.auth;
        create.url = cf.api + "/auth/create/oauthkeys";
        create.method = "POST";
        create.async = false;
        create.data = a.data;
        aj(create).success(function (data, status, headers) {
            alert('Keys Updated for '+value.name);
        }).error(function (data, status, headers) {
            alert('Update failed for '+value.name);
        });
    };
}]);

CloudFuze.factory('CFFactory',function($rootScope,$state){
    return {
        setoption: function (c) {
            if($rootScope.auth != undefined){
                setTimeout(function () {
                    $('#wrap').find('[selected="selected"]').removeAttr('selected');
                    $('#wrap').find('[label="' + c + '"]').attr('selected','selected');
                    angular.element($('#wrap').find('select')).triggerHandler('change');
                }, 400);
            }
            else{
                //alert('Unauthorized to access.');
                window.location.href = "#/";
            }
        },
        basicAuth:function(a,b){
            var t = a+":"+b;
            t = Base64.encode(t);
            return "Basic "+t;
        }
    };
});

CloudFuze.service('SyncAjax',[function(){
    this.syncCall = function(a){
        var _a = '';
        jQuery.ajax({
            type: a.method,
            url: a.url,
            data: a.data,
            headers: a.headers,
            async: a.async,
            success:function(data){
                data.status = 200;
                return _a = data;
            },
            error:function(error){
                return _a = error;
            }
        });
        return _a;
    }
}]);

CloudFuze.constant('CFVariable',{
    "api":"https://"+window.location.hostname+"/proxyservices/v1",
    "config":{
        method: '',
        url: '',
        data:'',
        headers: {
            "Content-Type": "application/json",
            "Authorization":'',
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        async:true
    }
});

CloudFuze.directive('updateTitle',['$rootScope','$timeout',function(r,t){
    return{
      link:function(scope,element){
          var listener = function(event,toState){
              var title = "Update Keys";
              if(toState.data && toState.data.pageTitle){
                  title = toState.data.pageTitle;
              }
              t(function(){
                 element.text(title);
              },0,false);
          };
          r.$on('$stateChangeSuccess',listener);
      }
    };
}]);