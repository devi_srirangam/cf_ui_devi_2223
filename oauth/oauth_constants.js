/////===================constants declaration================================
var domainURL = "";
var domainURL_put;
var redirectURL = '';
var host = window.location.hostname;
if(isProd){
    domainURL = "https://"+host;
    domainURL_put = "https://"+host+"/proxyservices";
    redirectURL = domainURL+"/oauth/oauth.html";
}
else if(isQA){
    domainURL = "https://"+host;
    domainURL_put = "https://"+host+"/proxyservices";
    redirectURL = domainURL+"/oauth/oauth.html";
}
else if(isDemo) {
    defaultDomain = "https://demos.cloudfuze.com/";
    redirectUrl = "https://demos.cloudfuze.com";
    GA = "UA-56350345-1";
    isProd = false;
} else if(isOnpremise){
    domainURL = "https://"+host;
    domainURL_put = "https://"+host+"/proxyservices";
    redirectURL = domainURL+"/oauth/oauth.html";
}else{
    domainURL= "https://devwebapp.cloudfuze.com";
    redirectURL = domainURL+"/oauth/oauth.html";
}

/*Drop Box*/
var dropbox_redirect_URL = domainURL+"/addcloud/dropbox/2/oauth2/token/";

/*Drop Box For Business*/
var dropbox_business_token = domainURL+"/addcloud/dropboxbusiness/1/oauth2/";

/*Egnyte For Business*/
var egnyte_Business_token = domainURL+"/addcloud/egnyte/oauth2/";
var egnyte_User_token = domainURL+"/addcloud/egnyte/pubapi/v1/userinfo/";

/*SkyDrive - One Drive*/
var skyDrive_userinfo = domainURL+"/addcloud/skydriveapi/v5.0/me?access_token=";
var skyDrive_postURL = domainURL+"/addcloud/skydrive/oauth20_token.srf";

var oneDrive_token_v2 = domainURL+"/addcloud/skydrive_v2/token";

/*One Drive Business Admin*/
var microsoftGraph  = domainURL+"/addcloud/microsoft/graph/";

/*One Drive Business*/

var OneDriveB_Token = domainURL+"/addcloud/onedrivebusiness/token/";
var OneDriveB_Service = domainURL+"/addcloud/onedrivebusiness/services/";


/*Google*/
var google_userinfo = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=";
var grdrive_postURL = domainURL+"/addcloud/google/o/oauth2/token";
var google_discover = 'https://www.googleapis.com/admin/directory/v1/users/';

/*Box*/
var box_Post_URL = domainURL+"/addcloud/box/1/oauth2/token";
var box_userinfo = domainURL+"/addcloud/box/2.0/users/me";

/*Box_Business*/
var  box_business_Post_URL = domainURL+"/addcloud/boxbusiness/oauth2";

/*Syncplicity*/
var  syncplicity_Post_URL = domainURL+"/addcloud/syncplicitybusiness/oauth2/";
var syncplicity_userinfo = domainURL+"/addcloud/syncplicitybusiness/users/";


/*Share File*/
var sharefile_gettokenURL =domainURL+"/addcloud/sharefile/oauth/token?grant_type=authorization_code&redirect_uri=";
var sharefiledata = null;
var sharefileUsers = domainURL+"/addcloud/sharefile/sfapi/v1/userinfo/";

/*Egnyte*/
//var egnyte_Client_Id1 = 'fmjq9n92yhj7zt2a4whxqf9m';
//var egnyte_Client_Id = "mwzfpsdrjmya4pbuyec3qy9t";

/*Orange*/
var orange_TokenURL = domainURL+"/addcloud/orange/token/";
var orangeUserInfo = domainURL+"/addcloud/orange/user/";

/*SalesForce*/
var salesForce_TokenUrl = domainURL+"/addcloud/salesforce/token/";
var salesForce_userInfo = domainURL+"/addcloud/salesforce/user/";

/*Amazon Storage*/
var amazon_storage_token = domainURL+"/addcloud/amazon/storage/token";
var amazon_storage_user = domainURL+"/addcloud/amazon/storage/profile";

/*Cisco Spark*/
var spark_token = domainURL+"/addcloud/spark/token";
var spark_profile = domainURL+"/addcloud/spark/profile";

/*SharePoint Online*/
var SharePointOnline_token = domainURL+'/addcloud/sharepoint/online/token';
var SharePoint_Online_token=domainURL+'/addcloud/sharepointonline/1/oauth2/';
var SharePoint_Online_services=domainURL+'/addcloud/sharepointonline/services/oauth2/ ';
var SharePoint_Online_email=domainURL+'/addcloud/sharepointonline/services2/';

/*Webdam*/
var webdam_token = domainURL+"/addcloud/webdam/token";
var webdam_profile = domainURL+"/addcloud/webdam/user";

/*Slack*/
var slack_token = domainURL+"/addcloud/slack/token";

//
//fmjq9n92yhj7zt2a4whxqf9m
////====================end of constants declaration=========================

/*Code to track errors*/

if (window.addEventListener) {
    window.addEventListener('error', trackJavaScriptError, false);
} else if (window.attachEvent) {
    window.attachEvent('onerror', trackJavaScriptError);
} else {
    window.onerror = trackJavaScriptError;
}
var BrowserDetect = {
    init: function () {
        this.browser = this.searchString(this.dataBrowser) || "An unknown browser";
        this.version = this.searchVersion(navigator.userAgent)
            || this.searchVersion(navigator.appVersion)
            || "an unknown version";
        this.OS = this.searchString(this.dataOS) || "an unknown OS";
    },
    searchString: function (data) {
        for (var i=0;i<data.length;i++)	{
            var dataString = data[i].string;
            var dataProp = data[i].prop;
            this.versionSearchString = data[i].versionSearch || data[i].identity;
            if (dataString) {
                if (dataString.indexOf(data[i].subString) != -1)
                    return data[i].identity;
            }
            else if (dataProp)
                return data[i].identity;
        }
    },
    searchVersion: function (dataString) {
        var index = dataString.indexOf(this.versionSearchString);
        if (index == -1) return;
        return parseFloat(dataString.substring(index+this.versionSearchString.length+1));
    },
    dataBrowser: [
        {
            string: navigator.userAgent,
            subString: "Chrome",
            identity: "Chrome"
        },
        {
            string: navigator.vendor,
            subString: "Apple",
            identity: "Safari",
            versionSearch: "Version"
        },
        {
            string: navigator.userAgent,
            subString: "Firefox",
            identity: "Firefox"
        },
        {
            string: navigator.userAgent,
            subString: "MSIE",
            identity: "Explorer",
            versionSearch: "MSIE"
        }
    ],
    dataOS : [
        {
            string: navigator.platform,
            subString: "Win",
            identity: "Windows"
        },
        {
            string: navigator.platform,
            subString: "Mac",
            identity: "Mac"
        },
        {
            string: navigator.userAgent,
            subString: "iPhone",
            identity: "iPhone/iPod"
        },
        {
            string: navigator.platform,
            subString: "Linux",
            identity: "Linux"
        }
    ]
};
BrowserDetect.init();
function trackJavaScriptError(e) {
    var _msg = e.filename+' : '+e.lineno+' - '+BrowserDetect.browser+' Ver: '+BrowserDetect.version+' OS: '+BrowserDetect.OS;
    //_gaq.push(['_trackEvent', "JavaScript Error" , e.message, _msg]);
    ga('send', 'event', "JavaScript Error" , e.message, _msg); //New Code
}