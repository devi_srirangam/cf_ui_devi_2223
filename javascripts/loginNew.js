window.CloudFuze = {};
/*window.CloudFuze.orangeLanding1 = '<div style="color: #fff;text-align: justify;font-size: 15px;">'+
    '<h4>&Egrave;largissez la port&egrave;e et le potentiel de votre cloud Orange avec CloudFuze !</h4>'+
    '<p>Pour tous vos fichiers. Sur tous vos appareils. O&ugrave; que vous soyez.</p>'+
    '<p>B&egrave;n&egrave;ficiez d\'un large &egrave;ventail de nouvelles fonctionnalit&egrave;s en acc&egrave;dant &agrave; votre espace de stockage Orange via CloudFuze :</p>'+
    '<ul style="list-style-type: disc;margin: 20px 0 20px 20px;">'+
    '<li>Un acc&egrave;s et une gestion multi-comptes : utilisez CloudFuze pour acc&egrave;der &agrave; votre cloud Orange et &agrave; d\'autres services de stockage &agrave; partir d\'un seul compte, et g&egrave;rer vos fichiers ainsi que leur transfert d\'un cloud &agrave; un autre plus facilement.</li>' +
    '<li>Un acc&egrave;s mobile : acc&egrave;dez &agrave; tous vos fichiers depuis vos appareils mobiles avec les applications CloudFuze pour iOS et Android.</li>'+
    '<li>Une capacit&egrave; de partage optimis&egrave;e : partagez facilement tous vos fichiers, quelles que soient leurs tailles, et b&egrave;n&egrave;ficiez de fonctionnalit&egrave;s de s&egrave;curit&egrave; avanc&egrave;es pour les mots de passe, les limitations de t&egrave;l&egrave;chargement et les dates d\'expiration.</li>'+
    '</ul>'+
    '<p>CloudFuze propose un panel d\'applications web, iOS, Android, et de bureau pour Windows. Nos clients peuvent pleinement b&egrave;n&egrave;ficier, o&ugrave; qu\'ils soient et depuis n\'importe quel appareil, de fonctionnalit&egrave;s de partage optimis&egrave;es, d\'espaces de travail partag&egrave;s et d\'autres fonctionnalit&egrave;s am&egrave;liorant la productivit&egrave;, sur leurs comptes de stockage Orange.</p>'+
    '<p>Cr&egrave;er votre compte CloudFuze et commencez &agrave; utiliser nos applications pour optimiser les possibilit&egrave;s d\'utilisation de votre cloud Orange au maximum !</p>'+
    '<p>Si vous souhaitez essayer le service, nous vous invitons de <a href="index.html?signup">cliquer ici</a></p>';
'</div>';
new*/

/*window.CloudFuze.orangeLanding = '<div style="color: #000;text-align: justify;font-size: 15px;font-family: \'Raleway\', Arial, sans-serif;" class="span12">'+
    '<h4>CloudFuze : c&#39;est quoi ?</h4>'+
    '<p>CloudFuze est une solution vous permettant d&#39;avoir un acc&egrave;s unique &agrave; tous vos services de stockage en ligne.</p>'+
    '<h4>B&eacute;n&eacute;ficiez d&#39;un large &eacute;ventail de nouvelles fonctionnalit&eacute;s en acc&eacute;dant &agrave; votre espace de stockage Orange via CloudFuze</h4>'+
    '<ul class="loginList"><li>Un acc&egrave;s et une gestion multi-comptes : utilisez CloudFuze pour acc&eacute;der &agrave; votre cloud Orange et &agrave; d&#39;autres services de stockage &agrave; partir d&#39;un seul compte, et g&eacute;rer vos fichiers ainsi que leur transfert d&#39;un cloud &agrave; un autre plus facilement.</li>' +
    '<li>Un acc&egrave;s mobile : acc&eacute;dez &agrave; tous vos fichiers depuis vos appareils mobiles avec les applications CloudFuze pour iOS et Android.</li>' +
    '<li>Une capacit&eacute; de partage optimis&eacute;e : partagez facilement tous vos fichiers, quelles que soient leurs tailles, et b&eacute;n&eacute;ficiez de fonctionnalit&eacute;s de s&eacute;curit&eacute; avanc&eacute;es pour les mots de passe, les limitations de t&eacute;l&eacute;chargement et les dates d&#39;expiration.</li></ul>'+
    '<p style="margin:0">&nbsp;</p><p>CloudFuze propose un panel d&#39;applications web, iOS, Android, et de bureau pour Windows.</p>' +
    '<i>Le service est propos&eacute; en langue anglaise exclusivement.</i>'+
    '<div class="span12" style="margin: 0">' +
    '<div class="span8"><h4>D&eacute;couvrez  le service sans engagement</h4>' +
    '<ul class="loginList"><li>Cr&eacute;er votre compte CloudFuze et d&eacute;couvrez les possibilit&eacute;s offertes</li>' +
    '<li>Les utilisateurs Orange disposent d&#39;une offre sp&eacute;cifique gratuite pendant 1 an</li></ul></div>' +
    '<div class="span4"><div class="orangeOffer"><div class="offer">offre sp&egrave;ciale gratuit 1 an pour toute inscription avant le 31/12/2015</div><span class="a"><strong>Souscription mensuelle</strong></span><span class="b"><i></i><h2>4.99&euro;/</h2>Mois</span>' +
    '<span class="c"><p>Application Mobile, Web, Bureau</p><p>Comptes illimit&eacute;s</p><p>Acc&egrave;s depuis un nombre illimit&eacute; de terminaux</p></span></div></div></div>';


window.CloudFuze.defaultLanding ='<img src="../img/cfslide1-1-3.png">';new*/

(function() {
    var LoginManager,
        __bind = function (fn, me) {
            return function () {
                return fn.apply(me, arguments);
            };
        };

    LoginManager = (function () {
        function LoginManager(container) {
            window.CloudFuze.action = "";
            window.CloudFuze.emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,6})?$/;
            this.container = container;
            this.hideAll = __bind(this.hideAll, this);
            this.showForgotForm = __bind(this.showForgotForm, this);
            this.showRegisterForm = __bind(this.showRegisterForm, this);
            this.showLoginForm = __bind(this.showLoginForm, this);
            this.showResendForm = __bind(this.showResendForm, this);
            this.showOrangeForm = __bind(this.showOrangeForm, this);
            this.loginForm = this.container.find("#login");
            this.registerForm = this.container.find("#register");
            this.forgotForm = this.container.find("#forgot");
            this.resendForm = this.container.find("#resendlink");
            this.confForm = this.container.find("#confmsg");
            this.entLogin = this.container.find("#entLogin");
            this.orangeForm = this.container.find('#orangeRegister');
            this.loginLink = this.container.find("#changeLink");
            this.signup222 = this.container.find("#SignUplink");
            this.forgotLink = this.container.find("#forgot-link-d");
            this.registerLink = this.container.find("#register-link-d");
            this.resendLink = this.container.find("#resend-link-d");
            this.loginLinks = this.container.find("#login-link");
            this.loginLinks2 = this.container.find("#login-link2");
            this.forgotLinks = this.container.find("#forgot-link");
            this.registerLinks = this.container.find("#register-link");
            this.resendLink = this.container.find("#resend-link");
            this.loginLinks.bind("click", this.showLoginForm);
            this.loginLinks2.bind("click", this.showLoginForm);
            this.forgotLinks.bind("click", this.showForgotForm);
            this.registerLinks.bind("click", this.showRegisterForm);
            this.signup222.bind("click", this.showLoginForm);
            this.resendLink.bind("click", this.showResendForm);
            this.loginSubmit = this.container.find("#login-submit");
            this.resendSubmit = this.container.find("#resend-submit");
            this.forgetSubmit = this.container.find("#forgot-submit");
            this.registerSubmit = this.container.find("#register-submit");
            this.loginSubmit.click(function (e) {
			 e.preventDefault();
                     $("#expiredAccount").css("display","none");
                var _this = $(this).closest(".login-wrapper");
                var statusMsg = _this.find(".statusMesg");
                var statusMsg1 = _this.find(".statusMesg1");
             
                statusMsg.html('');
                statusMsg1.html('');
                _this.find(".recendConf").html('');
                var email = _this.find("#email").val().trim().toLowerCase();
                var pwd = _this.find("input[type='password']").val();
                var _email = cfValidations.email(email);
				if(email.split("@")[1] == "sharklasers.com"){
				return false;
				}
                if(_email !== true ){
                    $("#login-manager .statusMesg").css("display", "");
                    $("#email").focus();
                    statusMsg.text(_email);
                }
                else{
                    var checksum = "loginPage";
                    var emailID= ForgotpasswordAjaxCall.searchUser(email, checksum, pwd);
				}
                
            });
              this.resendSubmit.click(function (e) {
                e.preventDefault();
              $("#expiredAccount").css("display","none");
                var _this = $(this).closest(".login-wrapper");
                var email = $(this).closest("form").find("#email").val().trim().toLowerCase();
                var resend = $(this).closest("form").find("#email");
                var statusMsg = _this.find(".statusMesg");
                if (email.length == 0) {
                    statusMsg.text("*Please enter email.");
                    resend.focus();
                    if(cfValidations)
                        cfValidations.addAnimation(_this);
                }
                else if (email.length > 50) {
                    statusMsg.text("*The email address that you entered does not have a CloudFuze account.");
                    resend.focus();
                    if(cfValidations)
                        cfValidations.addAnimation(_this);
                }
                else if (!window.CloudFuze.emailReg.test(email)) {
                    statusMsg.text("*Please enter a valid email address.");
                    resend.focus();
                    if(cfValidations)
                        cfValidations.addAnimation(_this);
                }
                else {
                    var checksum = "resendConfirmLink";
                    ForgotpasswordAjaxCall.searchUser(email, checksum, email);
                }
            });
            this.forgetSubmit.click(function (e) {
                e.preventDefault();
            $("#expiredAccount").css("display","none");
                var _this = $(this).closest(".login-wrapper");
                var email = $(this).closest("form").find("#email_reset").val().trim().toLowerCase();
                var statusMsg = _this.find(".statusMesg");
                var forgotEmail = $("#forgot").find("#email_reset");

                var _email = cfValidations.email(email);
                if(_email !== true ){
                    $("#login-manager .statusMesg").css("display", "");
                    statusMsg.text(_email);
                }
                else{
                    var checksum = "sendPwdLink";
                    ForgotpasswordAjaxCall.searchUser(email, checksum, email);

                }
              
            });

            this.registerSubmit.click(function (e) {
			e.preventDefault();
            $("#expiredAccount").css("display","none");
                var _this = $(this).closest(".login-wrapper");
                var statusMsg = _this.find('.statusMesg');
                var statusMsg3 = _this.find('.statusMesg3');
                var statusMsg4 = _this.find('.statusMesg4');
                var statusMsg5 = _this.find('.statusMesg5');
                var statusMsg6 = _this.find('.statusMesg6');

                var object = {};
                object.instance = null;
                object.name = $(this).closest("form").find("#name").val();
                object.email = $(this).closest("form").find("#email").val().toLowerCase();
				object.phone = $(this).closest("form").find("#phone").val();
                object.pwd = $(this).closest("form").find("#pwd").val();
                var check = _this.find('#terms_conditons').is(':checked');
                var _a = [];
                var _b = [];
                if (object.name.length >= 1) {
                    _b.push('name');
                }

                if (object.email.length >= 1) {
                    _b.push('email');

                }
				if (object.phone.length >= 1) {
                    _b.push('phone');

                }
                if (object.pwd.length >= 1) {
                    _b.push('password');
                }
                if (_b.length >= 2) {

                    var _name = cfValidations.name(object.name);
                    var _emailCheck =  cfValidations.email(object.email);
                    var _emailExist =  cfValidations. searchUser(object.email);
                    var _passCheck =  cfValidations.validatePwd(object.pwd);


                    if(_name == true){
                        $("#login-manager .statusMesg4").css("display", "none");
                        $("#login-manager .statusMesg6").css("display", "none");
                        $("#login-manager .statusMesg5").css("display", "none");
                        $("#login-manager .statusMesg").css("display", "none");


                        if(_emailCheck == true && _emailExist == true){
                            $("#login-manager .statusMesg2").css("display", "none");
                            $("#login-manager .statusMesg4").css("display", "none");
                            $("#login-manager .statusMesg6").css("display", "none");
                            $("#login-manager .statusMesg5").css("display", "none");


                            if(_passCheck == true){
                                $("#login-manager .statusMesg3").css("display", "none");
                                $("#login-manager .statusMesg2").css("display", "none");
                                $("#login-manager .statusMesg4").css("display", "none");
                                $("#login-manager .statusMesg6").css("display", "none");
                                $("#login-manager .statusMesg5").css("display", "none");
                                $("#login-manager #terms_conditons").focus();

							}
                            else{
                                if( object.pwd.length == 0){
                                    $("#login-manager .statusMesg3").css("display", "none");
                                    $("#login-manager .statusMesg5").css("display", "");
                                    statusMsg5.text("*Please enter password");
                                    $('#login-manager #pwd').focus();
                                    return false;

                                }
                                else {
                                    $('#login-manager #pwd').focus();
                                    $("#login-manager .statusMesg3").css("display", "none");
                                    $("#login-manager .statusMesg4").css("display", "none");
                                    $("#login-manager .statusMesg6").css("display", "none");
                                    $("#login-manager .statusMesg5").css("display", "");
                                    statusMsg5.text(_passCheck);
                                    return false;
                                }
                            }
                        }

                        else if( _emailCheck !== true && _emailExist == true){
                            $("#login-manager .statusMesg2").css("display", "none");

                            $("#login-manager .statusMesg6").css("display", "none");
                            $("#login-manager .statusMesg5").css("display", "none");
							$("#login-manager .statusMesg4").css("margin-top","2%").css("margin-bottom","0%");
                            $("#login-manager .statusMesg4").css("display","");
                            statusMsg4.text(_emailCheck);

                            $(" #login-manager #name").focusout();
                            $('#login-manager #email').focus();
                            return false;
                        }
                        else if( _emailCheck == true && _emailExist !== true){
                            $("#login-manager .statusMesg2").css("display", "none");

                            $("#login-manager .statusMesg6").css("display", "none");
                            $("#login-manager .statusMesg5").css("display", "none");
							$("#login-manager .statusMesg4").css("margin-top","0%").css("margin-bottom","-2%");
                            $("#login-manager .statusMesg4").css("display","");
                            statusMsg4.text(_emailExist);

                            $(" #login-manager #name").focusout();
                            $('#login-manager #email').focus();
                            return false;
                        }
                        else{
                            $("#login-manager .statusMesg2").css("display", "none");
                            $("#login-manager .statusMesg6").css("display", "none");
                            $("#login-manager .statusMesg5").css("display", "none");
							$("#login-manager .statusMesg4").css("margin-top","2%").css("margin-bottom","0%");
                            $("#login-manager .statusMesg4").css("display","");
                            statusMsg4.text(_emailCheck);

                            $(" #login-manager #name").focusout();
                            $('#login-manager #email').focus();
                            return false;
                        }

                    }
                    else{
                        $('#login-manager #name').focus();
                        $("#login-manager .statusMesg4").css("display", "none");
                        $("#login-manager .statusMesg5").css("display", "none");
                        $("#login-manager .statusMesg6").css("display", "");
                         $("#login-manager .statusMesg").css("display", "none");
                        statusMsg6.text(_name);
                        return false;
                    }




                }

                if (object.name.length < 1) {
                    _a.push('name');
                }

                if (object.email.length < 1) {
                    _a.push('email');

                }
                if (object.pwd.length < 1) {
                    _a.push('password');
                }
                
                if (_a.length > 1) {

                    if (object.name.length < 1 && object.email.length < 1 && object.pwd.length < 1  ) {
                        _this.find('#name').focus();
                        $("#login-manager .statusMesg").css("display", "");
                        statusMsg.text('*Please enter all required fields.');
                    }
                    else if(object.email.length < 1  && object.pwd.length < 1 ) {
                         

                        if(object.name.length >= 1){
                            var _p = cfValidations.name(object.name);

                            if(_p == true){
                                $("#login-manager .statusMesg5").css("display", "none");
                                statusMsg6.css("display","none");
                                _this.find('#email').focus();
                                $("#login-manager .statusMesg").css("display", "");
                                statusMsg.text('*Please enter all required fields.');
                            }
                            else{
                                $("#login-manager .statusMesg").css("display", "none");
                                $("#login-manager .statusMesg6").css("display", "");
                                statusMsg6.text(_p);
                                _this.find('#name').focus();

                            }

                        }
					}

                    if(cfValidations)
                      statusMsg.css("display","");
                     $("#login-manager .statusMesg3").css("display", "none");
                         $("#login-manager .statusMesg4").css("display", "none");
                            $("#login-manager .statusMesg5").css("display", "none");
                        $("#login-manager .statusMesg6").css("display", "none");



                          statusMsg.text('*Please enter all required fields.');                          

                        cfValidations.addAnimation(_this);
                }
                else if (_a.length == 1) {
                    var _email = cfValidations.email(object.email);
                    var _pwd = cfValidations.validatePwd(object.pwd);
                    var _name = cfValidations.name(object.name);
                    if(_name !== true){
                        $("#login-manager .statusMesg").css("display", "none");
                        $("#login-manager .statusMesg4").css("display", "none");
                        $("#login-manager .statusMesg5").css("display", "none");
                        $("#login-manager .statusMesg6").css("display", "");
                        statusMsg6.text(_name);
                        _this.find('#name').focus();

                        if (cfValidations)
                            cfValidations.addAnimation(_this);
                    }
                    else if(_email !== true){
                        $("#login-manager .statusMesg2").css("display", "none");
                        $("#login-manager .statusMesg4").css("display", "");

                        statusMsg4.text(_email);
                        _this.find('#email').focus();
                        $("#login-manager .statusMesg").css("display", "none");
                        if (cfValidations)
                            cfValidations.addAnimation(_this);
                    }
                    else if(object.pwd.length < 1){
                        $("#login-manager .statusMesg").css("display", "none");
                        $("#login-manager .statusMesg4").css("display", "none");
                        $("#login-manager .statusMesg6").css("display", "none");
                        $("#login-manager .statusMesg5").css("display", "");
                        $("#login-manager .statusMesg5").text("*Please enter password");
                        _this.find('#email').focusout();
                        _this.find('#pwd').focus();
                        if (cfValidations)
                            cfValidations.addAnimation(_this);



                    }
                


                    else {
                        $("#login-manager .statusMesg5").css("display", "none");
                        $("#login-manager .statusMesg4").css("display", "none");
                        _this.find('#pwd').focus();
                        statusMsg.text('Please enter ' + _a[0] + '.');
                        if (cfValidations)
                            cfValidations.addAnimation(_this);
                    }

                }
                
                else {
                    statusMsg.text('');
                    var _email = cfValidations.email(object.email);
                    var _name = cfValidations.name(object.name);
                    if (_name == true) {
                        if (_email == true) {
                            var _s = cfValidations.searchUser(object.email);
                            if (_s == true) {
                                var _pwd = cfValidations.validatePwd(object.pwd);
                                if (_pwd == true) {
                                    object.enable = true;
								/*	 $.ajax({
									 type: 'GET',
									 url: apicallurl+"/users/verifyUsermail/"+object.email,
									 async: false,
									 success: function (result) {
									if(result == true){*/
                                    var signup = loginAjaxCall.signup(object);
                                    if (window.CloudFuze.action == 'orange' && signup != "failed") {
                                        localStorage.setItem("UserAuthDetails", loginAjaxCall.BasicAuth(object.email, CryptoJS.MD5(object.pwd).toString()));
                                        localStorage.setItem('UserId', signup.id);
                                        localStorage.setItem('UserName', signup.lastName);
                                        localStorage.setItem('CFUser', JSON.stringify(signup));
                                        window.location.href = 'cloudmanager.html#orange'
                                    }
                                    else if (signup != "failed") {
                                        localStorage.setItem("UserAuthDetails", loginAjaxCall.BasicAuth(object.email, CryptoJS.MD5(object.pwd).toString()));
                                        localStorage.setItem('UserId', signup.id);
                                        localStorage.setItem('UserName', signup.lastName);
                                        localStorage.setItem('CFUser', JSON.stringify(signup));
                                        sendPageView(window.location.pathname + "?signedup=true");
                                        window.location.href = "cloudmanager.html?ver=1028"; 
                                    }
									/*}
									else{
										alert("Please enter valid email id"); 
										//$(".statusMesg").text("* Please enter valid mail id").css("display","");
									}
									 } 
									 });*/
                                }
                                else {
                                    $("#login-manager .statusMesg2").css("display","none");
                                    $("#login-manager .statusMesg").css("display","none");
                                    $("#login-manager .statusMesg3").css("display","");
                                    statusMsg3.text(_pwd);
                                    $("#register-submit").text('Sign Up');
                                    $("#register-submit").removeAttr('disabled');
                                    statusMsg5.text(_pwd);
                                    _this.find('#pwd').focus();
                                    if(cfValidations)
                                        cfValidations.addAnimation(_this);
                                }
                            }
                            else {
                                statusMsg4.text(_s);
                                _this.find('#email').focus();
                                if(cfValidations)
                                    cfValidations.addAnimation(_this);
                            }
                        }
                        else {
                            statusMsg4.text(_email);
                            _this.find('#email').focus();
                            if(cfValidations)
                                cfValidations.addAnimation(_this);
                        }
                    }
                    else{
                        statusMsg6.text(_name);
                        _this.find('#name').focus();
                        if(cfValidations)
                            cfValidations.addAnimation(_this);
                    }
                }
            });


            window.CloudFuze.action = window.location.search.substring(1).split('=')[0];
            if (window.CloudFuze.action === 'signup') {
                this.showRegisterForm();
            }
            else if (window.CloudFuze.action === 'forgotpassword') {
                this.showForgotForm();
            }
            else if (window.CloudFuze.action == 'resendlink') {
                this.showResendForm();
            }
            else if (window.CloudFuze.action == 'campaign') {
                this.showOrangeForm();
            }
            else if (window.CloudFuze.action == 'orange') {
                this.showRegisterForm();
            }
            else {
                this.showLoginForm();
            }
        }
        LoginManager.prototype.showLoginForm = function () {
           $("#expiredAccount").css("display","none");
          /*  this.hideAll();
            this.loginForm.show();
            $("#SignUplinktext").css("display","none");
            $("#loginWrapperFooter").removeClass("Dragbottom");
            this.signup222.hide();
            this.registerLink.show();
            $(".social-iconstab").css("display","");
            $("#login #email").focus();
		  return this.forgotLink.show();*/
		  this.hideAll();
			   $("#loginHeading").css("display","").text("Login to your account");
   $("#changeSpan").css("display","").text("Create an account for free ? ");
   $("#changeLink").css("display","").text("Sign up");
   $("#loginNote").css("display","");
   $("#welcome").css("display","");
   $("#login").css("display","");
   $("#loginType").css("display","");
     $("#register").css("display","none");
	   $(".underscoreDiv").css("display","");
	    $("#socialLogin").css("display","");
	 $("#contentDiv").css("padding-top","5%");
	    $("#hrLine").css("display","");
	 $("#forgot").css("display","none");
	 $("#login #email").focus();
	 $("#login-manager .statusMesg").css("display","none");
    $("#login-manager .statusMesg1").css("display","none");
    $("#login-manager .statusMesg2").css("display","none");
    $("#login-manager .statusMesg3").css("display","none");
    $("#login-manager .statusMesg4").css("display","none");
    $("#login-manager .statusMesg5").css("display","none");
    $("#login-manager .statusMesg6").css("display","none");
	 $(document).prop('title', 'Login to Your CloudFuze Account');
  //  window.history.pushState("CloudFuze", "Show login link", window.location.pathname+"?login=true");
    sendPageView(window.location.href);
        };
        LoginManager.prototype.showResendForm = function () {
           $("#expiredAccount").css("display","none");
            $("#SignUplinktext").css("display","none");
            this.hideAll();
            this.resendForm.show();
            this.registerLink.show();
            $("#resendlink #email").focus();
            return true;
        };
        LoginManager.prototype.showRegisterForm = function () {
          $("#expiredAccount").css("display","none");
		  
         /*   $("#SignUplinktext").css("display","");
            this.hideAll();
            this.registerForm.show();
			$("#phone").val('+1');
            this.signup222.show();
            this.loginLink.hide();
            $(".social-iconstab").css("display","");
            $("#login-link-d").css("display","none");
           
            $("#loginWrapperFooter").addClass("Dragbottom");
            $("#login-link2").css("display","");
         $("#login-linkal").css("display","");
          $("#login-link").css("display","");

            $('#name').focus();
		
		 return true; */
		 this.hideAll();
		  $("#loginHeading").css("display","").text("Create your free account");
  $("#changeSpan").css("display","").text("Already have an account ? ");
   $("#changeLink").css("display","").text("Sign in");
   $("#contentDiv").css("padding-top","1%");
    $("#loginNote").css("display","none");
   $("#welcome").css("display","none");
   $("#login").css("display","none");
   $("#loginType").css("display","");
     $(".underscoreDiv").css("display","");
	    $("#socialLogin").css("display","");
		   $("#hrLine").css("display","");
     $("#register").css("display","");
	             $('#name').focus();
	 $("#login-manager .statusMesg").html("");
    $("#login-manager .statusMesg").css("display","");
    $("#login-manager .statusMesg1").css("display","none");
    $("#login-manager .statusMesg2").css("display","none");
    $("#login-manager .statusMesg3").css("display","none");
    $(document).prop('title', 'Sign up for a CloudFuze Account');
    window.history.pushState("CloudFuze", "Show signup link", window.location.pathname+"?signup=true");
        };
        LoginManager.prototype.showForgotForm = function () {
         /*  $("#expiredAccount").css("display","none");
            $("#SignUplinktext").css("display","none");
            this.hideAll();
            this.forgotForm.show();
            $('#forgot #email_reset').focus();
            this.signup222.hide();
            this.loginLink.show();
            $(".social-iconstab").css("display","none");
            $("#login-link").css("display","none");
            $("#login-link2").css("display","");
            $("#login-linkal").css("display","none");
            $("#login-link2").html('<a id="login-link" style="color:#000;font-size:10pt"><span style="color:#4082ee;font-weight: bold;" id="textlogin">Login</span></a>');
            return this.registerLink.hide();
		 return true;*/
		 this.hideAll();
		    $("#loginHeading").css("display","").text("Forgot password");
   $("#changeSpan").css("display","").text("Remember your password ? ");
   $("#changeLink").css("display","").text("Sign in");
   $("#loginNote").css("display","none");
   $("#welcome").css("display","none");
   $("#login").css("display","none");
   $("#loginType").css("display","");
     $("#register").css("display","none");
	  $(".underscoreDiv").css("display","none");
	    $("#socialLogin").css("display","none");
		$("#contentDiv").css("padding-top","10%");
   $("#hrLine").css("display","none");
	 $("#forgot").css("display","");
	 $('#forgot #email_reset').focus();
	 window.history.pushState("CloudFuze", "Show forgot password link", window.location.pathname+"?forgotpassword=true");
    sendPageView(window.location.href);
        };
        LoginManager.prototype.showOrangeForm = function () {
 $("#expiredAccount").css("display","none");

            this.hideAll();
            this.orangeForm.show();
            this.loginLink.show();
            $('[name="name"]').focus();
            $('body').find('.span8').html(window.CloudFuze.orangeLanding);
            $('body').removeAttr('style').css('background','#DEEFFE');
            return this.forgotLink.show();
        };
        LoginManager.prototype.hideAll = function () {
            this.loginForm.hide();
            this.confForm.hide();
            this.entLogin.hide();
            this.resendForm.hide();
            this.registerForm.hide();
            this.forgotForm.hide();
            this.orangeForm.hide();
            $('#login-manager .statusMesg').html('');
            $('#login-manager i#resend-link').html('');
            $('#login-manager input').val('');
            this.loginLink.hide();
            this.forgotLink.hide();
            $('#register-link-d,#forgot-link-d,#login-link-d').css('border', 'none');
            $('#login-link-d').css('border-right', '0px solid #000');
            return this.registerLink.hide();
        };
        LoginManager.prototype.getParameterByName = function (name) {
            var regex, regexS, results;
            name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
            regexS = "[\\?&]" + name + "=([^&#]*)";
            regex = new RegExp(regexS);
            results = regex.exec(window.location.search);
            if (results === null) {
                return "";
            } else {
                return decodeURIComponent(results[1].replace(/\+/g, " "));
            }
        };
        return LoginManager;
    })();

    $(function () {
        return new LoginManager($("#login-manager"));
    });

}).call(this);

function showConfForm(checksum) {
    if(checksum == "resend"){
        $('#index-msg').html('<p>The confirmation link successfully sent to your registered email.</p>');
    }
    else if(checksum == "sendPwdLink"){
        $('#changeLink').hide();
        $("#login-wrapper").css("padding","0");
        $('#index-msg').html('<p style="padding:5% 5% 10%;text-align: center;">Please check your email for reset password link.</p><a id="changeLink" href="index.html?login=true" style="color:#1220F6;font-weight:bold;cursor:pointer;margin-left: 12%;text-decoration:none;">Sign in</a><span id="signupForFree""><a id="changeLink" href="index.html?signup=true" style="color:#1220F6;font-weight:bold;cursor:pointer;text-decoration:none;">Sign up</a> now for free</span>');
    }  
    else if(checksum == "signup"){
        $('#index-msg').html('<p>Thank You for Choosing CloudFuze!</p><p>You are just one step away from logging into your CloudFuze Account.We have sent you an e-mail, please confirm your email address.</p>');
    }
	$('#loginHeading').css("display","none");
    $('#login').hide();
    $('#register').hide();
    $('#forgot').hide();
    $('#resendlink').hide();
    $('#loginType').hide();
    $('#confmsg').show();
}

$('#pwd').keydown(function(e) {
    if (e.which == 32) {
        return false;
    }
else if(e.which == 13){
         e.preventDefault();
          $('#register-submit').trigger('click');
           return true;
    }


    else if (e.which == 9000) {
        e.preventDefault();
        var check = $('#terms_conditons').is(':checked');
        var _this = $(this).closest(".login-wrapper");
        var statusMsg = _this.find(".statusMesg");
        var statusMsg1 = _this.find(".statusMesg1");
        var statusMsg2 = _this.find(".statusMesg2");
        var statusMsg3 = _this.find(".statusMesg3");
        var statusMsg4 = _this.find(".statusMesg4");
        var statusMsg5 = _this.find(".statusMesg5");
        var statusMsg6 = _this.find(".statusMesg6");
        statusMsg.html('');
        statusMsg1.html('');
        statusMsg2.html('');
        statusMsg3.html('');
        statusMsg4.html('');
        statusMsg5.html('');
        statusMsg6.html('');
        var _pwd = $("#pwd").val();
        if (_pwd.length < 1) {
            $("#login-manager .statusMesg").css("display","none");
            $("#login-manager .statusMesg1").css("display","none");
            $("#login-manager .statusMesg3").css("display","");
            $("#login-manager .statusMesg3").text("*Please enter password");
            $("#login-manager .statusMesg2").css("display","none");
        }
        else {
            var pwd = cfValidations.validatePwd(_pwd);
            if(pwd == true){


                $("#login-manager .statusMesg").css("display", "none");
                $("#login-manager .statusMesg1").css("display", "none");
                $("#login-manager .statusMesg2").css("display", "none");
                $("#login-manager .statusMesg3").css("display", "none");
                $("#login-manager #terms_conditons").focus();
                $("#login-manager #pwd").focusout();


                /* if(check == false){
                     $("#login-manager .statusMesg").css("display", "");
                     statusMsg.text('*Please agree terms and conditions.');
                     $("#login-manager #terms_conditons").focus();
                     $("#login-manager #pwd").focusout();

                 }*/





            }
            else{
                $("#login-manager .statusMesg3").css("display","");
                statusMsg3.text(pwd);
                $("#pwd").focusout();


            }
            //var statusMsg3 = $(".statusMesg3");



        }
    }
    /* else if(e.which == 13){
         //$('#register-submit').trigger('click');
         return false;
     }*/
});

$('#name').keydown(function(e) {
    if (e.which == 32) {
        return true;
    }
else if(e.which == 13){
         e.preventDefault();
          $('#register-submit').trigger('click');
           return true;
    }

    else if (e.which == 9000 ) {
        e.preventDefault();
        var _this = $(this).closest(".login-wrapper");
        var statusMsg6 = _this.find(".statusMesg6");
        var _name = $("#name").val();
        if(_name.length == 0){
            $("#login-manager .statusMesg").css("display","none");
            $("#login-manager .statusMesg1").css("display"," ");
            $("#login-manager .statusMesg1").text("*Please enter your name");
            $("#login-manager .statusMesg2").css("display","none");
            $("#login-manager .statusMesg3").css("display","none")

        }
        else if(_name.length >=1){
            var _d = cfValidations.name(_name);
            if( _d == true){
                $("#login-manager .statusMesg1").css("display","none");
                $("#login-manager .statusMesg").css("display","none");
                $("#login-manager .statusMesg6").css("display","none");

                $("#name").focusout();
                $(".getemail").focus();

            }
            else{
                $("#login-manager .statusMesg").css("display","none");
                $("#login-manager .statusMesg1").css("display","none");
                $("#login-manager .statusMesg2").css("display","none");
                $("#login-manager .statusMesg6").css("display","");
                statusMsg6.text(_d);
                $("#name").focus();

            }


        }



    }
});


$('.getemail').keydown(function(e){
    if (e.which == 32) {
        return false;
    }
else if(e.which == 13){
         e.preventDefault();
          $('#register-submit').trigger('click');
           return true;
    }
    else if (e.which == 9000){
        e.preventDefault();
        var _this = $(this).closest(".login-wrapper");
        var statusMsg = _this.find(".statusMesg");
        var statusMsg1 = _this.find(".statusMesg1");
        var statusMsg2 = _this.find(".statusMesg2");
        var statusMsg3 = _this.find(".statusMesg3");
        var statusMsg4 = _this.find(".statusMesg4");
        var statusMsg5 = _this.find(".statusMesg5");
        var statusMsg6 = _this.find(".statusMesg6");

        statusMsg.html('');
        statusMsg1.html('');
        statusMsg2.html('');
        statusMsg3.html('');
        statusMsg4.html('');
        statusMsg5.html('');
        statusMsg6.html('');
        _this.find(".recendConf").html('');
        var email = _this.find("#email").val().trim().toLowerCase();
        var pwd = _this.find("input[type='password']").val();
        if (email.length == 0) {
            $("#login-manager .statusMesg4").css("display","none");
            $("#login-manager .statusMesg2").css("display","");
            statusMsg2.text("*Please enter your email.");
            if(cfValidations)
                cfValidations.addAnimation(_this);
            _this.find("#email").focus();
            _this.find("#safariChange").focusout();

        }
        
        else if (email.length >= 1) {
            var _email = cfValidations.email(email);
            if(_email == true){
                $("#login-manager .statusMesg4").css("display","none");
                var _s = cfValidations.searchUser(email);
                if(_s == true){
                    $("#login-manager .statusMesg").css("display","none");
                    $("#login-manager .statusMesg1").css("display","none");
                    $("#login-manager .statusMesg2").css("display","none");
                    $("#login-manager .statusMesg3").css("display","none");
                    $("#login-manager .statusMesg4").css("display","none");
                    $(".getemail").focusout();
                    $("#pwd").focus();
                }
                else {
                    $("#login-manager .statusMesg4").css("display","none");
                    $("#login-manager .statusMesg").css("display","none");
                    $("#login-manager .statusMesg1").css("display","none");
                    $("#login-manager .statusMesg2").css("display","");
                    $("#login-manager .statusMesg3").css("display","none");
                    statusMsg2.text(_s);

                }


            }

            else{
                $("#login-manager .statusMesg2").css("display","");
                statusMsg2.text(_email);

            }
        }

    }
    
});
//safariChange
$('#safariChange').keydown(function(e) {
    if (e.which == 32) {
        return false;
    }
else if(e.which == 13){
         e.preventDefault();
          $('#login-submit').trigger('click');

    }

    else if(e.which == 9000){
        e.preventDefault();
        $('#login-submit').trigger('click');
    }
});
$('.loginEmail').keydown(function(e){
    if (e.which == 32) {
        return false;
    }
    else if(e.which == 9000){
        e.preventDefault();
        var _this = $(this).closest(".login-wrapper");
        var statusMsg = _this.find(".statusMesg");
        var statusMsg1 = _this.find(".statusMesg1");
        statusMsg.html('');
        statusMsg1.html('');
        _this.find(".recendConf").html('');
        var email = _this.find("#email").val().trim().toLowerCase();
        var pwd = _this.find("input[type='password']").val();
        if (email.length == 0) {
                 statusMsg.css("display","")
            //$("#email").css("margin-bottom","0 !important");
            statusMsg.text("*Please enter your email.");
            if(cfValidations)
                cfValidations.addAnimation(_this);
            _this.find("#email").focus();
            _this.find("#safariChange").focusout();
        }
        else if (!window.CloudFuze.emailReg.test(email)) {
              statusMsg.css("display","")
            statusMsg.text("*Please enter a valid email address.");
            _this.find("#email").focus();
            if(cfValidations)
                cfValidations.addAnimation(_this);
        }
        else if (email.length >= 1) {
            var checksum = "loginPage";
            ForgotpasswordAjaxCall.searchUser(email, checksum,undefined);
            
        }

    }
});

$('#login').keypress(function(e){
    if(e.which == 13 || e.which == 9){
        $('#login-submit').trigger('click');
    }
});
$('#register').keypress(function(e){
    if(e.which == 13){
        $('#register-submit').trigger('click');
    }
});
$('#forgot').keypress(function(e){
    if(e.which == 13){
        $('#forgot-submit').trigger('click');
    }
});
$('#resendlink').keypress(function(e){
    if(e.which == 13){
        $('#resend-submit').trigger('click');
    }
});



$('#email_reset').keypress(function(e){

    if(e.which == 13 ){
        $('#forgot-submit').trigger('click');
        return false;
    }

    else if ( e.which == 9){
        $('#forgot-submit').trigger('click');
    }
    else if (e.which == 32) {
        return false;
    }
});
$('#entLogin').keypress(function(e){
    if(e.which == 13 || e.which == 9){
        $('#ent-submit').trigger('click');
        return false;
    }
});
$('#login-link-d').on('click',function(){
    $("#login-manager .statusMesg").css("display","none");
    $("#login-manager .statusMesg1").css("display","none");
    $("#login-manager .statusMesg2").css("display","none");
    $("#login-manager .statusMesg3").css("display","none");
    $("#login-manager .statusMesg4").css("display","none");
    $("#login-manager .statusMesg5").css("display","none");
    $("#login-manager .statusMesg6").css("display","none");
    $(document).prop('title', 'Login to Your CloudFuze Account');
    window.history.pushState("CloudFuze", "Show login link", window.location.pathname+"?login=true");
    sendPageView(window.location.href);


    //_gaq.push(['_trackPageview', window.location.href]);
});
$('#login-link').on('click',function(){
    $("#login-manager .statusMesg").css("display","none");
    $("#login-manager .statusMesg1").css("display","none");
    $("#login-manager .statusMesg2").css("display","none");
    $("#login-manager .statusMesg3").css("display","none");
    $("#login-manager .statusMesg4").css("display","none");
    $("#login-manager .statusMesg5").css("display","none");
    $("#login-manager .statusMesg6").css("display","none");
    $(document).prop('title', 'Login to Your CloudFuze Account');
    window.history.pushState("CloudFuze", "Show login link", window.location.pathname+"?login=true");
    sendPageView(window.location.href);
    //_gaq.push(['_trackPageview', window.location.href]);
});
$('#login-link-d').on('click',function(){
    $(document).prop('title', 'Login to Your CloudFuze Account');
    window.history.pushState("CloudFuze", "Show login link", window.location.pathname+"?login=true");
    sendPageView(window.location.href);
    //_gaq.push(['_trackPageview', window.location.href]);
});
$('#forgot-link').on('click',function(){
    window.history.pushState("CloudFuze", "Show forgot password link", window.location.pathname+"?forgotpassword=true");
    sendPageView(window.location.href);
    //_gaq.push(['_trackPageview', window.location.href]);
});
$('#register-link').on('click',function(){

    $("#login-manager .statusMesg").html("");
    $("#login-manager .statusMesg").css("display","");
    $("#login-manager .statusMesg1").css("display","none");
    $("#login-manager .statusMesg2").css("display","none");
    $("#login-manager .statusMesg3").css("display","none");
    $(document).prop('title', 'Sign up for a CloudFuze Account');
    window.history.pushState("CloudFuze", "Show signup link", window.location.pathname+"?signup=true");
//    sendPageView(window.location.href);
    //_gaq.push(['_trackPageview', window.location.href]);
});
$('#loginWrapperFooter').on('click','a',function(){
    $('#login').removeClass('loginHide');
});
$('#ent-submit').on('click',function(e){
    e.preventDefault();
    var subdomain = $(this).parent().find('#domain').val().toLowerCase();
    if(subdomain.trim().length > 1){
        var api = null;
        var isDev = /devweb/i.test(domainUrl);
        var isSl = /slweb/i.test(domainUrl);
        if(isDev){
            api = subdomain+".devservices.api.cloudfuze.com";
        }else if(isSl){
            api = subdomain+".slservices.api.cloudfuze.com";
        }else{
            api = subdomain+".apis.cloudfuze.com";
        }
        $.ajax({
            type: "GET",
            url: apicallurl + "/subdomain/find?subdomain="+api,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success:function(data){

            },
            complete:function(xhr, statusText){
                if(xhr.status == 200){
                    //var url = "https://"+subdomain+"."+defaultDomain.split('//')[1];
                    window.location.href = "https://"+subdomain+"."+defaultDomain.split('//')[1];
                }else{
                    $('#entLogin').find('.statusMesg').text('Domain is not registered with us.');
                }
            }
        });
    }
    else{
        $('#entLogin').find('.statusMesg').text('Please enter domain name.');
        return false;
    }
});
$("#_orangeC").on("click",function() {
    var object = {};
    var _this = $(this).closest('.orange-signup');
    object.name = _this.find('[name="name"]').val().trim();
    object.email = _this.find('[name="email"]').val().trim().toLowerCase();
    object.pwd = _this.find('[name="password"]').val();
    object.cpwd = _this.find('[name="cpassword"]').val();
    var check = _this.find('[name="terms_conditons"]').is(':checked');
    var statusMsg = _this.find('#errormsg');
    var _a = [];
    if(object.name.length < 1){
        _a.push('name');
    }
    if(object.email.length < 1){
        _a.push('email');
    }
    if(object.pwd == ""){
        _a.push('password')
    }
    if(object.cpwd == ""){
        _a.push('confirm password');
    }
    if(_a.length > 1){
        statusMsg.text('Please enter all required fields.')
    }
    else if(_a.length == 1){
        statusMsg.text('Please enter '+_a[0]+'.');
    }
    else{
        statusMsg.text('');
        var _name = cfValidations.name(object.name);
        if(_name == true){
            var _email = cfValidations.email(object.email);
            if(_email == true){
                var _s = cfValidations.searchUser(object.email);
                if(_s == true){
                    var _pwd = cfValidations.password(object.pwd,object.cpwd);
                    if(_pwd == true){
                        window.CloudFuze.orangeDetails = object;
                        _this.hide().next().show();
                    }else{
                        $("#login-manager .statusMesg3").css("display","");
                        statusMsg3.text(_pwd);
                    }
                }else{
                    statusMsg.text(_s);
                }
            }else{
                statusMsg.text(_email);
            }
        }else{
            statusMsg.text(_name);
        }
    }
});

$(".orange-plans").find("a.button").on('click',function(){
    var _v = window.CloudFuze.orangeDetails;
    _v.plan = $(this).attr('data');
    _v.type = $(this).attr('mode');
    _v.instance = "ORANGE";
    var _a = $('.orange-user-details');
    _a.find('td.name').text(_v.name);
    _a.find('td.email').text(_v.email);
    _a.find('span.plan').text(_v.plan);
    _a.find('span.type').text(_v.type);
    $(this).closest('.orange-plans').hide().next().show();
});

$('.orange-user-details').find('.change-plan').on('click',function(){
    $(this).closest('.orange-user-details').hide().prev().show();
});
$('.orange-user-details').find('button').on('click',function(){
    var _a = {
        "code":"EUR",
        "amount":window.CloudFuze.orangeDetails.plan,
        "success":"orangesuccess",
        "cancel":"orangecancel"
    };
    window.CloudFuze.orangeDetails.enable = true;
    var _b = loginAjaxCall.signup(window.CloudFuze.orangeDetails);
    if(_b != "failed"){
        $(this).text('Please wait...');
        localStorage.setItem('UserId',_b.id);
        localStorage.setItem('UserName',_b.lastName);
        localStorage.setItem('CFUser',JSON.stringify(_b));
        localStorage.setItem("UserAuthDetails",loginAjaxCall.BasicAuth(_b.primaryEmail,CryptoJS.MD5(window.CloudFuze.orangeDetails.pwd)));
        if(window.CloudFuze.orangeDetails.type == "monthly"){
            localStorage.setItem('MonthlyAMNT', MonthlyAMT);
            CFPaymentsPaypal.createPmntMnthlyRecurring(_a);
        }
        else{
            localStorage.setItem('YearlyAMNT', YearlyAMT);
            CFPaymentsPaypal.createPmntAnnualRecurring(_a);
        }
    }

});

$('#_cfheader').on('click',function(){
    window.open('','_new').location.href='https://www.cloudfuze.com';
});

$('#_cfheader').hover(function() {
    $(this).css('cursor','pointer');
});

