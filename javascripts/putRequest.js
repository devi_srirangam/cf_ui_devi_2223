/**
 * Created by CloudFuze on 12/1/2014.
 */
//var jsonData = data;
function showNotyNotification(type,msg) {
    if (type == 'notify')
        type='success';
    else if (type == 'warn')
        type='warning';
    noty({
        text: msg,
        layout: 'topRight',
        closeWith: ['button','click'],
        timeout: 13000,
        type: type,//'information-b', 'error-r', 'warning-y', 'success-g';
        maxVisible: 3,
    });
    //$('#noty_topRight_layout_container').css('right','20px');
    $('#noty_topRight_layout_container li').css({'height':'40px'});
    $('#noty_topRight_layout_container .noty_close').css({"background":"url('../img/PNG/cancel.png')","display":"block"});
}
localStorage.removeItem('cfObject');
var ajaxCall = {
    cloudput : function(url,data){
        var _method;
        if((data.cloudName === "ONEDRIVE_BUSINESS_ADMIN")||(data.cloudName === "MICROSOFT_TEAMS")||(data.cloudName === "ONEDRIVE_BUSINESS_ADMIN_HYBRID")||(data.cloudName === "DROPBOX_BUSINESS")||(data.cloudName === "BOX_BUSINESS")||(data.cloudName === "SHAREPOINT_ONLINE_BUSINESS")){
            _method = 'POST';
        }
        else{
            _method = 'PUT';
        }
        $.ajax({
            type:_method ,
            url: decodeURIComponent(url),
            headers: {
                "Content-Type": "application/json",
                "Authorization": localStorage.getItem("UserAuthDetails"),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE"
            },
            data: JSON.stringify(data),
            dataType: 'json',
            success: function (cloudObj) {
                if(cloudObj.cloudName == 'DROPBOX_BUSINESS'){
                    localStorage.setItem('multiuser',cloudObj.emailId);
                }
                closebox("Completed",data,cloudObj);
            },
            complete:function(xhr, statusText){
                if(xhr.status == 409){
                    localStorage.setItem("OauthProcess", "alreadyExist");
                    closebox("alreadyExist",data);
                }else if(xhr.status == 403){
                    if(xhr.getResponseHeader('exception') != undefined){
                        localStorage.setItem("OauthProcess", "restricted");
                        closebox("restricted",data);
                    }
                    else{
                        localStorage.setItem("OauthProcess", "failed");
                        closebox("failed",data);
                    }
                }else if(xhr.status == 500 && data.cloudName=='BOX_BUSINESS'){
                    localStorage.setItem("OauthProcess", "notadmin");
                    closebox("notadmin",data);
                }
                else if(xhr.status > 300){
                    localStorage.setItem("OauthProcess", "failed");
                    closebox("failed",data);
                }
                else if(xhr.status == 200){
if(localStorage.getItem('CFUser') != undefined && localStorage.getItem('CFUser') != null){ 
					activecampaign.eventTrack(data.cloudName,JSON.parse(localStorage.CFUser).primaryEmail,'Cloud Added');
                    }
if((data.cloudName === "ONEDRIVE_BUSINESS_ADMIN")){
                        var email = data.cloudUserId.split('|')[1];
                        localStorage.setItem("multiuser", email);
                    }
                    if((data.cloudName === "ONEDRIVE_BUSINESS_ADMIN")||(data.cloudName === "MICROSOFT_TEAMS")||(data.cloudName === "ONEDRIVE_BUSINESS_ADMIN_HYBRID")||(data.cloudName === "DROPBOX_BUSINESS")||(data.cloudName === "BOX_BUSINESS")||(data.cloudName === "SHAREPOINT_ONLINE_BUSINESS")){
						if(data.cloudName === "BOX_BUSINESS"){
						$('#noty_topRight_layout_container li').css({'height':'70px',"display":"block"});
    $('#noty_topRight_layout_container .noty_close').css({"background":"url('../img/PNG/cancel.png')","display":"block"});
						}
			    localStorage.setItem("OauthProcess", status);
                        closebox("Completed",data,cloudObj);//localStorage.setItem("OauthProcess", status);
                    }
                    else{
                        status="Completed";
                        localStorage.setItem("OauthProcess", status);
                        parent.window.close();
                    }
                }
            }
        });
    },
    search : function(data){
        data = Base64.decode(data);
        data = JSON.parse(data);
        $.ajax({
            type: "GET",
            url: data.url+""+data.userName,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success:function(data, textStatus, xhr) {

            }
        });
    },
    login : function(data){
        data = Base64.decode(data);
        data.password = CryptoJS.MD5(data.password);
        $.ajax({
            type: 'PUT',
            url: decodeURIComponent(url),
            headers: {
                "Content-Type": "application/json",
                "Authorization": ajaxCall.BasicAuth(data.email,data.password),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE"
            },
            dataType: 'json',
            success: function (cloudObj) {
            }
        });
    },
    BasicAuth:function(username, password) {//TODO move this function to a common utility file - Mike
        var tok = username + ':' + password;
        var hash = Base64.encode(tok);
        return "Basic " + hash;
    }//TODO resolve error - Mike  :  Primitive value returned from constructor will be lost when called with 'new'
};

var urlParams;
(window.onpopstate = function () {
    var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) {
            return decodeURIComponent(s.replace(pl, " "));
        },
        query = window.location.search.substring(1);
    urlParams = {};
    while (match = search.exec(query)) {
        urlParams[decode(match[1])] = decode(match[2]);
    }
    if(urlParams.hasOwnProperty("data")){
        var data = JSON.parse(Base64.decode(urlParams["data"])),
            _url = data['putUrl'];
        delete data['putUrl'];
        ajaxCall.cloudput(_url,data);
    }else if(urlParams.hasOwnProperty("login")){
        var login = window.location.search.split('?')[1].split('=')[1];
    }else if(urlParams.hasOwnProperty('search')){
        ajaxCall.search(decodeURIComponent(urlParams["search"]));
    }
})();

function closebox(status,data,cloudObj) {
    if (typeof (Storage) !== "undefined") {
        $('#processStatemsg').text("operation " + status);
        if(data.cloudName == 'CISCO_SPARK'){
            localStorage.setItem("OauthCloud", 'CISCO_SPARK');
        }
        if(status == "Completed" && (cloudObj.cloudName === "ONEDRIVE_BUSINESS_ADMIN"||cloudObj.cloudName === "MICROSOFT_TEAMS"||cloudObj.cloudName === "ONEDRIVE_BUSINESS_ADMIN_HYBRID"|| cloudObj.cloudName === "DROPBOX_BUSINESS"|| cloudObj.cloudName === "BOX_BUSINESS"|| cloudObj.cloudName === "SHAREPOINT_ONLINE_BUSINESS")){
            $.ajax({
                type: 'GET',
                url: window.location.origin+'/proxyservices/v1'+"/users/"+localStorage.getItem("UserId")+"/"+cloudObj.id+"/status",
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": localStorage.getItem("UserAuthDetails"),
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE"
                },
                success: function (data) {
                    if(data.cloudAddingStatus == true){
                        localStorage.setItem("OauthProcess", status);
                        parent.window.close();
                    }
                    else{
                        setInterval(closebox(status,data,cloudObj),5000);
                    }
                },
                complete:function(xhr, statusText){
                    if(xhr.status == 500 || xhr.status === 401){
                        // localStorage.setItem("OauthProcess", "failed");
                        parent.window.close();
                    }
                }
            })
        }
        else{
            parent.window.close();
        }

    }else {
        window.location.href = domainURL + "/oauth/closebox.html?status=" + status;
    }
}