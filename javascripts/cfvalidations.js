var cfValidations = {
    name : function(a){
        var user = /[^a-zA-Z ]/;
        var userlen = /[^a-zA-Z ]{2,20}/;
        if(user.test(a)){
            return "*Name can contain only letters (A-Z and a-z).";
        }
        //else if(userlen.test(a)){
        else if(a.length == 0){

            return "*Please enter name";
        }
        else if(a.length < 2 || a.length > 20){

            return "*Name must be 2-20 characters in length.";
        }
        else{
            return true;
        }
    },
    email : function(b){
        var email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,6})?$/;
        if(!email.test(b)){
            return "*Please enter a valid email address."
        }
        else if(b.length == 0){

            return "*Please enter your email";
        }
        else{
            return true;
        }
    },
    password: function(c,d){
        if(c.length < 6 || c.length > 20){
            return "*Password must be 6-20 characters in length.";
        }
        else if(c != d){
            return "*Password and confirmation of password do not match. Please reenter.";
        }
        else{
            return true;
        }
    },
    validatePwd : function (c) {
        if(c.length < 6 || c.length > 20){
            return "*Password must be 6-20 characters in length.";
        }
        else{
            return true;
        }
    },
    searchUser : function(b){
        var _b = '';
        $.ajax({
            type: "GET",
            url: apicallurl + "/users/validate?searchUser="+b,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                if (data != "") {
                    return _b =  "*The email address is already registered with us. Please use a different account.";
                }
                else{
                    return _b = true;
                }
            }
        });
        return _b;
    },
    addAnimation:function(_this){
        _this.addClass("wobble");
        return _this.bind("webkitAnimationEnd animationEnd mozAnimationEnd", function () {
            _this.off("webkitAnimationEnd");
            return _this.removeClass("wobble");
        });
    },
    getUserCurrency:function(e) {
        var _a = 'USD';
        $.ajax({
            url: domainUrl + "proxy/currencycode/currency.json",
            type: "get",
            async: false,
            success: function (f) {
                if(e.country!== null)
                    return _a = f[e.country];
            }
        });
        return _a;
    }
};