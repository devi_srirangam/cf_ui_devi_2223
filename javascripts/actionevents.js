var refreshCloud = [];
var CFManageCloudAccountsAjaxCall1 = {

    //CloudFuze Users
    getAuthDetails: function () {
        var dt = new Date();
        var time = dt.getTime();
        if (localStorage.getItem("time") != null) {
            // session expires on 2 hours mentioned in milliseconds.
            if (time - localStorage.getItem("time") >= 7200000) {
                // $.smallBox({
                //     title: "Your session has expired.  Please log in again.",
                //     color: "#e35e00",
                //     timeout: notifyTime
                // });
                alertError("Your session has expired.  Please log in again.");
                setTimeout(function () {
                    $('.cf-logout').trigger("click");
                    window.location.href = "index.html";
                }, 2000);
            }
        }
        localStorage.setItem("time", time);
        $.ajaxSetup({
            cache: false,
            error: function (jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    var a = window.location.pathname;
                    var b = /cloudmanager/i.test(a);
                    if (b) {
                        return false;
                    }
                    else if (PageName == "InnerFolders" && previousPage == "Share with Me") {
                        return false
                    }
		else if (localStorage.getItem("zohoError")) {
                        return false;
                    }
			 else {
                        var notification = {};
                        if (localStorage.getItem('UserAuthDetails') == undefined
                            || localStorage.getItem('UserAuthDetails') == null
                            || localStorage.getItem('UserAuthDetails') == 'null') {
                            notification.title = "You have been logged out";
                        } else {
                            notification.title = "Your password was changed.Please relogin";
                        }
                        notification.color = "#a4c400";
                        notification.timeout = notifyTime;
                        //$.smallBox(notification);
                        alertSuccess(notification.title);
                        setTimeout(function () {
                            $('.cf-logout').trigger("click");
                            window.location.href = "index.html";
                        }, 3000);
                    }
                } else if (jqXHR.status === 503) {
                    //$.smallBox({title: "Server is under maintanance.", color: "#a4c400", timeout: notifyTime});
                    alertSuccess("Server is under maintanance.");
                }
            }
        });
        return localStorage.getItem("UserAuthDetails");
    },
};
function showNotyNotification(type,msg) {
    if (type == 'notify')
        type='success';
    else if (type == 'warn')
        type='warning';
    noty({
        text: msg,
        layout: 'topRight',
        closeWith: ['button','click'],
        timeout: 13000,
        type: type,
        maxVisible: 3,
    });
    $('#noty_topRight_layout_container li').css({'height':'40px'});
    $('#noty_topRight_layout_container .noty_close').css({"background":"url('../img/PNG/cancel.png')","display":"block"});
}
var CFSubscriptionsInternal1= {
    GetlastSubscriptioninfo : function (){
        var apiUrl= apicallurl + "/subscription/all/"+localStorage.getItem('UserId');
        var subsresponse1;
        $.ajax({
            type: "GET",
            url: apiUrl,
            async: false,
            dataType: "application/json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall1.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                subsresponse1 = data;
            },

            complete: function (xhr, statusText) {
                if (xhr.status == 200) {
                    subsresponse = xhr.responseText;
                    subsresponse = jQuery.parseJSON(subsresponse);
                }
                if (xhr.status > 300) {
                }
            }
        });
        if(subsresponse !== undefined)
            return subsresponse[0] ;
    },

};


function checkIsaCustomUserForLogin(_userId) {
    var valid = false;
    var _url = apicallurl + "/report/entuser/validate/"+_userId;//+localStorage.getItem("UserId");
    $.ajax({
        type: "GET",
        url: _url,
        async: false,
        headers: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (result) {
            //alert("@ checkIsaCustomUser result22 = "+result);
            if(result == true){
                valid = true;
            }else{
                valid = false;
            }
        }
    });
    return valid;
}

var dashboarduname = localStorage != null ? localStorage.getItem('UserName') : '';
var ver='';
lastsubsresponse = CFSubscriptionsInternal1.GetlastSubscriptioninfo();
var isCusUser = checkIsaCustomUserForLogin(localStorage.getItem("UserId"));
if (lastsubsresponse == undefined && isCusUser === false)
    ver="(Trial User)";
$('#dash-uname').text(dashboarduname+ver);
$('#dash-uname1').text(dashboarduname+ver);
if($('#dash-uname').text().length > 10){
$(".ml-auto1").css("margin-left","5%");
$(".dropdown-content").css("left","10%");
}else{
$(".ml-auto1").css("margin-left","15%"); 
$(".dropdown-content").css("left","-15%");
}
$('#categorytogle').siblings('ul').children('li').slideToggle('slow');
$('ul.clsubmenu:visible').slideToggle('slow');
$('#cm-addcloud').on('click',function(){
    $('#addclouddiv').removeClass('divdisplay');
    $('#manageclouddiv').addClass('divdisplay');
});
$('#primary li').on('click',function(){
    localStorage.removeItem("movelocation");
});
$('#cm-managecloud').on('click',function(){
    $('#manageclouddiv').removeClass('divdisplay');
    $('#addclouddiv').addClass('divdisplay');
});
$('#CloudDriveList').on('click','.cf-refresh4',function(){
    $(this).removeClass('cf-refresh4').addClass('cloudSpinn');
    var id = $(this).parent().attr('id');
    sendGAEvents("Clicked on Cloud Sync",id);
	activecampaign.eventTrack('Cloud Sync',JSON.parse(localStorage.CFUser).primaryEmail,'clicked');
    //_gaq.push(['_trackEvent',  , localStorage.getItem('UserId'),id]);
    refreshCloud.push(id);
    CFManageCloudAccountsAjaxCall.refreshcloud(id);
});
$('.secondary-nav-menu > li').on('click',function(){
    $('.secondary-nav-menu > li').removeClass('active');
    $('#CFHomeSearch').val('');
    var _this = $(this).siblings('li');
    var _thisChild = _this.children('#CloudDriveList').children('li');
    //$('#listShowMore').addClass('divdisplay');
    $(this).addClass('active');
    //_this.children('ul.submenu:visible').slideUp(400);
    //_thisChild.children('ul.clsubmenu:visible').slideUp(200);
    //_thisChild.children('a.trigactive').removeClass('trigactive');
    if(sessionStorage.getItem('clouds') == "false"){
        var _a = $(this).children('a').attr('id');
        if(_a == "CFSharedWithMe"){
            $('#CF_no_clouds').hide();
        }
        else if(_a == "categorytogle" || _a == "mycloudstogle"){
            if(_a == "mycloudstogle"){
                sendGAEvents("All Files");
                //_gaq.push(['_trackEvent', "All Files" , localStorage.getItem('UserId')]);
            }
            else if(_a == "categorytogle"){
                sendGAEvents("Category");
                //_gaq.push(['_trackEvent', "Category" , localStorage.getItem('UserId')]);
            }
        }
        else {
            $('#CF_no_clouds').show();
        }
    }else{
        $('#CF_no_clouds').hide();
    }
});
$("a.trigger").click(function(e) {
    e.preventDefault();
    $(this).next("ul.submenu").slideToggle(500).parent().siblings('li').children('ul.submenu:visible').slideUp(400);
    var _this = $(this).siblings('ul').children('li');
    _this.children('a').removeClass('trigactive');
    _this.children('ul.clsubmenu:visible').slideUp(100);
});
$('#CloudDriveList').on('click','a.cltrigger',function(e) {
    e.preventDefault();
    $(this).addClass('trigactive').parent().siblings('li').children('a').removeClass('trigactive');
    $(this).next("ul.clsubmenu").slideToggle(500).parent().siblings('li').children('ul.clsubmenu:visible').slideUp(400);
    $(this).siblings('ul').children('li').removeClass('catactive');
});
$('#CloudDriveList').on('click','li.getCloudFiles>p',function(e) {
    //$('li.getCloudFiles').parent().prev('a').removeClass('trigactive');
    $('#CloudDriveList').find('.catactive').removeClass('catactive');
    $(this).parent().addClass('catactive');
    PageName = "CloudDrive";
    $('#CFHDelete .filecontrol-title').text('Delete');
    $('#CFHDelete').attr('title','Delete');
    document.title="Home";
    CFHideContents();
    InnerCloudId = [];
    InnerFolderId = [];
    CFShowContents();
    $('#headerText').text("My Clouds");
    SingleCloudId=$(this).parent().attr('id');
    SinglePId=$(this).parent().attr('pid');
    PageNumber=1;
    sendGAEvents("Get Cloud Files",SingleCloudId);
    //_gaq.push(['_trackEvent', "Get Cloud Files" , localStorage.getItem('UserId'),SingleCloudId]);
    $('#breadCrumbdync').empty();
    $('#breadCrumbdync').append('<li id="cloud" pid="'+SingleCloudId+'" class="BCRFList" cloudId="cloud"><a href="#" style="color:blue;text-decoration: underline;cursor:default;">My Clouds </a><span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a href="#"> '+$(this).text()+'</a></li>');
    $('#spinner1').removeClass('divdisplay');
    $('#'+viewTrack+'').trigger('click');
    setTimeout(function() {
        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(SingleCloudId, PageNumber);
    },100);
});
$('#categoryList').on('click','li.getFiles > a',function(e){
    e.preventDefault();
    if(sessionStorage.getItem('clouds') == "false"){
        $('#CF_no_clouds').show();
    }else{
        $('#CF_no_clouds').hide();
    }
    PageName = "Category";
    count=1;
    PageNumber=1;
    CFHideContents();
    CFShowContents();
    var catid = $(this).parent().attr('id');
    sendGAEvents("Get Category Files",catid);
    //_gaq.push(['_trackEvent', "Get Category Files" , localStorage.getItem('UserId'),catid]);
    var catname=$(this).text();
    categoryName=catname;
    $('#CFHDelete .filecontrol-title').text('Remove');
    $('#CFHDelete').attr('title','Remove');
    $('#breadCrumbdync').empty();
    $('#breadCrumbdync').append('<li id='+catname+' class="BCRFList" cloudId='+catname+'><a href="#" style="color:blue;text-decoration: underline;cursor:default;">Category </a><span class="divider" style=" font-size: 8pt;padding-left: 5px;">&#9658;</span><a href="#"> '+catname+'</a></li>');
    $('#headerText').text("Category");
    $('#spinner1').removeClass('divdisplay');
    setTimeout(function(){
        CFManageCategoryAjaxCall.getFilesForCategory(catid,PageNumber);
    },100);
    $(this).parent().addClass('catactive').siblings('li').removeClass('catactive');
    $('#'+viewTrack+'').trigger('click');
});
$('.cf-logout').on('click',function(e){
	alertSuccess("You have been logged out");
	    setTimeout(function(){
    // CFManageCloudAccountsAjaxCall.getAllMoveStatusForUser();
	activecampaign.eventTrack('Logout',JSON.parse(localStorage.CFUser).primaryEmail,'clicked'); 
    if(localStorage.getItem("MigtnDone") === "consumer" || localStorage.getItem("MigtnDone") === "multiUser"){

    }
    else{
     //   zohoCrm.upgradeSubscribtionRecord(JSON.parse(localStorage.CFUser).primaryEmail,'logout',"","","","");
	   activecampaign.getContact(JSON.parse(localStorage.CFUser).primaryEmail,'userBounced',"Yes");
    }
 if (localStorage.getItem("socialLogin") === "gloginSoc"){
signOut();
       function signOut(){
var auth2 = gapi.auth2.getAuthInstance();
         auth2.signOut().then(function () {
         console.log('User signed out.');
        });
};
      }
console.log('User signed out normally.');
    sendGAEvents("clicked on logout button");
    //_gaq.push(['_trackEvent', "LogOut" , localStorage.getItem('UserId')]);

	e.preventDefault();
    localStorage.clear();
    sessionStorage.clear();
    window.location.href="index.html";
	},1000);
});
$('#cancelcategory').on('click',function(){
    sendGAEvents("Cancel Category","Create Category");
    //_gaq.push(['_trackEvent', "Cancel Category" , localStorage.getItem('UserId'),"Create Category"]);
    $('#page-header input').prop('checked',false);
    disableActionPanel(actionPanel);
});
var workid= [];
var workname= [];
$('#addToWorkspace').on('click',function() {
    sendGAEvents("Add Files to Workspace");
    //_gaq.push(['_trackEvent', "Add Files to Workspace", localStorage.getItem('UserId')]);
    $('#addtoworklist').css('border-color', '');
    $('#selectWSEmpty').text('');
    $('#addtoworklist').children('option:eq(0)').nextAll().remove();
    var apiUrl = apicallurl + "/workspace/user";
    $.ajax({
        type: "GET", url: apiUrl, async: false, dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (userWorspace) {
            $.each(userWorspace, function (i, work) {
                $('#addtoworklist').append('<option id="' + work.id + '">' + work.workspaceName + '</option>');
                workid.push(work.id);
                workname.push(work.workspaceName);
            });
        }
    });
    $('#myworkspaceModel').modal();
});
$('#addtoworkspace').on('click',function(e){
    e.preventDefault();
    var workname1 = $('#addtoworklist').val();
    if(workname1 == "Select Workspace"){
        $('#addtoworklist').css('border-color','red');
        $('#selectWSEmpty').text("Please select workspace");
        return false
    }else {
        sendGAEvents("Add Files to Workspace");
        //_gaq.push(['_trackEvent', "Add Files to Workspace" , localStorage.getItem('UserId')]);
        $('#addtoworklist').css('border-color', '');
        var test = $.inArray(workname1, workname);
        var workid1 = workid[test];
        $('#cancelWorkspace').trigger('click');
        CFManageCloudAccountsAjaxCall.addFilesToWorkspace(workid1, FromfileId);
    }
});
$(document).ready(function () {
    var UserObj = JSON.parse(localStorage.getItem('CFUser'));
    if(UserObj == undefined || UserObj == null){
        window.location = "index.html";
        return false;
    }
    var _email=UserObj.primaryEmail;
    $.ajax({
        type: "GET",
        url:apicallurl+"/users/validate?searchUser="+_email,
        async: false,
        dataType: "json",
        headers:{
            "Content-Type":"application/json",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function(data, textStatus, xhr){
            if(data == undefined || data == null || data.length == 0){
                return false;
            }
	    else if (data.length !=0){
		if(data[0].expiresIn == undefined){
		return false; 
		}
	    } 
            var _sdate = CFInterConvertTimeUtility.Ptstamptomillsec(xhr.getResponseHeader("Date")),
                _exp = data[0].expiresIn,
                _orange = data[0].instance, 
                _html ="<div  style='font-size: 13pt;text-align: center;color: #FF4C4C;background: #FFF0F0;' id='expireMsg'><span>",
                _h = "Please <a href='settings.html#price?pinfo=true'>subscribe</a> to the paid plan to continue the migration.</span></div>",
                _target = $('#main'),
                _c = false;
            if(_c == false){//_orange == "ORANGE"){
                var isPaid = CFManageCloudAccountsAjaxCall.isPaidUser(); 
                if(CFManageCloudAccountsAjaxCall.isCustomUser()){ 
                    return true;
                } 
		    else if(_exp > _sdate){
                _c = checkExpire.goingToExpire(_exp,_sdate,isPaid);
                if(_c != false){
                    _html += _c +'. '+ _h;
                    return _target.prepend(_html);
                }
				else{
					_html = ""; 
				 return _target.prepend(_html);
				}
				
            }
            else if(_exp < _sdate){
                _c = checkExpire.expired(_exp,_sdate,isPaid);
				
                _html += _c +'. '+ _h;
                _target.prepend(_html);
                if(window.location.href === domainUrl + "pages/checkout.html")
                    return false;
                else
                    return window.location.href="../pages/settings.html#price?pinfo=true";
            }
          /*      checkIsaCustomUserForLogin();
                if(isPaid == false){
                    if(window.location.href === domainUrl + "pages/checkout.html")
                        return false;
                    else
                        return window.location.href="../pages/settings.html#price?pinfo=true";
                }
                else{
                    if(_exp < _sdate){
                        _c = checkExpire.expired(_exp,_sdate);
                        _html += _c +'. '+ _h;
                        _target.prepend(_html);
                        return window.location.href="../pages/settings.html#price?pinfo=true";
                    }
                    else if(_exp > _sdate){
                        return true;
                    }
                }*/
            }
            else if(_exp > _sdate){
                _c = checkExpire.goingToExpire(_exp,_sdate);
                if(_c != false){
                    _html += _c +'. '+ _h;
                    return _target.prepend(_html);
                }
				else{
					_html = "";
                    return _target.prepend(_html);
				}
            }
            else if(_exp < _sdate){
                _c = checkExpire.expired(_exp,_sdate);
                _html += _c +'. '+ _h;
                _target.prepend(_html);
                if(window.location.href === domainUrl + "pages/checkout.html")
                    return false;
                else
                    return window.location.href="../pages/settings.html#price?pinfo=true";
            }
        }
    });
});
$('#moveReportsRefresh').on('click',function(){
    PageNumber = 1;
    $('#moveReports').trigger('click');
});
$('#moveSyncReportsRefresh').on('click',function(){
    PageNumber = 1;
    $('#moveSyncReports').trigger('click');
});
$('#showMoreReports').on('click',function(){
    sendGAEvents("Show More Move Reports");
    //_gaq.push(['_trackEvent', "Show More Move Reports" , localStorage.getItem('UserId')]);
    PageNumber = PageNumber + 1;
    $('#spinner2').show();
    $('#showMoreReports').text('Fetching reports..');
    CFManageCloudAccountsAjaxCall.getAllMoveStatusForUser();
    CFManageCloudAccountsAjaxCall.getAllMoveStatusForSyncUser();
});
$('#moveReports').on('click',function(e){
    sendGAEvents("Show Move Reports");
    $("#movePageMultiUsr").css("overflow-y","hidden");
    //_gaq.push(['_trackEvent', "Show Move Reports" , localStorage.getItem('UserId')]);
    e.preventDefault();
    PageNumber = 1;
    $('#headerText').text('Migration Reports');
    localStorage.setItem("movelocation","schedules");
    $('#move-header > div').hide();
    $('#topHeader').show();
    $('#moveReportsContainer').show();
    $('#FooterLinks').show();
    $('#CFShowLoading').show();
    CFManageCloudAccountsAjaxCall.getAllMoveStatusForUser();
	setTimeout(function(){
    $("#CFShowLoading").hide().css('display','none');
	},1000);
});
$('#moveSyncReports').on('click',function(e){
    sendGAEvents("Show Move Reports");
    $("#movePageMultiUsr").css("overflow-y","hidden");
    //_gaq.push(['_trackEvent', "Show Move Reports" , localStorage.getItem('UserId')]);
    e.preventDefault();
    PageNumber = 1;
    $('#headerText').text('Sync Reports');
    localStorage.setItem("movelocation","schedules");
    $('#move-header > div').hide();
    $('#topHeader').show();
    $('#moveSyncReportsContainer').show();
    $('#FooterLinks').show();
    $('#CFShowLoading').show();
    CFManageCloudAccountsAjaxCall.getAllMoveStatusForSyncUser()
   setTimeout(function(){
    $("#CFShowLoading").hide().css('display','none');
	},1000);
});
$('#initiateNewMove').on('click',function(e){ 
    sendGAEvents("Initiate new Move");
    //_gaq.push(['_trackEvent', "Initiate new Move" , localStorage.getItem('UserId')]);
    localStorage.setItem("movelocation","initiatemove");
    e.preventDefault();
    location.reload();
});
$('#moveReportsForUser').on('click','#getMoveDetails',function(e){
    e.preventDefault();
    var moveId = $(this).attr('mid');
    if($(this).children('i').hasClass("cf-minus5"))
    {
        $("#"+moveId).children("#getMoveDetails").children('i').removeClass("cf-minus5").removeClass("fa fa-chevron-circle-up").addClass("cf-plus5").addClass('lnil lnil-chevron-down-circle').css("font-size","18px");
        $("#moveReportsForUser .hide-report1").hide();
        $("#moveReportsForUser .hide-report2").hide();
        return false;
    }
    $("#moveReportsForUser .hide-report1").hide();
    $("#moveReportsForUser .hide-report2").hide();
    $('#CFShowLoading').show();
    $("#getMoveDetails i").removeClass("cf-minus5").removeClass("fa fa-chevron-circle-up").addClass("cf-plus5").addClass('lnil lnil-chevron-down-circle').css("font-size","18px");
    if(!$("#"+moveId).children('i').hasClass("cf-plus5disabled"))
	$("#"+moveId).children("#getMoveDetails").children('i').removeClass("cf-plus5").removeClass('lnil lnil-chevron-down-circle').addClass("cf-minus5").addClass("fa fa-chevron-circle-up").css("font-size","22px");
    else
    {
       setTimeout(function(){
    $("#CFShowLoading").hide().css('display','none');
	},1000);
        alertSuccess("No migration details found.");
		$('#CFShowLoading').hide();
        return false;
    }
    var $pagination=$("#"+moveId).next().next().next().find("#moveFiles_pagination");
    if($pagination.data("twbs-pagination"))
        $pagination.twbsPagination('destroy');
    sendGAEvents("Get Move Details",moveId);
    //_gaq.push(['_trackEvent', "Get Move Details" , localStorage.getItem('UserId'),moveId]);
    CFManageCloudAccountsAjaxCall.movereportForGiveId(moveId,1);
});
$('#moveSyncReportsForUser').on('click','#getMoveSyncDetails',function(e){
    e.preventDefault();
	    var moveId = $(this).attr('mid');
    if($(this).children().hasClass("cf-minus5"))
    {
         $("#"+moveId).children("#getMoveSyncDetails").children('i').removeClass("cf-minus5").removeClass("fa fa-chevron-circle-up").addClass("cf-plus5").addClass('lnil lnil-chevron-down-circle').css("font-size","18px");
        $("#moveSyncReportsForUser .report").hide();
        // $("#moveSyncReportsForUser .report .InnerReport").hide();
        //  $("#moveSyncReportsForUser .report .FilesDetails").hide();
        return false;
    }
    $("#moveSyncReportsForUser .report").hide();
    // $("#moveSyncReportsForUser .report .InnerReport").hide();
    //  $("#moveSyncReportsForUser .report .FilesDetails").hide();
    $('#CFShowLoading').show();
	$("#getMoveSyncDetails i").removeClass("cf-minus5").removeClass("fa fa-chevron-circle-up").addClass("cf-plus5").addClass('lnil lnil-chevron-down-circle').css("font-size","18px");
    if(!$("#"+moveId).children('i').hasClass("cf-plus5disabled"))
	$("#"+moveId).children("#getMoveSyncDetails").children('i').removeClass("cf-plus5").removeClass('lnil lnil-chevron-down-circle').addClass("cf-minus5").addClass("fa fa-chevron-circle-up").css("font-size","22px");
    else
    {
        setTimeout(function(){ 
    $("#CFShowLoading").hide().css('display','none');
	},1000);
        alertSuccess("No migration details found.");
		$("#CFShowLoading").hide();
        return false;
    }

    var $pagination=$("#"+moveId).next().next().find("#moveFiles_pagination"); 
    if($pagination.data("twbs-pagination"))
        $pagination.twbsPagination('destroy');
    sendGAEvents("Get Move Details",moveId);
    //_gaq.push(['_trackEvent', "Get Move Details" , localStorage.getItem('UserId'),moveId]);
    CFManageCloudAccountsAjaxCall.SyncReports(moveId,1);
});
$('#moveSyncReportsForUser').on('click','.getSyncReportDetails',function(e){
    e.preventDefault();
	    var moveId = $(this).attr('id');
    if($(this).children().hasClass("cf-minus5"))
    {
$("#"+moveId).children('i').removeClass("cf-minus5").removeClass("fa fa-chevron-circle-up").addClass("cf-plus5").addClass('lnil lnil-chevron-down-circle').css("font-size","18px");
        $("#moveSyncReportsForUser .report .InnerReport").hide();
        $("#moveSyncReportsForUser .report .FilesDetails").hide();
        return false;
    }
    $("#moveSyncReportsForUser .report .InnerReport").hide();
    $("#moveSyncReportsForUser .report .FilesDetails").hide();
	$('#CFShowLoading').show();
	$(".getSyncReportDetails i").removeClass("cf-minus5").removeClass("fa fa-chevron-circle-up").addClass("cf-plus5").addClass('lnil lnil-chevron-down-circle').css("font-size","18px");
    if(!$("#"+moveId).children('i').hasClass("cf-plus5disabled"))
	$("#"+moveId).children('i').removeClass("cf-plus5").removeClass('lnil lnil-chevron-down-circle').addClass("cf-minus5").addClass("fa fa-chevron-circle-up").css("font-size","22px");
   else
    { 
        $('#CFShowLoading').hide();
        alertSuccess("No migration details found."); 
        return false;
    }

    var JobStatus = $(this).attr('jobstatus');
    var state = $(this).closest("tr").next();
    var $pagination=$("#"+moveId).parent().parent().find("#moveFiles_pagination");
    if($pagination.data("twbs-pagination"))
        $pagination.twbsPagination('destroy');
    sendGAEvents("Get Move Details",moveId);
    //_gaq.push(['_trackEvent', "Get Move Details" , localStorage.getItem('UserId'),moveId]);
    CFManageCloudAccountsAjaxCall.moveSyncInnerReports(moveId,1,JobStatus,state);
});

$('#move-header').on('change','input[name="srcfile"]',function(){
    if($(this).prop('checked') == true){
        $(this).parent().addClass('fileActive');
    }
    else{
        $(this).parent().removeClass('fileActive');
    }
});
$('#move-header').on('change','input[name="destCloud"]',function(){
    if($(this).prop('checked') == true){
        $(this).parent().siblings('.ui-droppable').removeClass('fileActive');
        $(this).parent().addClass('fileActive');
    }
});
$(document).on('click','[data-dismiss="modal"]',function(){
    $('#page-header input[type="checkbox"]').prop('checked',false);
    disableActionPanel(actionPanel);
    $('#LVContent .panel-dataHoverClass').each(function(){
        $(this).removeClass('panel-dataHoverClass');
    });
    var _a = $(this).attr('id');
    if(_a == "_sharePwdClose"){
        PageName = previousPage;
        enblePanel();
    }
});
$('#listShowMore').click(function(){
    $(this).text('Fetching Files...');
    sendGAEvents("Get Move Details");
    //_gaq.push(['_trackEvent', "Get Move Details" , localStorage.getItem('UserId'),PageName]);
    if(PageName == "Share by Me"){
        PageNumber = PageNumber + 1;
        CFManageCategoryAjaxCall.getFilesShareByMe(PageNumber);
    }
    else if(PageName == "Share with Me"){
        PageNumber = PageNumber + 1;
        CFManageCategoryAjaxCall.getFilesShareWithMe(PageNumber);
    }
    else if (PageName == "Home"){
        PageNumber = PageNumber + 1;
        //var dummypara;
        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles('',PageNumber);
    }
    else if (PageName == "Folders"){
        PageNumber = PageNumber + 1;
        CFManageCloudAccountsAjaxCall.getAllHomeFolders(PageNumber);
    }
    else if (PageName == "InnerFolders"){
        PageNumber = PageNumber + 1;
        CFManageCloudAccountsAjaxCall.gotoInnerFolderandFiles(SingleCloudId,SinglePId,PageNumber)
    }
    else if (PageName == "AllFiles" || PageName == "All Files"){
        PageNumber = PageNumber + 1;
        AllFilesForAllClouds = CFManageCloudAccountsAjaxCall.getAllFiles(PageNumber);
        CFManageCloudAccountsAjaxCall.getAllFileNames(AllFilesForAllClouds, PageNumber);
    }
    else if(PageName == "Recent files"){
        PageNumber = PageNumber + 1;
        CFManageCloudAccountsAjaxCall.getRecentFilesandFolders(PageNumber);
    }
    else if(PageName == "Favorites"){
        PageNumber = PageNumber + 1;
        CFManageCategoryAjaxCall.getFavouriteFiles(PageNumber);
    }
    else if(PageName == "Category"){
        PageNumber = PageNumber + 1;
        CFManageCategoryAjaxCall.getFilesForCategory(globalCategoryId,PageNumber);
    }
    else if(PageName == "Favorites" || PageName == "Share by Me" || PageName == "Share with Me" || PageName == "WorkSpace" || PageName == "InnerWorkSpace"){
        $('#ScrollBarScrolling').addClass('divdisplay');
        return false;
    }else if(PageName == "CloudDrive"){
        PageNumber = PageNumber + 1;
        CFManageCloudAccountsAjaxCall.getAllRootFoldersAndFiles(SingleCloudId , PageNumber);
    }else if(PageName == "search"){
        PageNumber = PageNumber + 1;
        CFManageCloudAccountsAjaxCall.searchFiles(searchTerm , PageNumber);
    }
});
$('#primary [data-stop="navigation"]').on('click',function(e){
    e.preventDefault();
    var location = $(this).children('a').attr('url');
    var _dropZone = $('#my-awesome-dropzone');
    var filecount = _dropZone.find('.dz-preview').length;
    var fileSuccess = _dropZone.find('.dz-success').length;
    var fileEror = _dropZone.find('.dz-error').length;
    var fileComplete = fileSuccess + fileEror;
    if(filecount == fileComplete){
        window.location = location;
    }else{
        $('#uploadLeavePage').attr('url',location);
        $('#uploadConfirmModel').modal('show');
    }
});
$('#uploadLeavePage').on('click',function(){
    //var location = $(this).attr('url');
    window.location = $(this).attr('url');
});
$('#closeUpload').on('click',function(){
    enblePanel();
    if (PageName == "InnerWorkSpace") {
        var uemail = JSON.parse(localStorage.getItem('CFUser')).primaryEmail;
        var _a = {
            'edit':false,
            'upload':false,
            'rename':false
        };
        if(wsdetails.editEmails != null && $.inArray(uemail,wsdetails.editEmails) > -1) {
            var _e = {
                'rename':true,
                'edit':true
            };
            _e = $.extend(_a,_e);
            enablePermisssions(_e);
        }
        else if(wsdetails.readEmails != null && $.inArray(uemail,wsdetails.readEmails) > -1){
            var _r = {
                'edit':true,
                'upload':true,
                'rename':true
            };
            _r = $.extend(_a,_r);
            enablePermisssions(_r);
        }
        else if(wsdetails.coOwnerEmails != null && $.inArray(uemail,wsdetails.coOwnerEmails) > -1){
            enablePermisssions(_a);
        }else if(uemail == wsdetails.ownerEmailId){
            enablePermisssions(_a);
        }
    }
    $(this).closest('#dropZone_Upload').fadeOut();
    $('#dropZone_Upload').find('.dz-preview').each(function(){
        $(this).remove();
    });
    $('#my-awesome-dropzone').removeClass('dz-started');
});
var checkExpire = {
    days : 259200000/*3*24*60*60*1000*/,
    goingToExpire:function(_e,_s,p){
        var _d = _e - _s;
        var _t = Math.round(_d/86400000);
        if(_t == 0 ){
			if(p == false){
            return "Your free trial will expire Today";
			}
			else
			return false;	
        }
        else if(_t < 4){
			if($("#dash-uname").text().includes("Trial User") == true){
            return "Your free trial will expire in "+_t+" day(s)";
			}
			else
			return false;
            
        }
        else{
            return false;
        }
    },
    expired:function(_e,_s,p){
        var _d = _s - _e;
        if(_d < this.days){
			if($("#dash-uname").text().includes("Trial User") == true){
            return "Your free trial expired "+jQuery.timeago(_e);
			}
			else
            return "Your paid subscription has ended "+jQuery.timeago(_e); 
        }
        else if(_d > this.days){
            if($("#dash-uname").text().includes("Trial User") == true){
            return "Your free trial expired "+jQuery.timeago(_e);
			}
			else
            return "Your paid subscription has ended "+jQuery.timeago(_e);
        }
    }
};
$('#CFRecentFilesAndFolders,#favourite,#CFSharedByMe,#CFSharedWithMe').click(function(){ 
    if (window.matchMedia('(max-width: 480px)').matches)
        $("#secondary .profile-menu .black").trigger("click");
});
$('#CloudDriveList').on('click','li.getCloudFiles>p',function() {
    if (window.matchMedia('(max-width: 480px)').matches)
        $("#secondary .profile-menu .black").trigger("click");

});
$('#categoryList').on('click','li',function() {
    if (window.matchMedia('(max-width: 480px)').matches)
        $("#secondary .profile-menu .black").trigger("click");

});