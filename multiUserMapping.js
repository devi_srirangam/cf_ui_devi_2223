/**
 * Created by Vinay computers on 8/31/2017.
 */

$("#move_main").on("change","#mappingClouds input[type=radio]",function () {
    if($("#srcClouds input:checked").length && $("#dstClouds input:checked").length){
        var searchData ={
            "mapping":false,
            "source" : false,
            "destination" : false
        }
        localStorage.setItem("searchData",JSON.stringify(searchData));
        $("#forNextMove").removeClass("disabled");
    }

});
$("#mapdUsers").on("change","input[name=inputMapdUrs]",function () {
    //  localStorage.removeItem("selectedMappings");
    localStorage.removeItem("validator");
    // localStorage.removeItem("selMappings");
    if ($('#mapdUsers input[name= "csvMapngCheckBox"]').length>0) {
        var checkedLength = $('#mapdUsers input[name= "csvMapngCheckBox"]:checked').length + $('#mapdUsers input[name="inputMapdUrs"]:checked').length;
        var totalLength = $('#mapdUsers input[name="csvMapngCheckBox"]').length + $('#mapdUsers input[name="inputMapdUrs"]').length;
    }
    else if ($('#mapdUsers input[name= "folderMapngCheckBox"]').length>0) {
        var checkedLength = $('#mapdUsers input[name= "inputMapdUrs"]:checked').length + $('#mapdUsers input[name="folderMapngCheckBox"]:checked').length;
        var totalLength = $('#mapdUsers input[name="inputMapdUrs"]').length + $('#mapdUsers input[name="folderMapngCheckBox"]').length;
    }
    else {
        var checkedLength = $('#mapdUsers input[name= "inputMapdUrs"]:checked').length;
        var totalLength = $('#mapdUsers input[name="inputMapdUrs"]').length;
    }
    if (checkedLength == totalLength) {
        $('#chkInput').prop('checked', true);
    } else {
        $('#chkInput').prop('checked', false);
    }
    var _d = $(this).parent().parent();
    var _obj ={
        "fromCloudId": {
            "id":$(_d).attr('srccldid'),
        },
        "toCloudId": {
            "id":$(_d).attr('dstncldid'),
        }
    };
    var _objEmail ={
        "fromMailId": $(_d).attr('srcemail'),
        "fileName":$(_d).attr('srcemail'),
        "toMailId": $(_d).attr('dstnemail'),
        "fromCloudName":$(_d).attr('srccldname'),
        "toCloudName":$(_d).attr('dstncldname'),

    };
    if($(this).is(':checked')){
        mappingOptions.localStorageOperation(_obj,'set');
        mappingOptions.localStorageOperation(_objEmail,'set1');
    }

    else{
        mappingOptions.localStorageOperation(_obj,'delete');
        mappingOptions.localStorageOperation(_objEmail,'delete1');

    }


    if((localStorage.selectedMappings != undefined && localStorage.getItem("selectedMappings").length > 10)||($('#mapdUsrs input[name="csvMapngCheckBox"]').prop('checked') == true || $('#mapdUsrs input[name="inputMapdUrs"]').prop('checked') == true))
        $("#forNextMove").removeClass("disabled"); //$('#mapdUsers input[name=inputMapdUrs]:checked').length ||
    else
        $("#forNextMove").addClass("disabled");
});
$("#forNextMove").click(function () {

    $(".ui-helper-hidden-accessible").hide();
    $("#forPreviousMove").removeClass("disabled");
    $("#forNextMove").addClass("disabled");
    $("#preview").css("display","none");
    var _step = parseInt($("#forNextMove").attr("data-step"));
    _step = _step + 1;
    if(_step == 0)
        return false;
    else if(_step == 1){
        $("#srcUsrs .custom-search-input input").val('');
        $("#mapdUsrs .custom-search-input input").val('');
        $("#dstnUsrs .custom-search-input input").val('');
        $("#forPreviousMove").addClass("disabled");
        $("#mappingClouds").css("display","");
        $("#mappingUsers").css("display","none");
        $('#mappingOptions').css("display","none");
        $('#mappingOptionsNew').css("display","none");
        $('#mappedMigration').css("display","none");
        //   $('#mappedSyncMigration').css("display","none");
        if($("#srcClouds input:checked").length && $("#dstClouds input:checked").length)
            $("#forNextMove").removeClass("disabled");

        $("#teamMigrationWidget ul li[data-val=" + (_step - 1) + "]").removeClass("active").addClass("completed");
        $("#forNextMove").attr("data-step",_step);
        $("#forPreviousMove").attr("data-prev",_step );
        $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");
    }
    else if(_step == 2){
        dataLimit();
        $("#srcUsrs .custom-search-input input").val('');
        $("#mapdUsrs .custom-search-input input").val('');
        $("#dstnUsrs .custom-search-input input").val('');
        $("#CFShowLoading").css("display","");
        mappingOptions.localStorageOperation('','rmv');
        mappingOptions.folderOperation('','rmv');
        CsvOperation('','rmv');
        fldrStorage('','rmv');
        fldrStorage('','rmv1');
        localStorage.setItem("multiUsrSrcCldId",$('#srcClouds input[name=sourceCloud]:checked').attr("id"));
        localStorage.setItem("multiUsrDstnCldId",$('#dstClouds input[name=dstCloud]:checked').attr("id"));
        var $srcChkd = $('#srcClouds input[name=sourceCloud]:checked'),$dstChkd = $('#dstClouds input[name=dstCloud]:checked');
        var _cldDtls ={
            srcCldName   : $($srcChkd).siblings("img").attr("src").split("/").pop(),
            dstCldName   : $($dstChkd).siblings("img").attr("src").split("/").pop(),
            srcAdminName : CFManageCloudAccountsAjaxCall.getMaxChars($($srcChkd).parent().siblings().find("h5").text(),20),
            dstAdminName :  CFManageCloudAccountsAjaxCall.getMaxChars($($dstChkd).parent().siblings().find("h5").text(),20)
        };
        var _srcCld = _cldDtls.srcCldName.split(".")[0];
        var _dstnCld = _cldDtls.dstCldName.split(".")[0];
        if((_srcCld === "ONEDRIVE_BUSINESS_ADMIN" && _dstnCld === "DROPBOX_BUSINESS")|| (_srcCld === "DROPBOX_BUSINESS" && _dstnCld === "ONEDRIVE_BUSINESS_ADMIN")){
            $("#csvFileUpload").css("display","none");
            $("#csvFileDownload").css("display","none");
            $("#sortngSrc").css("display","none");
            $("#sortngDstn").css("display","none");
            $("#help").css("display","none");
        }
        else{
            $("#csvFileUpload").css("display","");
            $("#csvFileDownload").css("display","");
            $("#sortngSrc").css("display","");
            $("#sortngDstn").css("display","");
            $("#help").css("display","");
        }
        localStorage.setItem("multiUsrSrcCldName",_cldDtls.srcCldName.split(".")[0]);
        localStorage.setItem("multiUsrDstnCldName",_cldDtls.dstCldName.split(".")[0]);
        pgngOptions.setCldImg(_cldDtls);
        sessionStorage.removeItem('source');
        sessionStorage.removeItem('destination');
        appendMappingScreenHtml( $($srcChkd).attr("id"),"source");
        appendMappingScreenHtml( $($dstChkd).attr("id"),"destination");
        appendingPrevMapping();
        /*   if(localStorage.getItem("Csv")){
               deleteMapping(true);
               $("#mapdUsers tbody").html('');
           }*/
        $("#mappingClouds").css("display","none");
        $("#mappingUsers").css("display","");
        $('#mappingOptions').css("display","none");
        $('#mappingOptionsNew').css("display","none");
        $('#mappedMigration').css("display","none");
        /* $('#mappedSyncMigration').css("display","none");
         if((_cldDtls.srcCldName ==="SHAREPOINT_ONLINE_BUSINESS.png")||(_cldDtls.dstCldName === "SHAREPOINT_ONLINE_BUSINESS.png")){
             disableMapping('add');
         }
         else{
             disableMapping('remove');
         } */
        if($('#mapdUsers input[name=inputMapdUrs]:checked').length)
            $("#forNextMove").removeClass("disabled");

        $("#teamMigrationWidget ul li[data-val=" + (_step - 1) + "]").removeClass("active").addClass("completed");
        $("#forNextMove").attr("data-step",_step);
        $("#forPreviousMove").attr("data-prev",_step );
        $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");
    }
    else if(_step == 3){
        //$("#CFShowLoading").css("display","");
        // syncVerification();
        trialUserMigration();

    }
    else if(_step == 4){
        //$("#forNextMove").removeClass("disabled");
        /* $("#CFShowLoading").css("display","");
           if(localStorage.getItem("fromCloudName") === "SHAREPOINT_ONLINE_BUSINESS" && localStorage.getItem("toCloudName") === "SYNCPLICITY"){
               if($('#jobType_dropDown :selected').text() != "Two-way sync"){
                   var versioning = $('#fileType_dropDown').val();
                   mappingOptions.spoToSyncUpdate(versioning);
               }
               else{
                   mappingOptions.updateJob();
               }
           }
           else if(mappingOptions.updateJob()){ */
        $("#CFShowLoading").css("display","");
        if(mappingOptions.updateJob()){
            $("#forNextMove").removeClass("disabled");

            $("#mappingClouds").css("display","none");
            $("#mappingUsers").css("display","none");
            $('#mappingOptions').css("display","none");
            $('#mappingOptionsNew').css("display","none");
            $("#preview").css("display","");
            $('#mappedMigration').css("display","none");
            //    $('#mappedSyncMigration').css("display","none");

            $("#forNextMove").css({"width": "140px", "margin-left": "-51px"});
            $("#forNextMove span").text("Start Migration");

            $("#teamMigrationWidget ul li[data-val=" + (_step - 1) + "]").removeClass("active").addClass("completed");
            $("#forNextMove").attr("data-step",_step);
            $("#forPreviousMove").attr("data-prev",_step );
            $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");
        }
        // var _mappingSrcUrs = $('#mapdUsers input:checked');
        // var _pricing = initiateMigration(_mappingSrcUrs);
        // if(_pricing){
        //     refreshReports();
        //     $("#mappingClouds").css("display","none");
        //     $("#mappingUsers").css("display","none");
        //     $('#mappingOptions').css("display","none");
        //     $('#mappedMigration').css("display","");
        //     setTimeout(function () {
        //         $("#mappedMigration #mgrtnReports tbody").html('');
        //         var _response = getMoveWorkspaces(1);
        //         appendMigrationWorkspaces(_response);
        //     },1000);
        // }

    }
    else if(_step == 5){
        var _isExists = false;
        $('div.prvsnStatus:contains("Not Provisioned")').each(function () {
            _isExists=true;
        });
        if(_isExists){
            showNotyNotification("notify","Non provisoned users will not migrate");
        }
        /* if($('#jobType_dropDown :selected').text() === "One-way sync" || $('#jobType_dropDown :selected').text() === "Two-way sync"){
                syncUsers();
                $('#mappedSyncMigration').css("display","");
            }
            else {
                mappingOptions.migrationInitiation(localStorage.getItem("jobId"));
                $('#mappedMigration').css("display","");
            } */

        mappingOptions.migrationInitiation(localStorage.getItem("jobId"));
        $("#mappingClouds").css("display","none");
        $("#mappingUsers").css("display","none");
        $('#mappingOptions').css("display","none");
        $('#mappingOptionsNew').css("display","none");
        $("#preview").css("display","none");
        $('#mappedMigration').css("display",""); //comment if sync exists
        $("#forNextMove span").text("Next");
        $("#forNextMove").css({"width": "83.5px", "margin-left": ""});
        $("#teamMigrationWidget ul li[data-val=" + (_step - 1) + "]").removeClass("active").addClass("completed");
        $("#forNextMove").attr("data-step",_step);
        $("#forPreviousMove").attr("data-prev",_step );
        $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");
    }

});
$("#forPreviousMove").click(function () {
    var _step = parseInt($("#forPreviousMove").attr("data-prev"));
    $(".ui-helper-hidden-accessible").hide();
    if(_step == 6)
        location.reload();



    $("#forNextMove").css({"width": "83.5px", "margin-left": ""});
    $("#forNextMove span").text("Next");
    $("#preview").css("display","none");
    $("#forNextMove").removeClass("disabled");
    _step = _step - 1;

    if(_step == 0)
        return false;
    else if(_step == 1){
        $("#srcUsrs .custom-search-input input").val('');
        $("#mapdUsrs .custom-search-input input").val('');
        $("#dstnUsrs .custom-search-input input").val('');
        $("#forPreviousMove").addClass("disabled");
        $("#mappingClouds").css("display","");
        $("#mappingUsers").css("display","none");
        $('#mappedMigration').css("display","none");
        //  $('#mappedSyncMigration').css("display","none");
        $('#mappingOptions').css("display","none");
        $('#mappingOptionsNew').css("display","none");
        $("#teamMigrationWidget ul li[data-val='1']").addClass("active");
        $("#teamMigrationWidget ul li").removeClass("active").removeClass("completed");
        if($("#srcClouds input:checked").length && $("#dstClouds input:checked").length)
            $("#forNextMove").removeClass("disabled");
    }
    else if(_step == 2){
        $("#srcUsrs .custom-search-input input").val('');
        $("#mapdUsrs .custom-search-input input").val('');
        $("#dstnUsrs .custom-search-input input").val('');
        $("#mappingClouds").css("display","none");
        $("#mappingUsers").css("display","");
        $('#mappedMigration').css("display","none");
        //  $('#mappedSyncMigration').css("display","none");
        $('#mappingOptions').css("display","none");
        $('#mappingOptionsNew').css("display","none");
        if($('#mapdUsers input[name=inputMapdUrs]:checked').length)
            $("#forNextMove").removeClass("disabled");
    }
    else if(_step == 3){
        // trialUserMigration();
        //  syncVerification();
        //mappingOptions.createJob();
        //  $("#CFShowLoading").css("display","");
        $("#jobEmptyPopup").css("display","none");
        $('.Editing_Jobname').attr("disabled","disabled");
        $("#checkTimediv").css("display","none");
        $(".Job_NAme").css("display","");
        $("#mappingClouds").css("display","none");
        $("#mappingUsers").css("display","none");
        if(localStorage.getItem("multiUsrSrcCldName") === "DROPBOX_BUSINESS" && localStorage.getItem("multiUsrDstnCldName") === "ONEDRIVE_BUSINESS_ADMIN"){
            $('#mappingOptionsNew').css("display","");
        }
        else{
            $('#mappingOptions').css("display","");
        }
        $("#preview").css("display","none");
        $('#mappedMigration').css("display","none");
        //    $('#mappedSyncMigration').css("display","none");
    }
    else if(_step == 4){
        $("#forNextMove").removeClass("disabled");

        $("#mappingClouds").css("display","none");
        $("#mappingUsers").css("display","none");
        $('#mappingOptions').css("display","none");
        $('#mappingOptionsNew').css("display","none");
        $("#preview").css("display","");
        $('#mappedMigration').css("display","none");
        //  $('#mappedSyncMigration').css("display","none");
    }
    else if(_step == 5){
        refreshReports();
        // var _mappingSrcUrs = $('#mapdUsers input:checked');
        // var _pricing = initiateMigration(_mappingSrcUrs);
        // if(_pricing){
        //     setTimeout(function () {
        //         refreshReports();
        //         $("#mappingClouds").css("display","none");
        //         $("#mappingUsers").css("display","none");
        //         $('#mappedMigration').css("display","");
        //     },500);
        // }
    }

    $("#forPreviousMove").attr("data-prev",_step);
    $("#forNextMove").attr("data-step",_step);
    $("#teamMigrationWidget ul li[data-val=" + (_step + 1) + "]").removeClass("active");
    $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active").removeClass("completed");
});
$("#teamMigrationWidget ul li").click(function () {
    if($("#teamMigrationWidget ul li.last").hasClass("completed"))
        return false;

    if($(this).hasClass("active") || $(this).hasClass("completed"))
    {
        var _step = parseInt($(this).find(".badge").text());
        $("#preview").css("display","none");
        /* if(_step == 0)
         return false;
         else if(_step == 1){
         $("#forPreviousMove").addClass("disabled");
         $("#mappingClouds").css("display","");
         $("#mappingUsers").css("display","none");
         $('#mappedMigration').css("display","none");
         $('#mappingOptions').css("display","none");
         if($("#srcClouds input:checked").length && $("#dstClouds input:checked").length)
         $("#forNextMove").removeClass("disabled");
         }
         else if(_step == 2){
         $("#mappingClouds").css("display","none");
         $("#mappingUsers").css("display","");
         $('#mappedMigration').css("display","none");
         $('#mappingOptions').css("display","none");
         if($('#mapdUsers input[name=inputMapdUrs]:checked').length)
         $("#forNextMove").removeClass("disabled");
         }
         else if(_step == 3){
         mappingOptions.createJob();
         }
         else if(_step == 4){
         $("#forNextMove").removeClass("disabled");

         $("#mappingClouds").css("display","none");
         $("#mappingUsers").css("display","none");
         $('#mappingOptions').css("display","none");
         $("#preview").css("display","");
         $('#mappedMigration').css("display","none");
         }
         else if(_step == 5){
         // mappingOptions.migrationInitation();
         // $("#mappingClouds").css("display","none");
         // $("#mappingUsers").css("display","none");
         // $('#mappedMigration').css("display","");
         refreshReports();
         }*/
        if(_step == 0)
            return false;
        else if(_step == 1){
            $("#srcUsrs .custom-search-input input").val('');
            $("#mapdUsrs .custom-search-input input").val('');
            $("#dstnUsrs .custom-search-input input").val('');
            $(".ui-helper-hidden-accessible").hide();
            $("#forPreviousMove").addClass("disabled");
            $("#mappingClouds").css("display","");
            $("#mappingUsers").css("display","none");
            $('#mappedMigration').css("display","none");
            //      $('#mappedSyncMigration').css("display","none");
            $('#mappingOptions').css("display","none");
            $('#mappingOptionsNew').css("display","none");
            if($("#srcClouds input:checked").length && $("#dstClouds input:checked").length)
                $("#forNextMove").removeClass("disabled");
        }
        else if(_step == 2){
            $("#srcUsrs .custom-search-input input").val('');
            $("#mapdUsrs .custom-search-input input").val('');
            $("#dstnUsrs .custom-search-input input").val('');
            $("#mappingClouds").css("display","none");
            $("#mappingUsers").css("display","");
            $('#mappedMigration').css("display","none");
            //    $('#mappedSyncMigration').css("display","none");
            $('#mappingOptions').css("display","none");
            $('#mappingOptionsNew').css("display","none");
            if($('#mapdUsers input[name=inputMapdUrs]:checked').length)
                $("#forNextMove").removeClass("disabled");
        }
        else if(_step == 3){
            //  syncVerification();
            //trialUserMigration();
            $("#mappingClouds").css("display","none");
            $("#mappingUsers").css("display","none");
            // if((localStorage.getItem("multiUsrSrcCldName") === "ONEDRIVE_BUSINESS_ADMIN" && (localStorage.getItem("multiUsrDstnCldName") === "DROPBOX_BUSINESS"))||(localStorage.getItem("multiUsrSrcCldName") === "DROPBOX_BUSINESS" && localStorage.getItem("multiUsrDstnCldName") === "ONEDRIVE_BUSINESS_ADMIN")){
            if(localStorage.getItem("multiUsrSrcCldName") === "DROPBOX_BUSINESS" && localStorage.getItem("multiUsrDstnCldName") === "ONEDRIVE_BUSINESS_ADMIN"){
                $('#mappingOptionsNew').css("display","");

            }
            else{
                $('#mappingOptions').css("display","");
            }
            $("#preview").css("display","none");
            $('#mappedMigration').css("display","none");
            //       $('#mappedSyncMigration').css("display","none");
            $("#forNextMove").removeClass("disabled");
        }
        else if(_step == 4){
            $("#forNextMove").removeClass("disabled");

            $("#mappingClouds").css("display","none");
            $("#mappingUsers").css("display","none");
            $('#mappingOptions').css("display","none");
            $('#mappingOptionsNew').css("display","none");
            $("#preview").css("display","");
            $('#mappedMigration').css("display","none");
            //    $('#mappedSyncMigration').css("display","none");
            $("#forNextMove").css({"width": "140px", "margin-left": "-51px"});
            $("#forNextMove span").text("Start Migration");
        }
        else if(_step == 5){
            refreshReports();
        }
        if(_step < 4){
            $("#forNextMove").css({"width": "83.5px", "margin-left": ""});
            $("#forNextMove span").text("Next");
        }

        $("#forPreviousMove").attr("data-prev",_step);
        $("#forNextMove").attr("data-step",_step);
        //$("#teamMigrationWidget ul li[data-val=" + (_step + 1) + "]").removeClass("active");
        $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active").removeClass("completed");
        _step = _step + 1;
        for(;_step<6;_step++)
            $("#teamMigrationWidget ul li[data-val=" + (_step) + "]").removeClass("active").removeClass("completed");
    }
});
function getDomainUserDetails(cid,pgNo,pgSize,searchCloudUser) {
    if(pgNo == undefined)
        pgNo = 1;
    if(pgSize == undefined )
        pgSize = 10;
    var _data = "cloudId="+cid+"&pageNo="+pgNo+"&pageSize="+pgSize;
    if(searchCloudUser != undefined)
        _data=_data+"&searchCloudUser="+searchCloudUser;
    var _usrsList;
    $.ajax({
        type: "GET",
        url: apicallurl + "/mapping/user/domain/list?" + _data,
        async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data) {
            _usrsList = data;
        }
    });
    return _usrsList;
}
function   appendMappingScreenHtml(cldId,target,pgNo,pgSize,searchCloudUser){
    if(searchCloudUser == undefined)
        migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),pgNo, pgSize,target);
    else{
        searchCloudUser = searchCloudUser.toLowerCase();
        migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),pgNo, pgSize,target,searchCloudUser); // if(searchCloudUser != undefined)
    }
    //     searchCloudUser = searchCloudUser.toLowerCase();
    //
    // var _usrData = getDomainUserDetails(cldId,pgNo,pgSize,searchCloudUser);
    // var _domainLength = _usrData.length;
    // var _valDomain;
    // if(target  == "source"){
    //     _valDomain = $("#srcCloudUsers  .forDomainNameMin:visible .fa-minus").next().text();
    //     $("#srcCloudUsers .message-widget").html("");
    // }
    // else{
    //     _valDomain = $("#dstCloudsUsers  .forDomainNameMin:visible .fa-minus").next().text();
    //     $("#dstCloudsUsers .message-widget").html("");
    // }
    // for(var i=0;i<_domainLength;i++){
    //     var _pgCnt;
    //     _pgCnt = Math.ceil(_usrData[i].noOfCloudsPreesent/_usrData[i].noOfCloudsReturn);
    //     if(searchCloudUser != undefined)
    //         _pgCnt = 1;
    //
    //     var _html = '<div class="className"> <div><div class="forDomainName" style="display: block" maxPages="pagesCount"><i class="fa fa-plus" aria-hidden="true" style="padding: 12px;cursor: pointer"></i><span style="font-size: initial;">domainName</span></div>';
    //     _html = _html.replace("domainName",_usrData[i].domainName).replace('pagesCount',_pgCnt);
    //     _html = _html + '<div class="forDomainNameMin" style="display: none"><i class="fa fa-minus" aria-hidden="true" style="padding: 12px;cursor: pointer"></i><span style="font-size: initial;">domainName</span> usersList</div></div></div>';
    //     _html = _html.replace("domainName",_usrData[i].domainName);
    //     var _usrAppend = '<div style="border-bottom: 1' +
    //         'px solid #f2f2f2;padding: 5px 0px"> <span style="font-size: initial"><input type="radio" name="srcUsers" style="margin-left: 11%;margin-bottom: 2%" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-left: 2%;"></i>&nbsp;&nbsp;userName</span> </div>';
    //     var _usrsLength = _usrData[i].cloudDetail.length;
    //     var _usrDataAppend = '';
    //     if(target  == "source" && _valDomain == _usrData[i].domainName)
    //         _html = _html.replace('class="forDomainName" style="display: block"','class="forDomainName" style="display: none"').replace('<div class="forDomainNameMin" style="display: none">','<div class="forDomainNameMin" style="display: block">');
    //
    //      else if(_valDomain == _usrData[i].domainName)
    //     _html = _html.replace('class="forDomainName" style="display: block"','class="forDomainName" style="display: none"').replace('<div class="forDomainNameMin" style="display: none">','<div class="forDomainNameMin" style="display: block">');
    //
    //     if(searchCloudUser != undefined){
    //         _html =  _html.replace('display: none','').replace('display: block','display: none');
    //     }
    //     for(var j=0;j<_usrsLength;j++)
    //     {
    //         _usrDataAppend = _usrDataAppend + _usrAppend.replace("userName",_usrData[i].cloudDetail[j].cloudUserId.split("|")[1].split("@")[0]).replace("emailId",_usrData[i].cloudDetail[j].cloudUserId.split("|")[1]).replace("cldId",_usrData[i].cloudDetail[j].id).replace("root",_usrData[i].cloudDetail[j].rootFolderId).replace("cloudName",_usrData[i].cloudDetail[j].name);
    //         if(target  != "source")
    //             _usrDataAppend = _usrDataAppend.replace("className", "dstCloudsUsers").replace("srcUsers","dstUsers");
    //         else
    //             _usrDataAppend = _usrDataAppend.replace("className", "srcCloudUsers");
    //     }
    //     _html = _html.replace("usersList",_usrDataAppend);
    //     if(target  == "source") {
    //         $("#srcCloudUsers .message-widget").append(_html);
    //         $(document).find("#mapdUsers table tbody tr").each(function(){
    //             userMappingDisable($(this).attr('srccldid'),'source');
    //         });
    //     }
    //     else {
    //         $("#dstCloudsUsers .message-widget").append(_html);
    //         //appendingPrevMapping();
    //         $(document).find("#mapdUsers table tbody tr").each(function(){
    //             userMappingDisable($(this).attr('dstncldid'),'destination');
    //         });
    //
    //     }
    // }
}

function userMappingDisable( id, target){
    if(target == 'source')
        $("#srcUsrs input[cloudid=" + id+ "]").attr('disabled',true).parent().parent("div").addClass('selectedClass');
    else if(target == 'destination')
        $("#dstnUsrs input[cloudid=" + id+ "]").attr('disabled',true).parent().parent("div").addClass('selectedClass');
}
function  appendingPrevMapping(pageNo,pageSize) {
    if(pageNo == undefined)
        pageNo = 1;

    pageSize = parseInt($('#mapdUsrs select').val());
    var  _sortFun = migrationMapping.sortBy();

    $.ajax({
        type: "GET",
        url: apicallurl + "/mapping/user/cache/list?sourceCloudId=" + localStorage.getItem("multiUsrSrcCldId") + "&destCloudId=" + localStorage.getItem("multiUsrDstnCldId") + "&pageNo=" + pageNo +"&pageSize=" + pageSize + _sortFun,
        async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data) {
            $("#mapdUsers tbody").html('');
            $("#chkInput").removeAttr("checked");
            var _len = data.length;
            if(_len == 0 && pageNo == 1){
                var _txt = "0 of 0";
                $("#mapdUsrs .paginationDiv").css("opacity","0.6");
                $('#mapdUsrs .paginationDiv select').prop('disabled', true);
                $('#mapdUsrs .paginationDiv input').prop('disabled', true);

                $("#mapdUsrs .paginationCF input").val(0);
                $("#mapdUsrs .paginationCF span").last().text(_txt );
            }
            if( _len == 0){
                $(document).find("#chkInput").attr("disabled", true).prop( "checked", false );
                if(pageNo != 1)
                    $("#CFShowLoading").css("display","none");
                $('.download').css('pointer-events', 'none');
                $('.download').css('opacity', '0.5');
                $('#help').css('visibility', 'hidden');

                $("#clearBtn").css("pointer-events",'none');
                $("#clearBtn").css("cursor","not-allowed");
                $("#clearBtn").css("opacity","0.6");
                return false;
            }
            for(var i=0;i<_len;i++){
                var _dataVal = data[i];
                var csvID = _dataVal.csvId;
                var csvName =  _dataVal.csvName;
                var migrateDstnFolderName = _dataVal.migrateFolderName;
                var _srcUsrDetails =  {
                    "userEmail": _dataVal.sourceCloudDetails.emailId,
                    "userCloudName": _dataVal.sourceCloudDetails.name,
                    "userCloudId": _dataVal.sourceCloudDetails.id,
                    "userRtFolId": _dataVal.sourceCloudDetails.rootFolderId,
                    "folderPath": _dataVal.sourceCloudDetails.folderPath,
                    "srcPathRootFolderId": _dataVal.sourceCloudDetails.pathRootFolderId,
                    "migrateSrcFolderName": _dataVal.sourceCloudDetails.migrateFolderName

                }
                var _dstnUsrDetails =  {
                    "dstnUserEmail": _dataVal.destCloudDetails.emailId,
                    "dstnUserCloudName":_dataVal.destCloudDetails.name ,
                    "dstnUserCloudId":_dataVal.destCloudDetails.id,
                    "dstnUserRtFolId": _dataVal.destCloudDetails.rootFolderId,
                    "dstnFolderPath": _dataVal.destCloudDetails.folderPath,
                    "dstnPathRootFolderId": _dataVal.destCloudDetails.pathRootFolderId
                }
                if ((_srcUsrDetails.userCloudName === "ONEDRIVE_BUSINESS_ADMIN" && _dstnUsrDetails.dstnUserCloudName === "DROPBOX_BUSINESS") || (_srcUsrDetails.userCloudName === "DROPBOX_BUSINESS" && _dstnUsrDetails.dstnUserCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
                    if (_srcUsrDetails.folderPath === null && _dstnUsrDetails.dstnFolderPath === null) {
                        _srcUsrDetails.folderPath = '/';
                        _dstnUsrDetails.dstnFolderPath = '/';
                    }
                    var isFolder = true;
                    mapdSlectedUrs(_srcUsrDetails, _dstnUsrDetails, csvID, csvName, migrateDstnFolderName, undefined, isFolder,'',_dataVal.duplicateCache,_dataVal.pathException);
                    if( $('#mapdUsers input[name= "inputMapdUrs"]').length > 0) {
                        var checkedLength = $('#mapdUsers input[name= "inputMapdUrs"]:checked').length + $('#mapdUsers input[name="folderMapngCheckBox"]:checked').length;
                        var totalLength = $('#mapdUsers input[name="inputMapdUrs"]').length + $('#mapdUsers input[name="folderMapngCheckBox"]').length;
                    }
                    else{
                        var checkedLength = $('#mapdUsers input[name= "folderMapngCheckBox"]:checked').length;
                        var totalLength = $('#mapdUsers input[name="folderMapngCheckBox"]').length;
                    }
                    if (checkedLength == totalLength) {
                        $('#chkInput').prop('checked', true);

                    }
                    else{
                        $('#chkInput').prop('checked', false);
                    }

                }
                else{
                    $("input[cloudid=" + _srcUsrDetails.userCloudId + "]").attr('disabled',true).parent().parent().addClass("selectedClass");
                    $("input[cloudid=" + _dstnUsrDetails.dstnUserCloudId + "]").attr('disabled',true).parent().parent().addClass("selectedClass");
                    $('.download').css('pointer-events', 'none');
                    $('.download').css('opacity', '0.5');
                    $('#help').css('visibility', 'hidden');
                    mapdSlectedUrs(_srcUsrDetails,_dstnUsrDetails,csvID,csvName,migrateDstnFolderName);
                }
            }
            if(pageNo == 1){
                $('#paginationMapng').empty();
                $('#paginationMapng').removeData("twbs-pagination");
                $('#paginationMapng').unbind("page");
                // var _totPages;
                // if(  data.length == 0 )
                //     _totPages = 1;
                // else
                //     _totPages = Math.ceil(data[0].noOfMappedCloudPressent/30);
                $("#mapdUsrs .paginationDiv input").val(1);
                // var _txt = "1 - " +  $('#mapdUsrs select').val();
                //
                // $("#mapdUsrs .paginationCF span").last().text(_txt + " of " + data[0].noOfMappedCloudPressent);

                // $('#paginationMapng').twbsPagination({
                //     totalPages: _totPages,
                //     visiblePages: 6,
                //     startPage:1,
                //     next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
                //     prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
                //     first:'<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
                //     last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
                //     onPageClick: function (event, page){
                //         appendingPrevMapping(page);
                //     }
                // });
            }
            //var _txt = ((pageNo-1) * $('#mapdUsrs select').val() + 1) + " - " + pageNo * $('#mapdUsrs select').val();
            //var _txt = pageNo +" of " + Math.ceil(/pageSize);
            var _selInptVal = $('#mapdUsrs select').val();
            var _totPages = Math.ceil(data[0].noOfMappedCloudPressent/_selInptVal);

            var _txt = pageNo + " of " + _totPages;

            $("#mapdUsrs .paginationCF span").last().text(_txt );
            $("#mapdUsrs .paginationCF input").val(pageNo );

            $("#mapdUsrs .paginationCF span").last().text(_txt);
            if( $("#mapdUsers tbody tr input[name=inputMapdUrs]:checked").length == 30 ){
                $("#chkInput").prop('checked', true);
            }
            setTimeout(function(){
                if($("#CFShowLoading").attr("autoMap") != "false")
                    $("#CFShowLoading").css("display","none");
            }, 2000);
        }
    });
}
$(document).on('click','#srcUsrs .forDomainName i',function() {
    $("#srcUsrs .forDomainNameMin").css("display","none");
    $("#srcUsrs .forDomainName").css("display","block");
    // $(this).parent().css("display","none");
    // $(this).parent().siblings(".forDomainNameMin").css("display","block");
    var _class = $(this).parents("div.forDomainName").attr("class").split(" ")[1];
    var $rmvUsrs = $(document.getElementById("srcCloudUsers").getElementsByClassName(_class));
    migrationMapping.getPgntdUsrsData(_class, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), 1, 10, "source", $rmvUsrs[1]);

    // if($("#paginationSrc .page.active a").text() == '1' )
    // {
    //     $('#paginationSrc').empty();
    //     $('#paginationSrc').removeData("twbs-pagination");
    //     $('#paginationSrc').unbind("page");
    //     var _totPages = $(this).parent().attr('maxpages');
    //     $('#paginationSrc').twbsPagination({
    //         totalPages: _totPages,
    //         visiblePages: 4,
    //         next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
    //         prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
    //         first:'<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
    //         last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
    //         onPageClick: function (event, page){
    //             appendMappingScreenHtml(localStorage.getItem("multiUsrSrcCldId"),"source",page,$("#srcCloudUsers select").val());
    //         }
    //     });
    // }
});
$(document).on('click','#dstnUsrs .forDomainName i',function() {
    $("#dstnUsrs .forDomainNameMin").css("display","none");
    $("#dstnUsrs .forDomainName").css("display","block");
    var _class = $(this).parents("div.forDomainName").attr("class").split(" ")[1];
    var $rmvUsrs = $(document.getElementById("dstCloudsUsers").getElementsByClassName(_class));
    migrationMapping.getPgntdUsrsData(_class, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), 1, 10, "destination", $rmvUsrs[1]);
    // if($("#paginationDtn .page.active a").text() == '1' )
    // {
    //     $('#paginationDtn').empty();
    //     $('#paginationDtn').removeData("twbs-pagination");
    //     $('#paginationDtn').unbind("page");
    //     var _totPages = $(this).parent().attr('maxpages');
    //     $('#paginationDtn').twbsPagination({
    //         totalPages: _totPages,
    //         visiblePages: 4,
    //         next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
    //         prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
    //         first:'<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
    //         last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
    //         onPageClick: function (event, page){
    //             appendMappingScreenHtml(localStorage.getItem("multiUsrDstnCldId"),"destination",page,$("#paginationDtn select").val());
    //         }
    //     });
    // }

});

var typingTimer;
//check input box empty and Enter key-press
$(document).find("#srcUsrs .custom-search-input input").keyup(function(e){
    if($(this).val().length == 0){
        var searchData = JSON.parse(localStorage.getItem("searchData"));
        searchData.source = false;
        localStorage.setItem("searchData",JSON.stringify(searchData));
        $(".ui-autocomplete.source").css("display","none");
        appendMappingScreenHtml( localStorage.getItem("multiUsrSrcCldId"),"source",1,10,$(this).val().trim());
    }
    else if( e.which == 13){


        var searchData = JSON.parse(localStorage.getItem("searchData"));
        searchData.source = true;
        localStorage.setItem("searchData",JSON.stringify(searchData));
        appendMappingScreenHtml(localStorage.getItem("multiUsrSrcCldId"),"source",1,10,$(this).val().trim());
        return;
    }
    if($(this).val().length &&((e.key != "ArrowLeft" && (e.key != "ArrowRight")))){
        localStorage.setItem('searchVal',$(this).val().trim());
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function(){
            var s = localStorage.getItem('searchVal');
            migrationMapping.autoComplete('source',s);

        }, 100);
    }
    // else if($(this).val().length == 1)
    //     migrationMapping.autoComplete( "source",$(this).val());
});
$(document).find("#srcUsrs .custom-search-input input").keypress(function(e){
    if($(this).val().length == 0){
        migrationMapping.autoComplete( "source",e.key);
    }
    else if((e.which == 13) || (e.keyCode == 8))
        $(".ui-autocomplete").css("display","none");

});
$(document).find("#dstnUsrs .custom-search-input input").keyup(function(e){
    if($(this).val().length == 0){
        var searchData = JSON.parse(localStorage.getItem("searchData"));
        searchData.destination = false;
        localStorage.setItem("searchData",JSON.stringify(searchData));
        $(".ui-autocomplete.destination").css("display","none");
        appendMappingScreenHtml(localStorage.getItem("multiUsrDstnCldId"),"destination");
    }
    else if( e.which == 13){

        var searchData = JSON.parse(localStorage.getItem("searchData"));
        searchData.destination = true;
        localStorage.setItem("searchData",JSON.stringify(searchData));
        appendMappingScreenHtml(localStorage.getItem("multiUsrDstnCldId"),"destination",1,10,$(this).val().trim());
        return;
    }
    if($(this).val().length){
        localStorage.setItem('searchVal',$(this).val().trim());
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function(){
            var s = localStorage.getItem('searchVal');
            migrationMapping.autoComplete('destination',s);

        }, 100);
    }

    // else if($(this).val().length == 1)
    //     migrationMapping.autoComplete( "destination",$(this).val());
});
$(document).find("#dstnUsrs .custom-search-input input").keypress(function(e){
    if($(this).val().length == 0){
        $(".ui-autocomplete.destination").css("display","none");
        migrationMapping.autoComplete( "destination",e.key);
    }
    else if( e.which == 13)
        $(".ui-autocomplete").css("display","none");
});
$(document).find("#mappedMigration .custom-search-input input").keyup(function(e){
    if($(this).val().length == 0){
        $(".ui-autocomplete.mapping").css("display","none");
        refreshReports();
    }
    else if( e.which == 13)
        mappingOptions.searchJobByName($(this).val());
});
/* $(document).find("#mappedSyncMigration .custom-search-input input").keyup(function(e){
    if($(this).val().length == 0){
        $(".ui-autocomplete.mapping").css("display","none");
        refreshReports();
    }
    else if( e.which == 13)
        mappingOptions.searchJobByName($(this).val());
}); */
$(document).find("#mapdUsrs .custom-search-input input").keyup(function(e){
    if($(this).val().length == 0)
    {
        if((e.key == "Control") ||(e.key == "Shift")|| (e.key == "Alt")){
            return false;
        }
        $(".ui-autocomplete.mapping").css("display","none");
        $("#mapdUsers tbody").html('');

        var searchData = JSON.parse(localStorage.getItem("searchData"));
        searchData.mapping = false;
        localStorage.setItem("searchData",JSON.stringify(searchData));
        // $("div").removeClass('selectedClass');
        // $("#srcUsrs input:radio").attr('disabled',false);
        // $("#dstnUsrs input:radio").attr('disabled',false);
        //deleteMapping();
        //autoMapping(1,10,localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"));
        appendingPrevMapping();
    }
    else if( e.which == 13){
        var searchData = JSON.parse(localStorage.getItem("searchData"));
        searchData.mapping = true;
        localStorage.setItem("searchData",JSON.stringify(searchData));
        autoMappingSearch($(this).val().trim());

        return;
    }
    if($(this).val().length){
        localStorage.setItem('searchVal',$(this).val().trim());
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function(){
            var s = localStorage.getItem('searchVal');
            migrationMapping.autoComplete('mapping',s);

        }, 100);
    }
});

/*... Mapped-Users keydown for firefox ...*/
$(document).find("#mapdUsrs .custom-search-input input").keydown(function(e){
    if(e.key == "Control" || e.key == "Shift" || e.key == "Alt"){
        e.preventDefault();
        return false;
    }
});

$(document).find("#mapdUsrs .custom-search-input input").keypress(function(e){
    if($(this).val().length == 0) {
        $(".ui-autocomplete.mapping").css("display","none");
        migrationMapping.autoComplete( "mapping",e.key);
    }
    else if( e.which == 13)
        $(".ui-autocomplete").css("display","none");
});
$(document).on('click','#srcUsrs .forDomainNameMin .fa-plus',function() {
    $("#srcUsrs .forDomainNameMin").css("display","none");
    $("#srcUsrs .forDomainNameMin").find('.fldr_parent1').css("display","none");
    $("#srcUsrs .forDomainNameMin").find('span.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');
    $("#srcUsrs .forDomainName").css("display","block");
});
$(document).on('click','#srcUsrs .forDomainNameMin .fa-minus',function() {
    if($(".sourceChkBox").prop('checked',true)){
        $(".sourceChkBox").prop('checked',false);
    }
    $("#srcUsrs .forDomainNameMin").find('.usr-folder').removeClass('fa-angle-up').addClass('fa-angle-down');
    $("#srcUsrs .forDomainNameMin").css("display","none");
    $("#srcUsrs .forDomainNameMin").find('.fldr_parent1').css("display","none");
    $("#srcUsrs .forDomainNameMin").find('span.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');
    $("#srcUsrs .forDomainName").css("display","block");
});
$(document).on('click','#dstnUsrs .forDomainNameMin .fa-plus',function() {
    $("#dstnUsrs .forDomainNameMin").css("display","none");
    $("#dstnUsrs .forDomainNameMin").find('.fldr_parent1').css("display","none");
    $("#dstnUsrs .forDomainNameMin").find('span.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');

    $("#dstnUsrs .forDomainName").css("display","block");
});
$(document).on('click','#dstnUsrs .forDomainNameMin .fa-minus',function() {
    if($("input[name='folderradiobtn']:checked")){
        $("input:radio").removeAttr("checked");
    }
    $("#dstnUsrs .forDomainNameMin").find('.usr-folder').removeClass('fa-angle-up').addClass('fa-angle-down');
    $("#dstnUsrs .forDomainNameMin").css("display","none");
    $("#dstnUsrs .forDomainNameMin").find('.fldr_parent1').css("display","none");
    $("#dstnUsrs .forDomainNameMin").find('span.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');
    $("#dstnUsrs .forDomainName").css("display","block");
});
$(document).on('click','#srcUsrs .custom-search-input button',function(){
    if(!$("#srcUsrs .custom-search-input input").val().length)
        return false;

    var searchData = JSON.parse(localStorage.getItem("searchData"));
    searchData.source = true;
    localStorage.setItem("searchData",JSON.stringify(searchData));
    appendMappingScreenHtml(localStorage.getItem("multiUsrSrcCldId"),"source",1,10,$("#srcUsrs .custom-search-input input").val().trim());
});
$(document).on('click','#dstnUsrs .custom-search-input button',function(){
    if(!$("#dstnUsrs .custom-search-input input").val().length)
        return false;

    var searchData = JSON.parse(localStorage.getItem("searchData"));
    searchData.destination = true;
    localStorage.setItem("searchData",JSON.stringify(searchData));
    appendMappingScreenHtml(localStorage.getItem("multiUsrDstnCldId"),"destination",1,10,$("#dstnUsrs .custom-search-input input").val().trim());
});
$(document).on('change','#srcUsrs select',function(){
    if($("#srcUsrs .custom-search-input input").val().length)
        return false;
    appendMappingScreenHtml(localStorage.getItem("multiUsrSrcCldId"),"source",1,$(this).val());

});
$(document).on('change','#dstnUsrs select',function(){
    if($("#dstnUsrs .custom-search-input input").val().length)
        return false;

    appendMappingScreenHtml(localStorage.getItem("multiUsrDstnCldId"),"destination",1,$(this).val());
});
$("#srcCloudUsers,#dstCloudsUsers").on('change','input',function(){
    if($("#srcCloudUsers input:radio:checked").length && $("#dstCloudsUsers input:radio:checked").length){
        var $srcCheckedUsr = $("#srcCloudUsers input:radio:checked");
        var $dstCheckedUsr = $("#dstCloudsUsers input:radio:checked");
        var _srcUsrDetails =  {
            "userEmail": $($srcCheckedUsr).attr("usremail"),
            "userCloudName": $($srcCheckedUsr).attr("cldname"),
            "userCloudId": $($srcCheckedUsr).attr("cloudid"),
            "userRtFolId": $($srcCheckedUsr).attr("rtfolid")
        }

        var _dstnUsrDetails =  {
            "dstnUserEmail": $($dstCheckedUsr).attr("usremail"),
            "dstnUserCloudName": $($dstCheckedUsr).attr("cldname"),
            "dstnUserCloudId": $($dstCheckedUsr).attr("cloudid"),
            "dstnUserRtFolId": $($dstCheckedUsr).attr("rtfolid")
        }
        $($srcCheckedUsr).parent().addClass("selectedClass");
        $($dstCheckedUsr).parent().addClass("selectedClass");
        $($srcCheckedUsr).removeAttr('checked');
        $($dstCheckedUsr).removeAttr('checked');
        mapdSlectedUrs(_srcUsrDetails,_dstnUsrDetails,'prepend');
        var _autoSave = autoSaving(_srcUsrDetails,_dstnUsrDetails);
    }
});
function mapdSlectedUrs(src,dstn,csvId,csvName,migrateDstnFolderName,append,isFolder,automap,duplicate,pathException) {
    $("#mapdUsrs .paginationDiv").css("opacity","1");
    $('#mapdUsrs .paginationDiv select').prop('disabled', false);
    $('#mapdUsrs .paginationDiv input').prop('disabled', false);
    if(isFolder === true) {
        if ((src.userCloudName === "ONEDRIVE_BUSINESS_ADMIN" && dstn.dstnUserCloudName === "DROPBOX_BUSINESS") || (src.userCloudName === "DROPBOX_BUSINESS" && dstn.dstnUserCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
            var obj = {
                "fromCloudId": {
                    "id": src.userCloudId,
                },
                "toCloudId": {
                    "id": dstn.dstnUserCloudId,
                },
                "fromRootId": src.userRtFolId,
                "toRootId": dstn.dstnUserRtFolId
            };
        }
    }

    else{
        $("#srcCloudUsers input[cloudid=" + src.userCloudId + "]").attr('disabled', true);
        $("#dstCloudsUsers input[cloudid=" + dstn.dstnUserCloudId + "]").attr('disabled',true);
        var _obj ={
            "fromCloudId": {
                "id":src.userCloudId,
            },
            "toCloudId": {
                "id":dstn.dstnUserCloudId,
            }
        };
    }
    var _pathTD = 'FromCloudFuze';
    $(document).find("#chkInput").attr("disabled", false);
    $("#clearBtn").css("pointer-events",'auto');
    $("#clearBtn").css("cursor","pointer");
    $("#clearBtn").css("opacity","1");
    var _input,input;
    if ((src.userCloudName === "ONEDRIVE_BUSINESS_ADMIN" && dstn.dstnUserCloudName === "DROPBOX_BUSINESS") || (src.userCloudName === "DROPBOX_BUSINESS" && dstn.dstnUserCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
        if(src.folderPath === null || dstn.dstnFolderPath === null || src.folderPath === undefined || dstn.dstnFolderPath === undefined || src.folderPath === "undefined" || dstn.dstnFolderPath === "undefined"){
            if(mappingOptions.localStorageOperation(_obj,'check'))
                if(pathException === null || pathException === undefined || pathException === 'null' || pathException === 'undefined')
                    _input = '<td><input type="checkbox" name="inputMapdUrs" checked="true"></td>';

                else
                    _input = '<td><input type="checkbox" name="inputMapdUrs" disabled="disabled"></td>';
            else
            if(pathException === null || pathException === undefined || pathException === 'null' || pathException === 'undefined')
                _input = '<td><input type="checkbox" name="inputMapdUrs"></td>';
            else
                _input = '<td><input type="checkbox" name="inputMapdUrs" disabled="disabled"></td>';
        }
        if (src.folderPath !== null || dstn.dstnFolderPath !== null) {
            if (mappingOptions.folderOperation(obj, 'check'))
                if (duplicate === true) {
                    input1 = '<td><input type="checkbox" name="folderMapngCheckBox" disabled="disabled"></td>';
                } else
                if(pathException === null || pathException === undefined || pathException === 'null' || pathException === 'undefined')
                    input1 = '<td><input type="checkbox" name="folderMapngCheckBox" checked="true"></td>';
                else
                    input1 = '<td><input type="checkbox" name="folderMapngCheckBox" disabled="disabled"></td>';
            else if (duplicate === true) {
                input1 = '<td><input type="checkbox" name="folderMapngCheckBox" disabled="disabled"></td>';
            } else
            if(pathException === null || pathException === undefined || pathException === 'null' || pathException === 'undefined')
                input1 = '<td><input type="checkbox" name="folderMapngCheckBox"></td>';
            else
                input1 = '<td><input type="checkbox" name="folderMapngCheckBox" disabled="disabled"></td>';
            if((src.userCloudName === "ONEDRIVE_BUSINESS_ADMIN" && src.userRtFolId === '/') || (dstn.dstnUserCloudName === "ONEDRIVE_BUSINESS_ADMIN" && dstn.dstnUserRtFolId === '/')){
                input1 = '<td><input type="checkbox" name="folderMapngCheckBox" disabled="disabled"></td>';
            }
        }
    }
    else {
        if(src.folderPath === null || dstn.dstnFolderPath === null || src.folderPath === undefined || dstn.dstnFolderPath === undefined || src.folderPath === "undefined" || dstn.dstnFolderPath === "undefined"){
            if(mappingOptions.localStorageOperation(_obj,'check'))
                _input = '<td><input type="checkbox" name="inputMapdUrs" checked="true"></td>';
            else
                _input = '<td><input type="checkbox" name="inputMapdUrs"></td>';
        }
        if (src.folderPath !== null || dstn.dstnFolderPath !== null) {
            if (CsvOperation(_obj, 'check')) {
                input = '<td><input type="checkbox" name="csvMapngCheckBox" checked="true"></td>';
            } else {
                input = '<td><input type="checkbox" name="csvMapngCheckBox"></td>';
            }
        }
        if (src.folderPath === null || dstn.dstnFolderPath === null) {
            if (CsvOperation(_obj, 'check')) {
                input = '<td><input type="checkbox" name="csvMapngCheckBox" checked="true"></td>';
            } else {
                input = '<td><input type="checkbox" name="csvMapngCheckBox"></td>';
            }
        }
    }

    if(append != undefined)
        $("#mapdUsers tbody").prepend('<tr srcemail="'+src.userEmail+'" srcCldName="'+src.userCloudName+'" srcCldid="'+src.userCloudId+'" srcRt="'+src.userRtFolId+'" dstnemail="'+dstn.dstnUserEmail+'" dstnCldName="'+dstn.dstnUserCloudName+'" dstnCldid="'+dstn.dstnUserCloudId+'" dstnRt="'+dstn.dstnUserRtFolId+'" >' + _input + '<td style="width: 48%"><img src=" ../img/drive/circle/'+src.userCloudName+'.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+src.userEmail+'">'+emailMaxChar(src.userEmail,25)+'</span></td><td style="width: 48%"><img src=" ../img/drive/circle/'+dstn.dstnUserCloudName+'.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+dstn.dstnUserEmail+'">'+emailMaxChar(dstn.dstnUserEmail,25)+'</span></td><td><span style="margin-left: -63px;font-size: inherit;" class="closeBtn"><i class="fa fa-times" aria-hidden="true" ></i></span></td></tr>');
    else {
        if (src.folderPath == null || src.folderPath === "undefined") {
            if (!$("#mapdUsers tbody tr").hasClass("CsvMapd")) {
                $("#mapdUsers tbody").append('<tr class="automapRow" srcemail="' + src.userEmail + '" srcCldName="' + src.userCloudName + '" srcCldid="' + src.userCloudId + '" srcRt="' + src.userRtFolId + '" dstnemail="' + dstn.dstnUserEmail + '" srcFolderPath="/"  dstnFolderPath="/" dstnCldName="' + dstn.dstnUserCloudName + '" dstnCldid="' + dstn.dstnUserCloudId + '" dstnRt="' + dstn.dstnUserRtFolId + '" >' + _input + '<td style="width: 48%"><img src=" ../img/drive/circle/' + src.userCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+src.userEmail+'">' + emailMaxChar(src.userEmail, 25) + '</span></td><td style="width: 48%"><img src=" ../img/drive/circle/' + dstn.dstnUserCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+dstn.dstnUserEmail+'">' + emailMaxChar(dstn.dstnUserEmail, 25) + '</span></td><td><span style="margin-left: -63px;font-size: inherit;" class="closeBtn"><i class="fa fa-times" aria-hidden="true"></i></span></td></tr>');
            }
            else {
                var _html = '<tr class="CsvMapd" srcemail="' + src.userEmail + '" srcCldName="' + src.userCloudName + '" srcCldid="' + src.userCloudId + '" srcRt="' + src.userRtFolId + '" dstnemail="' + dstn.dstnUserEmail + '" srcFolderPath="/"  dstnFolderPath="/" dstnCldName="' + dstn.dstnUserCloudName + '" dstnCldid="' + dstn.dstnUserCloudId + '" dstnRt="' + dstn.dstnUserRtFolId + '" >' + _input + '<td style="width: 48%"><img src=" ../img/drive/circle/' + src.userCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+src.userEmail+'">' + emailMaxChar(src.userEmail, 25) + '</span><br/><span class="csvPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleSrce">/</span></td><td style="width: 48%"><img src=" ../img/drive/circle/' + dstn.dstnUserCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+dstn.dstnUserEmail+'">' + emailMaxChar(dstn.dstnUserEmail, 25) + '</span><br/><span class="csvPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleDstin">/</span></td><td><span style="margin-left: -63px;font-size: inherit;" class="closeBtn"><i class="fa fa-times" aria-hidden="true"></i></span></td></tr>';
                _html = _html.replace('TitleSrce', "/").replace('TitleDstin', "/");
                $("#mapdUsers tbody").prepend(_html);//append
            }
        }
        else {
            if ((src.userCloudName === "ONEDRIVE_BUSINESS_ADMIN" && dstn.dstnUserCloudName === "DROPBOX_BUSINESS") || (src.userCloudName === "DROPBOX_BUSINESS" && dstn.dstnUserCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
                if (!$("#mapdUsers tbody tr").hasClass("CsvMapd")) {
                    if (duplicate === true) {
                        if(pathException === null || pathException === undefined || pathException === 'null' || pathException === 'undefined')
                            var _html = '<tr class="folderRow" srcemail="' + src.userEmail + '" srcCldName="' + src.userCloudName + '" srcCldid="' + src.userCloudId + '" srcRt="' + src.userRtFolId + '" duplicateCache="' + duplicate + '" dstnemail="' + dstn.dstnUserEmail + '" srcFolderPath="' + src.folderPath + '"  dstnFolderPath="' + dstn.dstnFolderPath + '" dstnCldName="' + dstn.dstnUserCloudName + '" dstnCldid="' + dstn.dstnUserCloudId + '" dstnRt="' + dstn.dstnUserRtFolId + '" >' + input1 + '<td style="width: 48%"><img src=" ../img/drive/circle/' + src.userCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+src.userEmail+'">' + emailMaxChar(src.userEmail, 25) + '</span><br/><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleSrce">' + CFManageCloudAccountsAjaxCall.getMaxChars(src.folderPath, 25) + '</span></td><td style="width: 48%;"><img src=" ../img/drive/circle/' + dstn.dstnUserCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+dstn.dstnUserEmail+'">' + emailMaxChar(dstn.dstnUserEmail, 25) + '</span><span style="margin-right: 20%;float: right;font-size: inherit;"><i class="fa fa-exclamation-circle manualPath" data-toggle="tooltip" data-placement="bottom" title="Duplicate Pair" aria-hidden="true" style="color: red;"></i></span><br/><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleDstin">' + CFManageCloudAccountsAjaxCall.getMaxChars(dstn.dstnFolderPath, 25) + '</span></td><td><span style="margin-left: -63px;font-size: inherit;" class="closeBtn"><i class="fa fa-times" aria-hidden="true"></i></span></td></tr>';
                        else
                            var _html = '<tr class="folderRow" srcemail="' + src.userEmail + '" srcCldName="' + src.userCloudName + '" srcCldid="' + src.userCloudId + '" srcRt="' + src.userRtFolId + '" pathException="' + pathException + '"  duplicateCache="' + duplicate + '" dstnemail="' + dstn.dstnUserEmail + '" srcFolderPath="' + src.folderPath + '"  dstnFolderPath="' + dstn.dstnFolderPath + '" dstnCldName="' + dstn.dstnUserCloudName + '" dstnCldid="' + dstn.dstnUserCloudId + '" dstnRt="' + dstn.dstnUserRtFolId + '" >' + input1 + '<td style="width: 48%"><img src=" ../img/drive/circle/' + src.userCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+src.userEmail+'">' + emailMaxChar(src.userEmail, 25) + '</span><br/><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleSrce">' + CFManageCloudAccountsAjaxCall.getMaxChars(src.folderPath, 25) + '</span></td><td style="width: 48%;"><img src=" ../img/drive/circle/' + dstn.dstnUserCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+dstn.dstnUserEmail+'">' + emailMaxChar(dstn.dstnUserEmail, 25) + '</span><span style="margin-right: 20%;float: right;font-size: inherit;"><i class="fa fa-exclamation-circle pathException" data-toggle="tooltip" data-placement="bottom" title="The specified file or folder name is too long. The URL path for all files and folders must be 400 characters or less." aria-hidden="true" style="color: red;"></i></span><br/><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleDstin">' + CFManageCloudAccountsAjaxCall.getMaxChars(dstn.dstnFolderPath, 25) + '</span></td><td><span style="margin-left: -63px;font-size: inherit;" class="closeBtn"><i class="fa fa-times" aria-hidden="true"></i></span></td></tr>';
                    } else
                    if(pathException === null || pathException === undefined || pathException === 'null' || pathException === 'undefined')
                        var _html = '<tr class="folderRow" srcemail="' + src.userEmail + '" srcCldName="' + src.userCloudName + '" srcCldid="' + src.userCloudId + '" srcRt="' + src.userRtFolId + '"  duplicateCache="' + duplicate + '" dstnemail="' + dstn.dstnUserEmail + '" srcFolderPath="' + src.folderPath + '"  dstnFolderPath="' + dstn.dstnFolderPath + '" dstnCldName="' + dstn.dstnUserCloudName + '" dstnCldid="' + dstn.dstnUserCloudId + '" dstnRt="' + dstn.dstnUserRtFolId + '" >' + input1 + '<td style="width: 48%"><img src=" ../img/drive/circle/' + src.userCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+src.userEmail+'">' + emailMaxChar(src.userEmail, 25) + '</span><br/><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleSrce">' + CFManageCloudAccountsAjaxCall.getMaxChars(src.folderPath, 25) + '</span></td><td style="width: 48%"><img src=" ../img/drive/circle/' + dstn.dstnUserCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+dstn.dstnUserEmail+'">' + emailMaxChar(dstn.dstnUserEmail, 25) + '</span><br/><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleDstin">' + CFManageCloudAccountsAjaxCall.getMaxChars(dstn.dstnFolderPath, 25) + '</span></td><td><span style="margin-left: -63px;font-size: inherit;" class="closeBtn"><i class="fa fa-times" aria-hidden="true"></i></span></td></tr>';
                    else
                        var _html = '<tr class="folderRow" srcemail="' + src.userEmail + '" srcCldName="' + src.userCloudName + '" srcCldid="' + src.userCloudId + '" srcRt="' + src.userRtFolId + '"  pathException="' + pathException + '"  duplicateCache="' + duplicate + '" dstnemail="' + dstn.dstnUserEmail + '" srcFolderPath="' + src.folderPath + '"  dstnFolderPath="' + dstn.dstnFolderPath + '" dstnCldName="' + dstn.dstnUserCloudName + '" dstnCldid="' + dstn.dstnUserCloudId + '" dstnRt="' + dstn.dstnUserRtFolId + '" >' + input1 + '<td style="width: 48%"><img src=" ../img/drive/circle/' + src.userCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+src.userEmail+'">' + emailMaxChar(src.userEmail, 25) + '</span><br/><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleSrce">' + CFManageCloudAccountsAjaxCall.getMaxChars(src.folderPath, 25) + '</span></td><td style="width: 48%"><img src=" ../img/drive/circle/' + dstn.dstnUserCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+dstn.dstnUserEmail+'">' + emailMaxChar(dstn.dstnUserEmail, 25) + '</span><span style="margin-right: 20%;float: right;font-size: inherit;"><i class="fa fa-exclamation-circle pathException" data-toggle="tooltip" data-placement="bottom" title="The specified file or folder name is too long. The URL path for all files and folders must be 400 characters or less." aria-hidden="true" style="color: red;"></i></span><br/><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleDstin">' + CFManageCloudAccountsAjaxCall.getMaxChars(dstn.dstnFolderPath, 25) + '</span></td><td><span style="margin-left: -63px;font-size: inherit;" class="closeBtn"><i class="fa fa-times" aria-hidden="true"></i></span></td></tr>';
                    _html = _html.replace('TitleSrce', src.folderPath).replace('TitleDstin', dstn.dstnFolderPath);
                    if(automap === true){
                        $("#mapdUsers tbody").append(_html);
                    }
                    else
                        $("#mapdUsers tbody").prepend(_html);//append
                }
            }
            else {
                var _html = '<tr style="width: 100%" id ="csvRow" class="CsvMapd" csvID="' + csvId + '" csvName="' + csvName + '" srcemail="' + src.userEmail + '" srcCldName="' + src.userCloudName + '" srcCldid="' + src.userCloudId + '" srcRt="' + src.userRtFolId + '"srcFolderPath="' + encodeURI(src.folderPath) + '" srcPathRootFolderId="' + src.srcPathRootFolderId + '" migrateSrcFolderName="' + src.migrateSrcFolderName + '" dstnFolderPath="' + encodeURI(dstn.dstnFolderPath) + '"  dstnemail="' + dstn.dstnUserEmail + '" dstnCldName="' + dstn.dstnUserCloudName + '" dstnCldid="' + dstn.dstnUserCloudId + '" dstnRt="' + dstn.dstnUserRtFolId + '" dstnPathRootFolderId="' + dstn.dstnPathRootFolderId + '" migrateDstnFolderName ="' + migrateDstnFolderName + '" >' + input + '<td style="width: 48%"><img src=" ../img/drive/circle/' + src.userCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+src.userEmail+'">' + emailMaxChar(src.userEmail, 25) + '<br/><span class="csvPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleSrce">' + CFManageCloudAccountsAjaxCall.getMaxChars(src.folderPath, 25) + '</span></td><td style="width: 48%"><img src=" ../img/drive/circle/' + dstn.dstnUserCloudName + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="'+dstn.dstnUserEmail+'">' + emailMaxChar(dstn.dstnUserEmail, 25) + '<br/><span class="csvPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleDstin">' + CFManageCloudAccountsAjaxCall.getMaxChars(dstn.dstnFolderPath, 25) + '</span></td><td><span style="margin-left: -63px;font-size: inherit;" class="closeBtn"><i class="fa fa-times" aria-hidden="true"></i></span></td></tr>';
                _html = _html.replace('TitleSrce', src.folderPath).replace('TitleDstin', dstn.dstnFolderPath);
                $("#mapdUsers tbody").prepend(_html);//append
            }
        }
        $.each($('.csvPath'), function() {
            if ($(this).text() !== "/" || null) {
                $('.download').css('pointer-events', 'auto');
                $('.download').css('opacity', '1');
                $('#help').css('visibility', 'visible');
            }
        });
    }
    if($('#mapdUsrs input[name="csvMapngCheckBox"]').prop('checked') === true || $('#mapdUsrs input[name="inputMapdUrs"]').prop('checked') === true || $('#mapdUsrs input[name="folderMapngCheckBox"]').prop('checked') === true)
    {
        $('#forNextMove').removeClass('disabled');
    }
    if($('input[name="inputMapdUrs"]:checked').length === 30 || $('input[name="csvMapngCheckBox"]:checked').length === 30 || $('input[name="folderMapngCheckBox"]:checked').length === 30){
        $("#chkInput").prop('checked', true);
    }
    else{
        $(document).find("#chkInput").prop( "checked", false );
    }
    if($("#chkInput:checked").length){
        var data = $('#mapdUsers input[name=folderMapngCheckBox]').length;
        for (i = 0; i < data; i++) {
            if ($('#mapdUsers input[name=folderMapngCheckBox]')[i].hasAttribute('disabled')) {
                $("input[name=folderMapngCheckBox]")[i].checked = false;
            } else {
                $("input[name=folderMapngCheckBox]")[i].checked = true;
            }
        }
    }

    if(!Number($("#mapdUsrs .paginationCF input").val())){
        $("#mapdUsrs .paginationCF input").val(1);
        $("#mapdUsrs .paginationCF span").last().text( "1 of 1" );
    }
    $('[data-toggle="tooltip"]').tooltip();
}
$(document).on('click','.closeBtn',function(){
    if($('#mapdUsers input[name="csvMapngCheckBox"]').length === 0)
        localStorage.removeItem('FolderChecked');
    localStorage.removeItem('CsvEmailChecked');
    if($('#mapdUsers input[name="inputMapdUrs"]').length === 0){
        localStorage.removeItem('selectedEmail');
        localStorage.removeItem('selectedMappings');
    }
    if($('#mapdUsers input[name="folderMapngCheckBox"]').length === 0){
        localStorage.removeItem('folderEmail');
        localStorage.removeItem('folderMappings');
    }
    localStorage.removeItem("validator");
    $('#csvFileUpload').removeAttr('disabled');
    $('#csvFile').removeAttr('disabled');
    var parent = $(this).parents("tr");
    var _appendSrcId = $(this).parents("tr").attr("srcCldid");
    var _appendDstnId = $(this).parents("tr").attr("dstnCldid");
    var _appendSrcPath =$(this).parents("tr").attr("srcfolderpath");
    var _appendDstnPath = $(this).parents("tr").attr("dstnfolderpath");
    var srccldname =$(this).parents("tr").attr("srccldname");
    var dstncldname = $(this).parents("tr").attr("dstncldname");
    var fromRootId = $(this).parents("tr").attr('srcrt');
    var toRootId = $(this).parents("tr").attr('dstnrt');
    var duplicateCache = $(this).parents("tr").attr('duplicateCache');
    var _path = $(this).parents("tr").attr('pathException');
    $("#srcCloudUsers input[cloudid=" + _appendSrcId + "]").attr('disabled',false).parent().removeClass('selectedClass').parent().removeClass('selectedClass');
    $("#dstCloudsUsers input[cloudid=" + _appendDstnId + "]").attr('disabled',false).parent().removeClass('selectedClass').parent().removeClass('selectedClass');
    $("#srcCloudUsers input[cloudid=" + _appendSrcId + "]").attr('disabled',false).parent().parent().removeClass('selectedClass');
    $("#dstCloudsUsers  input[cloudid=" + _appendDstnId + "]").attr('disabled',false).parent().parent().removeClass('selectedClass');

    $("#srcCloudUsers .usr-folder[user-id=" + _appendSrcId + "]").parent().removeClass('selectedClass');
    $("#dstCloudsUsers  .usr-folder[user-id=" + _appendDstnId + "]").parent().removeClass('selectedClass');
    if(srccldname === 'DROPBOX_BUSINESS' && dstncldname === 'ONEDRIVE_BUSINESS_ADMIN' || srccldname === 'ONEDRIVE_BUSINESS_ADMIN' && dstncldname === 'DROPBOX_BUSINESS')
    {
        if(_path === null ||_path === 'null' ||_path === undefined ||_path === 'undefined')
            var _data = "sourceCloudId="+_appendSrcId+"&destCloudId="+_appendDstnId+"&fromRootFolderId="+encodeURIComponent(fromRootId)+"&toRootFolderId="+encodeURIComponent(toRootId)+"&fromFolderPath="+encodeURIComponent(_appendSrcPath)+"&toFolderPath="+encodeURIComponent(_appendDstnPath);
        else
            var _data = "sourceCloudId="+_appendSrcId+"&destCloudId="+_appendDstnId+"&fromRootFolderId="+encodeURIComponent(fromRootId)+"&toRootFolderId="+encodeURIComponent(toRootId)+"&fromFolderPath="+encodeURIComponent(_appendSrcPath)+"&toFolderPath="+encodeURIComponent(_appendDstnPath)+"&pathException";
    } else{
        var _data = "sourceCloudId="+_appendSrcId+"&destCloudId="+_appendDstnId;
    }
    var _local = $(this);
    $.ajax({
        type: "DELETE",
        url: apicallurl + "/mapping/delete/mapplist?" + _data,
        async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data) {
            var _obj = {
                "fromCloudId": {
                    "id": _appendSrcId,
                },
                "toCloudId": {
                    "id": _appendDstnId,
                }
            };
            mappingOptions.localStorageOperation(_obj, 'delete');
            if($('#mapdUsers input[name=folderMapngCheckBox]').length) {
                var _obj = {
                    "fromCloudId": {
                        "id": _appendSrcId
                    },
                    "toCloudId": {
                        "id": _appendDstnId
                    },
                    "fromRootId": fromRootId,
                    "toRootId": toRootId,
                    "sourceFolderPath": _appendSrcPath,
                    "destFolderPath": _appendDstnPath,
                    "folder": "true"
                };
                var _objEmail = {
                    "fromMailId": parent.attr('srcemail'),
                    "fileName": parent.attr('srcemail'),
                    "toMailId": parent.attr('dstnemail'),
                    "fromCloudName": srccldname,
                    "toCloudName": dstncldname

                };
                mappingOptions.folderOperation(_obj, 'delete');
                mappingOptions.folderOperation(_objEmail, 'delete1');
            }
            $("#srcCloudUsers input[cloudid = " + $(this).parents("tr").attr("srcCldid") + "]").parent().removeClass("selectedClass");
            $("#dstCloudsUsers input[cloudid = " + $(this).parents("tr").attr("dstnCldid") + "]").parent().removeClass("selectedClass");
            $(_local).parents("tr").remove("");
            if($("#chkInput:checked").length){
                var data = $('#mapdUsers input[name=folderMapngCheckBox]').length;
                for (i = 0; i < data; i++) {
                    if ($('#mapdUsers input[name=folderMapngCheckBox]')[i].hasAttribute('disabled')) {
                        $("input[name=folderMapngCheckBox]")[i].checked = false;
                    } else {
                        $("input[name=folderMapngCheckBox]")[i].checked = true;
                    }
                }
            }
            var _pgNo = $("#mapdUsrs .paginationDiv input.input-sm").val();
            if (!$("#mapdUsrs .closeBtn").length) {
                $('.download').css('pointer-events', 'none');
                $('.download').css('opacity', '0.5');
                $('#help').css('visibility', 'hidden');
                $(document).find("#chkInput").attr("disabled", true).prop("checked", false);
                $("#clearBtn").css("pointer-events",'none');
                $("#clearBtn").css("cursor","not-allowed");
                $("#clearBtn").css("opacity","0.6");
                $("#forNextMove").addClass("disabled");
                if (_pgNo == 1) {
                    $("#mapdUsrs .paginationCF span").last().text("0 of 0");
                    $("#mapdUsrs .paginationDiv").css("opacity", "0.6");
                    $('#mapdUsrs .paginationDiv select').prop('disabled', true);
                    $('#mapdUsrs .paginationDiv input').prop('disabled', true);

                    $("#mapdUsrs .paginationDiv input.input-sm").val(0);
                }
                $('#mapdUsrs #csvFileUpload').css({'opacity': '1', 'cursor': 'pointer'});
                $('#mapdUsrs #csvFileUpload').click(true);
                $('#mapdUsrs .fa-exchange').removeClass('disabled');
            }
            else {
                migrationMapping.appendMapping(_pgNo, srccldname, dstncldname);

            }

        }
    });

});
//For AutoMapping
$(document).on('click','#mapdUsrs .fa-exchange',function(){
    localStorage.removeItem("Csv");
    localStorage.removeItem("FolderChecked");
    localStorage.removeItem("CsvEmailChecked");
    // if($(this).hasClass('disabeled'))
    //     return false;
    $("#CFShowLoading").css("display","");
    $("#mapdUsers tbody").html('');
    $("div").removeClass('selectedClass');
    $("#srcUsrs input:radio").attr('disabled',false);
    $("#dstnUsrs input:radio").attr('disabled',false);
    $("#CFShowLoading").attr("autoMap","true");
    deleteMapping();
    $("#mapdUsrs .paginationCF input").val(1);
    autoMapping(1,$("#mapdUsrs select").val(),localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"));
    $("#srcUsrs .custom-search-input input").val('');
    $("#dstnUsrs .custom-search-input input").val('');
    var searchData = JSON.parse(localStorage.getItem("searchData"));
    searchData.mapping = false;
    localStorage.setItem("searchData",JSON.stringify(searchData));
    //disableMapping('add');

});
function autoMapping(pgNo,pgSize,_srcMapdCldId,_dstnMapdCldId) {
    if(pgNo == undefined)
        pgNo = 1;
    //if(pgSize == undefined )
    pgSize = parseInt($('#mapdUsrs select').val());
    $("#CFShowLoading").css("display","");
    var _isError = false;
    var _data = "sourceCloudId="+_srcMapdCldId+"&destCloudId="+_dstnMapdCldId+"&pageNo="+pgNo+"&pageSize="+pgSize;
    var _xhr = $.ajax({
        type: "POST",
        url: apicallurl + "/mapping/user/clouds/list?" + _data,
        async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data) {

            $("#mapdUsers tbody").html('');
            $('#paginationMapng').empty();
            $('#paginationMapng').removeData("twbs-pagination");
            $('#paginationMapng').unbind("page");
            if(data.length === 0){
                setTimeout(function(){
                    $("#CFShowLoading").attr("autoMap","false");
                    $("#CFShowLoading").css("display","none");
                }, 1000);
            }
            else{
                var _totPages = Math.ceil(data[0].noOfMappedCloudPressent/$('#mapdUsrs select').val());

                var _txt = 1 + " of " + _totPages;

                $("#mapdUsrs .paginationCF span").last().text(_txt );
                $("#mapdUsrs .paginationCF input").val(1 );

                $('#paginationMapng').twbsPagination({
                    totalPages: _totPages,
                    visiblePages: 6,
                    startPage: pgNo,
                    next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
                    prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
                    first: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
                    last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
                    onPageClick: function (event, page) {
                        appendingPrevMapping(page);
                    }
                });
                for (var i = 0; i < data.length; i++) {
                    var _dataVal = data[i];
                    var csvID = _dataVal.csvId;
                    var csvName =  _dataVal.csvName;
                    var migrateDstnFolderName = _dataVal.migrateFolderName;
                    if ((_dataVal.sourceCloudDetails.name === "ONEDRIVE_BUSINESS_ADMIN" && _dataVal.destCloudDetails.name === "DROPBOX_BUSINESS") || (_dataVal.sourceCloudDetails.name === "DROPBOX_BUSINESS" && _dataVal.destCloudDetails.name === "ONEDRIVE_BUSINESS_ADMIN")) {
                        var _srcUsrDetails = {
                            "userEmail": _dataVal.sourceCloudDetails.emailId,
                            "userCloudName": _dataVal.sourceCloudDetails.name,
                            "userCloudId": _dataVal.sourceCloudDetails.id,
                            "userRtFolId": _dataVal.sourceCloudDetails.rootFolderId,
                            "folderPath":'/'
                        }
                        var _dstnUsrDetails = {
                            "dstnUserEmail": _dataVal.destCloudDetails.emailId,
                            "dstnUserCloudName": _dataVal.destCloudDetails.name,
                            "dstnUserCloudId": _dataVal.destCloudDetails.id,
                            "dstnUserRtFolId": _dataVal.destCloudDetails.rootFolderId,
                            "dstnFolderPath":'/'
                        }
                        $("input[cloudid=" + _srcUsrDetails.userCloudId + "]").parent().parent().addClass("selectedClass");
                        $("input[cloudid=" + _dstnUsrDetails.dstnUserCloudId + "]").parent().parent().addClass("selectedClass");
                        var isFolder = true,automap=true;
                        mapdSlectedUrs(_srcUsrDetails, _dstnUsrDetails, '','','',undefined,isFolder,automap,_dataVal.duplicateCache,_dataVal.pathException);
                    } else {
                        var _srcUsrDetails = {
                            "userEmail": _dataVal.sourceCloudDetails.emailId,
                            "userCloudName": _dataVal.sourceCloudDetails.name,
                            "userCloudId": _dataVal.sourceCloudDetails.id,
                            "userRtFolId": _dataVal.sourceCloudDetails.rootFolderId
                        }
                        var _dstnUsrDetails = {
                            "dstnUserEmail": _dataVal.destCloudDetails.emailId,
                            "dstnUserCloudName": _dataVal.destCloudDetails.name,
                            "dstnUserCloudId": _dataVal.destCloudDetails.id,
                            "dstnUserRtFolId": _dataVal.destCloudDetails.rootFolderId
                        }
                        $("input[cloudid=" + _srcUsrDetails.userCloudId + "]").parent().parent().addClass("selectedClass");
                        $("input[cloudid=" + _dstnUsrDetails.dstnUserCloudId + "]").parent().parent().addClass("selectedClass");
                        mapdSlectedUrs(_srcUsrDetails, _dstnUsrDetails);
                    }
                }

                showNotyNotification("notify", "Auto-mapping has been successfully completed.");
                migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), 1, $("#srcCloudUsers select").val(), 'source');
                migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), 1, $("#dstCloudsUsers select").val(), 'destination');
                setTimeout(function () {
                    $("#CFShowLoading").attr("autoMap", "false");
                    $("#CFShowLoading").css("display", "none");
                }, 1000);
            }
        },
        error: function (data) {
            _isError = true;
            appendingPrevMapping();
            showNotyNotification("notify","Auto-mapping has been successfully completed.");
            migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),1, $("#srcCloudUsers select").val(),'source');
            migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),1, $("#dstCloudsUsers select").val(),'destination');
        }
    });
    // setTimeout(function () {
    //     if(_xhr && _xhr.readyState != 4 && !_isError){
    //         _xhr.abort();
    //         appendingPrevMapping();
    //         //showNotyNotification("notify","Auto-mapping has been successfully completed.");
    //         migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),1, $("#srcCloudUsers select").val(),'source');
    //         migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),1, $("#dstCloudsUsers select").val(),'destination');
    //     }
    // }, 2 * 60 *1000);
}
function autoSaving(_srcMapdCldId,_dstnMapdCldId,isFolder) {
    var _autoSave;
    if(_srcMapdCldId.folderPath !== "undefined" || _dstnMapdCldId.dstnFolderPath !== "undefined" || _srcMapdCldId.folderPath !== null || _dstnMapdCldId.dstnFolderPath !== null)
    {
        var _data = "sourceCloudId="+_srcMapdCldId.userCloudId+"&destCloudId="+_dstnMapdCldId.dstnUserCloudId+"&sourcePath="+encodeURIComponent(_srcMapdCldId.folderPath)+"&destPath="+encodeURIComponent(_dstnMapdCldId.dstnFolderPath)+"&sourceFolderId="+ encodeURIComponent(_srcMapdCldId.userRtFolId)+"&destFolderId="+ encodeURIComponent(_dstnMapdCldId.dstnUserRtFolId);
        var _url = apicallurl + "/mapping/user/unmapped/list?" + _data + "&isFolder=true";
    }
    else{
        var _data = "sourceCloudId="+_srcMapdCldId.userCloudId+"&destCloudId="+_dstnMapdCldId.dstnUserCloudId;
        var _url =   apicallurl + "/mapping/user/unmapped/list?" + _data;
    }
    $.ajax({
        type: "POST",
        url: _url,
        // async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data) {
            _autoSave = data;
            if($("#mapdUsers tbody tr").length > 30){
                appendingPrevMapping(1);
                $("#mapdUsers tbody tr").last().remove();
            }
            if(isFolder === true){
                if($("#mapdUsers tbody tr").length > 29){
                    appendingPrevMapping(1);
                    $("#mapdUsers tbody tr").last().remove();
                }
            }
            setTimeout(function(){
                $("#CFShowLoading").css("display","none");
            }, 2000);
            if(isFolder === true) {
                if (_autoSave.length !== 0) {
                    mapdSlectedUrs(_srcMapdCldId, _dstnMapdCldId, '', '', '', undefined, isFolder, '', _autoSave[0].duplicateCache,_autoSave[0].pathException);
                }
            }
        }
    });
    return _autoSave;
}
//Searching in Mapped User
$(document).on('click','#mapdUsrs .custom-search-input button',function(){
    if(!$("#mapdUsrs .custom-search-input input").val().length)
        return false;

    var searchData = JSON.parse(localStorage.getItem("searchData"));
    searchData.mapping = true;
    localStorage.setItem("searchData",JSON.stringify(searchData));
    autoMappingSearch($("#mapdUsrs .custom-search-input input").val().trim());
});
function autoMappingSearch(SearchTerm,pageNo) {
    if(pageNo == undefined)
        pageNo = 1;

    var pageSize = $("#mapdUsrs .paginationDiv select").val();

    if($('#mapdUsrs .custom-search-input input').val()){
        var _sortFun = migrationMapping.sortBy();
    }
    $.ajax({
        type: "GET",
        url: apicallurl + "/mapping/user/search/list?searchMapp=" + SearchTerm + "&sourceCloudId=" + localStorage.getItem("multiUsrSrcCldId") + "&destCloudId=" + localStorage.getItem("multiUsrDstnCldId") + "&pageSize=" + pageSize +"&pageNo=" + pageNo+_sortFun,
        async : false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data) {
            if(data.length == 0){
                showNotyNotification("error","No Results Found");
                $("#chkInput").attr("disabled", true);
                $("#clearBtn").css("pointer-events",'none');
                $("#clearBtn").css("cursor","not-allowed");
                $("#clearBtn").css("opacity","0.6");
                var _txt ="0 of 0";

                $("#mapdUsrs .paginationCF span").last().text(_txt );
                $("#mapdUsrs .paginationCF input").val(0 );
            }
            else{
                $("#mapdUsers tbody").html("");
                var _totPages = Math.ceil(data[0].noOfMappedCloudPressent/$('#mapdUsrs select').val());
                var _txt = pageNo + " of " + _totPages;

                $("#mapdUsrs .paginationCF span").last().text(_txt );
                $("#mapdUsrs .paginationCF input").val(pageNo );
                if(pageNo == 1){
                    $('#paginationMapng').empty();
                    $('#paginationMapng').removeData("twbs-pagination");
                    $('#paginationMapng').unbind("page");
                    if(data.length != 0)
                    {

                        var _totPages = Math.ceil(data[0].noOfMappedCloudPressent/$('#mapdUsrs select').val());

                        var _txt = 1 + " of " + _totPages;

                        $("#mapdUsrs .paginationCF span").last().text(_txt );
                        $("#mapdUsrs .paginationCF input").val(1 );

                        $('#paginationMapng').twbsPagination({
                            totalPages: _totPages,
                            visiblePages: 6,
                            startPage:1,
                            next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
                            prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
                            first:'<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
                            last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
                            onPageClick: function (event, page){
                                autoMappingSearch(SearchTerm,page);
                            }
                        });
                    }
                }

                for(var i=0;i<data.length;i++){
                    var _dataVal = data[i];
                    var csvID = _dataVal.csvId;
                    var csvName =  _dataVal.csvName;
                    var migrateDstnFolderName = _dataVal.migrateFolderName;
                    var _srcUsrDetails =  {
                        "userEmail": _dataVal.sourceCloudDetails.emailId,
                        "userCloudName": _dataVal.sourceCloudDetails.name,
                        "userCloudId": _dataVal.sourceCloudDetails.id,
                        "userRtFolId": _dataVal.sourceCloudDetails.rootFolderId,
                        "folderPath": _dataVal.sourceCloudDetails.folderPath,
                        "srcPathRootFolderId": _dataVal.sourceCloudDetails.pathRootFolderId,
                        "migrateSrcFolderName": _dataVal.sourceCloudDetails.migrateFolderName
                    }
                    var _dstnUsrDetails =  {
                        "dstnUserEmail": _dataVal.destCloudDetails.emailId,
                        "dstnUserCloudName":_dataVal.destCloudDetails.name ,
                        "dstnUserCloudId":_dataVal.destCloudDetails.id,
                        "dstnUserRtFolId": _dataVal.destCloudDetails.rootFolderId,
                        "dstnFolderPath": _dataVal.destCloudDetails.folderPath,
                        "dstnPathRootFolderId": _dataVal.destCloudDetails.pathRootFolderId
                    }
                    if ((_srcUsrDetails.userCloudName === "ONEDRIVE_BUSINESS_ADMIN" && _dstnUsrDetails.dstnUserCloudName === "DROPBOX_BUSINESS") || (_srcUsrDetails.userCloudName === "DROPBOX_BUSINESS" && _dstnUsrDetails.dstnUserCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
                        if (_srcUsrDetails.folderPath === null && _dstnUsrDetails.dstnFolderPath === null) {
                            _srcUsrDetails.folderPath = '/';
                            _dstnUsrDetails.dstnFolderPath = '/';
                        }
                        var isFolder = true;
                        mapdSlectedUrs(_srcUsrDetails, _dstnUsrDetails, csvID, csvName, migrateDstnFolderName, undefined, isFolder,'',_dataVal.duplicateCache,_dataVal.pathException);
                        if( $('#mapdUsers input[name= "inputMapdUrs"]').length > 0) {
                            var checkedLength = $('#mapdUsers input[name= "inputMapdUrs"]:checked').length + $('#mapdUsers input[name="folderMapngCheckBox"]:checked').length;
                            var totalLength = $('#mapdUsers input[name="inputMapdUrs"]').length + $('#mapdUsers input[name="folderMapngCheckBox"]').length;
                        }
                        else{
                            var checkedLength = $('#mapdUsers input[name= "folderMapngCheckBox"]:checked').length;
                            var totalLength = $('#mapdUsers input[name="folderMapngCheckBox"]').length;
                        }
                        if (checkedLength == totalLength) {
                            $('#chkInput').prop('checked', true);

                        }
                        else{
                            $('#chkInput').prop('checked', false);
                        }

                    }
                    else {
                        mapdSlectedUrs(_srcUsrDetails, _dstnUsrDetails, csvID, csvName, migrateDstnFolderName,undefined,'','');
                        if( $('#mapdUsers input[name= "csvMapngCheckBox"]').length > 0) {
                            var checkedLength = $('#mapdUsers input[name= "inputMapdUrs"]:checked').length + $('#mapdUsers input[name="csvMapngCheckBox"]:checked').length;
                            var totalLength = $('#mapdUsers input[name="inputMapdUrs"]').length + $('#mapdUsers input[name="csvMapngCheckBox"]').length;
                        }
                        else{
                            var checkedLength = $('#mapdUsers input[name= "inputMapdUrs"]:checked').length;
                            var totalLength = $('#mapdUsers input[name="inputMapdUrs"]').length;
                        }
                        if (checkedLength == totalLength) {
                            $('#chkInput').prop('checked', true);

                        }
                        else{
                            $('#chkInput').prop('checked', false);
                        }
                    }
                }
            }

        }
    });
}

//pagination
$('#paginationSrc,#paginationMapng,#paginationDtn').twbsPagination({
    totalPages: 1,
    visiblePages: 4,
    next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
    prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
    first:'<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
    last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
    onPageClick: function (event, page){}
});

//Editing the path
/*$(document).on('click','.fa-pencil',function(){
    $(this).parent().css("display", "none");
    $(this).parent().siblings().css("display", "");
});*/
$(document).on('click','.Job_NAme',function(){
    $(this).hide();
    $('.Editing_Jobname').removeAttr("disabled");
    $(".Editing_Jobname").focus();
    $("#checkTimediv").css("display","");
    return false;
});
$(document).on('click','.fa-check',function(){
    var jobEmptyval = $(".Editing_Jobname").val();
    var _jobid  =  localStorage.getItem('jobId');
    if(!jobEmptyval || jobEmptyval == "" ){
        $("#jobEmptyPopup").css("display","");

    }
    else{
        checkDuplicateJobname(jobEmptyval,_jobid);
        //localStorage.setItem('jobName',$(".Editing_Jobname").val());

    }
    return false;
});
function checkDuplicateJobname(jobEmptyval,_jobid){
    $.ajax({
        type: "GET",
        //  url: url,
        //  _changeUrl =  "/move/newmultiuser/update/" + _jobid +"?jobName=" + _jobName + "&migrateFolderName=" + _path + "&isDeltaMigration="+_DeltaVal+mappingOptions.migrationOptionsChecked()+ "&withPermissions=true&unsupportedFiles="+_ZipVal;

        url: apicallurl +"/move/multiuser/jobValidation/"+ _jobid +"?jobName=" + jobEmptyval,
        aync : false,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            localStorage.setItem('jobName',data);
            $("#jobEmptyPopup").css("display","none");
            $('.Editing_Jobname').attr("disabled","disabled");
            $("#checkTimediv").css("display","none");
            $(".Job_NAme").css("display","");
        },
        complete: function (xhr) {
            if (xhr.status === 409) {
                $("#jobEmptyPopup").css("display","");
                $("#jobEmptyPopup").text('Job Name already exists.');
                //$(".Editing_Jobname").val(localStorage.getItem('jobName'))
                $("#checkTimediv").css("display","");
                $(".Job_NAme").css("display","none");
                $('.Editing_Jobname').removeAttr("disabled");
                $(".Editing_Jobname").focus();
            }
        }

    });
}

$(document).on('click','#checkTimediv .fa-times',function(){
    var s = localStorage.getItem('jobName');
    $(".Editing_Jobname").val(s);
    $(".Job_NAme").css("display","");
    $("#checkTimediv").css("display","none");
    var jobEmptyval = $(".Editing_Jobname").val();
    if(!jobEmptyval || jobEmptyval == "" ){
        $("#jobEmptyPopup").css("display","");
    }
    else{
        $("#jobEmptyPopup").css("display","none");
        $('.Editing_Jobname').attr("disabled","disabled");
    }
    return false;
});
$(document).on('keyup','.Editing_Jobname',function () {
    if($(".Editing_Jobname").val().length === 0) {
        var jobEmptyval = $(".Editing_Jobname").val();
        if (!jobEmptyval || jobEmptyval == "") {
            $("#jobEmptyPopup").css("display", "");
        }
        else {
            $("#jobEmptyPopup").css("display", "none");
        }
    }
});
$(document).on('keypress','.Editing_Jobname',function (e) {
    var filter = /^([a-zA-Z0-9 _\.\-])+$/;
    if($(".Editing_Jobname").val().length === 0) {

        if (e.key == ' ') {
            return false;
        }
        if (!filter.test(e.key)) {
            $("#jobEmptyPopup").css("display","");
            $("#jobEmptyPopup").text(e.key +' is not allowed.');
            return false;
        }
        else{
            $("#jobEmptyPopup").css("display","none");
            $("#jobEmptyPopup").text('Job Name cannot be empty.');
        }
    }
    if (!filter.test(e.key)) {
        $("#jobEmptyPopup").css("display","");
        $("#jobEmptyPopup").text(e.key +' is not allowed.');
        return false;
    }
    else{
        $("#jobEmptyPopup").css("display","none");
        $("#jobEmptyPopup").text('Job Name cannot be empty.');
    }
});
/*function jobNamCheck (a) {
    var filter = /^([a-zA-Z0-9 _\.\-])+$/;
    if (!filter.test(a)) {
        $("#jobEmptyPopup").css("display","");
        $("#jobEmptyPopup").text(a +'is not allowed.');
        return false;
    }

}*/





//Initiating Migration
function initiateMigration(_mappingSrcUrs){
    $("#mapdUsers tbody tr input[name=inputMapdUrs]:checked").each(function() {
        var _selectedTrValue =  $(this).parents('tr');
        var moveFileObject = {
            "fromId": $(_selectedTrValue).attr("srcrt"),
            "fromCId": $(_selectedTrValue).attr("srccldid"),
            "toId": $(_selectedTrValue).attr("dstnrt"),
            "toCId": $(_selectedTrValue).attr("dstncldid"),
            "fromMailId":  $(_selectedTrValue).attr("srcemail"),
            "toEmailId": $(_selectedTrValue).attr("dstnemail"),
            "fromCloudName": $(_selectedTrValue).attr("srccldname"),
            "toCloudName": $(_selectedTrValue).attr("dstncldname"),
            "notify": false,
            "userEmails": null,
            "isCopy": true,
            "multiUserMove": true,
            "destinationFolderName": 'FromCloudFuze'
        };
        multiUsrMoveNewCall(moveFileObject, []);
    });
}
//clearing all the data in mapped user
$(document).on('click','#clearBtn',function(){
    $("#CFShowLoading").css("display","");
    $("#mapdUsers tbody").html('');
    $("div").removeClass('selectedClass');
    $("span").removeClass('selectedClass');
    $("#srcUsrs input:radio").attr('disabled',false);
    $("#dstnUsrs input:radio").attr('disabled',false);
    //disableMapping('remove');
    $('#csvFileUpload').removeAttr('disabled');
    $('#csvFile').removeAttr('disabled');
    deleteMapping(true);
    localStorage.removeItem('FolderChecked');
    localStorage.removeItem('CsvEmailChecked');
    localStorage.removeItem('csvMigrationData');
    localStorage.removeItem("validator");
    if($("#mapdUsers tbody").html() == "")
    {
        $('#mapdUsrs #csvFileUpload').css({'opacity': '1','cursor':'pointer'});
        $('#mapdUsrs #csvFileUpload').click(true);
        $('#mapdUsrs .fa-exchange').removeClass('disabled');
    }
    $('.download').css('pointer-events', 'none');
    $('.download').css('opacity', '0.5');
    $('#help').css('visibility','hidden');
});

//Migration Screens

$(document).on('click','.moveWorksapceList .fa-plus',function(){
    $(document).find('.moveWorksapceList i.fa-minus').removeClass('fa-minus').addClass('fa-plus');
    $(document).find('.moveReportDetails').html('').css("display","none");
    $(this).closest("tr").find("i.fa-plus").addClass('fa-minus').removeClass('fa-plus');
    //   var _value = $(this).attr('jobtype');
    var _htmlCode = appendWorkspaceReport(getWorkspaceReport($(this).parents("tr").attr('wrkspaceid'),'all',1));
    //    var count = CFManageCloudAccountsAjaxCall.fileFolderCount($(this).parents("tr").attr('wrkspaceid'),'',_value);
    var count = CFManageCloudAccountsAjaxCall.fileFolderCount($(this).parents("tr").attr('wrkspaceid'));
    _htmlCode = _htmlCode.replace('TotalCounting',(count.totalFiles + count.totalFolders)).replace('ProcessedCounting',count.processedCount).replace('ErrorCounting',count.conflictCount);
    $(this).parents("tr").next(".moveReportDetails").append(_htmlCode).css("display","table-row");
    moveReportPagination($(this).parents("tr").attr('totcount'),$(this).parents("tr").attr('wrkspaceid'),'all');
});
$(document).on('click','.moveWorksapceList .fa-minus',function(){
    $(document).find('.moveWorksapceList i.fa-minus').removeClass('fa-minus').addClass('fa-plus');
    $(document).find('.moveReportDetails').html('').css("display","none");

});
$(document).on('click','.moveWorksapceList .fa-download',function(){
    if($(this).css('cursor') == 'pointer')
        downloadReportFile($(this).parents("tr").attr('wrkspaceid'));
});
/* $(document).on('click','.moveSyncWorksapceList .fa-plus',function(){
    $(document).find('.moveSyncWorksapceList i.fa-minus').removeClass('fa-minus').addClass('fa-plus');
    $(document).find('.moveReportDetails').html('').css("display","none");
    $(this).closest("tr").find("i.fa-plus").addClass('fa-minus').removeClass('fa-plus');
    var _value = $(this).attr('jobtype');
    if(_value === "TWO_WAY_SYNC"){
        var _htmlCode = appendTwoWaySyncWorkspaceReport(getWorkspaceReport_forTWS($(this).parents("tr").attr('wrkspaceid'),'all',1,$(this).parents("tr").attr('srccldname')));
        var count = CFManageCloudAccountsAjaxCall.fileFolderCount($(this).parents("tr").attr('wrkspaceid'),$(this).parents("tr").attr('srccldname'),_value);
    }
    else{
        var _htmlCode = appendSyncWorkspaceReport(getWorkspaceReport($(this).parents("tr").attr('wrkspaceid'),'all',1));
        var count = CFManageCloudAccountsAjaxCall.fileFolderCount($(this).parents("tr").attr('wrkspaceid'),'',_value);
    }
    _htmlCode = _htmlCode.replace('TotalCounting',(count.totalFiles + count.totalFolders)).replace('ProcessedCounting',count.processedCount).replace('ErrorCounting',count.conflictCount);
    $(this).parents("tr").next(".moveReportDetails").append(_htmlCode).css("display","table-row");
    moveSyncReportPagination($(this).parents("tr").attr('totcount'),$(this).parents("tr").attr('wrkspaceid'),'all',$(this).parents("tr").attr('srccldname'),_value);
});
$(document).on('click','.moveSyncWorksapceList .fa-minus',function(){
    $(document).find('.moveSyncWorksapceList i.fa-minus').removeClass('fa-minus').addClass('fa-plus');
    $(document).find('.moveReportDetails').html('').css("display","none");

});
$(document).on('click','.moveSyncWorksapceList .fa-download',function(){
    if($(this).css('cursor') == 'pointer')
        downloadReportFile($(this).parents("tr").attr('wrkspaceid'));
});
$(document).on('click','#mappedSyncMigration .bg-info .fa-refresh',function(){
    refreshReports();
}); */
$(document).on('click','#mappedMigration .bg-info .fa-refresh',function(){
    refreshReports();
});
$(document).on('change','.moveReportDetailsTable .statusDropDown select',function () {
    var _html = appendFilterStatusFiles(getWorkspaceReport($(this).parents("tr").prev("tr.moveWorksapceList").attr('wrkspaceid'),$(this).val(),1));
    $(this).parents(".moveReportDetailsTable").find(".reportTable tbody").html('').append(_html);
    var _count = 0;
    if($(this).val() == 'all')
        _count = parseInt($(".moveReportDetailsTable .summary .totCnt").text());
    if($(this).val() == 'processed')
        _count = parseInt($(".moveReportDetailsTable .summary .prcsdCnt").text());
    if($(this).val() == 'error')
        _count = parseInt($(".moveReportDetailsTable .summary .errCnt").text());

    moveReportPagination(_count,$(this).parents("tr").prev("tr.moveWorksapceList").attr('wrkspaceid'),$(this).val())
});
/* $(document).on('change','.moveSyncReportDetailsTable .statusDropDown select',function () {
    var _html = appendFilterStatusFiles(getWorkspaceReport($(this).parents("tr").prev("tr.moveSyncWorksapceList").attr('wrkspaceid'),$(this).val(),1));
    $(this).parents(".moveSyncReportDetailsTable").find(".reportTable tbody").html('').append(_html);
    var _count = 0;
    if($(this).val() == 'all')
        _count = parseInt($(".moveSyncReportDetailsTable .summary .totCnt").text());
    if($(this).val() == 'processed')
        _count = parseInt($(".moveSyncReportDetailsTable .summary .prcsdCnt").text());
    if($(this).val() == 'error')
        _count = parseInt($(".moveSyncReportDetailsTable .summary .errCnt").text());

    moveSyncReportPagination(_count,$(this).parents("tr").prev("tr.moveSyncWorksapceList").attr('wrkspaceid'),$(this).val())
});
$(document).on('click','#mappedSyncMigration button',function () {
    if(!$("#mappedSyncMigration .custom-search-input input").val().length)
        return false;
    mappingOptions.searchJobByName($('#mappedSyncMigration .custom-search-input input').val());
});
$(document).on('click','#mappedSyncMigration .filter',function () {
    var _sts = $(this).parents("ul").attr('id');
    if(_sts == 'fromDomain')
        getFilteredReports('/from?page_nbr=1&page_size=20&fromDomainName=' + $(this).text());
    else if(_sts == 'fromCloud')
        getFilteredReports('/from?page_nbr=1&page_size=20&fromCloudName=' + $(this).text());
    else if(_sts == 'toDomain')
        getFilteredReports('/to?page_nbr=1&page_size=20&toDomainName=' + $(this).text());
    else if(_sts == 'toCloud')
        getFilteredReports('/to?page_nbr=1&page_size=20&toCloudName=' + $(this).text());
}); */
$(document).on('click','#mappedMigration button',function () {
    if(!$("#mappedMigration .custom-search-input input").val().length)
        return false;

    //appendMigrationWorkspaces(searchMoveWorkspaces($('#mappedMigration .custom-search-input input').val()),'all',1);
    mappingOptions.searchJobByName($('#mappedMigration .custom-search-input input').val().trim());
});
$(document).on('click','#mappedMigration .filter',function () {
    var _sts = $(this).parents("ul").attr('id');
    if(_sts == 'fromDomain')
        getFilteredReports('/from?page_nbr=1&page_size=20&fromDomainName=' + $(this).text());
    else if(_sts == 'fromCloud')
        getFilteredReports('/from?page_nbr=1&page_size=20&fromCloudName=' + $(this).text());
    else if(_sts == 'toDomain')
        getFilteredReports('/to?page_nbr=1&page_size=20&toDomainName=' + $(this).text());
    else if(_sts == 'toCloud')
        getFilteredReports('/to?page_nbr=1&page_size=20&toCloudName=' + $(this).text());
});
$("#filterStatus a").click(function () {
    getFilteredReports('/status?page_nbr=1&page_size=20&searchStatus=' + $(this).attr("value"));
});
function refreshReports(){
    mappingOptions.appendMigrationJobs(1,60);
    // mappingOptions.appendSyncMigrationJobs(1,60);
}

function  getFilteredReports(url){
    $.ajax({
        type: "GET",
        url: apicallurl + "/mapping/filter" + url,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            appendMigrationWorkspaces(data);
            //    appendSyncMigrationWorkspaces(data);
        }
    });
}
function getFilter(){
    $.ajax({
        type: "GET",
        url: apicallurl + "/mapping/filter/domainandcloudname",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            var _len = data.sourceDomainNames.length;
            $(".dropdown-submenu .dropdown-menu").html('');
            for(var i=0;i<_len;i++){
                $("#fromDomain").append('<li><a tabindex="-1" href="#" class="from filter">' +  data.sourceDomainNames[i]+ '</a></li>');
            }
            var _len = data.sourceCloudNames.length;
            for(var i=0;i<_len;i++){
                $("#fromCloud").append('<li><a tabindex="-1" href="#" class="from filter">' +  data.sourceCloudNames[i]+ '</a></li>');
            }
            var _len = data.destDomainNames.length;
            for(var i=0;i<_len;i++){
                $("#toDomain").append('<li><a tabindex="-1" href="#" class="to filter">' +  data.destDomainNames[i]+ '</a></li>');
            }
            var _len = data.destCloudNames.length;
            for(var i=0;i<_len;i++){
                $("#toCloud").append('<li><a tabindex="-1" href="#" class="to filter">' +  data.destCloudNames[i]+ '</a></li>');
            }
        }
    });
}

function disableMapping(value){
    if(value == 'add')
        $(document).find('.fa-exchange').addClass('disabeled').css("cursor","not-allowed");
    else if(value == 'remove')
        $(document).find('.fa-exchange').removeClass('disabeled').css("cursor","pointer");
}
/* function disableMapping(value){
    if(value == 'add')
        $('#mapdUsrs .fa-exchange').addClass('noPointer').removeClass('fa-exchange');
    else if(value == 'remove')
        $('#mapdUsrs .noPointer').removeClass('noPointer').addClass('fa-exchange');
}
$('.noPointer').on('click',function(e){
    e.preventDefault();
}); */

function deleteMapping(_async){
    var _val = false;
    if(_async != undefined)
        _val = _async;

    $("#CFShowLoading").css("display","");
    $.ajax({
        type: "DELETE",
        url: apicallurl + "/mapping/deleteAll/mapplist?sourceAdminCloudId=" + localStorage.getItem("multiUsrSrcCldId") + "&destAdminCloudId=" + localStorage.getItem("multiUsrDstnCldId"),
        async:_val,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            var searchData = JSON.parse(localStorage.getItem("searchData"));
            searchData.mapping = false;
            localStorage.setItem("searchData",JSON.stringify(searchData));
            $('#mapdUsrs .custom-search-input input').val('');
            var _txt = "0 of 0";
            $("#forNextMove").addClass("disabled");
            $(document).find("#chkInput").attr("disabled", true).prop( "checked", false );
            $("#clearBtn").css("pointer-events",'none');
            $("#clearBtn").css("cursor","not-allowed");
            $("#clearBtn").css("opacity","0.6");
            mappingOptions.localStorageOperation('','rmv');
            mappingOptions.folderOperation('','rmv');
            CsvOperation('','rmv');
            $("#mapdUsrs .paginationDiv").css("opacity","0.6");
            $('#mapdUsrs .paginationDiv select').prop('disabled', true);
            $('#mapdUsrs .paginationDiv input').prop('disabled', true);
            $("#mapdUsrs .paginationCF span").last().text(_txt );
            $("#mapdUsrs .paginationCF input").val(0);
            $(document).find("#chkInput").attr("disabled", true);
            $("#clearBtn").css("pointer-events",'none');
            $("#clearBtn").css("cursor","not-allowed");
            $("#clearBtn").css("opacity","0.6");
            $('#paginationMapng').html('');
            $('#paginationMapng').twbsPagination({
                totalPages: 1,
                visiblePages: 4,
                next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
                prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
                first:'<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
                last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
                onPageClick: function (event, page){}
            });
            //   appendMappingScreenHtml( $('#srcClouds input[name=sourceCloud]:checked').attr("id"),"source");
            //   appendMappingScreenHtml( $('#dstClouds input[name=dstCloud]:checked').attr("id"),"destination");
            $('.download').css('pointer-events', 'none');
            $('.download').css('opacity', '0.5');
            $('#help').css('visibility','hidden');
        }
    });
    setTimeout(function () {
        if($("#CFShowLoading").attr("autoMap") == "false")
            $("#CFShowLoading").css("display", "none");
    }, 1000);
}
function emailMaxChar(string,len){
    if(string.length < len){
        return string;
    }
    var email = string.split('@');
    if(email[0].length > len )
        return(CFManageCloudAccountsAjaxCall.getMaxChars(email[0],len - 5) + '@' +  CFManageCloudAccountsAjaxCall.getMaxChars(email[1],5));
    else
        return(email[0] + '@' +  CFManageCloudAccountsAjaxCall.getMaxChars(email[1],len - email[0].length));
}
function disableDwnldBtn(status){
    /* commenting for download btn*/
    //||status == "MULTIUSER_INACTIVE" ||status == "PROCESSED_WITH_SOME_PAUSE" ||status == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE"
    if(status == "PROCESSED" ||status == "MULTIUSER_INACTIVE" ||status == "PROCESSED_WITH_SOME_PAUSE" ||status == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE"|| status == "ERROR" || status == "SUSPENDED" || status == "PROCESSED_WITH_SOME_ERRORS" || status == "PROCESSED_WITH_SOME_WARNINGS" || status == "CANCEL" || status == "PROCESSED_WITH_SOME_CONFLICTS" || status == "CONFLICT")
        return('style = "cursor: pointer;color: rgba(0,0,0,1)"');
    else
        return('style = "cursor: not-allowed;color: rgba(0,0,0,0.6)"');
}

function downloadReportFile(wid){
    $.ajax({
        type: "GET",
        url: apicallurl + "/mapping/download/movereport?moveWorkSpaceId=" + wid,
        async: true,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        complete: function (xhr) {
            var data = xhr.responseText;
            if (xhr.status == 200) {
                var blob = new Blob([data]);
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = 'migration_report_' + wid + '.csv';
                document.body.appendChild(link);
                link.setAttribute("type", "hidden");
                link.click();
            }
            else if (xhr.status == 202) {
                showNotyNotification("notify","Report is in Progress,will be ready in few minutes");
            }
        }
    });
}
function searchMoveWorkspaces(searchTerm) {
    var _result;
    $.ajax({
        type: "GET",
        url: apicallurl + "/mapping/search/moveworkspace?searchUser=" + searchTerm + "&page_nbr=1&page_size=20",
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            _result = data;
        }
    });
    return _result;
}

function appendFilterStatusFiles(reportData) {
    var moveStatus = {
        "PROCESSED": "Processed",
        "IN_PROGRESS": "In Progress",
        "NOT_PROCESSED": "In queue",
        "ERROR": "Error",
        "IN_QUEUE": "In queue",
        "WARNING": "Warning",
        "SUSPENDED": "Suspended",
        "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
        "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
        "PROCESSED_WITH_SOME_CONFLICTS":"Processed with some conflicts",
        "CONFLICT":"Conflict",
        "CANCEL": "Cancel",
        "PAUSE":"Pause",
        "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE":"Processed With Some Conflicts and Pause",
        "PROCESSED_WITH_SOME_PAUSE":"Processed With Some Pause"
    };
    var _fileRow = '<tr> <td> <div> <div class="fileIcon" style="float: left;width:20px;"> <i class="fa fa-file-text-o"></i> </div><div class="fileName">DisplayFileName</div></div></td><td class="PROCESSEDClass">DisplayProcessed</td><td>SizeMB</td><td>DateDisplay</td></tr>';
    var _len = reportData.length;
    var _filesHtml = '';
    for(var i=0; i < _len; i++){
        var _fileData = reportData[i];
        _filesHtml = _filesHtml + _fileRow.replace('DisplayFileName',CFManageCloudAccountsAjaxCall.getMaxChars(_fileData.cfFileMini.objectName,30)).replace('PROCESSEDClass',_fileData.processStatus).replace('DisplayProcessed',moveStatus[_fileData.processStatus]).replace('SizeMB',CFManageCloudAccountsAjaxCall.getObjectSize(_fileData.cfFileMini.objectSize, _fileData.cfFileMini.type) ).replace('DateDisplay',mappingOptions.getDateConversion(_fileData.createdTime));
    }
    return _filesHtml;
}
function appendWorkspaceReport(reportData) {
    var moveStatus = {
        "PROCESSED": "Processed",
        "IN_PROGRESS": "In Progress",
        "NOT_PROCESSED": "In queue",
        "ERROR": "Error",
        "IN_QUEUE": "In queue",
        "WARNING": "Warning",
        "SUSPENDED": "Suspended",
        "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
        "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
        "PROCESSED_WITH_SOME_CONFLICTS":"Processed with some conflicts",
        "CONFLICT":"Conflict",
        "CANCEL": "Cancel",
        "PAUSE":"Pause",
        "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE":"Processed With Some Conflicts and Pause",
        "PROCESSED_WITH_SOME_PAUSE":"Processed With Some Pause"
    };
    //<div style="margin-right: 3%;">Status : </div><select class="form-control" style="width: 50%;margin-top: -3%"> <option value="all">All</option> <option value="processed">Processed</option> <option value="error">Error</option> <option value="suspended">Suspended</option> </select>
    var _html = '<td colspan="8"> <div class="moveReportDetailsTable" style="padding: 25px;margin-left: 3%;margin-right: -2%;"> <div class="summary"> <div class="col-md-3"> <div>Total Files/Folders : </div><div class="totCnt">TotalCounting</div></div><div class="col-md-3" style="width : 28%"> <div>Processed Files/Folders : </div><div class="prcsdCnt">ProcessedCounting</div></div><div class="col-md-3"> <div>Conflict Files/Folders : </div><div class="errCnt">ErrorCounting</div></div><div class="form-group statusDropDown col-md-3"></div></div><div class="reportTable"> <table> <thead> <tr> <th>File-Name</th> <th>Status</th> <th>Size</th> <th>Date</th> </tr></thead> <tbody> BodyToAttachFiles </tbody> </table> </div><div class="pagination p1" style="padding: 0;margin: 0;border-radius: 0;width: 100%;"> <ul class="paginationReport" class="pagination-sm"></ul> </div></div></td>';
    var _fileRow = '<tr> <td> <div> <div class="fileIcon" style="float: left;width:20px;"> <i class="fa fa-file-text-o"></i> </div><div class="fileName">DisplayFileName</div></div></td><td class="PROCESSEDClass">DisplayProcessed showDesc</td><td>SizeMB</td><td>DateDisplay</td></tr>';
    var _len = reportData.length;
    var _filesHtml = '';
    for(var i=0; i < _len; i++){
        var _fileData = reportData[i], _fileRow1 =_fileRow,_a='';
        if(_fileData.cfFileMini.directory)
            _fileRow1 = _fileRow1.replace('class="fa fa-file-text-o"','class="fa fa-folder" style="font-size:16px;"');
        if (_fileData.processStatus == "CONFLICT" || _fileData.processStatus == "CANCEL" || _fileData.processStatus == "WARNING" || _fileData.processStatus == "ERROR") {
            if(_fileData.userErrorMsg == null){
                _fileData.userErrorMsg = "Cancel";
                _a = '<i id="_error' + _fileData.id + '" class="cf-warning3"  title=' + JSON.stringify(_fileData.userErrorMsg) + ' style="margin-left:5px;color:red"></i>';
            }
            else
                _a = '<i id="_error' + _fileData.id + '" class="cf-warning3"  title=' + JSON.stringify(_fileData.userErrorMsg) + ' style="margin-left:5px;color:red"></i>';
        }

        _filesHtml = _filesHtml + _fileRow1.replace('DisplayFileName',CFManageCloudAccountsAjaxCall.getMaxChars(_fileData.cfFileMini.objectName,30)).replace('PROCESSEDClass',_fileData.processStatus).replace('DisplayProcessed',moveStatus[_fileData.processStatus]).replace('showDesc',_a).replace('SizeMB',CFManageCloudAccountsAjaxCall.getObjectSize(_fileData.cfFileMini.objectSize, _fileData.cfFileMini.type) ).replace('DateDisplay',mappingOptions.getDateConversion(_fileData.createdTime));

    }
    _html =_html.replace('BodyToAttachFiles',_filesHtml)
    return _html;
}

function getWorkspaceReport(wid,status,pgNo) {
    var _result;
    status = status.toUpperCase();
    $.ajax({
        type: "GET",
        url: apicallurl + "/move/movereport?workspaceId=" + wid + "&page_nbr=" + pgNo + "&status="+ status,
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            _result = data;
        }
    });
    return _result;
}

/*For two way Sync*/
/* function getWorkspaceReport_forTWS(wid,status,pgNo,cldName) {
    var _result;
    status = status.toUpperCase();
    $.ajax({
        type: "GET",
        url: apicallurl + "/move/twoway/movereport?workspaceId=" + wid + "&page_nbr=" + pgNo + "&status="+ status+ "&fromCloudName="+cldName,
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            _result = data;
        }
    });
    return _result;
}

function appendSyncWorkspaceReport(reportData) {
    var moveStatus = {
        "PROCESSED": "Processed",
        "IN_PROGRESS": "In Progress",
        "NOT_PROCESSED": "In queue",
        "ERROR": "Error",
        "IN_QUEUE": "In queue",
        "WARNING": "Warning",
        "SUSPENDED": "Suspended",
        "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
        "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
        "PROCESSED_WITH_SOME_CONFLICTS":"Processed with some conflicts",
        "CONFLICT":"Conflict",
        "CANCEL": "Cancel",
        "PAUSE":"Pause"
    };
    //<div style="margin-right: 3%;">Status : </div><select class="form-control" style="width: 50%;margin-top: -3%"> <option value="all">All</option> <option value="processed">Processed</option> <option value="error">Error</option> <option value="suspended">Suspended</option> </select>
    var _html = '<td colspan="8"> <div class="moveSyncReportDetailsTable" style="padding: 25px;"> <div class="summary"> <div class="col-md-3"> <div>Total Files/Folders : </div><div class="totCnt">TotalCounting</div></div><div class="col-md-3" style="width : 28%"> <div>Processed Files/Folders : </div><div class="prcsdCnt">ProcessedCounting</div></div><div class="col-md-3"> <div>Conflict Files/Folders : </div><div class="errCnt">ErrorCounting</div></div><div class="form-group statusDropDown col-md-3"></div></div><div class="reportTable"> <table> <thead> <tr> <th>File-Name</th> <th>Status</th> <th>Size</th> <th>Date</th> </tr></thead> <tbody> BodyToAttachFiles </tbody> </table> </div><div class="pagination p1" style="padding: 0;margin: 0;border-radius: 0;width: 100%;"> <ul class="paginationReport" class="pagination-sm"></ul> </div></div></td>';
    var _fileRow = '<tr> <td> <div> <div class="fileIcon" style="float: left;width:20px;"> <i class="fa fa-file-text-o"></i> </div><div class="fileName">DisplayFileName</div></div></td><td class="PROCESSEDClass">DisplayProcessed showDesc</td><td>SizeMB</td><td>DateDisplay</td></tr>';
    var _len = reportData.length;
    var _filesHtml = '';
    for(var i=0; i < _len; i++){
        var _fileData = reportData[i], _fileRow1 =_fileRow,_a='';
        if(_fileData.cfFileMini.directory)
            _fileRow1 = _fileRow1.replace('class="fa fa-file-text-o"','class="fa fa-folder" style="font-size:16px;"');
        if (_fileData.processStatus == "CONFLICT" || _fileData.processStatus == "CANCEL" || _fileData.processStatus == "WARNING" || _fileData.processStatus == "ERROR") {
            if(_fileData.userErrorMsg == null){
                _fileData.userErrorMsg = "Cancel";
                _a = '<i id="_error' + _fileData.id + '" class="cf-warning3"  title=' + JSON.stringify(_fileData.userErrorMsg) + ' style="margin-left:5px;color:red"></i>';
            }
            else
                _a = '<i id="_error' + _fileData.id + '" class="cf-warning3"  title=' + JSON.stringify(_fileData.userErrorMsg) + ' style="margin-left:5px;color:red"></i>';
        }

        _filesHtml = _filesHtml + _fileRow1.replace('DisplayFileName',CFManageCloudAccountsAjaxCall.getMaxChars(_fileData.cfFileMini.objectName,30)).replace('PROCESSEDClass',_fileData.processStatus).replace('DisplayProcessed',moveStatus[_fileData.processStatus]).replace('showDesc',_a).replace('SizeMB',CFManageCloudAccountsAjaxCall.getObjectSize(_fileData.cfFileMini.objectSize, _fileData.cfFileMini.type) ).replace('DateDisplay',mappingOptions.getDateConversion(_fileData.createdTime));

    }
    _html =_html.replace('BodyToAttachFiles',_filesHtml)
    return _html;
}

function appendTwoWaySyncWorkspaceReport(reportData) {
    var moveStatus = {
        "PROCESSED": "Processed",
        "IN_PROGRESS": "In Progress",
        "NOT_PROCESSED": "In queue",
        "ERROR": "Error",
        "IN_QUEUE": "In queue",
        "WARNING": "Warning",
        "SUSPENDED": "Suspended",
        "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
        "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
        "PROCESSED_WITH_SOME_CONFLICTS":"Processed with some conflicts",
        "CONFLICT":"Conflict",
        "CANCEL": "Cancel",
        "PAUSE":"Pause"
    };
    var clouds = {
        "SYNCPLICITY" : "Syncplicity",
        "SHAREPOINT_ONLINE_BUSINESS" : "Sharepoint Online-Admin",
        "NETWORK_FILESHARES" : "Network FileShares",
        "BOX_BUSINESS" : "Box Business-Admin",
	"ONEDRIVE_BUSINESS_ADMIN" : "Onedrive for Business-Admin",
	"GOOGLE_TEAM_DRIVE":"Team Drives",
	"G_SUITE":"Google Suite",
	"DROPBOX_BUSINESS": "Dropbox Business-Admin",
	"EGNYTE_ADMIN": "Egnyte-Admin"
    }
    var _srcIcon =localStorage.getItem('twoWaySrcIcon');
    var _dstIcon =localStorage.getItem('twoWayDstnIcon');

    var _html = '<td colspan="6"> <div class="moveSyncReportDetailsTable" style="padding: 25px;"><div class="col-md-12" id="tabsForSync"><div class="col-md-6 activeTab" id="twowaySyncSrc" style="cursor:pointer;font-size:14px;font-weight:bold;padding-bottom: 1%;width:45%;text-align: center;"><img src=" ../img/drive/circle/_srcIcon.png" alt="pic" style="height: 20px;width: 20px;margin-right: 2%"/><span id="srcDisplayName">DisplaySrcName</span><i class="fa fa-long-arrow-right" style="margin: 0% 4%;font-size: 18px;"></i><img src=" ../img/drive/circle/_dstIcon.png" alt="pic" style="height: 20px;width: 20px;margin-right: 2%"/><span id="dstnDisplayName">DisplayDstnName</span></div><div class="col-md-6" id="twowaySyncDstn" style="cursor:pointer;font-size:14px;font-weight:bold;padding-bottom: 1%;width:45%;margin-left:5%;text-align: center;"><img src=" ../img/drive/circle/_dstIcon.png" alt="pic" style="height: 20px;width: 20px;margin-right: 2%"/><span id="dstnDisplayName">DisplayDstnName</span><i class="fa fa-long-arrow-right" style="margin: 0% 4%;font-size: 18px;"></i><img src=" ../img/drive/circle/_srcIcon.png" alt="pic" style="height: 20px;width: 20px;margin-right: 2%"/><span id="srcDisplayName">DisplaySrcName</span></div> </div> <div class="summary" style="margin-top: 4%;"> <div class="col-md-3"> <div>Total Files/Folders : </div><div class="totCnt">TotalCounting</div></div><div class="col-md-3" style="width : 28%"> <div>Processed Files/Folders : </div><div class="prcsdCnt">ProcessedCounting</div></div><div class="col-md-3"> <div>Conflict Files/Folders : </div><div class="errCnt">ErrorCounting</div></div><div class="form-group statusDropDown col-md-3"></div></div><div class="reportTable"> <table> <thead> <tr> <th>File-Name</th> <th>Status</th> <th>Size</th> <th>Date</th> </tr></thead> <tbody> BodyToAttachFiles </tbody> </table> </div><div class="pagination p1" style="padding: 0;margin: 0;border-radius: 0;width: 100%;"> <ul class="paginationReport" class="pagination-sm"></ul> </div></div></td>';
    var _fileRow = '<tr> <td> <div> <div class="fileIcon" style="float: left;width:20px;"> <i class="fa fa-file-text-o"></i> </div><div class="fileName">DisplayFileName</div></div></td><td class="PROCESSEDClass">DisplayProcessed showDesc</td><td>SizeMB</td><td>DateDisplay</td></tr>';
    var _len = reportData.length;
    var _filesHtml = '';
    for(var i=0; i < _len; i++){
        var _fileData = reportData[i], _fileRow1 =_fileRow,_a='';
        if(_fileData.cfFileMini.directory)
            _fileRow1 = _fileRow1.replace('class="fa fa-file-text-o"','class="fa fa-folder" style="font-size:16px;"');
        if (_fileData.processStatus == "CONFLICT" || _fileData.processStatus == "CANCEL" || _fileData.processStatus == "WARNING" || _fileData.processStatus == "ERROR") {
            if(_fileData.userErrorMsg == null){
                _fileData.userErrorMsg = "Cancel";
                _a = '<i id="_error' + _fileData.id + '" class="cf-warning3"  title=' + JSON.stringify(_fileData.userErrorMsg) + ' style="margin-left:5px;color:red"></i>';
            }
            else
                _a = '<i id="_error' + _fileData.id + '" class="cf-warning3"  title=' + JSON.stringify(_fileData.userErrorMsg) + ' style="margin-left:5px;color:red"></i>';
        }

        _filesHtml = _filesHtml + _fileRow1.replace('DisplayFileName',CFManageCloudAccountsAjaxCall.getMaxChars(_fileData.cfFileMini.objectName,30)).replace('PROCESSEDClass',_fileData.processStatus).replace('DisplayProcessed',moveStatus[_fileData.processStatus]).replace('showDesc',_a).replace('SizeMB',CFManageCloudAccountsAjaxCall.getObjectSize(_fileData.cfFileMini.objectSize, _fileData.cfFileMini.type) ).replace('DateDisplay',mappingOptions.getDateConversion(_fileData.createdTime));

    }
    _html =_html.replace('BodyToAttachFiles',_filesHtml);
    _html = _html.replace('_srcIcon',_srcIcon).replace('DisplaySrcName',clouds[_srcIcon]).replace('_dstIcon',_dstIcon).replace('DisplayDstnName',clouds[_dstIcon]).replace('_srcIcon',_srcIcon).replace('DisplaySrcName',clouds[_srcIcon]).replace('_dstIcon',_dstIcon).replace('DisplayDstnName',clouds[_dstIcon]);
    return _html;
} */

function getMoveWorkspaces(pageNumber){
    var _response;
    $.ajax({
        type: "GET",
        url: apicallurl + "/move/list?page_size=30&page_nbr=" + pageNumber,
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            _response = data;
        }
    });
    return _response;
}

function appendMigrationWorkspaces(data){
    //$("#mappedMigration #mgrtnReports tbody").html('');
    var moveStatus = {
        "PROCESSED": "Processed",
        "IN_PROGRESS": "In Progress",
        "NOT_PROCESSED": "In queue",
        "ERROR": "Error",
        "IN_QUEUE": "In queue",
        "WARNING": "Warning",
        "SUSPENDED": "Suspended",
        "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
        "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
        "PROCESSED_WITH_SOME_CONFLICTS":"Processed with some conflicts",
        "CONFLICT":"Conflict",
        "CANCEL": "Cancel",
        "PAUSE":"Pause",
        "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE":"Processed With Some Conflicts and Pause",
        "PROCESSED_WITH_SOME_PAUSE":"Processed With Some Pause"
    };
    $("#mappedMigration #mgrtnReports tbody").html('');
    var _workspaceLen = data.length;
    var _moveIdHtml = '<tr class="moveWorksapceList rmvdCloudClass" wrkSpaceId="workSpaceId" srcemail="srcEmailId" srccldname="srcCloudName" srccldid="srcCloudId" srcrt="srcRootId" dstnemail="dstnEmailId" dstncldname="dstnCloudName" dstncldid="dstnCloudId" dstnrt="dstnCloudRoot" prcsdCount="processedCount" errCount="errorCount" totCount="totalCount"> <td > <div> <i class="fa fa-plus" style="cursor: pointer"></i> </div></td><td> <div> <img src=" ../img/drive/circle/srcCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>srcEmailDisplay</div></div></td><td> <div> <img src=" ../img/drive/circle/dstCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>dstEmailDisplay</div></div></td><td class="processStatus">processStatusDispaly</td><td> <div>dateDisplay</div></td><td> <div style="margin-left: 24px;"><i class="fa fa-download" appendStyling></i></div></td></tr><tr class="moveReportDetails" style="display:none"></tr>';
    for(var i = 0; i < _workspaceLen; i++ ){
        var _result = data[i];
        if(_result.fromCloudId == null || _result.toCloudId == null)
            _moveIdHtml = _moveIdHtml.replace('rmvdCloudClass','cloudRmvd');
        else
            _moveIdHtml = _moveIdHtml.replace('cloudRmvd','rmvdCloudClass');


        if(_result.multiUserMove) {
            _result = JSON.parse(JSON.stringify(_result, function(key, value) {
                if(value === null) {
                    return "Not Available";
                }
                return value;
            }));

            //if(_result.id != null && _result.fromMailId != null && _result.fromCloudName != null && _result.fromCloudId != null && _result.fromRootId != null && _result.toMailId != null && _result.toCloudName != null && _result.toCloudId != null && _result.toRootId != null)
            var _html ="";
            _html = _moveIdHtml.replace('workSpaceId',_result.id).replace('srcEmailId',_result.fromMailId).replace('srcCloudName',_result.fromCloudName).replace('srcCloudId',_result.fromCloudId.id).replace('srcRootId',_result.fromRootId).replace('dstnEmailId',_result.toMailId).replace('dstnCloudName',_result.toCloudName).replace('dstnCloudId',_result.toCloudId.id).replace('dstnCloudRoot',_result.toRootId).replace('srcCloudImage',_result.fromCloudName).replace('srcEmailDisplay',_result.fromMailId).replace('dstCloudImage',_result.toCloudName).replace('dstEmailDisplay',_result.toMailId).replace('processStatus',_result.processStatus).replace('processStatusDispaly',moveStatus[_result.processStatus]).replace('dateDisplay',mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount',_result.processedCount).replace('errorCount',_result.errorCount).replace('totalCount',_result.totalFilesAndFolders).replace('appendStyling',disableDwnldBtn(_result.processStatus));

            $("#mappedMigration #mgrtnReports tbody").append(_html);
        }
    }
}
/* function appendSyncMigrationWorkspaces(data){
    //$("#mappedMigration #mgrtnReports tbody").html('');
    var moveStatus = {
        "PROCESSED": "Processed",
        "IN_PROGRESS": "In Progress",
        "NOT_PROCESSED": "In queue",
        "ERROR": "Error",
        "IN_QUEUE": "In queue",
        "WARNING": "Warning",
        "SUSPENDED": "Suspended",
        "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
        "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
        "PROCESSED_WITH_SOME_CONFLICTS":"Processed with some conflicts",
        "CONFLICT":"Conflict",
        "CANCEL": "Cancel"
    };
    $("#mappedSyncMigration #mgrtnSyncReports tbody").html('');
    var _workspaceLen = data.length;
    var _moveIdHtml = '<tr class="moveSyncWorksapceList rmvdCloudClass" wrkSpaceId="workSpaceId" srcemail="srcEmailId" srccldname="srcCloudName" srccldid="srcCloudId" srcrt="srcRootId" dstnemail="dstnEmailId" dstncldname="dstnCloudName" dstncldid="dstnCloudId" dstnrt="dstnCloudRoot" prcsdCount="processedCount" errCount="errorCount" totCount="totalCount"> <td > <div> <i class="fa fa-plus" style="cursor: pointer"></i> </div></td><td> <div> <img src=" ../img/drive/circle/srcCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>srcEmailDisplay</div></div></td><td> <div> <img src=" ../img/drive/circle/dstCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>dstEmailDisplay</div></div></td><td class="processStatus">processStatusDispaly</td><td> <div>dateDisplay</div></td><td> <div style="margin-left: 24px;"><i class="fa fa-download" appendStyling></i></div></td></tr><tr class="moveReportDetails" style="display:none"></tr>';
    for(var i = 0; i < _workspaceLen; i++ ){
        var _result = data[i];
        if(_result.fromCloudId == null || _result.toCloudId == null)
            _moveIdHtml = _moveIdHtml.replace('rmvdCloudClass','cloudRmvd');
        else
            _moveIdHtml = _moveIdHtml.replace('cloudRmvd','rmvdCloudClass');

        if(_result.multiUserMove) {
            _result = JSON.parse(JSON.stringify(_result, function(key, value) {
                if(value === null) {
                    return "Not Available";
                }
                return value;
            }));
            var _html ="";
            _html = _moveIdHtml.replace('workSpaceId',_result.id).replace('srcEmailId',_result.fromMailId).replace('srcCloudName',_result.fromCloudName).replace('srcCloudId',_result.fromCloudId.id).replace('srcRootId',_result.fromRootId).replace('dstnEmailId',_result.toMailId).replace('dstnCloudName',_result.toCloudName).replace('dstnCloudId',_result.toCloudId.id).replace('dstnCloudRoot',_result.toRootId).replace('srcCloudImage',_result.fromCloudName).replace('srcEmailDisplay',_result.fromMailId).replace('dstCloudImage',_result.toCloudName).replace('dstEmailDisplay',_result.toMailId).replace('processStatus',_result.processStatus).replace('processStatusDispaly',moveStatus[_result.processStatus]).replace('dateDisplay',mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount',_result.processedCount).replace('errorCount',_result.errorCount).replace('totalCount',_result.totalFilesAndFolders).replace('appendStyling',disableDwnldBtn(_result.processStatus));

            $("#mappedSyncMigration #mgrtnSyncReports tbody").append(_html);
        }
    }
}
function  moveSyncReportPagination(count,wid,status,srcCldName,jobType){
    $('#mappedSyncMigration .paginationReport').empty();
    $('#mappedSyncMigration .paginationReport').removeData("twbs-pagination");
    $('#mappedSyncMigration .paginationReport').unbind("page");
    if (jobType == "TWO_WAY_SYNC"){
        var _data = CFManageCloudAccountsAjaxCall.fileFolderCount(wid,srcCldName,jobType);
    }
    else {
        var _data = CFManageCloudAccountsAjaxCall.fileFolderCount(wid);
    }
    count = Math.ceil((_data.totalFiles + _data.totalFolders)/20);
    $("#mappedSyncMigration .paginationReport").twbsPagination({
        totalPages: count,
        visiblePages: 6,
        next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
        prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
        first:'<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
        last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
        onPageClick: function (event, page) {
            appendWorkspaceReportPaginated(getWorkspaceReport(wid,status,page));
        }
    });
} */

function  moveReportPagination(count,wid,status){
    $('#mappedMigration .paginationReport').empty();
    $('#mappedMigration .paginationReport').removeData("twbs-pagination");
    $('#mappedMigration .paginationReport').unbind("page");
    var _data = CFManageCloudAccountsAjaxCall.fileFolderCount(wid);
    count = Math.ceil((_data.totalFiles + _data.totalFolders)/20);
    $("#mappedMigration .paginationReport").twbsPagination({
        totalPages: count,
        visiblePages: 6,
        next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
        prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
        first:'<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
        last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
        onPageClick: function (event, page) {
            appendWorkspaceReportPaginated(getWorkspaceReport(wid,status,page));
        }
    });
}

function appendWorkspaceReportPaginated(reportData) {
    var moveStatus = {
        "PROCESSED": "Processed",
        "IN_PROGRESS": "In Progress",
        "NOT_PROCESSED": "In queue",
        "ERROR": "Error",
        "IN_QUEUE": "In queue",
        "WARNING": "Warning",
        "SUSPENDED": "Suspended",
        "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
        "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
        "PROCESSED_WITH_SOME_CONFLICTS":"Processed with some conflicts",
        "CONFLICT":"Conflict",
        "CANCEL": "Cancel",
        "PAUSE":"Pause",
        "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE":"Processed With Some Conflicts and Pause",
        "PROCESSED_WITH_SOME_PAUSE":"Processed With Some Pause"
    };
    $(".reportTable tbody").html('');
    var _fileRow = '<tr> <td> <div> <div class="fileIcon" style="float: left;width: 20px;"> <i class="fa fa-file-text-o"></i> </div><div class="fileName">DisplayFileName</div></div></td><td class="PROCESSEDClass">DisplayProcessed showDesc</td><td>SizeMB</td><td>DateDisplay</td></tr>';
    var _len = reportData.length;
    var _filesHtml = '';
    for(var i=0; i < _len; i++){
        var _files = _fileRow,_a = '';
        var _fileData = reportData[i];
        if(_fileData.folder)
            _files = _files.replace('fa-file-text-o','fa-folder');
        if (_fileData.processStatus == "CONFLICT" || _fileData.processStatus == "PAUSE"|| _fileData.processStatus == "CANCEL" || _fileData.processStatus == "WARNING" || _fileData.processStatus == "ERROR") {
            _a = '<i id="_error' + _fileData.id + '" class="cf-warning3"  title=' + JSON.stringify(_fileData.userErrorMsg) + ' style="margin-left:5px;color:red"></i>';
        }
        _filesHtml = _filesHtml + _files.replace('DisplayFileName',CFManageCloudAccountsAjaxCall.getMaxChars(_fileData.cfFileMini.objectName,30)).replace('PROCESSEDClass',_fileData.processStatus).replace('DisplayProcessed',moveStatus[_fileData.processStatus]).replace('showDesc',_a).replace('SizeMB',CFManageCloudAccountsAjaxCall.getObjectSize(_fileData.cfFileMini.objectSize, _fileData.cfFileMini.type) ).replace('DateDisplay',mappingOptions.getDateConversion(_fileData.createdTime));
    }
    $(".reportTable tbody").html(_filesHtml);
}
$(document).on('change','#chkInput',function () {
    //  localStorage.removeItem("selMappings");
    if($("#chkInput:checked").length){
        //   $("#forNextMove").removeClass("disabled");
        $("input[name=inputMapdUrs]").prop('checked',true);
        if($('#mapdUsers input[name=inputMapdUrs]:checked').length > 0){
            $("#forNextMove").removeClass("disabled");
        }
        if(!($(this).parents("table").find('tbody tr').hasClass("automapRow"))&& !($(this).parents("table").find('tbody tr').hasClass("folderRow"))){
            $('#mapdUsers input[name=inputMapdUrs]:checked').each(function(){
                var _obj ={
                    "fromCloudId": {
                        "id":$(this).closest('tr').attr('srccldid'),
                    },
                    "toCloudId": {
                        "id":$(this).closest('tr').attr('dstncldid')
                    }
                };
                var _objEmail ={
                    "fromMailId": $(this).closest('tr').attr('srcemail'),
                    "fileName":$(this).closest('tr').attr('srcemail'),
                    "toMailId": $(this).closest('tr').attr('dstnemail'),
                    "fromCloudName":$(this).closest('tr').attr('srccldname'),
                    "toCloudName":$(this).closest('tr').attr('dstncldname'),

                };
                if(!mappingOptions.localStorageOperation(_obj,'check')){
                    mappingOptions.localStorageOperation(_obj,'set');
                    mappingOptions.localStorageOperation(_objEmail,'set1');
                }
            });
        }
        else{
            $('#mapdUsers input[name=inputMapdUrs]:checked').each(function(){
                var _d = $(this).parent().parent();
                var _obj ={
                    "fromCloudId": {
                        "id":$(_d).attr('srccldid'),
                    },
                    "toCloudId": {
                        "id":$(_d).attr('dstncldid'),
                    }
                };
                var _objEmail ={
                    "fromMailId": $(_d).attr('srcemail'),
                    "fileName":$(_d).attr('srcemail'),
                    "toMailId": $(_d).attr('dstnemail'),
                    "fromCloudName":$(_d).attr('srccldname'),
                    "toCloudName":$(_d).attr('dstncldname'),

                };
                if(!mappingOptions.localStorageOperation(_obj,'check')){
                    mappingOptions.localStorageOperation(_obj,'set');
                    mappingOptions.localStorageOperation(_objEmail,'set1');
                }
            });
        }
    }
    else{
        $('#mapdUsers input[name=inputMapdUrs]').each(function(){
            var _d = $(this).parent().parent();
            $(this).prop('checked',false);
            var _obj ={
                "fromCloudId": {
                    "id":$(_d).attr('srccldid'),
                },
                "toCloudId": {
                    "id":$(_d).attr('dstncldid'),
                }
            };
            var _objEmail ={
                "fromMailId": $(_d).attr('srcemail'),
                "fileName":$(_d).attr('srcemail'),
                "toMailId": $(_d).attr('dstnemail'),
                "fromCloudName":$(_d).attr('srccldname'),
                "toCloudName":$(_d).attr('dstncldname'),

            };
            mappingOptions.localStorageOperation(_obj,'delete');
            mappingOptions.localStorageOperation(_objEmail,'delete1');
        });
        if(!JSON.parse(localStorage.getItem('selectedMappings')).length)
            $("#forNextMove").addClass("disabled");
        //mappingOptions.localStorageOperation('','rmv');
    }
});
$(document).on('change', 'input[name="folderMapngCheckBox"]', function(){
    //localStorage.removeItem("selectedMappings");
    //localStorage.removeItem('selectedEmail');
    localStorage.removeItem("validator");
    var s =JSON.parse(localStorage.getItem("folderMappings"))
    if($('input[name="folderMapngCheckBox"]:checked').length > 0 || s.length>0){
        $('#forNextMove').removeClass('disabled');
    }
    else
        $('#forNextMove').addClass('disabled');

    if($('#mapdUsers input[name="csvMapngCheckBox"]:checked').length >0){
        $('#forNextMove').removeClass('disabled');
    }
    if( $('#mapdUsers input[name= "inputMapdUrs"]').length > 0) {
        var checkedLength = $('#mapdUsers input[name= "inputMapdUrs"]:checked').length + $('#mapdUsers input[name="folderMapngCheckBox"]:checked').length;
        var totalLength = $('#mapdUsers input[name="inputMapdUrs"]').length + $('#mapdUsers input[name="folderMapngCheckBox"]').length;
    }
    else{
        var checkedLength = $('#mapdUsers input[name= "folderMapngCheckBox"]:checked').length;
        var totalLength = $('#mapdUsers input[name="folderMapngCheckBox"]').length;
    }
    if (checkedLength == totalLength) {
        $('#chkInput').prop('checked', true);

    }
    else{
        $('#chkInput').prop('checked', false);
    }


    var _obj = {
        "fromCloudId": {
            "id": $(this).closest('tr').attr('srccldid'),
        },
        "toCloudId": {
            "id": $(this).closest('tr').attr('dstncldid')
        },
        "fromRootId": $(this).closest('tr').attr('srcrt'),
        "toRootId": $(this).closest('tr').attr('dstnrt'),
        "sourceFolderPath": $(this).closest('tr').attr('srcfolderpath'),
        "destFolderPath": $(this).closest('tr').attr('dstnfolderpath'),
        "folder":"true"
    };
    var _objEmail = {
        "fromMailId": $(this).closest('tr').attr('srcemail'),
        "fileName": $(this).closest('tr').attr('srcemail'),
        "toMailId": $(this).closest('tr').attr('dstnemail'),
        "fromCloudName": $(this).closest('tr').attr('srccldname'),
        "toCloudName": $(this).closest('tr').attr('dstncldname'),

    };

    if($(this).is(':checked')) {
        mappingOptions.folderOperation(_obj, 'set');
        mappingOptions.folderOperation(_objEmail, 'set1');
    }
    else{
        mappingOptions.folderOperation(_obj,'delete');
        mappingOptions.folderOperation(_objEmail,'delete1');
    }
    if(!JSON.parse(localStorage.getItem('folderMappings')).length)
        $("#forNextMove").addClass("disabled");
});

function multiUsrMoveNewCall(moveFileObject,files) {
    // var jsonArray = {
    //     "fromRootId": moveFileObject.fromId,
    //     "toRootId": moveFileObject.toId,
    //     "type": "MOVE_WORKSPACE",
    //     "fromCloudId": {
    //         "id": moveFileObject.fromCId
    //     },
    //     "toCloudId": {
    //         "id": moveFileObject.toCId
    //     },
    //     "fromCloudName":moveFileObject.fromCloudName,
    //     "toCloudName":moveFileObject.toCloudName,
    //     "fromMailId":moveFileObject.fromMailId,
    //     "toMailId":moveFileObject.toEmailId,
    //     "fromCloudSpace": null,
    //     "toCloudSpace": null,
    //     "validSpace": true,
    //     "errorDescription": null,
    //     "totalFolders": null,
    //     "totalFiles": null,
    //     "moveFoldersStatus": false,
    //     "totalFilesAndFolders": 0,
    //     "userEmails": moveFileObject.userEmails,
    //     "threadBy": null,
    //     "retry": 0,
    //     "useEncryptKey": false,
    //     "notify": moveFileObject.notify,
    //     "fileMove": false,
    //     "success": false,
    //     "destinationFolderName":moveFileObject.destinationFolderName,
    //     "multiUserMove":true,
    //     "cfMultiUserMove":null,
    //     "fromAdminId":localStorage.getItem("mulUsrSrc"),
    //     "toAdminId": localStorage.getItem("mulUsrDst"),
    // };
    // var CFMultiUserMove=[];
    // $("#mapdUsrs tbody tr").each(function () {
    //     var CFMulti=
    //         {
    //             "fromEmailId":  $(this).attr("srcemail").trim(),
    //             "fromCloudId":  $(this).attr("srccldid").trim(),
    //             "fromCloudName": $(this).attr("srccldname").trim(),
    //             "fromRootId":  $(this).attr("srcrt").trim(),
    //             "toEmailId":$(this).attr("dstnemail").trim(),
    //             "toCloudId":$(this).attr("dstncldid").trim(),
    //             "toRootId":$(this).attr("dstnrt").trim(),
    //             "toCloudName":$(this).attr("dstncldname").trim(),
    //         }
    //     CFMultiUserMove.push(CFMulti);
    // });
    // jsonArray.cfMultiUserMove =CFMultiUserMove;
    // jsonArray.fileIds =files;
    //var MoveToJson = JSON.stringify(jsonArray);
    var MoveToJson = JSON.stringify( mappingOptions.localStorageOperation('','get') );

    $.ajax({
        type: 'POST',
        url: apicallurl + "/move/multiuser/create?isCopy=true",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails()
        },
        data: MoveToJson,
        dataType: 'json',
        success: function (move) {
            showNotyNotification("success","Migration initiated");
        },
        complete: function (xhr) {
            if (xhr.status > 300) {
                unCheckFile();
                showNotyNotification("error","Operation Failed");
            }
        }
    });
}


//Options Screen
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$(document).on('click','#appendJob .jobsRow .fa-plus',function () {
    var $parentTr = $(this).closest( ".jobsRow" );
    $('#appendJob .jobsRow .fa').removeClass('fa-minus').addClass('fa-plus');
    $('#appendJob .appendMigrateWorkSpaces').html('').css('display','none');
    $(this).removeClass('fa-plus').addClass('fa-minus');
    /*  var jobType = $($parentTr).attr('jobtype');
        var _htmlData = mappingOptions.getWorkspaces($($parentTr).attr('id'),1,50,jobType); */
    var _htmlData = mappingOptions.getWorkspaces($($parentTr).attr('id'),1,50);
    //$($parentTr).next('.appendMigrateWorkSpaces').css('display','block').html(_htmlData);
});
$(document).on('click','#appendJob .jobsRow .fa-minus',function () {
    $('#appendJob .jobsRow .fa').removeClass('fa-minus').addClass('fa-plus');
    $('#appendJob .appendMigrateWorkSpaces').html('').css('display','none');
});
/*$(document).on('click','#appendSyncJob .jobsRow .fa-plus',function () {
    var $parentTr = $(this).closest( ".jobsRow" );
    $('#appendSyncJob .jobsRow .fa').removeClass('fa-minus').addClass('fa-plus');
    $('#appendSyncJob .appendSyncMigrateWorkSpaces').html('').css('display','none');
    $(this).removeClass('fa-plus').addClass('fa-minus');
    var _data = $($parentTr).attr('id');
    var jobType = $($parentTr).attr('jobtype');
    var _htmlData = mappingOptions.getSyncWorkspaces($($parentTr).attr('id'),1,50,jobType);
    //$($parentTr).next('.appendSyncMigrateWorkSpaces').css('display','block').html(_htmlData);
});
$(document).on('click','#appendSyncJob .jobsRow .fa-minus',function () {
    $('#appendSyncJob .jobsRow .fa').removeClass('fa-minus').addClass('fa-plus');
    $('#appendSyncJob .appendSyncMigrateWorkSpaces').html('').css('display','none');
}); */

$(document).on('click','table .editJob .fa-pencil',function () {
    $('.editJob').css('display','none');
    $('.ClickedEditJob').css('display','flex');
    $('.ClickedEditJob .jobName').val($(".editJob .jobName").text());
    //$("#forNextMove").addClass('disabled');
    //$("#forPreviousMove").addClass('disabled');
});
$(document).on('click','table .ClickedEditJob .fa-times',function () {
    $('.editJob').css('display','flex');
    $('.ClickedEditJob').css('display','none');
    $('.editJob div').css("display",'')
    // $("#forNextMove").removeClass('disabled');
    //$("#forPreviousMove").removeClass('disabled');
});
$(document).on('click','table .ClickedEditJob .fa-check',function () {
    var _val = $('.ClickedEditJob .jobName').val();
    if(_val.length < 4)
    {
        showNotyNotification("error","Min 4  characters are required.");
        return false;
    }
    $('.editJob').css('display','flex');
    $('.ClickedEditJob').css('display','none');
    $('.editJob div').css("display",'')
    $(".editJob .jobName").text(_val);
    //$("#forNextMove").removeClass('disabled');
    //$("#forPreviousMove").removeClass('disabled');
});

$(document).on('click','table .editPath .fa-pencil',function () {
    $('.editPath').css('display','none');
    $('.ClickedEditPath').css('display','flex');
    $('.ClickedEditPath .pathName').val($('.editPath .jobPath').text());
    // $("#forNextMove").addClass('disabled');
    // $("#forPreviousMove").addClass('disabled');
});
$(document).on('click','table .ClickedEditPath .fa-times',function () {
    $('.editPath').css('display','flex');
    $('.ClickedEditPath').css('display','none');
    $('.editPath div').css("display",'')
    // $("#forNextMove").removeClass('disabled');
    // $("#forPreviousMove").removeClass('disabled');
});
$(document).on('click','table .ClickedEditPath .fa-check',function () {
    var _val = $('.ClickedEditPath .pathName').val();
    // if(_val.length < 4)
    // {
    //     showNotyNotification("error","Min 4  characters are required.");
    //     return false;
    // }
    if(_val == '/')
        showNotyNotification("notify","Files will get migrated on root.");

    $('.editPath').css('display','flex');
    $('.ClickedEditPath').css('display','none');
    $('.editPath div').css("display",'');
    $('.editPath .jobPath').text(_val);
    // $("#forNextMove").removeClass('disabled');
    // $("#forPreviousMove").removeClass('disabled');
});
// $(document).on('click','table .emailNotify .fa-check',function () {
//     var _val = $('.emailNotify input').val();
//     if(mappingOptions.checkEmail(_val)){
//         var _html = '<div style="float:left;margin:2px;padding:4px;border: 1px solid #ccc;background: #fff;"><span>developer@cloudfuze.com</span> <i class="fa fa-times" style="cursor:pointer;padding:0 2px 0 10px"></i></div>';
//         _html = _html.replace("developer@cloudfuze.com",_val);
//         $('#emailId').append(_html);
//         $('.emailNotify input').val('');
//     }
// });
// $(document).find("table .emailNotify input").keyup(function(e){
//     if( e.which == 13){
//         var _val = $('.emailNotify input').val();
//         if(mappingOptions.checkEmail(_val)){
//             var _html = '<div style="float:left;margin:2px;padding:4px;border: 1px solid #ccc;;background: #fff;"><span>developer@cloudfuze.com</span> <i class="fa fa-times" style="cursor:pointer;padding:0 2px 0 10px"></i></div>';
//             _html = _html.replace("developer@cloudfuze.com",_val);
//             $('#emailId').append(_html);
//             $('.emailNotify input').val('');
//         }
//     }
// });
$(document).on('click','table #emailId .fa-times',function () {
    $(this).parent().remove();
});


$(document).find("#mappingOptions input[type=text]").keypress(function(e) {
    /* if(e.which == 8 || e.which == 17 || e.which == 16 || e.which == 20) */
    if(e.which == 8 || e.which == 17 || e.which == 16 || e.which == 20 || e.which == 39 || e.which == 37)
        return true;
    else if(navigator.userAgent.search("Firefox") && e.which == 0)
        return true;

    else if(e.which == 13)
    {
        var _cls = $(this).attr('class');

        if(_cls.indexOf("pathName") >= 0){
            var _val = $('.ClickedEditPath .pathName').val();
            if(_val == '/')
                showNotyNotification("notify","Files will be migrated to root.");

            // $('.editPath').css('display','flex');
            // $('.ClickedEditPath').css('display','none');
            // $('.editPath div').css("display",'')
            // $('.editPath .jobPath').text(_val);
        }
        else{
            var _val = $('.ClickedEditJob .jobName').val();
            if(_val.length < 4)
            {
                showNotyNotification("error","Min 4  characters are required.");
                return false;
            }
            // $('.editJob').css('display','flex');
            // $('.ClickedEditJob').css('display','none');
            // $('.editJob div').css("display",'')
            // $(".editJob .jobName").text(_val);
        }
        return true;
    }
    //Enter: 13,Up: 38,Down: 40,Right: 39,Left: 37,Esc: 27,SpaceBar: 32,Ctrl: 17,Alt: 18,Shift: 16,backspace:8,capslock:20
    if($(this).val().length > 30 && $(this).attr('class') == 'jobName')
    {
        showNotyNotification("notify","Max 30 characters are allowed.");
        return false;
    }
    else if($(this).val().length > 15 && $(this).attr('class') == 'pathName')
    {
        showNotyNotification("notify","Max 15 characters are allowed.");
        return false;
    }
    return mappingOptions.acceptCharcter(String.fromCharCode(e.which));
});
$(document).find("#mappingOptions input[type=text].pathName").keydown(function(e) {
    var field=this;
    var _val = $(this).val();
    setTimeout(function () {
        if(field.value.indexOf('/') !== 0) {
            var _val = $(field).val();
            $(field).val('/' + _val);
        }
    }, 1);
});
var mappingOptions ={
    appendMigrationJobs : function(pageNumber,pageSize){
        $.ajax({
            type: "GET",
            url: apicallurl + "/move/newmultiuser/get/moveJob?page_nbr=" + pageNumber + "&page_size=" + pageSize ,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                mappingOptions.appendJobs(data);
            }
        });
    },

    searchJobByName:function(searchTerm){
        searchTerm = searchTerm.toLowerCase();
        $.ajax({
            type: "GET",
            url: apicallurl + "/move/newmultiuser/search/" + searchTerm,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                mappingOptions.appendJobs(data);
                //   mappingOptions.appendSyncJobs(data);
            }
        });
    },
    appendJobs:function (data) {
        var jobStatus = {
            "STARTED" : "Started",
            "COMPLETED" : "Completed",
            "SUSPENDED" : "Suspended",
            "IN_PROGRESS": "In Progress",
            "MULTIUSERTRAIL_COMPLETED": "Trial Completed"
        };

        /* var job = {
                  "ONETIME" : "One Time",
                  "DELTA" : "Delta"
              };

              var _appendHtml ='<tr id="job_ID" class="jobsRow" jobtype="jobType"> <td style="width: 5%"> <div> <i class="fa fa-plus" style="cursor: pointer"></i> </div></td><td style="width: 35%">Job_Name</td><td style="width:10%" id="jobType">Job_Type</td><td style="width: 20%">DestinationFolder</td><td class="Job_Status" style="font-weight: bold;width: 18%">Job_Status_Show</td><td>Date</td></tr><tr class="appendMigrateWorkSpaces" style="display: none"></tr>',//<td> <i class="fa fa-download"></i> </td> */

        var _appendHtml ='<tr id="job_ID" class="jobsRow"> <td style="width: 5%"> <div> <i class="fa fa-plus" style="cursor: pointer"></i> </div></td><td style="width: 35%">Job_Name</td><td style="width:10%" id="jobType">Job_Type</td><td style="width: 20%">DestinationFolder</td><td class="Job_Status" style="font-weight: bold;width: 18%">Job_Status_Show</td><td>Date</td></tr><tr class="appendMigrateWorkSpaces" style="display: none"></tr>',//<td> <i class="fa fa-download"></i> </td>
            _len = data.length;
        $("#appendJob tbody").html('');
        for(var i = 0; i < _len; i++){
            var _data = data[i],_html;
            var selectedtext ;
            selectedtext= data[i].jobType;
            if(selectedtext == undefined){
                selectedtext =data[i].jobType;
            }
            if(selectedtext == null){
                selectedtext="ONETIME";
            }
            var csv = data[i].isCSV;

            // $("#jobType").text("selectedText");
            if(_data.jobStatus != "STARTED"){
                if((selectedtext== "ONETIME") ||(selectedtext== "DELTA")) {
                    /*	 if(csv === true)
                                        _html = _appendHtml.replace("job_ID", _data.id).replace("jobType", _data.jobType).replace("Job_Name", _data.jobName).replace("Job_Type", job[selectedtext]).replace("DestinationFolder", '-').replace("Job_Status", _data.jobStatus).replace("Job_Status_Show", jobStatus[_data.jobStatus]).replace("Date", mappingOptions.getDateConversion(_data.createdTime));
                                    else
                                        _html = _appendHtml.replace("job_ID", _data.id).replace("jobType", _data.jobType).replace("Job_Name", _data.jobName).replace("Job_Type", job[selectedtext]).replace("DestinationFolder", '/' + _data.migrateFolderName).replace("Job_Status", _data.jobStatus).replace("Job_Status_Show", jobStatus[_data.jobStatus]).replace("Date", mappingOptions.getDateConversion(_data.createdTime)); */
                    if(csv === true)
                        _html = _appendHtml.replace("job_ID", _data.id).replace("Job_Name", _data.jobName).replace("Job_Type", selectedtext).replace("DestinationFolder", '-').replace("Job_Status", _data.jobStatus).replace("Job_Status_Show", jobStatus[_data.jobStatus]).replace("Date", mappingOptions.getDateConversion(_data.createdTime));
                    else
                        _html = _appendHtml.replace("job_ID", _data.id).replace("Job_Name", _data.jobName).replace("Job_Type", selectedtext).replace("DestinationFolder", '/' + _data.migrateFolderName).replace("Job_Status", _data.jobStatus).replace("Job_Status_Show", jobStatus[_data.jobStatus]).replace("Date", mappingOptions.getDateConversion(_data.createdTime));
                    $("#appendJob tbody").append(_html);

                }
            }
        }
    },
    /*    appendSyncMigrationJobs : function(pageNumber,pageSize){
           $.ajax({
               type: "GET",
               url: apicallurl + "/move/newmultiuser/get/moveJob?page_nbr=" + pageNumber + "&page_size=" + pageSize ,
               async: false,
               dataType: "json",
               headers: {
                   "Content-Type": "application/json",
                   "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                   "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
               },
               success: function (data) {
                   mappingOptions.appendSyncJobs(data);
               }
           });
       },

       appendSyncJobs:function (data) {
           var _displayImg, _cancleImg, iconsArray = [];
           var jobStatus = {
               "STARTED" : "Started",
               "COMPLETED" : "Completed",
               "SUSPENDED" : "Suspended",
               "IN_PROGRESS": "In Progress",
               "PAUSE": "Pause",
               "CANCEL": "Cancel"
           };
           var job = {
               "SYNC" : "One-way Sync",
               "TWO_WAY_SYNC" : "Two-way Sync"
           };

           var _len = data.length;

           for (var i = 0; i < _len; i++) {
               if((data[i].fromCloudId == null || data[i].toCloudId == null) && (data[i].jobStatus == "CANCEL")){
                   _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                   _displayImg = '<img src="../img/Pause.png" style="margin-left: 25%;cursor: not-allowed;opacity:0.2" class="dsblePause">';
               }
               else {
                   if (data[i].jobStatus == "CANCEL") {
                       _displayImg = '<img src="../img/Pause.png" style="margin-left: 25%;cursor: not-allowed;opacity:0.2" class="dsblePause">';
                       _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                       $(".fa-download").css("opacity","0.2").css("cursor","not-allowed");
                   }
                   else if (data[i].jobStatus == "PAUSE") {
                       _displayImg = '<img src="../img/Resume.png" style="margin-left: 25%;cursor: pointer;" class="syncPause">';
                       _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cnclBtn">';
                       $(".fa-download").css("opacity","0.2").css("cursor","not-allowed");
                   }
                   else if (data[i].jobStatus == "IN_PROGRESS") {
                       _displayImg = '<img src="../img/Pause.png" style="margin-left: 25%;cursor: pointer;" class="syncPause">';
                       _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cnclBtn">';
                       $(".fa-download").css("opacity","0.2").css("cursor","not-allowed");
                   }
                   else if (data[i].jobStatus == "COMPLETED") {
                       _displayImg = '<img src="../img/Pause.png" style="margin-left: 25%;cursor: pointer;" class="syncPause">';
                       _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cnclBtn">';
                       $(".fa-download").css("opacity","0.2").css("cursor","not-allowed");
                   }
                   else if (data[i].jobStatus == "SUSPENDED") {
                       _displayImg = '<img src="../img/Pause.png" style="margin-left: 25%;cursor: pointer;" class="syncPause">';
                       _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cnclBtn">';
                       $(".fa-download").css("opacity", "0.2").css("cursor", "not-allowed");
                   }
               }
               var obj = {
                   display : _displayImg,
                   cancel : _cancleImg
               };
               iconsArray.push(obj);
           }
           var _appendHtml ='<tr id="job_ID" class="jobsRow" jobtype="jobType"> <td style="width: 5%"> <div> <i class="fa fa-plus" style="cursor: pointer"></i> </div></td><td style="width: 34%">Job_Name</td><td style="width: 10%">Job_Type</td><td class="Job_Status" style="font-weight: bold;width: 25%;text-align: center;">Job_Status_Show</td><td style="width:10%">Date</td><td style="width: 10%;" JobID="jobID">_displayImg</td><td style="width: 10%;" JobId="jobId">_cancleImg</td></tr><tr class="appendSyncMigrateWorkSpaces" style="display: none"></tr>',//<td> <i class="fa fa-download"></i> </td>
               _len = data.length;
           $("#appendSyncJob tbody").html('');
           for(var i = 0; i < _len; i++){
               var _data = data[i],_html;
               var selectedtext ;
               selectedtext= data[i].jobType;
               if(selectedtext == undefined){
                   selectedtext =data[i].jobType;
               }
               if(selectedtext == null){
                   selectedtext="ONETIME";
               }
               // $("#jobType").text("selectedText");
               if(_data.jobStatus != "STARTED"){
                   if(selectedtext=="SYNC" || selectedtext == "TWO_WAY_SYNC") {
                       _html = _appendHtml.replace("job_ID", _data.id).replace("jobType", _data.jobType).replace("Job_Name", _data.jobName).replace("Job_Type", job[_data.jobType]).replace("Job_Status", _data.jobStatus).replace("Job_Status_Show", jobStatus[_data.jobStatus]).replace("Date", mappingOptions.getDateConversion(_data.createdTime)).replace('_displayImg',iconsArray[i].display).replace("jobID", _data.id).replace('_cancleImg',iconsArray[i].cancel).replace("jobId", _data.id);
                       $("#appendSyncJob tbody").append(_html);
                   }
               }
           }
       }, */
    /*   getWorkspaces : function (jobId,pageNumber,pageSize,jobType) { */
    getWorkspaces : function (jobId,pageNumber,pageSize) {
        $.ajax({
            type: "GET",
            url: apicallurl + "/move/newmultiuser/get/list/" + jobId + "?page_nbr=" + pageNumber + "&page_size=" + pageSize ,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                var _displayImg,_cancleImg,iconsArray = [];
                $.ajax({
                    type: "GET",
                    url: apicallurl + "/move/queue/status?jobId=" + jobId,
                    headers: {
                        "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                    },
                    success: function (refreshdata) {
                        var _len = refreshdata.length;

                        for (var i = 0; i < _len; i++) {
                            if (refreshdata[i].status == "NOT_PROCESSED" && refreshdata[i].threadStatus == "RESUME") {
                                _displayImg = '<img src="../img/Pause.png" style="cursor: not-allowed;opacity:0.2" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            else if (refreshdata[i].status == "CANCEL" && refreshdata[i].threadStatus == "CANCEL") {
                                _displayImg = '<img src="../img/Pause.png" style="cursor: not-allowed;opacity:0.2" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                                $(".fa-download").css("opacity","0.2").css("cursor","not-allowed");
                            }
                            else if (refreshdata[i].status == "PROCESSED" && refreshdata[i].threadStatus == "PAUSE") {
                                _displayImg = '<img src="../img/Resume.png" style="cursor: pointer" class="Pause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cancelBtn">';
                                $(".fa-download").css("opacity","0.2").css("cursor","not-allowed");
                            }
                            else if (refreshdata[i].status == "IN_PROGRESS" && refreshdata[i].threadStatus == "RESUME") {
                                _displayImg = '<img src="../img/Pause.png" style="cursor: pointer" class="Pause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cancelBtn">';
                            }
                            else if (refreshdata[i].status == "PAUSE" && refreshdata[i].threadStatus == "PAUSE") {
                                _displayImg = '<img src="../img/Resume.png" style="cursor:pointer;" class="Pause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cancelBtn">';
                            }
                            else if ((refreshdata[i].status == "PROCESSED" && refreshdata[i].threadStatus == "RESUME") && (data[i].processStatus == "PROCESSED_WITH_SOME_CONFLICTS" || data[i].processStatus == "SUSPENDED"  || data[i].processStatus == "WARNING" || data[i].processStatus == "PROCESSED_WITH_SOME_ERRORS" || data[i].processStatus == "PROCESSED_WITH_SOME_WARNINGS" || data[i].processStatus == "ERROR" || data[i].processStatus == "PROCESSED" || data[i].processStatus == "CANCEL" || data[i].processStatus == "CONFLICT" || data[i].processStatus == "NOT_PROCESSED")){
                                _displayImg = '<img src="../img/Pause.png" style="cursor: not-allowed;opacity:0.2;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            else if (refreshdata[i].status == "PROCESSED" && refreshdata[i].threadStatus == "RESUME") {
                                _displayImg = '<img src="../img/Pause.png" style="cursor:pointer;" class="Pause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cancelBtn">';
                            }
                            else if ((refreshdata[i].status == "PROCESSED" && refreshdata[i].threadStatus == "CANCEL") || (refreshdata[i].status == "IN_PROGRESS" && refreshdata[i].threadStatus == "CANCEL")) {
                                _displayImg = '<img src="../img/Resume.png" style="cursor:not-allowed;opacity:0.2;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cancelBtn">';
                            }
                            else if (data[i].threadStatus == "PROCESSED_WITH_SOME_CONFLICTS" || data[i].threadStatus == "PROCESSED_WITH_SOME_PAUSE" || data[i].threadStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE" || data[i].threadStatus == "SUSPENDED" || data[i].threadStatus == "WARNING" || data[i].threadStatus == "PROCESSED_WITH_SOME_ERRORS" || data[i].threadStatus == "PROCESSED_WITH_SOME_WARNINGS" || data[i].threadStatus == "ERROR" || data[i].threadStatus == "PROCESSED" || data[i].threadStatus == "CONFLICT" || data[i].threadStatus == "NOT_PROCESSED") {
                                _displayImg = '<img src="../img/Pause.png" style="opacity: 0.2;cursor: not-allowed;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="opacity: 0.2;cursor: not-allowed;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            else if (refreshdata[i].status == "SUSPENDED" && refreshdata[i].threadStatus == "SUSPENDED") {
                                _displayImg = '<img src="../img/Resume.png" style="cursor:not-allowed;opacity:0.2;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            else if (refreshdata[i].status == "PROCESSED" && refreshdata[i].threadStatus == "SUSPENDED") {
                                _displayImg = '<img src="../img/Resume.png" style="cursor:not-allowed;opacity:0.2;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            else if (data[i].threadStatus == "PROCESSED" && refreshdata[i].threadStatus == "SUSPENDED") {
                                _displayImg = '<img src="../img/Resume.png" style="cursor:not-allowed;opacity:0.2;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            else if (data[i].threadStatus == "PROCESSED" && refreshdata[i].threadStatus == "RESUME") {
                                _displayImg = '<img src="../img/Resume.png" style="cursor:not-allowed;opacity:0.2;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            else if (refreshdata[i].status == "TIMED_OUT" && refreshdata[i].threadStatus == "RESUME") {
                                _displayImg = '<img src="../img/Resume.png" style="cursor:not-allowed;opacity:0.2;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            //if((refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].status == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE") || (refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].status == "PROCESSED_WITH_SOME_PAUSE")){
                            if((refreshdata[i].userType === "MULTIUSER_INACTIVE" && data[i].processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE") || (refreshdata[i].userType === "MULTIUSER_INACTIVE" && data[i].processStatus == "PROCESSED_WITH_SOME_PAUSE")){
                                _displayImg = '<img src="../img/Resume.png" style="cursor:not-allowed;opacity:0.2;" class="disablePause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                            }
                            else if((refreshdata[i].userType === "MULTIUSER_INACTIVE") && (refreshdata[i].userPause == false)){
                                _displayImg = '<img src="../img/Pause.png" style="cursor: pointer" class="Pause">';
                                _cancleImg = '<img src="../img/cancel.png" style="cursor: pointer;margin-left: 12px;margin-top: -3px;" class="cancelBtn">';
                                if (data[i].processStatus == "PROCESSED_WITH_SOME_CONFLICTS" || data[i].processStatus == "SUSPENDED"  || data[i].processStatus == "WARNING" || data[i].processStatus == "PROCESSED_WITH_SOME_ERRORS" || data[i].processStatus == "PROCESSED_WITH_SOME_WARNINGS" || data[i].processStatus == "ERROR" || data[i].processStatus == "PROCESSED" || data[i].processStatus == "CANCEL" || data[i].processStatus == "CONFLICT" || data[i].processStatus == "NOT_PROCESSED"){
                                    _displayImg = '<img src="../img/Pause.png" style="cursor: not-allowed;opacity:0.2;" class="disablePause">';
                                    _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cleBtn">';
                                }
                                else if ((refreshdata[i].status == "PROCESSED" && refreshdata[i].threadStatus == "CANCEL") || (refreshdata[i].status == "IN_PROGRESS" && refreshdata[i].threadStatus == "CANCEL")) {
                                    _displayImg = '<img src="../img/Resume.png" style="cursor:not-allowed;opacity:0.2;" class="disablePause">';
                                    _cancleImg = '<img src="../img/cancel.png" style="cursor: not-allowed;opacity:0.2;margin-left: 12px;margin-top: -3px;" class="cancelBtn">';
                                }
                            }
                            var obj = {
                                display : _displayImg,
                                cancel : _cancleImg
                            };
                            iconsArray.push(obj);
                        }
                        var _appendHtml = '<td colspan="6"><table class="table table-striped" style="font-size: small;width:90%;margin-left:5%;margin-top: 2%;"> <thead> <tr style="border-bottom:2px solid #bbb!important;"> <th style="width: 03%;background: #4980b0;color: #fff;"></th> <th style="width: 25%;background: #4980b0;color: #fff;">From</th> <th style="width: 25%;background: #4980b0;color: #fff;">To</th> <th style="width: 20%;background: #4980b0;color: #fff;">Status</th> <th style="width: 15%;background: #4980b0;color: #fff;">Date</th> <th style="width: 9%;background: #4980b0;color: #fff;">Download</th> <th style="width: 12%;background: #4980b0;color: #fff;">Pause/Resume</th><th style="width: 12%;background: #4980b0;color: #fff;">Cancel</th>  </tr></thead> <tbody clas="appendMigrationWrkSpcs">appendWorkspaces </tbody> </table></td>',
                            //var _appendHtml = '<td colspan="6"><table class="table table-striped" style="font-size: small;width:90%;margin-left:5%;margin-top: 2%;"> <thead> <tr style="border-bottom:2px solid #bbb!important;"> <th style="width: 03%;background: #4980b0;color: #fff;"></th> <th style="width: 25%;background: #4980b0;color: #fff;">From</th> <th style="width: 25%;background: #4980b0;color: #fff;">To</th> <th style="width: 20%;background: #4980b0;color: #fff;">Status</th> <th style="width: 15%;background: #4980b0;color: #fff;">Date</th> <th style="width: 9%;background: #4980b0;color: #fff;">Download</th></tr></thead> <tbody clas="appendMigrationWrkSpcs">appendWorkspaces </tbody> </table></td>',

                            _workspaceLen = data.length, _html,
                            //_moveIdHtml = '<tr class="moveWorksapceList rmvdCloudClass" wrkSpaceId="workSpaceId" srcemail="srcEmailId" srccldname="srcCloudName" srccldid="srcCloudId" srcrt="srcRootId" dstnemail="dstnEmailId" dstncldname="dstnCloudName" dstncldid="dstnCloudId" dstnrt="dstnCloudRoot" prcsdCount="processedCount" errCount="errorCount" totCount="totalCount"> <td> <div> <i class="fa fa-plus" style="cursor: pointer"></i> </div></td><td> <div> <img src=" ../img/drive/circle/srcCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>srcEmailDisplay</div></div></td><td> <div> <img src=" ../img/drive/circle/dstCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>dstEmailDisplay</div></div></td><td class="processStatus">processStatusDispaly</td><td> <div>dateDisplay</div></td><td> <div style="margin-left: 24px;"><i class="fa fa-download" appendStyling></i></div></td></tr><tr class="moveReportDetails" style="display:none"></tr>';
                            /*             _moveIdHtml = '<tr class="moveWorksapceList rmvdCloudClass" wrkSpaceId="workSpaceId" srcemail="srcEmailId" srccldname="srcCloudName" srccldid="srcCloudId" srcrt="srcRootId" dstnemail="dstnEmailId" dstncldname="dstnCloudName" dstncldid="dstnCloudId" dstnrt="dstnCloudRoot" prcsdCount="processedCount" errCount="errorCount" totCount="totalCount"> <td> <div> <i class="fa fa-plus" style="cursor: pointer" jobtype="jobType"></i> </div></td><td> <div> <img src=" ../img/drive/circle/srcCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>srcEmailDisplay</div></div></td><td> <div> <img src=" ../img/drive/circle/dstCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>dstEmailDisplay</div></div></td><td class="processStatus pauseResumeVal">processStatusDispaly</td><td> <div>dateDisplay</div></td><td> <div style="margin-left: 24px;"><i class="fa fa-download" appendStyling></i></div></td><td><div style="margin-left: 33px;margin-top: -2px;" class="playResumeBtn">_displayImg </div></td><td><div>_cancleImg</div></td></tr><tr class="moveReportDetails" style="display:none"></tr>'; */
                            _moveIdHtml = '<tr class="moveWorksapceList rmvdCloudClass" wrkSpaceId="workSpaceId" srcemail="srcEmailId" srccldname="srcCloudName" srccldid="srcCloudId" srcrt="srcRootId" dstnemail="dstnEmailId" dstncldname="dstnCloudName" dstncldid="dstnCloudId" dstnrt="dstnCloudRoot" prcsdCount="processedCount" errCount="errorCount" totCount="totalCount"> <td> <div> <i class="fa fa-plus" style="cursor: pointer"></i> </div></td><td> <div> <img src=" ../img/drive/circle/srcCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>srcEmailDisplay</div></div></td><td> <div> <img src=" ../img/drive/circle/dstCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>dstEmailDisplay</div></div></td><td class="processStatus pauseResumeVal">processStatusDispaly</td><td> <div>dateDisplay</div></td><td> <div style="margin-left: 24px;"><i class="fa fa-download" appendStyling></i></div></td><td><div style="margin-left: 33px;margin-top: -2px;" class="playResumeBtn">_displayImg </div></td><td><div>_cancleImg</div></td></tr><tr class="moveReportDetails" style="display:none"></tr>';
                        var moveStatus = {
                            "PROCESSED": "Processed",
                            "PAUSE": "Pause",
                            "IN_PROGRESS": "In Progress",
                            "NOT_PROCESSED": "In queue",
                            "ERROR": "Error",
                            "IN_QUEUE": "In queue",
                            "WARNING": "Warning",
                            "SUSPENDED": "Suspended",
                            "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
                            "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
                            "PROCESSED_WITH_SOME_CONFLICTS": "Processed with some conflicts",
                            "CONFLICT": "Conflict",
                            "CANCEL": "Cancel",
                            "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE":"Processed With Some Conflicts and Pause",
                            "PROCESSED_WITH_SOME_PAUSE":"Processed With Some Pause"

                        };
                        for (var i = 0; i < _workspaceLen; i++) {
                            var _result = data[i];
                            if (_result.fromCloudId == null || _result.toCloudId == null)
                                _moveIdHtml = _moveIdHtml.replace('rmvdCloudClass', 'cloudRmvd');
                            else
                                _moveIdHtml = _moveIdHtml.replace('cloudRmvd', 'rmvdCloudClass');


                            if (_result.multiUserMove) {
                                _result = JSON.parse(JSON.stringify(_result, function (key, value) {
                                    if (value === null) {
                                        return "Not Available";
                                    }
                                    return value;
                                }));
                                if (refreshdata[i].threadStatus == "PAUSE" || refreshdata[i].threadStatus == "CANCEL" || refreshdata[i].threadStatus == "SUSPENDED") {
                                    if((refreshdata[i].userType === "MULTIUSER_INACTIVE" && data[i].processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE") || (refreshdata[i].userType === "MULTIUSER_INACTIVE" && data[i].processStatus == "PROCESSED_WITH_SOME_PAUSE")){
                                        if(_result.processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE"){
                                            var _value = "Trial completed with conflicts and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(_result.processStatus == "PROCESSED_WITH_SOME_PAUSE"){
                                            _value= "Trial completed and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].userPause == false && refreshdata[i].threadStatus == "PAUSE"){
                                            colorVal =_result.processStatus;
                                            _value = moveStatus[_result.processStatus];
                                        }
                                        else if(refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].userPause == false && refreshdata[i].threadStatus == "RESUME"){
                                            colorVal =_result.processStatus;
                                            _value = moveStatus[_result.processStatus];
                                        }
                                        else{
                                            _value = moveStatus[refreshdata[i].threadStatus];
                                            var colorVal =refreshdata[i].threadStatus;
                                        }
                                        /*	  _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('jobType', jobType).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus',colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel); */
                                        _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus',colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel);
                                    }
                                    else if((refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].status == "IN_PROGRESS") || (refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].status == "PROCESSED")){
                                        if(_result.processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE"){
                                            var _value = "Trial completed with conflicts and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(_result.processStatus == "PROCESSED_WITH_SOME_PAUSE"){
                                            _value= "Trial completed and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].userPause == false && refreshdata[i].threadStatus == "PAUSE"){
                                            colorVal =_result.processStatus;
                                            _value = moveStatus[_result.processStatus];
                                        }
                                        else if(refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].userPause == false && refreshdata[i].threadStatus == "RESUME"){
                                            colorVal =_result.processStatus;
                                            _value = moveStatus[_result.processStatus];
                                        }
                                        else{
                                            _value = moveStatus[refreshdata[i].threadStatus];
                                            var colorVal =refreshdata[i].threadStatus;
                                        }
                                        /*	_html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('jobType', jobType).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel); */
                                        _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel);
                                    }
                                    else{
                                        if(_result.processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE"){
                                            var _value = "Trial completed with conflicts and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(_result.processStatus == "PROCESSED_WITH_SOME_PAUSE"){
                                            _value= "Trial completed and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].userPause == false && refreshdata[i].threadStatus == "PAUSE"){
                                            colorVal =_result.processStatus;
                                            _value = moveStatus[_result.processStatus];
                                        }
                                        else if(refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].userPause == false && refreshdata[i].threadStatus == "RESUME"){
                                            colorVal =_result.processStatus;
                                            _value = moveStatus[_result.processStatus];
                                        }
                                        else{
                                            _value =moveStatus[refreshdata[i].threadStatus];
                                            var colorVal =refreshdata[i].threadStatus;
                                        }
                                        /*	  _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('jobType', jobType).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(refreshdata[i].threadStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel); */
                                        _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(refreshdata[i].threadStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel);
                                    }
                                }
                                else{
                                    if((refreshdata[i].userType === "MULTIUSER_INACTIVE" && data[i].processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE") || (refreshdata[i].userType === "MULTIUSER_INACTIVE" && data[i].processStatus == "PROCESSED_WITH_SOME_PAUSE")){
                                        if(_result.processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE"){
                                            var _value = "Trial completed with conflicts and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(_result.processStatus == "PROCESSED_WITH_SOME_PAUSE"){
                                            _value= "Trial completed and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else{
                                            _value = moveStatus[_result.processStatus];
                                            var colorVal =_result.processStatus;
                                        }
                                        /*	_html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('jobType', jobType).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(refreshdata[i].userType)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel); */
                                        _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(refreshdata[i].userType)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel);
                                    }
                                    else if((refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].status == "IN_PROGRESS") || (refreshdata[i].userType === "MULTIUSER_INACTIVE" && refreshdata[i].status == "PROCESSED")){
                                        if(_result.processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE"){
                                            var _value = "Trial completed with conflicts and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(_result.processStatus == "PROCESSED_WITH_SOME_PAUSE"){
                                            _value= "Trial completed and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else{
                                            _value = moveStatus[_result.processStatus];
                                            var colorVal =_result.processStatus;
                                        }
                                        /*    _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('jobType', jobType).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel); */
                                        _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly', _value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel);
                                    }
                                    else{
                                        if(_result.processStatus == "PROCESSED_WITH_SOME_CONFLICTS_AND_PAUSE"){
                                            var _value = "Trial completed with conflicts and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else if(_result.processStatus == "PROCESSED_WITH_SOME_PAUSE"){
                                            _value= "Trial completed and paused";
                                            var colorVal =_result.processStatus;
                                        }
                                        else{
                                            _value = moveStatus[_result.processStatus];
                                            var colorVal =_result.processStatus;
                                        }
                                        /*    _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('jobType', jobType).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly',_value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel); */
                                        _html = _html + _moveIdHtml.replace('workSpaceId', _result.id).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', colorVal).replace('processStatusDispaly',_value).replace('dateDisplay', mappingOptions.getDateConversion(_result.createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)).replace('_displayImg',iconsArray[i].display).replace('_cancleImg',iconsArray[i].cancel);

                                    }

                                }
                            }
                        }
                        _appendHtml = _appendHtml.replace('appendWorkspaces', _html).replace('undefined', '');
                        $('#appendJob #' + jobId).next('.appendMigrateWorkSpaces').html(_appendHtml).css('display', 'table-row');
                    }
                });
            }
        });
    },
    /*   getSyncWorkspaces : function (jobId,pageNumber,pageSize,jobtype) {
           $.ajax({
               type: "GET",
               url: apicallurl + "/move/newmultiuser/get/list/" + jobId + "?page_nbr=" + pageNumber + "&page_size=" + pageSize ,
               async: false,
               dataType: "json",
               headers: {
                   "Content-Type": "application/json",
                   "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                   "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
               },
               success: function (data) {
                   $.ajax({
                       type: "GET",
                       url: apicallurl + "/move/queue/status?jobId=" + jobId,
                       headers: {
                           "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                       },
                       success: function (refreshdata) {

                           var _appendHtml = '<td colspan="6"><table class="table table-striped" style="font-size: small;width:87%;margin-left:10%;margin-top: 2%;"> <thead> <tr style="border-bottom:2px solid #bbb!important;"> <th style="width: 03%;background: #4980b0;color: #fff;"></th> <th style="width: 25%;background: #4980b0;color: #fff;">From</th> <th style="width: 25%;background: #4980b0;color: #fff;">To</th> <th style="width: 20%;background: #4980b0;color: #fff;">Status</th> <th style="width: 15%;background: #4980b0;color: #fff;">Date</th> <th style="width: 9%;background: #4980b0;color: #fff;">Download</th>   </tr></thead> <tbody clas="appendMigrationWrkSpcs">appendSyncWorkspaces </tbody> </table></td>',
                               //var _appendHtml = '<td colspan="5"><table class="table table-striped" style="font-size: small;width:100%;margin-left:5%;margin-top: 2%;"> <thead> <tr style="border-bottom:2px solid #bbb!important;"> <th style="width: 03%;background: #4980b0;color: #fff;"></th> <th style="width: 25%;background: #4980b0;color: #fff;">From</th> <th style="width: 25%;background: #4980b0;color: #fff;">To</th> <th style="width: 20%;background: #4980b0;color: #fff;">Status</th> <th style="width: 15%;background: #4980b0;color: #fff;">Date</th> <th style="width: 9%;background: #4980b0;color: #fff;">Download</th></tr></thead> <tbody clas="appendMigrationWrkSpcs">appendWorkspaces </tbody> </table></td>',

                               _workspaceLen = data.length, _html,
                               //_moveIdHtml = '<tr class="moveWorksapceList rmvdCloudClass" wrkSpaceId="workSpaceId" srcemail="srcEmailId" srccldname="srcCloudName" srccldid="srcCloudId" srcrt="srcRootId" dstnemail="dstnEmailId" dstncldname="dstnCloudName" dstncldid="dstnCloudId" dstnrt="dstnCloudRoot" prcsdCount="processedCount" errCount="errorCount" totCount="totalCount"> <td> <div> <i class="fa fa-plus" style="cursor: pointer"></i> </div></td><td> <div> <img src=" ../img/drive/circle/srcCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>srcEmailDisplay</div></div></td><td> <div> <img src=" ../img/drive/circle/dstCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>dstEmailDisplay</div></div></td><td class="processStatus">processStatusDispaly</td><td> <div>dateDisplay</div></td><td> <div style="margin-left: 24px;"><i class="fa fa-download" appendStyling></i></div></td></tr><tr class="moveReportDetails" style="display:none"></tr>';

                               _moveIdHtml = '<tr class="moveSyncWorksapceList rmvdCloudClass" wrkSpaceId="workSpaceId" srcemail="srcEmailId" srccldname="srcCloudName" srccldid="srcCloudId" srcrt="srcRootId" dstnemail="dstnEmailId" dstncldname="dstnCloudName" dstncldid="dstnCloudId" dstnrt="dstnCloudRoot" prcsdCount="processedCount" errCount="errorCount" totCount="totalCount"> <td> <div> <i class="fa fa-plus" style="cursor: pointer" jobType = "jobtype"></i> </div></td><td> <div> <img src=" ../img/drive/circle/srcCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>srcEmailDisplay</div></div></td><td> <div> <img src=" ../img/drive/circle/dstCloudImage.png" alt="pic" class="migrationImage" style="float: left"/> <div>dstEmailDisplay</div></div></td><td class="processStatus pauseResumeVal">processStatusDispaly</td><td> <div>dateDisplay</div></td><td> <div style="margin-left: 24px;"><i class="fa fa-download" appendStyling></i></div></td></tr><tr class="moveReportDetails" style="display:none"></tr>';
                           var moveStatus = {
                               "PROCESSED": "Processed",
                               "PAUSE": "Pause",
                               "IN_PROGRESS": "In Progress",
                               "NOT_PROCESSED": "In queue",
                               "ERROR": "Error",
                               "IN_QUEUE": "In queue",
                               "WARNING": "Warning",
                               "SUSPENDED": "Suspended",
                               "PROCESSED_WITH_SOME_ERRORS": "Processed with some errors",
                               "PROCESSED_WITH_SOME_WARNINGS": "Processed with some warnings",
                               "PROCESSED_WITH_SOME_CONFLICTS": "Processed with some conflicts",
                               "CONFLICT": "Conflict",
                               "CANCEL": "Cancel"
                           };
                           var _html ='';
                           for (var i = 0; i < _workspaceLen; i++) {
                               var _result = data[i];
                               if (_result.fromCloudId == null || _result.toCloudId == null)
                                   _moveIdHtml = _moveIdHtml.replace('rmvdCloudClass', 'cloudRmvd');
                               else
                                   _moveIdHtml = _moveIdHtml.replace('cloudRmvd', 'rmvdCloudClass');


                               if (_result.multiUserMove) {
                                   _result = JSON.parse(JSON.stringify(_result, function (key, value) {
                                       if (value === null) {
                                           return "Not Available";
                                       }
                                       return value;
                                   }));
                                   localStorage.setItem("twoWaySrcIcon",_result.fromCloudName);
                                   localStorage.setItem("twoWayDstnIcon",_result.toCloudName);
                                   _html = _moveIdHtml.replace('workSpaceId', _result.id).replace('jobtype', jobtype).replace('srcEmailId', _result.fromMailId).replace('srcCloudName', _result.fromCloudName).replace('srcCloudId', _result.fromCloudId.id).replace('srcRootId', _result.fromRootId).replace('dstnEmailId', _result.toMailId).replace('dstnCloudName', _result.toCloudName).replace('dstnCloudId', _result.toCloudId.id).replace('dstnCloudRoot', _result.toRootId).replace('srcCloudImage', _result.fromCloudName).replace('srcEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.fromMailId, 30)).replace('dstCloudImage', _result.toCloudName).replace('dstEmailDisplay', CFManageCloudAccountsAjaxCall.getMaxChars(_result.toMailId, 30)).replace('processStatus', _result.processStatus).replace('processStatusDispaly', moveStatus[_result.processStatus]).replace('dateDisplay', mappingOptions.getDateConversion(refreshdata[i].createdTime)).replace('processedCount', _result.processedCount).replace('errorCount', _result.errorCount).replace('totalCount', _result.totalFilesAndFolders).replace('appendStyling', disableDwnldBtn(_result.processStatus)) + _html;
                               }
                           }
                           _appendHtml = _appendHtml.replace('appendSyncWorkspaces', _html);
                           $('#appendSyncJob #' + jobId).next('.appendSyncMigrateWorkSpaces').html(_appendHtml).css('display', 'table-row');

                       }
                   });
               }
           });
       }, */
    deleteJob:function () {
        var _jobId = localStorage["jobId"];
        if(_jobId != undefined){
            $.ajax({
                type: "DELETE",
                url: apicallurl + "/move/newmultiuser/delete/moveJob/" + _jobId ,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                    "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
                },
                success: function (data) {

                }
            });
        }
    },
    createJob:function () {
        CsvOperation('','rmv1');
        if($('#mapdUsers input[name=csvMapngCheckBox]:checked').length){
            $('#mapdUsers input[name=csvMapngCheckBox]:checked').each(function(){
                var _d = $(this).parent().parent();
                var obj = {
                    "fromCloudId": {
                        "id": $(_d).attr('srccldid'),
                    },
                    "toCloudId": {
                        "id": $(_d).attr('dstncldid'),
                    },
                    "sourceFolderPath": decodeURI($(_d).attr('srcFolderPath')),
                    "destFolderPath": decodeURI($(_d).attr('dstnFolderPath')),
                    "destinationFolderName": $(_d).attr('migrateDstnFolderName'),
                    "isCSV": "true"
                };
                var EmailObj = {
                    "fromMailId": $(_d).attr('srcemail'),
                    "fileName":$(_d).attr('srcemail'),
                    "toMailId": $(_d).attr('dstnemail'),
                    "fromCloudName":$(_d).attr('srccldname'),
                    "toCloudName":$(_d).attr('dstncldname'),
                };
                CsvOperation(obj,'set');
                CsvOperation(EmailObj,'setCsv');
            });
        }
        else{
            $('#mapdUsers input[name=csvMapngCheckBox]').each(function(){
                var _d = $(this).parent().parent();
                $(this).prop('checked',false);
                var obj = {
                    "fromCloudId": {
                        "id": $(_d).attr('srccldid'),
                    },
                    "toCloudId": {
                        "id": $(_d).attr('dstncldid'),
                    },
                    "sourceFolderPath": decodeURI($(_d).attr('srcFolderPath')),
                    "destFolderPath": decodeURI($(_d).attr('dstnFolderPath')),
                    "destinationFolderName": $(_d).attr('migrateDstnFolderName'),
                    "isCSV": "true"
                };
                var EmailObj = {
                    "fromMailId": $(_d).attr('srcemail'),
                    "fileName":$(_d).attr('srcemail'),
                    "toMailId": $(_d).attr('dstnemail'),
                    "fromCloudName":$(_d).attr('srccldname'),
                    "toCloudName":$(_d).attr('dstncldname'),
                };
                CsvOperation(obj,'delete');
                CsvOperation(EmailObj,'delete');
            });
        }
        mappingOptions.localStorageOperation('','rmv1');
        if($('#mapdUsers input[name=inputMapdUrs]:checked').length){
            if(!$(this).parents("table").find('tbody tr').hasClass("automapRow")){
                $('#mapdUsers input[name=inputMapdUrs]:checked').each(function(){
                    var _obj ={
                        "fromCloudId": {
                            "id":$(this).closest('tr').attr('srccldid'),
                        },
                        "toCloudId": {
                            "id":$(this).closest('tr').attr('dstncldid')
                        }
                    };
                    var _objEmail ={
                        "fromMailId": $(this).closest('tr').attr('srccldid'),
                        "fileName":$(this).closest('tr').attr('srcemail'),
                        "toMailId": $(this).closest('tr').attr('dstncldid'),
                        "fromCloudName":$(this).closest('tr').attr('srccldname'),
                        "toCloudName":$(this).closest('tr').attr('dstncldname'),

                    };
                    mappingOptions.localStorageOperation(_obj,'set');
                    mappingOptions.localStorageOperation(_objEmail,'set1');
                });
            }
            else{
                $('#mapdUsers input[name=inputMapdUrs]:checked').each(function(){
                    var _d = $(this).parent().parent();
                    var _obj ={
                        "fromCloudId": {
                            "id":$(_d).attr('srccldid'),
                        },
                        "toCloudId": {
                            "id":$(_d).attr('dstncldid'),
                        }
                    };
                    var _objEmail ={
                        "fromMailId": $(_d).attr('srcemail'),
                        "fileName":$(_d).attr('srcemail'),
                        "toMailId": $(_d).attr('dstnemail'),
                        "fromCloudName":$(_d).attr('srccldname'),
                        "toCloudName":$(_d).attr('dstncldname'),

                    };
                    mappingOptions.localStorageOperation(_obj,'set');
                    mappingOptions.localStorageOperation(_objEmail,'set1');
                });
            }
        }
        else{
            $('#mapdUsers input[name=inputMapdUrs]').each(function(){
                var _d = $(this).parent().parent();
                $(this).prop('checked',false);
                var _obj ={
                    "fromCloudId": {
                        "id":$(_d).attr('srccldid'),
                    },
                    "toCloudId": {
                        "id":$(_d).attr('dstncldid'),
                    }
                };
                var _objEmail ={
                    "fromMailId": $(_d).attr('srcemail'),
                    "fileName":$(_d).attr('srcemail'),
                    "toMailId": $(_d).attr('dstnemail'),
                    "fromCloudName":$(_d).attr('srccldname'),
                    "toCloudName":$(_d).attr('dstncldname'),

                };
                mappingOptions.localStorageOperation(_obj,'delete');
                mappingOptions.localStorageOperation(_objEmail,'delete1');
            });
        }
        var srcName = localStorage.getItem('multiUsrSrcCldName');
        var dstnName = localStorage.getItem('multiUsrDstnCldName');
        if(srcName === 'DROPBOX_BUSINESS' && dstnName === 'ONEDRIVE_BUSINESS_ADMIN' || srcName === 'ONEDRIVE_BUSINESS_ADMIN' && dstnName === 'DROPBOX_BUSINESS'){
            var _dataValue = JSON.parse(localStorage.getItem('folderMappings'));
        }
        else{
            if($('input[name="csvMapngCheckBox"]:checked')){
                var _dataValue = JSON.parse(localStorage.getItem('FolderChecked'));
            }
            if(_dataValue == null ||  _dataValue === []) {
                _dataValue =[];
                localStorage.removeItem("csvName");
            }
            if($('#mapdUsers input[name="inputMapdUrs"]:checked').length !== 0){
                var inpUsers = JSON.parse(localStorage.getItem('selectedMappings'));
                for(var i=0;i<inpUsers.length;i++) {
                    _dataValue.push(inpUsers[i]);
                }
            }
        }

        /*   var _dataValue = JSON.parse(localStorage.getItem('selMappings'));
              if(_dataValue.length === 0){
                  setTimeout(function () {
                      $("#CFShowLoading").css("display","none");
                  },500);
                  $(document).find("#chkInput").prop( "checked", false );
                  $('input[name="inputMapdUrs"]').removeAttr('checked');
                  $('input[name="csvMapngCheckBox"]').removeAttr('checked');
                  $("#forNextMove").addClass('disabled');
              }
              else{ */

        $.ajax({
            type: "POST",
            url: apicallurl + "/move/newmultiuser/create/job",
            //data:JSON.stringify(_job),
            data: JSON.stringify(_dataValue),
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                //$(document).find(".jobName").text(data.jobName);
                //$(document).find(".jobName").attr("job-id",data.id);
                $(document).find(".jobCount").text(data.listOfMoveWorkspaceId.length);
                //$(document).find(".jobPath").text('/');
                $('.ClickedEditPath .pathName').val('/');
                if(localStorage.getItem("csvName")){
                    $(document).find(".destinationPath").css("display","none");
                }
                else{
                    $(document).find(".destinationPath").css("display","");
                }
                if((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN" && data.toCloudName === "DROPBOX_BUSINESS")|| (data.fromCloudName === "DROPBOX_BUSINESS" && data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")){
                    $(document).find(".destinationPath").css("display","none");
                }
                else{
                    $(document).find(".destinationPath").css("display","");
                }
                if((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN" && data.toCloudName === "DROPBOX_BUSINESS")){
                    $('#jobType_dropDown').find('#cusDelta').css('display','none');
                }
                else{
                    var _isCustomUser =  CFManageCloudAccountsAjaxCall.isCustomUser();
                    if(_isCustomUser === false){
                        $('#jobType_dropDown').find('#cusDelta').css('display','none');
                    }
                    else{

                        $('#jobType_dropDown').find('#cusDelta').css('display','');
                    }
                }
                //$(document).find(".jobName").val(data.jobName);
                $('.ClickedEditJob .jobName').val(data.jobName);
                localStorage.setItem('jobName',data.jobName);
                $("#jobEmptyPopup").css("display","none");
                $('.Editing_Jobname').attr("disabled","disabled");
                $("#checkTimediv").css("display","none");
                $(".Job_NAme").css("display","");
                //var _selectedTrValue =  $("#mapdUsers tbody tr input[name=inputMapdUrs]").parents('tr');
                $(document).find(".jobFrom").html('<div style="display: flex"> <img src="../img/drive/circle/' + data.fromCloudName +'.png" alt="'+ data.fromCloudName+'" class="mgrtnOptionsCloudImg"> <div>' + CLName[data.fromCloudName] +'</div></div>');
                $(document).find(".jobTo").html('<div style="display: flex"> <img src="../img/drive/circle/' + data.toCloudName +'.png" alt="'+ data.toCloudName+'" class="mgrtnOptionsCloudImg"> <div>' + CLName[data.toCloudName] +'</div></div>');
                $("#mappingClouds").css("display","none");
                $("#mappingUsers").css("display","none");
                if(data.fromCloudName === "DROPBOX_BUSINESS" && data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN"){
                    $('#mappingOptionsNew').css("display","");
                    $('.optionsDiv1 .Editing_Jobname').val(data.jobName);
                }
                else{
                    $('#mappingOptions').css("display","");
                }



                /*     if(data.fromCloudName === "EGNYTE_ADMIN" && data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN"){
                         $('#jobType_dropDown').find('#delta').css('display','none');
                         $('#jobType_dropDown').find('#twowaySync').css('display','none');
                     }
                     else{
                         $('#jobType_dropDown').find('#delta').css('display','');
                         $('#jobType_dropDown').find('#onewaySync').css('display','');
                         $(".destinationPath").css("display","");
                     }
                     if(data.fromCloudName === "SHAREPOINT_ONLINE_BUSINESS" && data.toCloudName === "SYNCPLICITY"){
                         if($('#jobType_dropDown :selected').text()== "Two-way sync"){
                             $(".spoToSyncCity").css("display","none");
                         }
                         else{
                             $(".spoToSyncCity").css("display","");
                         }
                         localStorage.setItem("fromCloudId",data.fromCloudId);
                         localStorage.setItem("toCloudId",data.toCloudId);
                     }
                     else{
                         $(".spoToSyncCity").css("display","none");
                     }
                     localStorage.setItem("fromCloudName",data.fromCloudName);
                     localStorage.setItem("toCloudName",data.toCloudName); */
                $('#mappedMigration').css("display","none");
                //     $('#mappedSyncMigration').css("display","none");
                $("#forNextMove").removeClass('disabled');
                $("#forPreviousMove").removeClass('disabled');
                localStorage.setItem("jobId",data.id);
                setTimeout(function () {
                    $("#CFShowLoading").css("display","none");
                },2000);
                fldrStorage('','rmv');
                fldrStorage('','rmv1');
                mappingOptions.appendOptions(data.fromCloudName,data.toCloudName);
                var _step = parseInt($("#forNextMove").attr("data-step"));
                if(_step == 2) {
                    _step = _step + 1;
                    $("#teamMigrationWidget ul li[data-val=" + (_step - 1) + "]").removeClass("active").addClass("completed");
                    $("#forNextMove").attr("data-step", _step);
                    $("#forPreviousMove").attr("data-prev", _step);
                    $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");
                }
                else if(_step == 3){
                    $("#teamMigrationWidget ul li[data-val=" + (_step + 1) + "]").removeClass("active");
                    $("#forNextMove").attr("data-step", _step);
                    $("#forPreviousMove").attr("data-prev", _step);
                    $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");
                }
                //        localStorage.removeItem("selectedMappings");
                // localStorage.removeItem("selMappings");
            }
        });
        //   }
    },
    migrationOptionsChecked:function(){
        var _specialCharacter = "_",
            _sharedContent = false,
            _fusionTables = false,
            _drawings = false;
        if( localStorage.getItem('multiUsrSrcCldName') == "DROPBOX_BUSINESS" && localStorage.getItem('multiUsrDstnCldName') == "ONEDRIVE_BUSINESS_ADMIN") {
            _specialCharacter = '-';
        }
        else{
            // if($(document).find("#rplcChar:checked").length)
            _specialCharacter =  $(document).find(".splChar").find("[name='splCharacter']:checked").val();
        }
        if($(document).find("#chkShrdCntnt:checked").length)
            _sharedContent =  true;
        if($(document).find("#fsnTbls:checked").length || $("#tmFldrs:checked").length)
            _fusionTables =  true;
        if($(document).find("#gsteDrawings:checked").length)
            _drawings =  true;
        /*    if(localStorage.getItem('fromCloudName') == "SHAREPOINT_ONLINE_BUSINESS" && localStorage.getItem('toCloudName') == "SYNCPLICITY"){
                if($('#jobType_dropDown :selected').text()== "Two-way sync"){
                    localStorage.removeItem('fusionTables');
                }
                if(localStorage.getItem('fusionTables')){
                    _fusionTables = localStorage.getItem('fusionTables');
                }
            } */
        return "&pageNo=1&pageSize=10&specialCharacter=" + encodeURIComponent(_specialCharacter) + "&sharedContent=" + _sharedContent+"&fusionTables=" + _fusionTables + "&drawings=" + _drawings;
    },
    updateJob:function () {
        // if(1)
        // {
        //     $("#CFmulUsrmoveStatus").modal('show');
        //     $("#forNextMove").removeClass("disabled");
        //     return false;
        // }
        // var _jobName =  $(document).find(".editJob .jobName").text(),
        //     _jobid  =  $(document).find(".jobName").attr("job-id"),
        //     _path = $('.editPath .jobPath').text();//$("input.pathName").val();


        //emptying jobname
        var _srcOpt = localStorage.getItem('multiUsrSrcCldName'),dstnOpt = localStorage.getItem('multiUsrDstnCldName');
        var _table = $("#preview table");
        $(_table).find(".jobName").text('');
        $(_table).find(".count").html('');
        //$(_table).find(".count").html(data.listOfMoveWorkspaceId.length + '<div class="pull-right" style="cursor: pointer;text-decoration:underline">Show</div>');
        $(_table).find(".others").text('');
        $(_table).find(".srcImg").html('');
        $(_table).find(".dstImg").html('');
        $(_table).find(".path").text('');

        var selectedText = $("#jobType_dropDown").find("option:selected").val();
        if(dstnOpt === "ONEDRIVE_BUSINESS_ADMIN" &&  _srcOpt === "DROPBOX_BUSINESS")
            var _jobName =  $(".Editing_Jobname").val();
        else
            var _jobName =  $(".ClickedEditJob .jobName").val();
        var _jobid  =  localStorage.getItem('jobId'),
            _path = $(".ClickedEditPath .pathName").val();//$("input.pathName").val();
        _path = _path.indexOf('/') == 0 ? _path.substring(1) : _path;
        //  var _val = $('.emailNotify input').val();
        //  _val = _val.split(/[;,]+/);;
        // // _val = mappingOptions.uniqueArray(_val);
        //   var _emailLen = _val.length;
        if(_jobName.length < 4)
        {
            showNotyNotification("error","For job name minimum 4  characters are required.");
            $("#forNextMove").removeClass('disabled');
            return false;
        }
        var _val = [];
        var fileValue= [];
        $(".notifiedFileTypes .emailParent span").each(function(){
            fileValue.push($(this).text());
        });
        var _fileTypeNot = $("#fileTypeExc").val();
        if(_fileTypeNot.length){
            if (!mappingOptions.checkfileType(_fileTypeNot))
                return false;
            else
                fileValue.push(_fileTypeNot);
        }


        if((_srcOpt === "ONEDRIVE_BUSINESS_ADMIN" && dstnOpt === "DROPBOX_BUSINESS") ||(dstnOpt === "ONEDRIVE_BUSINESS_ADMIN" &&  _srcOpt === "DROPBOX_BUSINESS")) {

            $(".notifiedMailsNew .emailParent span").each(function(){
                _val.push($(this).text());
            });
            var _emailNot = $("#additionalEmails").val();
            if(_emailNot.length){
                if (!mappingOptions.checkEmailNew(_emailNot))
                    return false;
                else
                    _val.push(_emailNot);
            }
        }
        else{
            $(".notifiedMails .emailParent span").each(function(){
                _val.push($(this).text());
            });
            var _emailNot = $("#additionalEmailsold").val();
            if(_emailNot === undefined){
                _emailNot ="";
            }
            if(_emailNot.length){
                if (!mappingOptions.checkEmail(_emailNot))
                    return false;
                else
                    _val.push(_emailNot);
            }
        }
        $(_table).find("tbody").find(".dstnPath").css("display", "none");
        var _DeltaVal = 'false',
            _fileVerVal = 'false',
            _ZipVal = 'false';
        /*  var _filecheckedVal = $("input[name='versionHistory']:checked").val(); */
        var _ZipcheckedVal = $("input[name='formats']:checked").val();

        var  _fileSize = $("#SizeVal").val();
        if(_fileSize.length == 0){
            _fileSize = "";

        }
        else {
            _fileSize = $("#SizeVal").val() + ":" + $(".select_jobStart").val();
        }

        /* if(_filecheckedVal === "YES") {
             _fileVerVal = "true";
         }*/
        if(_ZipcheckedVal === "YES"){
            _ZipVal = "true";
        }
        //var isBackup = 'false';
        if(($('#jobType_dropDown :selected').text() === "Delta" ) || $("#Select_job").find("input:checked").val() === "DElTA" ){
            _DeltaVal = 'true';
            if(_fileSize.length == 0){
                _changeUrl =  "/move/newmultiuser/update/" + _jobid +"?jobName=" + _jobName + "&migrateFolderName=" + _path + "&isDeltaMigration="+_DeltaVal+mappingOptions.migrationOptionsChecked()+ "&withPermissions=true&unsupportedFiles="+_ZipVal;
            }
            else
                _changeUrl =  "/move/newmultiuser/update/" + _jobid +"?jobName=" + _jobName + "&migrateFolderName=" + _path + "&isDeltaMigration="+_DeltaVal+mappingOptions.migrationOptionsChecked()+ "&withPermissions=true&unsupportedFiles="+_ZipVal+"&restrictFileSize="+_fileSize;
        }
        else{
            if(_fileSize.length == 0){
                _changeUrl =  "/move/newmultiuser/update/" + _jobid +"?jobName=" + _jobName + "&migrateFolderName=" + _path +mappingOptions.migrationOptionsChecked()+ "&withPermissions=true&unsupportedFiles="+_ZipVal;
            }
            else
                _changeUrl =  "/move/newmultiuser/update/" + _jobid +"?jobName=" + _jobName + "&migrateFolderName=" + _path +mappingOptions.migrationOptionsChecked()+ "&withPermissions=true&unsupportedFiles="+_ZipVal+"&restrictFileSize="+_fileSize;

        }
        // for(var i=0;i<_emailLen;i++){
        //     var value = _val[i];
        //     if(value.length && !mappingOptions.checkEmail(value)){
        //         $("#forNextMove").removeClass('disabled');
        //         return false;
        //     }
        //
        // }
        /*     var _DeltaVal = 'false';
             var isBackup = 'false';
             var twoWayBackUp = 'false';
             if($('#jobType_dropDown :selected').text() === "Delta"){
                 _DeltaVal = 'true';
             }
             var url = apicallurl + "/move/newmultiuser/update/" + _jobid +"?jobName=" + _jobName + "&migrateFolderName=" + _path + "&isDeltaMigration="+_DeltaVal+"&isBackup="+isBackup+mappingOptions.migrationOptionsChecked()+"&twoWayBackUp=" +twoWayBackUp;

             if($('#jobType_dropDown :selected').text() === "One-way sync"){
                 isBackup = 'true';
             }
             if($('#jobType_dropDown :selected').text() === "Two-way sync"){
                 twoWayBackUp = 'true';
             }
             if($('#jobType_dropDown :selected').text()== "One-way sync" || $('#jobType_dropDown :selected').text()== "Two-way sync"){
                 var d = new Date();
                 var f = d.toString().split(' ')[4].split(':');
                 var g = d.toString().split(' ')[2];
                 var h = d.toString().split(' ')[0].toUpperCase();

                 var cronValue = "seconds minutes hours dayOfMonth month dayOfWeek year";
                 var tenMins = cronValue.replace('seconds','0').replace('minutes','0/10').replace('hours','*').replace('dayOfMonth','1/1').replace('month','*').replace('dayOfWeek','?').replace('year','*');
                 var day = cronValue.replace('seconds',f[2]).replace('minutes',f[1]).replace('hours',f[0]).replace('dayOfMonth','?').replace('month','*').replace('dayOfWeek','*').replace('year','*');
                 var month = cronValue.replace('seconds',f[2]).replace('minutes',f[1]).replace('hours',f[0]).replace('dayOfMonth',g).replace('month','*').replace('dayOfWeek','?').replace('year','*');
                 var week = cronValue.replace('seconds',f[2]).replace('minutes',f[1]).replace('hours',f[0]).replace('dayOfMonth','?').replace('month','*').replace('dayOfWeek',h).replace('year','*');
                 if($('#selectFrequency input[name="selectFreq"]:checked').val() == "Day"){
                     var cronExpression = day;
                 }
                 else if ($('#selectFrequency input[name="selectFreq"]:checked').val() == "tenMinutes") {
                     var cronExpression = tenMins;
                 }
                 else if ($('#selectFrequency input[name="selectFreq"]:checked').val() == "Week") {
                     var cronExpression = week;
                 }
                 else{
                     var cronExpression = month;
                 }

                 var url = apicallurl + "/move/newmultiuser/update/" + _jobid +"?jobName=" + _jobName + "&migrateFolderName=" + _path + "&isDeltaMigration="+_DeltaVal+"&isBackup="+isBackup+mappingOptions.migrationOptionsChecked()+"&twoWayBackUp=" +twoWayBackUp+"&cronExpression=" +cronExpression;
             } */
        $.ajax({
            type: "PUT",
            //  url: url,
            url: apicallurl + _changeUrl,
            aync : false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                //   localStorage.setItem('jobList',JSON.stringify(data.jobList));
                //var _splChar = $(".splChar input[name=splCharacter]:checked").val();
                // var _shareContent = $("#chkShrdCntnt").is(":checked");
                var _table = $("#preview table");
                //    $(_table).find("tbody").find("#JobName").css("display","");
                $(_table).find(".jobName").text(data.jobName);
                localStorage.setItem('jobName',data.jobName);
                //     if($('#jobType_dropDown :selected').text() != "One-way sync"){
                $(_table).find(".count").html(data.listOfMoveWorkspaceId.length);
                /*   }
             else {
                        $(_table).find(".count").html(data.jobList.length);
                        //  $(_table).find("tbody").find("#JobName").css("display", "none");
                    }
                    if (localStorage.getItem("csvName")||(data.fromCloudName === "EGNYTE_ADMIN" && data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) { */
                if (localStorage.getItem("csvName") || (data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN" && data.toCloudName === "DROPBOX_BUSINESS")|| (data.fromCloudName === "DROPBOX_BUSINESS" && data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
                    $(_table).find("tbody").find(".dstnPath").css("display", "none");
                }
                else{
                    $(_table).find("tbody").find(".dstnPath").css("display", "");
                    var _path = "/" + data.migrateFolderName;
                    $(_table).find(".path").text(_path);
                }
                if (data.fromCloudName === "DROPBOX_BUSINESS" && data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN") {
                    $(_table).find("tbody").find("#JobName").css("display", "");
                    $(_table).find("tbody").find(".fileSizeExclude").css("display", "");
                    $(_table).find("tbody").find(".unSupZipFiles").css("display", "");
                    $(_table).find("tbody").find(".fileTypeExclude").css("display", "");
                     if(data.restrictFileSize === null || data.restrictFileSize === undefined || data.restrictFileSize === "null" || data.restrictFileSize === "undefined" ){  
                    var  size = '-';
                    }
                    else{
                       size = data.restrictFileSize;
                        size = size.replace(':',' ');
                    }
                    $(_table).find(".fileSizeEx").text(size);
                    if(data.unsupportedFiles === true){
                        var unsupported = 'Yes';
                    }
                    else{
                        var unsupported = 'No';
                    }
                    $(_table).find(".unSupportedZipFiles").text(unsupported);
                    //$(_table).find(".fileVerHis").text(fileSizeEx);
                }
                else{
                    $(_table).find("tbody").find("#JobName").css("display", "none");
                    $(_table).find("tbody").find(".fileSizeExclude").css("display", "none");
                    $(_table).find("tbody").find(".unSupZipFiles").css("display", "none");
                    $(_table).find("tbody").find(".fileTypeExclude").css("display", "none");
                }
                //$(_table).find(".count").html(data.listOfMoveWorkspaceId.length + '<div class="pull-right" style="cursor: pointer;text-decoration:underline">Show</div>');
                $(_table).find(".others").text('');
                $(_table).find(".srcImg").html('<img src="../img/drive/circle/' + data.fromCloudName+ '.png" alt="cloud" style="height: 25px;"><div style="margin: 5px;">' + CLName[data.fromCloudName]+ '</div>');
                $(_table).find(".dstImg").html('<img src="../img/drive/circle/' + data.toCloudName+ '.png" alt="cloud" style="height: 25px;"><div style="margin: 5px;">' + CLName[data.toCloudName]+ '</div>');
                $(_table).find("#jobType").text(data.jobType);
                /*   var _step = parseInt($("#forNextMove").attr("data-step"));
                   if (_step == 3) {
                       $("#mappingClouds").css("display", "none");
                       $("#mappingUsers").css("display", "none");
                       $('#mappingOptions').css("display", "none");
                       $("#preview").css("display", "");
                       $('#mappedMigration').css("display", "none");

                       $("#forNextMove").css({"width": "140px", "margin-left": "-51px"});
                       $("#forNextMove span").text("Start Migration");
                       _step = _step + 1;
                       $("#teamMigrationWidget ul li[data-val=" + (_step - 1) + "]").removeClass("active").addClass("completed");
                       $("#forNextMove").attr("data-step", _step);
                       $("#forPreviousMove").attr("data-prev", _step);
                       $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");

                   } */
                var _srcOpt = data.fromCloudName,dstnOpt = data.toCloudName;
                mappingOptions.migrationRestrictions(_jobid,_val,fileValue,_srcOpt,dstnOpt);
                mgrtnPrs.pgntdData(1,10);

            }
        });
        return true;
    },
    /*  spoToSyncUpdate:function(version) {
          var sourceCldId = localStorage.getItem('fromCloudId');
          var destCldId = localStorage.getItem('toCloudId');
          var userId = localStorage.getItem('UserId');
          $.ajax({
              type: "GET",
              url: apicallurl + "/move/multiuser/verify/versionhistory/" + userId + "?sourceCloudId=" + sourceCldId + "&destinationCloudId=" + destCldId + "&version=" + version,
              headers: {
                  "Content-Type": "application/json",
                  "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                  "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
              },
              success: function (data) {
                  console.log(data);
                  if (data === "Not_Allow:true") {
                      $('#spoToSyncVersion').modal('show');
                      $('#spoToSync').text('Previously for these combination of users, migration was initiated with File versioning. Click continue for migration with File versioning.');
                      $("#forNextMove").removeClass('disabled');
                      localStorage.setItem('fusionTables',true);
                  }
                  else if (data === "Not_Allow:false") {
                      $('#spoToSyncVersion').modal('show');
                      $('#spoToSync').text('Previously for these combination of users, migration was initiated without File versioning. Click continue for migration without File versioning.');
                      $("#forNextMove").removeClass('disabled');
                      localStorage.setItem('fusionTables',false);
                  }
                  else {
                      localStorage.setItem('fusionTables',version);
                      mappingOptions.updateJob();
                  }
              }
          });
      }, */
    migrationRestrictions:function (jobid,emails,fileValue,_srcOpt,dstnOpt) {
        // var _emails = [],_json;
        // $("#emailId span").each(function(){
        //     _emails.push($(this).text());
        // });
        if((_srcOpt === "ONEDRIVE_BUSINESS_ADMIN" && dstnOpt === "DROPBOX_BUSINESS") ||(dstnOpt === "ONEDRIVE_BUSINESS_ADMIN" &&  _srcOpt === "DROPBOX_BUSINESS")){
            _json ={
                "notToMoveExtension":fileValue,
                "emailValues":emails,
            }
        }
        else{
            _json ={
                "notToMoveExtension":[],
                "emailValues":emails
            }
        }
        $.ajax({
            type: "PUT",
            url: apicallurl + "/move/newmultiuser/update/restriction/" + jobid ,
            data : JSON.stringify(_json),
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                //mappingOptions.migrationInitiation(jobid);
                var _len,len ;

                var _table = $("#preview table");
                if(data.emailValues != null)
                    _len = data.emailValues.length;

                $(_table).find(".emails").text(' - ');
                if(data.notToMoveExtension != null)
                    len = data.notToMoveExtension.length;
                $(_table).find(".fileTypeEx").text(' - ');

                for(var i =0; i < _len;i++){
                    if(i === 0)
                        $(_table).find(".emails").text(data.emailValues[0]);
                    else
                        $(_table).find(".emails").append(", " + data.emailValues[i]);
                }
                if(data.fromCloudName === 'DROPBOX_BUSINESS' && data.toCloudName === 'ONEDRIVE_BUSINESS_ADMIN'){
                    for(var j =0; j < len;j++){
                        if(j === 0)
                            $(_table).find(".fileTypeEx").text(data.notToMoveExtension[0]);
                        else
                            $(_table).find(".fileTypeEx").append(", " + data.notToMoveExtension[j]);
                    }
                }
            }
        });
    },
    migrationIntnRootFolder:function(jobid){
        $.ajax({
            type: "POST",
            url: apicallurl + "/move/newmultiuser/rootfolderid/" + jobid,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {


            }
        });
    },
    migrationInitiation:function(jobid){
        $.ajax({
            type: "POST",
            url: apicallurl + "/move/newmultiuser/create/" + jobid,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                mappingOptions.localStorageOperation('','rmv');
                mappingOptions.folderOperation('','rmv');
                CsvOperation('','rmv');
                localStorage.removeItem("jobId");
                mappingOptions.appendMigrationJobs(1,60);
                $("#mappingClouds").css("display","none");
                $("#mappingUsers").css("display","none");
                $('#mappingOptions').css("display","none");
                $('#mappingOptionsNew').css("display","none");
                $("#preview").css("display","none");
                $('#mappedMigration').css("display","");
                //   $('#mappedSyncMigration').css("display","none");


                var _step = parseInt($("#forNextMove").attr("data-step"));
                _step = _step + 1;

                $("#teamMigrationWidget ul li[data-val=" + (_step - 1) + "]").removeClass("active").addClass("completed");
                $("#forNextMove").attr("data-step",_step);
                $("#forPreviousMove").attr("data-prev",_step );
                $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");
            }
        });
    },
    acceptCharcter : function(val){
        if (/[a-z\d_-]/i.test(val))
            return val;
        else if(val == '.')
            return val;
        /*   else if(val == ' ')
               return val; */
        else{
            // $('#acceptCharacterError').modal('show');
            showNotyNotification("error","Special character not allowed.");
            return false;
        }
    },
    uniqueArray:function(items){
        return Array.from(new Set(items));
    },
    checkEmail:function (email) {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            showNotyNotification("error", email + ' is not valid email address.');
            return false;
        }
        return mappingOptions.checkDuplicatedEmails(email);

        // var isExists = false;
        // $(emailsList).each(function(){
        //     if($(this).text().trim().toLowerCase() == email.toLowerCase())
        //         isExists = true;
        // });
        // var _len = emailsList.length;
        // for(var i=0;i<_len;i++){
        //     if(emailsList[i].toLowerCase() == email.toLowerCase())
        //              isExists = true;
        // }
        // else if(isExists){
        //     showNotyNotification("error", email + 'This E-mail is already added for notification');
        //     return false;
        // }
        // else{
        //     showNotyNotification("notify", email + " is added for notification.");
        //     return true;
        // }
    },
    checkEmailNew:function (email) {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!filter.test(email)) {
            showNotyNotification("error", email + ' is not valid email address.');
            return false;
        }

        return mappingOptions.checkDuplicatedEmailsNew(email);
    },
    checkDuplicatedEmails:function (email) {
        var _val = [email],_rtn=true;
        $(".notifiedMails .emailParent span").each(function(){
            _val.push($(this).text().trim());
        });
        if(_val.length !== new Set(_val).size){
            _rtn = false;
            showNotyNotification("error",email+ " is already entered.");
            $("textarea").val('');

        }
        return _rtn;
    },
    checkDuplicatedEmailsNew:function (email) {
        var _val = [email],_rtn=true;
        $(".notifiedMailsNew .emailParent span").each(function(){
            _val.push($(this).text().trim());
        });
        if(_val.length !== new Set(_val).size){
            _rtn = false;
            showNotyNotification("error",email+ " is already entered.");
            $("textarea").val('');

        }
        return _rtn;
    },
    checkfileType:function (_excFiles) {
        var filter = /^([a-zA-Z0-9])+$/;
        if (!filter.test(_excFiles)) {
            showNotyNotification("error", _excFiles + ' is not valid file type.');
            return false;
        }

        return mappingOptions.checkDuplicatedfileType(_excFiles);
    },
    checkDuplicatedfileType:function (_excFiles) {
        var _fileValue = [_excFiles],_rtn=true;
        $(".notifiedFileTypes .emailParent span").each(function(){
            _fileValue.push($(this).text().trim());
        });
        if(_fileValue.length !== new Set(_fileValue).size){
            _rtn = false;
            showNotyNotification("error",_excFiles+ " is already entered.");
            $("#fileTypeExc").val('');

        }
        return _rtn;
    },
    getDateConversion:function (timestamp) {
        var _ago = Date.now() - timestamp;
        if( _ago < 86400000)
            return jQuery.timeago(timestamp);
        else
            return CFManageCloudAccountsAjaxCall.getDateConversion(timestamp);
    },
    localStorageOperation : function (value , op) {
        //"fromCloudId"."id"   "toCloudId" ."id"
        var _storage = JSON.parse(localStorage.getItem('selectedMappings')),_rtnValue, _len ;
        var _storageEmail = JSON.parse(localStorage.getItem('selectedEmail')),_manuVal, len ;
        if(_storage != null)
            _len = _storage.length;
        else
        {
            _len = 0;
            _storage = [];
        }
        if(_storageEmail != null)
            len = _storageEmail.length;
        else
        {
            len = 0;
            _storageEmail = [];
        }



        var  operations ={
            checkExists :function () {
                _rtnValue = false;
                for(var i=0; i<_len; i++){
                    if(_storage[i].fromCloudId.id == value.fromCloudId.id  && _storage[i].toCloudId.id == value.toCloudId.id)
                        _rtnValue = true;
                }
            },
            deleteValue : function () {
                var  deletedArray = _storage.filter(function(el) {
                    return el.fromCloudId.id !== value.fromCloudId.id;
                });
                localStorage.setItem('selectedMappings',JSON.stringify(deletedArray));
                _rtnValue = true;
            },
            deleteEmailValue : function () {
                var  deletedArray = _storageEmail.filter(function(el) {
                    return el.fromMailId !== value.fromMailId;
                });
                localStorage.setItem('selectedEmail',JSON.stringify(deletedArray));
                _manuVal = true;
            },

            //
            setValue : function () {
                _storage.push(value);
                localStorage.setItem('selectedMappings',JSON.stringify(_storage));
                _rtnValue = true;
            },
            setEmailVal : function () {
                _storageEmail.push(value);
                localStorage.setItem('selectedEmail',JSON.stringify(_storageEmail));
                _manuVal = true;
            },
            getValue :function () {
                _rtnValue = _storage;
            },
            remove   : function () {
                localStorage.removeItem('selectedMappings');
                localStorage.removeItem('selectedEmail');
                $('input[name="inputMapdUrs"]').removeAttr('checked');
                _rtnValue = true;
            },
            remove_1   : function () {
                localStorage.removeItem('selectedMappings');
                localStorage.removeItem('selectedEmail');
                _rtnValue = true;
            }
        };
        switch(op){

            case 'check'  : operations.checkExists();
                break;
            case 'delete' : operations.deleteValue();
                break;
            case 'delete1' : operations.deleteEmailValue();
                break;
            case 'set'    : operations.setValue();
                break;
            case 'set1'    : operations.setEmailVal();
                break;
            case 'get'    : operations.getValue();
                break;
            case 'rmv'    : operations.remove();
                break;
            case 'rmv1'    : operations.remove_1();
                break;
        }

        return _rtnValue;
        return  _manuVal;
    },
    folderOperation : function (value , op) {
        //"fromCloudId"."id"   "toCloudId" ."id"
        var _storage = JSON.parse(localStorage.getItem('folderMappings')),_rtnValue, _len ;
        var _storageEmail = JSON.parse(localStorage.getItem('folderEmail')),_manuVal, len ;
        if(_storage != null)
            _len = _storage.length;
        else
        {
            _len = 0;
            _storage = [];
        }
        if(_storageEmail != null)
            len = _storageEmail.length;
        else
        {
            len = 0;
            _storageEmail = [];
        }



        var  operations ={
            checkExists :function () {
                _rtnValue = false;
                for(var i=0; i<_len; i++){
                    if(_storage[i].fromCloudId.id === value.fromCloudId.id  && _storage[i].toCloudId.id === value.toCloudId.id){
                        if(_storage[i].fromRootId === value.fromRootId  && _storage[i].toRootId === value.toRootId)
                            _rtnValue = true;
                    }
                }
            },
            deleteValue : function () {
                var  deletedArray = _storage.filter(function(el) {
                    return  el.fromRootId !== value.fromRootId || el.toRootId !== value.toRootId;

                });
                localStorage.setItem('folderMappings',JSON.stringify(deletedArray));
                _rtnValue = true;
            },
            deleteEmailValue : function () {
                var  deletedArray = _storageEmail.filter(function(el) {
                    return el.fromMailId !== value.fromMailId;
                });
                localStorage.setItem('folderEmail',JSON.stringify(deletedArray));
                _manuVal = true;
            },

            //
            setValue : function () {
                _storage.push(value);
                localStorage.setItem('folderMappings',JSON.stringify(_storage));
                _rtnValue = true;
            },
            setEmailVal : function () {
                _storageEmail.push(value);
                localStorage.setItem('folderEmail',JSON.stringify(_storageEmail));
                _manuVal = true;
            },
            getValue :function () {
                _rtnValue = _storage;
            },
            remove   : function () {
                localStorage.removeItem('folderMappings');
                localStorage.removeItem('folderEmail');
                $('input[name="inputMapdUrs"]').removeAttr('checked');
                _rtnValue = true;
            },
            remove_1   : function () {
                localStorage.removeItem('folderMappings');
                localStorage.removeItem('folderEmail');
                _rtnValue = true;
            }
        };
        switch(op){

            case 'check'  : operations.checkExists();
                break;
            case 'delete' : operations.deleteValue();
                break;
            case 'delete1' : operations.deleteEmailValue();
                break;
            case 'set'    : operations.setValue();
                break;
            case 'set1'    : operations.setEmailVal();
                break;
            case 'get'    : operations.getValue();
                break;
            case 'rmv'    : operations.remove();
                break;
            case 'rmv1'    : operations.remove_1();
                break;
        }

        return _rtnValue;
        return  _manuVal;
    },
    appendOptions:function(src,dst){
        /*   if(src == "SHAREPOINT_ONLINE_BUSINESS"){// || src == "NETWORK_FILESHARES"){ //commented on 24Dec2018 -Rathnakar
               $("#chkShrdCntnt").attr("disabled", true);
           }
           else{
               $("#chkShrdCntnt").attr("disabled", false);
           } */
        var _srcHtml='';
        $(".expandSourceOptionsHide").find("tr:gt(0)").remove();
        $(".expandDsteOptionsHide").find("tr:gt(0)").remove();
        $(".expndDstCldName").html("<b>Destination Cloud: </b>" + CLName[dst]);
        $(".expndSrcCldName").html("<b>Source Cloud: </b>" + CLName[src]);
        var $ODSC = $(".expndSrcCldName").parents("tr");
        $($ODSC).show();
        $(".expandSourceOptionsHide").show();
        if(src === "G_SUITE"){
            if(dst === "G_SUITE"){
                _srcHtml = '<tr><td style="display: flex;width: 100%;"><input type="checkbox" id="fsnTbls"><div style="margin-top: 2px;margin-left: 5px;">Migrate FusionTables</div> </td></tr>';
                _srcHtml = _srcHtml + '<tr><td style="display: flex;width: 100%;"><input type="checkbox" id="gsteDrawings"><div style="margin-top: 2px;margin-left: 5px;">Migrate Drawings</div> </td></tr>';
            }

        }
        else if(src === "ONEDRIVE_BUSINESS_ADMIN"){
            $($ODSC).hide();
            $(".expandSourceOptionsHide").hide();
        }
        // else if(src === "BOX_BUSINESS"){
        //     _srcHtml = '<tr><td style="display: flex;width: 100%;"><input type="checkbox" id="grpFldrs"><div style="margin-top: 2px;margin-left: 5px;">Migrate Group Folders</div> </td></tr>';
        // }
        // else if(src === "DROPBOX_BUSINESS"){
        //     _srcHtml = '<tr><td style="display: flex;width: 100%;"><input type="checkbox" id="tmFldrs"><div style="margin-top: 2px;margin-left: 5px;">Migrate TeamFolders</div> </td></tr>';
        // }
        $(".expandSourceOptionsHide table tbody").append(_srcHtml);
        /*    if(src == "EGNYTE_ADMIN" && dst == "ONEDRIVE_BUSINESS_ADMIN") {
                $(".expandSourceOptionsHide").hide();
                $(".expandDsteOptionsHide").hide();

            } */
        if(src == "DROPBOX_BUSINESS" && dst == "ONEDRIVE_BUSINESS_ADMIN") {
            $($ODSC).hide();
            $(".expandSourceOptionsHide").hide();
        }
        // $(".expandSourceOptionsHide").css("display","none");
        // $(".expandDsteOptionsHide").css("display","none");
        /*    if(src == "NETWORK_FILESHARES" && dst == "ONEDRIVE_BUSINESS_ADMIN") {
                $(".expandDsteOptionsHide").hide();
               // $(".expndSrcCldName").hide();
                $(".destinationPath").css("display","none");
               // $(".expndDstCldName").parent().parent().hide();// hiding tr of the div

                // $("#jobType_dropDown option[id='delta']").remove();
                $("#jobType_dropDown option[id='onewaySync']").remove();
                $("#jobType_dropDown option[id='twowaySync']").remove();
            } */
    },
    appendUserPairs:function () {
        //var _userData = JSON.parse(localStorage.getItem("selectedMappings"));
        var _html = '<tr><td><img src=" ../img/drive/circle/ONEDRIVE_BUSINESS_ADMIN.png" alt="pic" class="forImage">Melvin@cloudfuze.co</td><td><img src=" ../img/drive/circle/ONEDRIVE_BUSINESS_ADMIN.png" alt="pic" class="forImage">Tania@cloudfuze.co</td></tr>';
        //<td style="width: 26%"><div class="arrowHeader"><div></div><a class="line-arrow square right"></a></div></td>

        for(var i=0;i<10;i++)
            $("#appendSelectedPairs").append(_html);
    }
}

var migrationMapping = {
    getDomainsList: function (srcAdminId, dstAdminId, pageNumber, pageSize, target) {
        //if($("#CFShowLoading").attr("autoMap") == "false")

        var _cld,_cldId;
        if (pageNumber == undefined)
            pageNumber = 1;
        if (pageSize == undefined)
            pageSize = 10;
        if (target == 'source'){
            _cld = 'sourceCloudId';
            _cldId = srcAdminId;
        }
        else{
            _cld = 'destCloudId';
            _cldId = dstAdminId;
        }

        $("#CFShowLoading").css("display", "");
        $.ajax({
            type: "GET",
            url: apicallurl + "/mapping/user/domain/list/" + _cld + "?srcCloudId=" + srcAdminId + "&destCloudId=" + dstAdminId + "&pageNo=" + pageNumber + "&pageSize=" + pageSize,
            //async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                if(data.length !== 0 && data[0].cloudDetail.length !== 0){
                    if(target === 'source')
                        sessionStorage.setItem('source',data[0].cloudDetail[0].name);
                    else
                        sessionStorage.setItem('destination',data[0].cloudDetail[0].name);
                }
                if(data.length !== 0 && data[0].cloudDetail.length === 0) {
                    setTimeout(function () {
                        if($("#CFShowLoading").attr("autoMap") === "false")
                            $("#CFShowLoading").css("display", "none");
                    }, 2000);
                }
                migrationMapping.appendDomains(data, pageSize, target,'',pageNumber);
                //migrationMapping.appendSlctdCloudImg( _cldId ,target);
                var srcName = sessionStorage.getItem('source');
                var dstnName = sessionStorage.getItem('destination');
                if(srcName !== null && dstnName !== null)
                    setTimeout(function () {
                        if($("#CFShowLoading").attr("autoMap") === "false")
                            $("#CFShowLoading").css("display", "none");
                    }, 2000);
            }
        });


    },
    getSearchUser: function (srcAdminId, dstAdminId, pageNumber, pageSize, target, searchUser) {
        if (!searchUser.length) {
            migrationMapping.getDomainsList(srcAdminId, dstAdminId, pageNumber, pageSize, target);
            return false;
        }
        var _cld;
        if (pageNumber == undefined)
            pageNumber = 1;

        if (pageSize == undefined)
            pageSize = 10;

        if (target == 'source')
            _cld = 'sourceCloudId';
        else
            _cld = 'destCloudId';

        $("#CFShowLoading").css("display", "");
        $.ajax({
            type: "GET",
            url: apicallurl + "/mapping/user/searchUsers/list/" + _cld + "?srcCloudId=" + srcAdminId + "&destCloudId=" + dstAdminId + "&searchCloudUser=" + searchUser + "&pageNo=" + pageNumber + "&pageSize=" + pageSize,
            //async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (response) {
                var _dataLength = response.length;
                if(_dataLength === 0){
                    migrationMapping.getDomainsList(srcAdminId, dstAdminId, pageNumber, pageSize, target);
                    showNotyNotification("error","No Results Found");
                    return false;
                }
                else{
                    var _cldEmails= [];
                    for(var i=0; i<_dataLength;i++){
                        var _cldDetailsLength = response[i].cloudDetail.length;
                        for(var j=0; j<_cldDetailsLength;j++){
                            _cldEmails.push(response[i].cloudDetail[j].emailId);

                        }
                    }


                    migrationMapping.appendDomains(response, pageSize, target, 'searchAppend',pageNumber);

                }
                setTimeout(function () {
                    $("#CFShowLoading").css("display", "none");
                }, 2000);
            }
        });


    },
    getPgntdUsrsData: function (domain, srcAdminId, dstAdminId, pageNo, pageSize, target, usrsRmv,keyword) {
        var searchData = JSON.parse(localStorage.getItem("searchData"));
        keyword = "";
        if (target == 'source'){
            _cld = 'sourceCloudId';
            if(searchData.source) {
                keyword = $("#srcUsrs .custom-search-input input").val().trim();
            }
        }
        else{
            _cld = 'destCloudId';
            if(searchData.destination) {
                keyword = $("#dstnUsrs .custom-search-input input").val().trim();
            }
        }
        $.ajax({
            type: "GET",
            url: apicallurl + "/mapping/user/users/list/" + _cld + "?srcAdminCloudId=" + srcAdminId + "&destAdminCloudId=" + dstAdminId + "&domainName=" + encodeURIComponent(domain) + "&pageNo=" + pageNo + "&pageSize=" + pageSize + "&searchUser=" +keyword,
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (domainData) {
                var _usrAppend = '<div class="fldr_parent" style="border-bottom: 1px solid #f2f2f2;padding: 5px 0px"><p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><span class="fa fa-plus usr-folder" useremail="emailId" folder-id="root" user-id="cldId" cloudname="cloudName" aria-hidden="true" style="cursor:pointer;"></span><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span style="font-size: initial">&nbsp;&nbsp;userName</span></p></div>';
                var _srcCldName = localStorage.getItem('multiUsrSrcCldName');
                var _dstnCldName = localStorage.getItem('multiUsrDstnCldName');
                var _usrsLength = domainData.cloudDetail.length;
                var _usrDataAppend = '';
                var _usrData = '';
                for (var j = 0; j < _usrsLength; j++) {
                    if((_srcCldName === "ONEDRIVE_BUSINESS_ADMIN" && _dstnCldName === "DROPBOX_BUSINESS") || (_srcCldName === "DROPBOX_BUSINESS" && _dstnCldName === "ONEDRIVE_BUSINESS_ADMIN") ){
                        if (target == "source"){
                            if (_srcCldName === "ONEDRIVE_BUSINESS_ADMIN" && domainData.cloudDetail[j].rootFolderId === "/") {
                                _usrAppend = '<div class="fldr_parent" pgno="1" style="border-bottom: 1' +
                                    'px solid #f2f2f2;padding: 5px 0px" title="User authentication failed"> <p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><i class="fa fa-angle-down usr-folder11" folder-id="root" cloudname="cloudName" user-id="cldId" useremail="emailId" aria-hidden="true" style="color: #ddd;cursor:pointer;font-size: 24px!important;padding-bottom: 2%;margin-left: -13%;"></i><div style="margin-left: 31px;margin-top: -13%;"><input type="checkbox"  name="srccheckBox"  class="sourceChkBox" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName" disabled></div><div style="margin-left: 19%;margin-top: -7%;"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span id="textSpan1" style="font-size:initial;" title="'+domainData.cloudDetail[j].emailId+'">&nbsp;&nbsp;userName</span>' +
                                    '</div></p><span id="folderLoading" style="position: absolute;left: 10%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" >Loading...</span> </div>';

                            } else {
                                var _obj ={
                                    "id":domainData.cloudDetail[j].rootFolderId,
                                    "cloudId":domainData.cloudDetail[j].id,
                                };
                                if(fldrStorage(_obj,'check1')){
                                    var input = '<input type="checkbox"   name="srccheckBox" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName" checked="checked">';
                                }
                                else{
                                    var input =  '<input type="checkbox"   name="srccheckBox" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName">';
                                }

                                _usrAppend = '<div class="fldr_parent" pgno="1" style="border-bottom: 1' +
                                    'px solid #f2f2f2;padding: 5px 0px"> <p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><i class="fa fa-angle-down usr-folder" folder-id="root" cloudname="cloudName" user-id="cldId" useremail="emailId" aria-hidden="true" style="cursor:pointer;font-size: 24px!important;padding-bottom: 2%;margin-left: -13%;"></i><div style="margin-left: 31px;margin-top: -13%;">'+input+'</div><div style="margin-left: 19%;margin-top: -7%;"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span id="textSpan1" style="font-size:initial;" title="'+domainData.cloudDetail[j].emailId+'">&nbsp;&nbsp;userName</span>' +
                                    '</div></p><span id="folderLoading" style="position: absolute;left: 10%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" >Loading...</span></div>';

                            }
                        }
                        else if (target == "destination"){
                            if (_dstnCldName === "ONEDRIVE_BUSINESS_ADMIN" && domainData.cloudDetail[j].rootFolderId === "/") {
                                _usrAppend = '<div class="fldr_parent" pgno="1" style="border-bottom: 1' +
                                    'px solid #f2f2f2;padding: 5px 0px" title="User authentication failed"> <p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><i class="fa fa-angle-down usr-folder11" folder-id="root" cloudname="cloudName" user-id="cldId" useremail="emailId" aria-hidden="true" style="color: #ddd;cursor:pointer;font-size: 24px!important;padding-bottom: 2%;margin-left: -13%;"></i><div style="margin-left: 31px;margin-top: -13%;"><input type="radio"  class="dstnRadioBtn" name="folderradiobtn" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName" disabled></div><div style="margin-left: 19%;margin-top: -7%;"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span id="textSpan1" style="font-size:initial;" title="'+domainData.cloudDetail[j].emailId+'">&nbsp;&nbsp;userName</span>' +
                                    '</div></p><span id="folderLoading" style="position: absolute;left: 10%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" >Loading...</span> </div>';
                            } else {
                                _usrAppend = '<div class="fldr_parent" pgno="1" style="border-bottom: 1' +
                                    'px solid #f2f2f2;padding: 5px 0px"> <p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><i class="fa fa-angle-down usr-folder" folder-id="root" cloudname="cloudName" user-id="cldId" useremail="emailId" aria-hidden="true" style="cursor:pointer;font-size: 24px!important;padding-bottom: 2%;margin-left: -13%;"></i><div style="margin-left: 31px;margin-top: -13%;"><input type="radio" name="folderradiobtn" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName"></div><div style="margin-left: 19%;margin-top: -7%;"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span id="textSpan1" style="font-size:initial;" title="'+domainData.cloudDetail[j].emailId+'">&nbsp;&nbsp;userName</span>' +
                                    '</div></p><span id="folderLoading" style="position: absolute;left: 10%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" >Loading...</span></div>';

                            }
                        }

                    }
                    else{

                        _usrAppend = '<div class="" style="border-bottom: 1px solid #f2f2f2;padding: 5px 0px"> <span style="font-size: initial"><input type="radio" name="srcUsers" disabled="false" style="margin-left: 11%;margin-bottom: 2%" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-left: 2%;"></i>&nbsp;&nbsp;userName</span> </div>';
                    }


                    _usrDataAppend = _usrAppend;
                    var _emailVal = domainData.cloudDetail[j].cloudUserId.split("|")[1].split("@")[0];
                    if(_emailVal.length>=25){
                        _emailVal = CFManageCloudAccountsAjaxCall.getMaxChars(_emailVal, 23);
                    }
                    else{
                        _emailVal;
                    }
                    _usrDataAppend = _usrDataAppend.replace("userName", _emailVal).replace("emailId", domainData.cloudDetail[j].cloudUserId.split("|")[1]).replace("cldId", domainData.cloudDetail[j].id).replace("root", domainData.cloudDetail[j].rootFolderId).replace("cloudName", domainData.cloudDetail[j].name);
                    _usrDataAppend = _usrDataAppend.replace("emailId", domainData.cloudDetail[j].cloudUserId.split("|")[1]).replace("cldId", domainData.cloudDetail[j].id).replace("root", domainData.cloudDetail[j].rootFolderId).replace("cloudName", domainData.cloudDetail[j].name);

                    if (domainData.cloudDetail[j].flag){
                        _usrDataAppend = _usrDataAppend.replace('<div class=""', '<div class="selectedClass"').replace('input type="radio" name="srcUsers" disabled="false"', 'input type="radio" name="srcUsers" disabled="disabled"');
                        _usrDataAppend = _usrDataAppend.replace("emailId", domainData.cloudDetail[j].cloudUserId.split("|")[1]).replace("cldId", domainData.cloudDetail[j].id).replace("root", domainData.cloudDetail[j].rootFolderId).replace("cloudName", domainData.cloudDetail[j].name);

                    }
                    else
                        _usrDataAppend = _usrDataAppend.replace('input type="radio" name="srcUsers" disabled="false"', 'input type="radio" name="srcUsers"');

                    if (target != 'source')
                        _usrDataAppend = _usrDataAppend.replace('type="radio" name="srcUsers"', 'type="radio" name="dstUsers"');

                    _usrData = _usrData + _usrDataAppend;
                }
                // var _usrsRmv= usrsRmv.split("#")[1].split(" ")[0];
                // usrsRmv = $(document.getElementById(_usrsRmv).getElementsByClassName(domain));
                // usrsRmv = $(usrsRmv[1]);

                $(usrsRmv).find("div").each(function () {
                    if ($(this).hasClass("pagination"))
                        return false;
                    else
                        $(this).remove();
                });
                $(usrsRmv).find(".pagination.p1").before(_usrData);
                var _num = $(usrsRmv).find(".pagination.p1 span").last().text().match(/\d+$/)[0];
                $(usrsRmv).find(".pagination.p1 span").last().text( pageNo + " of " + _num);
                $(usrsRmv).find(".pagination.p1 input").val(pageNo);
                $(usrsRmv).css("display","block");
                $(usrsRmv).siblings(".forDomainName").css("display","none");


            }
        });
    },
    appendDomains: function (domainListData, pageSize, appendTarget, check,pageNumber) {
        if(pageNumber == undefined)
            pageNumber = 1;

        var _domainLength = domainListData.length;
        if(_domainLength === 0){
            if(appendTarget === 'source'){
                $("#srcUsrs .paginationCF input.input-sm").val(0);
                $("#srcUsrs .paginationCF span").last().text("0 of 0");
                $("#srcUsrs .paginationDiv").addClass("disabled").css("opacity","0.6");
                $("#srcCloudUsers .message-widget").html('');
                $('#srcUsrs .paginationDiv select').prop('disabled', true);
                $('#srcUsrs .paginationDiv input').prop('disabled', true);

            }
            else{
                $("#dstnUsrs .paginationCF input.input-sm").val(0);
                $("#dstnUsrs .paginationCF span").last().text("0 of 0");
                $("#dstnUsrs .paginationDiv").addClass("disabled").css("opacity","0.6");
                $("#dstCloudsUsers .message-widget").html('');
                $('#dstnUsrs .paginationDiv select').prop('disabled', true);
                $('#dstnUsrs .paginationDiv input').prop('disabled', true);
            }
            return false;
        }
        else{
            if (appendTarget === 'source') {
                $("#srcUsrs .paginationDiv").removeClass("disabled").css("opacity", "1");
                $('#srcUsrs .paginationDiv select').prop('disabled', false);
                $('#srcUsrs .paginationDiv input').prop('disabled', false);
            } else {
                $("#dstnUsrs .paginationDiv").removeClass("disabled").css("opacity", "1");
                $('#dstnUsrs .paginationDiv select').prop('disabled', false);
                $('#dstnUsrs .paginationDiv input').prop('disabled', false);
            }

        }

        if (domainListData.length != 0)
            _domainLength = domainListData[0].noOfDomainsPresent;
        var _className;
        if(appendTarget == "source")
            _className = $("#srcUsrs .forDomainNameMin:visible").attr("class");
        else
            _className = $("#dstnUsrs .forDomainNameMin:visible").attr("class");

        var _pgCnt = Math.ceil(_domainLength / pageSize),
            _HTML = '<div class="className"> <div><div class="forDomainName DomainNameAttr" style="display: block"><i class="fa fa-plus" aria-hidden="true" style="padding: 12px;cursor: pointer"></i><span style="font-size: initial;">domainName</span></div>';
        if (check === 'searchAppend') {
            _domainLength = domainListData.length;
            _pgCnt = Math.ceil(_domainLength / pageSize);
        }

        var _valDomain, _len = 0;
        //var _txt = ((pageSize -1) * parseInt($("#srcUsrs select").val()) + 1)  + " of " + _domainLength
        var _size = 0;
        if (domainListData.length != 0)
            _size  = Math.ceil(domainListData[0].noOfDomainsPresent/$("#srcUsrs select").val());

        var _txt = pageNumber + " of " + _size;
        if (appendTarget == "source") {
            // var _txt = Math.ceil(pageSize/$("#srcUsrs select").val());
            //
            // if(_domainLength < pageSize){
            //     _txt = _txt + " - " + _domainLength;
            $("#srcUsrs .paginationCF input.input-sm").val(pageNumber);
            // }
            //
            // else{
            //     _txt = _txt + " - " + pageSize;
            //     $("#srcUsrs .paginationCF input.input-sm").val((_txt - 1) * pageSize + 1);
            // }

            //$("#srcUsrs .paginationCF span").last().text(  _txt + " of " + _domainLength);
            $("#srcUsrs .paginationCF span").last().text(  _txt );
            _len = $("#srcUsrs .custom-search-input input").val().length;
            //$("#srcUsrs .custom-search-input input").val(pageNumber);
            // _valDomain = $("#srcCloudUsers  .forDomainNameMin:visible .fa-minus").next().text();
            _valDomain = $("#srcCloudUsers  .forDomainNameMin:visible .fa-minus").next().text();
            $("#srcCloudUsers .message-widget").html('');
            if(_domainLength <10 || _domainLength == 10){
                $("#srcUsrs .paginationDiv").addClass("disabled").css("opacity","0.6");
                $('#srcUsrs .paginationDiv select').prop('disabled', true);
                $('#srcUsrs .paginationDiv input').prop('disabled', true);
            }
        }
        else {
            // var _txt = Math.ceil(pageSize/$("#dstnUsrs select").val());
            //
            // if(_domainLength < pageSize){
            //     _txt = _txt + " - " + _domainLength;
            $("#dstnUsrs .paginationCF input.input-sm").val(pageNumber);
            // }
            //
            // else{
            //     _txt = _txt + " - " + pageSize;
            //     $("#dstnUsrs .paginationCF input.input-sm").val((_txt - 1) * pageSize + 1);
            // }
            //
            // $("#dstnUsrs .paginationCF span").last().text( _txt  + " of " + _domainLength);
            var _size  = Math.ceil(domainListData[0].noOfDomainsPresent/$("#dstnUsrs select").val());
            var _txt = pageNumber + " of " + _size;
            $("#dstnUsrs .paginationCF span").last().text( _txt );
            _len = $("#dstnUsrs .custom-search-input input").val().length;
            _valDomain = $("#dstCloudsUsers  .forDomainNameMin:visible .fa-minus").next().text();
            $("#dstCloudsUsers .message-widget").html('');
            if(_domainLength <10 || _domainLength == 10){
                $("#dstnUsrs .paginationDiv").addClass("disabled").css("opacity", "0.6");
                $('#dstnUsrs .paginationDiv select').prop('disabled', true);
                $('#dstnUsrs .paginationDiv input').prop('disabled', true);
            }
        }


        for (var i = 0; i < domainListData.length; i++) {//_domainLength
            var domainData = domainListData[i], _html = _HTML;
            console.log(domainData);
            //var _pgntn = '<div class=paginationDiv><div class=show><label>Show rows:</label><select class="form-control input-sm"><option>30<option>50<option>100</select></div><div class=paginationCF style=display:flex><div style=width:140px><span style=padding:2px>Go to:</span> <input class="form-control input-sm"> <span style=margin-top:4%>28-37 of 138</span></div><i class="fa fa-chevron-left"></i> <i class="fa fa-chevron-right"></i></div></div>';
            var _pgntn = '<div class=paginationDiv><div class=paginationCF style=display:flex><div style=width:175px><span style=padding:4px>Go to:</span> <input class="form-control input-sm" value="1"> <span style=margin-top:4%>1 of ' + Math.ceil(domainData.noOfCloudsPreesent/10) + '</span></div><i class="fa fa-chevron-left" style="font-size: 12px!important;"></i> <i class="fa fa-chevron-right" style="font-size: 12px!important;"></i></div></div>';
            _html = _html.replace("domainName", CFManageCloudAccountsAjaxCall.getMaxChars(domainData.domainName,23)).replace("DomainNameAttr", domainData.domainName);
            _html = _html + '<div class="forDomainNameMin DomainNameAttr" style="display: none"><i class="fa fa-minus" aria-hidden="true" style="padding: 12px;cursor: pointer"></i><span style="font-size: initial;">domainName</span> usersList<div class="pagination p1" style="padding: 0;margin: 0;border-top: 1px solid #ccc;border-radius: 0;width: 100%;margin-top: 5%;">' + _pgntn + '</div></div></div></div>';      //<ul class="pagination-sm pagination appendUsrPagination"></ul>
            _html = _html.replace("domainName", CFManageCloudAccountsAjaxCall.getMaxChars(domainData.domainName,23)).replace("DomainNameAttr", domainData.domainName);
            var _usrAppend1;
            var _usrsLength = domainData.cloudDetail.length;
            var _usrDataAppend = '';
            var _srcCldName = localStorage.getItem('multiUsrSrcCldName');
            var _dstnCldName = localStorage.getItem('multiUsrDstnCldName');

            if (appendTarget == "source" && _len &&   check === 'searchAppend')
                _html = _html.replace('class="' + "forDomainName " + domainData.domainName + '" style="display: block"', 'class="' + "forDomainName " + domainData.domainName + '" style="display: none"').replace('<div class="' + "forDomainNameMin " + domainData.domainName + '" style="display: none">', '<div class="' + "forDomainNameMin " + domainData.domainName + '" style="display: block">');

            else if (_len && check === 'searchAppend')
                _html = _html.replace('class="' + "forDomainName " + domainData.domainName + '" style="display: block"', 'class="' + "forDomainName " + domainData.domainName + '" style="display: none"').replace('<div class="' + "forDomainNameMin " + domainData.domainName + '" style="display: none">', '<div class="' + "forDomainNameMin " + domainData.domainName + '" style="display: block">');

            for (var j = 0; j < _usrsLength; j++) {
                if ((_srcCldName === "ONEDRIVE_BUSINESS_ADMIN" && _dstnCldName === "DROPBOX_BUSINESS") || (_srcCldName === "DROPBOX_BUSINESS" && _dstnCldName === "ONEDRIVE_BUSINESS_ADMIN")) {
                    if (appendTarget == "source") {
                        if (_srcCldName === "ONEDRIVE_BUSINESS_ADMIN" && domainData.cloudDetail[j].rootFolderId === "/") {
                            _usrAppend1 = '<div class="fldr_parent" pgno="1" style="border-bottom: 1' +
                                'px solid #f2f2f2;padding: 5px 0px" title="User authentication failed"> <p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><i class="fa fa-angle-down usr-folder11" folder-id="root" cloudname="cloudName" user-id="cldId" useremail="emailId" aria-hidden="true" style="color: #ddd;cursor:pointer;font-size: 24px!important;padding-bottom: 2%;margin-left: -13%;"></i><div style="margin-left: 31px;margin-top: -13%;"><input type="checkbox"  name="srccheckBox"  class="sourceChkBox" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName" disabled></div><div style="margin-left: 19%;margin-top: -7%;"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span id="textSpan1" style="font-size:initial;" title="'+domainData.cloudDetail[j].emailId+'">&nbsp;&nbsp;userName</span>' +
                                '</div></p><span id="folderLoading" style="position: absolute;left: 10%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" >Loading...</span> </div>';

                        } else {
                            _usrAppend1 = '<div class="fldr_parent" pgno="1" style="border-bottom: 1' +
                                'px solid #f2f2f2;padding: 5px 0px"> <p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><i class="fa fa-angle-down usr-folder" folder-id="root" cloudname="cloudName" user-id="cldId" useremail="emailId" aria-hidden="true" style="cursor:pointer;font-size: 24px!important;padding-bottom: 2%;margin-left: -13%;"></i><div style="margin-left: 31px;margin-top: -13%;"><input type="checkbox"  name="srccheckBox"  class="sourceChkBox" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName"></div><div style="margin-left: 19%;margin-top: -7%;"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span id="textSpan1" style="font-size:initial;" title="'+domainData.cloudDetail[j].emailId+'">&nbsp;&nbsp;userName</span>\n' +
                                '</div></p> <span id="folderLoading" style="position: absolute;left: 10%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" >Loading...</span></div>';
                        }
                    } else if (appendTarget == "destination") {
                        if (_dstnCldName === "ONEDRIVE_BUSINESS_ADMIN" && domainData.cloudDetail[j].rootFolderId === "/") {
                            _usrAppend1 = '<div class="fldr_parent" pgno="1" style="border-bottom: 1' +
                                'px solid #f2f2f2;padding: 5px 0px" title="User authentication failed"> <p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><i class="fa fa-angle-down usr-folder11" folder-id="root" cloudname="cloudName" user-id="cldId" useremail="emailId" aria-hidden="true" style="color: #ddd;cursor:pointer;font-size: 24px!important;padding-bottom: 2%;margin-left: -13%;"></i><div style="margin-left: 31px;margin-top: -13%;"><input type="radio"  class="dstnRadioBtn" name="folderradiobtn" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName" disabled></div><div style="margin-left: 19%;margin-top: -7%;"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span id="textSpan1" style="font-size:initial;" title="'+domainData.cloudDetail[j].emailId+'">&nbsp;&nbsp;userName</span>' +
                                '</div></p><span id="folderLoading" style="position: absolute;left: 10%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" >Loading...</span> </div>';
                        } else {
                            _usrAppend1 = '<div class="fldr_parent" pgno="1" style="border-bottom: 1' +
                                'px solid #f2f2f2;padding: 5px 0px"> <p class="usr-data" style="padding: 5px 0px 5px 35px;margin-bottom:0;"><i class="fa fa-angle-down usr-folder" folder-id="root" cloudname="cloudName" user-id="cldId" useremail="emailId" aria-hidden="true" style="cursor:pointer;font-size: 24px!important;padding-bottom: 2%;margin-left: -13%;"></i><div style="margin-left: 31px;margin-top: -13%;"><input type="radio"  class="dstnRadioBtn" name="folderradiobtn" folderPath="/" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName"></div><div style="margin-left: 19%;margin-top: -7%;"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-right: -3px;cursor:default;pointer-events: none;margin-left:10px;"></i><span id="textSpan1" style="font-size:initial;" title="'+domainData.cloudDetail[j].emailId+'">&nbsp;&nbsp;userName</span>' +
                                '</div></p><span id="folderLoading" style="position: absolute;left: 10%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" >Loading...</span> </div>';

                        }
                    }

                }

                else{
                    _usrAppend1= '<div class="" style="border-bottom: 1' +
                        'px solid #f2f2f2;padding: 5px 0px"> <span style="font-size: initial"><input type="radio" name="srcUsers" disabled="false" style="margin-left: 11%;margin-bottom: 2%" usremail="emailId" cloudId="cldId" rtfolId="root" cldName="cloudName"><i class="fa fa-user" aria-hidden="true" style="font-size: large;margin-left: 2%;"></i>&nbsp;&nbsp;userName</span> </div>';
                    ;
                }


                var _usrAppend = _usrAppend1;
                var _emailVal = domainData.cloudDetail[j].cloudUserId.split("|")[1].split("@")[0];
                if(_emailVal.length>=25){
                    _emailVal = CFManageCloudAccountsAjaxCall.getMaxChars(_emailVal, 23);
                }
                else{
                    _emailVal;
                }
                _usrAppend = _usrAppend.replace("userName", _emailVal).replace("emailId", domainData.cloudDetail[j].cloudUserId.split("|")[1]).replace("cldId", domainData.cloudDetail[j].id).replace("root", domainData.cloudDetail[j].rootFolderId).replace("cloudName", domainData.cloudDetail[j].name);
                _usrAppend = _usrAppend.replace("emailId", domainData.cloudDetail[j].cloudUserId.split("|")[1]).replace("cldId", domainData.cloudDetail[j].id).replace("root", domainData.cloudDetail[j].rootFolderId).replace("cloudName", domainData.cloudDetail[j].name);


                if (domainData.cloudDetail[j].flag)
                    _usrAppend = _usrAppend.replace('<div class=""', '<div class="selectedClass"').replace('input type="radio" name="srcUsers" disabled="false"', 'input type="radio" name="srcUsers" disabled="disabled"');
                else
                    _usrAppend = _usrAppend.replace('input type="radio" name="srcUsers" disabled="false"', 'input type="radio" name="srcUsers"');


                if (appendTarget != "source")
                    _usrAppend = _usrAppend.replace("className", "dstCloudsUsers").replace("srcUsers", "dstUsers");
                else
                    _usrAppend = _usrAppend.replace("className", "srcCloudUsers");

                _usrDataAppend = _usrDataAppend + _usrAppend;
            }
            _html = _html.replace("usersList", _usrDataAppend);


            if (appendTarget == "source"){
                $("#srcCloudUsers .message-widget").append(_html);
            }

            else if (appendTarget == "destination"){
                $("#dstCloudsUsers .message-widget").append(_html);
            }

            migrationMapping.apndUsrPagination(domainData.noOfCloudsPreesent, appendTarget, i);
        }
        if(_className != undefined && _className.length){
            _className = _className.split("forDomainNameMin ")[1];
            if (appendTarget == "source"){
                //var _parent = $(document.getElementById("srcUsrs").getElementsByClassName(_className));
                $(".ui-autocomplete").css("display","none");
                // $(_parent[1]).css("display","block");
                // $(_parent[0]).css("display","none");
                // $("#srcCloudUsers .forDomainNameMin." + _className).css("display","block");
                // $("#srcCloudUsers .forDomainName." + _className).css("display","none");
            }

            else if (appendTarget == "destination"){
                // $("#dstCloudsUsers .forDomainNameMin." + _className).css("display","block");
                // $("#dstCloudsUsers .forDomainName." + _className).css("display","none");
                //var _parent = $(document.getElementById("dstUsrs").getElementsByClassName(_className));
                $(".ui-autocomplete").css("display","none");
                //$(_parent[1]).css("display","block");
                //$(_parent[0]).css("display","none");

            }
        }

    },
    apndUsrPagination: function (usrsCount, target, i) {
        var $sel;
        if (target == "source")
            $sel = "#srcCloudUsers .appendUsrPagination:eq(" + i + ")";
        else
            $sel = "#dstCloudsUsers .appendUsrPagination:eq(" + i + ")";

        // $($sel).twbsPagination({
        //     totalPages: Math.ceil(usrsCount / 10),
        //     visiblePages: 3,
        //     next: '<i class="fa fa-angle-right" style="font-size: 19px;"></i>',
        //     prev: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i>',
        //     first: '<i class="fa fa-angle-left"  style="font-size: 19px;"></i><i class="fa fa-angle-left" style="font-size: 19px;"></i>',
        //     last: '<i class="fa fa-angle-right" style="font-size: 19px;"></i><i class="fa fa-angle-right" style="font-size: 19px;"></i>',
        //     onPageClick: function (event, page) {
        //         var $rmvUsrs, _domain;
        //         if (target == "source") {
        //             //$rmvUsrs = "#srcCloudUsers .forDomainNameMin:eq(" + i + ")";
        //             $rmvUsrs = "#srcCloudUsers .forDomainNameMin[class*='" + _class +"']";
        //             //_domain = $(".forDomainNameMin:eq(" + i + ") i").next("span").text();
        //             _domain = $("#srcUsrs .forDomainNameMin:eq(" + i + ")").attr('class').split(" ")[1];
        //         }
        //         else {
        //             //$rmvUsrs = "#dstCloudsUsers .forDomainNameMin:eq(" + i + ")";
        //             $rmvUsrs = "#dstCloudsUsers .forDomainNameMin[class*='" + _class +"']";
        //             //_domain = $(".forDomainNameMin:eq(" + i + ") i").next("span").text();
        //             _domain = $("#dstnUsrs .forDomainNameMin:eq(" + i + ")").attr('class').split(" ")[1];
        //         }
        //         migrationMapping.getPgntdUsrsData(_domain, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), page, 10, target, $rmvUsrs);
        //     }
        // });
    },
    appendMapping: function (pageNo,srccldname,dstncldname) {
        if(pageNo == 0)
            pageNo = 1;

        var _val = $("#mapdUsrs .paginationDiv select").val(), _rtn = pageNo;
        var pageSize =  _val;


        $.ajax({
            type: "GET",
            url: apicallurl + "/mapping/user/cache/list?sourceCloudId=" + localStorage.getItem("multiUsrSrcCldId") + "&destCloudId=" + localStorage.getItem("multiUsrDstnCldId") + "&pageNo=" + pageNo + "&pageSize="+pageSize,
            async: false,
            headers: {
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            },
            success: function (data) {
                $("#mapdUsers tbody").html('');
                $("#chkInput").removeAttr("checked");
                var _len = data.length;
                if (_len == 0 && pageNo == 1) {
                    var _txt = "0 of 0";
                    $("#mapdUsrs .paginationDiv").css("opacity", "0.6");
                    $('#mapdUsrs .paginationDiv select').prop('disabled', true);
                    $('#mapdUsrs .paginationDiv input').prop('disabled', true);

                    $("#mapdUsrs .paginationCF input").val(0);
                    $("#mapdUsrs .paginationCF span").last().text(_txt);
                }
                if (_len == 0) {
                    $(document).find("#chkInput").attr("disabled", true).prop("checked", false);
                    if (pageNo != 1)
                        $("#CFShowLoading").css("display", "none");
                    $('.download').css('pointer-events', 'none');
                    $('.download').css('opacity', '0.5');
                    $('#help').css('visibility', 'hidden');

                    $("#clearBtn").css("pointer-events", 'none');
                    $("#clearBtn").css("cursor", "not-allowed");
                    $("#clearBtn").css("opacity", "0.6");
                    return false;
                }
                for (var i = 0; i < _len; i++) {
                    var _dataVal = data[i];
                    var csvID = _dataVal.csvId;
                    var csvName = _dataVal.csvName;
                    var migrateDstnFolderName = _dataVal.migrateFolderName;
                    var _srcUsrDetails = {
                        "userEmail": _dataVal.sourceCloudDetails.emailId,
                        "userCloudName": _dataVal.sourceCloudDetails.name,
                        "userCloudId": _dataVal.sourceCloudDetails.id,
                        "userRtFolId": _dataVal.sourceCloudDetails.rootFolderId,
                        "folderPath": _dataVal.sourceCloudDetails.folderPath,
                        "srcPathRootFolderId": _dataVal.sourceCloudDetails.pathRootFolderId,
                        "migrateSrcFolderName": _dataVal.sourceCloudDetails.migrateFolderName

                    }
                    var _dstnUsrDetails = {
                        "dstnUserEmail": _dataVal.destCloudDetails.emailId,
                        "dstnUserCloudName": _dataVal.destCloudDetails.name,
                        "dstnUserCloudId": _dataVal.destCloudDetails.id,
                        "dstnUserRtFolId": _dataVal.destCloudDetails.rootFolderId,
                        "dstnFolderPath": _dataVal.destCloudDetails.folderPath,
                        "dstnPathRootFolderId": _dataVal.destCloudDetails.pathRootFolderId
                    }
                    if ((_srcUsrDetails.userCloudName === "ONEDRIVE_BUSINESS_ADMIN" && _dstnUsrDetails.dstnUserCloudName === "DROPBOX_BUSINESS") || (_srcUsrDetails.userCloudName === "DROPBOX_BUSINESS" && _dstnUsrDetails.dstnUserCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
                        if (_srcUsrDetails.folderPath === null && _dstnUsrDetails.dstnFolderPath === null) {
                            _srcUsrDetails.folderPath = '/';
                            _dstnUsrDetails.dstnFolderPath = '/';
                        }
                        var isFolder = true;
                        mapdSlectedUrs(_srcUsrDetails, _dstnUsrDetails, csvID, csvName, migrateDstnFolderName, undefined, isFolder, '', _dataVal.duplicateCache, _dataVal.pathException);
                        if ($('#mapdUsers input[name= "inputMapdUrs"]').length > 0) {
                            var checkedLength = $('#mapdUsers input[name= "inputMapdUrs"]:checked').length + $('#mapdUsers input[name="folderMapngCheckBox"]:checked').length;
                            var totalLength = $('#mapdUsers input[name="inputMapdUrs"]').length + $('#mapdUsers input[name="folderMapngCheckBox"]').length;
                        } else {
                            var checkedLength = $('#mapdUsers input[name= "folderMapngCheckBox"]:checked').length;
                            var totalLength = $('#mapdUsers input[name="folderMapngCheckBox"]').length;
                        }
                        if (checkedLength == totalLength) {
                            $('#chkInput').prop('checked', true);

                        } else {
                            $('#chkInput').prop('checked', false);
                        }

                    } else {
                        $("input[cloudid=" + _srcUsrDetails.userCloudId + "]").attr('disabled', true).parent().parent().addClass("selectedClass");
                        $("input[cloudid=" + _dstnUsrDetails.dstnUserCloudId + "]").attr('disabled', true).parent().parent().addClass("selectedClass");
                        $('.download').css('pointer-events', 'none');
                        $('.download').css('opacity', '0.5');
                        $('#help').css('visibility', 'hidden');
                        mapdSlectedUrs(_srcUsrDetails, _dstnUsrDetails, csvID, csvName, migrateDstnFolderName);
                    }
                }
                if (pageNo == 1) {
                    $('#paginationMapng').empty();
                    $('#paginationMapng').removeData("twbs-pagination");
                    $('#paginationMapng').unbind("page");

                    $("#mapdUsrs .paginationDiv input").val(1);

                    var _selInptVal = $('#mapdUsrs select').val();
                    var _totPages = Math.ceil(data[0].noOfMappedCloudPressent / _selInptVal);

                    var _txt = pageNo + " of " + _totPages;

                    $("#mapdUsrs .paginationCF span").last().text(_txt);
                    $("#mapdUsrs .paginationCF input").val(pageNo);

                    $("#mapdUsrs .paginationCF span").last().text(_txt);
                    if ($("#mapdUsers tbody tr input[name=inputMapdUrs]:checked").length == 30) {
                        $("#chkInput").prop('checked', true);
                    }
                    setTimeout(function () {
                        if ($("#CFShowLoading").attr("autoMap") != "false")
                            $("#CFShowLoading").css("display", "none");
                    }, 2000);
                }
            }
        });
    },
    appendSlctdCloudImg:function(cloudId,target){
        var _len = AllCloudsInfo.length,
            _html = '<div class="showCloud"><img alt="Cloud" src="../img/drive/circle/showCloudImg.png"><div>showCloudName</div></div>'
        for(var i =0; i<_len; i++){
            var _cloud = AllCloudsInfo[i];
            if( cloudId == _cloud.id){
                _html = _html.replace('showCloudImg',_cloud.cloudName).replace('showCloudName',_cloud.userDisplayName);
                if(target == 'source')
                    $("#srcCloudUsers .forShow").append(_html);
                else
                    $("#dstnUsrs .forShow").append(_html);
            }
        }
    },
    autoComplete : function (target,keyword) {
        var _cldEmails = [],_trgt = '#mapdUsrs';
        if(target == "mapping") {
            _cldEmails = migrationMapping.getEnrtdMpdUsrSearchList(keyword);
        }
        //_cldEmails = migrationMapping.getMpdUsrSearchList(keyword);
        else{
            _cldEmails = migrationMapping.getEntrdSearchuserList(target,keyword);
            //_cldEmails = migrationMapping.getSearchuserList(target,keyword);getEntrdSearchuserList

            if(target == 'source')
                _trgt = '#srcUsrs';
            else
                _trgt = '#dstnUsrs';
        }

        $(_trgt +" .custom-search-input input").autocomplete({
            delay: 500,
            minLength:1,
            source: function( request, response ) {
                $(_trgt +" .custom-search-input input").autocomplete( "option", "source", _cldEmails );
                response( _cldEmails );

            },// _cldEmails,
            focus: function(event, ui) {
                // prevent autocomplete from updating the textbox
                //event.preventDefault();
                $(this).val(ui.item.value);
                $('.autoCompleteScroll.ui-menu-item').each(function () {
                    if($(this).text() === ui.item.value)
                        $(this).css('background-color','#f2f2f2');
                    else
                        $(this).css('background-color','#ffffff');
                })

            },
            select: function( event,ui) {
                if(_trgt == "#srcUsrs"){
                    appendMappingScreenHtml(localStorage.getItem("multiUsrSrcCldId"),"source",1,10,ui.item.value);
                    //  $(".ui-autocomplete").css("margin-top","-33.8% !important");
                }
                else if(_trgt == "#dstnUsrs"){
                    appendMappingScreenHtml(localStorage.getItem("multiUsrDstnCldId"),"destination",1,10,ui.item.value);
                    //  $(".ui-autocomplete").css("margin-top","-34% !important");
                }
                else if(_trgt == '#mapdUsrs'){
                    autoMappingSearch(ui.item.value);
                    //   $(".ui-autocomplete").css("margin-top","-33.8% !important");
                }
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            ul.addClass(target);
            ul.appendTo(_trgt +" .card-block");
            $(".ui-autocomplete.ui-front.ui-menu.ui-widget.ui-widget-content.source").css("display","none");
            $(".ui-autocomplete.ui-front.ui-menu.ui-widget.ui-widget-content.destination").css("display","none");
            return $("<li class='autoCompleteScroll'>" + item.label + "</li>").appendTo(ul);
        };
    },
    getSearchuserList : function (target,keyword) {
        var _cldEmails= [],_cld = "sourceCloudId";
        if(target != "source")
            _cld = "destCloudId";

        $.ajax({
            type: "GET",
            url: apicallurl + "/mapping/user/searchUsers/list/" + _cld + "?srcCloudId=" + localStorage.multiUsrSrcCldId + "&destCloudId=" + localStorage.multiUsrDstnCldId + "&searchCloudUser=" + keyword + "&pageNo=1",
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (response) {
                var _dataLength = response.length;

                for(var i=0; i<_dataLength;i++){
                    var _cldDetailsLength = response[i].cloudDetail.length;
                    for(var j=0; j<_cldDetailsLength;j++){
                        _cldEmails.push(response[i].cloudDetail[j].emailId);

                    }
                }
            }
        });
        return _cldEmails;
    },
    getEntrdSearchuserList : function (target,keyword) {
        var _cldEmails= [],_cld = "sourceCloudId";
        if(target != "source")
            _cld = "destCloudId";

        $.ajax({
            type: "GET",
            url: apicallurl + "/mapping/user/autoSearchUsers/list/" +_cld+ "?srcCloudId=" + localStorage.multiUsrSrcCldId + "&destCloudId=" + localStorage.multiUsrDstnCldId + "&searchCloudUser=" + keyword,
            async: false,
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (response) {
                _cldEmails = response;

            }
        });
        return _cldEmails;
    },
    getMpdUsrSearchList : function (keyword) {
        var _cldEmails= [];
        $.ajax({
            type: "GET",
            async: false,
            url: apicallurl + "/mapping/user/search/list?searchMapp=" + keyword + "&sourceCloudId=" + localStorage.getItem("multiUsrSrcCldId") + "&destCloudId=" + localStorage.getItem("multiUsrDstnCldId") +"&pageNo=1",
            headers: {
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            },
            success: function (data) {
                var _len = data.length;

                for(var i=0;i<_len;i++){
                    _cldEmails.push(data[i].sourceCloudDetails.emailId);
                    _cldEmails.push(data[i].destCloudDetails.emailId);
                }
            }
        });
        return mappingOptions.uniqueArray(_cldEmails);

    },
    getEnrtdMpdUsrSearchList : function (keyword) {
        var _cldEmails= [];
        $.ajax({
            type: "GET",
            async: false,
            url: apicallurl + "/mapping/user/autoMapping/search/list?searchMapp=" + keyword + "&sourceCloudId=" + localStorage.getItem("multiUsrSrcCldId") + "&destCloudId=" + localStorage.getItem("multiUsrDstnCldId"),
            headers: {
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            },
            success: function (data) {
                _cldEmails = data;


            }
        });
        return mappingOptions.uniqueArray(_cldEmails);

    },
    sortMapping:function(trgt) {
        var _sort = localStorage.getItem("mpngSrtng"),
            _obj ={
                "sortBy": trgt,
                "orderBy":'ASC'
            };
        if(_sort != null){
            _sort = JSON.parse(_sort);
            if(_sort.orderBy == 'ASC'){
                _obj.orderBy = 'DESC';
            }
        }
        localStorage.setItem("mpngSrtng",JSON.stringify(_obj));
        if(!$('#mapdUsrs .custom-search-input input').val()){
            appendingPrevMapping();
            return false;
        }
        else{
            appendingPrevMapping();
        }


    },
    sortBy:function () {
        var _sort = localStorage.getItem("mpngSrtng"),_rtn='';

        if(_sort != null){
            _sort = JSON.parse(_sort);
            _rtn = "&sortBy=" + _sort.sortBy + "&orderBy=" + _sort.orderBy;
        }
        return _rtn;

    }
}

var pgngOptions = {
    setCldImg : function(_cldDtls){
        var $srcUsrImg = $("#srcCloudUsers .user-img img"),$dstUsrImg = $("#dstCloudsUsers .user-img img"),
            _rslt = $($srcUsrImg).attr("src").split("/").pop();
        $($srcUsrImg).attr("src",$($srcUsrImg).attr("src").replace(_rslt,_cldDtls.srcCldName));
        $($srcUsrImg).parent().siblings().find("h5").text(_cldDtls.srcAdminName);
        _rslt = $($dstUsrImg).attr("src").split("/").pop();
        $($dstUsrImg).attr("src",$($dstUsrImg).attr("src").replace(_rslt,_cldDtls.dstCldName));
        $($dstUsrImg).parent().siblings().find("h5").text(_cldDtls.dstAdminName);
    },
    chkNumber : function (val) {
        return !/\D/.test( val );
    }
};

//Source  Show value change

$(document).on('change','#srcUsrs select',function(){
    var searchData = JSON.parse(localStorage.getItem("searchData"));

    if($(this).parents('.paginationDiv').hasClass('disabled'))
        return false;
    $('#srcUsrs .paginationCF input').val(1);
    if(searchData.source){
        if($("#srcUsrs .custom-search-input input").val().length){
            migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),1, $('#srcUsrs select').val(),"source",$("#srcUsrs .custom-search-input input").val().toLowerCase().trim()); // if(searchCloudUser != undefined)
            return false;
        }
    }
    else{
        migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), 1, $(this).val(), 'source');
    }

});

$(document).find('#srcUsrs .paginationCF input:last').keypress(function(e){
    var searchData = JSON.parse(localStorage.getItem("searchData"));

    if($(this).parents('.paginationDiv').hasClass('disabled'))
        return false;

    if((!$(this).val().length) && parseInt(e.key) === 0)
        return false;

    var _chkZero = parseInt($(this).val());
    if(($(_chkZero).length == undefined) && (Number(e.key) == 0))
        return false;

    if(e.key == "Enter" && _chkZero && !$("#srcUsrs .custom-search-input input").val().length){
        migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),$(this).val() ,$('#srcUsrs select').val(), 'source');
        $(this).blur();
    }
    else if(e.key == "Enter" && _chkZero && $("#srcUsrs .custom-search-input input").val().length){
        if(searchData.source){
            migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),$(this).val(), $('#srcUsrs select').val(),"source",$("#srcUsrs .custom-search-input input").val().toLowerCase().trim()); // if(searchCloudUser != undefined)
            $(this).blur();
        }
        else {
            migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),$(this).val() ,$('#srcUsrs select').val(), 'source');

        }


    }

    if(pgngOptions.chkNumber(String.fromCharCode(e.which))){
        if((_chkZero *10 + Number(e.key)) == 0)
            return false;

        var _val = (Number($(this).val())) * 10 + Number(e.key);
        if(parseInt( _val) <= $('#srcUsrs .paginationCF span').text().match(/\d+$/)[0] )
            return true;
        else
            return false;
    }
    else if(e.key == "Backspace"){
        return true;
    }
    else
        return false;
});


//Mapping-users  Show value change

$(document).on('change','#mapdUsrs select',function(){

    var searchData = JSON.parse(localStorage.getItem("searchData"));

    if(searchData.mapping){
        autoMappingSearch($("#mapdUsrs .custom-search-input input").val().trim(),1);
    }
    else
        appendingPrevMapping(1,parseInt($(this).val()));

    $('#mapdUsrs .paginationCF input').val(1);
    // if(!$("#mapdUsrs .custom-search-input input").val().length){
    //     autoMappingSearch($("#mapdUsrs .custom-search-input input").val(),1);
    // }
    // //return false;
    // else
    //     appendingPrevMapping(1,parseInt($(this).val()));
    //
    // $('#mapdUsrs .paginationCF input').val(1);
    // appendingPrevMapping(1,parseInt($(this).val()));
});

//Mapping Pagination

$(document).find('#mapdUsrs .paginationCF input').keypress(function(e){
    var searchData = JSON.parse(localStorage.getItem("searchData"));

    if((!$(this).val().length) && parseInt(e.key) === 0)
        return false;

    var _chkZero = Number($('#mapdUsrs .paginationCF input').val());
    if((_chkZero == 0) && (Number(e.key) == 0))
        return false;

    if(e.key == "Enter" && _chkZero && !$("#mapdUsrs .custom-search-input input").val().length){
        var _val = parseInt($('#mapdUsrs select').val());
        //appendingPrevMapping(Math.ceil($(this).val()/_val),_val);
        appendingPrevMapping(Math.ceil($(this).val()),_val);
        $(this).blur();
    }
    else if($("#mapdUsrs .custom-search-input input").val().length && e.key == "Enter" ){
        if(searchData.mapping){
            autoMappingSearch($("#mapdUsrs .custom-search-input input").val().trim(),_chkZero)
            $('#mapdUsrs .paginationCF input').blur();
            $('#mapdUsrs .paginationCF input').val(_chkZero);
        }
        else{
            appendingPrevMapping(Math.ceil($(this).val()),_val);
        }

    }
    if(pgngOptions.chkNumber(String.fromCharCode(e.which))){
        if((_chkZero *10 + Number(e.key)) == 0)
            return false;

        var _val = (Number($(this).val())) * 10 + Number(e.key);
        if(parseInt( _val) <= $('#mapdUsrs .paginationCF span').text().match(/\d+$/)[0] )
            return true;
        else
            return false;
    }
    else if(e.key == "Backspace"){
        return true;
    }
    else
        return false;
});

//Destination  Show value change
$(document).on('change','#dstnUsrs select',function(){
    var searchData = JSON.parse(localStorage.getItem("searchData"));
    if($(this).parents('.paginationDiv').hasClass('disabled'))
        return false;

    $('#dstnUsrs .paginationCF input').val(1);
    if(searchData.destination) {
        if ($("#dstnUsrs .custom-search-input input").val().length) {
            migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), 1, $('#dstnUsrs select').val(), "destination", $("#dstnUsrs .custom-search-input input").val().toLowerCase().trim()); // if(searchCloudUser != undefined)
            return false;
        }
    }
    else{
        migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), 1, $(this).val(), 'destination');
    }

});

$(document).find('#dstnUsrs .paginationCF input:last').keypress(function(e){
    var searchData = JSON.parse(localStorage.getItem("searchData"));

    if($(this).parents('.paginationDiv').hasClass('disabled'))
        return false;

    if((!$(this).val().length) && parseInt(e.key) === 0)
        return false;

    var _chkZero = parseInt($(this).val());
    if(($(_chkZero).length == undefined) && (Number(e.key) == 0))
        return false;

    if(e.key == "Enter" && _chkZero && !$("#dstnUsrs .custom-search-input input").val().length){
        migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), $(this).val(),$('#dstnUsrs select').val(), 'destination');
        $(this).blur();
    }
    if(e.key == "Enter" && _chkZero && $("#dstnUsrs .custom-search-input input").val().length){
        if(searchData.destination) {
            migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),$(this).val(), $('#dstnUsrs select').val(),"destination",$("#dstnUsrs .custom-search-input input").val().toLowerCase().trim()); // if(searchCloudUser != undefined)
            $(this).blur();
        }
        else{
            migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), $(this).val(),$('#dstnUsrs select').val(), 'destination');

        }

    }
    if(pgngOptions.chkNumber(String.fromCharCode(e.which))){
        if((_chkZero *10 + Number(e.key)) == 0)
            return false;

        var _val = (Number($(this).val())) * 10 + Number(e.key);
        if(parseInt( _val) <= $('#dstnUsrs .paginationCF span').text().match(/\d+$/)[0] )
            return true;
        else
            return false;
    }
    else if(e.key == "Backspace"){
        return true;
    }
    else
        return false;
});

// Source step-forward
$(document).on('click',"#srcUsrs .fa-chevron-right:last",function () {
    var searchData = JSON.parse(localStorage.getItem("searchData"));

    if($(this).parents('.paginationDiv').hasClass('disabled'))
        return false;

    var _selVal =parseInt($('#srcUsrs select').val());
    //var _curPage = Number($("#srcUsrs .paginationDiv input").val()) + _selVal;
    var _curPage = Number($("#srcUsrs .paginationDiv:last input").val()) + 1;
    var _maxPages = Number($("#srcUsrs .paginationDiv:last span").text().match(/\d+$/)[0]);


    if( _curPage <= _maxPages ){
        var _class = $("#srcUsrs .forDomainNameMin:visible").attr("class");
        $("#srcUsrs .paginationDiv:last input").val(_curPage);
        var _searchCloudUser = $("#srcUsrs .custom-search-input input").val().trim();
        if(searchData.source) {
            if(_searchCloudUser.length)
                migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),_curPage, _selVal,'source',_searchCloudUser);
        }
        else
            migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), _curPage, _selVal, 'source');
    }

})

// Source step-backward
$(document).on('click',"#srcUsrs .fa-chevron-left:last",function () {
    var searchData = JSON.parse(localStorage.getItem("searchData"));

    if($(this).parents('.paginationDiv').hasClass('disabled'))
        return false;

    var _selVal =parseInt($('#srcUsrs select').val());
    //var _curPage = Number($("#srcUsrs .paginationDiv input").val()) - _selVal;
    var _curPage = Number($("#srcUsrs .paginationDiv:last input").val()) - 1;


    if( _curPage > 0 ){
        var _class = $("#srcUsrs .forDomainNameMin:visible").attr("class");
        //migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), _curPage, _selVal, 'source');
        $("#srcUsrs .paginationDiv:last input").val(_curPage);
        var _searchCloudUser = $("#srcUsrs .custom-search-input input").val().trim();
        if(searchData.source) {
            if(_searchCloudUser.length)
                migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),_curPage, _selVal,'source',_searchCloudUser);
        }
        else
            migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), _curPage, _selVal, 'source');

    }

})

// Destination step-forward
$(document).on('click',"#dstnUsrs .fa-chevron-right:last",function () {
    var searchData = JSON.parse(localStorage.getItem("searchData"));

    if($(this).parents('.paginationDiv').hasClass('disabled'))
        return false;

    var _selVal =parseInt($('#dstnUsrs select').val());
    //var _curPage = Number($("#dstnUsrs .paginationDiv input").val()) + _selVal;
    var _curPage = Number($("#dstnUsrs .paginationDiv:last input").val()) + 1;
    var _maxPages = Number($("#dstnUsrs .paginationDiv:last span").text().match(/\d+$/)[0]);


    if( _curPage <= _maxPages ){
        var _class = $("#dstnUsrs .forDomainNameMin:visible").attr("class");
        // migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), _curPage, _selVal, 'destination');
        var _searchCloudUser = $("#dstnUsrs .custom-search-input input").val().trim();
        if(searchData.destination) {
            if(_searchCloudUser.length)
                migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),_curPage, _selVal,'destination',_searchCloudUser);
        }
        else
            migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), _curPage, _selVal, 'destination');

        $("#dstnUsrs .paginationDiv:last input").val(_curPage);
    }

})

// Destination step-backward
$(document).on('click',"#dstnUsrs .fa-chevron-left:last",function () {
    var searchData = JSON.parse(localStorage.getItem("searchData"));
    if($(this).parents('.paginationDiv').hasClass('disabled'))
        return false;

    var _selVal =parseInt($('#dstnUsrs select').val());
    //var _curPage = Number($("#dstnUsrs .paginationDiv input").val()) - _selVal;
    var _curPage = Number($("#dstnUsrs .paginationDiv:last input").val()) - 1;


    if( _curPage > 0 ){
        var _class = $("#dstnUsrs .forDomainNameMin:visible").attr("class");
        //migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), _curPage, _selVal, 'destination');
        var _searchCloudUser = $("#dstnUsrs .custom-search-input input").val().trim();
        if(searchData.destination) {
            if(_searchCloudUser.length)
                migrationMapping.getSearchUser(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),_curPage, _selVal,'destination',_searchCloudUser);
        }
        else
            migrationMapping.getDomainsList(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), _curPage, _selVal, 'destination');


        $("#dstnUsrs .paginationDiv:last input").val(_curPage);
    }

})

// Mapped step-forward
$(document).on('click',"#mapdUsrs .fa-chevron-right",function () {
    var searchData = JSON.parse(localStorage.getItem("searchData"));

    var _selVal = parseInt($('#mapdUsrs select').val());
    var _curPage = Number($("#mapdUsrs .paginationDiv input").val()) + 1;
    var _maxPages = Number($("#mapdUsrs .paginationDiv span").text().match(/\d+$/)[0]);



    if( _curPage <= _maxPages ){
        if($("#mapdUsrs .custom-search-input input").val().length) {
            if (searchData.mapping) {
                autoMappingSearch($("#mapdUsrs .custom-search-input input").val().trim(), _curPage);
            }
            else
                appendingPrevMapping(_curPage, _selVal);
        }
        else{
            appendingPrevMapping(_curPage, _selVal);
        }

        $("#mapdUsrs .paginationDiv input").val(_curPage);
    }

})

// Mapped step-backward
$(document).on('click',"#mapdUsrs .fa-chevron-left",function () {
    var searchData = JSON.parse(localStorage.getItem("searchData"));
    var _selVal = parseInt($('#mapdUsrs select').val());
    var _curPage = Number($("#mapdUsrs .paginationDiv input").val()) - 1;


    if( _curPage > 0 ){
        if($("#mapdUsrs .custom-search-input input").val().length) {
            if (searchData.mapping) {
                autoMappingSearch($("#mapdUsrs .custom-search-input input").val().trim(), _curPage);
            }
            else
                appendingPrevMapping(_curPage,_selVal);
        }
        else
            appendingPrevMapping(_curPage,_selVal);

        $("#mapdUsrs .paginationDiv input").val(_curPage);
    }

})



/*var pgno =  parseInt($(this).parents(".fldr_parent").attr('pgno'));
    pgno += 1;
    $(this).parents(".fldr_parent").attr('pgno', pgno);
    if($(this).hasClass('hasnext')) {
        $(this).parent(".fldr_parent1").css("display","none");
        $(this).parent(".fldr_parent1").next().css("display","block");
    }
    else{
        dataList(this, pgno);
        return false;
    }
*/




// Source users step-forward
$(document).on('click',"#srcCloudUsers .forDomainNameMin .fa-chevron-right",function () {
    var _selVal = 1; //parseInt($('#srcUsrs select').val());
    var _curPage = Number($(this).siblings().find("input").val()) + _selVal;
    var _maxPages = Number($(this).siblings().find("span:nth-child(3)").text().match(/\d+$/)[0]);
    var _class = $(this).parents("div.forDomainNameMin").attr("class").split(" ")[1];
    //var $rmvUsrs = "#srcCloudUsers .forDomainNameMin[class*='" + _class +"']";
    var $rmvUsrs = $(document.getElementById("srcCloudUsers").getElementsByClassName(_class));
    $($rmvUsrs[1]).css("display","block");
    $($rmvUsrs[0]).css("display","none");
    if( _curPage <= _maxPages ){
        migrationMapping.getPgntdUsrsData(_class, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), _curPage, 10, "source", $rmvUsrs[1]);
        $(this).siblings().find("input").val(_curPage);
    }
})



// Source users step-backward
$(document).on('click',"#srcCloudUsers .forDomainNameMin .fa-chevron-left",function () {
    var _selVal = 1; //parseInt($('#srcUsrs select').val());
    var _curPage = Number($(this).siblings().find("input").val()) - _selVal;
    var _class = $(this).parents("div.forDomainNameMin").attr("class").split(" ")[1];
    //var $rmvUsrs = "#srcCloudUsers .forDomainNameMin[class*='" + _class +"']";
    var $rmvUsrs = $(document.getElementById("srcCloudUsers").getElementsByClassName(_class));
    $($rmvUsrs[1]).css("display","block");
    $($rmvUsrs[0]).css("display","none");

    if( _curPage > 0 ){
        $(this).siblings().find("input").val(_curPage);
        migrationMapping.getPgntdUsrsData(_class, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), _curPage, 10, "source", $rmvUsrs[1]);
    }
})

// Destination user step-forward
$(document).on('click',"#dstCloudsUsers .forDomainNameMin .fa-chevron-right",function () {
    var _selVal = 1; //parseInt($('#srcUsrs select').val());
    var _curPage = Number($(this).siblings().find("input").val()) + _selVal;
    var _maxPages = Number($(this).siblings().find("span:nth-child(3)").text().match(/\d+$/)[0]);
    var _class = $(this).parents("div.forDomainNameMin").attr("class").split(" ")[1];
    //var $rmvUsrs = "#dstCloudsUsers .forDomainNameMin[class*='" + _class +"']";
    var $rmvUsrs = $(document.getElementById("dstCloudsUsers").getElementsByClassName(_class));
    $($rmvUsrs[1]).css("display","block");
    $($rmvUsrs[0]).css("display","none");
    if( _curPage <= _maxPages ){
        migrationMapping.getPgntdUsrsData(_class, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), _curPage, 10, "destination", $rmvUsrs[1]);
        $(this).siblings().find("input").val(_curPage);
    }

})

// Destination users step-backward
$(document).on('click',"#dstCloudsUsers .forDomainNameMin .fa-chevron-left",function () {

    var _selVal = 1; //parseInt($('#srcUsrs select').val());
    var _curPage = Number($(this).siblings().find("input").val()) - _selVal;
    var _class = $(this).parents("div.forDomainNameMin").attr("class").split(" ")[1];
    //var $rmvUsrs = "#dstCloudsUsers .forDomainNameMin[class*='" + _class +"']";
    var $rmvUsrs = $(document.getElementById("dstCloudsUsers").getElementsByClassName(_class));
    $($rmvUsrs[1]).css("display","block");
    $($rmvUsrs[0]).css("display","none");

    if( _curPage > 0 ){
        $(this).siblings().find("input").val(_curPage);
        migrationMapping.getPgntdUsrsData(_class, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), _curPage, 10, "destination", $rmvUsrs[1]);
    }

});





/*$("#rplcChar").on('change',function(){
    if($(this).is(':checked')){
        $(this).next().next().css("display","");
    }
    else{
        $(this).next().next().css("display","none");
    }
});*/

//source and destination options
$(document).on('click','.fa-plus.expandSourceOptions',function () {
    $(this).removeClass('fa-plus').addClass('fa-minus');
    $(".expandSourceOptionsHide").css('display','');
});
$(document).on('click','.fa-plus.expandDstnOptions',function () {
    $(this).removeClass('fa-plus').addClass('fa-minus');
    $(".expandDsteOptionsHide").css('display','');
});
$(document).on('click','.fa-minus.expandSourceOptions',function () {
    $(this).removeClass('fa-minus').addClass('fa-plus');
    $(".expandSourceOptionsHide").css('display','none');
});
$(document).on('click','.fa-minus.expandDstnOptions',function () {
    $(this).removeClass('fa-minus').addClass('fa-plus');
    $(".expandDsteOptionsHide").css('display','none');
});
$(document).on('click','.bold .cf-plus5',function () {
    //mappingOptions.appendUserPairs();
    $(this).removeClass("cf-plus5").addClass("cf-minus5");
    $(document).find(".showTable").css("display",'none');
    // if($(this).text().toLowerCase().indexOf("show") > -1){
    //     $(this).text("Hide");
    //     $(document).find(".showTable").css("display",'');
    // }
    // else{
    //     $(this).text("Show");
    //     $(document).find(".showTable").css("display",'none');
    // }
});
$(document).on('click','.bold .cf-minus5',function () {
    $(this).removeClass("cf-minus5").addClass("cf-plus5");
    $(document).find(".showTable").css("display",'');

});





/* ... Source Users ...*/

$(document).on('keypress','#srcCloudUsers .paginationCF input',function(e){
    if((!$(this).val().length) && parseInt(e.key) === 0)
        return false;

    var _chkZero = parseInt($(this).val());
    if(($(_chkZero).length == undefined) && (Number(e.key) == 0))
        return false;

    if (e.key == "Enter" && _chkZero) {
        var _class = $(this).parents(".forDomainNameMin").attr("class").split(" ")[1];
        //var $rmvUsrs = "#srcCloudUsers .forDomainNameMin[class*='" + _class +"']";
        var $rmvUsrs = $(document.getElementById("srcCloudUsers").getElementsByClassName(_class));
        migrationMapping.getPgntdUsrsData(_class, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), $(this).val(), 10, "source", $rmvUsrs[1]);
        $(this).blur();
    }

    if(pgngOptions.chkNumber(String.fromCharCode(e.which))){
        if((_chkZero *10 + Number(e.key)) == 0)
            return false;

        var _val = (Number($(this).val())) * 10 + Number(e.key);
        if(parseInt( _val) <= Number( $(this).siblings("span").text().match(/\d+$/)[0]) )
            return true;
        else
            return false;
    }
    else if(e.key == "Backspace"){
        return true;
    }
    else
        return false;

});

/* ... Destination Users ...*/

$(document).on('keypress','#dstCloudsUsers .paginationCF input',function(e){
    if((!$(this).val().length) && parseInt(e.key) === 0)
        return false;

    var _chkZero = parseInt($(this).val());
    if(($(_chkZero).length == undefined) && (Number(e.key) == 0))
        return false;

    if(e.key == "Enter" && _chkZero){
        var _class = $(this).parents(".forDomainNameMin").attr("class").split(" ")[1];
        //var $rmvUsrs = "#dstCloudsUsers .forDomainNameMin[class*='" + _class +"']";
        var $rmvUsrs = $(document.getElementById("dstCloudsUsers").getElementsByClassName(_class));
        migrationMapping.getPgntdUsrsData(_class, localStorage.getItem("multiUsrSrcCldId"), localStorage.getItem("multiUsrDstnCldId"), $(this).val(), 10, "destination", $rmvUsrs[1]);
        $(this).blur();
    }
    if(pgngOptions.chkNumber(String.fromCharCode(e.which))){
        if((_chkZero *10 + Number(e.key)) == 0)
            return false;

        var _val = (Number($(this).val())) * 10 + Number(e.key);
        if(parseInt( _val) <= Number( $(this).siblings("span").text().match(/\d+$/)[0]) )
            return true;
        else
            return false;
    }
    else if(e.key == "Backspace"){
        return true;
    }
    else
        return false;
});


/*....   Migration-Pairs show value .... */
$(document).on('change','#preview select',function(){
    $('#preview .paginationCF input').val(1);
    mgrtnPrs.pgntdData(1,parseInt($(this).val()));
});



/*....   Migration-Pairs Pagination .... */
$(document).find('#preview .paginationCF input').keypress(function(e){
    if((!$(this).val().length) && parseInt(e.key) === 0)
        return false;
    var _num ;
    if(e.key == "Enter" )
        _num = Number($(this).val());
    else
        _num = Number($(this).val())*10 + Number(e.key);

    if(_num < 1){
        $('#preview .paginationCF input').blur();
        $('#preview .paginationCF input').val($("#preview .paginationCF span").text().split("Go to:")[1].split(" of")[0]);
        return false;
    }


    if(e.key == "Enter" && (_num >0)){
        var _val = parseInt($('#preview select').val());
        mgrtnPrs.pgntdData(Math.ceil($(this).val()),_val);
        $(this).blur();
    }
    if(pgngOptions.chkNumber(String.fromCharCode(e.which))){
        var _val = (Number($(this).val())) * 10 + Number(e.key);
        if(parseInt( _val) <= $('#preview .paginationCF span').text().match(/\d+$/)[0] )
            return true;
        else
            return false;
    }
    else if(e.key == "Backspace"){
        return true;
    }
    else
        return false;
});



/*....   Migration-Pairs step-forward .... */
$(document).on("click","#preview .fa-chevron-right",function () {
    var _selVal = parseInt($('#preview select').val());
    var _curPage = Number($("#preview .paginationDiv input").val()) + 1;
    var _maxPages = Number($("#preview .paginationDiv span").text().match(/\d+$/)[0]);



    if( _curPage <= _maxPages ){
        mgrtnPrs.pgntdData(Math.floor(_curPage/1),parseInt($('#preview select').val()));
        $("#preview .paginationDiv input").val(_curPage);
    }

})

/*....   Migration-Pairs step-backward .... */
$(document).on("click","#preview .fa-chevron-left",function () {
    var _selVal = parseInt($('#preview select').val());
    var _curPage = Number($("#preview .paginationDiv input").val()) - 1;


    if( _curPage > 0 ){
        mgrtnPrs.pgntdData(Math.floor(_curPage/1),parseInt($('#preview select').val()));
        $("#preview .paginationDiv input").val(_curPage);
    }

})


/*....   Migration-Pairs pagination .... */
var mgrtnPrs = {
    pgntdData : function (pgNo,pgSze) {
        $.ajax({
            type: "GET",
            url: apicallurl + "/move/newmultiuser/preview/" + localStorage.jobId + "?pageNo=" + pgNo +"&pageSize=" + pgSze,
            //   async: false,
            headers: {
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            },
            success: function (data) {
                var _dataLength = data.previewDetail.length;   //if(_dataLength)
                var _len = data.listOfMoveWorkspaceId.length;
                if(pgNo == 1)
                    $("#preview .paginationDiv input").val(1);

                $("#appendSelectedPairs").html("");
                var _size = parseInt(pgNo*pgSze);
                if( _size > _len)
                    _size = _len;

                //var _txt = " " + parseInt(((pgNo - 1)*pgSze) + 1) + " - " + _size + " of " + _len;
                var _txt = pgNo + " of " + Math.ceil(_len/pgSze);
                $(document).find("#preview .paginationCF div span:nth-child(3)").text(_txt);
                if((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN")  || (data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
                    $("#preview").find("#previewMapping").find("thead tr th.nonProvisoned").remove();
                    var _th = '<th style="width: 28%;" class="nonProvisoned">Provision Status</th>';
                    $("#preview").find("#previewMapping").find("thead tr").append(_th);
                    $("#preview").find("#previewMapping").find("thead tr").find(".SrcUsrs").css("width", '35%');
                    $("#preview").find("#previewMapping").find("thead tr").find(".DstnUsrs").css("width", '35%');
                    var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div class = "prvsnStatus" style ="margin-left: 8%">Not Provisioned</div></div>';
                }
                else {
                    $("#preview").find("#previewMapping").find("thead tr th.nonProvisoned").remove();
                    var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 53%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div></div>';
                }
                //   localStorage.removeItem('FolderChecked');
                for(var i=0;i<_dataLength;i++){
                    var _usrData = data.previewDetail[i];
                    if (((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN")&&(data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN"))&&((data.previewDetail[i].toProvision) == true || (data.previewDetail[i].fromProvision) == true)){
                        var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div class = "prvsnStatus" style ="margin-left: 8%;color:green">Not Provisioned</div></div>';
                        var _appendHtml = _html.replace("Melvin@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.fromEmailId,25)).replace("Tania@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.toEmailId,25)).replace("Not Provisioned","Provisioned");
                    }
                    else if (data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN"&&data.previewDetail[i].fromProvision == true){
                        var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div class = "prvsnStatus" style ="margin-left: 8%;color:green">Not Provisioned</div></div>';
                        var _appendHtml = _html.replace("Melvin@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.fromEmailId,25)).replace("Tania@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.toEmailId,25)).replace("Not Provisioned","Provisioned");
                    }
                    else if (data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN"&& data.previewDetail[i].toProvision == true){
                        var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div class = "prvsnStatus" style ="margin-left: 8%;color:green">Not Provisioned</div></div>';
                        var _appendHtml = _html.replace("Melvin@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.fromEmailId,25)).replace("Tania@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.toEmailId,25)).replace("Not Provisioned","Provisioned");
                    }else{
                        var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div class = "prvsnStatus" style ="margin-left: 8%;color:red">Not Provisioned</div></div>';
                        var _appendHtml = _html.replace("Melvin@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.fromEmailId,25)).replace("Tania@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.toEmailId,25)).replace("Not Provisioned","Not Provisioned");
                    }
                    if(!((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN")||(data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN"))){
                        var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 53%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div></div>';
                        var _appendHtml = _html.replace("Melvin@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.fromEmailId,25)).replace("Tania@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.toEmailId,25));
                    }
                    if(i%2 == 1)
                        _appendHtml = _appendHtml.replace("#ffffff","#f2f2f2");

                    $("#appendSelectedPairs").append(_appendHtml);
                }

                /*         var _dataLength = data.previewDetail.length;   //if(_dataLength)
                                if($('#jobType_dropDown :selected').text() !== "One-way sync") {
                                    var _len = data.listOfMoveWorkspaceId.length;
                                }
                                else {
                                    var _len = JSON.parse(localStorage.getItem('selMappings')).length;
                                }
                                if(pgNo == 1)
                                    $("#preview .paginationDiv input").val(1);

                                $("#appendSelectedPairs").html("");
                                var _size = parseInt(pgNo*pgSze);
                                if( _size > _len)
                                    _size = _len;

                                //var _txt = " " + parseInt(((pgNo - 1)*pgSze) + 1) + " - " + _size + " of " + _len;
                                var _txt = pgNo + " of " + Math.ceil(_len/pgSze);
                                $(document).find("#preview .paginationCF div span:nth-child(3)").text(_txt);
                                if($('#jobType_dropDown :selected').text() === "One-way sync") {
                                    $("#preview").find("#previewMapping").find("thead tr th.job_name").remove();
                                    var JOBNAME = '<th style="width: 19%;" class="job_name">Job Name</th>';
                                    $("#preview").find("#previewMapping").find("thead tr").append(JOBNAME);
                                    $("#preview").find("#previewMapping").find("thead tr").find(".SrcUsrs").css("width", '30%');
                                    $("#preview").find("#previewMapping").find("thead tr").find(".DstnUsrs").css("width", '30%');
                                    if ((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN") || (data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
                                        $("#preview").find("#previewMapping").find("thead tr th.nonProvisoned").remove();
                                        var _th = '<th style="width: 20%;" class="nonProvisoned">Provision Status</th>';
                                        $("#preview").find("#previewMapping").find("thead tr").append(_th);
                                        $("#preview").find("#previewMapping").find("thead tr").find(".SrcUsrs").css("width", '30%');
                                        $("#preview").find("#previewMapping").find("thead tr").find(".DstnUsrs").css("width", '30%');
                                        var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div style="width: 20%;margin-right: 6%;">ODFB-ODFB-May.30.2018-46</div><div class = "prvsnStatus" style="width: 15%;">Not Provisioned</div></div>';
                                    }
                                    else {
                                        $("#preview").find("#previewMapping").find("thead tr th.nonProvisoned").remove();
                                        var _html = '<div id="selectedPairs" style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div style="width: 22%;text-align: center;margin-left: 10%">ODFB-ODFB-May.30.2018-46</div></div>';
                                    }
                                }
                                else {
                                    $("#preview").find("#previewMapping").find("thead tr th.job_name").remove();
                                    if ((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN") || (data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) {
                                        $("#preview").find("#previewMapping").find("thead tr th.nonProvisoned").remove();
                                        var _th = '<th style="width: 28%;" class="nonProvisoned">Provision Status</th>';
                                        $("#preview").find("#previewMapping").find("thead tr").append(_th);
                                        $("#preview").find("#previewMapping").find("thead tr").find(".SrcUsrs").css("width", '35%');
                                        $("#preview").find("#previewMapping").find("thead tr").find(".DstnUsrs").css("width", '35%');
                                        var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 30%;margin-right: 4%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 30%;margin-right: 5%"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div class = "prvsnStatus" style="width: 20%;text-align: center;margin-left: 5%">Not Provisioned</div></div>';
                                    }
                                    else{
                                        var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 53%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div></div>';
                                    }
                                }
                                //   localStorage.removeItem('FolderChecked');
                                for(var i=0;i<_dataLength;i++){
                                    var _usrData = data.previewDetail[i];
                                    if($('#jobType_dropDown :selected').text() === "One-way sync") {
                                        if (((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN") || (data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) && ((data.previewDetail[i].toProvision) === true || (data.previewDetail[i].fromProvision) === true)) {
                                            var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div style="width: 20%;margin-right: 6%;">ODFB-ODFB-May.30.2018-46</div><div class = "prvsnStatus" style ="width:15%;color:red">Not Provisioned</div></div>';
                                            var _appendHtml = _html.replace("Melvin@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.fromEmailId, 25)).replace("Tania@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.toEmailId, 25)).replace("ODFB-ODFB-May.30.2018-46",_usrData.jobName).replace("Not Provisioned", "Not Provisioned");
                                        }
                                        else if (((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN") || (data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) && ((data.previewDetail[i].toProvision) === false || (data.previewDetail[i].fromProvision) === false)) {
                                            var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div style="width: 20%;margin-right: 6%;">ODFB-ODFB-May.30.2018-46</div><div class = "prvsnStatus" style ="width:15%;color:green">Not Provisioned</div></div>';
                                            var _appendHtml = _html.replace("Melvin@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.fromEmailId, 25)).replace("Tania@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.toEmailId, 25)).replace("ODFB-ODFB-May.30.2018-46",_usrData.jobName).replace("Not Provisioned", "Provisioned");
                                        }
                                        else{
                                            var _html = '<div id="selectedPairs" style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div style="width: 22%;text-align: center;margin-left: 10%"">ODFB-ODFB-May.30.2018-46</div>></div>';
                                            var _appendHtml = _html.replace("Melvin@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.fromEmailId, 25)).replace("Tania@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.toEmailId, 25)).replace("ODFB-ODFB-May.30.2018-46",_usrData.jobName);
                                        }
                                    }
                                    else{
                                        if (((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN") || (data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) && ((data.previewDetail[i].toProvision) === true || (data.previewDetail[i].fromProvision) === true)) {
                                            var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div class = "prvsnStatus" style ="margin-left: 8%;color:red">Not Provisioned</div></div>';
                                            var _appendHtml = _html.replace("Melvin@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.fromEmailId, 25)).replace("Tania@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.toEmailId, 25)).replace("Not Provisioned", "Not Provisioned");
                                        }
                                        else if (((data.fromCloudName === "ONEDRIVE_BUSINESS_ADMIN") || (data.toCloudName === "ONEDRIVE_BUSINESS_ADMIN")) && ((data.previewDetail[i].toProvision) === false || (data.previewDetail[i].fromProvision) === false)) {
                                            var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 35%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div style="width: 35%;"><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div><div class = "prvsnStatus" style ="margin-left: 8%;color:green">Not Provisioned</div></div>';
                                            var _appendHtml = _html.replace("Melvin@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.fromEmailId, 25)).replace("Tania@cloudfuze.co", CFManageCloudAccountsAjaxCall.getMaxChars(_usrData.toEmailId, 25)).replace("Not Provisioned", "Provisioned");
                                        }
                                        else{
                                            var _html = '<div style="display: flex;border-bottom: 1px solid #ccc;padding: 8px;background: #ffffff"><div style="width: 53%"><img src=" ../img/drive/circle/' + data.fromCloudName + '.png" alt="From cloud Image" class="forImage">Melvin@cloudfuze.co</div><div><img src=" ../img/drive/circle/' + data.toCloudName + '.png" alt="To cloud Image" class="forImage">Tania@cloudfuze.co</div></div>';
                                            var _appendHtml = _html.replace("Melvin@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.fromEmailId,25)).replace("Tania@cloudfuze.co",CFManageCloudAccountsAjaxCall.getMaxChars( _usrData.toEmailId,25));

                                        }
                                    }
                                    if(i%2 == 1)
                                        _appendHtml = _appendHtml.replace("#ffffff","#f2f2f2");

                                    $("#appendSelectedPairs").append(_appendHtml);
                                }
                                $("#forNextMove").removeClass("disabled");
                               */
                setTimeout(function(){
                    $("#CFShowLoading").css("display","none");
                }, 2000);

            }
        });
    }
}


/* ....  On Blur  .... */
$(document).find('#srcUsrs #srcCloudUsers').siblings().find("input.input-sm").focusout(function(){
    var _val = $(this).siblings().text().split("Go to:")[1].split(" ")[0];
    if( parseInt($(this).val()) != _val)
        $(this).val(_val);
});
$(document).find('#dstnUsrs #dstCloudsUsers').siblings().find("input.input-sm").focusout(function(){
    // if($(this).val().length || parseInt($(this).val())<1)
    //     $(this).val($(this).siblings().text().split("Go to:")[1].split(" ")[0]);

    var _val = $(this).siblings().text().split("Go to:")[1].split(" ")[0];
    if( parseInt($(this).val()) != _val)
        $(this).val(_val);
});
$(document).find('#mapdUsrs .paginationCF input').focusout(function(){
    var _val = $(this).siblings().text().split("Go to:")[1].split(" ")[0];
    if( parseInt($(this).val()) != _val)
        $(this).val(_val);
});

$(document).on('focusout','#srcUsrs #srcCloudUsers input.input-sm',function(){
    var _val = $(this).siblings().text().split("Go to:")[1].split(" ")[0];
    if( parseInt($(this).val()) != _val)
        $(this).val(_val);
});
$(document).on('focusout','#dstnUsrs #dstCloudsUsers input.input-sm',function(){
    var _val = $(this).siblings().text().split("Go to:")[1].split(" ")[0];
    if( parseInt($(this).val()) != _val)
        $(this).val(_val);
});
$(document).find('#preview .paginationCF input').focusout(function(){
    var _val = $(this).siblings().text().split("Go to:")[1].split(" ")[0];
    if( parseInt($(this).val()) != _val)
        $(this).val(_val);
});


// Adding Notification emails

$(document).find('#additionalemailsold').keypress(function(e){
    if(e.key == "Enter" || e.key == ',' || e.keyCode == 32){
        e.preventDefault();
        var _email = $(this).val().trim();
        if(!mappingOptions.checkEmail(_email)){
            $("#forNextMove").removeClass('disabled');
            return false;
        }
        else{
            var _html = '<div style="border: 1px solid #aaa;padding: 5px;display:inline-block;background: #fff;margin:8px 5px;" class="emailParent"><span>' + _email +'</span><div style=" margin: 0 0 0 13px; font-size: 12px;float: right;" class="closeEmail"><i class="fa fa-times" style="cursor: pointer"></i></div></div>&nbsp'
            $(".notifiedMails").append(_html);
            $(this).val('');
            //$("textarea").val("");
        }
    }

});
$(document).find('#additionalEmails').keypress(function(e){
    if(e.key == "Enter" || e.key == ',' || e.keyCode == 32){
        e.preventDefault();
        var _email = $(this).val().trim();
        if(_email.length === 0){
            return false;
        }
        if(!mappingOptions.checkEmailNew(_email)){
            $("#forNextMove").removeClass('disabled');
            return false;
        }
        else{
            var _html = '<div style="border: 1px solid #aaa;padding: 5px;display:inline-block;background: #fff;margin:8px 5px;" class="emailParent"><span>' + _email +'</span><div style=" margin: 0 0 0 13px; font-size: 12px;float: right;" class="closeEmail"><i class="fa fa-times" style="cursor: pointer"></i></div></div>&nbsp'
            $(".notifiedMailsNew").append(_html);
            $(this).val('');
            //$("textarea").val("");
        }
    }

});
$(document).find('#fileTypeExc').keypress(function(e){
    if(e.key == "Enter" || e.key == ',' || e.keyCode == 32){
        e.preventDefault();
        var _excFiles = $(this).val().toLowerCase().trim();
        if(_excFiles.length === 0){
            return false;
        }
        if(!mappingOptions.checkfileType(_excFiles)){
            $("#forNextMove").removeClass('disabled');
            return false;
        }
        else{
            var _html = '<div style="border: 1px solid #aaa;padding: 5px;display:inline-block;background: #fff;margin:8px 5px;" class="emailParent"><span>' + _excFiles +'</span><div style=" margin: 0 0 0 13px; font-size: 12px;float: right;" class="closeFile"><i class="fa fa-times" style="cursor: pointer"></i></div></div>&nbsp'
            $(".notifiedFileTypes").append(_html);
            $(this).val('');
            //$("textarea").val("");
        }
    }

});
$(document).on('click','.closeEmail',function() {
    $(this).parent().remove();
});
$(document).on('click','.closeFile',function() {
    $(this).parent().remove();
});
//sorting order in mapped screen
$("#sortngSrc").click(function () {
    var searchData = JSON.parse(localStorage.getItem("searchData"));
    if(searchData.mapping) {
        if ($('#mapdUsrs .custom-search-input input').val()) {
            var obj = JSON.parse(localStorage.getItem("mpngSrtng"));
            obj.sortBy = "sourceCloud";
            //obj.orderBy = 'ASC';
            if (obj.orderBy == 'ASC') {
                obj.orderBy = 'DESC';
            }
            else
                obj.orderBy = 'ASC';
            localStorage.setItem("mpngSrtng", JSON.stringify(obj));
            autoMappingSearch($("#mapdUsrs .custom-search-input input").val().trim());
        }
    }
    else{
        migrationMapping.sortMapping("sourceCloud");
    }

});
$("#sortngDstn").click(function () {
    var searchData = JSON.parse(localStorage.getItem("searchData"));
    if(searchData.mapping) {
        if ($('#mapdUsrs .custom-search-input input').val()) {
            var obj = JSON.parse(localStorage.getItem("mpngSrtng"));
            obj.sortBy = "destCloud";
            //obj.orderBy = 'DESC';
            if (obj.orderBy == 'ASC') {
                obj.orderBy = 'DESC';
            }
            else
                obj.orderBy = 'ASC';
            localStorage.setItem("mpngSrtng", JSON.stringify(obj));
            autoMappingSearch($("#mapdUsrs .custom-search-input input").val().trim());
            return false;
        }
    }
    else
        migrationMapping.sortMapping("destCloud");
});
//Pressing other characters at destination

$(document).find("#dstnUsrs .custom-search-input input").keydown(function(e){
    if((e.key == "Control") ||(e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z")|| (e.key == "ArrowUp") || (e.key == "ArrowDown")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
$(document).find("#dstnUsrs .custom-search-input input").keypress(function(e){
    if((e.key == "Control") ||(e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z")|| (e.key == "ArrowUp") || (e.key == "ArrowDown")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
$(document).find("#dstnUsrs .custom-search-input input").keyup(function(e){
    if((e.key == "Control") ||(e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z")|| (e.key == "ArrowUp")|| (e.key == "ArrowDown")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
//Pressing other characters at source

$(document).find("#srcUsrs .custom-search-input input").keydown(function(e){
    if((e.key == "Control") ||(e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z")|| (e.key == "ArrowUp") || (e.key == "ArrowDown")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        //return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
$(document).find("#srcUsrs .custom-search-input input").keypress(function(e){
    if((e.key == " ") || (e.key == "Control") ||(e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z") || (e.key == "ArrowUp") || (e.key == "ArrowDown")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        //return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
$(document).find("#srcUsrs .custom-search-input input").keyup(function(e){
    if((e.key == " ") || (e.key == "Control") ||(e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z")|| (e.key == "ArrowUp") || (e.key == "ArrowDown")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        //return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
//Mozilla issues for refresh
$(document).find("#mapdUsrs .custom-search-input input").keydown(function(e){
    if((e.key == " ") || (e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z")|| (e.key == "ArrowUp") || (e.key == "ArrowDown")){
        e.preventDefault();
        return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
$(document).find("#mapdUsrs .custom-search-input input").keypress(function(e){
    if((e.key == " ") || (e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z")|| (e.key == "ArrowUp") || (e.key == "ArrowDown")){
        e.preventDefault();
        return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
$(document).find("#mapdUsrs .custom-search-input input").keyup(function(e){
    if((e.key == " ") ||(e.key == "Escape") || (e.key == "CapsLock")|| (e.key == "Shift")|| (e.key == "Alt")|| (e.key == "ctrl+c")|| (e.key == "ctrl+z")|| (e.key == "ArrowUp") || (e.key == "ArrowDown")){
        e.preventDefault();
        return false;
    }
    else if((e.key == "ArrowLeft") || (e.key == "ArrowRight")){
        $("#CFShowLoading").css("display","none");
    }
});
//Enter loading issue
$(document).find("#srcUsrs .custom-search-input input").keydown(function(e){
    if(($("#srcUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
$(document).find("#srcUsrs .custom-search-input input").keypress(function(e){
    if(($("#srcUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
$(document).find("#srcUsrs .custom-search-input input").keyup(function(e){
    if(($("#srcUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
$(document).find("#dstnUsrs .custom-search-input input").keydown(function(e){
    if(($("#dstnUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
$(document).find("#dstnUsrs .custom-search-input input").keypress(function(e){
    if(($("#dstnUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
$(document).find("#dstnUsrs .custom-search-input input").keyup(function(e){
    if(($("#dstnUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
$(document).find("#mapdUsrs .custom-search-input input").keydown(function(e){
    if(($("#mapdUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
$(document).find("#mapdUsrs .custom-search-input input").keypress(function(e){
    if(($("#mapdUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
$(document).find("#mapdUsrs .custom-search-input input").keyup(function(e){
    if(($("#mapdUsrs .custom-search-input input").val().length == 0) && (e.key == "Enter")){
        $("#CFShowLoading").css("display","none");
        e.preventDefault();
        return false;
    }
});
//Long files name option when onedrive business  as destination
$(document).on('click','.fa-plus.longFileName',function () {
    $(this).removeClass('fa-plus').addClass('fa-minus');
    var _options = '<tr class="minusclicked"><td><div id="radioBtnVal"><input type="radio" name="longFileRadioBtn" style="margin-left: 8%;margin-top: 0%;" id="root">&nbspRootLevel<input type="radio" name="longFileRadioBtn" style="margin-left: 6%;margin-top: 0%;" id="parent">&nbspInnerLevel</div></td></tr>';
    $(".expandDsteOptionsHide table tbody").append(_options);
    $(".longFileName").css('display','');
});
$(document).on('click','.fa-minus.longFileName',function () {
    $(this).addClass('fa-plus').removeClass('fa-minus');
});
function dataList(thisevent, pgno,nxtToken,email){
    var triggeredclass = $(thisevent).attr('id');
    var _cloudId = $(thisevent).attr('user-id');
    var _folderId = encodeURIComponent($(thisevent).attr('folder-id'));
    var _userId = localStorage.getItem("UserId");


    var nxtTokenid = encodeURIComponent(nxtToken);//$(thisevent).attr('nxtpgtoken')
    //var prevTokenid = encodeURIComponent($(thisevent).parent(".fldr_parent1").prev().find('.fa-chevron-left').attr('prevPgToken'));
    var  pgNo = pgno;
    var  pgSize = 20;
    var apiurl = apicallurl + "/filefolder/userId/"+_userId+"/cloudId/"+_cloudId+"?folderId="+_folderId+"&page_nbr="+pgNo+"&page_size="+pgSize;
    $.ajax({
        type: "GET",
        url: apiurl,
        //async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Content-Type":"application/json",
        },
        success: function (data) {
            console.log(data);
            /*  if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs')) {
                  $(thisevent).parents('#srcUsrs').find('.usr-data').removeClass('scrollActive');
              }
              else{
                  $(thisevent).parents('#dstnUsrs').find('.usr-data').removeClass('scrollActive');

              }*/
            if(data.length == 0){
                $(thisevent).parent('.usr-data').parent().siblings().siblings().siblings().find('span#folderLoading').css("display","none");
                $(thisevent).parent('.usr-data').parent().siblings().siblings().siblings().find('span#textSpan1').css("display","block").css("margin-left","12%").css("margin-top","-7%");

            }
            if(data.length === 0){
                _html = '<div class="fldr_parent1" nxtpgtoken="' + null + '" pgno="1">';
            }
            else
                _html = '<div class="fldr_parent1" nxtpgtoken="'+data[0].nextPageToken+'" pgno="1">';
            for(var i=0;i<data.length;i++){
                var createdTime = CFManageCloudAccountsAjaxCall.getDateConversion(data[i].createdTime);
                var folderSize = CFManageCloudAccountsAjaxCall.getObjectSize(data[i].folderContentSize);
                var _foldrName = data[i].objectName.length;
                var foldrHover = data[i].objectName;
                if(folderSize === "___"){
                    folderSize =0;
                }
                var titleBar = 'Size : '+folderSize+'&#13Date : '+createdTime;
                if(data[i].cloudName == 'SHAREPOINT_ONLINE_BUSINESS' && data[i].type == 'SITE')
                {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs'))
                        _html += '<p style="display:block;margin-left:40px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+':true" fromCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" type="'+ data[i].type +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="checkbox"   name="firstInnChkBox" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" class="folderChkBox" /><i class="pull-left LVSITE" style="height: 15px;width: 15px;position: relative;left: 45px;"></i><span title="'+titleBar+'" style="font-size:initial;margin-left: 12px;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                    else
                        _html += '<p style="display:block;margin-left:40px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+':true" toCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn" /><i class="pull-left LVSITE" style="height: 15px;width: 15px;position: relative;left: 45px;"></i><span title="'+titleBar+'" style="font-size:initial;margin-left: 12px;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                }
                else if(data[i].cloudName == 'SHAREPOINT_ONLINE_BUSINESS' && data[i].type == 'FOLDER') {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs'))
                        _html += '<p style="display:block;margin-left:22px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="checkbox"  name="firstInnChkBox" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" class="folderChkBox"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span title="'+titleBar+'" style="font-size:initial;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                    else
                        _html += '<p style="display:block;margin-left:22px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+'" toCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio"  parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span title="'+titleBar+'" style="font-size:initial;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                }
                else if(data[i].type == 'FOLDER') {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs')){
                        var _obj ={
                            "id":data[i].id,
                            "cloudId":data[i].cloudId,
                        };
                        if(fldrStorage(_obj,'check1')){
                            var input = '<input type="checkbox"  name="firstInnChkBox" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" class="folderChkBox"  checked="checked"/>';
                        }
                        else{
                            var input = '<input type="checkbox"  name="firstInnChkBox" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" class="folderChkBox"/>';
                        }
                        if(data[i].cloudName === "DROPBOX_BUSINESS"){
                            if(_foldrName >= 23) {
                                if(i === 14)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;padding-left: 7px;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;padding-left: 7px;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 14)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="'+data[i].nextPageToken+'"><span  name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;padding-left: 7px;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="'+data[i].nextPageToken+'"><span  name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;padding-left: 7px;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                        }
                        else{
                            if(_foldrName >= 23) {
                                if(i === 14)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 14)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';

                        }

                    }
                    else {
                        if (data[i].cloudName === "DROPBOX_BUSINESS") {
                            if(_foldrName >= 23) {
                                if(i === 14)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" title="' + foldrHover + '" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;padding-left: 7px;top: 65%;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;"><span style="position: absolute;">Loading...</span></span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" title="' + foldrHover + '" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;padding-left: 7px;top: 65%;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;"><span style="position: absolute;">Loading...</span></span></span></p>';
                            }
                            else
                            if(i === 14)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;padding-left: 7px;top: 65%;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;"><span style="position: absolute;">Loading...</span></span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;padding-left: 7px;top: 65%;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;"><span style="position: absolute;">Loading...</span></span></span></p>';
                        } else {
                            if(_foldrName >= 23) {
                                if(i === 14)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" title="' + foldrHover + '" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" title="' + foldrHover + '" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 14)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                        }
                    }
                }
                else
                    _html += '<p style="cursor:default;display:block;width:100%;margin:5px 0px 5px 45px;"><span user-id="'+data[i].cloudId+'" toCloudId="'+data[i].cloudId+'" folder-id="'+data[i].id+'"></span><img src="../images/icons/file.png" style="height: 18px;width: 18px;margin-left: 10px;margin-right: -3px;margin-top: -2px;"><span style="font-size:initial;" title="'+data[i].objectName+'">&nbsp;&nbsp;'+CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23)+'</span></p>';
            }
            _html += '</div>';
            if($(thisevent).hasClass('fa-angle-up')|| triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") {
                if ((triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") && (data[0].nextPageToken == "" || data.length < 20)) {
                    $(thisevent).find('.active').append(_html);
                    $(thisevent).find('.active').addClass('ended');
                } else if (triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") {
                    $(thisevent).find('.active').append(_html);
                } else {
                    if (data.length < 20)
                        $(thisevent).parents('.fldr_parent').addClass('ended');
                    $(thisevent).parents('.fldr_parent').append(_html);
                }
                $(thisevent).parent('.usr-data').parent().siblings().siblings().siblings().find('span#folderLoading').css("display", "none");
                $(thisevent).parents('.fldr_parent').css('padding-bottom','0%');
                $(thisevent).parent('.usr-data').parent().siblings().siblings().siblings().find('span#textSpan').css("display", "");
                if ($(thisevent).find('.active').children('.usr-data').siblings().find('input[name="srccheckBox"]:checked').length) {
                    $(thisevent).find('.active').children('.fldr_parent1').find('input').prop("checked", true).attr('disabled', true);
                }
                if ($(thisevent).parent().siblings().find('input[name="srccheckBox"]:checked').length) {
                    $(thisevent).parents('.fldr_parent').children('.fldr_parent1').find('input').prop("checked", true).attr('disabled', true);
                }
            }
        }
    });
    $(thisevent).parent('.usr-data').addClass('haschild');

}
function scrollDataList(thisevent, pgno,nxtToken,email){
    var triggeredclass = $(thisevent).attr('id');
    var  pgNo = pgno;
    var  pgSize = 20;
    var _userId = localStorage.getItem("UserId");
    var cloudname = $(thisevent).find('.clickeddiv').parents('.fldr_parent').find('span').attr('cloudname');
    var _cloudId = $(thisevent).find('.active .clickeddiv').attr('user-id');
    var apiurl = apicallurl + "/filefolder/userId/" + _userId + "/cloudId/" + _cloudId + "?page_nbr="+pgNo+"&page_size=" + pgSize + "&nextPreviousId=" + nxtToken;
    $.ajax({
        type: "GET",
        url: apiurl,
        async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Content-Type":"application/json",
        },
        success: function (data) {
            console.log(data);
            if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs')) {
                $(thisevent).parents('#srcUsrs').find('.usr-data').removeClass('scrollActive');
            }
            else{
                $(thisevent).parents('#dstnUsrs').find('.usr-data').removeClass('scrollActive');

            }
            if(data.length == 0){
                $(thisevent).parent('.usr-data').parent().siblings().siblings().siblings().find('span#folderLoading').css("display","none");
                $(thisevent).parent('.usr-data').parent().siblings().siblings().siblings().find('span#textSpan1').css("display","block").css("margin-left","12%").css("margin-top","-7%");

            }
            if(data.length === 0){
                _html = '<div class="fldr_parent1" nxtpgtoken="' + null + '" pgno="1">';
            }
            else
                _html = '<div class="fldr_parent1" nxtpgtoken="'+data[0].nextPageToken+'" pgno="1">';
            for(var i=0;i<data.length;i++){
                var createdTime = CFManageCloudAccountsAjaxCall.getDateConversion(data[i].createdTime);
                var folderSize = CFManageCloudAccountsAjaxCall.getObjectSize(data[i].folderContentSize);
                var _foldrName = data[i].objectName.length;
                var foldrHover = data[i].objectName;
                if(folderSize === "___"){
                    folderSize =0;
                }
                var titleBar = 'Size : '+folderSize+'&#13Date : '+createdTime;
                if(data[i].cloudName == 'SHAREPOINT_ONLINE_BUSINESS' && data[i].type == 'SITE')
                {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs'))
                        _html += '<p style="display:block;margin-left:40px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+':true" fromCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" type="'+ data[i].type +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="checkbox"   name="firstInnChkBox" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" class="folderChkBox" /><i class="pull-left LVSITE" style="height: 15px;width: 15px;position: relative;left: 45px;"></i><span title="'+titleBar+'" style="font-size:initial;margin-left: 12px;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                    else
                        _html += '<p style="display:block;margin-left:40px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+':true" toCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn" /><i class="pull-left LVSITE" style="height: 15px;width: 15px;position: relative;left: 45px;"></i><span title="'+titleBar+'" style="font-size:initial;margin-left: 12px;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                }
                else if(data[i].cloudName == 'SHAREPOINT_ONLINE_BUSINESS' && data[i].type == 'FOLDER') {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs'))
                        _html += '<p style="display:block;margin-left:22px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="checkbox"  name="firstInnChkBox" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" class="folderChkBox"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span title="'+titleBar+'" style="font-size:initial;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                    else
                        _html += '<p style="display:block;margin-left:22px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+'" toCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio"  parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span title="'+titleBar+'" style="font-size:initial;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                }
                else if(data[i].type == 'FOLDER') {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs')){
                        var _obj ={
                            "id":data[i].id,
                            "cloudId":data[i].cloudId,
                        };
                        if(fldrStorage(_obj,'check1')){
                            var input = '<input type="checkbox"  name="firstInnChkBox" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" class="folderChkBox"   checked="checked"/>';
                        }
                        else{
                            var input = '<input type="checkbox"  name="firstInnChkBox" parentPath="/" folderPath="/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" class="folderChkBox"/>';
                        }
                        if(data[i].cloudName === "DROPBOX_BUSINESS"){
                            if(_foldrName >= 23) {
                                if(i === 14)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;padding-left: 7px;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;padding-left: 7px;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 14)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="'+data[i].nextPageToken+'"><span  name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;padding-left: 7px;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="'+data[i].nextPageToken+'"><span  name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;padding-left: 7px;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                        }
                        else{
                            if(_foldrName >= 23) {
                                if(i === 14)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 14)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="'+data[i].nextPageToken+'"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';

                        }

                    }
                    else {
                        if (data[i].cloudName === "DROPBOX_BUSINESS") {
                            if(_foldrName >= 23) {
                                if(i === 14)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" title="' + foldrHover + '" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;padding-left: 7px;top: 65%;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;"><span style="position: absolute;">Loading...</span></span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" title="' + foldrHover + '" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;padding-left: 7px;top: 65%;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;"><span style="position: absolute;">Loading...</span></span></span></p>';
                            }
                            else
                            if(i === 14)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;padding-left: 7px;top: 65%;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;"><span style="position: absolute;">Loading...</span></span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;padding-left: 7px;top: 65%;left: 11%;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;"><span style="position: absolute;">Loading...</span></span></span></p>';
                        } else {
                            if(_foldrName >= 23) {
                                if(i === 14)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" title="' + foldrHover + '" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan" title="' + foldrHover + '" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 14)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data" nextPageToken="' + data[i].nextPageToken + '"><span name="'+data[i].cloudName +'" user-id="' + data[i].cloudId + '" toCloudId="' + data[i].cloudId + '" folder-id="' + data[i].id + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="/" folderPath="/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 7px;margin-right: -3px;margin-top: -2px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName, 23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                        }
                    }
                }
                else
                    _html += '<p style="cursor:default;display:block;width:100%;margin:5px 0px 5px 45px;"><span user-id="'+data[i].cloudId+'" toCloudId="'+data[i].cloudId+'" folder-id="'+data[i].id+'"></span><img src="../images/icons/file.png" style="height: 18px;width: 18px;margin-left: 10px;margin-right: -3px;margin-top: -2px;"><span style="font-size:initial;" title="'+data[i].objectName+'">&nbsp;&nbsp;'+CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23)+'</span></p>';
            }
            _html += '</div>';
            if($(thisevent).hasClass('fa-angle-up')||triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") {
                if ((triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") && (data[0].nextPageToken == "" || data.length < 20)) {
                    $(thisevent).find('.active').append(_html);
                    $(thisevent).find('.active').addClass('ended');
                } else if (triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") {
                    $(thisevent).find('.active').append(_html);
                } else {
                    if (data.length < 20)
                        $(thisevent).parents('.fldr_parent').addClass('ended');
                    $(thisevent).parents('.fldr_parent').append(_html);
                }
                $(thisevent).parent('.usr-data').parent().siblings().siblings().siblings().find('span#folderLoading').css("display", "none");
                $(thisevent).parents('.fldr_parent').css('padding-bottom','0%');
                $(thisevent).parent('.usr-data').parent().siblings().siblings().siblings().find('span#textSpan').css("display", "");
                if ($(thisevent).find('.active').children('.usr-data').siblings().find('input[name="srccheckBox"]:checked').length) {
                    $(thisevent).find('.active').children('.fldr_parent1').find('input[type="checkbox"]').prop("checked", true).attr('disabled', true);
                }
                if ($(thisevent).parent().siblings().find('input[name="srccheckBox"]:checked').length) {
                    $(thisevent).parents('.fldr_parent').children('.fldr_parent1').find('input[type="checkbox"]').prop("checked", true).attr('disabled', true);
                }
            }
        }
    });
    $(thisevent).parent('.usr-data').addClass('haschild');

}

$(document).on('click','#dstnFolderScroll .forDomainNameMin .fa-angle-down.usr-folder',function(){
    $('#dstnFolderScroll .fldr_parent').find('span#folderLoading').css("display","none");
    $(this).parents('.fldr_parent').css('padding-bottom','0%');
    $('#dstnFolderScroll .fldr_parent').removeClass('active ended');
    $('#dstnFolderScroll .fldr_parent').find('> .fldr_parent1').html('');
    $('#dstnFolderScroll .fldr_parent').find('.usr-folder').removeClass('fa-angle-up').addClass('fa-angle-down');
    $('#dstnFolderScroll .fldr_parent').parents('.className').find('.active').removeClass('active');
    $('#dstnFolderScroll .fldr_parent').find('.clickeddiv').removeClass('clickeddiv');
    $(this).parents('.fldr_parent').addClass('active');
    $(this).addClass('clickeddiv');
    //$(this).parents('.fldr_parent.active').find('span').css('display','none');
    $(this).parents('.fldr_parent.active').find('span#folderLoading').css("display","");
    $(this).parents('.fldr_parent').css('padding-bottom','10%');
    var pgno = $(this).parents('.fldr_parent').attr('pgno');
    var email = $(this).parents('.fldr_parent.active').find('.usr-folder').attr('useremail');
    /* if(!$(this).parent('.usr-data').hasClass('haschild')) {
         dataList(this, pgno,'',email);
     }
     else{
         $(this).parents('.fldr_parent.active').find('span').css('display','').css("margin-left","2%");
         $(this).parents('.fldr_parent.active').find('span#folderLoading').css("display","none");
     }*/
    dataList(this, pgno,'',email);
    // $(this).parents('.fldr_parent').nextAll().hide();
    $('.pagination.p1').show();
    // $(this).parents('.fldr_parent').find('> .fldr_parent1').show();
    if($('#srcCloudUsers input[name="srccheckBox"]:checked').length){
        $(this).parents('.fldr_parent').children('.fldr_parent1').find('input[name="firstInnChkBox"]').prop( "checked", true).attr('disabled',true);
        $(this).parents('.fldr_parent').children('.fldr_parent1').find('input[name="folderSubChkBox"]').prop( "checked", true).attr('disabled',true);
    }
    else{
        $(this).parents('.fldr_parent').children('.fldr_parent1').find('input[name="firstInnChkBox"]').prop( "checked", false).attr('disabled',false);
        $(this).parents('.fldr_parent').children('.fldr_parent1').find('input[name="folderSubChkBox"]').prop( "checked", false).attr('disabled',false);
    }
    $(this).removeClass('fa-angle-down').addClass('fa-angle-up');
    $(this).parents('.fldr_parent').find('.fldr_parent1 > .usr-data').show();
    flagclicked = '1';
    return false;
});
$(document).on('click','#dstnFolderScroll .forDomainNameMin .fa-angle-up.usr-folder',function() {
    $(this).parent('.usr-data').parent().siblings().siblings().siblings().find('span#folderLoading').css("display","none");
    $(this).parents('.fldr_parent').css('padding-bottom','0%');
    $(this).parents('.fldr_parent').find('.fldr_parent1').remove();
    $(this).parents('.fldr_parent').removeClass('active');
    $(this).parents('.className').find('.active').removeClass('active');
    $(this).parents('.fldr_parent').find('.clickeddiv').removeClass('clickeddiv');
    $(this).removeClass('clickeddiv');
    $(this).parents('.fldr_parent').find('.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');
    $(this).parents('.fldr_parent').find('.fa-caret-right').css('color','gray');
    $(this).removeClass('fa-angle-up').addClass('fa-angle-down');
    $(this).parents('.fldr_parent').siblings().show();
    return false;
});
$(document).on('click','#srcFolderScroll .forDomainNameMin .fa-angle-down.usr-folder',function(){
    $('#srcFolderScroll .fldr_parent').find('span#folderLoading').css("display","none");
    $(this).parents('.fldr_parent').css('padding-bottom','0%');
    $('#srcFolderScroll .fldr_parent').removeClass('active ended');
    $('#srcFolderScroll .fldr_parent').find('> .fldr_parent1').remove();
    $('#srcFolderScroll .fldr_parent').find('.usr-folder').removeClass('fa-angle-up').addClass('fa-angle-down');
    $('#srcFolderScroll .fldr_parent').parents('.className').find('.active').removeClass('active');
    $('#srcFolderScroll .fldr_parent').find('.clickeddiv').removeClass('clickeddiv');
    $(this).parents('.fldr_parent').addClass('active');
    $(this).addClass('clickeddiv');
    //$(this).parents('.fldr_parent.active').find('span').css('display','none');
    $(this).parents('.fldr_parent.active').find('span#folderLoading').css("display","");
    $(this).parents('.fldr_parent.active').css('padding-bottom','10%');
    var pgno = $(this).parents('.fldr_parent').attr('pgno');
    var email = $(this).parents('.fldr_parent.active').find('.usr-folder').attr('useremail');
    /* if(!$(this).parent('.usr-data').hasClass('haschild')) {
         dataList(this, pgno,'',email);
     }
     else{
         $(this).parents('.fldr_parent.active').find('span').css('display','').css("margin-left","3%");
         $(this).parents('.fldr_parent.active').find('span#folderLoading').css("display","none");
     }*/
    dataList(this, pgno,'',email);
    // $(this).parents('.fldr_parent').nextAll().hide();
    $('.pagination.p1').show();
    // $(this).parents('.fldr_parent').find('> .fldr_parent1').show();

    $(this).removeClass('fa-angle-down').addClass('fa-angle-up');
    $(this).parents('.fldr_parent').find('.fldr_parent1 > .usr-data').show();
    flagclicked = '1';
    return false;
});
$(document).on('click','#srcFolderScroll .forDomainNameMin .fa-angle-up.usr-folder',function() {
    $(this).parent('.usr-data').parent().siblings().siblings().siblings().find('span#folderLoading').css("display","none");
    $(this).parents('.fldr_parent').css('padding-bottom','0%');
    $(this).parents('.fldr_parent').find('.fldr_parent1').remove();
    $(this).parents('.fldr_parent').removeClass('active');
    $(this).parents('.fldr_parent').removeClass('opened');
    $(this).parents('.className').find('.active').removeClass('active');
    $(this).parents('.fldr_parent').find('.clickeddiv').removeClass('clickeddiv');
    $(this).removeClass('clickeddiv');
    $(this).parents('.fldr_parent').find('.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');
    $(this).parents('.fldr_parent').find('.fa-caret-right').css('color','gray');
    $(this).removeClass('fa-angle-up').addClass('fa-angle-down');
    $(this).parents('.fldr_parent').siblings().show();
    return false;
});
$('#srcFolderScroll,#dstnFolderScroll').on('scroll', function() {
    if ($('#srcFolderScroll').find('.scrollActive').length ||$('#dstnFolderScroll').find('.scrollActive').length) {
        if ($(this).attr('id') === "srcFolderScroll") {
            var position = $('#srcFolderScroll').find('.scrollActive').position().top;
        } else {
            var position = $('#dstnFolderScroll').find('.scrollActive').position().top;
        }
    }

    if ( $(this).scrollTop() + $(this).innerHeight()  > position){
        /* if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight-$(this)[0].scrollHeight/2) {*/
        if(flagclicked == '1') {
            flagclicked = '0';
            return false;
        }
        var pgno = parseInt($(this).find('.active').attr('pgno'));
        var email = $(this).find('.clickeddiv').siblings('input').attr('usremail');//$(this).parents('.fldr_parent').find('.usr-folder').attr('useremail');
        var folderPath = $(this).find('.clickeddiv').siblings('input').attr('folderpath');
        pgno += 1;
        $(this).find(".active").attr('pgno', pgno);
        if ($(this).find('.clickeddiv').parents('.fldr_parent').hasClass('active')) {
            var nxtToken = encodeURIComponent($(this).find('.clickeddiv').parents('.fldr_parent').children('.fldr_parent1:last').attr('nxtpgtoken'));
        } else {
            var nxtToken = encodeURIComponent($(this).find('.clickeddiv').siblings('.fldr_parent1:last').attr('nxtpgtoken'));
        }
        if(nxtToken === "null" || nxtToken === "" || nxtToken === undefined || nxtToken === "undefined"){
            return false;
        }
        else {
            if($(this).find('.clickeddiv').parents('.fldr_parent').hasClass('active')) {
                var email = $(this).find('.clickeddiv').attr('useremail');
                scrollDataList(this, pgno, nxtToken, email);
            }
            else {
                scrollSubDataList(this, pgno, email, folderPath,nxtToken);

            }
        }

        if ($('#srcCloudUsers input[name="srccheckBox"]:checked').length) {
            $('.fldr_parent').children('.fldr_parent1.active').find('input[type="checkbox"]').prop("checked", true).attr('disabled', true);
        }

        if ($(this).find('.clickeddiv').siblings('input[type="checkbox"]').is(':checked')) {
            $(this).find('.clickeddiv').siblings('.fldr_parent1').find('input[type="checkbox"]').prop("checked", true).attr('disabled', true);
        }
        return false;
    }
});

function subDataList(thisevent, pgno,email,folderPath)
{
    var triggeredclass = $(thisevent).attr('id');
    var _cloudId = $(thisevent).parents('.mapp-blck').find('.clickeddiv').attr('user-id');
    var _folderId = encodeURIComponent($(thisevent).parents('.mapp-blck').find('.clickeddiv').attr('folder-id'));
    var _parentId = encodeURIComponent($(thisevent).parents('.mapp-blck').find('.clickeddiv').attr('parentid'));
    var _userId = localStorage.getItem("UserId");
    var  pgNo = pgno;
    var  pgSize = 10;
    var apiurl = apicallurl + "/filefolder/userId/"+_userId+"/cloudId/"+_cloudId+"?folderId="+_folderId+"&parentId="+_parentId+"&pageNo="+pgNo+"&page_size="+pgSize;
    $.ajax({
        type: "GET",
        url: apiurl,
        //async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Content-Type":"application/json",
        },
        success: function (data) {
            console.log(data);
            /*     if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs')) {
                     $(thisevent).parents('#srcUsrs').find('.usr-data').removeClass('scrollActive');
                 }
                 else{
                     $(thisevent).parents('#dstnUsrs').find('.usr-data').removeClass('scrollActive');

                 }*/
            if (data.length == 0) {
                $(thisevent).siblings('#folderLoading').css('display', 'none');
                $(thisevent).siblings('.fldr_parent1').remove();
                $(thisevent).siblings("#textSpan").css('display', '');
            }
            if(data.length === 0){
                _html = '<div class="fldr_parent1" nxtpgtoken="' + null + '" pgno="1">';
            }
            else
                $(thisevent).siblings('.fldr_parent1').remove();
            _html = '<div class="fldr_parent1" nxtpgtoken="' + data[0].nextPageToken + '" pgno="1">';
            for (var i = 0; i < data.length; i++) {
                var createdTime = CFManageCloudAccountsAjaxCall.getDateConversion(data[i].createdTime);
                var folderSize = CFManageCloudAccountsAjaxCall.getObjectSize(data[i].folderContentSize);
                var _foldrName = data[i].objectName.length;
                var foldrHover = data[i].objectName;
                if(folderSize === "___"){
                    folderSize =0;
                }
                var titleBar = 'Size : '+folderSize+'&#13Date : '+createdTime;
                if(data[i].cloudName == 'SHAREPOINT_ONLINE_BUSINESS' && data[i].type == 'SITE')
                {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs'))
                        _html += '<p style="display:block;margin-left:10px;width:100%;" class="usr-data"><span user-id="' + data[i].cloudId + ':true" fromCloudId="' + data[i].cloudId + '" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;font-size: 20px;color: gray;"></span><input type="checkbox" name="folderSubChkBox" parentPath="' + folderPath + '" folderPath="' + folderPath + '/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" class="folderChkBox" /><i class="pull-left LVSITE" style="height: 15px;width: 15px;position: relative;left: 45px;"></i>><span title="' + titleBar + '" style="font-size:initial;margin-left: 12px;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                    else
                        _html += '<p style="display:block;margin-left:10px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+':true"  fromCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;font-size: 20px;color: gray;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn" /><i class="pull-left LVSITE" style="height: 15px;width: 15px;position: relative;left: 45px;"></i><span title="'+titleBar+'" style="font-size:initial;margin-left: 12px;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                }
                else if(data[i].cloudName == 'SHAREPOINT_ONLINE_BUSINESS' && data[i].type == 'FOLDER') {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs'))
                        _html += '<p style="display:block;margin-left:22px;width:100%;" class="usr-data"><span user-id="' + data[i].cloudId + '" fromCloudId="' + data[i].cloudId + '" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;font-size: 20px;color: gray;"></span><input type="checkbox" name="folderSubChkBox" parentPath="' + folderPath + '" folderPath="' + folderPath + '/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderSubChkBox"/><img src="../images/icons/Folder.png" style="height: 15px;width: 15px;margin-left: 3px;margin-right: -5px;position:relative;bottom:1px;"><span title="' + titleBar + '" style="font-size:initial;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                    else
                        _html += '<p style="display:block;margin-left:22px;width:100%;" class="usr-data"><span user-id="' + data[i].cloudId + '" fromCloudId="' + data[i].cloudId + '" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;font-size: 20px;color: gray;"></span><input type="radio" parentPath="' + folderPath + '" folderPath="' + folderPath + '/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/Folder.png" style="height: 15px;width: 15px;margin-left: 3px;margin-right: -5px;position:relative;bottom:1px;"><span title="' + titleBar + '" style="font-size:initial;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                }
                else if(data[i].type == 'FOLDER') {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs')){
                        var _obj ={
                            "id":data[i].id,
                            "cloudId":data[i].cloudId,
                        };
                        if(fldrStorage(_obj,'check1')){
                            var input = '<input type="checkbox" name="folderSubChkBox" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderSubChkBox"  checked="true"/>';
                        }
                        else{
                            var input = '<input type="checkbox" name="folderSubChkBox" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderSubChkBox"/>';
                        }
                        if(data[i].cloudName === "DROPBOX_BUSINESS"){
                            if(_foldrName >= 23) {
                                if(i === 9)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 9)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                        }
                        else{
                            if(_foldrName >= 23) {
                                if(i === 9)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 9)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';

                        }
                    }
                    else{
                        if(data[i].cloudName === "DROPBOX_BUSINESS"){
                            if(_foldrName >= 23) {
                                if(i === 9)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  title="'+foldrHover+'"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  title="'+foldrHover+'"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 9)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                        }
                        else{
                            if(_foldrName >= 23) {
                                if(i === 9)
                                    _html += '<p style="display:block;margin-left:22px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';

                            }
                            else
                            if(i === 9)
                                _html += '<p style="display:block;margin-left:22px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';

                        }
                    }
                }
                else
                    _html += '<p style="display:block;margin:5px 0px 5px 10px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data"><span name="'+data[i].cloudName +'" fromCloudId="'+data[i].cloudId+'" folder-id="'+data[i].id+'" style="cursor:pointer"></span><img src="../images/icons/file.png" style="height: 18px;width: 18px;margin-left: 10px;margin-right: -3px;"><span style="font-size:initial;" title="'+data[i].objectName+'">&nbsp;&nbsp;'+CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23)+'</span></p>';
            }
            _html += '</div>';
            if ($(thisevent).hasClass('fa-caret-down')|| triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") {
                if ((triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") && (data[0].nextPageToken == "" || data.length < 10)) {
                    $(thisevent).find('.clickeddiv').parent('.usr-data').append(_html);
                    $(thisevent).find('.active').addClass('ended');
                } else if (triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") {
                    $(thisevent).find('.clickeddiv').parent('.usr-data').append(_html);
                } else {
                    if (data.length < 10) {
                        $(thisevent).parents('.fldr_parent').addClass('ended');
                        $(thisevent).parents('.fldr_parent1').css('display', '')
                    }

                    $(thisevent).parent('.usr-data').append(_html);
                }
                $(thisevent).siblings('#folderLoading').css('display', 'none');
                //  $(thisevent).siblings("#textSpan").css("display","");
                if ($('#srcCloudUsers input[name="srccheckBox"]:checked').length) {
                    $(thisevent).parents('.fldr_parent').children('.clickeddiv').siblings('input[name="firstInnChkBox"]').prop("checked", true).attr('disabled', true);
                    $(thisevent).parents('.fldr_parent').children('.clickeddiv').siblings('input[name="folderSubChkBox"]').prop("checked", true).attr('disabled', true);
                }

                if ($('#srcCloudUsers input[name="firstInnChkBox"]:checked').length) {
                    $(thisevent).parents('.fldr_parent1').children('.clickeddiv').siblings('input[name="folderSubChkBox"]').prop("checked", true).attr('disabled', true);
                }

                if ($(thisevent).siblings('input[type="checkbox"]').is(':checked')) {
                    $(thisevent).siblings('.fldr_parent1').find('input[type="checkbox"]').prop("checked", true).attr('disabled', true);
                }
            }
        }
    });
    $(thisevent).parent('.usr-data').addClass('haschild');
}
function scrollSubDataList(thisevent, pgno,email,folderPath,nxtToken)
{
    var triggeredclass = $(thisevent).attr('id');
    var _cloudId = $(thisevent).parents('.mapp-blck').find('.clickeddiv').attr('user-id');
    var _folderId = encodeURIComponent($(thisevent).parents('.mapp-blck').find('.clickeddiv').attr('folder-id'));
    var _parentId = encodeURIComponent($(thisevent).parents('.mapp-blck').find('.clickeddiv').attr('parentid'));
    var _userId = localStorage.getItem("UserId");
    var  pgNo = pgno;
    var  pgSize = 10;
    var apiurl = apicallurl + "/filefolder/userId/" + _userId + "/cloudId/" + _cloudId + "?pageNo="+pgNo+"&page_size=" + pgSize + "&nextPreviousId=" + nxtToken;
    $.ajax({
        type: "GET",
        url: apiurl,//apicallurl + "/getfiles/files/"+_cloudId+"?" + "folderId="+_folderId+"&pageNo=1&pageSize=5",
        async: false,
        headers: {
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Content-Type":"application/json",
        },
        success: function (data) {
            console.log(data);
            if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs')) {
                $(thisevent).parents('#srcUsrs').find('.usr-data').removeClass('scrollActive');
            }
            else{
                $(thisevent).parents('#dstnUsrs').find('.usr-data').removeClass('scrollActive');

            }
            if (data.length == 0) {
                $(thisevent).siblings('#folderLoading').css('display', 'none');
                $(thisevent).siblings("#textSpan").css('display', '');
            }
            if(data.length === 0){
                _html = '<div class="fldr_parent1" nxtpgtoken="' + null + '" pgno="1">';
            }
            else
                _html = '<div class="fldr_parent1" nxtpgtoken="' + data[0].nextPageToken + '" pgno="1">';
            for (var i = 0; i < data.length; i++) {
                var createdTime = CFManageCloudAccountsAjaxCall.getDateConversion(data[i].createdTime);
                var folderSize = CFManageCloudAccountsAjaxCall.getObjectSize(data[i].folderContentSize);
                var _foldrName = data[i].objectName.length;
                var foldrHover = data[i].objectName;
                if(folderSize === "___"){
                    folderSize =0;
                }
                var titleBar = 'Size : '+folderSize+'&#13Date : '+createdTime;
                if(data[i].cloudName == 'SHAREPOINT_ONLINE_BUSINESS' && data[i].type == 'SITE')
                {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs'))
                        _html += '<p style="display:block;margin-left:10px;width:100%;" class="usr-data"><span user-id="' + data[i].cloudId + ':true" fromCloudId="' + data[i].cloudId + '" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;font-size: 20px;color: gray;"></span><input type="checkbox" name="folderSubChkBox" parentPath="' + folderPath + '" folderPath="' + folderPath + '/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" class="folderChkBox" /><i class="pull-left LVSITE" style="height: 15px;width: 15px;position: relative;left: 45px;"></i>><span title="' + titleBar + '" style="font-size:initial;margin-left: 12px;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                    else
                        _html += '<p style="display:block;margin-left:10px;width:100%;" class="usr-data"><span user-id="'+data[i].cloudId+':true"  fromCloudId="'+data[i].cloudId+'" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;font-size: 20px;color: gray;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn" /><i class="pull-left LVSITE" style="height: 15px;width: 15px;position: relative;left: 45px;"></i><span title="'+titleBar+'" style="font-size:initial;margin-left: 12px;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                }
                else if(data[i].cloudName == 'SHAREPOINT_ONLINE_BUSINESS' && data[i].type == 'FOLDER') {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs'))
                        _html += '<p style="display:block;margin-left:22px;width:100%;" class="usr-data"><span user-id="' + data[i].cloudId + '" fromCloudId="' + data[i].cloudId + '" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;font-size: 20px;color: gray;"></span><input type="checkbox" name="folderSubChkBox" parentPath="' + folderPath + '" folderPath="' + folderPath + '/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderSubChkBox"/><img src="../images/icons/Folder.png" style="height: 15px;width: 15px;margin-left: 3px;margin-right: -5px;position:relative;bottom:1px;"><span title="' + titleBar + '" style="font-size:initial;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                    else
                        _html += '<p style="display:block;margin-left:22px;width:100%;" class="usr-data"><span user-id="' + data[i].cloudId + '" fromCloudId="' + data[i].cloudId + '" _folderid="' + data[i].id.split("/")[1] + '" folder-id="' + data[i].id.split("/")[2] + '" parentid="' + data[i].parent + '" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;font-size: 20px;color: gray;"></span><input type="radio" parentPath="' + folderPath + '" folderPath="' + folderPath + '/' + data[i].objectName + '" cldname="' + data[i].cloudName + '"cloudid="' + data[i].cloudId + '" rtfolid="' + data[i].id + '"usremail="' + email + '" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/Folder.png" style="height: 15px;width: 15px;margin-left: 3px;margin-right: -5px;position:relative;bottom:1px;"><span title="' + titleBar + '" style="font-size:initial;">&nbsp;&nbsp;' + data[i].objectName + '</span></p>';
                }
                else if(data[i].type == 'FOLDER') {
                    if ($(thisevent).parents('.mapp-blck').hasClass('src-usrs')){
                        var _obj ={
                            "id":data[i].id,
                            "cloudId":data[i].cloudId,
                        };
                        if(fldrStorage(_obj,'check1')){
                            var input = '<input type="checkbox" name="folderSubChkBox" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderSubChkBox"  checked="true"/>';
                        }
                        else{
                            var input = '<input type="checkbox" name="folderSubChkBox" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderSubChkBox"/>';
                        }
                        if(data[i].cloudName === "DROPBOX_BUSINESS"){
                            if(_foldrName >= 23) {
                                if(i === 9)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i ===9)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                        }
                        else{
                            if(_foldrName >= 23) {
                                if(i === 9)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 9)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span>'+input+'<img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="top: 65%;left: 11%;position: absolute;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';

                        }
                    }
                    else{
                        if(data[i].cloudName === "DROPBOX_BUSINESS"){
                            if(_foldrName >= 23) {
                                if(i === 9)
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  title="'+foldrHover+'"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  title="'+foldrHover+'"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            }
                            else
                            if(i === 9)
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;position:relative;margin-bottom: 6%;width:100%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                        }
                        else{
                            if(_foldrName >= 23) {
                                if(i === 9)
                                    _html += '<p style="display:block;margin-left:22px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                                else
                                    _html += '<p style="display:block;margin-left:22px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan" title="'+foldrHover+'" style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';

                            }
                            else
                            if(i === 9)
                                _html += '<p style="display:block;margin-left:22px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data scrollActive"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';
                            else
                                _html += '<p style="display:block;margin-left:22px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data"><span name="'+data[i].cloudName +'" user-id="'+data[i].cloudId+'" fromCloudId="'+data[i].cloudId+'" folder-id="' + data[i].id + '" parentid="'+ data[i].parent +'" class="usr-folder1 fa fa-caret-right" style="cursor:pointer;margin-left: 5%;color: gray;font-size: 20px;"></span><input type="radio" parentPath="'+folderPath+'" folderPath="'+folderPath+'/'+ data[i].objectName +'" cldname="'+ data[i].cloudName +'"cloudid="'+ data[i].cloudId +'" rtfolid="'+ data[i].id +'"usremail="'+ email +'" style="margin:-2px 0 0 14px;" name="folderradiobtn"/><img src="../images/icons/folder.png" style="height: 15px;width: 15px;margin-left: 10px;margin-right: -3px;"><span id="textSpan"  style="font-size:initial;position: absolute;">&nbsp;&nbsp;' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23) + '</span><span id="folderLoading" style="position: absolute;top: 65%;left: 11%;padding-left: 7px;display: none;"><img  src="../img/ajax-loader11.gif" alt="img" ><span style="position: absolute;">Loading...</span></span></p>';

                        }
                    }
                }
                else
                    _html += '<p style="display:block;margin:5px 0px 5px 10px;width:100%;position:relative;margin-bottom: 6%;" class="usr-data"><span fromCloudId="'+data[i].cloudId+'" folder-id="'+data[i].id+'" style="cursor:pointer"></span><img src="../images/icons/file.png" style="height: 18px;width: 18px;margin-left: 10px;margin-right: -3px;"><span style="font-size:initial;" title="'+data[i].objectName+'">&nbsp;&nbsp;'+CFManageCloudAccountsAjaxCall.getMaxChars(data[i].objectName,23)+'</span></p>';
            }
            _html += '</div>';
            if ($(thisevent).hasClass('fa-caret-down')|| triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") {
                if ((triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") && (data[0].nextPageToken == "" || data.length < 10)) {
                    $(thisevent).find('.clickeddiv').parent('.usr-data').append(_html);
                    $(thisevent).find('.active').addClass('ended');
                } else if (triggeredclass == "srcFolderScroll" || triggeredclass == "dstnFolderScroll") {
                    $(thisevent).find('.clickeddiv').parent('.usr-data').append(_html);
                } else {
                    if (data.length < 10) {
                        $(thisevent).parents('.fldr_parent').addClass('ended');
                        $(thisevent).parents('.fldr_parent1').css('display', '')
                    }

                    $(thisevent).parent('.usr-data').append(_html);
                }
                $(thisevent).siblings('#folderLoading').css('display', 'none');
                //  $(thisevent).siblings("#textSpan").css("display","");
                if ($('#srcCloudUsers input[name="srccheckBox"]:checked').length) {
                    $('.fldr_parent').children('.fldr_parent1').find('input[name="firstInnChkBox"]').prop("checked", true).attr('disabled', true);
                    $('.fldr_parent').children('.fldr_parent1').find('input[name="folderSubChkBox"]').prop("checked", true).attr('disabled', true);
                }

                if ($('.fldr_parent').children('.fldr_parent1').children('.fileActive').children('input[type="checkbox"]').is(':checked')) {
                    $('.fldr_parent').children('.fldr_parent1').children('.fileActive').children('.fldr_parent1').find('input').prop("checked", true).attr('disabled', true);
                }

                if ($(thisevent).siblings('input[type="checkbox"]').is(':checked')) {
                    $(thisevent).siblings('.fldr_parent1').find('input[type="checkbox"]').prop("checked", true).attr('disabled', true);
                }
            }
        }
    });
    $(thisevent).parent('.usr-data').addClass('haschild');
}

$(document).on('click','.forDomainNameMin .fldr_parent1 .fa-caret-right',function(){
    $(this).siblings('.fldr_parent1').remove();
    $(this).parents('.className').find('.active').removeClass('active').addClass('opened');
    $(this).closest('.fldr_parent1').addClass('active');
    $(this).parents('.className').find('.clickeddiv').removeClass('clickeddiv').addClass('openeddiv');
    $(this).addClass('clickeddiv');
    //   $(this).siblings('span').css('display','none');
    $(this).siblings('#folderLoading').css('display','');
    $(this).css('margin-bottom', '10px');
    var pgno = $(this).closest('.fldr_parent1').attr('pgno');
    var folderPath = $(this).siblings('input').attr('folderPath');
    var email =$(this).parents('.fldr_parent').find('.usr-folder').attr('useremail');
    /*   if(!$(this).parent('.usr-data').hasClass('haschild')) {
           subDataList(this, pgno,email,folderPath);
       }
       else{
           $(this).siblings('span').css('display','');
           $(this).siblings('#folderLoading').css('display','none');
       }*/
    subDataList(this, pgno,email,folderPath);
    // $(this).parent().siblings().hide();
    // $(this).closest('.fldr_parent1').siblings('.fldr_parent1').hide();
    $(this).parent().find('> .fldr_parent1').remove();


    $(this).parent().find('> .fldr_parent1').find('> .usr-data').remove();
    $(this).removeClass('fa-caret-right').addClass('fa-caret-down');
    $(this).css('color','black');
    flagclicked = '1';
    return false;
});
$(document).on('click','.forDomainNameMin .fldr_parent1 .fa-caret-down',function() {
    $(this).closest('.fldr_parent1').find('.clickeddiv').siblings('#folderLoading').css('display','none');
    $(this).parent('.usr-data').find('.fldr_parent1').remove();
    $(this).parent('.usr-data').siblings('.usr-data:eq(7)').addClass('scrollActive');
    $(this).css('margin-bottom', '0');
    $(this).closest('.fldr_parent1').find('.usr-data > .fa').css('margin-bottom', '0');
    $(this).closest('.fldr_parent1').find('.opened').removeClass('opened');
    $(this).closest('.fldr_parent1').find('.active').removeClass('active');
    $(this).closest('.fldr_parent1').find('.openeddiv').removeClass('openeddiv');
    $(this).closest('.fldr_parent1').find('.clickeddiv').removeClass('clickeddiv');
    $(this).closest('.fldr_parent1').removeClass('active').removeClass('opened');
    $(this).parents('div').eq(1).find('> .usr-data > .openeddiv').removeClass('openeddiv').addClass('clickeddiv');
    $(this).parents('div').eq(1).removeClass('opened').addClass('active');
    $(this).removeClass('clickeddiv').removeClass('openeddiv');
    $(this).parent('.usr-data').find('.fa-caret-down').removeClass('fa-caret-down').addClass('fa-caret-right');
    $(this).removeClass('fa-caret-down').addClass('fa-caret-right');
    $(this).css('color','gray');
    $(this).parent('.usr-data').siblings().show();
    $(this).closest('.fldr_parent1').siblings('.fldr_parent1').show();
    return false;
});

//CSV file for uploading
$('#csvFileUpload').click(function() {
    document.getElementById("csvFile").value = "";
    $('#csvFile').click();
});
function fileUpload() {
    localStorage.removeItem("selectedMappings");
    localStorage.removeItem('selectedEmail');
    localStorage.removeItem('folderEmail');
    localStorage.removeItem('folderMappings');
    localStorage.removeItem("validator");
    localStorage.removeItem('csvMigrationData');
    if($("#mapdUsers tbody tr").length)
        deleteMapping(true);
    $("#forNextMove").addClass("disabled");
    localStorage.removeItem("csvName");
    var upload = document.getElementById('csvFile').files[0];
    localStorage.setItem("csvName",upload.name);
    $("#CFShowLoading").css("display","");
    $("#CFShowLoading").attr("autoMap","true");
    var reader = new FileReader();
    reader.onload = function() {
        sessionStorage.setItem('result',reader.result);
        sessionStorage.setItem('csvIteration',1);
        csvFileUpld(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"), reader.result);
    };
    reader.readAsText(upload);
};
function csvFileUpld(_srcMapdCldId,_dstnMapdCldId,_fileContent) {
    if($("#mapdUsers tbody tr").hasClass('CsvMapd'))
        deleteCsv();
    var pgNo,pgSize;
    if(pgNo == undefined)
        pgNo = 1;
    pgSize = 30;

    var _data = "sourceCloudId="+_srcMapdCldId+"&destCloudId="+_dstnMapdCldId+"&pageNo="+pgNo+"&pageSize="+pgSize;
    $("#CFShowLoading").css("display","");
    $.ajax({
        //async: false,
        type: "POST",
        data:_fileContent,
        //url: apicallurl + "/mapping/user/migration/csv?" + _data,
        url: apicallurl + "/mapping/user/path/csv?" + _data,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data, textStatus, xhr) {
            console.log(data);
            var headers = xhr.getAllResponseHeaders().toLowerCase().trim();
            var values = headers.split("\n");
            var count = [];
            for(var i = 0; i < values.length; i++) {
                var hrdValue = values[i].split(': ');
                count[hrdValue[0].trim()] = hrdValue[1].trim();
            }
            var dupLength = count["duplicatecount"];
            /*     for(var i=0;i<data.length;i++) {
                     var j=i+1;
                     for (var j; j < data.length; j++) {
                         if (data[i].sourceCloudDetails.emailId == data[j].sourceCloudDetails.emailId || data[i].destCloudDetails.emailId == data[j].destCloudDetails.emailId) {
                             delete data[j];
                         }
                     }
                     function filter_array(data) {
                         var index = -1,
                             arr_length = data ? data.length : 0,
                             resIndex = -1,
                             result = [];

                         while (++index < arr_length) {
                             var value = data[index];

                             if (value) {
                                 result[++resIndex] = value;
                             }
                         }

                         return result;
                     }
                     var data = filter_array(data);
                 }*/
            //deleteMapping();
            $("#mapdUsers tbody").html('');
            $("#srcUsrs").find("div").removeClass('selectedClass');
            $("#dstnUsrs").find("div").removeClass('selectedClass');
            $("#srcUsrs").find("span").removeClass('selectedClass');
            $("#dstnUsrs").find("span").removeClass('selectedClass');
            $("#srcUsrs input:radio").attr('disabled',false);
            $("#dstnUsrs input:radio").attr('disabled',false);

            if(data.length === 0) {
                showNotyNotification("notify", "No mappings available in uploaded CSV");
                $(document).find("#chkInput").attr("disabled", true);
                $("#clearBtn").css("pointer-events",'none');
                $("#clearBtn").css("cursor","not-allowed");
                $("#clearBtn").css("opacity","0.6");
                deleteMapping(true);
                $("#CFShowLoading").attr("autoMap","false");
                $("#CFShowLoading").css("display","none");
            }
            else {
                $(document).find("#chkInput").attr("disabled", false);
                $("#clearBtn").css("pointer-events",'auto');
                $("#clearBtn").css("cursor","pointer");
                $("#clearBtn").css("opacity","1");
                $("#CFShowLoading").attr("autoMap", "true");
                $("#mapdUsers tbody").html('');
                $('.download').css('pointer-events', 'none');
                $('.download').css('opacity', '0.5');
                $('#help').css('visibility', 'hidden');
                mapdUploadedCsvUrs(data);
                var lngth = $("#mapdUsers tbody tr").length;
                /* if(dupLength > 0)
                 showNotyNotification("notify",dupLength +" " + "duplicate entries found");
                showNotyNotification("notify",lngth +" " + "pairs are uploaded sucessfully through CSV");*/
            }
            // var _chkCsv = data[0].csv;
            localStorage.setItem("Csv", true);
            localStorage.setItem("csvMigrationData", JSON.stringify(data));
            $.each($('.csvPath'), function(){
                if ($(this).text() != "/" || null) {
                    $('.download').css('pointer-events', 'auto');
                    $('.download').css('opacity', '1');
                    $('#help').css('visibility', 'visible');
                    localStorage.setItem("validator","true");
                }
            });
            if(localStorage.getItem("validator"))
                csvAutoDownload();
            else{
                $("#CFShowLoading").attr("autoMap","false");
                $("#CFShowLoading").css("display","none");
            }
        },
        error: function(err) {
            console.error(err);
        }
    });
}

function mapdUploadedCsvUrs(data) {
    for(var i=0;i<data.length;i++) {
        $("#mapdUsrs .paginationDiv").css("opacity", "1");
        $('#mapdUsrs .paginationDiv select').prop('disabled', false);
        $('#mapdUsrs .paginationDiv input').prop('disabled', false);
        $("#srcCloudUsers input[cloudid=" + data[i].sourceCloudDetails.id + "]").attr('disabled', true).parent().addClass("selectedClass").parent().addClass("selectedClass");
        $("#dstCloudsUsers input[cloudid=" + data[i].destCloudDetails.id + "]").attr('disabled', true).parent().addClass("selectedClass").parent().addClass("selectedClass");
        $(document).find("#chkInput").attr("disabled", false);
        $("#clearBtn").css("pointer-events",'auto');
        $("#clearBtn").css("cursor","pointer");
        $("#clearBtn").css("opacity","1");
        var _obj = {
            "fromCloudId": {
                "id": data[i].sourceCloudId,
            },
            "toCloudId": {
                "id": data[i].destCloudId,
            }
        };
        var EmailObj = {
            "fromMailId": data[i].sourceCloudDetails.emailId,
            "fileName":data[i].sourceCloudDetails.emailId,
            "toMailId": data[i].destCloudDetails.emailId,
            "fromCloudName": data[i].sourceCloudDetails.name,
            "toCloudName": data[i].destCloudDetails.name
        };
        /* if (srcEmail === data[i].sourceCloudDetails.emailId || dstnEmail === data[i].destCloudDetails.emailId) {
             CsvOperation(_obj, 'delete');
         }
         else
             var object = _obj;*/
        var _input;
        if (mappingOptions.localStorageOperation(_obj, 'check'))
            _input = '<td><input type="checkbox" name="inputMapdUrs" checked="true"></td>';
        else
            _input = '<td><input type="checkbox" name="inputMapdUrs"></td>';

        //deleteMapping(true);
        localStorage.removeItem('FolderChecked');
        localStorage.removeItem('CsvEmailChecked');
        var csvName = localStorage.getItem("csvName");
        var _html = '<tr style="width: 100%" id="csvRow" csvid="' + data[i].csvId + '" srcemail="' + data[i].sourceCloudDetails.emailId + '" dstnemail="' + data[i].destCloudDetails.emailId + '" sourcAdminCloudId="' + data[i].sourceAdminCloudId + '" destAdminCloudId="' + data[i].destAdminCloudId + '" csvname="' + csvName + '" srccldid="' + data[i].sourceCloudId + '"  srcrt="' + data[i].sourceCloudDetails.rootFolderId + '" dstncldid="' + data[i].destCloudId + '" dstnrt="' + data[i].destCloudDetails.rootFolderId + '" class="CsvMapd" srcPathRootFolderId="' + data[i].sourceCloudDetails.pathRootFolderId + '" srcFolderPath="' + encodeURI(data[i].sourceCloudDetails.folderPath) + '"  srcRootFolderId="' + data[i].sourceCloudDetails.rootFolderId + '"  migrateSrcFolderName="' + data[i].sourceCloudDetails.migrateFolderName + '" srcCldName="' + data[i].sourceCloudDetails.name + '"dstnCldName="' + data[i].destCloudDetails.name + '" dstnPathRootFolderId="' + data[i].destCloudDetails.pathRootFolderId + '" dstnRootFolderId="' + data[i].destCloudDetails.rootFolderId + '" dstnFolderPath="' + encodeURI(data[i].destCloudDetails.folderPath) + '" migrateDstnFolderName="' + data[i].migrateFolderName + '">' + '<td><input type="checkbox" name="csvMapngCheckBox"></td><td style="width: 48%"><img src=" ../img/drive/circle/' + data[i].sourceCloudDetails.name + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="EmailSrcTitle" data-placement="bottom" style="">' + emailMaxChar(data[i].sourceCloudDetails.emailId, 25) + '</span><br/><span class="csvPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleSrce" data-placement="bottom" style="margin-left: 7%;">' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].sourceCloudDetails.folderPath, 25) + '</span></td><td style="width: 48%"><img src=" ../img/drive/circle/' + data[i].destCloudDetails.name + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="EmailDstnTitle">' + emailMaxChar(data[i].destCloudDetails.emailId, 25) + '</span><br/><span class="csvPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleDstin">' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].destCloudDetails.folderPath, 25) + '</span></td><td><span style="width: 4%;font-size: inherit;margin-left: -63px;" class="closeBtn"><i class="fa fa-times" aria-hidden="true" id="closeCsvBtn"></i></span></td></tr>';
        _html = _html.replace('TitleSrce', data[i].sourceCloudDetails.folderPath).replace('TitleDstin', data[i].destCloudDetails.folderPath).replace('EmailSrcTitle', data[i].sourceCloudDetails.emailId).replace('EmailDstnTitle',  data[i].destCloudDetails.emailId);
        $("#mapdUsers tbody").append(_html);
        /*  var srcEmail = $("#mapdUsers tbody tr:last-child").attr('srcemail');
          var dstnEmail = $("#mapdUsers tbody tr:last-child").attr('dstnemail');*/

        if ($('input[name="inputMapdUrs"]:checked').length == 30){
            $("#chkInput").prop('checked', true);
        }
        else{
            $(document).find("#chkInput").prop("checked", false);
        }



        if (!Number($("#mapdUsrs .paginationCF input").val())) {
            $("#mapdUsrs .paginationCF input").val(1);
            $("#mapdUsrs .paginationCF span").last().text("1 of 1");
        }

    }

    $('[data-toggle="tooltip"]').tooltip();

}
$(document).ready(function(){
    // appendingPrevMapping();
    //  localStorage.removeItem("selMappings");
    localStorage.removeItem("validator");
    if(localStorage.getItem("Csv")){
        //deleteMapping(true);
        $('.download').css('pointer-events', 'auto');
        $('.download').css('opacity', '1');
        $('#help').css('visibility', 'visible');
    }
    if($('#mapdUsrs input[name="csvMapngCheckBox"]').prop('checked') == false)
    {
        $('#forNextMove').addClass('disabled');
    }
    $("input[name$='type']").click(function () {
        $("#Timeperiod").css("display","none");
        $("#Timeperiod2").css("display","none");
        $("#DElTA").removeAttr("checked");
        $("#ONEWAY_SYNC").removeAttr("checked");
        $("#TWOWAY_SYNC").removeAttr("checked");
        $(".selected_optionjob").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
        $(".selected_option").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
        $(".selected_option .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    });
    $("input[name$='type1']").click(function () {
        $("#Timeperiod").css("display","block");
        $("#Timeperiod2").css("display","none");
        $("#ONETIME").removeAttr("checked");
        $("#ONEWAY_SYNC").removeAttr("checked");
        $("#TWOWAY_SYNC").removeAttr("checked");
        $(".dropdown-menu1").css("display","none");
        $("#Timeperiod2 .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        $(".selected_optionjob").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
    });
    $("input[name$='type2']").click(function () {
        $("#Timeperiod").css("display","none");
        $("#Timeperiod2").css({"display":"block","top":"60px"});
        $("#ONETIME").removeAttr("checked");
        $("#DElTA").removeAttr("checked");
        $("#TWOWAY_SYNC").removeAttr("checked");
        $("#Timeperiod2 .dropdown-menu1job").css("display","none");
        $("#Timeperiod2 .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        $(".selected_optionjob").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
        $(".selected_option").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
        $(".selected_option .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        $(".selected_optionjob span").empty();
    });
    $("input[name$='type3']").click(function () {
        $("#Timeperiod").css("display","none");
        $("#Timeperiod2").css({"display":"block","top":"98px"});
        $("#ONETIME").removeAttr("checked");
        $("#DElTA").removeAttr("checked");
        $("#ONEWAY_SYNC").removeAttr("checked");
        $("#Timeperiod2 .dropdown-menu1job").css("display","none");
        $("#Timeperiod2 .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        $(".selected_optionjob").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
        $(".selected_option").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
        $(".selected_option .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        $(".selected_optionjob span").empty();
    });
    $(document).on("cut copy paste #SizeVal",function(e) {
        e.preventDefault();
    });
    $(document).on("cut copy paste #fileTypeExc",function(e) {
        e.preventDefault();
    });

});
/*$(document).on('change', '.mapp-blck input', function(){
    if(($('.src-usrs.mapp-blck input[type="checkbox"]:checked').length > 0) && ($('.dest-usrs.mapp-blck input[type="radio"]:checked').length > 0)) {
        if(!$('#mapdUsrs .fa-exchange').hasClass('disabled'))
        {
            $("#mapdUsers tbody").html('');
            //$('#forNextMove').removeClass('disabled');
            $('#mapdUsrs #csvFileUpload').css({'opacity': '0.5','cursor':'not-allowed'});
            $('#mapdUsrs #csvFileUpload').click(false);
            $('#csvFileUpload').attr('disabled','disabled');
            $('#csvFile').attr('disabled','disabled');
            $('#mapdUsrs .fa-exchange').addClass('disabled');
        }
        var $srcCheckedUsr = $('.src-usrs.mapp-blck input[type="checkbox"]:checked').parents('.fldr_parent').find('.usr-folder');
        var $dstCheckedUsr = $('.dest-usrs.mapp-blck input[type="radio"]:checked').parents('.fldr_parent').find('.usr-folder');
        var fileArray = [];
        $.each($('.src-usrs.mapp-blck input[type="checkbox"]:checked'), function(){
            fileArray.push($(this).parent('.usr-data').children('.usr-folder1').attr('folder-id'));
        });
        var _srcUsrDetails =  {
            "userEmail": $($srcCheckedUsr).attr("useremail"),
            "userCloudName": $($srcCheckedUsr).attr("cloudname"),
            "userCloudId": $($srcCheckedUsr).attr("user-id"),
            "userRtFolId": $($srcCheckedUsr).attr("folder-id"),
            "file":fileArray
        }

        var _dstnUsrDetails =  {
            "dstnUserEmail": $($dstCheckedUsr).attr("useremail"),
            "dstnUserCloudName": $($dstCheckedUsr).attr("cloudname"),
            "dstnUserCloudId": $($dstCheckedUsr).attr("user-id"),
            "dstnUserRtFolId": $($dstCheckedUsr).attr("folder-id"),
            "dstnToRtId":$("#dstCloudsUsers input:radio:checked").parent().find("span").attr("folder-id")
        }
        $($srcCheckedUsr).parent().addClass("selectedClass");
        $($dstCheckedUsr).parent().addClass("selectedClass");
        $('.src-usrs.mapp-blck input[type="checkbox"]:checked').removeAttr('checked');
        $('.dest-usrs.mapp-blck input[type="radio"]:checked').removeAttr('checked');
        mapdSlectedUrs(_srcUsrDetails,_dstnUsrDetails,'prepend');
        var _autoSave = autoSaving(_srcUsrDetails, _dstnUsrDetails);
    }
    else
        $('#forNextMove').addClass('disabled');
});*/
$(document).on('change', 'input[name="csvMapngCheckBox"]', function(){
    //localStorage.removeItem("selectedMappings");
    //localStorage.removeItem('selectedEmail');
    localStorage.removeItem("validator");
    if($('input[name="csvMapngCheckBox"]:checked').length > 0){
        $('#forNextMove').removeClass('disabled');
    }
    else
        $('#forNextMove').addClass('disabled');

    if($('#mapdUsers input[name="inputMapdUrs"]:checked').length >0){
        $('#forNextMove').removeClass('disabled');
    }
    if( $('#mapdUsers input[name= "inputMapdUrs"]').length >0) {
        var checkedLength = $('#mapdUsers input[name= "csvMapngCheckBox"]:checked').length + $('#mapdUsers input[name="inputMapdUrs"]:checked').length;
        var totalLength = $('#mapdUsers input[name="csvMapngCheckBox"]').length + $('#mapdUsers input[name="inputMapdUrs"]').length;
    }
    else{
        var checkedLength = $('#mapdUsers input[name= "csvMapngCheckBox"]:checked').length;
        var totalLength = $('#mapdUsers input[name="csvMapngCheckBox"]').length;
    }
    if (checkedLength == totalLength) {
        $('#chkInput').prop('checked', true);
    }
    else{
        $('#chkInput').prop('checked', false);
    }
    var _d = $(this).parent().parent();
    var _srcRtFolId, _dstnRtFolId;
    if ($(_d).attr('srcPathRootFolderId') == "null") {
        _srcRtFolId = $(_d).attr('srcRootFolderId');
    }
    else
        _srcRtFolId = $(_d).attr('srcPathRootFolderId');

    var rootId = $(_d).attr('dstnPathRootFolderId');
    if (rootId == "null") {
        _dstnRtFolId = $(_d).attr('dstnRootFolderId');
    }
    else
        _dstnRtFolId = rootId;

    var obj = {
        "fromCloudId": {
            "id": $(_d).attr('srccldid'),
        },
        "toCloudId": {
            "id": $(_d).attr('dstncldid'),
        },
        "sourceFolderPath": decodeURI($(_d).attr('srcFolderPath')),
        "destFolderPath": decodeURI($(_d).attr('dstnFolderPath')),
        "destinationFolderName": $(_d).attr('migrateDstnFolderName'),
        "isCSV": "true"
    };
    var EmailObj = {
        "fromMailId": $(_d).attr('srcemail'),
        "fileName":$(_d).attr('srcemail'),
        "toMailId": $(_d).attr('dstnemail'),
        "fromCloudName":$(_d).attr('srccldname'),
        "toCloudName":$(_d).attr('dstncldname'),
    };

    if($(this).is(':checked')) {
        CsvOperation(obj, 'set');
        CsvOperation(EmailObj, 'setCsv');
    }
    else{
        CsvOperation(obj,'delete');
        CsvOperation(EmailObj,'deleteCSv');
    }
});
$(document).on('click', '#chkInput', function(){
    //localStorage.removeItem("selectedMappings");
    localStorage.removeItem('selectedEmail');
    localStorage.removeItem('folderEmail');
    localStorage.removeItem('csvEmailChecked');
    localStorage.removeItem("validator");
    //CsvOperation('','rmv');
    var src = localStorage.getItem('multiUsrSrcCldName');
    var dstn = localStorage.getItem('multiUsrDstnCldName');
    if ((src === "ONEDRIVE_BUSINESS_ADMIN" && dstn === "DROPBOX_BUSINESS") || (src === "DROPBOX_BUSINESS" && dstn === "ONEDRIVE_BUSINESS_ADMIN")) {
        if($("#chkInput:checked").length) {
            //  $("#forNextMove").removeClass("disabled");
            var data = $('#mapdUsers input[name=folderMapngCheckBox]').length;
            for (i = 0; i < data; i++) {
                if ($('#mapdUsers input[name=folderMapngCheckBox]')[i].hasAttribute('disabled')) {
                    $("input[name=folderMapngCheckBox]")[i].checked = false;
                } else {
                    $("input[name=folderMapngCheckBox]")[i].checked = true;
                }
                if($('#mapdUsers input[name=folderMapngCheckBox]:checked').length > 0){
                    $("#forNextMove").removeClass("disabled");
                }
            }
            if ($(this).parents("table").find('tbody tr').hasClass("folderRow")) {
                $('#mapdUsers input[name=folderMapngCheckBox]:checked').each(function () {
                    var _obj = {
                        "fromCloudId": {
                            "id": $(this).closest('tr').attr('srccldid')
                        },
                        "toCloudId": {
                            "id": $(this).closest('tr').attr('dstncldid')
                        },
                        "fromRootId": $(this).closest('tr').attr('srcrt'),
                        "toRootId": $(this).closest('tr').attr('dstnrt'),
                        "sourceFolderPath": $(this).closest('tr').attr('srcfolderpath'),
                        "destFolderPath": $(this).closest('tr').attr('dstnfolderpath'),
                        "folder":"true"
                    };
                    var _objEmail = {
                        "fromMailId": $(this).closest('tr').attr('srcemail'),
                        "fileName": $(this).closest('tr').attr('srcemail'),
                        "toMailId": $(this).closest('tr').attr('dstnemail'),
                        "fromCloudName": $(this).closest('tr').attr('srccldname'),
                        "toCloudName": $(this).closest('tr').attr('dstncldname'),

                    };
                    if (!mappingOptions.folderOperation(_obj, 'check')) {
                        mappingOptions.folderOperation(_obj, 'set');
                        mappingOptions.folderOperation(_objEmail, 'set1');
                    }
                });
            }
        }
        else {
            $('#mapdUsers input[name=folderMapngCheckBox]').each(function () {
                var _d = $(this).parent().parent();
                $(this).prop('checked', false);
                var _obj = {
                    "fromCloudId": {
                        "id": $(this).closest('tr').attr('srccldid')
                    },
                    "toCloudId": {
                        "id": $(this).closest('tr').attr('dstncldid')
                    },
                    "fromRootId": $(this).closest('tr').attr('srcrt'),
                    "toRootId": $(this).closest('tr').attr('dstnrt'),
                    "sourceFolderPath": $(this).closest('tr').attr('srcfolderpath'),
                    "destFolderPath": $(this).closest('tr').attr('dstnfolderpath'),
                    "folder": "true"
                };
                var _objEmail = {
                    "fromMailId": $(_d).attr('srcemail'),
                    "fileName": $(_d).attr('srcemail'),
                    "toMailId": $(_d).attr('dstnemail'),
                    "fromCloudName": $(_d).attr('srccldname'),
                    "toCloudName": $(_d).attr('dstncldname'),

                };
                mappingOptions.folderOperation(_obj, 'delete');
                mappingOptions.folderOperation(_objEmail, 'delete1');
            });
            if (!JSON.parse(localStorage.getItem('folderMappings')).length)
                $("#forNextMove").addClass("disabled");
        }
    }
    else {
        if ($("#chkInput:checked").length) {
            $("#forNextMove").removeClass("disabled");
            $("input[name=csvMapngCheckBox]").prop('checked', true);
            $('#mapdUsers input[name=csvMapngCheckBox]:checked').each(function () {
                var _d = $(this).parent().parent();
                var obj = {
                    "fromCloudId": {
                        "id": $(_d).attr('srccldid'),
                    },
                    "toCloudId": {
                        "id": $(_d).attr('dstncldid'),
                    },
                    "sourceFolderPath": decodeURI($(_d).attr('srcFolderPath')),
                    "destFolderPath": decodeURI($(_d).attr('dstnFolderPath')),
                    "destinationFolderName": $(_d).attr('migrateDstnFolderName'),
                    "isCSV": "true"
                };
                var EmailObj = {
                    "fromMailId": $(_d).attr('srcemail'),
                    "fileName": $(_d).attr('srcemail'),
                    "toMailId": $(_d).attr('dstnemail'),
                    "fromCloudName": $(_d).attr('srccldname'),
                    "toCloudName": $(_d).attr('dstncldname'),
                };
                CsvOperation(obj, 'set');
                CsvOperation(EmailObj, 'setCsv');
            });
        } else {
            $('#mapdUsers input[name=csvMapngCheckBox]').each(function () {
                var _d = $(this).parent().parent();
                $(this).prop('checked', false);
                var obj = {
                    "fromCloudId": {
                        "id": $(_d).attr('srccldid'),
                    },
                    "toCloudId": {
                        "id": $(_d).attr('dstncldid'),
                    },
                    "sourceFolderPath": decodeURI($(_d).attr('srcFolderPath')),
                    "destFolderPath": decodeURI($(_d).attr('dstnFolderPath')),
                    "destinationFolderName": $(_d).attr('migrateDstnFolderName'),
                    "isCSV": "true"
                };
                var EmailObj = {
                    "fromMailId": $(_d).attr('srcemail'),
                    "fileName": $(_d).attr('srcemail'),
                    "toMailId": $(_d).attr('dstnemail'),
                    "fromCloudName": $(_d).attr('srccldname'),
                    "toCloudName": $(_d).attr('dstncldname'),
                };
                CsvOperation(obj, 'delete');
                CsvOperation(EmailObj, 'deleteCsv');
            });
            if (!JSON.parse(localStorage.getItem('FolderChecked')).length)
                $("#forNextMove").addClass("disabled");
            //mappingOptions.localStorageOperation('','rmv');
        }
    }
});

function CsvOperation(value , op) {
    //"fromCloudId"."id"   "toCloudId" ."id"
    var _Fldr = JSON.parse(localStorage.getItem('FolderChecked')),_rtnValue, _len ;
    var _csvEmail = JSON.parse(localStorage.getItem('CsvEmailChecked')),csvValue, len ;
    if(_Fldr != null)
        _len = _Fldr.length;
    else
    {
        _len = 0;
        _Fldr = [];
    }
    if(_csvEmail != null)
        len = _csvEmail.length;
    else
    {
        len = 0;
        _csvEmail = [];
    }
    var  operations ={
        checkExists :function () {
            _rtnValue = false;
            for(var i=0; i<_len; i++){
                if(_Fldr[i].fromCloudId.id == value.fromCloudId.id  && _Fldr[i].toCloudId.id == value.toCloudId.id)
                    _rtnValue = true;
            }
        },
        deleteValue : function () {
            var  deletedArray = _Fldr.filter(function(el) {
                return el.fromCloudId.id !== value.fromCloudId.id;
            });
            localStorage.setItem('FolderChecked',JSON.stringify(deletedArray));
            _rtnValue = true;
        },
        deleteCsvValue : function () {
            var  deletedCsvArray = _csvEmail.filter(function(el) {
                return el.fromMailId !== value.fromMailId;
            });
            localStorage.setItem('CsvEmailChecked',JSON.stringify(deletedCsvArray));
            csvValue = true;
        },
        setValue : function () {
            _Fldr.push(value);
            localStorage.setItem('FolderChecked',JSON.stringify(_Fldr));
            _rtnValue = true;
        },
        setCsvValue : function () {
            _csvEmail.push(value);
            localStorage.setItem('CsvEmailChecked',JSON.stringify(_csvEmail));
            csvValue = true;
        },
        getValue :function () {
            _rtnValue = _Fldr;
            csvValue = _csvEmail;
        },
        remove   : function () {
            localStorage.removeItem('FolderChecked');
            $('input[name="csvMapngCheckBox"]').removeAttr('checked');
            _rtnValue = true;
            localStorage.removeItem('CsvEmailChecked');
            $('input[name="csvMapngCheckBox"]').removeAttr('checked');
            csvValue = true;
        },
        remove_1  : function () {
            localStorage.removeItem('FolderChecked');
            _rtnValue = true;
            localStorage.removeItem('CsvEmailChecked');
            csvValue = true;
        }
    };
    switch(op){

        case 'check'  : operations.checkExists();
            break;
        case 'delete' : operations.deleteValue();
            break;
        case 'deleteCSv' : operations.deleteCsvValue();
            break;
        case 'set'    : operations.setValue();
            break;
        case 'setCsv'    : operations.setCsvValue();
            break;
        case 'get'    : operations.getValue();
            break;
        case 'rmv'    : operations.remove();
            break;
        case 'rmv1'    : operations.remove_1();
            break;

    }

    return _rtnValue;
    return csvValue;
};

$(document).on('click', '#closeCsvBtn', function(){
    $(this).parents('.CsvMapd').remove();
});
//CSV file downloading
csvAutoDownload = function() {
    if(sessionStorage.getItem('csvDownload') != 'false'){
        showNotyNotification("notify","Your CSV is validating, Once validation is completed, Validation report gets downloaded.");
    }
    sessionStorage.setItem('csvDownload','true');
    $("#CFShowLoading").css("display","");
    $("#CFShowLoading .backdrop p").css("padding","2%");
    $("#CFShowLoading .backdrop p").text("Csv validation is in process, Please don't close the window...");
    $("#CFShowLoading").attr("autoMap","true");
    var Users;
    var csvid = $("#mapdUsers tbody").find("#csvRow").attr("csvid");
    var csvName = $("#mapdUsers tbody tr").attr("csvname");
    var userID = localStorage.getItem("UserId");
    var srcAdmincldid = $("#mapdUsers tbody tr").attr("sourcAdminCloudId");
    var dstnAdmincldid = $("#mapdUsers tbody tr").attr("destAdminCloudId");
    var _data = csvid + "?" + "userId=" + userID + "&sourceAdminCloudId=" + srcAdmincldid + "&destAdminCloudId=" + dstnAdmincldid + "&csvName=" + csvName;
    //$("#CFShowLoading").css("display","");
    $.ajax({
        //async: false,
        type: "GET",
        url: apicallurl + "/mapping/download/csvcreator/" + _data,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data, textStatus, xhr) {
            if(  xhr.responseText.length < 150){
                deleteMapping(true);
                $("#mapdUsers tbody").html('');
                var s = JSON.parse(sessionStorage.getItem('csvIteration'));
                var t = s+1;
                sessionStorage.setItem('csvIteration',t);
                if(s < 4){
                    sessionStorage.setItem('csvDownload','false');
                    csvFileUpld(localStorage.getItem("multiUsrSrcCldId"),localStorage.getItem("multiUsrDstnCldId"),sessionStorage.getItem("result"));
                }
                else{
                    $('.download').css('pointer-events', 'none');
                    $('.download').css('opacity', '0.5');
                    $('#help').css('visibility','hidden');
                    $("#CFShowLoading").attr("autoMap", "false");
                    $("#CFShowLoading").css("display", "none");
                    $("#CFShowLoading .backdrop p").text("Please wait while Loading...");
                    showNotyNotification("notify","Please upload your CSV again.");
                }
            }
            else{
                $(document).find("#chkInput").attr("disabled", false);
                var response = xhr.getResponseHeader("Content-Disposition").trim();
                var fName = response.split('=')[1];
                Users = data;
                Users = "data:application/csv," + encodeURIComponent(Users);
                var x = document.createElement("a");
                x.setAttribute("href", Users);
                x.setAttribute("download", fName);
                document.body.appendChild(x);
                x.click();
                UpdateCsvPath();
                sessionStorage.setItem('csvDownload','true');
                $("#CFShowLoading").attr("autoMap", "false");
                $("#CFShowLoading").css("display", "none");
                $("#CFShowLoading .backdrop p").text("Please wait while Loading...");
            }
        },

        error: function (err) {
            console.error(err);
        }
    });
}
UpdateCsvPath = function(){
    var csvID = $("#mapdUsers tbody").find("#csvRow").attr("csvid");
    var userID = localStorage.getItem("UserId");
    var srcAdmincldid = $("#mapdUsers tbody tr").attr("sourcAdminCloudId");
    var dstnAdmincldid = $("#mapdUsers tbody tr").attr("destAdminCloudId");
    var data = "?" + "userId=" + userID + "&sourceAdminCloudId=" + srcAdmincldid + "&destAdminCloudId=" + dstnAdmincldid;
    $.ajax({
        //async: false,
        type: "GET",
        url: apicallurl + "/mapping/user/latest/mappingcache/" + csvID + data,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data) {
            $("#mapdUsers tbody").html('');
            if (data.length == 0){
                $('.download').css('pointer-events', 'none');
                $('.download').css('opacity', '0.5');
                $('#help').css('visibility','hidden');
            }
            else{
                var csvName = localStorage.getItem("csvName");
                for(var i=0;i<data.length;i++){
                    var _html = '<tr style="width: 100%" id="csvRow" csvid="' + data[i].csvId + '" srcemail="' + data[i].sourceCloudDetails.emailId + '" dstnemail="' + data[i].destCloudDetails.emailId + '" sourcAdminCloudId="' + data[i].sourceAdminCloudId + '" destAdminCloudId="' + data[i].destAdminCloudId + '" csvname="' + csvName + '" srccldid="' + data[i].sourceCloudId + '"  srcrt="' + data[i].sourceCloudDetails.rootFolderId + '" dstncldid="' + data[i].destCloudId + '" dstnrt="' + data[i].destCloudDetails.rootFolderId + '" class="CsvMapd" srcPathRootFolderId="' + data[i].sourceCloudDetails.pathRootFolderId + '" srcFolderPath="' + encodeURI(data[i].sourceCloudDetails.folderPath) + '"  srcRootFolderId="' + data[i].sourceCloudDetails.rootFolderId + '"  migrateSrcFolderName="' + data[i].sourceCloudDetails.migrateFolderName + '" srcCldName="' + data[i].sourceCloudDetails.name + '"dstnCldName="' + data[i].destCloudDetails.name + '" dstnPathRootFolderId="' + data[i].destCloudDetails.pathRootFolderId + '" dstnRootFolderId="' + data[i].destCloudDetails.rootFolderId + '" dstnFolderPath="' + encodeURI(data[i].destCloudDetails.folderPath) + '" migrateDstnFolderName="' + data[i].migrateFolderName + '">' + '<td><input type="checkbox" name="csvMapngCheckBox"></td><td style="width: 48%"><img src=" ../img/drive/circle/' + data[i].sourceCloudDetails.name + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="EmailSrcTitle" data-placement="bottom" style="margin-left: 7%;">' + emailMaxChar(data[i].sourceCloudDetails.emailId, 25) + '</span><br/><span class="csvPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleSrce" data-placement="bottom" style="margin-left: 7%;">' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].sourceCloudDetails.folderPath, 25) + '</span></td><td style="width: 48%"><img src=" ../img/drive/circle/' + data[i].destCloudDetails.name + '.png" alt="pic" class="forImage"><span class="manualPath" data-toggle="tooltip" data-placement="bottom" style="font-size: 14px !important;" title="EmailDstnTitle">' + emailMaxChar(data[i].destCloudDetails.emailId, 25) + '</span><br/><span class="csvPath" data-toggle="tooltip" data-placement="bottom" style="margin-left: 7%;" title="TitleDstin">' + CFManageCloudAccountsAjaxCall.getMaxChars(data[i].destCloudDetails.folderPath, 25) + '</span></td><td><span style="width: 4%;font-size: inherit;margin-left: -63px;" class="closeBtn"><i class="fa fa-times" aria-hidden="true" id="closeCsvBtn"></i></span></td></tr>';
                    _html = _html.replace('TitleSrce', data[i].sourceCloudDetails.folderPath).replace('TitleDstin', data[i].destCloudDetails.folderPath).replace('EmailSrcTitle', data[i].sourceCloudDetails.emailId).replace('EmailDstnTitle',  data[i].destCloudDetails.emailId);
                    $("#mapdUsers tbody").append(_html);
                }
                $('.download').css('pointer-events', 'auto');
                $('.download').css('opacity', '1');
                $('#help').css('visibility','visible');
                var lngth = $("#mapdUsers tbody tr").length;
                showNotyNotification("notify",lngth +" " + "pairs are uploaded sucessfully through CSV");
            }
            $('[data-toggle="tooltip"]').tooltip();

        },

        error: function (err) {
            console.error(err);
        }
    });

}

csvDownload = function(){
    $("#CFShowLoading").css("display","");
    $("#CFShowLoading").attr("autoMap","true");
    var csvID = $("#mapdUsers tbody").find("#csvRow").attr("csvid");
    var array = [];
    var Users;
    var csvData = [];
    $("#mapdUsers tbody tr").each(function () {
        var _obj = {
            "sourceCloudId": $(this).attr('srccldid'),
            "sourceEmailId": $(this).attr('srcemail'),
            "sourceFolderPath":  decodeURI($(this).attr('srcFolderPath')),
            "destCloudId": $(this).attr('dstncldid'),
            "destEmailId":  $(this).attr('dstnemail'),
            "destFolderPath": decodeURI($(this).attr('dstnFolderPath')),
            "destCloudName": $(this).attr('dstnCldName'),
        };
        array.push(_obj);
        localStorage.setItem("csvUsers",JSON.stringify(array));
    });
    var _data = localStorage.getItem("csvUsers");
    $.ajax({
        //async: false,
        type: "POST",
        url: apicallurl + "/mapping/download/csvmappingcache/" + csvID,
        data: _data,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data, textStatus, xhr) {
            var response = xhr.getResponseHeader("Content-Disposition").trim();
            var fName = response.split('=')[1];
            Users = data;
            Users = "data:application/csv," + encodeURIComponent(Users);
            var x = document.createElement("a");
            x.setAttribute("href", Users);
            x.setAttribute("download", fName);
            document.body.appendChild(x);
            x.click();
            $('.download').css('pointer-events', 'auto');
            $('.download').css('opacity', '1');
            $('#help').css('visibility','visible');
            $("#CFShowLoading").attr("autoMap", "false");
            $("#CFShowLoading").css("display", "none");
        },

        error: function (err) {
            console.error(err);
        }
    });
}

deleteCsv = function(){
    var csvId = $("#mapdUsers tbody").find("#csvRow").attr("csvid");
    $.ajax({
        //async: false,
        type: "DELETE",
        url: apicallurl + "/mapping/delete/csvmappcache/" + csvId,
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
        },
        success: function (data) {
        },

        error: function (err) {
            console.error(err);
        }
    });
}
//PauseResume and Cancel button in report page
$('.pauseResumeVal').html('Pause');
$(document).on('click', '.Pause,.cancelBtn', function(){
    var _val;
    var parent = $(this);
    if($(this).attr('src') == "../img/Resume.png" ){
        $(this).attr('src',"../img/Pause.png");
        _val="RESUME";
    }
    else if($(this).attr('src') == "../img/Pause.png" ){
        $(this).attr('src',"../img/Resume.png");
        _val="PAUSE";
    }
    else{
        _val="CANCEL";
    }
    $.ajax({
        type: "GET",
        url: apicallurl + "/move/pauseresume/workspaceId?"+"workSpaceId="+$(this).parents("tr").attr("wrkspaceid") + "&status=" + _val + "&userId=" + localStorage.getItem("UserId"),
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            if(data.threadStatus == "RESUME"){
                parent.parents("tr").children('td:eq(3)').html('In Progress').css("color","blue").css("font-weight","bold");
            }
            else if(data.threadStatus == "PAUSE"){
                parent.parents("tr").children('td:eq(3)').html('Pause').css("color","red").css("font-weight","bold");
            }
            else if(data.threadStatus == "CANCEL"){
                parent.parents("tr").children('td:eq(3)').html('Cancel').css("color","red").css("font-weight","bold");
                //parent.parents("tr").children('td:eq(5)').find(".fa-download").css("opacity","0.2").css("cursor","not-allowed");
                parent.parents("tr").children('td:eq(7)').find(".cancelBtn").css("opacity","0.2").css("cursor","not-allowed");
                parent.parents("tr").children('td:eq(6)').find('.Pause').css("opacity","0.2").css("cursor","not-allowed");

            }
            else if(data.threadStatus){
                if(status == "PROCESSED" || status == "ERROR" || status == "SUSPENDED" || status == "PROCESSED_WITH_SOME_ERRORS" || status == "PROCESSED_WITH_SOME_WARNINGS" || status == "CANCEL" || status == "PROCESSED_WITH_SOME_CONFLICTS" || status == "CONFLICT")
                    $('.pauseResumeVal').css("color","red").css("cursor", "not-allowed").css("opacity","0.2");
            }
        }
    });
});
/* $(document).on('click', '.syncPause', function() {
    var _val;
    var parent = $(this);
    if ($(this).attr('src') == "../img/Resume.png") {
        $(this).attr('src', "../img/Pause.png");
        _val = "RESUME";
    }
    else if ($(this).attr('src') == "../img/Pause.png") {
        $(this).attr('src', "../img/Resume.png");
        _val = "PAUSE";
    }

    var jobId = $(this).parents('tr.jobsRow').attr('id');
    $.ajax({
        type: "GET",
        url: apicallurl + "/move/newmultiuser/update/sync/" + jobId + "?schedulerStatus=" + _val,
        async: false,
        dataType: "json",
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            if (data.jobStatus == "IN_PROGRESS") {
                parent.parents("tr").children('td:eq(3)').html('In Progress').css("color", "blue").css("font-weight", "bold");
            }
            else if (data.jobStatus == "PAUSE") {
                parent.parents("tr").children('td:eq(3)').html('Pause').css("color", "red").css("font-weight", "bold");
            }
        }
    });
});
$(document).on('click', '.cnclBtn', function() {
    var parent = $(this);
    if(parent.parents("tr").attr('jobType') === "TWO_WAY_SYNC"){
        $('#cancelSync').find('#cnclMsg').text('Clicking "Proceed" will permanently Stop this Two-way Sync Scheduler');
    }
    else{
        $('#cancelSync').find('#cnclMsg').text('Clicking "Proceed" will permanently Stop this One-way Sync Scheduler');
    }
    $('#cancelSync').modal('show');
    $('#proceed').on('click', function () {
        var _val = "STOP";
        var jobId = parent.parents('tr.jobsRow').attr('id');
        $.ajax({
            type: "GET",
            url: apicallurl + "/move/newmultiuser/update/sync/" + jobId + "?schedulerStatus=" + _val,
            async: false,
            dataType: "json",
            headers: {
                "Content-Type": "application/json",
                "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
                "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
            },
            success: function (data) {
                if (data.jobStatus == "CANCEL") {
                    parent.parents("tr").children('td:eq(3)').html('Cancel').css("color", "red").css("font-weight", "bold");
                    parent.parents("tr").children('td:eq(5)').find(".dsblePause").css("opacity", "0.2").css("cursor", "not-allowed");
                    parent.parents("tr").children('td:eq(5)').find('.syncPause').removeClass('syncPause').addClass(".dsblePause").css("opacity", "0.2").css("cursor", "not-allowed");
                    parent.parents("tr").children('td:eq(6)').find(".cnclBtn").removeClass('cnclBtn').addClass("cleBtn").css("opacity", "0.2").css("cursor", "not-allowed");
                }
                $('#cancelSync').modal('hide');
                if(parent.parents("tr").attr('jobType') === "TWO_WAY_SYNC"){
                    showNotyNotification('notify', 'Two-Way Sync scheduler has been cancelled.');
                }
                else{
                    showNotyNotification('notify', 'One-Way Sync scheduler has been cancelled.');
                }
            }
        });
    });
});

function syncVerification() {
    $("#appendSelectedPairs").html("");

    CsvOperation('','rmv1');
    if($('#mapdUsers input[name=csvMapngCheckBox]:checked').length){
        $('#mapdUsers input[name=csvMapngCheckBox]:checked').each(function(){
            var _d = $(this).parent().parent();
            var obj = {
                "fromCloudId": {
                    "id": $(_d).attr('srccldid'),
                },
                "toCloudId": {
                    "id": $(_d).attr('dstncldid'),
                },
                "sourceFolderPath": decodeURI($(_d).attr('srcFolderPath')),
                "destFolderPath": decodeURI($(_d).attr('dstnFolderPath')),
                "destinationFolderName": $(_d).attr('migrateDstnFolderName'),
                "isCSV": "true"
            };
            var EmailObj = {
                "fromMailId": $(_d).attr('srcemail'),
                "fileName":$(_d).attr('srcemail'),
                "toMailId": $(_d).attr('dstnemail'),
                "fromCloudName":$(_d).attr('srccldname'),
                "toCloudName":$(_d).attr('dstncldname'),
            };
            CsvOperation(obj,'set');
            CsvOperation(EmailObj,'setCsv');
        });
    }
    else{
        $('#mapdUsers input[name=csvMapngCheckBox]').each(function(){
            var _d = $(this).parent().parent();
            $(this).prop('checked',false);
            var obj = {
                "fromCloudId": {
                    "id": $(_d).attr('srccldid'),
                },
                "toCloudId": {
                    "id": $(_d).attr('dstncldid'),
                },
                "sourceFolderPath": decodeURI($(_d).attr('srcFolderPath')),
                "destFolderPath": decodeURI($(_d).attr('dstnFolderPath')),
                "destinationFolderName": $(_d).attr('migrateDstnFolderName'),
                "isCSV": "true"
            };
            var EmailObj = {
                "fromMailId": $(_d).attr('srcemail'),
                "fileName":$(_d).attr('srcemail'),
                "toMailId": $(_d).attr('dstnemail'),
                "fromCloudName":$(_d).attr('srccldname'),
                "toCloudName":$(_d).attr('dstncldname'),
            };
            CsvOperation(obj,'delete');
            CsvOperation(EmailObj,'delete');
        });
    }
    mappingOptions.localStorageOperation('','rmv1');
    if($('#mapdUsers input[name=inputMapdUrs]:checked').length){
        if(!$(this).parents("table").find('tbody tr').hasClass("automapRow")){
            $('#mapdUsers input[name=inputMapdUrs]:checked').each(function(){
                var _obj ={
                    "fromCloudId": {
                        "id":$(this).closest('tr').attr('srccldid'),
                    },
                    "toCloudId": {
                        "id":$(this).closest('tr').attr('dstncldid')
                    }
                };
                var _objEmail ={
                    "fromMailId": $(this).closest('tr').attr('srccldid'),
                    "fileName":$(this).closest('tr').attr('srcemail'),
                    "toMailId": $(this).closest('tr').attr('dstncldid'),
                    "fromCloudName":$(this).closest('tr').attr('srccldname'),
                    "toCloudName":$(this).closest('tr').attr('dstncldname'),

                };
                mappingOptions.localStorageOperation(_obj,'set');
                mappingOptions.localStorageOperation(_objEmail,'set1');
            });
        }
        else{
            $('#mapdUsers input[name=inputMapdUrs]:checked').each(function(){
                var _d = $(this).parent().parent();
                var _obj ={
                    "fromCloudId": {
                        "id":$(_d).attr('srccldid'),
                    },
                    "toCloudId": {
                        "id":$(_d).attr('dstncldid'),
                    }
                };
                var _objEmail ={
                    "fromMailId": $(_d).attr('srcemail'),
                    "fileName":$(_d).attr('srcemail'),
                    "toMailId": $(_d).attr('dstnemail'),
                    "fromCloudName":$(_d).attr('srccldname'),
                    "toCloudName":$(_d).attr('dstncldname'),

                };
                mappingOptions.localStorageOperation(_obj,'set');
                mappingOptions.localStorageOperation(_objEmail,'set1');
            });
        }
    }
    else{
        $('#mapdUsers input[name=inputMapdUrs]').each(function(){
            var _d = $(this).parent().parent();
            $(this).prop('checked',false);
            var _obj ={
                "fromCloudId": {
                    "id":$(_d).attr('srccldid'),
                },
                "toCloudId": {
                    "id":$(_d).attr('dstncldid'),
                }
            };
            var _objEmail ={
                "fromMailId": $(_d).attr('srcemail'),
                "fileName":$(_d).attr('srcemail'),
                "toMailId": $(_d).attr('dstnemail'),
                "fromCloudName":$(_d).attr('srccldname'),
                "toCloudName":$(_d).attr('dstncldname'),

            };
            mappingOptions.localStorageOperation(_obj,'delete');
            mappingOptions.localStorageOperation(_objEmail,'delete1');
        });
    }

    if($('input[name="csvMapngCheckBox"]:checked')){
        var CsvSync = JSON.parse(localStorage.getItem('FolderChecked'));
    }
    if(CsvSync == null ||  CsvSync === []) {
        CsvSync =[];
        localStorage.removeItem("csvName");
    }
    if($('#mapdUsers input[name="inputMapdUrs"]:checked').length !== 0){
        var inpUsers = JSON.parse(localStorage.getItem('selectedMappings'));
        for(var i=0;i<inpUsers.length;i++) {
            CsvSync.push(inpUsers[i]);
        }
    }
    localStorage.removeItem("selMappings");
    localStorage.removeItem("syncMappings");
    $.ajax({
        type: "POST",
        url: apicallurl + "/move/newmultiuser/verify/sync",
        data: JSON.stringify(CsvSync),
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            //  localStorage.setItem("syncMappings",data);
            // var syncUsers = localStorage.getItem("syncMappings");

            if($('input[name="csvMapngCheckBox"]:checked')){
                var selMpngs = JSON.parse(localStorage.getItem('FolderChecked'));
                //exists = true;
            }
            if(selMpngs == null)
                selMpngs =[];
            if($('#mapdUsers input[name="inputMapdUrs"]:checked').length != 0){
                var s = JSON.parse(localStorage.getItem('selectedMappings'));
                for(var i=0;i<s.length;i++) {
                    selMpngs.push(s[i]);
                }
            }
            var array1 = [];
            if(data.length !=0) {

                for (var i = 0; i < data.length; i++) {
                    array1.push(data[i]);
                }
            }
            // var selMpngs= JSON.parse(localStorage.getItem('selectedMappings'));

            var array2 = [];
            for(var i=0;i<selMpngs.length;i++){
                array2.push(selMpngs[i].fromCloudId.id);
            }
            //   var UnSync = array2.filter( function(sync) {
             //          return !this.has(sync)
             //      },
             //      new Set(data) );

            if(array1.length  != 0) {
                for (var i=0;i<array2.length;i++){
                    for (var j=0;j<array1.length;j++){
                        if(array2[i] == array1[j])
                            delete selMpngs[i];
                    }
                }
                function filter_array(selMpngs) {
                    var index = -1,
                        arr_length = selMpngs ? selMpngs.length : 0,
                        resIndex = -1,
                        result = [];

                    while (++index < arr_length) {
                        var value = selMpngs[index];

                        if (value) {
                            result[++resIndex] = value;
                        }
                    }

                    return result;
                }
                localStorage.setItem("selMappings",JSON.stringify(filter_array(selMpngs)));
                showNotyNotification("notify","Some of the Pairs are under Sync Process and processed Migration process with remaining allowed Pairs");
            }
            else{
                localStorage.setItem("selMappings",JSON.stringify(selMpngs));
            }
            mappingOptions.createJob();
        }
    });

}


function syncUsers() {
    var JOBLIST = JSON.parse(localStorage.getItem("jobList"));
    var _obj ={
        "jobList": JOBLIST
    };
    $.ajax({
        type: "POST",
        url: apicallurl + "/move/newmultiuser/create/syncjob",
        data: JSON.stringify(_obj),
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function (data) {
            console.log(data);
            mappingOptions.localStorageOperation('','rmv');
            CsvOperation('','rmv');
            // localStorage.removeItem("jobList");
            mappingOptions.appendSyncMigrationJobs(1,60);
            $("#mappingClouds").css("display","none");
            $("#mappingUsers").css("display","none");
            $('#mappingOptions').css("display","none");
            $("#preview").css("display","none");
            $('#mappedMigration').css("display","none");
            $('#mappedSyncMigration').css("display","");


            var _step = parseInt($("#forNextMove").attr("data-step"));
            _step = _step + 1;
            $("#teamMigrationWidget ul li[data-val=" + (_step - 1) + "]").removeClass("active").addClass("completed");
            $("#forNextMove").attr("data-step",_step);
            $("#forPreviousMove").attr("data-prev",_step );
            $("#teamMigrationWidget ul li[data-val=" + _step + "]").addClass("active");
            //$("#scheck").trigger('click');
        }
    });
} */
function trialUserMigration() {
    if($('input[name="csvMapngCheckBox"]:checked')){
        var checkSelection = JSON.parse(localStorage.getItem('CsvEmailChecked'));
    }
    if(checkSelection == null ||  checkSelection === []) {
        checkSelection =[];
        localStorage.removeItem("csvName");
    }
    if($('#mapdUsers input[name="inputMapdUrs"]:checked').length !== 0){
        var inpUsers = JSON.parse(localStorage.getItem('selectedEmail'));
        for(var i=0;i<inpUsers.length;i++) {
            checkSelection.push(inpUsers[i]);
        }
    }
    if($('#mapdUsers input[name="folderMapngCheckBox"]:checked').length !== 0){
        var inputUsers = JSON.parse(localStorage.getItem('folderEmail'));
        for(var i=0;i<inputUsers.length;i++) {
            checkSelection.push(inputUsers[i]);
        }
    }
    $.ajax({
        type: "POST",
        url: apicallurl + "/move/multiuser/verify/"+localStorage.getItem("UserId"),
        data: JSON.stringify(checkSelection),
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        complete: function (xhr) {
            var data = xhr.responseText;
            if (data == "multiuser paid" || data == "true:success") {
                mappingOptions.createJob();
                // syncVerification();
            }
            else if (data === "false:User not Paid user data exceed"){
                //dataLimitMulti();
                $('#entDataLimit').modal('show');
                $("#CFShowLoading").attr("autoMap", "false");
                $("#CFShowLoading").css("display", "none");
            }
            else{
                // dataLimitMulti();
                var msg = data;
                var noOfusrs = msg.split(":").pop();
                $("#number").text(noOfusrs);
                $('#EntMigrationDtls').modal('show');
                $("#CFShowLoading").attr("autoMap", "false");
                $("#CFShowLoading").css("display", "none");
            }

        }

    });

}

/*function dataLimit(){
    $.ajax({
        url:  apicallurl+"/move/multiuser/verify/datalimit/"+localStorage.getItem("UserId"),
        type: 'GET',
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function(response){
            if(response.userLimitExeceeded ===  true) {
                $('#ExtraUserPayment').modal('show');
                var Csvlength = $('input[name="csvMapngCheckBox"]:checked').length;
                var MapdLength = $('#mapdUsers input[name="inputMapdUrs"]:checked').length;
                var totalLength = Csvlength + MapdLength;
                $("#users").html(totalLength);
                var total = totalLength * 25;
                $("#totalVal").html("$" + total);
                $("#CFShowLoading").attr("autoMap", "false");
                $("#CFShowLoading").css("display", "none");
            }
            else {
                mappingOptions.createJob();
                setInterval(dataLimit, 30000);
            }

        }
    });
}
function dataLimitMulti(){
    $.ajax({
        url:  apicallurl+"/move/multiuser/verify/datalimit/"+localStorage.getItem("UserId"),
        type: 'GET',
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function(response){
            if(response.userLimitExeceeded ===  true) {
                $('#entDataLimit').modal('show');
                $("#CFShowLoading").attr("autoMap", "false");
                $("#CFShowLoading").css("display", "none");
            }
            else{
                $('#EntMigrationDtls').modal('show');
                $("#CFShowLoading").attr("autoMap", "false");
                $("#CFShowLoading").css("display", "none");
            }


        }
    });
}*/
/*function Check(){
    $('.proceedBtn').removeClass('disabled');
    $('.proceedBtn').css('opacity','1');
}*/
function cancel() {
    $("#chkInput").prop('checked', false);
    $('#mapdUsrs input[name= "inputMapdUrs"]').prop('checked', false);
    $('#mapdUsrs input[name="csvMapngCheckBox"]').prop('checked', false);
    $('#mapdUsrs input[name="folderMapngCheckBox"]').prop('checked', false);
    localStorage.removeItem("selectedEmail");
    localStorage.removeItem("selectedMappings");
    localStorage.removeItem('folderEmail');
    localStorage.removeItem('folderMappings');
    localStorage.removeItem("CsvEmailChecked");
    localStorage.removeItem("csvMigrationData");
    localStorage.removeItem("FolderChecked");


}
function dataLimit(){
    $.ajax({
        url:  apicallurl+"/move/multiuser/verify/trailuser/"+localStorage.getItem("UserId"),
        type: 'GET',
        headers: {
            "Content-Type": "application/json",
            "Authorization": CFManageCloudAccountsAjaxCall.getAuthDetails(),
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE,OPTIONS"
        },
        success: function(response){
            var _isCustomUser =  CFManageCloudAccountsAjaxCall.isCustomUser();
            if(response == "MULTIUSER_INACTIVE" && _isCustomUser == false){
                $("#sixGbmsg").css("display","block");
                $("#cusDelta").css("display","none");

            }
        }
    });
}
/* $(document).on('click','#spoToSyncCont',function(){
    var versioning = $('#fileType_dropDown').val();
    if(versioning === 'false'){
        versioning = 'true';
    }
    else{
        versioning = 'false';
    }

    mappingOptions.spoToSyncUpdate(versioning);
});
//Reports page change for one way sync two
$(document).on('click','#twowaySyncSrc',function(){
    var ivalue = $(this).parents('tbody').find('tr.moveSyncWorksapceList.rmvdCloudClass').children('td').find('i.fa-minus');
    var _htmlCode = appendTwoWaySyncWorkspaceReport(getWorkspaceReport_forTWS($(ivalue).parents("tr").attr('wrkspaceid'),'all',1,$(ivalue).parents("tr").attr('srccldname')));
    var count = CFManageCloudAccountsAjaxCall.fileFolderCount($(ivalue).parents("tr").attr('wrkspaceid'),$(ivalue).parents("tr").attr('srccldname'),$(ivalue).attr('jobtype'));
    _htmlCode = _htmlCode.replace('TotalCounting',(count.totalFiles + count.totalFolders)).replace('ProcessedCounting',count.processedCount).replace('ErrorCounting',count.conflictCount);
    $(document).find('.moveReportDetails').html('').css("display","none");
    $(ivalue).parents("tr").next(".moveReportDetails").css("display","");
    $(ivalue).parents("tr").next(".moveReportDetails").append(_htmlCode);
    $('#twowaySyncDstn').removeClass('activeTab');
    $('#twowaySyncSrc').addClass('activeTab');
    moveSyncReportPagination($(ivalue).parents("tr").attr('totcount'),$(ivalue).parents("tr").attr('wrkspaceid'),'all',$(ivalue).parents("tr").attr('srccldname'),$(ivalue).attr('jobtype'));


});
$(document).on('click','#twowaySyncDstn',function(){
//    $(document).find('.moveReportDetails').html('').css("display","none");
    var ivalue = $(this).parents('tbody').find('tr.moveSyncWorksapceList.rmvdCloudClass').children('td').find('i.fa-minus');
    var _htmlCode = appendTwoWaySyncWorkspaceReport(getWorkspaceReport_forTWS($(ivalue).parents("tr").attr('wrkspaceid'),'all',1,$(ivalue).parents("tr").attr('dstncldname')));
    var count = CFManageCloudAccountsAjaxCall.fileFolderCount($(ivalue).parents("tr").attr('wrkspaceid'),$(ivalue).parents("tr").attr('dstncldname'),$(ivalue).attr('jobtype'));
    _htmlCode = _htmlCode.replace('TotalCounting',(count.totalFiles + count.totalFolders)).replace('ProcessedCounting',count.processedCount).replace('ErrorCounting',count.conflictCount);
    $(document).find('.moveReportDetails').html('').css("display","none");
    $(ivalue).parents("tr").next(".moveReportDetails").css("display","");
    $(ivalue).parents("tr").next(".moveReportDetails").append(_htmlCode);
    $('#twowaySyncSrc').removeClass('activeTab');
    $('#twowaySyncDstn').addClass('activeTab');
    moveSyncReportPagination($(ivalue).parents("tr").attr('totcount'),$(ivalue).parents("tr").attr('wrkspaceid'),'all',$(ivalue).parents("tr").attr('dstncldname'),$(ivalue).attr('jobtype'));

});

$(document).on('change','#jobType_dropDown',function(){
    if($('#jobType_dropDown :selected').text() === "Two-way sync" || $('#jobType_dropDown :selected').text() === "One-way sync") {
        $('#selectFrequency').css('display', '');
    }
    else{
        $('#selectFrequency').css('display', 'none');
    }
    if((localStorage.getItem('fromCloudName') === "SHAREPOINT_ONLINE_BUSINESS") && (localStorage.getItem('toCloudName') === "SYNCPLICITY")){
        if($('#jobType_dropDown :selected').text() === "Two-way sync"){
            $('.spoToSyncCity').css('display', 'none');

        }
        else{
            $('.spoToSyncCity').css('display', '');
        }
    }
    else{
        $('.spoToSyncCity').css('display', 'none');
    }
}); */



/*Folder display checkbox events*/
// $(document).on('change','.forDomainNameMin input',function(){
$("#srcCloudUsers,#dstCloudsUsers").on('change','input',function(){
    if($("#srcCloudUsers input:checkbox:checked").length && $("#dstCloudsUsers input:radio:checked").length) {
        /*  if($("#srcCloudUsers input[name=srccheckBox]:checked").length){
              var $srcCheckedUsr = $("#srcCloudUsers input[name=srccheckBox]:checked");
              var $dstCheckedUsr = $("#dstCloudsUsers input:radio:checked");
          }
          else {*/
        $("#CFShowLoading").css("display","");
        var $srcCheckedUsr = JSON.parse(localStorage.getItem('srcFldrsChecked'));
        var $dstCheckedUsr = $("#dstCloudsUsers input:radio:checked");
        for (var i = 0; i < $srcCheckedUsr.length; i++) {
            var  _srcUsrDetails = $srcCheckedUsr[i];
            var _dstnUsrDetails = {
                "dstnUserEmail": $($dstCheckedUsr).attr("usremail"),
                "dstnUserCloudName": $($dstCheckedUsr).attr("cldname"),
                "dstnUserCloudId": $($dstCheckedUsr).attr("cloudid"),
                "dstnUserRtFolId": $($dstCheckedUsr).attr("rtfolid"),
                "dstnFolderPath": $($dstCheckedUsr).attr("folderPath")
            }
            $($srcCheckedUsr[i]).removeAttr('checked');
            $($dstCheckedUsr).removeAttr('checked');
            var isFolder = true;
            var _autoSave = autoSaving(_srcUsrDetails, _dstnUsrDetails,isFolder);

        }
        $("#srcCloudUsers input:checkbox:checked").prop("checked", false).attr('disabled', false);
        fldrStorage('','rmv');
    }
});

$("#srcCloudUsers").on("change","input[name=srccheckBox]",function () {
    var s = $(this);
    if (s.filter(':checked').length){
        $(this).parents('.fldr_parent').children('.fldr_parent1').find('input[name="firstInnChkBox"]').prop( "checked", true).attr('disabled',true);
        $(this).parents('.fldr_parent').children('.fldr_parent1').find('input[name="folderSubChkBox"]').prop( "checked", true).attr('disabled',true);
    }
    else{
        $(this).parents('.fldr_parent').children('.fldr_parent1').find('input[name="firstInnChkBox"]').prop( "checked", false).attr('disabled',false);
        $(this).parents('.fldr_parent').children('.fldr_parent1').find('input[name="folderSubChkBox"]').prop( "checked", false).attr('disabled',false);
    }
});
$("#srcCloudUsers").on("change","input[name=firstInnChkBox]",function () {
    var s = $(this);
    if (s.filter(':checked').length){
        $(this).siblings('.fldr_parent1').find('input').prop( "checked", true).attr('disabled',true);
    }
    else{
        $(this).siblings('.fldr_parent1').find('input[name="folderSubChkBox"]').prop( "checked", false).attr('disabled',false);
        //   $(this).parents('.fldr_parent1').find('input[name="firstInnChkBox"]').prop( "checked", false);
        $(this).parents('.fldr_parent').find('input[name="srccheckBox"]').prop( "checked", false).attr('disabled',false);
    }
    /*if(($(this).parent().parent().children().length)=== ($(this).parent().parent().children().children('input:checked').length)){
        $(this).parent().parent('.fldr_parent1').siblings().find('input').prop( "checked", true);
        $(this).parent().parent().find('input').attr('disabled',true);
    }*/
});

$("#srcCloudUsers").on("change","input[name=folderSubChkBox]",function () {
    var s = $(this);
    if (s.filter(':checked').length){
        $(this).closest('.usr-data').find('.fldr_parent1').find('input[name="folderSubChkBox"]').prop( "checked", true).attr('disabled',true);
        // $(this).closest('.fldr_parent1').parents('.fldr_parent1').children('.fileActive').find('input[name="firstInnChkBox"]')
    }
    else{
        $(this).closest('.usr-data').find('.fldr_parent1').find('input[name="folderSubChkBox"]').prop( "checked", false).attr('disabled',false);
        $(this).parents('.usr-data').find('input[name="firstInnChkBox"]').prop( "checked", false).attr('disabled',false);
        $(this).parents('.fldr_parent').find('input[name="srccheckBox"]').prop( "checked", false).attr('disabled',false);
    }
    /* if(($(this).parent().parent().children().length)=== ($(this).parent().parent().children().children('input:checked').length)){
         $(this).parent().parent('.fldr_parent1').siblings('input').prop( "checked", true);
         $(this).parent().parent().find('input').attr('disabled',true);
     }*/
});
$("#srcCloudUsers").on('click','input',function(){
    var _srcUsrDetails = {
        "userEmail": $(this).attr("usremail"),
        "userCloudName": $(this).attr("cldname"),
        "userCloudId": $(this).attr("cloudid"),
        "userRtFolId": $(this).attr("rtfolid"),
        "folderPath": $(this).attr("folderPath")
    };
    if($(this).is(':checked')) {
        if(!fldrStorage(_srcUsrDetails,'check')) {
            fldrStorage(_srcUsrDetails, 'set');
        }
    }
    else{
        fldrStorage(_srcUsrDetails,'delete');
    }
});
function fldrStorage(value , op) {
    var _srcFldr = JSON.parse(localStorage.getItem('srcFldrsChecked')),_rtnsrcValue, _len ;
    if(_srcFldr != null)
        _len = _srcFldr.length;
    else
    {
        _len = 0;
        _srcFldr = [];
    }
    var  operations ={
        checkSrcExists :function () {
            _rtnsrcValue = false;
            for(var i=0; i<_len; i++){
                if(_srcFldr[i].userCloudId === value.userCloudId  && _srcFldr[i].userRtFolId === value.userRtFolId)
                    _rtnsrcValue = true;
            }
        },
        checkSrcExists1 :function () {
            _rtnsrcValue = false;
            for(var i=0; i<_len; i++){
                if(_srcFldr[i].userCloudId === value.cloudId  && _srcFldr[i].userRtFolId === value.id)
                    _rtnsrcValue = true;
            }
        },

        deleteSrcValue : function () {
            var  deletedSrcArray = _srcFldr.filter(function(el) {
                return el.userRtFolId !== value.userRtFolId;
            });
            localStorage.setItem('srcFldrsChecked',JSON.stringify(deletedSrcArray));
            _rtnsrcValue = true;
        },

        setSrcValue : function () {
            _srcFldr.push(value);
            localStorage.setItem('srcFldrsChecked',JSON.stringify(_srcFldr));
            _rtnsrcValue = true;
        },

        getValue :function () {
            _rtnsrcValue = _srcFldr;

        },
        remove   : function () {
            localStorage.removeItem('srcFldrsChecked');
            _rtnsrcValue = true;

        },

    };
    switch(op){

        case 'check'  : operations.checkSrcExists();
            break;
        case 'check1'  : operations.checkSrcExists1();
            break;
        case 'delete' : operations.deleteSrcValue();
            break;
        case 'set'    : operations.setSrcValue();
            break;
        case 'get'    : operations.getValue();
            break;
        case 'rmv'    : operations.remove();
            break;
    }

    return _rtnsrcValue;
};
///options page new design
$(".fromDate").datepicker({
    showOn: 'button',
    buttonImageOnly: true,
    buttonImage: 'https://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
    onSelect: function(dateText, inst) {
        var date = $(this).val();
        var time = $('#time').val();
    }
});
$(".toDate").datepicker({
    showOn: 'button',
    buttonImageOnly: true,
    buttonImage: 'https://jqueryui.com/resources/demos/datepicker/images/calendar.gif',
    onSelect: function(dateText, inst) {
        var date = $(this).val();
        var time = $('#time').val();
    }
});
$(document).on('click','.JobOptions .fa-minus',function() {
    $(this).removeClass('fa-minus').addClass('fa-plus');
    $(".optionsDiv1").css("display","none");
    $(".optionsDiv2").css("display","block");
    $(".Fileoptions .fa-plus").removeClass('fa-plus').addClass('fa-minus');
});
$(document).on('click','.JobOptions .fa-plus',function() {
    $(this).removeClass('fa-plus').addClass('fa-minus');
    $(".optionsDiv1").css("display","block");
    $(".optionsDiv2").css("display","none");
    $(".Fileoptions  .fa-minus").removeClass('fa-minus').addClass('fa-plus');
});
$(document).on('click','.Fileoptions  .fa-plus',function() {
    $(this).removeClass('fa-plus').addClass('fa-minus');
    $(".JobOptions .fa-minus").removeClass('fa-minus').addClass('fa-plus');
    $(".optionsDiv2").css("display","block");
    $(".optionsDiv1").css("display","none");
});
$(document).on('click','.Fileoptions  .fa-minus',function() {
    $(this).removeClass('fa-minus').addClass('fa-plus');
    //$(".JobOptions .fa-plus").removeClass('fa-plus').addClass('fa-minus');
    $(".optionsDiv2").css("display","none");
    //$(".optionsDiv1").css("display","block");
});
$(document).on('click','#Timeperiod .fa-chevron-down',function(){
    $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up")
    $(".dropdown-menu1").css("display","block");
    $(".selected_option").css("border-radius","0");
    $(".selected_option").css({"border-top-left-radius":"10px","border-top-right-radius":"10px"});
});
$(document).on('click','#Timeperiod .fa-chevron-up',function(){
    $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down")
    $(".dropdown-menu1").css("display","none");
    $(".selected_option").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
});
$(document).on('click','#Timeperiod2 .fa-chevron-down',function(){
    $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up")
    $(".dropdown-menu1job").css("display","block");
    $(".selected_optionjob").css("border-radius","0");
    $(".selected_optionjob").css({"border-top-left-radius":"10px","border-top-right-radius":"10px"});
});
$(document).on('click','#Timeperiod2 .fa-chevron-up',function(){
    $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down")
    $(".dropdown-menu1job").css("display","none");
    $(".selected_optionjob").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
});
//Delta
pastweek = function(){
    $(".selected_option span").text($("#Past_week").text());
    $(".dropdown-menu1").css("display","none");
    $(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_option").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
pastMonth = function(){
    $(".selected_option span").text($("#Past_Month").text());
    $(".dropdown-menu1").css("display","none");
    $(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_option").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
past3months = function(){
    $(".selected_option span").text($("#Past_3Months").text());
    $(".dropdown-menu1").css("display","none");
    $(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_option").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
//oneway-sync or two way sync
Daily = function(){
    $(".selected_optionjob span").text($("#Daily").text());
    $(".dropdown-menu1job").css("display","none");
    $(".selected_optionjob .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_optionjob").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
Weekly = function(){
    $(".selected_optionjob span").text($("#Weekly").text());
    $(".dropdown-menu1job").css("display","none");
    $(".selected_optionjob .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_optionjob").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
Monthly = function(){
    $(".selected_optionjob span").text($("#Monthly").text());
    $(".dropdown-menu1job").css("display","none");
    $(".selected_optionjob .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_optionjob").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
//fileoptions
$(document).on('click','#Timeperiodfile .fa-chevron-down',function(){
    $(this).removeClass("fa-chevron-down").addClass("fa-chevron-up")
    $(".dropdown-menufile").css("display","block");
    $(".selected_optionfile").css("border-radius","0");
    $(".selected_optionfile").css({"border-top-left-radius":"10px","border-top-right-radius":"10px"});
});
$(document).on('click','#Timeperiodfile .fa-chevron-up',function(){
    $(this).removeClass("fa-chevron-up").addClass("fa-chevron-down")
    $(".dropdown-menufile").css("display","none");
    $(".selected_optionfile").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
});
$('.allownumericwithdecimal').on("input propertychange paste",function (event) {
    var reg = /^0+/gi;
    if (this.value.match(reg)) {
        this.value = this.value.replace(reg, '');
    }
    return true;
});
$(".allownumericwithdecimal").keypress(function (e) {
    if(this.value.length == 0){
        if(e.which === 46)
            return false;
    }
    if (e.which != 8 && e.which != 0 && ((e.which < 48 && e.which != 46) || e.which > 57)) {
        return false;
    }
    if (e.which === 46 && this.value.split('.').length === 2) {
        return false;
    }
    var s = this.value;
    if(s.includes('.') === true)
        if(s.split('.')[1].length > 4){
            return false;
        }
});
pastweekfile = function(){
    $(".selected_optionfile span").text($("#Past_weekfile").text());
    $(".dropdown-menufile").css("display","none");
    $(".selected_optionfile .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_optionfile").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
pastMonthfile = function(){
    $(".selected_optionfile span").text($("#Past_Monthfile").text());
    $(".dropdown-menufile").css("display","none");
    $(".selected_optionfile .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_optionfile").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
past3monthsfile = function(){
    $(".selected_optionfile span").text($("#Past_3Monthsfile").text());
    $(".dropdown-menufile").css("display","none");
    $(".selected_optionfile .fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    $(".selected_optionfile").css({"border-bottom-left-radius":"10px","border-bottom-right-radius":"10px"});
}
